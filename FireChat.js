import firebase from 'firebase'; // 4.8.1
import AsyncStorage from '@react-native-community/async-storage';
import {call} from 'react-native-reanimated';

class Fire {
  constructor() {
    this.init();
    this.observeAuth();
  }

  state = {
    recieverId: null,
    conversationID: 0,
  };

  set recieverId(id) {
    this.state.recieverId = id;
  }

  init = () => {
    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: 'AIzaSyDnGnJX-OEzUXqIFSOjK1tkeBEbZJwLBQU',
        authDomain: 'referapps-1556610832512.firebaseapp.com',
        databaseURL: 'https://referapps-1556610832512.firebaseio.com/',
        projectId: 'referapps-1556610832512',
        storageBucket: 'referapps-1556610832512.appspot.com',
        messagingSenderId: '1047669812840',
        appId: '1:1047669812840:web:00092bfafb244f1cef95dd',
        measurementId: 'G-2K0E01D885',
        // apiKey: 'AIzaSyDLgW8QG1qO8O5WZLC1U8WaqCr5-CvEVmo',
        // authDomain: 'chatter-b85d7.firebaseapp.com',
        // databaseURL: 'https://chatter-b85d7.firebaseio.com',
        // projectId: 'chatter-b85d7',
        // storageBucket: '',
        // messagingSenderId: '861166145757',
      });
    }
  };

  observeAuth = () =>
    firebase.auth().onAuthStateChanged(this.onAuthStateChanged);

  onAuthStateChanged = (user) => {
    if (!user) {
      try {
        firebase.auth().signInAnonymously();
      } catch ({message}) {
        alert(message);
      }
    }
  };

  get uid() {
    return (firebase.auth().currentUser || {}).uid;
  }

  get ref() {
    return firebase.database().ref('messages');
  }

  get conversations() {
    return firebase.database().ref('conversation');
  }

  getThread = async (callback) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    Fire.shared.conversations.once('value', function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        if (userData.id == childData.sender.id) {
          callback(childKey, childData);
        }
        // console.log("Child Key", childKey)
        // console.log("Child Data", childData)
      });
    });
    return firebase.database().ref(`messages/${this.state.conversationID}`);
  };

  getConversation = async (reciever, callback) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    Fire.shared.conversations.once('value', function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        // console.log(childKey, childData, "THIS")
        if (
          childData.sender &&
          userData.data.id == childData.sender.id &&
          childData.reciever.id == reciever.id
        ) {
          // console.log("CONVERSATION FOUND!")
          callback(childKey, childData);
        }
      });
    });
  };

  getConversations = async (type, callback) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    // console.log("USER DATA", userData)
    Fire.shared.conversations.once('value', function (snapshot) {
      let result = [];
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        // console.log("USER DATA", userData.data)
        // console.log(childKey, childData, "THIS")
        if (childData.sender && userData.data.id == childData.sender.id) {
          // console.log("CONVERSATION FOUND!")
          var chatData;
          firebase
            .database()
            .ref(`messages/${childKey}`)
            .limitToLast(1)
            .once('value', (snapshot) => {
              snapshot.forEach(function (chatSnapShot) {
                var chatKey = chatSnapShot.key;
                chatData = chatSnapShot.val();
                result.push({
                  id: childKey,
                  data: childData.reciever,
                  lastUpdate: chatData.timestamp,
                  lastMessage:
                    chatData.user._id == userData.data.id
                      ? 'You: ' + chatData.text
                      : chatData.text,
                });
                // console.log("Result", result)
              });
              callback(result);
            });
        } else if (
          childData.reciever &&
          childData.reciever.id == userData.data.id
        ) {
          // console.log("CONVERSATION FOUND!")
          var chatData,
            unread = 0;
          firebase
            .database()
            .ref(`messages/${childKey}`)
            .limitToLast(1)
            .once('value', (snapshot) => {
              snapshot.forEach(function (chatSnapShot) {
                var chatKey = chatSnapShot.key;
                chatData = chatSnapShot.val();
                result.push({
                  id: childKey,
                  data: childData.sender,
                  lastUpdate: chatData.timestamp,
                  lastMessage:
                    chatData.user._id == userData.data.id
                      ? 'You: ' + chatData.text
                      : chatData.text,
                  isRead: chatData.isRead,
                });
                // console.log("Result", result)
              });
              callback(result);
            });
        }
      });
    });
  };

  readAll = async (childKey) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    // console.log("USER DATA", userData)
    firebase
      .database()
      .ref(`messages/${childKey}`)
      .once('value', (snapshot) => {
        snapshot.forEach(function (chatSnapShot) {
          chatSnapShot.ref.update({
            isRead: true,
          });
        });
      });
  };

  onNewConversationMsg = async (callback) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    // console.log("USER DATA", userData)
    Fire.shared.conversations.on('value', function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        // console.log("USER DATA", userData.data)
        console.log(childKey, childData, 'THIS');
        if (childData.sender && userData.data.id == childData.sender.id) {
          // console.log("CONVERSATION FOUND!")
          var chatData;
          firebase
            .database()
            .ref(`messages/${childKey}`)
            .limitToLast(1)
            .on('child_added', (snapshot) => {
              chatData = snapshot.val();
              callback({
                id: childKey,
                data: childData.sender,
                lastUpdate: chatData.timestamp,
                lastMessage:
                  chatData.user._id == userData.data.id
                    ? 'You: ' + chatData.text
                    : chatData.text,
              });
            });
        } else if (
          childData.reciever &&
          childData.reciever.id == userData.data.id
        ) {
          // console.log("CONVERSATION FOUND!")
          var chatData;
          firebase
            .database()
            .ref(`messages/${childKey}`)
            .limitToLast(1)
            .on('child_added', (snapshot) => {
              chatData = snapshot.val();
              callback({
                id: childKey,
                data: childData.sender,
                lastUpdate: chatData.timestamp,
                lastMessage:
                  chatData.user._id == userData.data.id
                    ? 'You: ' + chatData.text
                    : chatData.text,
                isRead: chatData.isRead,
              });
            });
        }
      });
    });
  };

  addConversation = async (messages, reciever, callback) => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    await this.conversations.push({
      sender: {
        id: userData.data.id,
        avatar: userData.data.image,
        name: userData.data.fname + ' ' + userData.data.lname,
      },
      reciever: reciever,
    });
    await Fire.shared.conversations.once('value', function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        var childData = childSnapshot.val();
        console.log(childKey, childData, 'THIS');
        if (
          childData.sender &&
          userData.data.id == childData.sender.id &&
          childData.reciever.id == reciever.id
        ) {
          console.log('CONVERSTATION FOUND!', messages);
          const {text, user} = messages[0];
          const message = {
            text,
            user,
            timestamp: Fire.shared.timestamp,
            // createdAt: new Date()
            isRead: false,
          };
          firebase.database().ref('messages').child(childKey).push(message);
          callback(childKey, childData);
        }
      });
    });
  };

  parse = (snapshot) => {
    const {timestamp: numberStamp, text, user} = snapshot.val();
    const {key: _id} = snapshot;
    const timestamp = new Date(numberStamp);
    const message = {
      _id,
      timestamp,
      createdAt: timestamp,
      text,
      user,
    };
    return message;
  };

  on = (id, callback) => {
    let reference = firebase
      .database()
      .ref(`messages/${id ? id : ''}`)
      // let reference = firebase.database().ref(`messages`)
      .limitToLast(20)
      .on('child_added', (snapshot) => callback(this.parse(snapshot)));
  };

  get timestamp() {
    return firebase.database.ServerValue.TIMESTAMP;
  }

  get creationDate() {
    let date = new Date();
  }

  // send the message to the Backend
  send = (messages, cvid) => {
    for (let i = 0; i < messages.length; i++) {
      const {text, user} = messages[i];
      const message = {
        text,
        user,
        timestamp: this.timestamp,
        isRead: false,
        // createdAt: new Date()
      };
      console.log('will append', cvid);
      this.append(message, cvid);
    }
  };

  append = (message, cvid) =>
    firebase.database().ref(`messages/${cvid}`).push(message);
  // append = message => this.ref.push(message)

  // close the connection to the Backend
  off() {
    this.ref.off();
  }
}

Fire.shared = new Fire();
export default Fire;
