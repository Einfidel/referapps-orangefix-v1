import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import storage from '@react-native-firebase/storage'
import functions from '@react-native-firebase/functions';

import React, { Component, useState, useRef, useEffect } from 'react'
import {
  StyleSheet, View, Text, TextInput, ScrollView, SafeAreaView,
  Dimensions, Keyboard, KeyboardAvoidingView, Platform,
  Alert,
  TouchableWithoutFeedback, TouchableOpacity, ActivityIndicator
} from 'react-native'
import palette from '../../../styles/palette'
import fontSizes from '../../../styles/fontSizes'
import container from '../../../styles/container'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import ButtonBlue from '../../button/button.blue.js'
import Br from '../../layout/Br'
import { useStoreState, useStoreActions } from 'easy-peasy'
import { ThemeContextConsumer } from '../../../styles/ThemeManager'
import theme from '../../../styles/theme'
import textInputPaddingVerticalWrapper from '../../helpers/textInputPaddingVerticalWrapper'
import { isYaziUser, isYaziGuide } from '../../helpers/getAppVariant'
import { Formik, useFormikContext } from 'formik'
import ButtonLinkedIn from '../../button/button.linkedin.js';
import ButtonGoogle from '../../button/button.google.js';
import * as Yup from 'yup'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/pro-solid-svg-icons';
import Toast from 'react-native-toast-message';

import countriesData from "../../../countries.json"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
SignUp.navigationOptions = {
  title: "Sign Up"
}

export default function SignUp({ navigation }) {

  function themeStyles(darkState) {
    return {
      TextField: {
        fontSize: 0.04 * Dimensions.get('window').width,
        color: '#283078',
        textAlign: 'left',
        marginTop: '1%',
        marginBottom: '1%',
        borderBottomColor: '#BDBDBD',
        borderBottomWidth: 2
      },
      TextFieldPassword: {
        flex: 1,
        fontSize: 0.04 * Dimensions.get('window').width,
        color: '#283078',
        textAlign: 'left',
        marginTop: '1%',

      },
      TextFieldPlaceholder: {
        fontSize: 0.04 * Dimensions.get('window').width,
        color: 'rgba(191,191,198,1)',
        textAlign: 'left',
        marginTop: '1%',
        marginBottom: '1%',
        borderBottomColor: '#BDBDBD',
        borderBottomWidth: 2,
      },
      DateField: {
        fontSize: 0.04 * Dimensions.get('window').width,
        color: '#283078',
        textAlign: 'left',
        marginTop: '4%',
        marginBottom: '1%',
        borderBottomColor: '#BDBDBD',
        paddingBottom: Platform.OS == "ios" ? 0 : 12,
        borderBottomWidth: 2,
      },
      DateFieldPlaceHolder: {
        fontSize: 0.04 * Dimensions.get('window').width,
        color: 'rgba(191,191,198,1)',
        textAlign: 'left',
        marginTop: '4%',
        marginBottom: '1%',
        borderBottomColor: '#BDBDBD',
        paddingBottom: Platform.OS == "ios" ? 0 : 12,
        borderBottomWidth: 2,
        marginRight: '5%',
      },
      columnOne: {
        flex: Platform.OS === "ios" ? 8 : 1,
        width: "90%",
        marginTop: wp('5%'),
        marginLeft: Platform.OS === "ios" ? wp('5%') : null,
        justifyContent: "center",
        alignContent: "center",

      },
      columnTwo: {
        flex: 5,
        width: Platform.OS === "ios" ? " 90%" : '90%',
        alignContent: "center",
        marginBottom: wp('5%'),
      },
      columnThree: {
        flex: 3,
        width: '100%',
        alignItems: 'center',
        justifyContent: "center",
        alignContent: "center",
        marginBottom: '5%',
      },
      bodyContainer: {
        flexDirection: 'row',
        width: '95%',
        alignItems: 'flex-start',
      },
      ErrorField: {
        color: 'red'
      }
    }
  }

  const [isFetchingData, setIsFetchingData] = useState(false)
  const [isSecured, setIsSecured] = useState(true)
  const scrollRef = useRef(null);
  const saveProfile = useStoreActions((action) => action.profile.saveProfile)



  const onRegisterPress = async (newValues) => {
    const values = newValues

    setIsFetchingData(true)
    const checkUserAuthEmailOnCall = functions().httpsCallable('checkUserAuthEmailOnCall')

    try {
      const userData = await checkUserAuthEmailOnCall({ email: values.email })
      const uid = userData?.data?.uid
      if (uid) {
        Alert.alert(
          'Oooopppss.',
          `Email is already used in our Yazi ecosystem. Try logging in using your credentials.`,
          [
            { text: 'OK', onPress: () => navigation.navigate('LogIn') },
          ],
          { cancelable: false },
        )
        return;
      }
    } catch (e) {
      Alert.alert(
        'Oooopppss.',
        `Something happened, please try again`,
        [
          { text: 'OK', onPress: () => setIsFetchingData(false) },
        ],
        { cancelable: false },
      )

      return;
    }

    const checkUserAuthMobileOnCall = functions().httpsCallable('checkUserAuthMobileOnCall')

    try {
      const userData = await checkUserAuthMobileOnCall({ phoneNumber: values.phone })
      const uid = userData?.data?.uid
      if (uid) {
        Alert.alert(
          'Oooopppss.',
          `Phone is already used in our Yazi ecosystem. Try logging in using your credentials.`,
          [
            { text: 'OK', onPress: () => navigation.navigate('LogIn') },
          ],
          { cancelable: false },
        )
        return;
      }
    } catch (e) {
      Alert.alert(
        'Oooopppss.',
        `Something happened, please try again`,
        [
          { text: 'OK', onPress: () => setIsFetchingData(false) },
        ],
        { cancelable: false },
      )

      return;
    }

    try {
      auth()
        .createUserWithEmailAndPassword(values.email, values.password)
        .then((response) => {
          console.log("success creating account")
          auth()
            .signInWithEmailAndPassword(values.email, values.password)
            .then((response) => {
              console.log("signing in with email and pass")
              let userType
              let guideType
              let approvalStatus
              if (isYaziGuide()) {
                userType = ""
                guideType = "tutor"
                approvalStatus = "pending"
              }
              else if (isYaziUser()) {
                userType = 'student'
                guideType = ""
                approvalStatus = "active"
              }
              const usersRef = firestore().collection('users')
              const uid = response.user.uid

              const data = {
                id: uid,
                email: values.email,
                ///     fullName: values.fullName,
                phone: values.phone,
                //  birthday: firestore.Timestamp.fromDate(date), // values.birth,
                userType,
                guideType,
                isEmailVerified: false,
                isPhoneVerified: false,
                dateCreated: firestore.FieldValue.serverTimestamp(),
                approvalStatus,
                signupDone: false
                //   region: values.region,
                //     city: values.city,
              }

              const dataAsync = {
                id: uid,
                email: values.email,
                ///     fullName: values.fullName,
                phone: values.phone,
                //  birthday: firestore.Timestamp.fromDate(date), // values.birth,
                userType,
                guideType,
                isEmailVerified: false,
                isPhoneVerified: false,
                approvalStatus,
                signupDone: false
                //   region: values.region,
                //     city: values.city,
              }

              try {
                console.log("setting data ... async")
                saveProfile(dataAsync)
              } catch (e) {
                console.log("setting data error async", e)
              }

              usersRef
                .doc(uid)
                .set(data)
                .then((query) => {
                  console.log("writing .... firebase")
                  navigation.replace('Verify', { screen: "VerificationPage" })
                })
                .catch((error) => {
                  setIsFetchingData(false)
                  console.log("not loading properly then after set")
                  alert("Encountered a setting error.", error)
                })
            })
            .catch((error) => {
              setIsFetchingData(false)
              console.log("not loading properly setting")
              alert("Encountered a setting error.", error)
            })
        })
        .catch((error) => {
          setIsFetchingData(false)
          //prompts when duplicate email
          alert("Email already exist.", error)
        })
    } catch (e) {
      console.log("error sigup", e)
    }
  }

  const saveLogin = useStoreActions(actions => actions.auth.login)

  const onSkipPress = () => {
    auth()
      .signInWithEmailAndPassword('franzelclark@gmail.com', 'clark1234')
      .then((response) => {
        const uid = response.user.uid
        const usersRef = firestore().collection('users')
        usersRef
          .doc(uid)
          .get()
          .then(firestoreDocument => {
            if (!firestoreDocument.exists) {
              alert("User does not exist anymore.")
              return
            }
            const user = firestoreDocument.data()
            console.log(user)
            saveLogin(user)
            //navigation.navigate('Dashboard', {user})
            //navigation.navigate('Chat', {user})
          })
          .catch(error => {
            alert(error)
          })
      })
      .catch(error => {
        alert(error)
      })
  }

  return (<ThemeContextConsumer>
    {context => (
      <SafeAreaView style={container}>
        <KeyboardAwareScrollView
          innerRef={ref => { scrollRef.current = ref; }}
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
        >
          <Formik
            initialValues={{
              email: '',
              phone: '',
              password: '',
              referral: ''
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string().email('Please enter a valid email address').required('Your email is required'),
              phone: Yup.number('Please enter a valid phone number').required('Your phone number is required'),
              password: Yup.string().required('Your password is required').min(6, 'Your password is too short'),
              referral: Yup.string()
            })}
            onSubmit={values => onRegisterPress(values)}
          >
            {({ errors, touched, values, handleChange, handleBlur, handleSubmit }) => (
              <View style={{ flex: 1, justifyContent: "center", alignContent: "center", alignItems: "center" }} >
                <View style={themeStyles(context.theme).columnOne}>
                  <Text style={{ ...fontSizes.heading3, color: context.theme ? theme.dark.text : theme.light.text, marginBottom: wp('5%') }}>Sign up</Text>
                  <View style={themeStyles(context.theme).bodyContainer}>
                    <Text style={{ ...fontSizes.textSub, color: context.theme ? theme.dark.text : theme.light.text }}>Already have an account?</Text>
                    <Text onPress={() => navigation.navigate('LogIn')} style={{ ...fontSizes.textSub, color: context.theme ? theme.dark.accent : theme.light.accent, marginLeft: wp('2.5%'), }}>
                      Log in
                    </Text>
                  </View>
                </View>
                <View style={themeStyles(context.theme).columnTwo} >
                  <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                    Email Address
                    </Text>

                  <TextInput
                    paddingVertical={textInputPaddingVerticalWrapper()}
                    placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                    placeholder='user@gmail.com'
                    name="email"
                    onChangeText={handleChange('email')}
                    onBlur={handleBlur('email')}
                    style={themeStyles(context.theme).TextField}
                    // onChangeText={(text) => {setEmail(text)}}
                    value={values.email}
                    autoCapitalize="none"
                    keyboardType="email-address"
                    autoCorrect={false}
                  />
                  {errors.email && touched.email ? <Text style={themeStyles(context.theme).ErrorField}>{errors.email}</Text> : null}

                  <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                    Phone number
                  </Text>

                  <TextInput
                    paddingVertical={textInputPaddingVerticalWrapper()}
                    placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                    placeholder='+9939 99393 3'
                    onChangeText={handleChange('phone')}
                    onBlur={handleBlur('phone')}
                    style={themeStyles(context.theme).TextField}
                    // onChangeText={(text) => setPhone(text)}
                    value={values.phone}
                    autoCapitalize="none"
                    keyboardType="phone-pad"

                  />
                  {errors.phone && touched.phone ? <Text style={themeStyles(context.theme).ErrorField}>{errors.phone}</Text> : null}

                  <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                    Password
                  </Text>
                  <View style={{
                    flexDirection: 'row',
                    borderBottomWidth: 2,
                    borderColor: '#BDBDBD',
                  }}>
                    <TextInput
                      paddingVertical={textInputPaddingVerticalWrapper()}
                      placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                      placeholder='Password'
                      onChangeText={handleChange('password')}
                      onBlur={handleBlur('password')}
                      style={themeStyles(context.theme).TextFieldPassword}
                      secureTextEntry={isSecured}
                      // onChangeText={(text) => setPassword(text)}
                      value={values.password}
                      autoCapitalize="none"

                    />
                    <TouchableOpacity onPress={() => setIsSecured(!isSecured)}>
                      {
                        isSecured ? <FontAwesomeIcon icon={faEyeSlash} size={wp('7%')} color={"gray"} /> : <FontAwesomeIcon icon={faEye} size={wp('7%')} color={isYaziUser() ? "rgba(255,108,26,0.8)" : "rgba(37, 110, 255,0.8)"} />
                      }
                    </TouchableOpacity>
                  </View>
                  {errors.password && touched.password ? <Text style={themeStyles(context.theme).ErrorField}>{errors.password}</Text> : null}
                  <View style={{ flexDirection: "row", }}>
                    <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                      Referral Code
                  </Text>
                    <Text style={{ ...fontSizes.textSub2, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                      (Optional)
                    </Text>
                  </View>
                  <View style={{

                  }}>
                    <TextInput
                      paddingVertical={textInputPaddingVerticalWrapper()}
                      placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                      placeholder='Referral Code'
                      onChangeText={handleChange('referral')}
                      onBlur={handleBlur('referral')}
                      style={themeStyles(context.theme).TextField}
                      value={values.referral}
                      autoCapitalize="none"
                    />

                  </View>
                  {errors.referral && touched.referral ? <Text style={themeStyles(context.theme).ErrorField}>{errors.referral}</Text> : null}
                </View>
                <View style={themeStyles(context.theme).columnThree}>
                  <ButtonBlue onPress={handleSubmit} disabled={isFetchingData}>Next</ButtonBlue>
                  {/* <Text onPress={() => navigate('VerificationPage')} style={{...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: wp('5%') }}>
                  Skip for now
                </Text> */}
                  {/* <Text style={{ ...fontSizes.heading6, color: context.theme ? theme.dark.text : theme.light.text, alignSelf: 'center', margin: wp('2%'), }}>
                    OR
                  </Text> */}
                  {/* <ButtonGoogle onPress={() => Toast.show({
                    type: 'info',
                    position: 'bottom',
                    text1: 'Thank you for clicking!',
                    text2: 'We are still working on this feature.',
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 40,
                    onShow: () => { },
                    onHide: () => { },
                    onPress: () => { }
                  })} >Sign up with Google</ButtonGoogle>
                  <View style={{ margin: wp('2%') }} />
                  <ButtonLinkedIn onPress={() => Toast.show({
                    type: 'info',
                    position: 'bottom',
                    text1: 'Thank you for clicking!',
                    text2: 'We are still working on this feature.',
                    visibilityTime: 4000,
                    autoHide: true,
                    bottomOffset: 40,
                    onShow: () => { },
                    onHide: () => { },
                    onPress: () => { }
                  })} >Sign up with LinkedIn</ButtonLinkedIn> */}


                </View>
              </View>
            )}
          </Formik>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    )
    }
  </ThemeContextConsumer >
  )
}
