import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import storage from '@react-native-firebase/storage'
import functions from '@react-native-firebase/functions'

import React, { Component, useEffect, useState, useLayoutEffect, useRef } from 'react';
import { Button, StyleSheet, View, Text, TextInput, SafeAreaView, Dimensions, Keyboard, KeyboardAvoidingView, Platform, Animated, Alert } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Formik } from 'formik';
import { useStoreState, useStoreActions } from 'easy-peasy'
import { isYaziUser, isYaziGuide } from '../helpers/getAppVariant'
import { ThemeContextConsumer } from '../../styles/ThemeManager'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';

import palette from '../../styles/palette'
import fontSizes from '../../styles/fontSizes'
import container from '../../styles/container'
import ButtonBlue from '../button/button.blue.js';
import ButtonLinkedIn from '../button/button.linkedin.js';
import ButtonGoogle from '../button/button.google.js';
import TextInputPassword from '../textinput/textinput.password.js';
import TextInputEmail from '../textinput/textinput.email.js';
import Br from '../layout/Br'
import theme from '../../styles/theme'
import * as Yup from 'yup';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faEye, faEyeSlash } from '@fortawesome/pro-solid-svg-icons';
import textInputPaddingVerticalWrapper from '../helpers/textInputPaddingVerticalWrapper'
// import { GoogleSignin, statusCodes } from '@react-native-community/google-signin'
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
// import auth from '@react-native-firebase/auth'
// import firestore from '@react-native-firebase/firestore'
import backgroundServer from 'react-native-background-actions';
import Toast from 'react-native-toast-message';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ButtonGrayLogin from '../button/button.gray.login';
// LoginScreen.navigationOptions = {
// 	title: "Log In",
// 	headerLeft: () => {
// 		return null
// 	}
// }

export default function LoginScreen({ navigation, route }) {
	// navigation.setOptions({
	// 	headerShown: true
	// })
	function themeStyles(darkState) {
		return {
			ColumnContainer: {
				flexDirection: 'row',
			},
			columnOne: {
				flex: 1,
				width: '90%',
				alignSelf: "center",
				alignContent: "center",
				marginTop: wp('5%'),

			},
			columnTwo: {
				flex: Platform.OS === "android" ? 5 : 7,
				alignSelf: "center",
				alignContent: "center",
				width: '90%',
				marginTop: Platform.OS === "android" ? '4.5%' : 0
			},
			columnThree: {
				flex: 4,
				width: wp('100%'),
				justifyContent: "center"
			},
			ErrorField: {
				fontFamily: 'Arial',
				fontSize: 15,
				color: darkState ? theme.dark.errorText : theme.light.errorText,
				textAlign: 'left',
				marginBottom: 5,

				marginTop: 5,
			},
			formikContainer: {
				flex: .1,
				width: '100%',
			},
		}
	}

	// temporary to streamline dev testing
	// const [email, setEmail] = useState(isYaziUser() ? 'dev@yazi.com' : 'tutor@yazi.com') 
	const [email, setEmail] = useState(isYaziUser() ? 'dev@yazi.com' : 'charliesiphron_narnia@gmail.com')
	//const [email, setEmail] = useState(isYaziUser() ? 'dev@yazi.com' : 'john2@gmail.com')
	//const [email, setEmail] = useState('dev@yazi.com')
	//const [email, setEmail] =  useState('charliesiphron_narnia@gmail.com')

	//const [password, setPassword] = useState(isYaziUser() ? 'yazi123' : 'yazi123')
	const [password, setPassword] = useState(isYaziUser() ? 'yazi123' : 'charlie')
	//const [password, setPassword] = useState('yazi123')
	//const [password, setPassword] = useState('charlie')
	const [prevScreen, setPrevScreen] = useState("")
	const hasUnsavedChanges = Boolean(prevScreen);
	// global state
	const saveLogin = useStoreActions(actions => actions.auth.login)
	const saveProfile = useStoreActions((action) => action.profile.saveProfile)
	const [KeyboardVisible, setKeyboardVisible] = useState(false)
	const [isSecured, setIsSecured] = useState(true)
	const scrollRef = useRef(null);
	// temporary to streamline dev testing
	// useEffect(() => {
	// 	if (hasUnsavedChanges) {
	// 		// props.navigation.setOptions(
	// 		// 	{
	// 		// 		title: 'Updated!',
	// 		// 		headerLeft: () => {
	// 		// 			return null
	// 		// 		}
	// 		// 	})
	// 	}

	// 	// auth().signOut().then(function () {
	// 	// 	// Sign-out successful.
	// 	// }).catch(function (error) {
	// 	// 	// An error happened.
	// 	// });

	// 	const keyboardDidShowListener = Keyboard.addListener(
	// 		'keyboardDidShow',
	// 		() => {
	// 			setKeyboardVisible(true); // or some other action
	// 			console.log("Hellow Show")
	// 			//	scrollRef.current.scrollToEnd({ animated: true })
	// 			setTimeout(() => {
	// 				//scrollRef.current.scrollToEnd({ animated: true })
	// 				scrollRef.current.scrollTo({ x: 0, y: 0, animated: false });
	// 			}, 0.5)
	// 		}
	// 	);
	// 	const keyboardDidHideListener = Keyboard.addListener(
	// 		'keyboardDidHide',
	// 		() => {
	// 			setKeyboardVisible(false); // or some other action
	// 			console.log("Hellow Hide")
	// 		}
	// 	);

	// 	return () => {
	// 		keyboardDidHideListener.remove();
	// 		keyboardDidShowListener.remove();
	// 	};
	// }
	// )
	const _scrollToInput = (e) => {
		scrollRef.current.scrollToFocusedInput(e)
	}


	const onLoginPress = (values) => {
		const { email, password } = values
		console.log(email, password)
		try {
			auth()
				.signInWithEmailAndPassword(email, password)
				.then((response) => {


					// auth().currentUser.getIdToken(/* forceRefresh */ true).then((idToken) => {
					// 	console.log("this is token", idToken) //save to easy peasy token
					// }).catch(error => {
					// 	alert("idToken", error)
					// })

					try {
						const user = response.user
						const uid = user.uid
						const usersRef = firestore().collection('users')
						usersRef
							.doc(uid)
							.get()
							.then(async (firestoreDocument) => {
								if (!firestoreDocument.exists) {
									console.log(" is there a doucment")
									//Create a data in collection based in firebase details
									let userType
									let guideType
									let approvalStatus
									if (isYaziGuide()) {
										userType = ""
										guideType = "tutor"
										approvalStatus = "pending"
									} else {
										userType = 'student'
										guideType = ""
										approvalStatus = "active"
									}
									const userData = {
										userType,
										guideType,
										id: response?.user?.uid,
										fullName: response?.user?.displayName,
										email: response?.user?.email,
										isEmailVerified: response?.user?.emailVerified,
										phoneNumber: response?.user?.phoneNumber,
										isPhoneVerified: false,
										dateCreated: firestore.FieldValue.serverTimestamp(),
										signupDone: false,
										approvalStatus
									}
									await usersRef
										.doc(uid)
										.set(userData)
										.then((query) => {
											console.log("writing .... firebase", userData)
											navigation.replace('Verify', { screen: "VerificationPage" })
										})
										.catch((error) => {
											console.log("not loading properly then after set")
											alert("Encountered a setting error.", error)
										})
									return;
								}

								const user = firestoreDocument.data()
								const fixedBday = firestoreDocument.data()?.birthday?.toDate()
								const fixedDateCreated = firestoreDocument.data()?.dateCreated?.toDate()
								const fixedData = { ...user, birthday: fixedBday, dateCreated: fixedDateCreated }
								saveLogin(user)
								saveProfile(fixedData)
								//navigation.navigate('Dashboard', {user})
								//navigation.navigate('Chat', {user})
								//TODO
								if (fixedData.isEmailVerified || fixedData.isPhoneVerified) {
									navigation.replace("Dashboard")
									// if (isYaziUser()) {
									// 	navigation.replace("Dashboard", { screen: "Dashboard" })
									// } else {
									// 	navigation.replace("Dashboard", { screen: "MentorDashboard" })
									// }
								} else {
									navigation.replace("Verify")
								}
							})
							.catch(error => {
								alert("logging error", error)
							})
					} catch (e) {
						console.log("error in moving to dashboard", e)
					}
				})
				.catch(error => {
					alert(error)
				})
		} catch (e) {
			console.log("login catch", e)
		}

	}

	// not working on mobile environment
	// signInWithGoogle = async () => {

	// 	var provider = new auth.GoogleAuthProvider()
	// 	provider.addScope('profile')
	// 	provider.addScope('email')

	// 	auth().signInWithPopup(provider).then(function(result) {
	// 		// This gives you a Google Access Token.
	// 		var token = result.credential.accessToken
	// 		// The signed-in user info.
	// 		var user = result.user

	// 		console.log({ user })
	// 	})
	// }
	// const signInGoogles = () => {
	// 	Toast.show({
	// 		type: 'info',
	// 		position: 'bottom',
	// 		text1: 'Thank you for clicking!',
	// 		text2: 'We are still working on this feature.',
	// 		visibilityTime: 4000,
	// 		autoHide: true,
	// 		bottomOffset: 40,
	// 		onShow: () => { },
	// 		onHide: () => { },
	// 		onPress: () => { }
	// 	})
	// }

	const prepareDataIfRegister = async ({ uid, values }) => {
		console.log("signing in google")
		let userType
		let guideType
		let userStatus
		// if (isYaziGuide()) {
		// 	// TODO: 'tutor' for now use global.guideType but need to be redirected first to tutor / mentor selection screen 
		// 	userType = '' //global.guideType
		// 	guideType = "tutor"
		// 	userStatus = "pending"
		// }
		// else if (isYaziUser()) {
		// 	userType = 'student'
		// 	guideType = ""
		// 	userStatus = "pending"
		// }
		const usersRef = firestore().collection('users')
		//const uid = response.user.uid
		const typeOfUser = isYaziUser() ? 'student' : 'guide'

		const data = {
			id: uid,
			email: values.email,
			///     fullName: values.fullName,
			phone: values.phone,
			//  birthday: firestore.Timestamp.fromDate(date), // values.birth,
			// userType,
			// guideType,
			isEmailVerified: false,
			isPhoneVerified: false,
			dateCreated: firestore.FieldValue.serverTimestamp(),
			//	approvalStatus: userStatus,
			//	signupDone: false
			//   region: values.region,
			//     city: values.city,

			googleSignIn: true
		}

		const dataAsync = {
			id: uid,
			email: values.email,
			///     fullName: values.fullName,
			phone: values.phone,
			//  birthday: firestore.Timestamp.fromDate(date), // values.birth,
			// userType,
			// guideType,
			isEmailVerified: values.isEmailVerified, // TODO: will get directly from Google
			isPhoneVerified: false,
			//		approvalStatus: userStatus,
			//		signupDone: false
			//   region: values.region,
			//     city: values.city,

			googleSignIn: true
		}

		try {
			console.log("setting data ... async")
			saveProfile(dataAsync)
		} catch (e) {
			console.log("setting data error async", e)
		}


		usersRef
			.doc(uid)
			.set(data)
			.then((query) => {
				console.log("writing .... firebase")

				if (values.isEmailVerified) {
					if (isYaziGuide()) {
						navigation.replace('Dashboard', { screen: "MentorDashboard" })
					} else {
						navigation.replace('Dashboard', { screen: "Dashboard" })
					}
				} else {
					navigation.replace('Verify', { screen: "VerificationPage" })
				}
				// const user = auth().currentUser
				// user.updateProfile({
				//   displayName: values.email,
				//   //    photoURL:
				// })
				//   .then(() => {
				//     navigation.replace('Verify', { screen: "VerificationPage" })

				//     // navigation.navigate('Verify', { screen: "VerificationPage", params: { userData: data } })
				//   })
				//   .catch((error) => {
				//     setIsFetchingData(false)
				//     console.log("not loading properly update profile")
				//     alert("update display name ", error)
				//   })
				// user.updatePhoneNumber({

				// }).then(() => {

				// })
				//   .catch((error) => {

				//   })
			})
			.catch((error) => {
				setIsFetchingData(false)
				console.log("not loading properly then after set")
				alert("setting data", error)
			})
	}

	const signInWithGoogle = async () => {

		// not supported on native implementations
		// const response = await auth().signInWithPopup(auth.GoogleAuthProvider)
		// .catch(error => {
		// 	console.log(error.code)
		// })
		// return

		// same case - not supported on native implementations
		// const response = await auth().signInWithRedirect(auth.GoogleAuthProvider)
		// .catch(error => {
		// 	console.log(error.code)
		// })
		// return

		try {
			await GoogleSignin.hasPlayServices()
			//const userInfo = await GoogleSignin.signIn()
			//this.setState({ userInfo })
			//console.log({ userInfo })

			const { idToken, user } = await GoogleSignin.signIn()
			const { email } = user

			console.log({ email })

			const checkUserAuthEmailOnCall = functions().httpsCallable('checkUserAuthEmailOnCall')
			const userData_fromFunc = await checkUserAuthEmailOnCall({ email })
			const uid_fromFunc = userData_fromFunc?.data?.uid

			console.log({ userData_fromFunc })
			console.log({ providerData: userData_fromFunc.providerData })

			const checkUserAuthEmailOnCall_WithDB = functions().httpsCallable('checkUserAuthEmailOnCall_WithDB')
			const emailOnDB = await checkUserAuthEmailOnCall_WithDB({ email })

			let googleSignIn = false

			if (emailOnDB) {

				console.log({ emailOnDB })
				console.log({ emailOnDB_err: emailOnDB?.data?.e })

				googleSignIn = emailOnDB?.data?.data?.googleSignIn

				console.log({ googleSignIn })
			}

			if (uid_fromFunc && !googleSignIn) {
				Alert.alert(
					'Oooopppss.',
					`Email is already used in our Yazi ecosystem. If you want to use Google sign-in, try logging in using the same email, go to Settings and tap Link Google Account.`,
					[
						{ text: 'OK', onPress: () => navigation.navigate('LogIn') },
					],
					{ cancelable: false },
				)
				// TODO: prompt user for password and do the link credential function
				return
			}


			// Create a Google credential with the token
			const googleCredential = auth.GoogleAuthProvider.credential(idToken)
			//const googleCredential = firebase.auth.GoogleAuthProvider.credential(idToken)
			console.log({ googleCredential })

			// Sign-in the user with the credential
			const response = await auth().signInWithCredential(googleCredential)
			console.log({ user: response.user })

			// const response0 = await auth().signInWithCredential(googleCredential)
			// console.log({ user: response0.user })
			// const response = await response0.user.linkWithCredential(googleCredential)

			const uid = response.user.uid
			const usersRef = firestore().collection('users')

			usersRef.doc(uid)
				.get()
				.then(firestoreDocument => {
					if (!firestoreDocument.exists) {
						//alert("User does not exist anymore.")
						const user_ = response.user
						const values = {
							id: uid,
							email: user_.email,
							fullName: user_.displayName,
							isEmailVerified: user_.emailVerified
						}
						prepareDataIfRegister({ uid, values })
						return
					}

					const user = firestoreDocument.data()
					console.log(user)
					saveLogin(user)
					//navigation.navigate('Dashboard', {user})
					//navigation.navigate('Chat', {user})
					if (isYaziUser()) {
						navigation.replace("Dashboard", { screen: "Dashboard" })
					} else {
						navigation.replace("Dashboard", { screen: "MentorDashboard" })
					}

				})
				.catch(error => {
					alert(error)
				})

			// Alert.alert(
			// 	"Success",
			// 	JSON.stringify({ payload: userInfo.user }),
			// 	[
			// 		{
			// 			text: "Cancel",
			// 			onPress: () => console.log("Cancel Pressed"),
			// 			style: "cancel"
			// 		},
			// 		{ text: "OK", onPress: () => console.log("OK Pressed") }
			// 	],
			// 	{ cancelable: false }
			// )

		} catch (error) {

			// Alert.alert(
			// 	"Error",
			// 	JSON.stringify({ error }),
			// 	[
			// 		{
			// 			text: "Cancel",
			// 			onPress: () => console.log("Cancel Pressed"),
			// 			style: "cancel"
			// 		},
			// 		{ text: "OK", onPress: () => console.log("OK Pressed") }
			// 	],
			// 	{ cancelable: false }
			// )
			console.log({ error })

			if (error.code === statusCodes.SIGN_IN_CANCELLED) {
				// user cancelled the login flow
				console.log('sign in error - cancelled')
			} else if (error.code === statusCodes.IN_PROGRESS) {
				// operation (e.g. sign in) is in progress already
				console.log('sign in error - in progress')
			} else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
				// play services not available or outdated
				console.log('sign in error - play services not available')
			} else {
				// some other error happened
				console.log('sign in error - other')
			}
		}
	}
	const signInWithLinkedIn = () => {
		Toast.show({
			type: 'info',
			position: 'bottom',
			text1: 'Thank you for clicking!',
			text2: 'We are still working on this feature.',
			visibilityTime: 4000,
			autoHide: true,
			bottomOffset: 40,
			onShow: () => { },
			onHide: () => { },
			onPress: () => { }
		})
	}

	const sendPasswordResetEmail = (values) => {
		const emailAddress = values.email
		auth().sendPasswordResetEmail(emailAddress).then(function () {
			// Email sent.
			alert('Successfully sent password reset email. Please check your email.')
		}).catch(function (error) {
			// An error happened.
			console.log(error)
			alert('Failed to send password reset email.')
		})
	}


	return (<ThemeContextConsumer>
		{context => (
			<SafeAreaView style={container}>
				<KeyboardAwareScrollView
					innerRef={ref => { scrollRef.current = ref; }}
					contentContainerStyle={{ flexGrow: 1 }}
					showsVerticalScrollIndicator={false}
					showsHorizontalScrollIndicator={false}
				>
					<Formik
						initialValues={{
							email: '',
							password: ''
						}}
						validationSchema={Yup.object().shape({
							email: Yup.string().email('Please enter a valid email address').required('Your email is required'),
							password: Yup.string().required('Your password is required').min(6, 'Your password is too short'),
						})}
						onSubmit={values => onLoginPress(values)}
					>
						{({ errors, touched, handleChange, handleBlur, handleSubmit, values }) => (
							<View style={{ flex: 1 }}>
								<View style={themeStyles(context.theme).columnOne}>
									<Text style={{ ...fontSizes.heading3, color: context.theme ? theme.dark.text : theme.light.text, marginBottom: wp('5%') }}>
										Welcome back!
									</Text>

									<View style={themeStyles(context.theme).ColumnContainer}>
										<Text style={{ ...fontSizes.textSub, color: context.theme ? theme.dark.text : theme.light.text, }}>
											Don't have an account?
										</Text>
										<Text onPress={() => navigation.navigate('SignUp')} style={{ ...fontSizes.textSub, marginLeft: wp('2.5%'), color: context.theme ? theme.dark.accent : theme.light.accent, }}>
											Sign up
										</Text>
									</View>
								</View>
								<View style={themeStyles(context.theme).columnTwo}>

									<Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text }}>
										Email Address
									</Text>

									<TextInputEmail
										placeholder={"user@gmail.com"}
										onChangeText={handleChange('email')}
										onBlur={handleBlur('email')}
										value={values.email}
									/>
									{errors.email && touched.email ? <Text style={themeStyles(context.theme).ErrorField}>{errors.email}</Text> : null}

									<Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: hp("2%") }}>
										Password
									</Text>
									<View style={{
										flexDirection: 'row',
										borderBottomWidth: 2,
										borderColor: '#BDBDBD',
										justifyContent: 'center',
										alignItems: 'flex-end',


									}}>
										<TextInput
											paddingVertical={textInputPaddingVerticalWrapper()}
											placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
											placeholder='Password'
											onChangeText={handleChange('password')}
											onBlur={handleBlur('password')}
											style={{
												flex: 1,
												fontSize: 15,
												color: '#283078',
												textAlign: 'left',
												marginTop: '1%',
											}}
											secureTextEntry={isSecured}
											// onChangeText={(text) => setPassword(text)}
											value={values.password}
											autoCapitalize="none"
										/>
										<TouchableOpacity style={{ marginBottom: 10 }} onPress={() => setIsSecured(!isSecured)}>
											{
												isSecured ? <FontAwesomeIcon icon={faEyeSlash} size={24} color={"gray"} /> : <FontAwesomeIcon icon={faEye} size={24} color={isYaziUser() ? "rgba(255,108,26,0.8)" : "rgba(37, 110, 255,0.8)"} />
											}
										</TouchableOpacity>
									</View>
									{errors.password && touched.password ? <Text style={themeStyles(context.theme).ErrorField}>{errors.password}</Text> : null}

									<TouchableOpacity onPress={() => {
										// TODO: enable when needed / ready
										// auth().sendPasswordResetEmail('raymel@orangefix.net').then(() => {
										//   console.log('email sending done')
										// })
										// .catch(err => {
										//   console.log({ emailSendingError: err })
										// })
										if (values && values.email) {
											sendPasswordResetEmail(values)
										} else {
											alert('Please enter an email address')
										}
									}}>
										<Text style={{ ...fontSizes.textSub, color: 'grey', marginBottom: wp('5%'), textAlign: "right", marginTop: 10 }}>
											Forgot your password?
										</Text>
									</TouchableOpacity>

								</View>
								<View style={themeStyles(context.theme).columnThree}>
									<ButtonBlue onPress={handleSubmit}>Log in</ButtonBlue>
									<Text style={{ ...fontSizes.heading6, color: context.theme ? theme.dark.text : theme.light.text, alignSelf: 'center', margin: wp('2%'), }}>
										OR
									</Text>
									<ButtonGrayLogin onPress={() => { navigation.navigate('VerificationSmsPage_Login') }}>Continue with Phone</ButtonGrayLogin>
									<View style={{ margin: wp('2%') }} />
									<ButtonGoogle onPress={signInWithGoogle}>Continue with Google</ButtonGoogle>
									<View style={{ margin: wp('2%') }} />
									<ButtonLinkedIn onPress={signInWithLinkedIn}>Continue with LinkedIn</ButtonLinkedIn>
									<View style={{ margin: wp('2%') }} />
								</View>
								{/* 
								{KeyboardVisible ? <View style={{ height: hp("20%") }} /> : null} */}
							</View>
						)}
					</Formik>

				</KeyboardAwareScrollView>
			</SafeAreaView>)}
	</ThemeContextConsumer>
	)
}
