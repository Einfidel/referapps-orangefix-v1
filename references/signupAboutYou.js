import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import storage from '@react-native-firebase/storage'

import React, { useState, useEffect, useRef } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Keyboard, KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Button, findNodeHandle
} from 'react-native';
import { ThemeContextConsumer } from '../../../styles/ThemeManager'
import theme from '../../../styles/theme';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import textInputPaddingVerticalWrapper from '../../helpers/textInputPaddingVerticalWrapper';
import palette from '../../../styles/palette';
import fontSizes from '../../../styles/fontSizes';
import container from '../../../styles/container';
import ButtonBlue from '../../button/button.blue.js';
;
import { Formik } from 'formik';
import * as Yup from 'yup';
import Br from '../../layout/Br'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from 'moment'
import MultiSelect from 'react-native-multiple-select';
import { sortedCountries } from "../../utils/countriesManager"
import { isYaziUser } from '../../helpers/getAppVariant';
import { useStoreState, useStoreActions } from 'easy-peasy'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDownPicker from 'react-native-dropdown-picker';
import { generateUUID } from "../../utils/uniqueKeyGenerator"
import countriesData from "../../../countries.json"
import languagesData from "../../../languages.json"

let countriesNew = Object.keys(countriesData.country).sort().map(function (key, index) {
  return {
    label: countriesData.country[key],
    value: countriesData.country[key]
  }
});

let fixedLanguages = Object.keys(languagesData).sort((a, b) => (a.name > b.name) ? -1 : 1).map(function (key, index) {
  return {
    id: key,
    label: languagesData[key].name,
    value: key
    // nativeName: languagesData[key].nativeName
  }
});
export default function SignUpMentorAboutYouScreen({ navigation }) {
  // static navigationOptions = {
  //   title: 'Setting up your profile',
  //   fontFamily: 'Arial',
  //   fontSize: 28,
  //   color: '#283078',
  // };

  const savedUser = useStoreState((state) => state.profile.profile)
  const parsedUser = JSON.parse(savedUser)

  const parsedLang = parsedUser?.languages ? parsedUser?.languages : []
  const _parsedLang = parsedLang.map((lang) => lang.id)

  const _languageText = parsedLang.reduce((str, lang, index) => {
    let text = "";
    if (index === parsedLang.length - 1) {
      text += `${str} ${lang.label}`;
    } else if (index === 0) {
      text += `${str}${lang.label},`
    } else {
      text += `${str} ${lang.label},`
    }
    return text
  }, "")

  const [userData, setUserData] = useState({})
  const multiSelectRef = useRef(null)
  const scrollRef = useRef(null);
  const dropdownRef = useRef(null);
  const languagedropdownRef = useRef(null);
  const [show, setShow] = useState(false)
  const [isDateDirty, setIsDateDirty] = useState(false)
  const isDateAvailable = parsedUser?.birthday ? moment(parsedUser.birthday).toDate() : new Date(moment().subtract(18, 'years'))
  const [date, setDate] = useState(isDateAvailable)
  const [selectedRegion, setSelectedRegion] = useState([])
  const [KeyboardVisible, setKeyboardVisible] = useState(false)
  const [isDropdownPickerVisible, setIsDropdownPickerVisible] = useState(false);
  const [isLanguagePickerVisible, setIsLanguagePickerVisible] = useState(false);
  const [countriesList, setCountriesList] = useState(countriesNew);
  const [languages, setLanguages] = useState(fixedLanguages)
  const [selectedLanguage, setSelectedLanguages] = useState(_parsedLang)
  const [multiText, setMultiText] = useState(_languageText)
  const [teachingAt, setTeachingAt] = useState("")
  const [numOfLines, setNumberOfLines] = useState(1)
  const [bioHeight, setBioHeight] = useState(40)

  const user = auth().currentUser;
  const uid = user.uid;

  function themeStyles(darkState) {
    return {
      ErrorField: {
        fontFamily: 'Arial',
        fontSize: 15,
        marginTop: wp('2.5%'),
        color: 'red',
        textAlign: 'left',
        marginBottom: 5,
        // marginLeft: "2.5%"
      },
      columnOne: {
        flex: 7,
        width: '90%',
        marginTop: wp('5%'), //
        marginBottom: wp('5%'),
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        justifyContent: 'flex-start',
      },
      columnTwo: {
        flex: 2,
        width: '90%',
        marginLeft: wp('5%'),
        marginRight: wp('5%'),
        justifyContent: 'flex-start',
      },
      columnThree: {
        flex: 2,
        // borderWidth: 1,
        // borderColor: "green",
        paddingVertical: hp("2.5%"),
        //  marginBottom: hp('1%'),
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start',
      },
      ColumnSeparator: {
        flex: 0.2,
        flexDirection: 'row',
        width: '100%',
        marginTop: 10,
      },
      ColumnItems: {
        flex: 0.5,
        flexDirection: 'column',
        width: '45%',
        marginBottom: 0,
      },
      Spacer: {
        marginBottom: 50,
      },
      TextField: {
        ...fontSizes.textPlain,
        color: '#283078',
        textAlign: 'left',
        marginTop: '1%',
        paddingLeft: 5

        // marginRight: '5%',
      },
      TextFieldPlaceholder: {
        ...fontSizes.textPlain,
        color: 'rgba(191,191,198,1)',
        textAlign: 'left',
        marginTop: '1%',
        //  marginBottom: '1%',
        // borderBottomColor: '#BDBDBD',
        // borderBottomWidth: 2,
      },
      InputContainer: {
        borderBottomColor: '#BDBDBD',
        borderBottomWidth: 2,
        marginTop: '3%'
      }
    };
  }
  useEffect(() => {
    // firestore()
    //   .collection("languages")
    //   // .limit(5)
    //   .get()
    //   .then(querySnapshot => {
    //     if (querySnapshot.empty) {
    //       return
    //     }
    //     let data = querySnapshot.docs.map(ref => ({
    //       id: ref.id,
    //       label: ref.data().name,
    //       ...ref.data(),
    //     }))
    //     const sortedData = data.sort((a, b) => (a.label > b.label) ? 1 : -1)
    //     console.log(sortedData)
    //     setLanguages(sortedData)
    //   })
    //   .catch(error => {
    //     console.log("Error getting documents: ", error);
    //     console.log("Use effect initiated error")

    //   })

    //  
  }, [])
  useEffect(() => {
    try {
      if (parsedUser?.signupDone) {
        updateUser(parsedUser)
        //         const user = firebase.auth().currentUser
        // const uid = user.uid

        // const usersRef = firebase.firestore().collection('users')
        // usersRef
        //   .doc(uid)
        //   .get()
        //   .then(doc => {
        //     if (doc.data().signupDone) {
        //       const fixedDate = doc.data().birthday.toDate()
        //       const user = doc.data()
        //       updateUser(fixedDate, user)
        //     } else {
        //       console.log("not done")
        //     }

        //   }).catch((e) => {
        //     console.log("error", e)
        //   })
        //   } else {
        //     console.log("not done")
        //   }
      } else {
        if (parsedUser?.birthday) {
          setIsDateDirty(true)
        }
      }
    } catch (e) {

    }

  }, [])

  // useEffect(() => {
  //   const keyboardDidShowListener = Keyboard.addListener(
  //     'keyboardDidShow',
  //     () => {
  //       setKeyboardVisible(true); // or some other action
  //       console.log("Hellow Show")
  //       //	scrollRef.current.scrollToEnd({ animated: true })
  //       setTimeout(() => {
  //         //  scrollRef.current.scrollToEnd({ animated: true })
  //         scrollRef.current.scrollTo({ x: 0, y: 0, animated: false });
  //         //this.scrollView.scrollTo({ x: DEVICE_WIDTH * current_index, y: 0, animated: false });
  //       }, 0.5)
  //     }
  //   );
  //   const keyboardDidHideListener = Keyboard.addListener(
  //     'keyboardDidHide',
  //     () => {
  //       setKeyboardVisible(false); // or some other action
  //       console.log("Hellow Hide")
  //     }
  //   );

  //   return () => {
  //     keyboardDidHideListener.remove();
  //     keyboardDidShowListener.remove();
  //   };
  // })
  const handleCountriesPickerClose = () => {
    setIsDropdownPickerVisible(false)
  }

  const handleCountriesPickerOpen = () => {
    setIsDropdownPickerVisible(true)
    setIsLanguagePickerVisible(false)
    setShow(false)
  }

  const handleLanguagePickerClose = () => {
    setIsLanguagePickerVisible(false)
  }

  const handleLanguagePickerOpen = () => {
    setIsLanguagePickerVisible(true)
    setIsDropdownPickerVisible(false)
    setShow(false)
  }

  const _scrollToInput = (e) => {
    scrollRef.current.scrollToFocusedInput(e)
  }

  const updateUser = (user) => {
    //fix to get on 

    setIsDateDirty(true)
    setUserData(user)
  }

  const showDatepicker = () => {
    setIsDropdownPickerVisible(false)
    setIsLanguagePickerVisible(false)
    setShow(true)
  }
  const hideDatepicker = () => {
    setShow(false)
  }

  const onConfirmDate = (date) => {
    // console.warn("A date has been picked: ", date)
    hideDatepicker()
    setDate(date)
    setIsDateDirty(true)

  }
  const onSelectedItemsChange = selectedItems => {
    setSelectedRegion(selectedItems)
  };
  const closeMultiSelectIfOpened = () => {
    if (multiSelectRef.current.state.selector) {
      multiSelectRef.current._toggleSelector();
    }
  }


  const onRegister = (values) => {


    const tempLang = languages.filter(lang => selectedLanguage.includes(lang.value));

    const addressData = {
      country: values.country,
      state: values.state,
      city: values.city,
      postalCode: values.postalCode,
      streetAddress: values.streetAddress,
    }
    const teachingData = {
      teachingAt: values.teachingAt,
    }
    const data = {
      fullName: values.fullName,
      bio: values.bio,
      languages: tempLang,
      birthday: firestore.Timestamp.fromDate(date),
      addressData,
      teachingData
    };

    const dataToPass = {
      fullName: values.fullName,
      birthday: moment(date).toString(),
      addressData
    }
    const usersRef = firestore().collection('users');

    usersRef
      .doc(uid)
      .set(data, { merge: true })
      .then(() => {
        user.updateProfile({
          displayName: values.fullName,
          //    photoURL:
        })
          .then(() => {
            navigation.navigate('SignUpPhoto', { dataToPass })
            // if (isYaziUser()) {
            //   navigation.navigate('SignUpPhoto', { dataToPass })
            // } else {
            //   navigation.navigate('SignUpSubjectToTeachScreen')
            // }
          })
          .catch((error) => {
            alert("update display name ", error)
          })
      })
      .catch((error) => {
        alert(error);
      });
  }

  const createLanguages = async () => {
    await Promise.all(fixedLanguages.map(async (language) =>
      await firestore().collection('languages').doc().set(
        {
          name: language.name,
          nativeName: language.nativeName,
          value: language.value
        }
      ).then(() => console.log("success"))
    ))
  }

  const closeReferences = () => {
    dropdownRef.current.close()
    languagedropdownRef.current.close()
  }
  const handleMultipleText = (selected) => {
    const tempLang = languages.filter(lang => selected.includes(lang.value));

    const languageText = tempLang.reduce((str, lang, index) => {
      let text = ""
      if (index === parsedLang.length - 1) {
        text += `${str} ${lang.label}`;
      } else if (index === 0) {
        text += `${str}${lang.label},`
      } else {
        text += `${str} ${lang.label},`
      }
      return text
    }, "")

    setMultiText(languageText)
  }

  const handleContentSize = (height) => {
    const newHeight = height / 20
    const _numOfLines = Math.floor(newHeight)
    const _bioHeight = Math.max(35, height)
    setNumberOfLines(_numOfLines)
    setBioHeight(_bioHeight)

  }

  return (
    <ThemeContextConsumer>
      {context => (
        <SafeAreaView style={container}>
          <KeyboardAwareScrollView
            innerRef={ref => { scrollRef.current = ref; }}
            contentContainerStyle={{ flexGrow: 1 }}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            onScroll={() => dropdownRef.current.close()}
          >
            <Formik
              initialValues={{
                fullName: parsedUser?.fullName,
                birth: date,
                bio: parsedUser?.bio,
                streetAddress: parsedUser?.addressData?.streetAddress,
                teachingAt: parsedUser?.teachingData?.teachingAt,
                languages: selectedLanguage,
                country: parsedUser?.addressData?.country,
                state: parsedUser?.addressData?.state,
                city: parsedUser?.addressData?.city,
                postalCode: parsedUser?.addressData?.postalCode,
              }}
              validationSchema={Yup.object().shape({
                fullName: Yup.string().required('Your name is required'),
                birth: Yup.date('Please enter a valid date').required('Your birthday is required'),
                bio: Yup.string().nullable(true),
                streetAddress: Yup.string().nullable(true),//.required('Street Address is required'),
                teachingAt: Yup.string().nullable(true),
                country: Yup.string().required('Country is required'),
                state: Yup.string().nullable(true),//.required('State is required'),
                city: Yup.string().required('City is required'),
                postalCode: Yup.string().required('Postal Code is required'),
              })}
              // onSubmit={values => console.log(values.streetAddress)}
              onSubmit={(values) => onRegister(values)}>
              {({ errors, touched, values, handleChange, handleBlur, handleSubmit, setFieldValue }) => (
                <View style={{ flex: 1 }}>
                  {/* <View style={{ position: "absolute", height: 500, width: "100%", backgroundColor: "black" }}>
                  <MultiSelect
                    // hideTags
                    items={sortedCountries}
                    uniqueKey="id"
                    ref={multiSelectRef}
                    fixedHeight={true}
                    onSelectedItemsChange={onSelectedItemsChange}
                    icon
                    single
                    selectedItems={selectedRegion}
                    selectText="Countries"
                    searchInputPlaceholderText="Search Countries"
                    onChangeInput={(text) => console.log(text)}
                    altFontFamily="Arial"
                    tagRemoveIconColor="#FF3CA5"
                    tagBorderColor={theme.light.text}
                    tagTextColor={theme.light.text}
                    tagFontSize={5}
                    selectedItemTextColor="#CCC"
                    selectedItemIconColor={theme.light.text}
                    itemTextColor={theme.light.text}
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor={theme.light.text}
                    submitButtonText="Submit"
                    styleDropdownMenuSubsection={{}}
                    flatListProps={{ nestedScrollEnabled: true }}
                  />
                </View> */}
                  <View style={themeStyles(context.theme).columnOne}>
                    {isYaziUser() ? <>
                      <Text style={{ ...fontSizes.heading6, color: context.theme ? theme.dark.text : theme.light.text, marginBottom: wp('2.5%'), }}>2 of 3</Text>
                      <Text style={{ ...fontSizes.heading3, color: context.theme ? theme.dark.text : theme.light.text, marginBottom: wp('2.5%') }}>Tell us about you</Text>
                      <Text style={{ ...fontSizes.textPlain, color: context.theme ? theme.dark.text : theme.light.text, lineHeight: wp('5.5%') }}>
                        Write a few words about yourself, so your mentors and tutors can know you more
						</Text>
                    </>
                      : <>
                        <Text
                          style={{
                            ...fontSizes.heading6,
                            color: theme.light.text,
                            marginBottom: wp('2.5%'),
                          }}>
                          1 of 7
                </Text>
                        <Text
                          style={{
                            ...fontSizes.heading3,
                            color: theme.light.text
                          }}>
                          Please tell us a little bit about yourself
                </Text>
                      </>}
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '5%' }}>Full name</Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                        placeholder={"John Doe"}
                        onChangeText={handleChange('fullName')}
                        onFocus={closeReferences}
                        onBlur={handleBlur('fullName')}
                        value={values.fullName}
                        style={themeStyles(context.theme).TextField}
                        autoCorrect={false}
                      //      ref={fullNameRef}
                      //   onSubmitEditing={fullNameKeypress}
                      // onChangeText={(text) => setFullName(text)}
                      />

                    </View>
                    {errors.fullName && touched.fullName ? <Text style={themeStyles(context.theme).ErrorField}>{errors.fullName}</Text> : null}
                    <TouchableOpacity onPress={() => showDatepicker()} style={themeStyles(context.theme).InputContainer} >
                      <Text
                        style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                        Birthday
                </Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                        placeholder={moment(values.birth).format("MMMM DD, YYYY").toString()}
                        onFocus={showDatepicker}
                        value={moment(values.birth).format("MMMM DD, YYYY").toString()}
                        //      onChangeText={handleChange('birth')}
                        //  onBlur={hideDatepicker} // TODO: omitted - not building on Xcode 11.3
                        style={isDateDirty ? themeStyles(context.theme).TextField : themeStyles(context.theme).TextFieldPlaceholder}
                        //onChangeText={(text) => setBirth(text)} // TODO: brought back - not building on Xcode 11.3
                        autoCapitalize="none"
                        //onPress={showDatepicker} // TODO: omitted - not building on Xcode 11.3
                        //showSoftInputOnFocus={false}
                        // ref={bdayRef}
                        pointerEvents="none"
                        editable={false}
                      />
                    </TouchableOpacity>
                    {errors.birth && touched.birth ? <Text style={themeStyles(context.theme).ErrorField}>{errors.birth}</Text> : null}

                    <DateTimePickerModal
                      isVisible={show}
                      mode="date"
                      headerTextIOS="Birthdate"
                      date={date}
                      onConfirm={(date) => { setFieldValue('birth', date); onConfirmDate(date); }}
                      onCancel={hideDatepicker}
                      minimumDate={new Date(moment().subtract(118, 'years'))}
                      maximumDate={new Date(moment().subtract(18, 'years'))}

                    />
                    <View style={[themeStyles(context.theme).InputContainer]}>
                      <Text
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                        }}>
                        Bio
                </Text>
                      <TextInput
                        placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                        placeholder='Write a few words here...'
                        style={{ height: Platform.OS === 'ios' ? hp("12%") : bioHeight, ...themeStyles(context.theme).TextField }}
                        //  maxLength={101}
                        multiline={true}
                        scrollEnabled
                        value={values.bio}
                        onChangeText={handleChange('bio')}
                        onContentSizeChange={(e) => handleContentSize(e.nativeEvent.contentSize.height)}
                        onFocus={closeReferences}
                        numberOfLines={numOfLines}
                        textAlignVertical={'top'}
                        autoCorrect={false}
                      />
                    </View>
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '5%' }}>Teaching At</Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                        placeholder={"Yazi Academy"}
                        onChangeText={handleChange('teachingAt')}
                        onFocus={closeReferences}
                        onBlur={handleBlur('teachingAt')}
                        value={values.teachingAt}
                        style={themeStyles(context.theme).TextField}
                        autoCorrect={false}
                      //      ref={fullNameRef}
                      //   onSubmitEditing={fullNameKeypress}
                      // onChangeText={(text) => setFullName(text)}
                      />

                    </View>
                    {errors.teachingAt && touched.teachingAt ? <Text style={themeStyles(context.theme).ErrorField}>{errors.teachingAt}</Text> : null}
                    <View style={[themeStyles(context.theme).InputContainer, { zIndex: 4000, paddingBottom: "2%" }]}>
                      <Text onPress={handleLanguagePickerOpen}
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                          marginBottom: "2%"
                        }}>
                        Language
                    </Text>
                      <DropDownPicker
                        arrowStyle={{ display: 'none' }}
                        controller={instance => languagedropdownRef.current = instance}
                        isVisible={isLanguagePickerVisible}
                        style={{ display: !isLanguagePickerVisible ? 'flex' : "none", borderWidth: 0, paddingLeft: 0 }}
                        containerStyle={{ display: "flex" }}
                        dropDownStyle={{ position: 'relative' }}
                        dropDownMaxHeight={hp("40%")}
                        placeholder="Select a Language"
                        searchable={true}
                        searchablePlaceholder="Search for spoken Language"
                        searchablePlaceholderTextColor="gray"
                        labelStyle={{
                          color: '#283078',
                          fontSize: 16,
                          textAlign: "left",

                        }}
                        multiple={true}
                        multipleText={selectedLanguage.length > 0 ? multiText : "Select a Language"}
                        seachableStyle={{}}
                        searchableError={() => <Text>Not Found</Text>}
                        onOpen={handleLanguagePickerOpen}
                        onClose={handleLanguagePickerClose}
                        items={languages}
                        defaultValue={values.languages}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        activeLabelStyle={{
                          fontWeight: "bold", fontSize: 17
                        }}
                        onChangeList={(items, callback) => {
                          new Promise((resolve, reject) => resolve(setLanguages(items)))
                            .then(() => callback())
                            .catch(() => { });
                        }}
                        onChangeItem={(item) => {
                          setSelectedLanguages(item);
                          handleMultipleText(item);
                          // setFieldValue("languages", item)
                        }
                        }
                      />
                    </View>

                    <View style={[themeStyles(context.theme).InputContainer, { zIndex: 4000, paddingBottom: "2%" }]}>
                      <Text onPress={handleCountriesPickerOpen}
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                          marginBottom: "2%"
                        }}>
                        Country
                    </Text>
                      <DropDownPicker
                        arrowStyle={{ display: 'none' }}
                        controller={instance => dropdownRef.current = instance}
                        isVisible={isDropdownPickerVisible}
                        style={{ display: !isDropdownPickerVisible ? 'flex' : "none", borderWidth: 0, paddingLeft: 0 }}
                        containerStyle={{ display: "flex" }}
                        dropDownStyle={{ position: 'relative' }}
                        dropDownMaxHeight={hp("40%")}
                        placeholder="Select a Country / Region"
                        searchable={true}
                        searchablePlaceholder="Search for a Country / Region"
                        searchablePlaceholderTextColor="gray"
                        labelStyle={{
                          color: '#283078',
                          fontSize: 16,
                          textAlign: "left",

                        }}
                        seachableStyle={{}}
                        searchableError={() => <Text>Not Found</Text>}
                        onOpen={handleCountriesPickerOpen}
                        onClose={handleCountriesPickerClose}
                        items={countriesList}
                        defaultValue={values.country}
                        itemStyle={{ justifyContent: 'flex-start' }}
                        activeLabelStyle={{
                          fontWeight: "bold", fontSize: 17
                        }}
                        onChangeList={(countriesList, callback) => {
                          new Promise((resolve, reject) => resolve(setCountriesList(countriesList)))
                            .then(() => callback())
                            .catch(() => { });
                        }}
                        //      defaultValue={frequencyValue}
                        onChangeItem={(item) => { setFieldValue("country", item.value) }}

                      />
                    </View>
                    {errors.country && touched.country ? (
                      <Text style={themeStyles(context.theme).ErrorField}>
                        {errors.country}
                      </Text>
                    ) : null}
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                        }}>
                        Street Address
                </Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        onChangeText={handleChange('streetAddress')}
                        onBlur={handleBlur('streetAddress')}
                        onFocus={closeReferences}
                        value={values.streetAddress}
                        placeholder="Some Address"
                        style={themeStyles(context.theme).TextField}
                      />
                    </View>
                    {errors.streetAddress && touched.streetAddress ? (
                      <Text style={themeStyles(context.theme).ErrorField}>
                        {errors.streetAddress}
                      </Text>
                    ) : null}
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                        }}>
                        State
                    </Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        onChangeText={handleChange('state')}
                        onBlur={handleBlur('state')}
                        onFocus={closeReferences}
                        value={values.state}
                        placeholder="State"
                        style={themeStyles(context.theme).TextField}
                      />
                    </View>
                    {errors.state && touched.state ? (
                      <Text style={themeStyles(context.theme).ErrorField}>
                        {errors.state}
                      </Text>
                    ) : null}
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                        }}>
                        City
                    </Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        onChangeText={handleChange('city')}
                        onBlur={handleBlur('city')}
                        onFocus={closeReferences}
                        value={values.city}
                        placeholder="City"
                        style={themeStyles(context.theme).TextField}
                      />
                    </View>
                    {errors.city && touched.city ? (
                      <Text style={themeStyles(context.theme).ErrorField}>
                        {errors.city}
                      </Text>
                    ) : null}
                    <View style={themeStyles(context.theme).InputContainer}>
                      <Text
                        style={{
                          ...fontSizes.heading7,
                          color: theme.light.text,
                          marginTop: wp('5%'),
                        }}>
                        Postal Code
                    </Text>
                      <TextInput
                        paddingVertical={textInputPaddingVerticalWrapper()}
                        onChangeText={handleChange('postalCode')}
                        onBlur={handleBlur('postalCode')}
                        onFocus={closeReferences}
                        value={values.postalCode}
                        placeholder="1600"
                        style={themeStyles(context.theme).TextField}
                      />
                    </View>
                    {errors.postalCode && touched.postalCode ? (
                      <Text style={themeStyles(context.theme).ErrorField}>
                        {errors.postalCode}
                      </Text>
                    ) : null}

                    {/* <TextInput
                      paddingVertical={textInputPaddingVerticalWrapper()}
                      onChangeText={handleChange('country')}
                      onBlur={handleBlur('country')}
                      value={values.country}
                      placeholder="Country"
                      style={themeStyles(context.theme).TextField}
                    /> */}

                    {/* <TouchableOpacity onPress={() => multiSelectRef?.current?._toggleSelector()} >
                        <Text
                          style={{ ...fontSizes.heading7, color: context.theme ? theme.dark.text : theme.light.text, marginTop: '8%' }}>
                          Country
                </Text>
                        <TextInput
                          paddingVertical={textInputPaddingVerticalWrapper()}
                          placeholderTextColor={context.theme ? theme.dark.inactiveText : theme.light.inactiveText}
                          placeholder={moment(values.birth).format("MMMM DD, YYYY").toString()}
                          value={moment(date).format("MMMM DD, YYYY").toString()}
                          onChangeText={handleChange('birth')}
                          //  onBlur={hideDatepicker} // TODO: omitted - not building on Xcode 11.3
                          style={isDateDirty ? themeStyles(context.theme).TextField : themeStyles(context.theme).TextFieldPlaceholder}
                          //onChangeText={(text) => setBirth(text)} // TODO: brought back - not building on Xcode 11.3
                          autoCapitalize="none"
                          //onPress={showDatepicker} // TODO: omitted - not building on Xcode 11.3
                          //showSoftInputOnFocus={false}
                          // ref={bdayRef}
                          pointerEvents="none"
                          editable={false}
                        />
                      </TouchableOpacity> */}

                    {/*
                      <TouchableWithoutFeedback onPress={closeMultiSelectIfOpened}>



                      </TouchableWithoutFeedback> */}




                  </View>
                  <View style={themeStyles(context.theme).columnThree}>
                    <ButtonBlue
                      onPress={handleSubmit}>
                      Next
                </ButtonBlue>
                    <Br howMany={1} />
                    <Text onPress={() => isYaziUser() ? navigation.navigate('SignUpPhoto') : navigation.navigate('SignUpPhoto')} style={{ ...fontSizes.heading7, color: '#283078' }}>
                      Skip for now </Text>
                  </View>

                  {/* <Text style={styles.Label}>Full name</Text>
					<TextInput placeholder='John Doe' style={styles.TextField}/> */}
                </View>
              )}
            </Formik>
          </KeyboardAwareScrollView>
        </SafeAreaView>
      )
      }
    </ThemeContextConsumer >
  );
}
