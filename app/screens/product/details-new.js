import React, {
  useState,
  useEffect,
} from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { ScrollView, Text, TouchableOpacity, View, Dimensions, AlertIOS, StatusBar, Alert, Linking, Clipboard, Image, Platform, Modal, TouchableHighlight, TouchableWithoutFeedback, StyleSheet, } from 'react-native';
import { Header, Left, Body, Right, Thumbnail, Container, Footer, FooterTab } from 'native-base';
import { Col, Row } from 'react-native-easy-grid';
import Swiper from 'react-native-swiper';
import StarRating from 'react-native-star-rating';
import ImageZoom from 'react-native-image-pan-zoom';
import * as Animatable from 'react-native-animatable';
import FlatListComponent from '../../components/flatlist';
import RenderItem from '../../components/renderItem';
import styles from './styles2';
import FastImage from 'react-native-fast-image';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS, alterNode, makeTableRenderer, defaultTableStylesSpecs, cssRulesFromSpecs } from 'react-native-render-html-table-bridge';
import WebView from 'react-native-webview';
import Endpoints from '../../components/api/endpoints';
import ImageViewer from 'react-native-image-zoom-viewer';
import QRCode from 'react-native-qrcode-svg';

import auth from '@react-native-firebase/auth';
import { getFontScaleSync } from 'react-native-device-info';

const api_token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
const product_api = 'https://www.referapps.com/api/product/show.json?id=';
const pin_item_api = 'https://www.referapps.com/api/pin/create.json';
const unpin_item_api = 'https://www.referapps.com/api/pin/delete.json';
const user_products_api = 'https://www.referapps.com/api/product/user-products.json?';
const pinned_items_api = 'https://www.referapps.com/api/pin/all.json?';
const refer_api = 'https://www.referapps.com/api/product/refer.json';
const reviews_api = 'https://www.referapps.com/api/product/reviews.json?include=info,date';
const add_to_cart = 'https://www.referapps.com/api/cart/add.json';
const product_inventory = 'https://www.referapps.com/api/product/price-inventory.json';
const checkout_api = 'https://www.referapps.com/api/paypal/checkout.json?include=date,info';

const imgSample = require('../../assets/robot-dev.png')
const profile = require('../../assets/face0.jpg')

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const config = {
  WebViewComponent: WebView
};

// const renderers = {
//   table: makeTableRenderer(config)
// };

const cssRules = `
table {
  width: 100%;
  background-color: #FFF;
}
`
const htmlConfig = {
  alterNode,
  // renderers,
  ignoredTags: IGNORED_TAGS,
  cssRules
};

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default function ProductScreen({ navigation, route }) {
  const [product_id, setProduct_id] = useState(null);
  const [loggedIn, setLoggedIn] = useState(false);
  const [warranty, setWarranty] = useState('');
  const [des, setDes] = useState(' ');
  const [user_img, setUser_img] = useState('');
  const [shp_name, setShp_name] = useState('');
  const [shop, setShop] = useState([]);
  const [commission, setCommission] = useState(0);
  const [productLoaded, setProductLoaded] = useState(true);
  const [pinnedLoaded, setPinnedLoaded] = useState(null);
  const [user_productsLoaded, setUser_productsLoaded] = useState(null);
  const [reviewsLoaded, setReviewsLoaded] = useState(null);
  const [product, setProduct] = useState([]);
  const [user_products, setUser_products] = useState([]);
  const [product_name, setProduct_name] = useState('');
  const [product_images, setProduct_images] = useState([]);
  const [reviews, setReviews] = useState([]);
  const [refer_url, setRefer_url] = useState('');
  const [imageModal, setImageModal] = useState(false);
  const [referModal, setReferModal] = useState(false);
  const [specificationsModal, setSpecificationsModal] = useState(false);
  const [addToCartModal, setAddToCartModal] = useState(false);
  const [modal_image, setModal_image] = useState('');
  const [already_pinned, setAlready_pinned] = useState(false);
  const [componentHeight, setComponentHeight] = useState('');
  const [user_id, setUser_id] = useState('');
  const [pin_id, setPin_id] = useState(0);
  const [shop_id, setShop_id] = useState(null);
  const [deliveryFeeLowest, setDeliveryFeeLowest] = useState(0);
  const [deliveryFeeHighest, setDeliveryFeeHighest] = useState(0);

  const [variation1_selected, setVariation1_selected] = useState(null);
  const [variation2_selected, setVariation2_selected] = useState(null);
  const [quantity, setQuantity] = useState(0);
  const [stock, setStock] = useState(0);
  const [fetchingStock, setFetchingStock] = useState(false);
  const [price, setPrice] = useState(null);
  const [delivery_courier_slug, setDelivery_courier_slug] = useState('');
  const [delivery_courier_fee, setDelivery_courier_fee] = useState('');
  const [isSinglePurchase, setIsSinglePurchase] = useState(false);

  const [modalVisible, setModalVisible] = useState(false);
  const [referVisible, setReferVisible] = useState(false);
  const [userData, setUserData] = useState({});
  const [productDetail, setProductDetail] = useState([]);
  const [previewImages, setPreviewImages] = useState([]);

  useEffect(() => {
    async function fetchUser() {
      let ud = await AsyncStorage.getItem("user_data")
      if (ud == null) {
        return
      }
      let user = typeof ud == 'string' ? JSON.parse(ud) : null
      setUserData(user);
      return user;
    };

    fetchUser().then((user) => {
      init(user);
    });

    // return async () => {
    //   await AsyncStorage.removeItem("app_refer_state");
    //   Linking.removeEventListener('url', handleOpenURL);
    // }
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;

      async function reload() {
        if (userData?.data != null) {
          console.log("useFocusEffect userData not null")
          try {
            if (isActive) {
              await referItemFetch();
              await checkPinnedItems();
              await reviewsFetch();
            }
          } catch (e) {
            // Handle error
          }
        }
      };

      reload();

      return () => {
        isActive = false;
      };
    }, [])
  );

  const init = async (user) => {
    // console.log('Params', JSON.stringify(route.params, null, '\t'));
    // let token = await AsyncStorage.getItem("auth")  

    // console.log("init user", user)
    if (user == null) {
      setLoggedIn(false);
    } else {
      setLoggedIn(true);
    }

    await productFetch();
    await reviewsFetch();

    if (user.data.id && user.data.id !== 0) {
      await referItemFetch(user);
      await checkPinnedItems(user);
    }
  }

  const _renderItem = ({ item }) => {
    // console.warn(item)
    return (
      <RenderItem
        onPress={() => {
          navigation.push('ProductDetails', { product_id: item.id })
        }}
        uri={item.info.data.image.full_path}
        id={item.id}
        product_name={item.product_name}
        country_code={item.country_code}
        price={item.price}
        ratings={item.ratings}
      />
    );
  }

  const productFetch = async () => {
    try {
      let formData = new FormData();
      formData.append('id', route.params.product_id);
      formData.append('include', 'info');
      formData.append('api_token', api_token);
      const response = await fetch(product_api + route.params.product_id + '&include=info,date&api_token=' + api_token);
      const responseJson = await response.json();

      // console.log("route.params.product_id", route.params.product_id)
      // console.log("responseJson", responseJson)
      // console.log("responseJson.data.info.data.description", responseJson.data.info.data.description)

      // console.log("responseJson.data.info.data", responseJson.data.info.data)
      // console.log("product.info.data", product.info.data)

      setProduct_id(route.params.product_id);

      setProduct(responseJson.data)
      setProduct_name(responseJson.data.product_name)
      setProduct_images(responseJson.data.info.data.product_images)
      // console.log("responseJson.data.info.data.product_images", responseJson.data.info.data.product_images)
      setUser_id(responseJson.data.user_id)
      setCommission(responseJson.data.referrer_commission)
      setDes(responseJson.data.info.data.description)
      setWarranty(responseJson.data.info.data.warranty_details)
      setUser_img(responseJson.data.info.data.shop_info.shop_image)
      setShp_name(responseJson.data.info.data.shop_info.shop_name)
      setShop_id(responseJson.data.info.data.user_id)
      setShop(responseJson.data.info.data.shop_info)
      setProductLoaded(true)
      setDeliveryFeeLowest(parseFloat(responseJson.data.info.data.shipping_info.delivery_options[0].delivery_option_info.fee))
      setDeliveryFeeHighest(parseFloat(responseJson.data.info.data.shipping_info.delivery_options[0].delivery_option_info.fee))

      userProductsFetch();

      var delivery_options = responseJson.data.info.data.shipping_info.delivery_options;
      let tmpLowest = parseFloat(delivery_options[0].delivery_option_info.fee);
      let tmpHighest = parseFloat(delivery_options[0].delivery_option_info.fee);

      delivery_options.map((deliveryOption) => {
        if (deliveryOption.delivery_option_info.fee > tmpHighest) {
          tmpHighest = parseFloat(deliveryOption.delivery_option_info.fee);
        }
        // console.log("deliveryOption.delivery_option_info.fee < tmpLowest", deliveryOption.delivery_option_info.fee < tmpLowest, " || deliveryOption.delivery_option_info.fee", deliveryOption.delivery_option_info.fee, " || tmpLowest", tmpLowest)
        if (deliveryOption.delivery_option_info.fee < tmpLowest) {
          tmpLowest = parseFloat(deliveryOption.delivery_option_info.fee);
        }
      })
      // console.log(tmpLowest, " - ", tmpHighest);

      setDeliveryFeeLowest(tmpLowest)
      setDeliveryFeeHighest(tmpHighest)

      // console.log(deliveryFeeLowest, " - ", deliveryFeeHighest);
      // console.warn('product data', product.info.data.shipping_info.delivery_options[0].delivery_option_name)
    }
    catch (e) {
      console.warn("productFetch", e);
    }
  }

  const reviewsFetch = async () => {
    // data.info.data.user_info.avatar
    // console.warn('prodid', route.params.product_id)
    // alert(JSON.stringify(product.id));
    try {
      let formData = new FormData();
      formData.append('product_id', product.id);
      formData.append('api_token', api_token);
      const data = await fetch(reviews_api, {
        method: 'POST',
        body: formData
      });
      const response = await data.json();
      // console.log('Reviews', JSON.stringify(response, null, '\t'));
      setReviews(response.data)
      setReviewsLoaded(response.status)
    }
    catch (e) {
      console.warn("reviewsFetch", e);
    }

  }

  const userProductsFetch = async () => {
    try {
      const response = await fetch(user_products_api + "user_id=" + product.user_id + "&include=info&page=1&per_page=7&api_token=" + api_token);
      const responseJson = await response.json();

      setUser_products(responseJson.data)
      setUser_productsLoaded(true)
      // console.log("user products", responseJson.data)
    }
    catch (e) {
      console.warn("userProductsFetch", e);
    }
  }

  const _renderReviews = () => {
    // alert(JSON.stringify(data.info.data.user_info.avatar))
    // alert(JSON.stringify(reviews))
    if (reviews.length > 0) {
      return reviews.map((data) => {
        return (
          <View>
            <View style={styles.rrCon}>
              <View style={{ alignSelf: 'flex-start', }}>
                {_renderReviewerAvatar(data.info.data.user_info.avatar)}
              </View>
              <View style={styles.nameCon}>
                <Text style={styles.txtName}>{data.info.data.user_info.name}</Text>
                <Col style={{ width: '25%', paddingTop: 2 }}>
                  <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor={'#f36e23'}
                    halfStarColor={'#f36e23'}
                    starSize={11}
                    style={{ justifyContent: 'flex-end' }}
                  />
                </Col>
                <Col style={styles.col}>
                  <Text style={styles.txtDesciption}>{data.description}</Text>
                </Col>
              </View>
            </View>
          </View>
        );
      });
    }
    else {
      return (
        <View>
          <Thumbnail
            source={require('../../assets/internal/4.png')}
            square
            style={styles.img4}
          />
        </View>
      );
    }
  }

  const _renderReviewerAvatar = (avatar) => {
    const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
    const uri = avatar;
    return (
      <FastImage
        style={styles.fastImg}
        tint='light'
        // resizeMode='contain'
        // {...{preview, uri}}
        source={{ uri: avatar }}
      />
    );
  }

  const renderHeader = () => {
    return (
      <View style={styles.headerCon}>
        <Swiper
          showsButtons={true}
          autoplay={true}
          loop={true}
          autoplayTimeout={2}
          autoplayDirection={true}
          automaticallyAdjustContentInsets={true}
          height={300}
          showsPagination={false}
        >
          {imageSliderList()}
        </Swiper>
        <View style={styles.con2}>
          <Text style={styles.productText}>{product.product_name}</Text>
          <View style={styles.space} />
          {product?.info?.data?.with_discount === "yes" ?
            <Text style={styles.productPrice}>
              <Text style={styles.productPriceWithDiscount}>
                {product.country_code === 'PH' && '₱'}
                {product.country_code === 'US' && '$'}
                {" "}{(parseFloat(product?.info?.data?.price) + parseFloat(product?.info?.data?.discount_amount)).toFixed(2)}
              </Text>
              {" "}{product.country_code === 'PH' && '₱'}
              {product.country_code === 'US' && '$'}
              {" "}{product.price}{" "}{" "}
              <View style={styles.space}>
                <Text style={styles.discountPercentage}>
                  {"-"}{parseFloat(product?.info?.data?.discount_percentage).toFixed(2)}{"%"}
                </Text>
              </View>
            </Text>
            :
            <Text style={styles.productPrice}>
              {product.country_code === 'PH' && '₱'}
              {product.country_code === 'US' && '$'}
              {" "}{product.price}
            </Text>}
          <View style={styles.space} />
          <View style={styles.starCon}>
            {product.ratings >= 0 ? <StarRating
              disabled={true}
              emptyStar={'ios-star-outline'}
              fullStar={'ios-star'}
              halfStar={'ios-star-half'}
              iconSet={'Ionicons'}
              maxStars={5}
              rating={product.ratings}
              fullStarColor={'#f36e23'}
              halfStarColor={'#f36e23'}
              starSize={11}
            /> :
              <Text numberOfLines={1} style={styles.txtRatings}>
                ({product.ratings})
              </Text>}
            {product.ratings >= 0 &&
              <Text style={styles.txtrate}>
                {product.ratings}/5.0
              </Text>}
          </View>
        </View>
        <View style={styles.border} />
        {/* {_renderVariants()} */}
        <View style={styles.referrCommissionCon}>
          <Text style={styles.txtReferr}>Referrer Commission
            {product?.info?.data?.commission_type === "fix" ?
              " ₱" + commission
              :
              " " + commission + "%"}

          </Text>
          <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text>
        </View>

        <View style={styles.userimgCon}>
          <View style={{ flexDirection: 'row', }}>
            <View style={styles.fastImgCon}>
              <FastImage
                style={styles.userimg}
                tint='light'
                // resizeMode='contain'
                // {...{profile}}
                source={{
                  uri:
                    user_img
                }}
              />
            </View>
            <View style={styles.shpnameCon}>
              <Text style={{ alignSelf: 'center' }}>{shp_name}</Text>
              <Text style={styles.txtVerify}>{product.is_verify === 'yes' && 'Verified Shop'}</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity onPress={() => {
              let propsData = {};
              propsData.shop_id = shop_id;
              navigation.navigate('ViewShop', propsData)
            }
            } style={styles.btnViewCon}>
              <Text style={styles.txtViewShop}>View Shop</Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity style={styles.specificationsCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start' }}>
              <Text style={styles.txtSpecifications}>Category:</Text>
            </View>
            <View style={styles.brandCon}>
              <Text style={styles.txtBrand}>{product.category}</Text>
            </View>
          </View>
        </TouchableOpacity>

        {/* <TouchableOpacity onPress={() => alert('Under Development')} style={styles.specificationsCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start' }}>
              <Text style={styles.txtSpecifications}>Specifications:</Text>
            </View>
            <View style={styles.brandCon}>
              <Text style={styles.txtBrand}>Brand, Model ...</Text>
            </View>
          </View>
          <View>
            <View style={styles.rightArrowCon}>
              <Thumbnail
                source={require('../../assets/icon_sub.png')}
                small
                square
                style={styles.imgRight}
              />
            </View>
          </View>
        </TouchableOpacity> */}

        {_renderAttributes()}

        <View style={styles.logCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start', }}>
              <Thumbnail
                source={require('../../assets/internal/icon_Logistics.png')}
                square
                style={styles.imgLog}
              />
            </View>
            <View style={styles.shipCon}>
              <Text style={styles.txtShip}>Shipping Fee</Text>
              <Text style={styles.txtNumber}>{":  ₱" + deliveryFeeLowest + " - ₱" + deliveryFeeHighest}</Text>
            </View>
            {/* <View style={styles.imgDownCon}>
              <Thumbnail
                source={require('../../assets/internal/icon_down.png')}
                square
                style={styles.imgDown}
              />
            </View> */}
          </View>
        </View>

        <View style={{ backgroundColor: '#f5f6fa', }} />
        <View style={styles.desCon}>
          <Row>
            <View style={{ paddingTop: 15, paddingHorizontal: 15 }}>
              <Text style={[styles.detailText, { fontSize: 16, paddingVertical: 5, fontWeight: 'bold' }]}>Product Details</Text>
            </View>
          </Row>
          <ScrollView>
            <View style={{
              paddingRight: 15,
              paddingLeft: 15
            }}>
              {/* <HTML html={`<iframe srcdoc="${des}"></iframe>`} /> */}
              <HTML html={des} {...htmlConfig} />
            </View>
          </ScrollView>
          <Row>
            <View style={styles.detailsCon}>
              <Text style={[styles.detailText, {
                // fontFamily: 'roboto-medium',
                fontSize: 16,
                paddingVertical: 5,
                fontWeight: 'bold'
              }]}>Warranty Details</Text>
            </View>
          </Row>
          <ScrollView>
            <View style={{
              paddingRight: 15,
              paddingLeft: 15
            }}>
              <HTML html={des} {...htmlConfig} style={{ backgroundColor: 'red' }} />
            </View>
          </ScrollView>
        </View>

        <View style={{ padding: 5, backgroundColor: '#f5f6fa', }} />

        <View style={styles.reviewsCon}>
          <Text style={styles.txtProductR}>Product Reviews ({product.number_of_reviews})</Text>
          <TouchableOpacity onPress={() => alert('Under Development')}>
            <Text style={styles.txtView}>View All</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.renderReview}>
          {reviewsLoaded && _renderReviews()}
        </View>

        {user_products.length > 0 && <View>
          <View style={styles.rrContainer}>
            <Text style={styles.txtSame}>From the same store</Text>
            <TouchableOpacity
              onPress={() => alert('Under Development')}
              style={{ justifyContent: 'space-between', }}>
              <View style={{ flex: 1 }} />
              <Text style={styles.viewAll}>View All</Text>
            </TouchableOpacity>
          </View>
        </View>}
      </View>
    );
  }

  const imageSliderList = () => {
    return product_images.map((data, i) => {
      const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
      const uri = data.directory + '/' + data.filename;
      // console.log("iamge slider uri", uri)
      return (
        <TouchableOpacity style={{ alignSelf: 'center', height: 300, width: Dimensions.get('window').width, }} onPress={() => {
          let res = []
          for (var x = 0; x < product_images.length; x++) {
            res.push({ url: product_images[x].directory + '/' + product_images[x].filename })
          }

          setImageModal(true)
          setPreviewImages(res)
        }}>
          <Image
            style={{
              height: 300,
              width: Dimensions.get('window').width,
              backgroundColor: '#ffffff',
              // resizeMode: 'contain'
            }}
            // source={{ uri: uri + "?" + new Date().getTime() }}
            source={{ uri: uri }}
          />
        </TouchableOpacity>
      );
    });
  }

  const modalImageSliderList = () => {
    return product_images.map((data, i) => {
      const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
      const uri = data.directory + '/' + data.filename;

      // console.log("modalImageSliderList uri", uri)
      return (
        <View
          style={{ alignSelf: 'center', height: 120, width: 12 }}
        >
          <Image
            style={{
              height: 125,
              width: 125,
              backgroundColor: '#fff',
              // resizeMode: 'stretch',
              marginLeft: -50,
              marginTop: 10
            }}
            // source={{ uri: uri + "?" + new Date().getTime() }}
            source={{ uri: uri }}
          />
        </View>

      );
    });
  }

  const handleSwiper = () => {
    return (
      <>
        <Swiper
          style={{ alignSelf: 'center', height: 175, alignItems: 'center' }}
          showsButtons={false}
          autoplay={true}
          loop={true}
          autoplayTimeout={5}
          autoplayDirection={true}
          automaticallyAdjustContentinsets={true}
          horizontal={true}
        // showPagination={true}
        // renderPagination={
        //   renderPage
        // }
        >
          {modalImageSliderList()}
        </Swiper>
      </>
    )
  }

  const referItemFetch = async (user) => {
    let tmpUser = null;
    if (user == null) {
      tmpUser = userData;
    } else {
      tmpUser = user
    }

    // console.log("referItemFetch tmpUser", tmpUser)
    try {
      let formData = new FormData();
      formData.append('user_id', tmpUser.data.id);
      formData.append('product_id', route.params.product_id);
      formData.append('api_token', api_token);
      const response = await fetch(refer_api, {
        method: 'POST',
        headers: { 'Content-type': 'multipart/form-data' },
        body: formData
      });
      const responseJson = await response.json();
      setRefer_url(responseJson.url)
    }
    catch (e) {
      console.warn("referItemFetch", e);
    }
  }

  const checkPinnedItems = async (user) => {
    let tmpUser = null;
    if (user == null) {
      tmpUser = userData;
    } else {
      tmpUser = user
    }
    // console.log("checkPinnedItems tmpUser", tmpUser)
    try {
      var data = await fetch(pinned_items_api + 'user_id=' + tmpUser.data.id + '&api_token=' + api_token);
      var response = await data.json();
      var validate = response.data.filter(x => x.product_id === route.params.product_id);
      if (validate.length > 0) {
        setAlready_pinned(true);
        setPin_id(validate[0].id)
      }
      else {
        setAlready_pinned(false);
      }
    }
    catch (e) {
      console.warn("checkPinnedItems", e);
    }
  }

  const pinItem = async () => {
    console.log('props', route.params.product_id)
    try {
      setPinnedLoaded(false)
      let formData = new FormData();
      formData.append('user_id', userData.data.id);
      formData.append('product_id', route.params.product_id);
      formData.append('api_token', api_token);
      var data = await fetch(pin_item_api + '?user_id=' + userData.data.id + '&include=info,date&api_token=' + api_token, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData
      });
      var response = await data.json();
      console.log('dataaa', response)

      if (response.status === true) {
        Alert.alert(
          'Success!',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                setPinnedLoaded(true)
                setAlready_pinned(true)
              },
            },
          ],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(
          'Error!',
          response.errors,
          [
            {
              text: 'OK',
              onPress: () => {
                setPinnedLoaded(true)
                setAlready_pinned(getFontScaleSync)
              },
            }
          ],
          { cancelable: false }
        );
      }
    }
    catch (e) {
      console.warn("pinItem", e);
    }
  }

  const unpinItem = async () => {
    try {
      setPinnedLoaded(false)

      let formData = new FormData();
      formData.append('user_id', userData.data.id);
      formData.append('pin_id', pin_id);
      formData.append('api_token', api_token);

      // console.log("unpin userData", userData);
      console.log("unpin formdata", formData);

      const data = await fetch(unpin_item_api, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: formData
      });

      const response = await data.json();
      console.log("unpin response", response)

      if (response.status) {
        setPin_id(0)
        setPinnedLoaded(true)
        setAlready_pinned(false)

        Alert.alert(
          'Success!',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: false }
        );
      }
      else {
        setPinnedLoaded(true)
        setAlready_pinned(true)
        Alert.alert(
          'Error!',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: false }
        );
      }
    }
    catch (e) {
      console.warn("unpinItem", e);
    }
  }

  const _copyToClip = async () => {
    await Clipboard.setString(refer_url);
    alert('URL copied to clipboard!');
  }

  const _renderVariants = () => {
    if (product?.info?.data?.with_variation === "yes") {
      if (product?.info?.data?.variation_1[0].options.length > 0) {
        let variation1 = product?.info?.data?.variation_1;

        return (
          <View>

            <View style={styles.referrCommissionCon}>
              <Text style={styles.txtReferr}>
                {variation1[0].variation_name + ":"}
              </Text>
              {/* <Text style={styles.txtReferrs}>
                {_renderVariant1(variation1)}
              </Text> */}
              <View style={{ flexDirection: 'row', marginTop: 10, }}>
                {/* <View style={{ alignSelf: 'flex-start' }}> */}
                {_renderVariant1(variation1)}
                {/* </View> */}
              </View>
            </View>
            <View style={styles.border} />
          </View>
        )
      }
    }
    // _renderVariant2();
  }

  const _renderVariant1 = (variation1) => {
    return (
      variation1[0].options.map((option, i) => {
        return (
          // option + " "
          <TouchableOpacity
            activeOpacity={1}
            onPress={() => {
              alert("asd")
              // handleStandardCourierSubmit();
              // renderDiscountedPriceSaveBtn()
            }}
            style={styles.referrerSaveBtn}
          >
            <Text style={styles.SaveText}>{option}</Text>
          </TouchableOpacity>

          // <Text style={styles.txtReferrs}>
          //   {option}
          // </Text>
        )
      })
    )
  }

  const _renderVariant2 = () => {
    if (product?.info?.data?.variation_2[0].options.length > 0) {
      // console.log("variation2")
      let variation2 = product?.info?.data?.variation_2;
      for (let index = 0; index < variation2[0].options.length; index++) {
        // console.log("variation2 index", index)
        // return (
        //   <View style={styles.referrCommissionCon}>
        //     <Text style={styles.txtReferr}>
        //       {variation2[0].options[index]}
        //     </Text>
        //     {/* <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text> */}
        //   </View>
        // )
      }
    }
  }

  const _renderAttributes = () => {
    // return product.info.data.attributes.map((data, i) => {

    if (product?.info?.data?.attributes.length > 0) {
      let attributes = product.info.data.attributes

      return attributes.map((attribute, i) => {
        return (
          <View key={i}>
            <TouchableOpacity style={styles.specificationsCon}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ alignSelf: 'flex-start' }}>
                  <Text style={styles.txtSpecifications}>{attribute.attribute_name}</Text>
                </View>
                <View style={styles.brandCon}>
                  <Text style={styles.txtBrand}>{attribute.attribute_value}</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )
      })

      // for (let i = 0; i < attributes.length; i++) {
      //   // console.log("attributes", attributes)
      //   return (
      //     <View>
      //       <TouchableOpacity style={styles.specificationsCon}>
      //         <View style={{ flexDirection: 'row' }}>
      //           <View style={{ alignSelf: 'flex-start' }}>
      //             <Text style={styles.txtSpecifications}>{attributes[i].attribute_name + ":"}</Text>
      //           </View>
      //           <View style={styles.brandCon}>
      //             <Text style={styles.txtBrand}>{attributes[i].attribute_value}</Text>
      //           </View>
      //         </View>
      //       </TouchableOpacity>
      //     </View>
      //   )
      // }
    } else {
      return null
    }
  }

  const handleAddToCart = async () => {
    console.warn('missing', product.info.data.shipping_info.delivery_options[0].delivery_option)
    try {
      let formData = new FormData();
      formData.append("product_id", product.id);
      formData.append("qty", quantity);
      formData.append("user_id", userData.data.id);
      formData.append("price", product.price);
      formData.append("variations", '');
      formData.append("api_token", api_token);
      formData.append("variation_1", product.info.data.variation_1);
      formData.append("variation_2", product.info.data.variation_2);
      formData.append("delivery_courier_slug", product.info.data.shipping_info.delivery_options[0].delivery_option);
      formData.append("delivery_courier_fee", product.info.data.shipping_info.delivery_options[0].delivery_option_info.fee)
      console.log("this is cart data", formData)
      const data = await fetch(Endpoints.add_to_cart + `?include=date,info`, {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
      )
      const response = await data.json()
      console.warn('product detail', JSON.stringify(response))
      if (response.status == true) {
        Alert.alert(
          'Success',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                setModalVisible(false)
              }
            },
          ],
          { cancelable: true }
        );
      }
      else {
        Alert.alert(
          'Error',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: true }
        );
      }
    }
    catch (e) {
      console.log("handleAddToCar", e)
    }
  }

  const handleCheckout = async () => {
    try {

      let checkoutItem = [];
      checkoutItem.pid = product.id;
      checkoutItem.product_name = product.product_name;
      checkoutItem.price = product.price;
      checkoutItem.qty = quantity;
      checkoutItem.total = parseFloat(quantity) * parseFloat(product.price);
      checkoutItem.delivery_courier_slug = product.info.data.shipping_info.delivery_options[0].delivery_option
      checkoutItem.delivery_courier_fee = product.info.data.shipping_info.delivery_options[0].delivery_option_info.fee

      if (product?.info.data?.with_variation.toString().toLowerCase() === "no") {
        checkoutItem.variations = null;
        checkoutItem.variation_1 = null;
        checkoutItem.variation_2 = null;
      } else if (product?.info.data?.with_variation.toString().toLowerCase() === "yes") {
        // checkoutItem.variation_1 = product.info.data.variation_1[0].options[0];
        // checkoutItem.variation_2 = product.info.data.variation_2[0].options[0];
        checkoutItem.variation_1 = 0;
        checkoutItem.variation_2 = 0;
        checkoutItem.variations = product.info.data.variation_1[0].options[0] + " " + product.info.data.variation_2[0].options[0];
      }

      checkoutItem.uid = userData.data.id;
      checkoutItem.checkoutType = "single";

      setModalVisible(false)

      navigation.navigate('Checkout', { checkoutData: checkoutItem })
    } catch (err) {
      console.log(err)
    }
  }

  const handleLogin = () => {
    Alert.alert(
      'Unauthenticated',
      'You are not logged in! Login now to continue',
      [
        {
          text: 'Login',
          onPress: () => navigation.navigate("Sign In"),
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      { cancelable: false }
    );
  }

  return (
    <Container style={{
      backgroundColor: '#f5f6fa'
    }}>
      <Modal visible={imageModal} transparent={true} onRequestClose={() => {
        setImageModal(false)
      }}>
        <ImageViewer imageUrls={previewImages} />
      </Modal>
      <Header
        style={styles.headCon}>
        <Left style={styles.leftCon}>
          <TouchableOpacity onPress={() => navigation.goBack()}
            style={styles.imgBackCon}>
            <Thumbnail
              source={require('../../assets/headericon/left-arrow-dark.png')}
              style={styles.imgBack}
              square
            />
            <Text
              ellipsizeMode='tail'
              numberOfLines={1}
              style={[styles.headerText, { marginHorizontal: 15, alignSelf: 'center', }]}
            >{product_name}</Text>
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 0, paddingLeft: 10, }}>
          <View style={{ paddingLeft: 15 }} />
        </Body>
        <Right style={styles.rightCon}>
          <TouchableOpacity style={styles.cartCon} onPress={() => {
            if (!loggedIn) handleLogin()
            else navigation.navigate("Cart Screen")
          }}>
            <Thumbnail
              source={require('../../assets/internal/cart.png')}
              style={styles.cartImg}
              square
            />
          </TouchableOpacity>
          {already_pinned ?
            <TouchableOpacity
              onPress={() => {
                if (!loggedIn) handleLogin()
                else {
                  Alert.alert(
                    'Warning!',
                    'Are you sure you want to unpin this item?',
                    [
                      {
                        text: 'OK',
                        onPress: () => unpinItem(),
                      },
                      {
                        text: 'Cancel',
                        style: 'cancel',
                      },
                    ],
                    { cancelable: false }
                  );
                }
              }}

              style={styles.pinnedCon}
            >
              <Thumbnail
                source={require('../../assets/internal/icon_Pinned.png')}
                style={styles.imgPin}
                square
              />
            </TouchableOpacity>
            :
            <TouchableOpacity
              onPress={() => {
                if (!loggedIn) handleLogin()
                else {
                  Alert.alert(
                    'Warning!',
                    'Are you sure you want to pin this item?',
                    [
                      {
                        text: 'OK',
                        onPress: () => pinItem(),
                      },
                      {
                        text: 'Cancel',
                        style: 'cancel',
                      },
                    ],
                    { cancelable: false }
                  );
                }
              }}
              style={styles.unpinCon}
            >
              <Thumbnail
                source={require('../../assets/internal/icon_Unpin.png')}
                style={styles.imgUnpin}
                square
              />
            </TouchableOpacity>}
        </Right>
      </Header>

      <FlatListComponent
        data={user_products.filter(x => x.id != route.params.product_id)}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}
        ListHeaderComponent={renderHeader()}
        renderItem={_renderItem}
        removeClippedSubviews={false}
        windowSize={18}
        maxToRenderPerBatch={4}
        scrollEventThrottle={16}
      />

      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        style={styles.modals}
      >
        <TouchableWithoutFeedback onPress={() => { setModalVisible(!modalVisible) }}>
          <View style={styles.modalCon}>
            <Animatable.View animation="fadeInUpBig" duration={1000} style={styles.animate}>
              <TouchableHighlight>
                <View>
                  <View style={styles.modCon}>
                    {handleSwiper()}
                    {/* <Image
                        style={styles.imgProduct}
                        tint='default'
                        source={product_images != null ? { uri: product_images } : imgSample}
                        resizeMode='contain'
                      /> */}
                    <View style={{ alignSelf: 'flex-start', width: width / 2.5, height: height / 6.5, backgroundColor: '#fff', marginTop: 2 }}>
                      <Text style={styles.txtProductName} adjustsFontSizeToFit={true}>{product.product_name}</Text>
                      <Text style={styles.txtCountryCode} adjustsFontSizeToFit={true}>{product.country_code == 'PH' && '₱ '}{product.country_code == 'US' && '$ '}{product.price}</Text>
                      <Text style={styles.txtStock} adjustsFontSizeToFit={true}>Stock: {stock}</Text>
                      <ScrollView>
                        <View>
                          <HTML html={des} {...htmlConfig} />
                        </View>
                      </ScrollView>
                    </View>
                    <TouchableOpacity
                      onPress={() => { setModalVisible(!modalVisible) }}
                      style={styles.btnExit}
                    >
                      <Thumbnail
                        source={require('../../assets/internal/icon_ex.png')}
                        square
                        style={styles.imgExit}
                      />
                    </TouchableOpacity>
                  </View>
                  <View style={styles.countCon}>
                    <TouchableOpacity onPress={() => {
                      setQuantity(quantity - 1)
                    }} disabled={quantity == 1 ? true : false}>
                      {/* minusQuantity()}> */}
                      <Thumbnail
                        square
                        source={require('../../assets/icon_minus.png')}
                        style={styles.imgAdd}
                      // resizeMode="contain"
                      />
                    </TouchableOpacity>
                    <Text style={styles.txtQuantity}>{quantity}</Text>
                    <TouchableOpacity onPress={() => {
                      setQuantity(quantity + 1)
                    }}>
                      {/* addQuantity()}> */}
                      <Thumbnail
                        square
                        source={require('../../assets/icon_plus.png')}
                        style={styles.imgAdd}
                      // resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity onPress={() => {
                    isSinglePurchase ? handleCheckout() : handleAddToCart();
                  }} style={styles.btnAddCartCon}>
                    <Text style={styles.txtAddCart}>
                      {isSinglePurchase ? "Buy Now" : "Add To Cart"}
                    </Text>
                  </TouchableOpacity>
                </View>
              </TouchableHighlight>
            </Animatable.View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>

      <Modal
        visible={referModal}
        style={{
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, .5)',
        }}
        transparent={true}
      >
        <TouchableWithoutFeedback onPress={() => { setReferModal(!referModal) }}>
          <View style={{
            flex: 1,
            justifyContent: 'center',
            backgroundColor: 'rgba(0,0,0,0.5)',
            opacity: 1,
            // width,
          }}>

            <View style={{
              width: width - 70,
              backgroundColor: '#FFF',
              alignItems: 'center',
              paddingLeft: 20,
              paddingRight: 20,
              borderRadius: 15,
              height: height / 1.4,
              alignSelf: 'center'
            }}>
              <View style={{
                flexDirection: 'row',
                // width:width -70
                // backgroundColor:'pink',
                // width:width/1.5,
                // justifyContent:'space-between'
              }}>
                <Text style={{
                  width: 210,
                  textAlign: 'center',
                  borderBottomColor: '#00a14b',
                  borderBottomWidth: 2,
                  // fontFamily: 'roboto-medium',
                  fontSize: 16,
                  color: '#231f20',
                  paddingVertical: 10,
                }}>Refer an item.</Text>
                <TouchableOpacity onPress={() => {
                  setReferModal(false)
                }}
                  style={{
                    position: 'absolute',
                    right: 0,
                    marginRight: -35,
                    padding: 10,
                    alignSelf: 'center',
                  }}>
                  <Thumbnail
                    source={require('../../assets/internal/icon_ex.png')}
                    style={{
                      tintColor: '#7f8c8d',
                      width: 13,
                      height: 13,
                    }}
                  />
                </TouchableOpacity>
              </View>

              <View style={{
                justifyContent: 'space-around',
                alignItems: 'center',
              }}>
                <View style={{ paddingTop: 5 }} />
                <Text style={{
                  // fontFamily: 'roboto-medium',
                  fontSize: 16,
                  color: '#231f20',
                }}>Share this product.</Text>
                <View style={{ paddingTop: 5 }} />
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                }}>When someone buys this product that you referred, 25% of sales will be yours.</Text>
                <View style={{ paddingTop: 10 }} />
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                }}>Keep Referring</Text>
                <View style={{ paddingTop: 10 }} />
                <View style={{
                  height: 50,
                  width: '100%',
                }}>
                  <ScrollView
                    horizontal={true}
                  >
                    <View style={{ justifyContent: 'space-around', flexDirection: 'row' }}>
                      <TouchableOpacity
                        onPress={() => {

                          Share.open({
                            title: 'Share this product',
                            subject: 'ReferApps',
                            message: refer_url
                          })
                            .then((res) => { console.log(res) })
                            .catch((err) => { err && console.log(err); });
                        }}
                        style={{
                          paddingRight: 5,
                          paddingLeft: 5,
                        }}
                      >
                        <Thumbnail
                          source={require('../../assets/internal/product/icon_refer.png')}
                          style={{
                            marginTop: 2.5,
                            width: 40,
                            height: 35
                          }}
                          square
                        />
                      </TouchableOpacity>
                    </View>
                  </ScrollView>
                </View>
                <View style={{ paddingTop: 5 }} />
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                }}>or just share this link</Text>
                <View style={{ paddingTop: 5 }} />
                <TouchableOpacity
                  onPress={() => _copyToClip()}
                  style={{
                    backgroundColor: '#ecf0f1',
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Text style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: '#5998ff',
                    paddingLeft: 5,
                    paddingRight: 5,
                  }}>{refer_url}</Text>
                </TouchableOpacity>
                <View style={{ paddingTop: 10 }} />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>

      <Modal
        visible={specificationsModal}
        style={{
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, .5)',
          position: 'absolute',
          justifyContent: 'space-between',
        }}
      >

        <View style={{
          width: width,
          flex: 1,
        }} />

        <Animatable.View animation="fadeInUpBig" duration={1000}
          style={{
            flex: 0,
            backgroundColor: '#ffffff',
            width: width,
          }}
          onLayout={
            (event) => {
              find_dimesions(event.nativeEvent.layout)
            }
          }
        >

          <View
            style={{
              flexDirection: 'row',
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 15,
              paddingRight: 15,
              alignItems: 'center',
              justifyContent: 'space-between',
            }}
          >
            <View style={{
              justifyContent: 'space-around',
              flex: 0,
            }}>
              <Thumbnail
                source={require('../../assets/internal/icon_ex.png')}
                square
                style={{
                  width: 16,
                  height: 16,
                  tintColor: '#ffffff',
                }}
              />
            </View>
            <View style={{
              justifyContent: 'space-around',
              flex: 1,
            }}>
              <Text style={{
                fontSize: 16,
                color: '#231f20',
                alignSelf: 'center',
              }}>Specifications</Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                setSpecificationsModal(false)
              }}
              style={{
                justifyContent: 'space-around',
                flex: 0,
                width: 25,
                height: 25,
                borderRadius: 12.5,
                backgroundColor: '#ecf0f1',
                alignItems: 'center',
              }}
            >
              <Thumbnail
                source={require('../../assets/internal/icon_ex.png')}
                square
                style={{
                  width: 14,
                  height: 14,
                  tintColor: '#231f20',
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{
            borderTopWidth: .7,
            width: width,
            borderColor: '#ecf0f1',
          }} />

          <View style={{
            height: (height / 2),
          }}>
          </View>

          <View style={{
            height: (height / 2),
          }}>
            <Text style={{
              fontSize: 12,
              color: '#7f8c8d',
              paddingBottom: 5,
            }}>There are no attributes for this product.</Text>
          </View>

        </Animatable.View>
      </Modal>

      <Footer style={{}}>
        <FooterTab style={{ backgroundColor: '#FFF' }}>

          <TouchableOpacity onPress={() => {
            if (loggedIn) {
              Alert.alert('Coming Soon!', 'This feature will be available soon.')
              // navigation.navigate('Conversation', { reciever: { id: shop_id, name: shop.shop_name, avatar: shop.shop_image } })
            } else handleLogin()
          }} style={styles.chatCon}>
            <Thumbnail
              source={require('../../assets/internal/product/icon_chat.png')}
              style={styles.bottomTabImage}
              square
            />
            <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Chat</Text>
          </TouchableOpacity>

          <View style={styles.barrier} />

          <TouchableOpacity onPress={() => {
            if (loggedIn) {
              setIsSinglePurchase(false)
              setModalVisible(true)
            } else handleLogin()
          }} style={styles.btnModalAddCon}>
            <Thumbnail
              source={require('../../assets/internal/product/icon_add2cart.png')}
              style={styles.bottomTabImage}
              square
            />
            <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Add to Cart</Text>
          </TouchableOpacity>

          <View style={styles.barrier} />

          <TouchableOpacity
            onPress={() => {
              if (loggedIn) {
                console.warn("REFER URL", refer_url);

                if (refer_url != "" && refer_url != null) {
                  // console.log("refer_url", refer_url);
                  navigation.navigate('QRGenerator', {
                    url: refer_url,
                    name: product_name,
                    image: product_images[0]?.directory + '/' + product_images[0]?.filename,
                  });
                }

                // setReferModal(true);
              } else handleLogin()
            }}
            style={styles.btnModalAddCon}>
            {/* {setReferModal(true);}}  */}
            <Thumbnail
              source={require('../../assets/internal/product/icon_refer.png')}
              style={styles.bottomTabImage}
              square
            />
            <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Refer</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              if (loggedIn) {
                setIsSinglePurchase(true)
                setModalVisible(true)

                // navigation.navigate('Checkout', {url: 'https://www.google.com'})
              } else handleLogin()
            }}
            // alert('Coming Soon', 'This feature will be available soon')} 
            style={styles.btnBuynow}>
            <Text style={[styles.bottomTabBuyText, { color: '#ffffff' }]}>Buy Now</Text>
          </TouchableOpacity>

        </FooterTab>
      </Footer>
    </Container>
  );
}

const HTMLstyles = StyleSheet.create({
  h1: {
    fontWeight: '300',
    color: '#231f20',
  },
  h2: {
    fontWeight: '300',
    color: '#231f20',
  },
  h3: {
    fontWeight: '300',
    color: '#231f20',
  },
  ul: {
    fontWeight: '100',
    color: '#231f20',
  },
  p: {
    fontWeight: '100',
    color: '#231f20',
  },
  i: {
    fontWeight: '100',
    color: '#231f20',
  }
});
