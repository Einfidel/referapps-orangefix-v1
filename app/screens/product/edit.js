import React, {
  useState,
  useEffect,
  createRef,
} from 'react';

import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import {
  Root,
  Container,
  Header,
  Left,
  Body,
  Right,
  Item,
  Input,
  Toast,
} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import { Picker } from '@react-native-picker/picker';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux';
import { styles, sell } from './styles';
import Service from '../../components/api/service';
import appleAuth from '@invertase/react-native-apple-authentication';
import Icon from '../../components/icons';
import Endpoints from '../../components/api/endpoints';

const { width, height } = Dimensions.get('window');

export default function EditProductScreen({ navigation, route }) {
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [discountedPriceModal, setDiscountedPriceModal] = useState(false);
  const [photoModal, setPhotoModal] = useState(false);
  const [referrerModal, setReferrerModal] = useState(false);
  const [packagingModal, setPackagingModal] = useState(false);
  const [conditionModal, setConditionModal] = useState(false);
  const [categoriesModal, setCategoriesModal] = useState(false);
  const [subCategoriesModal, setSubCategoriesModal] = useState(false);
  const [specificCategoryModal, setSpecificCategoryModal] = useState(false);
  const [shippingModal, setShippingModal] = useState(false);
  const [packaging_size, setPackagingSize] = useState([{ width: '' }, { length: '' }, { height: '' }]);

  const [user, setUser] = useState(null);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [category_id, setCategoryId] = useState('');
  const [category, setCategory] = useState('');
  const [subcategory, setSubcategory] = useState('');
  const [specificcategory, setSpecificCategory] = useState('');
  const [attributes, setAttributes] = useState([]);
  const [hasVariation, setHasVariation] = useState(false);
  const [variationOptions, setVariationOptions] = useState([]);
  const [price, setPrice] = useState(0);
  const [hasDiscount, setHasDiscount] = useState(false);
  const [discountIsFixed, setDiscountIsFixed] = useState(false);
  const [discountFixedAmmount, setDiscountFixedAmmount] = useState(0);
  const [discountPercentageAmmount, setDiscountPercentageAmmount] = useState(0);
  const [discountedPrice, setDiscountedPrice] = useState(0);
  const [stock, setStock] = useState(0);
  const [weight, setWeight] = useState(0);
  const [to_send_packagingSize, setToSendPackagingSize] = useState([]);
  const [to_send_attributes, setToSendAttributes] = useState([]);
  const [condition, setCondition] = useState('');
  const [warranty, setWarranty] = useState('');
  const [commissionIsFixed, setCommissionIsFixed] = useState(false);
  const [referrerFixedCommission, setReferrerFixedCommission] = useState('');
  const [referrerPercentageCommission, setReferrerPercentageCommission] = useState('');
  const [lastCommission, setLastCommission] = useState('');
  const [text, setText] = useState('');
  const [conditionText, setConditionText] = useState('');
  const [images, setImages] = useState([]);
  const [oldImages, setOldImages] = useState([]);
  const [freeShipping, setFreeShipping] = useState(true);
  const [ownPackaging, setOwnPackaging] = useState(false);
  const [forPickup, setForPickup] = useState(false);
  const [shippingFeeOptions, setShippingFeeOptions] = useState([]);
  const [main_categories, setMainCategories] = useState([]);
  const [shipping_options, setShippingOptions] = useState([]);

  const [subCategoriesList, setSubCategoriesList] = useState([]);
  const [specificCategoriesList, setSpecificCategoriesList] = useState([]);
  const [categoryAttributes, setCategoryAttributes] = useState([]);

  // const commissionInput = createRef();

  const [filePath, setFilePath] = useState(null);
  const [imageUpdated, setImageUpdated] = useState(null);
  const [fileData, setFileData] = useState(null);

  const [responseData, setResponseData] = useState(null);
  const [loadingModal, setLoadingModal] = useState(null);
  const [imageSelected, setImageSelected] = useState(null);

  const [attributeTmp, setAttributeTmp] = useState("");

  const [submitTapped, setSubmitTapped] = useState(false);

  const [fetchedProduct, setFetchedProduct] = useState([
  ]);

  const [iosMargin, setiosMargin] = useState(0);

  const [updateProductData, setUpdateProductData] = useState([
    { pid: '' },
    { name: '' },
    { commissionIsFixed: false },
    { referrerFixedCommission: '' },
    { referrerPercentageCommission: '' },
    { to_send_attributes: [] },
    { category_id: '' },
    { description: '' },
    { condition: '' },
    { warranty: '' },
    { country_code: '' },
    { is_recurring: '' },
    { variationOptions: [] },
    { price: '' },
    { hasDiscount: false },
    { discountAmmount: '' },
    { discountFixedAmmount: '' },
    { discountPercentageAmmount: '' },
    { discountedPrice: '' },
    { stock: '' },
    { weight: '' },
    { to_send_packagingSize: [] },
    { shippingFeeOptions: [] },
    { freeShipping: true },
    { ownPackaging: false },
    { forPickup: false },
    { images: [] },
  ]);

  //errors
  const [fieldErrors, setFieldErrors] = useState([
    { imageEmpty: false },
    { imageMorethan5mb: false },
    { nameEmpty: false },
    { descriptionEmpty: false },
    { commission: false },
    { mainCategoryEmpty: false },
    { conditionEmpty: false },
    { weightEmpty: false },
    { packageSizeEmpty: false },
  ])

  const [currency, setCurrency] = useState(null);

  useEffect(() => {
    // console.log("useEffect");
    onLoad();

    appleAuth.isSupported ? setiosMargin(0) : setiosMargin(0);

    // fetchProduct(); Product is received from params on your_product.js
    let { info, referrer_commission, is_recurring, is_pickup } = route.params;
    let {
      image,
      product_name,
      description,
      category,
      category_id,
      attributes,
      parent_category_id,
      product_images,
      country_code,
      shipping_info,
      condition,
      commission_type,
      with_discount,
      discount_amount,
      discount_percentage,
      warranty_details,
      inventory,
      with_variation,
      variation_1,
      variation_2,
      price,
    } = info.data;

    // console.log("route.params.variation_1", route.params.variation_1);
    // console.log("Edit Product product_images:", product_images)

    // setImages([...images, { uri: image.full_path }])
    // console.log("Edit Product product_images:", [...images, { uri: image.full_path }])
    // Edit Product product_images: [{"uri": "https://www.referapps.com/storage/products/20210804/BjHx1SuFC3ijYDKXWosn080420210254.jpg"}]

    let tmpImagesArray = [];

    product_images.map((imageTmp, index) => {
      if (imageTmp) {
        console.log("Edit Product product_images:", imageTmp.directory + "/" + imageTmp.filename)
        tmpImagesArray = [...tmpImagesArray, {
          uri: imageTmp.directory + "/" + imageTmp.filename
        }]
      }
    });

    setImages(tmpImagesArray);
    setOldImages(product_images);
    // console.log('product_images', product_images);
    // Edit Product product_images: https://www.referapps.com/storage/products/20210804/BjHx1SuFC3ijYDKXWosn080420210254.jpg


    setName(product_name);
    setDescription(description);
    commission_type == "fix" ? setCommissionIsFixed(true) : setCommissionIsFixed(false);
    commission_type == "fix" ? setReferrerFixedCommission(referrer_commission) : setReferrerPercentageCommission(referrer_commission);
    // setCategory(category);
    if (with_variation == "yes") {
      setHasVariation(true);
      // console.log("inventory", inventory[0])
      let variationArray = [];
      let tmpStock = 0;
      let tmpPrice = 0;

      if (variation_2 != null) {
        variation_1[0].options.map((option1, index1) => {
          let childArray = [];
          // console.log("1st Variation: ", variation_1[0].variation_name)
          // console.log(index1, " option: ", option1)

          variation_2[0].options.map((option2, index2) => {
            tmpStock = 0;
            tmpPrice = 0;
            inventory.map((inventoryOption, index3) => {
              if (inventoryOption.variation_1_id == index1 && inventoryOption.variation_2_id == index2) {
                tmpStock = inventoryOption.stock;
                tmpPrice = inventoryOption.price;
              }
            })
            if (childArray.length > 0) {
              childArray = [
                ...childArray,
                {
                  "variation_name": variation_2[0].variation_name,
                  "name": option2,
                  "stock": tmpStock,
                  "price": tmpPrice,
                }
              ]
            } else {
              childArray = [
                {
                  "variation_name": variation_2[0].variation_name,
                  "name": option2,
                  "stock": tmpStock,
                  "price": tmpPrice,
                }
              ]
            }

            // console.log("2 vars childArray", childArray)
          })

          if (variationArray.length > 0) {
            variationArray = [
              ...variationArray,
              {
                "variation_name": variation_1[0].variation_name,
                "parent_name": option1,
                "child": childArray
              }
            ];
          } else {
            variationArray = [
              {
                "variation_name": variation_1[0].variation_name,
                "parent_name": option1,
                "child": childArray
              }
            ];
          }
        })
      } else {
        variation_1[0].options.map((option1, index1) => {
          tmpStock = 0;
          tmpPrice = 0;
          inventory.map((inventoryOption, index2) => {
            if (inventoryOption.variation_1_id == index1) {
              tmpStock = inventoryOption.stock;
              tmpPrice = inventoryOption.price;
            }
          })

          if (variationArray.length > 0) {
            variationArray = [...variationArray, {
              "variation_name": variation_1[0].variation_name,
              "parent_name": option1,
              "stock": tmpStock,
              "price": tmpPrice,
              "child": []
            }];
          } else {
            variationArray = [{
              "variation_name": variation_1[0].variation_name,
              "parent_name": option1,
              "stock": tmpStock,
              "price": tmpPrice,
              "child": []
            }];
          }
        })

        console.log("1 var variationArray", variationArray)
      }

      setVariationOptions(variationArray);
    } else {
      setHasVariation(false);
    }

    // setPrice(with_variation == "yes" ? '' : inventory.price);
    setPrice(with_variation == "yes" ? '0' : parseFloat(price) + parseFloat(discount_amount));
    console.log("with_variation", with_variation)
    console.log("parseFloat(price) + parseFloat(discount_amount)", parseFloat(price) + parseFloat(discount_amount))
    setHasDiscount(with_discount == "yes" ? true : false);
    if (with_discount === 'yes') {
      setDiscountFixedAmmount(parseFloat(discount_amount));
      setDiscountPercentageAmmount(parseFloat(discount_percentage));
      setDiscountedPrice(parseFloat(price));
    }
    setStock(with_variation == "yes" ? '0' : inventory.stock);
    let isOwnPackaging = "false";
    shipping_info.is_own_packaging ? isOwnPackaging = "true" : isOwnPackaging = "false";
    setOwnPackaging(isOwnPackaging);
    let isPickup = "false";
    setWeight(shipping_info.weight);
    setPackagingSize([
      {
        width: shipping_info.width,
      },
      {
        length: shipping_info.length,
      },
      {
        height: shipping_info.height,
      }
    ]);
    setToSendPackagingSize([
      {
        width: parseFloat(shipping_info.width),
      },
      {
        length: parseFloat(shipping_info.length),
      },
      {
        height: parseFloat(shipping_info.height),
      }
    ]);
    setFreeShipping(shipping_info.delivery_options[0].delivery_option == "free-shipping" ? true : false)
    setShippingFeeOptions(sortFetchedShippingFeeOptions(shipping_info.delivery_options));
    setOwnPackaging(shipping_info.is_own_packaging ? true : false)
    setForPickup(is_pickup == "yes" ? true : false);
    setCondition(condition == "used" ? "Used ( Like new )" : condition == "new" ? "New" : '');
    setWarranty(warranty_details === '---' ? '' : warranty_details)
    setToSendAttributes(attributes);

    fetchShippingOptions();
    fetchMainCategories();

    // console.log("shippingFeeOptions fetched", sortFetchedShippingFeeOptions(shipping_info.delivery_options));
    // console.log("shipping_options fetched", shipping_options);

    setAllCategories(category_id, category, parent_category_id[0], attributes);
  }, [])

  const onLoad = async () => {
    try {
      // Set User
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) setUser(user);

      // Set Currency
      if (user?.data?.country_code === "PH") {
        setCurrency("₱");
      } else if (user?.data?.country_code === "US") {
        setCurrency("$");
      } else {
        setCurrency("");
      }

      fetchProduct();
    } catch (error) {
      console.warn(error);
    }

  }

  const setAllCategories = async (category_id_tmp, category, parent_category_id, attributes) => {
    setCategoryId(category_id_tmp);

    // console.log("category", category)
    // console.log("parent_category_id", parent_category_id)

    let catLevels = category.split('>').length;

    // console.log("catLevels", catLevels)

    let mainCatName = category.split('>')[0].trim();

    try {
      await Service.getMainCategories(
        (res) => {
          setMainCategories(res.data);
          setCategory(mainCatName);
          // console.log("mainCatName", mainCatName)
          // console.log("setMainCategories", res.data)
          // console.log("setMainCategories")
        },
        (err) => console.log(err),
      );
    } catch (error) {
      console.warn('Error', error.message);
    }

    if (category.split('>')[1] != null) {
      let subCatName = category.split('>')[1].trim();
      try {
        await Service.getSubCategory(
          parent_category_id.main_category,
          (res) => {
            // console.log("setSubCategoriesList res", res);
            setSubCategoriesList(res.sub.data);
            setSubcategory(subCatName);
            // console.log("subCatName", subCatName)
            // console.log("setSubCategoriesList")
          },
          (err) => console.log(err)
        );
      } catch (error) {
        console.warn('Error', error.message);
      }

      if (category.split('>')[2] != null) {
        let specificCatName = category.split('>')[2].trim();
        try {
          await Service.getSpecificCategory(
            parent_category_id.sub_category,
            async (res) => {
              setSpecificCategoriesList(res.data);
              setSpecificCategory(specificCatName);
              // console.log("setSpecificCategory", specificCatName)
              // console.log("setSpecificCategoriesList")

              await Service.getCategoryAttribute(
                parent_category_id.specific_category,
                (res) => {
                  // console.log("getCategoryAttribute res.data", res.data)
                  // console.log("getCategoryAttribute attributes", attributes)
                  // console.log("attributes", attributes)
                  res.data.map((attribute, index) => {
                    if (attributes.length > 0) {
                      attribute.value = attributes[index].attribute_value;
                    } else {
                      attribute.value = null;
                    }
                    attribute.attribute = attribute.attribute_name;
                    attribute.required = attribute.is_required;
                    attribute.category_id = category_id_tmp;
                    categoryAttributes[index] = attribute;
                    // console.log("attribute.value", categoryAttributes[index].value)
                    // console.log("attribute.attribute_value", categoryAttributes[index].attribute_value)
                    // console.log("attribute.attribute", categoryAttributes[index].attribute)
                    // console.log("attribute.attribute_name", categoryAttributes[index].attribute_name)
                    // console.log("attribute.required", categoryAttributes[index].required)
                    // console.log("attribute.is_required", categoryAttributes[index].is_required)
                    // console.log("attribute.category_id", categoryAttributes[index].category_id)
                    // console.log("~~~~~~~~~~~~~~~~~~")

                    if ((res.data.length - 1) == index) {
                      setCategoryAttributes(categoryAttributes);
                    }
                  });

                  setAttributeTmp("trigger");
                },
                (err) => console.log(err)
              );

            },
            (err) => console.log(err)
          );
        } catch (error) {
          console.warn('Error', error.message);
        }
      }
    }
  }

  const sortFetchedShippingFeeOptions = (data) => {
    if (data.length === 0) return data;
    let sortedData = [];
    data.map((item, index) => {
      sortedData.push({
        id: index + 1,
        name: item.delivery_option,
        shipping_fee: item.delivery_option_info.fee,
      });
    });
    return sortedData;
  };

  const fetchMainCategories = async () => {
    try {
      await Service.getMainCategories(
        (res) => {
          // console.log(
          //   'Main categories response',
          //   JSON.stringify(res, null, '\t'),
          // );
          res.status && setMainCategories(res.data);;
        },
        (err) => console.log(err),
      );
    } catch (error) {
      console.warn('Error', error.message);
    }
  };

  const fetchShippingOptions = async () => {
    try {
      // await Service.getShippingOptions(
      await Service.getPartnerLogistics(
        (res) => {
          if (res.status) {
            let shipping_options_tmp = res.data;
            let shippingFeeOptions_tmp = shippingFeeOptions;

            shippingFeeOptions_tmp.map((item) => {
              let index = shipping_options_tmp.findIndex(
                (option) => option.slug === item.name,
              );
              shipping_options_tmp[index].enabled = true;
              shipping_options_tmp[index].fee = item.shipping_fee;
            });
            setShippingOptions(shipping_options_tmp);
          }
        },
        (err) => console.log('Error', err.message),
      );
    } catch (error) {
      console.warn('Error', error.message);
    }
  };

  const fetchProduct = async () => {
    // Product is received from params on your_product.js

    // const _user = await AsyncStorage.getItem('user_data');
    // let user = _user ? JSON.parse(_user) : null;
    // if (user) {
    //   await Service.getAccountProducts(
    //     user.data.id,
    //     1,
    //     (res) => {
    //       this.setState({
    //         userServices: res,
    //         user: user,
    //       });
    //     },
    //     (err) => console.log(err),
    //   );
    // }

    let user
    if (!user) {
      const _user = await AsyncStorage.getItem('user_data');
      user = _user ? JSON.parse(_user) : null;
      if (user) setUser(user);
    }


    // let data = {}
    // data.user_id = user.data.id
    // data.viewType = 'profile'
    // data.sort_by = 'latest'

    // await Service.getAccountProducts(
    //   data,
    //   (res) => {        // 
    //     let allService = [];
    //     for (let i = 0; i < res.data.length; i++) {
    //       allService.push(res.data[i]);
    //     }
    //     setFetchedProduct(allService);
    //   },
    //   (err) => console.log('Service.getAccountProducts Error', err)
    // );
  }

  const handleImagePicker = () => {
    if (images.length > 0) {
      return images.map((img, imgIndex) => {
        return (
          <TouchableOpacity key={imgIndex} activeOpacity={1} onPress={() => {
            setPhotoModal(true);
            setImageSelected(imgIndex);
          }}>
            <FastImage
              key={imgIndex}
              source={{ uri: img.uri }}
              style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        );
      });
    } else {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto();
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleUIAddPhoto = () => {
    if (images.length > 0 && images.length < 11) {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto()
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        setImages([...images, response]);
        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleChangePhoto = (index) => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        images.splice(index, 1, response);

        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleRemovePhoto = (index) => {
    images.splice(index, 1);
  };

  const renderPhotoModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={photoModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setPhotoModal(!photoModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    handleChangePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Change Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Remove Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    setPhotoModal(!photoModal);
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible1} style={styles.modalCon}>
          {_renderReferrModal()}
        </Modal>
      </View>
    );
  };

  const handlePercentageCommissionInputChange = (text) => {
    setReferrerPercentageCommission(text.replace(/[^0-9]/g, ''));
  };

  const handleFixedCommissionInputChange = (text) => {
    setReferrerFixedCommission(text);
  };

  const _renderReferrModal = () => {
    // Fixed
    if (commissionIsFixed) {
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerFixedCommission}
                  onChangeText={(text) => handleFixedCommissionInputChange(text)}
                // maxLength={2}
                />
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else { // Percentage
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerPercentageCommission}
                  onChangeText={(text) => handlePercentageCommissionInputChange(text)}
                  maxLength={2}
                />
                <Text style={sell.percentText}>%</Text>
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  const _renderReferrerSaveButton = () => {
    if (!commissionIsFixed && (referrerPercentageCommission < 5 || referrerPercentageCommission > 50)) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      setModalVisible1(false);
      // setReferrerModal(false);
      // setLastCommission(referrerFixedCommission);
    }
  };

  const renderCategoriesModal = (data) => {
    // console.log("Main Category ID", category_id);
    // console.log("Main Category Name", category);
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={categoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setCategoriesModal(!categoriesModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {main_categories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSubCategory(
                            cat.id,
                            (res) => {
                              if (category != cat.category_name) {
                                setSubcategory('');
                                setSpecificCategory('');
                                setCategoryAttributes([]);

                                setCategoryId(cat.id);
                                setSubCategoriesList(res.sub.data);
                                // console.log("cat.category_name", cat.category_name, cat.id);
                                setCategory(cat.category_name);
                              }
                              setCategoriesModal(false);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSubCategoriesModal = (data) => {
    // console.log("Sub Category ID", category_id);
    // console.log("Sub Category Name", subcategory);
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={subCategoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSubCategoriesModal(!subCategoriesModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Sub Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {subCategoriesList.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSpecificCategory(
                            cat.id,
                            (res) => {
                              if (subcategory != cat.category_name) {
                                setCategoriesModal(false);
                                setSpecificCategory('');
                                setCategoryAttributes([]);
                                setCategoryId(cat.id);
                                // console.log("getSpecificCategory", res.data);
                                setSubcategory(cat.category_name);
                                setSpecificCategoriesList(res.data);
                              }
                              setSubCategoriesModal(false);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSpecificCategoriesModal = (data) => {
    // console.log("Specific Category ID", category_id);
    // console.log("Specific Category Name", specificcategory);
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={specificCategoryModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSpecificCategoryModal(!specificCategoryModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Specific Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {specificCategoriesList.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getCategoryAttribute(
                            cat.id,
                            (res) => {
                              if (specificcategory != cat.category_name) {
                                setSubCategoriesModal(false);
                                setAttributes([]);
                                setCategoryId(cat.id);
                                setSpecificCategory(cat.category_name);
                                setCategoryAttributes([]);
                                remapCategoryAttributes(res.data, cat.id);
                                // setCategoryAttributes(res.data);

                              }
                              setSpecificCategoryModal(false);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const remapCategoryAttributes = (data, categoryId) => {
    categoryAttributes.map((attribute, index) => {
      categoryAttributes[index] = null;
    });

    data.map((attribute, index) => {
      attribute.value = "";
      attribute.attribute = attribute.attribute_name;
      attribute.required = attribute.is_required;
      attribute.category_id = categoryId;
      categoryAttributes[index] = attribute;
      // console.log("categoryAttributes", categoryAttributes)

      if ((data.length - 1) == index) {
        setCategoryAttributes(categoryAttributes);
      }

      setAttributeTmp("Trigger");
      let tmp = attributeTmp;
    });
  }

  const renderCategoryAttributes = () => {
    // console.log("categoryAttributes", categoryAttributes);
    // console.log("renderCategoryAttributes");
    return categoryAttributes.map((attribute, index) => {
      // console.log("attribute", attribute)
      if (attribute) {
        return (
          <View key={index}>
            {renderAttributeErrors(attribute)}
            <View style={sell.whiteBorder}>
              <TouchableOpacity activeOpacity={0.4}
              // onPress={() => setCategoriesModal(true)}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 10 }} />
                    <Text style={sell.inputText}>{attribute.attribute_name}</Text>
                  </View>
                  <View style={{}}>
                    <Input
                      style={sell.group_input_text}
                      placeholder={'Set ' + attribute.attribute_name}
                      placeholderTextColor={'#7f8c8d'}
                      value={attribute.value}
                      onChangeText={(value) => {
                        attribute.value = value;

                        // Used to trigger
                        setAttributeTmp(value);
                        let tmp = attributeTmp;
                      }}
                    // value={attribute.value}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    });
  };

  const renderPrice = () => {
    // console.log("price", price)
    if (!hasVariation) {
      return (
        <View style={sell.white}>
          <View style={sell.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginLeft: 25 }} />
              <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} />
              <Text style={sell.inputText}>Price</Text>
            </View>
            <Item style={sell.input_title}>
              <View style={{ paddingLeft: 20 }}>
                <Text style={sell.Kg}>{currency}</Text>
              </View>
              <Input
                style={sell.group_input_text}
                placeholder={'Price'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'decimal-pad'}
                onChangeText={(price) => {
                  if (!discountIsFixed) {
                    setDiscountedPrice(parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01));
                  } else if (discountIsFixed) {
                    setDiscountedPrice(parseFloat(price) - parseFloat(discountFixedAmmount));
                  }
                  console.log("onchange price", price)
                  setPrice(price);
                }}
                value={price.toString()}
              />
            </Item>
          </View>
        </View>
      )
    }
  }

  const renderDiscountType = () => {
    if (hasDiscount && !hasVariation) {
      return (
        // DISCOUNT TYPE
        <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
          <TouchableOpacity
            activeOpacity={0.4}
            // onPress={() => setModalVisible1(true)}
            style={sell.inputPickerContainer}
          >
            <View style={sell.inputPickerContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Discount Type</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Picker
                  selectedValue={discountIsFixed}
                  mode='dropdown'
                  style={[sell.datePicker, {}]}
                  itemStyle={{
                    // fontFamily: 'Roboto',
                    fontSize: 12,
                    color: '#231f20',
                  }}
                  onValueChange={(value) => {
                    setDiscountIsFixed(value);
                    if (value) {
                      setDiscountedPrice(price - discountFixedAmmount)
                    } else {
                      setDiscountedPrice(price - (discountPercentageAmmount * 0.01))
                    }
                  }}
                >
                  <Picker.Item label='% Percent' value={false} />
                  <Picker.Item label='Fixed' value={true} />
                </Picker>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )
    };
  }

  const renderDiscountAmmount = () => {
    if (hasDiscount && !hasVariation) {
      return (
        <View style={sell.whiteBorder}>
          <TouchableOpacity
            activeOpacity={0.4}
            onPress={() => setDiscountedPriceModal(true)}
            style={sell.inputContainer}
          >
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Discount Amt.</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.hintText2}>
                  {getDiscountText()}
                </Text>
                <View style={sell.spaceRight} />
                <FastImage
                  source={require('../../assets/icon_sub.png')}
                  style={sell.openIcon}
                  tintColor={'#7f8c8d'}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }

  const renderDiscountedPrice = () => {
    if (hasDiscount && !hasVariation) {
      return (
        // DISCOUNTED PRICE 
        <View style={sell.white}>
          <View style={sell.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginLeft: 25 }} />
              <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} />
              <Text style={sell.inputText}>Discounted Price</Text>
            </View>
            <Item style={sell.input_title}>
              <View style={{ paddingLeft: 50 }}>
                <Text style={sell.Kg}>{currency} {discountedPrice}</Text>
              </View>
            </Item>
          </View>
        </View>
      );
    }
  }

  const renderDiscountedPriceModal = () => {
    if (discountIsFixed) {// Fixed
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Modal animationType='slide' transparent={true} visible={discountedPriceModal} style={styles.modalCon}>
            <TouchableWithoutFeedback
              onPress={() => {
                setDiscountedPriceModal(!discountedPriceModal);
              }}
            >
              <View style={styles.containerModal}>
                <View style={sell.ReferrerModalConatiner}>
                  <Text style={sell.ReferrerModalHeaderText}>Set Discounted Price (1 - {price});</Text>
                  <Item>
                    <Input
                      style={sell.SetCommission}
                      placeholder={'Set Discount'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      value={discountFixedAmmount}
                      onChangeText={(text) => {
                        setDiscountFixedAmmount(text);
                      }}
                    // maxLength={2}
                    />
                  </Item>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => renderDiscountedPriceSaveBtn()}
                    style={sell.referrerSaveBtn}
                  >
                    <Text style={sell.SaveText}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    } else { // Percentage
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Modal animationType='slide' transparent={true} visible={discountedPriceModal} style={styles.modalCon}>
            <TouchableWithoutFeedback
              onPress={() => {
                setDiscountedPriceModal(!discountedPriceModal);
              }}
            >
              <View style={styles.containerModal}>
                <View style={sell.ReferrerModalConatiner}>
                  <Text style={sell.ReferrerModalHeaderText}>Set Discounted Price(1% - 100%)</Text>
                  <Item>
                    <Input
                      style={sell.SetCommission}
                      placeholder={'Set Discount'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      value={discountPercentageAmmount}
                      onChangeText={(text) => {
                        setDiscountPercentageAmmount(text.replace(/[^0-9]/g, ''));
                      }}
                      maxLength={2}
                    />
                    <Text style={sell.percentText}>%</Text>
                  </Item>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => renderDiscountedPriceSaveBtn()}
                    style={sell.referrerSaveBtn}
                  >
                    <Text style={sell.SaveText}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    }
  };

  const renderDiscountedPriceSaveBtn = () => {
    if (!discountIsFixed && (parseFloat(discountPercentageAmmount) < 1 || parseFloat(discountPercentageAmmount) > 100)) {
      return alert('Invalid Percentage! (1% - 100%)');
    } else if (!discountIsFixed) {
      setDiscountedPrice(parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01));
      setDiscountedPriceModal(false);
    }

    if (discountIsFixed && (parseFloat(discountFixedAmmount) < 1 || parseFloat(discountFixedAmmount) > parseFloat(price))) {
      return alert('Invalid Ammount! (1 - ' + price + ')');
    } else if (discountIsFixed) {
      setDiscountedPrice(parseFloat(price) - parseFloat(discountFixedAmmount));
      setDiscountedPriceModal(false);
    }
  };

  const getDiscountText = () => {
    // console.log(discountFixedAmmount);
    if (discountIsFixed) { // Fixed
      if (discountFixedAmmount == 0) {
        return 'Set Discount';
      } else {
        return discountFixedAmmount;
      }
    } else { // Percentage
      if (discountPercentageAmmount == 0) {
        return 'Set Discount ' + "%";
      } else {
        return discountPercentageAmmount + "%";
      }
    }
  }

  const renderStock = () => {
    if (!hasVariation) {
      return (
        <View style={sell.white}>
          <View style={sell.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginLeft: 25 }} />
              <FastImage source={require('../../assets/internal/icon_stock.png')} style={sell.sideIcon} />
              <Text style={sell.inputText}>Stock</Text>
            </View>
            <Item style={sell.input_title}>
              <Input
                style={sell.group_input_text}
                placeholder={'Stock'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(stock) => setStock(stock)}
                value={stock}
              />
            </Item>
          </View>
        </View>
      )
    }
  }

  //Condition Modal
  const onPressButton = (value) => {
    setConditionText(value);
    setCondition(value);
  };

  const conditionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={conditionModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setConditionModal(!conditionModal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.conditionModalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    onPressButton('New');
                    setConditionModal(!conditionModal);
                  }}
                >
                  New
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    onPressButton('Used ( Like new )');
                    setConditionModal(!conditionModal);
                  }}
                >
                  Used ( Like new )
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderPackageModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible2} style={styles.modalCon}>
          {_renderPackagingModal()}
        </Modal>
      </View>
    );
  };

  const _renderPackagingModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible2(!modalVisible2);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.PackagingModalConatiner}>
            <Text style={sell.PackagingModalHeaderText}>Packaging Size</Text>
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Width (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 0;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[0].width.toString().toLowerCase() == "nan" ? null : packaging_size[0].width.toString()}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Length (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 1;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[1].length.toString().toLowerCase() == "nan" ? null : packaging_size[1].length.toString()}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Height (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 2;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[2].height.toString().toLowerCase() == "nan" ? null : packaging_size[2].height.toString()}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.SizeNote}>Note: Use packaging size, not the product size</Text>

            <View style={sell.space} />
            <View style={sell.space} />

            {_renderPackagingSaveButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const _renderPackagingSaveButton = () => {
    if (
      packaging_size[0].width > 0 &&
      packaging_size[1].length > 0 &&
      packaging_size[2].height > 0
    ) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            setModalVisible2(false),
              setToSendPackagingSize(packaging_size)
            // console.warn(to_send_packagingSize)
          }}
          style={sell.SaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity >
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() =>
            setModalVisible2(false)
          }
          disabled
          style={sell.disableSaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }
  };

  const _handlePackagingInput = (index, value) => {
    var data = [...packaging_size];
    if (index === 0) {
      data.splice(index, 1, { width: parseFloat(value) });
    }
    if (index === 1) {
      data.splice(index, 1, { length: parseFloat(value) });
    }
    if (index === 2) {
      data.splice(index, 1, { height: parseFloat(value) });
    }
    setPackagingSize(data);
    // console.log(packaging_size);
  };

  const sortShippingFee = () => {
    if (shippingFeeOptions.length == 0) return 'Set Shipping Fee';
    let sorted = shippingFeeOptions.sort(function (a, b) {
      return a.shipping_fee - b.shipping_fee;
    });
    return sorted[0].shipping_fee + ' - ' + sorted[sorted.length - 1].shipping_fee;
  };

  const handleSubmit = async () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // reset all errors to false
    fieldErrors.map((error) => {
      error = false;
    });

    // If Image Empty
    if (images.length <= 0) {
      fieldErrors.imageEmpty = true;
      submitCancelled = true;
    }

    // If Image more than 5mb
    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
        submitCancelled = true;
      }
    });

    // If Name Empty
    if (!name.trim()) {
      fieldErrors.nameEmpty = true;
      submitCancelled = true;
    }

    // If Description Empty
    if (!description.trim()) {
      fieldErrors.descriptionEmpty = true;
      submitCancelled = true;
    }

    // If Commision Empty
    // console.log("referrerFixedCommission", referrerFixedCommission)
    if (commissionIsFixed) {
      if (!referrerFixedCommission.trim()) {
        fieldErrors.commissionEmpty = true;
        submitCancelled = true;
      }
    } else {
      if (!referrerPercentageCommission.trim()) {
        fieldErrors.commissionEmpty = true;
        submitCancelled = true;
      }
    }

    // console.log("category_id", category_id);
    // If Main Category Empty
    if (category_id == '' || category_id == null) {
      fieldErrors.mainCategoryEmpty = true;
      submitCancelled = true;
    }

    if (!hasVariation) {
      if (price === 0 || price == null) {
        fieldErrors.price = true;
        submitCancelled = true;
      }

      if (stock === 0 || stock == null) {
        fieldErrors.stock = true;
        submitCancelled = true;
      }
    }

    if (hasDiscount) {
      if (discountIsFixed) {
        if (discountFixedAmmount === 0 || discountFixedAmmount == null) {
          fieldErrors.price = true;
          submitCancelled = true;
        }
      } else {
        if (discountPercentageAmmount === 0 || discountPercentageAmmount == null) {
          fieldErrors.price = true;
          submitCancelled = true;
        }
      }
    }

    // If Required attributes are empty
    // console.log("checking categoryAttributes");
    categoryAttributes.map((attribute, index) => {
      // console.log("checking", attribute.attribute_name);
      if (!attribute?.value?.trim() && attribute.is_required == "yes") {
        // console.log(attribute.attribute_name, "is empty");
        submitCancelled = true;
      }
    });

    // If hasVariation and variationOptions Empty
    if (hasVariation) {
      if (variationOptions.length <= 0 || variationOptions === null) {
        fieldErrors.variationOptions = true;
        submitCancelled = true;
      }
    }

    // If Condition Empty
    if (!weight.trim()) {
      fieldErrors.weightEmpty = true;
      submitCancelled = true;
    }

    // If Packaging Width Empty
    if (packaging_size[0].width == '' || packaging_size[0].width <= 0 ||
      packaging_size[1].length == '' || packaging_size[1].length <= 0 ||
      packaging_size[2].height == '' || packaging_size[2].height <= 0) {
      fieldErrors.packageSizeEmpty = true;
      submitCancelled = true;
    }

    // If Condition Empty
    if (!condition.trim()) {
      fieldErrors.conditionEmpty = true;
      submitCancelled = true;
    }

    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    // console.log(JSON.stringify(categoryAttributes));
    // console.log("attribute.value", categoryAttributes[0].value)
    // console.log("attribute.attribute_value", categoryAttributes[0].attribute_value)
    // console.log("attribute.attribute", categoryAttributes[0].attribute)
    // console.log("attribute.attribute_name", categoryAttributes[0].attribute_name)
    // console.log("attribute.required", categoryAttributes[0].required)
    // console.log("attribute.is_required", categoryAttributes[0].is_required)
    // console.log("attribute.category_id", categoryAttributes[0].category_id)
    // console.log("~~~~~~~~~~~~~~~~~~")
    // console.log("user", user);
    console.log("variationOptions", variationOptions);

    updateProductData.pid = route.params.id;
    updateProductData.name = name;
    updateProductData.commissionIsFixed = commissionIsFixed;
    updateProductData.referrerFixedCommission = referrerFixedCommission;
    updateProductData.referrerPercentageCommission = referrerPercentageCommission;
    updateProductData.to_send_attributes = categoryAttributes;
    updateProductData.category_id = parseInt(category_id);
    updateProductData.description = description;
    updateProductData.condition = condition;
    updateProductData.warranty = warranty;
    updateProductData.country_code = user.data.country_code;
    updateProductData.is_recurring = route.params.is_recurring;
    updateProductData.hasVariation = hasVariation;
    updateProductData.variationOptions = variationOptions;
    updateProductData.price = parseFloat(price);
    updateProductData.hasDiscount = hasDiscount;
    if (hasDiscount) {
      if (discountIsFixed) { // Old Price divided by Fixed Discount is = to Percntage Discount
        updateProductData.discountFixedAmmount = discountFixedAmmount;
        updateProductData.discountPercentageAmmount = price / discountFixedAmmount
      } else {// Old Price divided by Fixed Discount is = to Percntage Discount
        updateProductData.discountFixedAmmount = price * discountPercentageAmmount
        updateProductData.discountPercentageAmmount = discountPercentageAmmount;
      }
    }
    updateProductData.discountedPrice = parseFloat(discountedPrice);
    updateProductData.stock = parseInt(stock);
    updateProductData.weight = parseFloat(weight);
    updateProductData.freeShipping = freeShipping;
    updateProductData.to_send_packagingSize = to_send_packagingSize;
    updateProductData.shippingFeeOptions = shippingFeeOptions;
    updateProductData.forPickup = forPickup;
    updateProductData.ownPackaging = ownPackaging;
    updateProductData.images = images;

    // console.log("updateProductData", updateProductData)
    // console.log("updateProductData.pid", updateProductData.pid);
    // console.log("updateProductData.name", updateProductData.name);
    // console.log("updateProductData.commissionIsFixed", updateProductData.commissionIsFixed);
    // console.log("updateProductData.referrerFixedCommission", updateProductData.referrerFixedCommission);
    // console.log("updateProductData.referrerPercentageCommission", updateProductData.referrerPercentageCommission);
    // console.log("updateProductData.to_send_attributes", updateProductData.to_send_attributes);
    // console.log("updateProductData.category_id", updateProductData.category_id);
    // console.log("updateProductData.description", updateProductData.description);
    // console.log("updateProductData.condition", updateProductData.condition);
    // console.log("updateProductData.warranty", updateProductData.warranty);
    // console.log("updateProductData.variationOptions", updateProductData.variationOptions);
    // console.log("updateProductData.price", updateProductData.price);
    // console.log("updateProductData.stock", updateProductData.stock);
    // console.log("updateProductData.weight", updateProductData.weight);
    // console.log("updateProductData.to_send_packagingSize", updateProductData.to_send_packagingSize);
    // console.log("updateProductData.shippingFeeOptions", updateProductData.shippingFeeOptions);
    // console.log("updateProductData.images", updateProductData.images);
    // console.log("updateProductData.freeShipping", updateProductData.freeShipping);
    // console.log("updateProductData.ownPackaging", updateProductData.ownPackaging);
    // console.log("updateProductData.forPickup", updateProductData.forPickup);

    // if no errors then go
    setSubmitLoading(true);
    // setSubmitLoading(false);

    try {
      await Service.updateProduct(
        updateProductData,
        async (res) => {
          console.log('Response', JSON.stringify(res));
          if (res.status) {
            // Update Images
            // oldImagesimages.map((oldImage) => {
            //   let imageUnchanged = false;
            //   images.map((newImage) => {
            //     let uri = '';
            //     if (newImage.uri == null) {
            //       let uri = newImage.isCaptured ? newImage.uri : newImage.fullpath;
            //     } else {
            //       uri = newImage.uri
            //     }

            //     if (oldImage.uri == uri) {
            //       imageUnchanged == true;
            //     } else {

            //     }

            //   });

            //   if (!imageUnchanged)
            // });

            // Add Images
            images.map(async (newImage, index) => {
              console.log('addProductImage~~~~~~')
              let imageData = [];
              imageData.file = newImage;
              imageData.name = name;
              imageData.pid = route.params.id;

              await Service.addProductImage(
                imageData,
                (res) => {
                  console.log('Service.addProductImage Success: ', res)

                  // Remove Images
                  if ((images.length - 1) == index) {
                    oldImages.map(async (oldImage) => {
                      let imageData = [];
                      imageData.fid = oldImage.id;
                      imageData.pid = route.params.id;

                      await Service.removeProductImage(
                        imageData,
                        (res) => {
                          console.log('Service.removeProductImage Success: ', res)

                        },
                        (err) => {
                          setSubmitLoading(false);
                          Alert.alert('Error', err.message)
                          console.log('Service.removeProductImage Error 1', err)
                        }
                      )
                    });

                  }
                },
                (err) => {
                  setSubmitLoading(false);
                  Alert.alert('Error', err.message)
                  console.log('Service.addProductImage Error 1', err)
                }
              )
            });


            Alert.alert(
              'Succesfully!',
              res.msg,
              [
                {
                  text: 'OK',
                  onPress: async () => {
                    // await fetchProduct();
                    // console.log('Service.updateProduct res', res)
                    setSubmitLoading(false);
                    // await route.params.onBackPress();
                    await navigation.goBack();
                  },
                },
              ],
              { cancelable: false },
            );
          } else {
            setSubmitLoading(false);
            Alert.alert(res.status, res.msg);
            console.log('Service.updateProduct Error 3', res)
          }
        },
        (err) => {
          setSubmitLoading(false);
          Alert.alert('Error', err.message)
          console.log('Service.updateProduct Error 2', err)
        },
      );
    } catch (error) {
      setSubmitLoading(false);
      Alert.alert('Upload Error', 'Please try again');
      console.log('Service.updateProduct Error 1', error)
    }
  };

  const handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={submitLoading}
        onRequestClose={() => {
          // console.log('Modal has been closed.');
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              // backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  const getCommissionText = () => {
    // Fixed
    if (commissionIsFixed) {
      if (referrerFixedCommission == '') {
        return 'Set Commission';
      } else {
        return referrerFixedCommission;
      }
    } else { // Percentage
      if (referrerPercentageCommission == '') {
        return 'Set Commission ' + "%";
      } else {
        return parseInt(referrerPercentageCommission).toString() + "%";
      }
    }
  }

  const renderImageError = () => {
    if (submitTapped) {
      if (images.length <= 0) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Atleast 1 Image is required.
            </Text>
          </View>
        );
      } else if (fieldErrors.imageMorethan5mb) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * The maximum size for an image is 5mb
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderNameError = () => {
    if (submitTapped) {
      if (!name.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Name is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderDescriptionError = () => {
    if (submitTapped) {
      if (!description.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Description is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderCommissionError = () => {
    if (submitTapped) {
      if (commissionIsFixed) {
        if (!referrerFixedCommission.trim()) {
          return (
            <View style={sell.fieldErrors}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * Referrer Commission is required.
              </Text>
            </View>
          );
        }
      } else {
        if (!referrerPercentageCommission.trim()) {
          return (
            <View style={sell.fieldErrors}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * Referrer Commission is required.
              </Text>
            </View>
          );
        }
      }
    }
    return <View />;
  };

  const renderMainCategoryError = () => {
    if (submitTapped) {
      if (category_id == '' || category_id == null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Main Category is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderAttributeErrors = (currentAttribute) => {
    let tmp = attributeTmp;

    return categoryAttributes.map((attribute, index) => {
      if (submitTapped) {
        if (!attribute?.value?.trim() && attribute.is_required == "yes" && currentAttribute == attribute) {
          return (
            <View style={sell.fieldErrors} key={index}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * {attribute.attribute_name} is required.
              </Text>
            </View>
          );
        }
      }
      return <View />;
    });
  };

  const renderVariationError = () => {
    if (submitTapped && hasVariation) {
      if (variationOptions.length <= 0 || variationOptions === null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Variations are required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderWeightError = () => {
    if (submitTapped) {
      if (weight == '' || weight == null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Weight is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderPriceError = () => {
    if (submitTapped && !hasVariation) {
      if (price === 0 || price == null) {
        return (
          <View style={[sell.fieldErrors, { paddingTop: 0 }]}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Price is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderStockError = () => {
    if (submitTapped && !hasVariation) {
      if (stock === 0 || stock === null) {
        return (
          <View style={[sell.fieldErrors, { paddingTop: 0 }]}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Stock is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderDiscountedPriceError = () => {
    if (submitTapped && hasDiscount) {
      if (discountedPrice === 0 || discountedPrice == null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Discounted Price is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderPackageSizeError = () => {
    if (submitTapped) {
      if (packaging_size[0].width == '' || packaging_size[0].width <= 0 ||
        packaging_size[1].length == '' || packaging_size[1].length <= 0 ||
        packaging_size[2].height == '' || packaging_size[2].height <= 0) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * All Packaging Fields are required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderConditionError = () => {
    if (submitTapped) {
      if (!condition.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Condition is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  return (
    <Root>
      <Container style={sell.container}>
        <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
          <Left>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <FastImage source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} resizeMode={FastImage.resizeMode.contain} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={sell.headerText}> Edit Product </Text>
          </Body>
          <Right>
            <View style={sell.rightView} />
          </Right>
        </Header>
        {handleLoading()}
        <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
          <View>

            {renderPhotoModal()}

            {renderImageError()}
            <View style={sell.uploadContainer}>
              <ScrollView horizontal={true} scrollEnabled={true} style={{ width: width / 2 - 2 }}>
                {handleImagePicker()}
                {handleUIAddPhoto()}
              </ScrollView>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {renderNameError()}
            <View style={sell.whiteName}>
              <View style={{ flexDirection: 'row', height: hp('5.8%') }}>
                <View style={{ flexDirection: 'column', width: wp('90%') }}>
                  <Input
                    style={{ fontSize: hp('1.7%'), marginLeft: 15 }}
                    onChangeText={(name) => setName(name)}
                    value={name}
                    placeholder='Product Name'
                    placeholderTextColor='#7f8c8d'
                    maxLength={50}
                  />
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                  <Text style={sell.Length50}>{name.length}/50</Text>
                </View>
              </View>
            </View>

            {renderDescriptionError()}
            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(description) => setDescription(description)}
                    value={description}
                    placeholder='Description'
                    placeholderTextColor='#7f8c8d'
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />


            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Referer Commission</Text>
            </View>

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Commission  Type</Text>
                  </View>
                  {/* <View style={{ flexDirection: 'row' }}>
                    <Text style={[sell.hintText2, { marginLeft: 100 }]}>{commissionIsFixed ? "Fixed" : "Percentage"}</Text>
                  </View> */}
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={commissionIsFixed}
                      mode='dropdown'
                      style={[sell.datePicker, {}]}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',

                      }}
                      onValueChange={(value) => {
                        setCommissionIsFixed(value);
                      }}
                    >
                      <Picker.Item label='% Percentage' value={false} />
                      <Picker.Item label='Fixed' value={true} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {renderCommissionError()}
            <View style={sell.whiteBorder}>
              {renderReferrerModal()}
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => {
                  setModalVisible1(true);

                  // console.log("images:", images);
                  // Edit Product product_images: [{"uri": "https://www.referapps.com/storage/products/20210804/BjHx1SuFC3ijYDKXWosn080420210254.jpg"}]
                }}
                style={sell.inputContainer}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Referer Commission</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>
                      {getCommissionText()}
                    </Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Categories</Text>
            </View>

            {/* MAIN CATEGORY */}
            {renderMainCategoryError()}
            {renderCategoriesModal()}
            <View style={sell.whiteBorder}>
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => {
                  setCategoriesModal(true);
                }}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_category.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Category</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{category ? category : 'Set Category'}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {/* SUB CATEGORY */}
            {renderSubCategoriesModal()}
            {category && subCategoriesList.length > 0 ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    setSubCategoriesModal(true);
                  }}
                >
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Sub Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {subcategory ? subcategory : 'Set Sub Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            {/* SPECIFIC CATEGORY */}
            {renderSpecificCategoriesModal()}
            {subcategory && specificCategoriesList.length > 0 ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity activeOpacity={0.4} onPress={() => {
                  setSpecificCategoryModal(true)
                }}>
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Specific Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {specificcategory ? specificcategory : 'Set Specific Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            {/* ATTRIBUTES */}
            {categoryAttributes.length > 0 ? renderCategoryAttributes() : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {/* VARIATIONS */}
            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Variations</Text>
            </View>

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Has Variation</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={hasVariation}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setHasVariation(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {hasVariation ?
              <>
                {renderVariationError()}
                <View style={sell.white}>
                  <TouchableOpacity
                    style={sell.inputContainer}
                    activeOpacity={0.4}
                    onPress={() =>
                      navigation.navigate('ProductVariations', {
                        variationOptions,
                        onAdd: (variations) => {
                          console.log("variations", variations)
                          setVariationOptions(variations);
                        },
                      })}
                  >
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 25 }} />
                      <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} />
                      <Text style={sell.inputText}>Variations</Text>
                    </View>

                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>Set Variations</Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </>
              : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {!hasVariation ?
              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Price and Inventory</Text>
              </View>
              : null}

            {/* PRICE */}
            {renderPriceError()}
            {renderPrice()}

            {/* HAS DISCOUNT */}
            {!hasVariation ?
              <View style={[sell.whiteBorder, { marginBottom: iosMargin, marginTop: iosMargin },]}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    if (price == 0) {
                      Toast.show({
                        text: 'Set your product Price first',
                      });
                    }
                  }}
                  style={sell.inputPickerContainer}
                >
                  <View style={sell.inputPickerContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 25 }} />
                      <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                      <Text style={sell.inputText}>Has Discount</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }} pointerEvents={price == 0 ? "none" : "auto"}>
                      <Picker
                        selectedValue={hasDiscount}
                        mode='dropdown'
                        style={sell.datePicker}
                        itemStyle={{
                          // fontFamily: 'Roboto',
                          fontSize: 12,
                          color: '#231f20',
                        }}
                        onValueChange={(value) => {
                          setHasDiscount(value);
                          if (price == 0) {
                            Toast.show({
                              text: 'Set your product Price first',
                            });
                          }
                        }}
                      >
                        <Picker.Item label='Yes' value={true} />
                        <Picker.Item label='No' value={false} />
                      </Picker>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              : null}

            {/* DISCOUNT SERIES */}
            {renderDiscountedPriceModal()}
            {renderDiscountType()}
            {renderDiscountedPriceError()}
            {renderDiscountAmmount()}
            {renderDiscountedPrice()}

            {/* STOCK */}
            {renderStockError()}
            {renderStock()}

            {!hasVariation ?
              <View>
                <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
                <View style={sell.space} />
                <View style={sell.space} />
                <View style={sell.space} />
              </View>
              : null}

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Shipping</Text>
            </View>

            {/* WEIGHT */}
            {renderWeightError()}
            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_weight.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Weight in</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.weight_group_input_text}
                    placeholder={'Weight in '}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'number-pad'}
                    value={parseInt(weight).toString().toLowerCase() == "nan" ? '' : parseInt(weight).toString()}
                    onChangeText={(weight) => setWeight(weight)}
                  />
                  <View style={{ paddingRight: 30 }}>
                    <Text style={sell.Kg}>Kg</Text>
                  </View>
                </Item>
              </View>
            </View>

            {/* PACKAGING SIZE */}
            {renderPackageSizeError()}
            <View style={sell.white}>
              {renderPackageModal()}
              <TouchableOpacity onPress={() => setModalVisible2(true)} style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_size.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Packaging Size</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text numberOfLines={1} style={[sell.size_group_input_text, { width: width - 200 }]}>
                    {packaging_size[0].width > 0 &&
                      packaging_size[0].width.toString() +
                      'cm X ' +
                      packaging_size[1].length.toString() +
                      'cm X ' +
                      packaging_size[2].height.toString() +
                      'cm'}
                    {!packaging_size[0].width > 0 && 'Packaging Size'}
                  </Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            {/* FREE SHIPPING */}
            <View style={[sell.whiteBorder, { marginBottom: iosMargin, marginTop: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Free Shipping</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={freeShipping}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setFreeShipping(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {!freeShipping ? (
              <View style={sell.white}>
                <TouchableOpacity
                  onPress={() => {
                    if (!weight) {
                      Toast.show({
                        text: 'Set your product Weight first',
                      });
                    } else {
                      navigation.navigate('ShippingOptions', {
                        logistics: shipping_options,
                        shippingFeeOptions,
                        onDone: (options, logs) => {
                          setShippingFeeOptions(options);
                          setShippingOptions(logs);
                        }
                      });
                    }
                  }}
                  style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Shipping Fee</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{sortShippingFee()}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Own Packaging</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={ownPackaging}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setOwnPackaging(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/mapPin.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>For Pickup</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={forPickup}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setForPickup(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Others</Text>
            </View>

            {renderConditionError()}
            <View style={sell.white}>
              {conditionsModal()}
              <TouchableOpacity style={sell.inputContainer} onPress={() => setConditionModal(true)}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_condition.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Condition</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.condition_group_input_text}>{condition}</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(warranty) => setWarranty(warranty)}
                    value={warranty}
                    placeholder='Set Warranty Information'
                    placeholderTextColor={'#7f8c8d'}
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={{ paddingTop: 10, paddingBottom: 5 }}>
              <TouchableOpacity
                nativeOpacity={0.4}
                onPress={() =>
                  handleSubmit()
                }
                style={sell.nextButton}>
                <Text style={sell.SellText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    </Root >
  );
};
