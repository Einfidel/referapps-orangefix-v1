import * as React from 'react';
import { Text, TouchableOpacity, View, Dimensions, StyleSheet, Image, Alert, } from 'react-native';
import CustomHeader from '../../components/header';
import Assets from '../../components/assets.manager.js';
import FastImage from 'react-native-fast-image'

const { width, height } = Dimensions.get('window');

export default function HomeScreen(navigation) {
  return (
    <View>
      <CustomHeader title="Add New Item" navigation={navigation} />
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View style={styles.imgUploadContainer}>
          <FastImage
            source={Assets.product.upload_icon}
            style={styles.imgUpload}
            resizeMode={FastImage.resizeMode.contain}
          />
          <Text style={styles.txtNote}>To provide marketing tools to companies and individuals and create a perpetual income.</Text>
          <TouchableOpacity
            onPress={() => navigation.navigation.navigate('CreateProduct')}
            style={styles.btnCon}>
            <Text style={styles.txtBtn}>Product</Text>
          </TouchableOpacity>
          <View style={{ padding: 5, }} />
          <TouchableOpacity onPress={() => {
            // Alert.alert('Coming Soon!', 'This feature will be available soon.')
            navigation.navigation.navigate('CreateService')
          }} style={styles.btnCon}>
            <Text style={styles.txtBtn}>Service</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}


const styles = StyleSheet.create({
  header: {
    backgroundColor: '#ffffff',
  },
  txtHeader: {
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    fontFamily: 'roboto-bold'
  },
  imgUploadContainer: {
    // flex: 1,
    alignItems: 'center',
    marginTop: height / 2 - 180,
    justifyContent: 'center',
    alignSelf: 'center',
    // position:'absolute',
    // backgroundColor:'pink'
  },
  imgUpload: {
    // backgroundColor:'blue',
    width: 130,
    height: 130,
    resizeMode: 'contain'
  },
  txtNote: {
    // fontFamily:'Roboto',
    fontSize: 10,
    color: '#7f8c8d',
    textAlign: 'center',
    paddingBottom: 20,
    width: width / 1.4,
    paddingVertical: 25,
  },
  btnCon: {
    backgroundColor: '#f36e23',
    borderWidth: .7,
    alignSelf: 'center',
    borderRadius: 4,
    width: width / 2.3,
    paddingVertical: 3,
    borderColor: '#f36e23',
    shadowColor: '#383a3d',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  txtBtn: {
    fontSize: 13,
    color: '#ffffff',
    alignSelf: 'center',
    // fontFamily:'Roboto'
  }
})
