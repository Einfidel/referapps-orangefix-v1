import { StyleSheet, Platform, Dimensions } from 'react-native';

var width = Dimensions.get('window').width;
export default StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  pinImage: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginRight: 10,
  },
  headerText: {
    // fontFamily: 'roboto-bold',
    color: '#231f20',
    fontSize: 20,
    justifyContent: 'center',
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  detailsContainer: {
    flex: 1,
    paddingTop: 5,
  },
  productText: {
    // fontFamily: 'Roboto',
    color: '#231f20',
    alignSelf: 'flex-start',
    fontSize: 18,
    paddingLeft: 16,
    paddingRight: 16,
  },
  productPrice: {
    // fontFamily: 'Roboto',
    color: '#ea4123',
    fontSize: 13,
    paddingLeft: 16,
    paddingRight: 16,
    textDecorationLine: 'none',
  },
  productStock: {
    // fontFamily: 'Roboto',
    color: '#6F6F6F',
    fontSize: 13,
    paddingLeft: 16,
    paddingRight: 16,
    textDecorationLine: 'none',
    marginBottom: 4,
  },
  productPriceWithDiscount: {
    // fontFamily: 'Roboto',
    color: '#868686',
    fontSize: 13,
    paddingLeft: 16,
    paddingRight: 16,
    textDecorationLine: 'line-through',
  },
  discountPercentage: {
    // fontFamily: 'Roboto',
    color: '#fff',
    fontSize: 12,
    paddingHorizontal: 10,
    backgroundColor: '#FF7C62',
    borderRadius: 100,
    textAlignVertical: 'center',
  },
  detailText: {
    color: '#231f20',
    // fontFamily: 'Roboto',
    fontSize: 15,
    alignSelf: 'flex-start',
  },
  productDetailText: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 13,
    alignSelf: 'flex-start',
  },
  bottomTabImage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  bottomTabText: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: '#ffffff'
  },
  bottomTabBuyText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    color: '#ffffff'
  },
  space: {
    paddingTop: 5,
  },
  border: {
    height: .7,
    width: width,
    backgroundColor: '#d0dbdb',
  },


  //New styles
  headCon: {
    backgroundColor: '#ffffff',
    ...Platform.select({
      ios: {
        alignItems: 'center',
        paddingBottom: 15,
        height: 60
      },
      android: {
        // marginTop: 10
      },
    })
  },
  leftCon: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgBackCon: {
    marginLeft: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgBack: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  rightCon: {
    flex: 0,
    flexDirection: 'row',
  },
  cartCon: {
    justifyContent: 'space-around',
    flex: 0,
    width: 34,
    height: 34,
    borderRadius: 17,
    alignItems: 'center',
  },
  cartImg: {
    alignSelf: 'center',
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: '#f36e23',
  },
  pinnedCon: {
    justifyContent: 'space-around',
    flex: 0,
    width: 34,
    height: 34,
    borderRadius: 17,
    alignItems: 'center',
  },
  imgPin: {
    alignSelf: 'center',
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  unpinCon: {
    justifyContent: 'space-around',
    flex: 0,
    width: 34,
    height: 34,
    borderRadius: 17,
    alignItems: 'center',
  },
  imgUnpin: {
    alignSelf: 'center',
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  modals: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  modalCon: {
    flex: 1,
    justifyContent: 'flex-end',
    backgroundColor: 'rgba(0,0,0,0.5)',
    opacity: 1
  },
  animate: {
    backgroundColor: '#FFF',
    width: width,
  },
  modCon: {
    flexDirection: 'row',
    // paddingTop:10,
    // // paddingHorizontal:15,
    // alignItems: 'center',
    // justifyContent: 'space-between',
  },
  imgProduct: {
    marginLeft: 20,
    height: 90,
    width: 80,
    backgroundColor: '#ffffff'
  },
  txtProductName: {
    // fontFamily: 'roboto.medium',
    fontSize: 14,
    color: '#231f20',
    // height:40,
    width: 150,
    maxHeight: 35,
  },
  txtCountryCode: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#ea4123',
  },
  txtStock: {
    // fontFamily: 'Roboto',
    fontSize: 8,
    color: '#7f8c8d',
    marginTop: 2
  },
  txtStocks: {
    // fontFamily: 'Roboto',
    fontSize: 8,
    color: '#7f8c8d',
    maxHeight: 30,
    marginTop: 2
  },
  btnExit: {
    width: 25,
    height: 25,
    borderRadius: 12.5,
    alignItems: 'center',
    alignSelf: 'flex-start',
    margin: 5,
    // paddingVertical:10,
  },
  imgExit: {
    width: 14,
    height: 14,
    tintColor: '#231f20',
  },
  line: {
    borderTopWidth: .7,
    width: width,
    borderColor: '#ecf0f1',
  },
  countCon: {
    width: '35%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignSelf: 'center',
    paddingVertical: 7
  },
  imgAdd: {
    width: 23,
    height: 23,
    tintColor: '#2580bc'
  },
  txtQuantity: {
    // fontFamily: 'roboto.medium',
    fontSize: 16,
    color: '#231f20',
  },
  btnAddCartCon: {
    backgroundColor: '#22b05e',
    paddingVertical: 5,
    width: 250,
    alignSelf: 'center',
    borderRadius: 15,
    marginVertical: 10
  },
  txtAddCart: {
    color: '#fff',
    // fontFamily:'Roboto',
    textAlign: 'center',
    fontSize: 12,
    paddingVertical: 2,
  },

  //Footer
  footerCon: {
    backgroundColor: 'orange',
    height: 60,
    borderTopColor: "transparent",
    shadowColor: '#47494c',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
    position: 'absolute',
    bottom: 0
  },
  chatCon: {
    justifyContent: 'space-around',
    width: '20%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  barrier: {
    width: .7,
    marginTop: 12,
    marginBottom: 12,
    borderLeftWidth: .7,
    borderColor: '#00a14b'
  },
  btnModalAddCon: {
    justifyContent: 'space-around',
    width: '20%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  btnBuynow: {
    width: '40%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#79b956'
  },

  //Render Reviews 
  rrCon: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    marginLeft: 16,
    marginRight: 16,
    width: width,
  },
  nameCon: {
    paddingLeft: 10,
    alignSelf: 'center',
    paddingRight: 16,
  },
  txtName: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
  },
  col: {
    width: '90%',
    paddingRight: 32,
  },
  txtDescription: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
    justifyContent: 'center',
  },
  img4: {
    resizeMode: 'contain',
    width: 120,
    height: 60,
    alignSelf: 'center',
  },

  //Render Reviewer Avatar
  fastImg: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'black',
    resizeMode: 'contain'
  },

  //Render Header
  headerCon: {
    backgroundColor: '#ffffff',
    borderBottomColor: 'transparent',
    shadowColor: '#47494c',
    // shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  con2: {
    backgroundColor: '#ffffff',
    paddingTop: 10,
    paddingBottom: 10,
  },
  starCon: {
    flex: 1,
    paddingTop: 2,
    paddingLeft: 16,
    paddingRight: 16,
    flexDirection: 'row',
  },
  txtrate: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
    paddingLeft: 5,
  },
  txtRatings: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
  },
  referrCommissionCon: {
    paddingHorizontal: 15,
    paddingVertical: 10,
    backgroundColor: '#f5f6fa',
  },
  optionBtn: {
    backgroundColor: '#f5f6fa',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderColor: "#00a14b",
    borderWidth: 1,
    borderRadius: 15,
    justifyContent: 'center',
    // marginTop: hp('2%'),
    // marginBottom: hp('1%'),
    paddingVertical: 1,
    paddingHorizontal: 8,
    marginHorizontal: 3,
    marginBottom: 5,
  },
  selectedOptionBtn: {
    backgroundColor: '#00a14b',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderColor: "#222",
    borderWidth: 1,
    borderRadius: 15,
    justifyContent: 'center',
    // marginTop: hp('2%'),
    // marginBottom: hp('1%'),
    paddingVertical: 1,
    paddingHorizontal: 8,
    marginHorizontal: 3,
    marginBottom: 5,
  },
  optionText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#444',
  },
  selectedOptionText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#fff',
  },
  txtReferr: {
    color: '#e58736',
    // fontFamily:'roboto-bold',
    fontSize: 15,
    paddingTop: 5
  },
  txtReferrQrCode: {
    color: '#e58736',
    // fontFamily:'roboto-bold',
    fontSize: 14,
    paddingTop: 5
  },
  txtReferrs: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#7f8c8d',
    width: width / 1.2
  },
  userimgCon: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 16,
    paddingRight: 16,
    height: 70,
    width: width,
    justifyContent: 'space-between',
    borderTopWidth: .7,
    borderColor: '#d0dbdb',
  },
  fastImgCon: {
    alignSelf: 'flex-start',
    flexDirection: 'row',
    paddingRight: 10
  },
  userimg: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  shpnameCon: {
    paddingLeft: 10,
    alignSelf: 'center',
  },
  txtVerify: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
  },
  btnViewCon: {
    borderWidth: .7,
    borderColor: '#7f8c8d',
    height: 30,
    width: 70,
    justifyContent: 'space-around',
  },
  txtViewShop: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
  },
  specificationsCon: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 16,
    paddingRight: 16,
    height: 50,
    width: width,
    justifyContent: 'space-between',
    borderTopWidth: .7,
    borderColor: '#d0dbdb',
  },
  specificationsCon2: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 16,
    paddingRight: 16,
    // height: 50,
    width: width,
    justifyContent: 'space-between',
    borderTopWidth: .7,
    borderColor: '#d0dbdb',
  },
  txtSpecifications: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#7f8c8d',
  },
  brandCon: {
    paddingLeft: 10,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  txtBrand: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
  },
  rightArrowCon: {
    height: 20,
    width: 60,
    justifyContent: 'space-around'
  },
  imgRight: {
    resizeMode: 'contain',
    alignSelf: 'flex-end',
    width: 13,
    height: 13,
  },
  logCon: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 16,
    paddingRight: 16,
    height: 50,
    width: width,
    justifyContent: 'space-between',
    borderTopWidth: .7,
    borderBottomWidth: .7,
    borderColor: '#d0dbdb',
  },
  imgLog: {
    resizeMode: 'contain',
    width: 15,
    height: 15,
  },
  shipCon: {
    paddingLeft: 10,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  txtShip: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#7f8c8d',
  },
  txtNumber: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
  },
  imgDownCon: {
    paddingLeft: 10,
    alignSelf: 'center',
  },
  imgDown: {
    resizeMode: 'contain',
    width: 13,
    height: 13,
  },
  desCon: {
    borderTopWidth: .7,
    borderBottomWidth: .7,
    backgroundColor: '#fff',
    borderColor: '#d0dbdb',
  },
  txtDes: {
    // fontFamily:'Roboto',
    fontSize: 12, width: width / 1.2
  },
  detailsCon: {
    paddingTop: 18,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  txtDetails: {
    width: width / 1.2,
    // fontFamily: 'Roboto',
    fontSize: 12, color: '#7f8c8d',
  },
  txtProductR: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
  },
  txtView: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#f36e23',
  },
  reviewsCon: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 16,
    paddingRight: 16,
    height: 50,
    width: width,
    justifyContent: 'space-between',
  },
  renderReview: {
    borderTopWidth: .7,
    borderBottomWidth: .7,
    borderColor: '#d0dbdb',
    backgroundColor: '#ffffff',
  },
  rrContainer: {
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtSame: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  viewAll: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: '#ff1a1a',
  }







});
