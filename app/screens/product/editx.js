import React from 'react';
import { useState, useEffect } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import { Container, Header, Left, Body, Right, Item, Input } from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
// import { connect } from 'react-redux';
import { styles, sell } from './styles';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';

const { width, height } = Dimensions.get('window');

export default function EditProductScreen(navigation) {
  const [modal1, setmodal1] = useState(false);
  const [modal2, setmodal2] = useState(false);
  const [refermodal, setrefermodal] = useState(false);
  const [packageModal, setpackagemodal] = useState(false);
  const [conditionmodal, setconditionmodal] = useState(false);
  const [packaging_size, setpackaging_size] = useState([
    {
      width: '',
    },
    {
      length: '',
    },
    {
      height: '',
    },
  ]);

  const [name, setname] = useState('');
  const [description, setdescription] = useState('');
  const [price, setprice] = useState('');
  const [stockVal, setstock] = useState('');
  const [weight, setweight] = useState('');
  const [to_send_packagingSize, setto_send_packagingSize] = useState([]);
  const [condition, setcondition] = useState('');
  const [warranty, setwarranty] = useState('');
  const [referrer_commission, setreferrercomission] = useState('');
  const [lastCommission, setlastcommission] = useState('');
  const [text, settext] = useState('');
  const [value, setvalue] = useState('');
  const [modalVisible2, setModalVisible2] = useState(false)
  const [product_image, setProductImage] = useState([{
    uri: {}
  }])
  const [category, setCategory] = useState('')
  const [categoriesModal, setcategoriesModal] = useState(false)
  const [subcategory, setsubcategory] = useState('')
  const [specificcategory, setspecificcategory] = useState('')
  const [subCategoriesModal, setsubCategoriesModal] = useState(false)
  const [subCategories, setsubCategories] = useState([])
  const [userProfile, setProfile] = useState({})
  const [conditionModal, setconditionModal] = useState(false)
  const [copyImg, setCopyImg] = useState([
    {
      uri: {}
    }
  ])

  const init = async () => {
    let user = await AsyncStorage.getItem('user_data');
    let userData = user ? JSON.parse(user) : null;
    setProfile(userData)
    console.warn('props', userData)
    if (userData) {
      await Service.getProductDetails(
        navigation.route.params.id,
        (res) => {
          console.warn('PRODUCT DETAILS', res.data);
          setname(res.data.product_name)
          setdescription(res.data.info.data.description)
          setprice(res.data.info.data.price)
          setstock(res.data.info.data.inventory.stock)
          setweight(res.data.info.data.shipping_info.weight)
          // setpackaging_size([res.data.info.data.shipping_info.width + '' + cm, res.data.info.data.shipping_info.length + '' + cm, res.data.info.data.shipping_info.height + '' + cm])
          setProductImage([{
            uri: res.data.info.data.image.full_path
          }])
          setmodal1(res.data.referrer_commission)
          setCategory(res.data.category)
        },
        (err) => console.log(err)
      );
    }
    console.warn('ada', stockVal)
  };

  useEffect(() => {
    console.warn('pinasa', navigation.route.params)
    console.log('logs', navigation.route.params)
    init();
  }, []);

  const handleInputChange = (text) => {
    setreferrercomission(text.replace(/[^0-9]/g, ''));
  };

  const renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modal1} style={styles.modalCon}>
          {renderReferrModal()}
        </Modal>
      </View>
    );
  };

  const renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setmodal1(!modal1);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.ReferrerModalConatiner}>
            <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
            <Item>
              <Input
                style={sell.SetCommission}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                value={referrer_commission}
                onChangeText={(text) => handleInputChange(text)}
                maxLength={2}
              />
              <Text style={sell.percentText}>%</Text>
            </Item>
            <TouchableOpacity activeOpacity={1} onPress={() => renderReferrerSaveButton()} style={sell.referrerSaveBtn}>
              <Text style={sell.SaveText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderReferrerSaveButton = () => {
    if (referrer_commission < 5 || referrer_commission > 50) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      setrefermodal(false);
      setmodal1(false);
    }
  };

  const onPressButton = (value) => {
    this.setState({ value });
  };

  const setConditionModal = (visible) => {
    setconditionModal(visible);
  };

  const showCategory = () => {
    setcategoriesModal(true)
    setsubcategory('')
    setspecificcategory('')
  }

  const conditionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={conditionmodal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setconditionmodal(!conditinmodal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.conditionModalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    // this.onPressButton('New');
                    setconditionmodal(!conditinmodal);
                    // this.setConditionModal(!this.state.conditionModal);
                  }}
                >
                  New
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    // this.onPressButton('Used ( Like new )');
                    // this.setConditionModal(!this.state.conditionModal);
                    setconditionmodal(!conditinmodal);
                  }}
                >
                  Used ( Like new )
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderPackageModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modal2} style={styles.modalCon}>
          {renderPackagingModal()}
        </Modal>
      </View>
    );
  };

  const renderPackagingModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible2(!modalVisible2)
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.PackagingModalConatiner}>
            <Text style={sell.PackagingModalHeaderText}>Packaging Size</Text>
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Width (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 0;
                  var value = x;
                  handlePackagingInput(index, value);
                }}
                value={packaging_size[0].width}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Length (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 1;
                  var value = x;
                  handlePackagingInput(index, value);
                }}
                value={packaging_size[1].length}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Height (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 2;
                  var value = x;
                  handlePackagingInput(index, value);
                }}
                value={packaging_size[2].height}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.SizeNote}>Note: Use packaging size, not the product size</Text>

            <View style={sell.space} />
            <View style={sell.space} />

            {renderPackagingSaveButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderPackagingSaveButton = () => {
    if (packaging_size[0].width.length > 0 && packaging_size[1].length.length && packaging_size[2].height.length) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            setpackagemodal(false);
            setmodal2(false);
            setto_send_packagingSize(packaging_size);
          }}
          style={sell.SaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => {
            setpackagemodal(false);
            setmodal2(false);
          }}
          disabled
          style={sell.disableSaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }
  };

  const handlePackagingInput = (index, value) => {
    var data = [...packaging_size];
    if (index === 0) {
      data.splice(index, 1, { width: value });
    }
    if (index === 1) {
      data.splice(index, 1, { length: value });
    }
    if (index === 2) {
      data.splice(index, 1, { height: value });
    }
    setpackaging_size(data);
    console.log(packaging_size);
  };

  const handleSubCategoriesModal = () => {
    setsubCategoriesModal(!subCategoriesModal)
  }

  const handleSaveChanges = async () => {
    let catID = navigation.route.params.info.data.category_id
    await Service.updateAccountService({
      service: {
        productName: navigation.route.params.info.data.product_name,
        description: description,
        price: price,
        referrer_commission: referrer_commission,
      },
      images: product_image,
      uid: userProfile.data.id,
      pid: navigation.route.params.id,
      cid: catID,
      weight: weight,
      category: navigation.route.params.info.data.category,
      stock: stockVal,
    },
      (res) => {
        if (res.status) {
          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  await init();
                  await navigation.route.params.onBackPress();
                  await navigation.navigation.goBack();
                },
              },
            ],
            { cancelable: false }
          );
        } else {
          alert(res.msg);
          return false;
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  // const renderSubCategoriesModal = (data) => {
  //   return (
  //     <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
  //       <Modal
  //         animationType='slide'
  //         transparent={false}
  //         visible={!subCategoriesModal}
  //         style={{ backgroundColor: 'red' }}
  //       >
  //         <TouchableWithoutFeedback
  //           onPress={handleSubCategoriesModal}
  //           style={{ backgroundColor: 'red' }}
  //         >
  //           <View style={{ ...styles.containerModal }}>
  //             <View style={sell.PackagingModalConatiner}>
  //               <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
  //               <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
  //                 <View style={{ padding: 10 }} />
  //                 {subCategories.map((cat, i) => {
  //                   return (
  //                     <TouchableOpacity
  //                       onPress={async () => {
  //                         await Service.getSpecificCategory(
  //                           this.state.category_id,
  //                           (res) => {
  //                             this.setState({
  //                               subcategory: cat.category_name,
  //                               subCategoriesModal: false,
  //                               specificCategories: res.data,
  //                             });
  //                           },
  //                           (err) => console.log(err)
  //                         );
  //                       }}
  //                       key={i}
  //                       style={{ flex: 1, padding: 10 }}
  //                     >
  //                       <Text>{cat.category_name}</Text>
  //                     </TouchableOpacity>
  //                   );
  //                 })}
  //                 <View style={{ padding: 20 }} />
  //               </ScrollView>
  //             </View>
  //           </View>
  //         </TouchableWithoutFeedback>
  //       </Modal>
  //     </View>
  //   );
  // };

  return (
    <Container style={sell.container}>
      <Header style={{ backgroundColor: '#ffffff', marginTop: 30 }}>
        <Left style={{ flex: 1 }}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 0 }}>
          <Text style={sell.headerText}> Edit Product </Text>
        </Body>
        <Right style={{ flex: 1 }}>
          <View style={sell.rightView} />
        </Right>
      </Header>
      <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
        <View>
          <View style={sell.uploadContainer}>
            <ScrollView horizontal={true} scrollEnabled={true} style={{ width: width / 2 - 2 }}>
              <TouchableOpacity activeOpacity={1} onPress={() => alert('Under Development Stage')}>
                <FastImage source={product_image == null ? require('../../assets/Sell_Product-09.png') : { uri: product_image }} large style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }} />
              </TouchableOpacity>
            </ScrollView>
          </View>
          <View style={sell.whiteName}>
            <View style={{ flexDirection: 'row', height: hp('5.8%') }}>
              <View style={{ flexDirection: 'column', width: wp('90%') }}>
                <Input
                  style={{ fontSize: hp('1.7%'), marginLeft: 15 }}
                  onChangeText={(name) => setname(name)}
                  value={name}
                  placeholder='Product Name'
                  placeholderTextColor='#7f8c8d'
                  maxLength={50}
                />
              </View>
              <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                <Text style={sell.Length50}>{name.length}/50</Text>
              </View>
            </View>
          </View>

          <View style={sell.whiteDescription}>
            <View style={sell.descriptionContainer}>
              <View style={sell.DescriptionItem}>
                <TextInput
                  style={sell.descriptionText}
                  onChangeText={(description) => setdescription(description)}
                  value={description}
                  placeholder='Description'
                  placeholderTextColor='#7f8c8d'
                  editable={true}
                  multiline={true}
                />
              </View>
            </View>
          </View>

          <View style={sell.itemBorder} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.whiteBorder}>
            {renderReferrerModal()}
            <TouchableOpacity activeOpacity={0.4} onPress={() => setmodal1(true)} style={sell.inputContainer}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={sell.spaceLeft} />
                  <View style={sell.spaceLeft} />
                  <Text style={[sell.hintText, { marginLeft: 38 }]}>Referrer Commission</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>
                    {referrer_commission == '' ? 'Set Commission %' : referrer_commission + '%'}
                  </Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>

          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.whiteBorder}>
            <TouchableOpacity
              activeOpacity={0.4}
              onPress={() => { alert("Under development stage!") }}
            >
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 27 }} />
                  <FastImage source={require('../../assets/internal/icon_category.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Category</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>{category ? category : 'Set Category'}</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
          {/* {renderSubCategoriesModal()} */}
          <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.spaceLeft}>
            <Text style={sell.group_title}>Price and Inventory</Text>
          </View>

          <View style={sell.white}>
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 27 }} />
                <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Price</Text>
              </View>
              <Item style={sell.input_title}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'Price'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'decimal-pad'}
                  onChangeText={(x) => setprice(x)}
                  value={price}
                />
              </Item>
            </View>
          </View>

          <View style={sell.white}>
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 27 }} />
                <FastImage source={require('../../assets/internal/icon_stock.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Stock</Text>
              </View>
              <Item style={sell.input_title}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'Stock'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  onChangeText={(x) => setstock(x)}
                  value={stockVal}
                />
              </Item>
            </View>
          </View>

          <View style={sell.white}>
            <TouchableOpacity
              style={sell.inputContainer}
              activeOpacity={0.4}
              onPress={() => navigation.navigation.navigate('ProductVariations')}
            >
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Variations</Text>
              </View>

              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.hintText2}>Set Variations</Text>
                <View style={sell.spaceRight} />
                <FastImage source={require('../../assets/icon_sub.png')} style={sell.openIcon} tintColor={'#7f8c8d'} />
              </View>
            </TouchableOpacity>
          </View>

          <View style={sell.itemBorder} />

          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.spaceLeft}>
            <Text style={sell.group_title}>Shipping</Text>
          </View>

          <View style={sell.white}>
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_weight.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Weight in</Text>
              </View>
              <Item style={sell.input_title}>
                <Input
                  style={sell.weight_group_input_text}
                  placeholder={'Weight in '}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  onChangeText={(x) => setweight(x)}
                  value={weight}
                />
                <View style={{ paddingRight: 30 }}>
                  <Text style={sell.Kg}>Kg</Text>
                </View>
              </Item>
            </View>
          </View>

          <View style={sell.white}>
            {renderPackageModal()}
            <TouchableOpacity onPress={() => setModalVisible2(true)} style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_size.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Packaging Size</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.size_group_input_text}>
                  {packaging_size[0].width.length > 0 &&
                    packaging_size[0].width +
                    'cm X ' +
                    packaging_size[1].length +
                    'cm X ' +
                    packaging_size[2].height +
                    'cm'}
                  {!packaging_size[0].width.length > 0 && 'Packaging Size'}
                </Text>
                <View style={sell.spaceRight} />
                <FastImage source={require('../../assets/icon_sub.png')} style={sell.openIcon} tintColor={'#7f8c8d'} />
              </View>
            </TouchableOpacity>
          </View>

          <View style={sell.white}>
            <TouchableOpacity onPress={() => alert('Under Development Stage')} style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Shipping Fee</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.hintText2}>Set Shipping Fee</Text>
                <View style={sell.spaceRight} />
                <FastImage source={require('../../assets/icon_sub.png')} style={sell.openIcon} tintColor={'#7f8c8d'} />
              </View>
            </TouchableOpacity>
          </View>

          <View style={sell.itemBorder} />
          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.spaceLeft}>
            <Text style={sell.group_title}>Others</Text>
          </View>

          <View style={sell.white}>
            {conditionsModal()}
            <TouchableOpacity style={sell.inputContainer} onPress={() => setConditionModal(true)}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_condition.png')} style={sell.sideIcon} />
                <Text style={sell.inputText}>Condition</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.condition_group_input_text}>{value}</Text>
                <View style={sell.spaceRight} />
                <FastImage source={require('../../assets/icon_sub.png')} style={sell.openIcon} tintColor={'#7f8c8d'} />
              </View>
            </TouchableOpacity>
          </View>

          <View style={sell.whiteDescription}>
            <View style={sell.descriptionContainer}>
              <View style={sell.DescriptionItem}>
                <TextInput
                  style={sell.descriptionText}
                  onChangeText={(warranty) => setwarranty(warranty)}
                  value={warranty}
                  placeholder='Set Warranty Information'
                  placeholderTextColor={'#7f8c8d'}
                  editable={true}
                  multiline={true}
                />
              </View>
            </View>
          </View>

          <View style={sell.itemBorder} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={{ paddingTop: 10, paddingBottom: 5 }}>
            <TouchableOpacity
              avtiveOpacity={0.4}
              onPress={() =>
                handleSaveChanges()
                // console.warn('asd')
              }
              style={sell.nextButton}
            >
              <Text style={sell.SellText}>Save Changes</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </Container>
  );
}
