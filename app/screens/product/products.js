import React from 'react'
import { View, Text, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import { Container, Header, Thumbnail, Content, Left, Right } from 'native-base'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
const { width, height } = Dimensions.get('window')

const Recent = () => {
  return (
    <View style={{ backgroundColor: '#ffffff' }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 25 }}>
        <Thumbnail
          source={require('../../../assets/images/icon1.png')}
          style={{
            width: 100,
            height: 100,
            resizeMode: 'contain'
          }} />
        <View style={{ marginHorizontal: 20 }}>
          <Text>Poster</Text>
          <Text>P 50.00</Text>
          <Text>Stocks : 138</Text>
          <Text>Sold: 12</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', height: 55 }}>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>EDIT</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
const Popular = () => {
  return (
    <View style={{ backgroundColor: '#ffffff' }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 25 }}>
        <Thumbnail
          source={require('../../../assets/images/icon1.png')}
          style={{
            width: 100,
            height: 100,
            resizeMode: 'contain'
          }} />
        <View style={{ marginHorizontal: 20 }}>
          <Text>Poster</Text>
          <Text>P 50.00</Text>
          <Text>Stocks : 138</Text>
          <Text>Sold: 12</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', height: 55 }}>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>EDIT</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
const Soldout = () => {
  return (
    <View style={{ backgroundColor: '#ffffff' }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 25 }}>
        <Thumbnail
          source={require('../../../assets/images/icon1.png')}
          style={{
            width: 100,
            height: 100,
            resizeMode: 'contain'
          }} />
        <View style={{ marginHorizontal: 20 }}>
          <Text>Poster</Text>
          <Text>P 50.00</Text>
          <Text>Stocks : 138</Text>
          <Text>Sold: 12</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', height: 55 }}>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>EDIT</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
const Stock = () => {
  return (
    <View style={{ backgroundColor: '#ffffff' }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 25 }}>
        <Thumbnail
          source={require('../../../assets/images/icon1.png')}
          style={{
            width: 100,
            height: 100,
            resizeMode: 'contain'
          }} />
        <View style={{ marginHorizontal: 20 }}>
          <Text>Poster</Text>
          <Text>P 50.00</Text>
          <Text>Stocks : 138</Text>
          <Text>Sold: 12</Text>
        </View>
      </View>
      <View style={{ flexDirection: 'row', height: 55 }}>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>EDIT</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => alert('under development')}
          style={{ borderColor: '#EDEDEB', width: '50%', justifyContent: 'center', alignItems: 'center', borderTopWidth: 1.5, borderBottomWidth: 1.5, borderLeftWidth: 1.5, borderRightWidth: .5 }}>
          <Text style={{ fontSize: 15 }}>DELETE</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}
export default class YourProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'recent', title: 'Recent' },
        { key: 'popularity', title: 'Popular' },
        { key: 'soldout', title: 'Sold Out' },
        { key: 'stock', title: 'Stock' },
      ],
    }
  }

  _renderLabel = props => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d'

    return (
      <Text style={[{ fontSize: 12 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <Left style={styles.left}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={require('../../../assets/images/headericon/left-arrow-dark.png')}
                style={styles.backIcon} />
            </TouchableOpacity>
            <View style={styles.addVariation}>
              <Text style={styles.headerText}>Products</Text>
            </View>
          </Left>
          <Right style={{ right: 15, alignSelf: 'center' }}>
            <TouchableOpacity onPress={() => alert('Under Development')}>
              <Thumbnail
                source={require('../../../assets/images/internal/product/icon_chat.png')}
                style={styles.chatIcon}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              recent: Recent,
              popularity: Popular,
              soldout: Soldout,
              stock: Stock,
            })}
            renderTabBar={props =>
              <TabBar
                {...props}
                labelStyle={{
                  // fontFamily: 'regular', 
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 3, borderBottomColor: '#f36e23' }}
                indicatorContainerStyle={{ borderBottomWidth: 1, borderBottomColor: '#EDEDEB' }}
                style={{ backgroundColor: '#ffffff', elevation: 0 }}
                renderLabel={this._renderLabel(props)}
              />
            }
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left'
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  addVariation: {
    marginLeft: 25
  },
  chatIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },

})
