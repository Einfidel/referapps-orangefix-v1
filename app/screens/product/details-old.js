import React from 'react';
import { ScrollView, Text, TouchableOpacity, View, Dimensions, AlertIOS, StatusBar, Alert, Linking, Clipboard, Image, Platform, Modal, TouchableHighlight, TouchableWithoutFeedback, StyleSheet, } from 'react-native';
import { Header, Left, Body, Right, Thumbnail, Container, Footer, FooterTab } from 'native-base';
import { Col, Row } from 'react-native-easy-grid';
import Swiper from 'react-native-swiper';
import StarRating from 'react-native-star-rating';
import ImageZoom from 'react-native-image-pan-zoom';
import * as Animatable from 'react-native-animatable';
import FlatListComponent from '../../components/flatlist';
import RenderItem from '../../components/renderItem';
import styles from './styles2';
import FastImage from 'react-native-fast-image';
import Share from 'react-native-share';
import AsyncStorage from '@react-native-community/async-storage';
import HTML from 'react-native-render-html';
import { IGNORED_TAGS, alterNode, makeTableRenderer, defaultTableStylesSpecs, cssRulesFromSpecs } from 'react-native-render-html-table-bridge';
import WebView from 'react-native-webview';
import Endpoints from '../../components/api/endpoints';
import ImageViewer from 'react-native-image-zoom-viewer';
import QRCode from 'react-native-qrcode-svg';

import auth from '@react-native-firebase/auth';

const api_token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
const product_api = 'https://www.referapps.com/api/product/show.json?id=';
const pin_item_api = 'https://www.referapps.com/api/pin/create.json';
const unpin_item_api = 'https://www.referapps.com/api/pin/delete.json';
const user_products = 'https://www.referapps.com/api/product/user-products.json?';
const pinned_items_api = 'https://www.referapps.com/api/pin/all.json?';
const refer_api = 'https://www.referapps.com/api/product/refer.json';
const reviews_api = 'https://www.referapps.com/api/product/reviews.json?include=info,date';
const add_to_cart = 'https://www.referapps.com/api/cart/add.json';
const product_inventory = 'https://www.referapps.com/api/product/price-inventory.json';
const checkout_api = 'https://www.referapps.com/api/paypal/checkout.json?include=date,info';

const imgSample = require('../../assets/robot-dev.png')
const profile = require('../../assets/face0.jpg')

const MyStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[styles.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
);

const config = {
  WebViewComponent: WebView
};

// const renderers = {
//   table: makeTableRenderer(config)
// };

const cssRules = `
table {
  width: 100%;
  background-color: #FFF;
}
`

const htmlConfig = {
  alterNode,
  // renderers,
  ignoredTags: IGNORED_TAGS,
  cssRules
};


var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ProductScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    this.product_id = this.props.route.params.product_id
    this.state = {
      loggedIn: false,
      warranty: '',
      des: '   ',
      user_img: '',
      shp_name: '',
      shop: [],
      commission: 0,
      productLoaded: null,
      pinnedLoaded: true,
      user_productsLoaded: null,
      reviewsLoaded: null,
      product: [],
      user_products: [],
      product_name: '',
      product_images: [],
      reviews: [],
      refer_url: '',
      imageModal: false,
      referModal: false,
      specificationsModal: false,
      addToCartModal: false,
      modal_image: '',
      already_pinned: false,
      componentHeight: '',
      user_id: '',
      pin_id: 0,
      deliveryFeeLowest: 0,
      deliveryFeeHighest: 0,

      variation1_selected: null,
      variation2_selected: null,
      quantity: 1,
      stock: 0,
      fetchingStock: false,
      price: null,
      delivery_courier_slug: '',
      delivery_courier_fee: '',
      isSinglePurchase: false,

      modalVisible: false,
      referVisible: false,
      userData: {},
      productDetail: [],
      previewImages: [],
    }
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setReferModal(visible) {
    this.setState({ referModal: visible });
  }

  componentDidMount = async () => {
    // console.log('Params', JSON.stringify(this.props.route.params, null, '\t'));
    // let token = await AsyncStorage.getItem("auth")    
    const user = auth().currentUser
    let personData = await AsyncStorage.getItem('user_data')
    let res = JSON.parse(personData)
    this.setState({ userData: res })
    // console.log('propssss', this.state.userData)

    if (user == null) {
      this.setState({ loggedIn: false })
      // Alert.alert("Unauthenticated", "You are not logged in.")
    } else {
      this.setState({ loggedIn: true })
    }

    await this.productFetch();
    await this.reviewsFetch();
    if (this.state.userData.data.id && this.state.userData.data.id !== 0) {
      await this.referItemFetch();
      await this.checkPinnedItems();
    }
  }

  componentDidUpdate = async (prevProps) => {
    if (this.props.userId !== prevProps.userId) {
      await this.referItemFetch();
      await this.checkPinnedItems();
      await this.reviewsFetch();
    }
  }

  componentWillUnmount = async () => { // C
    await AsyncStorage.removeItem("app_refer_state");
    Linking.removeEventListener('url', this.handleOpenURL);
  }

  _renderItem = ({ item }) => {
    // console.warn(item)
    return (
      <RenderItem
        onPress={() => this.props.navigation.push('ProductDetails', { product_id: item.id })}
        uri={item.info.data.image.full_path}
        id={item.id}
        product_name={item.product_name}
        country_code={item.country_code}
        price={item.price}
        ratings={item.ratings}
      />
    );
  }

  productFetch = async () => {
    try {
      let formData = new FormData();
      formData.append('id', this.props.route.params.product_id);
      formData.append('include', 'info');
      formData.append('api_token', api_token);
      const response = await fetch(product_api + this.props.route.params.product_id + '&include=info,date&api_token=' + api_token);
      const responseJson = await response.json();
      // alert(JSON.stringify(responseJson, null, '\t'))
      // console.log('Des',responseJson.data.info.data.description)
      // let tdes = responseJson.data.info.data.description
      // let content = tdes.split("<tr>")[1].split("</tr>")[0]
      // let content = tdes.split("<tbody>")[1].split("</tbody>")[0]
      // let tabled = `<table><tbody><tr>${content}</tr></tbody></table>`
      // console.warn('tata', tabled)

      // console.log("this.props.route.params.product_id", this.props.route.params.product_id)
      // console.log("responseJson", responseJson)
      // console.log("responseJson.data.info.data.description", responseJson.data.info.data.description)

      // console.log("responseJson.data.info.data", responseJson.data.info.data)
      // console.log("this.state.product.info.data", this.state.product.info.data)

      this.setState({
        product: responseJson.data,
        product_name: responseJson.data.product_name,
        product_images: responseJson.data.info.data.product_images,
        user_id: responseJson.data.user_id,
        commission: responseJson.data.referrer_commission,
        des: responseJson.data.info.data.description,
        // des: tabled,
        warranty: responseJson.data.info.data.warranty_details,
        user_img: responseJson.data.info.data.shop_info.shop_image,
        shp_name: responseJson.data.info.data.shop_info.shop_name,
        shop_id: responseJson.data.info.data.user_id,
        shop: responseJson.data.info.data.shop_info,
        // user_img: responseJson.data.info.data.attributes.shipping_info.inventory.shop_info.shop_image,
        productLoaded: true,
        deliveryFeeLowest: parseFloat(responseJson.data.info.data.shipping_info.delivery_options[0].delivery_option_info.fee),
        deliveryFeeHighest: parseFloat(responseJson.data.info.data.shipping_info.delivery_options[0].delivery_option_info.fee),
      }, function () {
        this.userProductsFetch();
      });

      var delivery_options = responseJson.data.info.data.shipping_info.delivery_options;
      let tmpLowest = parseFloat(delivery_options[0].delivery_option_info.fee);
      let tmpHighest = parseFloat(delivery_options[0].delivery_option_info.fee);

      delivery_options.map((deliveryOption) => {
        if (deliveryOption.delivery_option_info.fee > tmpHighest) {
          tmpHighest = parseFloat(deliveryOption.delivery_option_info.fee);
        }
        // console.log("deliveryOption.delivery_option_info.fee < tmpLowest", deliveryOption.delivery_option_info.fee < tmpLowest, " || deliveryOption.delivery_option_info.fee", deliveryOption.delivery_option_info.fee, " || tmpLowest", tmpLowest)
        if (deliveryOption.delivery_option_info.fee < tmpLowest) {
          tmpLowest = parseFloat(deliveryOption.delivery_option_info.fee);
        }
      })
      // console.log(tmpLowest, " - ", tmpHighest);
      this.setState({
        deliveryFeeHighest: tmpHighest,
        deliveryFeeLowest: tmpLowest,
      });
      // console.log(this.state.deliveryFeeLowest, " - ", this.state.deliveryFeeHighest);
      // console.warn('product data', this.state.product.info.data.shipping_info.delivery_options[0].delivery_option_name)
    }
    catch (e) {
      console.warn(e);
    }
  }

  reviewsFetch = async () => {
    // data.info.data.user_info.avatar
    // console.warn('prodid', this.props.route.params.product_id)
    // alert(JSON.stringify(this.state.product.id));
    try {
      let formData = new FormData();
      formData.append('product_id', this.state.product.id);
      formData.append('api_token', api_token);
      const data = await fetch(reviews_api, {
        method: 'POST',
        body: formData
      });
      const response = await data.json();
      // console.log('Reviews', JSON.stringify(response, null, '\t'));
      this.setState({
        reviews: response.data,
        reviewsLoaded: response.status,
      });
    }
    catch (e) {
      console.warn(e);
    }

  }
  userProductsFetch = async () => {
    try {
      const response = await fetch(user_products + "user_id=" + this.state.product.user_id + "&include=info&page=1&per_page=7&api_token=" + api_token);
      const responseJson = await response.json();
      this.setState({
        user_products: responseJson.data,
        user_productsLoaded: true,
      });
      // console.log("user products", responseJson.data)
    }
    catch (e) {
      console.warn(e);
    }
  }

  _renderReviews = () => {
    // alert(JSON.stringify(this.data.info.data.user_info.avatar))
    // alert(JSON.stringify(this.state.reviews))
    if (this.state.reviews.length > 0) {
      return this.state.reviews.map((data) => {
        return (
          <View>
            <View style={styles.rrCon}>
              <View style={{ alignSelf: 'flex-start', }}>
                {this._renderReviewerAvatar(data.info.data.user_info.avatar)}
              </View>
              <View style={styles.nameCon}>
                <Text style={styles.txtName}>{data.info.data.user_info.name}</Text>
                <Col style={{ width: '25%', paddingTop: 2 }}>
                  <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor={'#f36e23'}
                    halfStarColor={'#f36e23'}
                    starSize={11}
                    style={{ justifyContent: 'flex-end' }}
                  />
                </Col>
                <Col style={styles.col}>
                  <Text style={styles.txtDesciption}>{data.description}</Text>
                </Col>
              </View>
            </View>
          </View>
        );
      });
    }
    else {
      return (
        <View>
          <Thumbnail
            source={require('../../assets/internal/4.png')}
            square
            style={styles.img4}
          />
        </View>
      );
    }
  }

  _renderReviewerAvatar = (avatar) => {
    const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
    const uri = avatar;
    return (
      <FastImage
        style={styles.fastImg}
        tint='light'
        // resizeMode='contain'
        // {...{preview, uri}}
        source={{ uri: avatar }}
      />
    );
  }

  imageViewList() {
    return this.state.product_images.map((data, i) => {
      const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' }
      const uri = data.directory + '/' + data.filename;
      return (
        <ImageZoom
          cropWidth={Dimensions.get('window').width}
          cropHeight={400}
          imageWidth={Dimensions.get('window').width}
          imageHeight={400}
        >
        </ImageZoom>
      );
    });
  }

  renderHeader = () => {
    return (
      <View style={styles.headerCon}>
        <Swiper
          showsButtons={true}
          autoplay={true}
          loop={true}
          autoplayTimeout={2}
          autoplayDirection={true}
          automaticallyAdjustContentInsets={true}
          height={300}
          showsPagination={false}
        >
          {this.imageSliderList()}
        </Swiper>
        <View style={styles.con2}>
          <Text style={styles.productText}>{this.state.product.product_name}</Text>
          <View style={styles.space} />
          {this.state.product?.info?.data?.with_discount === "yes" ?
            <Text style={styles.productPrice}>
              <Text style={styles.productPriceWithDiscount}>
                {this.state.product.country_code === 'PH' && '₱'}
                {this.state.product.country_code === 'US' && '$'}
                {" "}{(parseFloat(this.state.product?.info?.data?.price) + parseFloat(this.state.product?.info?.data?.discount_amount)).toFixed(2)}
              </Text>
              {" "}{this.state.product.country_code === 'PH' && '₱'}
              {this.state.product.country_code === 'US' && '$'}
              {" "}{this.state.product.price}{" "}{" "}
              <View style={styles.space}>
                <Text style={styles.discountPercentage}>
                  {"-"}{parseFloat(this.state.product?.info?.data?.discount_percentage).toFixed(2)}{"%"}
                </Text>
              </View>
            </Text>
            :
            <Text style={styles.productPrice}>
              {this.state.product.country_code === 'PH' && '₱'}
              {this.state.product.country_code === 'US' && '$'}
              {" "}{this.state.product.price}
            </Text>}
          <View style={styles.space} />
          <View style={styles.starCon}>
            {this.state.product.ratings >= 0 ? <StarRating
              disabled={true}
              emptyStar={'ios-star-outline'}
              fullStar={'ios-star'}
              halfStar={'ios-star-half'}
              iconSet={'Ionicons'}
              maxStars={5}
              rating={this.state.product.ratings}
              fullStarColor={'#f36e23'}
              halfStarColor={'#f36e23'}
              starSize={11}
            /> :
              <Text numberOfLines={1} style={styles.txtRatings}>
                ({this.state.product.ratings})
              </Text>}
            {this.state.product.ratings >= 0 &&
              <Text style={styles.txtrate}>
                {this.state.product.ratings}/5.0
              </Text>}
          </View>
        </View>
        <View style={styles.border} />
        {this._renderVariants()}
        <View style={styles.referrCommissionCon}>
          <Text style={styles.txtReferr}>Referrer Commission
            {this.state.product?.info?.data?.commission_type === "fix" ?
              " ₱" + this.state.commission
              :
              " " + this.state.commission + "%"}

          </Text>
          <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text>
        </View>

        <View style={styles.userimgCon}>
          <View style={{ flexDirection: 'row', }}>
            <View style={styles.fastImgCon}>
              <FastImage
                style={styles.userimg}
                tint='light'
                // resizeMode='contain'
                // {...{profile}}
                source={{
                  uri:
                    this.state.user_img
                }}
              />
            </View>
            <View style={styles.shpnameCon}>
              <Text style={{ alignSelf: 'center' }}>{this.state.shp_name}</Text>
              <Text style={styles.txtVerify}>{this.state.product.is_verify === 'yes' && 'Verified Shop'}</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('ViewShop', this.state)} style={styles.btnViewCon}>
              <Text style={styles.txtViewShop}>View Shop</Text>
            </TouchableOpacity>
          </View>
        </View>

        <TouchableOpacity style={styles.specificationsCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start' }}>
              <Text style={styles.txtSpecifications}>Category:</Text>
            </View>
            <View style={styles.brandCon}>
              <Text style={styles.txtBrand}>{this.state.product.category}</Text>
            </View>
          </View>
        </TouchableOpacity>

        {/* <TouchableOpacity onPress={() => alert('Under Development')} style={styles.specificationsCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start' }}>
              <Text style={styles.txtSpecifications}>Specifications:</Text>
            </View>
            <View style={styles.brandCon}>
              <Text style={styles.txtBrand}>Brand, Model ...</Text>
            </View>
          </View>
          <View>
            <View style={styles.rightArrowCon}>
              <Thumbnail
                source={require('../../assets/icon_sub.png')}
                small
                square
                style={styles.imgRight}
              />
            </View>
          </View>
        </TouchableOpacity> */}

        {this._renderAttributes()}

        <View style={styles.logCon}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ alignSelf: 'flex-start', }}>
              <Thumbnail
                source={require('../../assets/internal/icon_Logistics.png')}
                square
                style={styles.imgLog}
              />
            </View>
            <View style={styles.shipCon}>
              <Text style={styles.txtShip}>Shipping Fee</Text>
              <Text style={styles.txtNumber}>{":  ₱" + this.state.deliveryFeeLowest + " - ₱" + this.state.deliveryFeeHighest}</Text>
            </View>
            {/* <View style={styles.imgDownCon}>
              <Thumbnail
                source={require('../../assets/internal/icon_down.png')}
                square
                style={styles.imgDown}
              />
            </View> */}
          </View>
        </View>

        <View style={{ backgroundColor: '#f5f6fa', }} />
        <View style={styles.desCon}>
          <Row>
            <View style={{ paddingTop: 15, paddingHorizontal: 15 }}>
              <Text style={[styles.detailText, { fontSize: 16, paddingVertical: 5, fontWeight: 'bold' }]}>Product Details</Text>
            </View>
          </Row>
          <ScrollView>
            <View style={{
              paddingRight: 15,
              paddingLeft: 15
            }}>
              {/* <HTML html={`<iframe srcdoc="${this.state.des}"></iframe>`} /> */}
              <HTML html={this.state.des} {...htmlConfig} />
            </View>
          </ScrollView>
          <Row>
            <View style={styles.detailsCon}>
              <Text style={[styles.detailText, {
                // fontFamily: 'roboto-medium',
                fontSize: 16,
                paddingVertical: 5,
                fontWeight: 'bold'
              }]}>Warranty Details</Text>
            </View>
          </Row>
          <ScrollView>
            <View style={{
              paddingRight: 15,
              paddingLeft: 15
            }}>
              <HTML html={this.state.des} {...htmlConfig} style={{ backgroundColor: 'red' }} />
            </View>
          </ScrollView>
        </View>

        <View style={{ padding: 5, backgroundColor: '#f5f6fa', }} />

        <View style={styles.reviewsCon}>
          <Text style={styles.txtProductR}>Product Reviews ({this.state.product.number_of_reviews})</Text>
          <TouchableOpacity onPress={() => alert('Under Development')}>
            <Text style={styles.txtView}>View All</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.renderReview}>
          {this.state.reviewsLoaded && this._renderReviews()}
        </View>

        {this.state.user_products.length > 0 && <View>
          <View style={styles.rrContainer}>
            <Text style={styles.txtSame}>From the same store</Text>
            <TouchableOpacity
              onPress={() => alert('Under Development')}
              style={{ justifyContent: 'space-between', }}>
              <View style={{ flex: 1 }} />
              <Text style={styles.viewAll}>View All</Text>
            </TouchableOpacity>
          </View>
        </View>}
      </View>
    );
  }

  imageSliderList() {
    return this.state.product_images.map((data, i) => {
      const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
      const uri = data.directory + '/' + data.filename;
      // console.log("iamge slider uri", uri)
      return (
        <TouchableOpacity style={{ alignSelf: 'center', height: 300, width: Dimensions.get('window').width, }} onPress={() => {
          let res = []
          for (var x = 0; x < this.state.product_images.length; x++) {
            res.push({ url: this.state.product_images[x].directory + '/' + this.state.product_images[x].filename })
          }
          this.setState({ imageModal: true, previewImages: res })
        }}>
          <Image
            style={{
              height: 300,
              width: Dimensions.get('window').width,
              backgroundColor: '#ffffff',
              // resizeMode: 'contain'
            }}
            // source={{ uri: uri + "?" + new Date().getTime() }}
            source={{ uri: uri }}
          />
        </TouchableOpacity>
      );
    });
  }

  modalImageSliderList() {
    return this.state.product_images.map((data, i) => {
      const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
      const uri = data.directory + '/' + data.filename;

      // console.log("modalImageSliderList uri", uri)
      return (
        <View
          style={{ alignSelf: 'center', height: 120, width: 12 }}
        // onPress={() => this.setState({imageModal: true, modal_image: data.directory+'/'+data.filename})}
        >
          <Image
            style={{
              height: 125,
              width: 125,
              backgroundColor: '#fff',
              // resizeMode: 'stretch',
              marginLeft: -50,
              marginTop: 10
            }}
            // source={{ uri: uri + "?" + new Date().getTime() }}
            source={{ uri: uri }}
          />
        </View>

      );
    });
  }

  handleSwiper() {
    return (
      <>
        <Swiper
          style={{ alignSelf: 'center', height: 175, alignItems: 'center' }}
          showsButtons={false}
          autoplay={true}
          loop={true}
          autoplayTimeout={5}
          autoplayDirection={true}
          automaticallyAdjustContentinsets={true}
          horizontal={true}
        // showPagination={true}
        // renderPagination={
        //   this.renderPage
        // }
        >
          {this.modalImageSliderList()}
        </Swiper>
      </>
    )
  }

  _renderUserAvatar = () => {
    const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' }
    // user_img: responseJson.data.info.data.shop_info.shop_image,
    const uri = this.state.user_image;
    // const uri = this.state.user_img
    return (
      <FastImage
        style={{
          width: 50,
          height: 50,
          borderRadius: 25,
        }}
        tint='light'
        // resizeMode='contain'
        {...{ preview, uri }}
        source={this.state.user_img != undefined ? uri : preview}
      // source={this.state.user_img}
      />
    );
  }

  referItemFetch = async () => {
    try {
      let formData = new FormData();
      formData.append('user_id', this.state.userData.data.id);
      formData.append('product_id', this.props.route.params.product_id);
      formData.append('api_token', api_token);
      const response = await fetch(refer_api, {
        method: 'POST',
        headers: { 'Content-type': 'multipart/form-data' },
        body: formData
      });
      const responseJson = await response.json();
      this.setState({
        refer_url: responseJson.url,
      });
    }
    catch (e) {
      console.warn(e);
    }
  }

  checkPinnedItems = async () => {
    try {
      var data = await fetch(pinned_items_api + 'user_id=' + this.state.userData.data.id + '&api_token=' + api_token);
      var response = await data.json();
      var validate = response.data.filter(x => x.product_id === this.props.route.params.product_id);
      if (validate.length > 0) {
        this.setState({
          already_pinned: true,
          pin_id: validate[0].id,
        });
      }
      else {
        this.setState({
          already_pinned: false,
        });
      }
    }
    catch (e) {
      console.warn(e);
    }
  }
  pinItem = async () => {
    console.log('props', this.props.route.params.product_id)
    try {
      this.setState({ pinnedLoaded: false });
      let formData = new FormData();
      formData.append('user_id', this.state.userData.data.id);
      formData.append('product_id', this.props.route.params.product_id);
      formData.append('api_token', api_token);
      var data = await fetch(pin_item_api + '?user_id=' + this.state.userData.data.id + '&include=info,date&api_token=' + api_token, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData
      });
      var response = await data.json();
      console.log('dataaa', response)

      if (response.status === true) {
        Alert.alert(
          'Success!',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => this.setState({
                pinnedLoaded: true,
                already_pinned: true,
              }),
            },
          ],
          { cancelable: false }
        );
      }
      else {
        Alert.alert(
          'Error!',
          response.errors,
          [
            {
              text: 'OK',
              onPress: () => this.setState({
                pinnedLoaded: true,
                already_pinned: false,
              }),
            }
          ],
          { cancelable: false }
        );
      }
    }
    catch (e) {
      console.warn(e);
    }
  }

  unpinItem = async () => {
    try {
      this.setState({
        pinnedLoaded: false,
      });

      let formData = new FormData();
      formData.append('user_id', this.state.userData.data.id);
      formData.append('pin_id', this.state.pin_id);
      formData.append('api_token', api_token);

      // console.log("unpin userData", this.state.userData);
      console.log("unpin formdata", formData);

      const data = await fetch(unpin_item_api, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: formData
      });

      const response = await data.json();
      console.log("unpin response", response)

      if (response.status) {
        this.setState({
          pin_id: 0,
          pinnedLoaded: true,
          already_pinned: false,
        });
        Alert.alert(
          'Success!',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: false }
        );
      }
      else {
        this.setState({
          pinnedLoaded: true,
          already_pinned: true,
        });
        Alert.alert(
          'Error!',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: false }
        );
      }
    }
    catch (e) {
      console.warn(e);
    }
  }
  inventoryFetch = async () => {
    try {

      this.setState({ fetchingStock: true });
      var hasVariation = 'no';

      // if(this.state.product.info.data.variation_1) {
      //   var hasVariation = 'yes';
      // }
      // else {
      //   var hasVariation = 'no';
      // }

      let formData = new FormData();

      formData.append('with_variation', hasVariation);
      if (!this.state.variation1_selected) {
        formData.append('variation_1_id', this.state.variation1_selected);
      }
      else {
        formData.append('variation_1_id', '0');
      }
      if (!this.state.variation2_selected) {
        formData.append('variation_2_id', this.state.variation2_selected);
      }
      else {
        formData.append('variation_2_id', '0');
      }
      formData.append('product_id', this.props.route.params.product_id);
      formData.append('api_token', api_token);

      const data = await fetch(product_inventory, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData
      });

      const response = await `data`.json();

      this.setState({
        stock: response.data.stock,
        price: response.data.price,
      }, () => {
        this.setState({ fetchingStock: false });
      });
    }
    catch (e) {
      console.warn(e);
    }
  }
  minusQuantity = () => {
    if (this.state.quantity > 1) {
      this.setState({
        quantity: this.state.quantity - 1
      });
    }
    else {
      AlertIOS.alert(
        'Error!',
        'You need atleast 1 to proceed.',
      );
    }
  }
  addQuantity = () => {
    if (this.state.quantity < this.state.stock) {
      this.setState({
        quantity: this.state.quantity + 1
      });
    }
    else {
      AlertIOS.alert(
        'Error!',
        'Not enough stock to proceed.',
      );
    }
  }
  // renderProductImage = () => {
  //   // alert(JSON.stringify(this.props.data.info.data.product_images))
  //   const preview = { uri: 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' };
  //   const uri = this.state.product_images[0].directory + '/' + this.state.product_images[0].filename;
  //   console.log("renderProductImage uri", uri)
  //   return (
  //     <View style={{ flex: 0 }}>
  //       <Image
  //         style={{ height: 150, width: 150, backgroundColor: '#ffffff' }}
  //         tint='default'
  //         {...{ preview, uri }}
  //         resizeMode='contain'
  //       />
  //     </View>
  //   );
  // }
  _renderReferButton = () => {
    if (this.props.userId > 0) {
      if (this.state.refer_url) {
        return (
          <TouchableOpacity
            style={{
              justifyContent: 'space-around',
              width: '20%',
              alignItems: 'center',
              paddingTop: 10,
              paddingBottom: 10,
            }}
            onPress={() => {
              Alert.alert(
                'Coming Soon!',
                'This feature will be available soon.'
              );
            }}
          >
            <Thumbnail
              source={require('../../assets/internal/product/icon_refer.png')}
              style={styles.bottomTabImage}
              square
            />
            <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Refer</Text>
          </TouchableOpacity>
        );
      }
      else {
        return (
          <TouchableOpacity
            style={{
              justifyContent: 'space-around',
              width: '20%',
              alignItems: 'center',
              paddingTop: 10,
              paddingBottom: 10,
            }}
            onPress={() => {
              AlertIOS.alert(
                'Error!',
                'There has been an error trying to refer this item!',
                [
                  {
                    text: 'Okay',
                    style: 'cancel',
                  },
                ],
              );
            }}
          >
            <Thumbnail
              source={require('../../assets/internal/product/icon_refer.png')}
              style={styles.bottomTabImage}
              square
            />
            <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Refer</Text>
          </TouchableOpacity>
        );
      }
    }
    else {
      return (
        <TouchableOpacity
          style={{
            justifyContent: 'space-around',
            width: '20%',
            alignItems: 'center',
            paddingTop: 10,
            paddingBottom: 10,
          }}
          onPress={() => {
            AlertIOS.alert(
              'Error!',
              'Seems like you are not logged in yet! Please login first to refer this item!',
              [
                {
                  text: 'Cancel',
                  style: 'cancel',
                },
                {
                  text: 'Log In',
                  onPress: () => this.props.navigation.navigate('Login'),
                },
              ],
            );
          }}
        >
          <Thumbnail
            source={require('../../assets/internal/product/icon_refer.png')}
            style={styles.bottomTabImage}
            square
          />
          <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Refer</Text>
        </TouchableOpacity>
      )
    }
  }
  _copyToClip = async () => {
    await Clipboard.setString(this.state.refer_url);
    alert('URL copied to clipboard!');
  }

  _renderVariants = () => {
    if (this.state.product?.info?.data?.with_variation === "yes") {
      this._renderVariant1();
      // this._renderVariant2();
    }
  }

  _renderVariant1 = () => {
    if (this.state.product?.info?.data?.variation_1[0].options.length > 0) {
      let variation1 = this.state.product?.info?.data?.variation_1;

      console.log("variation1", variation1)
      console.log("variation1.options", variation1.options)

      variation1[0].options.map((option) => {
        console.log("variation1 option", option)
        return (
          <View style={styles.referrCommissionCon}>
            <Text style={styles.txtReferr}>
              {option}
            </Text>
            {/* <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text> */}
          </View>)
      })

      // for (let index = 0; index < variation1[0].options.length; index++) {
      //   console.log("variation1", index)
      //   // return (
      //   //   <View style={styles.referrCommissionCon}>
      //   //     <Text style={styles.txtReferr}>
      //   //       {variation1[0].options[index]}
      //   //     </Text>
      //   //     {/* <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text> */}
      //   //   </View>
      //   // )
      // }
    }
  }

  _renderVariant2 = () => {
    if (this.state.product?.info?.data?.variation_2[0].options.length > 0) {
      console.log("variation2")
      let variation2 = this.state.product?.info?.data?.variation_2;
      for (let index = 0; index < variation2[0].options.length; index++) {
        console.log("variation2 index", index)
        // return (
        //   <View style={styles.referrCommissionCon}>
        //     <Text style={styles.txtReferr}>
        //       {variation2[0].options[index]}
        //     </Text>
        //     {/* <Text style={styles.txtReferrs}>Referrer Commission is the percentage of the product price that will go to the referrer on every successful referrals</Text> */}
        //   </View>
        // )
      }
    }
  }

  _renderAttributes = () => {
    // return this.state.product.info.data.attributes.map((data, i) => {

    if (this.state.product?.info?.data?.attributes.length > 0) {
      let attributes = this.state.product.info.data.attributes

      // attributes.map((attribute, i) => (
      //   <View>
      //     <TouchableOpacity style={styles.specificationsCon}>
      //       <View style={{ flexDirection: 'row' }}>
      //         <View style={{ alignSelf: 'flex-start' }}>
      //           <Text style={styles.txtSpecifications}>{attribute.attribute_name}</Text>
      //         </View>
      //         <View style={styles.brandCon}>
      //           <Text style={styles.txtBrand}>{attribute.attribute_value}</Text>
      //         </View>
      //       </View>
      //     </TouchableOpacity>
      //   </View>
      // ))

      for (let i = 0; i < attributes.length; i++) {
        // console.log("attributes", attributes)
        return (
          <View>
            <TouchableOpacity style={styles.specificationsCon}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ alignSelf: 'flex-start' }}>
                  <Text style={styles.txtSpecifications}>{attributes[i].attribute_name + ":"}</Text>
                </View>
                <View style={styles.brandCon}>
                  <Text style={styles.txtBrand}>{attributes[i].attribute_value}</Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )
      }

    } else {
      return null
    }


    // <View key={i}>
    //   <View style={{
    //     paddingTop: 10,
    //     paddingBottom: 10,
    //     paddingLeft: 15,
    //     paddingRight: 15,
    //   }}>
    //     <Text style={{
    //       // fontFamily: 'Roboto',
    //       fontSize: 12,
    //       color: '#7f8c8d',
    //       paddingBottom: 5,
    //     }}>{data.attribute_name}</Text>
    //     <Text style={{
    //       // fontFamily: 'roboto-medium',
    //       fontSize: 12,
    //       color: '#231f20',
    //     }}>{data.attribute_value}</Text>
    //   </View>

    //   <View style={{
    //     borderTopWidth: .7,
    //     width: width,
    //     borderColor: '#ecf0f1',
    //   }} />
    // </View>
  }
  renderPage = (index, total, context) => {
    return (
      <View style={{ alignSelf: 'center', justifyContent: 'flex-end', position: 'absolute', bottom: 0, }}>
        <Text style={{ color: '#FFF' }}>
          <Text style={{ color: '#FFF' }}>{index + 1}</Text>/{total}
        </Text>
      </View>
    )
  }

  handleAlert() {
    return (
      Alert.alert(
        'You are not logged in! Login now to refer this product!',
        {
          text: 'OK',
          onPress: () => {
            this.props.navigation.navigate('Sign In')
          }
        },
        { cancelable: false }
      )
    )
  }

  handleAddToCart = async () => {
    console.warn('missing', this.state.product.info.data.shipping_info.delivery_options[0].delivery_option)
    try {
      let formData = new FormData();
      formData.append("product_id", this.state.product.id);
      formData.append("qty", this.state.quantity);
      formData.append("user_id", this.state.userData.data.id);
      formData.append("price", this.state.product.price);
      formData.append("variations", '');
      formData.append("api_token", api_token);
      formData.append("variation_1", this.state.product.info.data.variation_1);
      formData.append("variation_2", this.state.product.info.data.variation_2);
      formData.append("delivery_courier_slug", this.state.product.info.data.shipping_info.delivery_options[0].delivery_option);
      formData.append("delivery_courier_fee", this.state.product.info.data.shipping_info.delivery_options[0].delivery_option_info.fee)
      console.log("this is cart data", formData)
      const data = await fetch(Endpoints.add_to_cart + `?include=date,info`, {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
      )
      const response = await data.json()
      console.warn('product detail', JSON.stringify(response))
      if (response.status == true) {
        Alert.alert(
          'Success',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                this.setState({ modalVisible: false })
              }
            },
          ],
          { cancelable: true }
        );
      }
      else {
        Alert.alert(
          'Error',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: true }
        );
      }
    }
    catch (e) {
      console.log(e)
    }
  }

  handleCheckout = async () => {
    try {

      let checkoutItem = [];
      checkoutItem.pid = this.state.product.id;
      checkoutItem.product_name = this.state.product.product_name;
      checkoutItem.price = this.state.product.price;
      checkoutItem.qty = this.state.quantity;
      checkoutItem.total = parseFloat(this.state.quantity) * parseFloat(this.state.product.price);
      checkoutItem.delivery_courier_slug = this.state.product.info.data.shipping_info.delivery_options[0].delivery_option
      checkoutItem.delivery_courier_fee = this.state.product.info.data.shipping_info.delivery_options[0].delivery_option_info.fee

      if (this.state.product?.info.data?.with_variation.toString().toLowerCase() === "no") {
        checkoutItem.variations = null;
        checkoutItem.variation_1 = null;
        checkoutItem.variation_2 = null;
      } else if (this.state.product?.info.data?.with_variation.toString().toLowerCase() === "yes") {
        // checkoutItem.variation_1 = this.state.product.info.data.variation_1[0].options[0];
        // checkoutItem.variation_2 = this.state.product.info.data.variation_2[0].options[0];
        checkoutItem.variation_1 = 0;
        checkoutItem.variation_2 = 0;
        checkoutItem.variations = this.state.product.info.data.variation_1[0].options[0] + " " + this.state.product.info.data.variation_2[0].options[0];
      }

      checkoutItem.uid = this.state.userData.data.id;
      checkoutItem.checkoutType = "single";

      // let formData = new FormData();
      // formData.append("product_id", this.state.product.id);
      // formData.append("qty", this.state.quantity);
      // formData.append("user_id", this.state.userData.data.id);
      // formData.append("variations", '');
      // formData.append("type", 'single');
      // // formData.append("variation_1", this.state.product.info.data.variation_1 || '');
      // // formData.append("variation_2", this.state.product.info.data.variation_2 || '');
      // formData.append("variation_1", '');
      // formData.append("variation_2", '');
      // formData.append("api_token", api_token);
      // console.log(formData)
      // const data = await fetch(checkout_api, {
      //   method: 'POST',
      //   headers: {
      //     'Content-Type': 'multipart/form-data',
      //   },
      //   body: formData
      // });
      // const response = await data.json();

      this.setState({ modalVisible: false })

      this.props.navigation.navigate('Checkout', { checkoutData: checkoutItem })
    } catch (err) {
      console.log(err)
    }
  }

  handleLogin() {
    Alert.alert(
      'Unauthenticated',
      'You are not logged in! Login now to continue',
      [
        {
          text: 'Login',
          onPress: () => this.props.navigation.navigate("Sign In"),
        },
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <Container style={{
        backgroundColor: '#f5f6fa'
      }}>
        <Modal visible={this.state.imageModal} transparent={true} onRequestClose={() => this.setState({ imageModal: false })}>
          <ImageViewer imageUrls={this.state.previewImages} />
        </Modal>
        <Header
          style={styles.headCon}>
          <Left style={styles.leftCon}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}
              style={styles.imgBackCon}>
              <Thumbnail
                source={require('../../assets/headericon/left-arrow-dark.png')}
                style={styles.imgBack}
                square
              />
              <Text
                ellipsizeMode='tail'
                numberOfLines={1}
                style={[styles.headerText, { marginHorizontal: 15, alignSelf: 'center', }]}
              >{this.state.product_name}</Text>
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 0, paddingLeft: 10, }}>
            <View style={{ paddingLeft: 15 }} />
          </Body>
          <Right style={styles.rightCon}>
            <TouchableOpacity style={styles.cartCon} onPress={() => {
              if (!this.state.loggedIn) this.handleLogin()
              else this.props.navigation.navigate("Cart Screen")
            }}>
              <Thumbnail
                source={require('../../assets/internal/cart.png')}
                style={styles.cartImg}
                square
              />
            </TouchableOpacity>
            {this.state.already_pinned ?
              <TouchableOpacity
                onPress={() => {
                  if (!this.state.loggedIn) this.handleLogin()
                  else {
                    Alert.alert(
                      'Warning!',
                      'Are you sure you want to unpin this item?',
                      [
                        {
                          text: 'OK',
                          onPress: () => this.unpinItem(),
                        },
                        {
                          text: 'Cancel',
                          style: 'cancel',
                        },
                      ],
                      { cancelable: false }
                    );
                  }
                }}

                style={styles.pinnedCon}
              >
                <Thumbnail
                  source={require('../../assets/internal/icon_Pinned.png')}
                  style={styles.imgPin}
                  square
                />
              </TouchableOpacity>
              :
              <TouchableOpacity
                onPress={() => {
                  if (!this.state.loggedIn) this.handleLogin()
                  else {
                    Alert.alert(
                      'Warning!',
                      'Are you sure you want to pin this item?',
                      [
                        {
                          text: 'OK',
                          onPress: () => this.pinItem(),
                        },
                        {
                          text: 'Cancel',
                          style: 'cancel',
                        },
                      ],
                      { cancelable: false }
                    );
                  }
                }}
                style={styles.unpinCon}
              >
                <Thumbnail
                  source={require('../../assets/internal/icon_Unpin.png')}
                  style={styles.imgUnpin}
                  square
                />
              </TouchableOpacity>}
          </Right>
        </Header>

        <FlatListComponent
          data={this.state.user_products.filter(x => x.id != this.props.route.params.product_id)}
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          ListHeaderComponent={this.renderHeader()}
          renderItem={this._renderItem}
          removeClippedSubviews={false}
          windowSize={18}
          maxToRenderPerBatch={4}
          scrollEventThrottle={16}
        />

        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.modalVisible}
          style={styles.modals}
        >
          <TouchableWithoutFeedback onPress={() => { this.setModalVisible(!this.state.modalVisible) }}>
            <View style={styles.modalCon}>
              <Animatable.View animation="fadeInUpBig" duration={1000} style={styles.animate}>
                <TouchableHighlight>
                  <View>
                    <View style={styles.modCon}>
                      {this.handleSwiper()}
                      {/* <Image
                        style={styles.imgProduct}
                        tint='default'
                        source={this.state.product_images != null ? { uri: this.state.product_images } : imgSample}
                        resizeMode='contain'
                      /> */}
                      <View style={{ alignSelf: 'flex-start', width: width / 2.5, height: height / 6.5, backgroundColor: '#fff', marginTop: 2 }}>
                        <Text style={styles.txtProductName} adjustsFontSizeToFit={true}>{this.state.product.product_name}</Text>
                        <Text style={styles.txtCountryCode} adjustsFontSizeToFit={true}>{this.state.product.country_code == 'PH' && '₱ '}{this.state.product.country_code == 'US' && '$ '}{this.state.product.price}</Text>
                        <Text style={styles.txtStock} adjustsFontSizeToFit={true}>Stock: {this.state.stock}</Text>
                        <ScrollView>
                          <View>
                            <HTML html={this.state.des} {...htmlConfig} />
                          </View>
                        </ScrollView>
                      </View>
                      <TouchableOpacity
                        onPress={() => { this.setModalVisible(!this.state.modalVisible) }}
                        style={styles.btnExit}
                      >
                        <Thumbnail
                          source={require('../../assets/internal/icon_ex.png')}
                          square
                          style={styles.imgExit}
                        />
                      </TouchableOpacity>
                    </View>
                    <View style={styles.countCon}>
                      <TouchableOpacity onPress={() => this.setState({ quantity: this.state.quantity - 1 })} disabled={this.state.quantity == 1 ? true : false}>
                        {/* this.minusQuantity()}> */}
                        <Thumbnail
                          square
                          source={require('../../assets/icon_minus.png')}
                          style={styles.imgAdd}
                        // resizeMode="contain"
                        />
                      </TouchableOpacity>
                      <Text style={styles.txtQuantity}>{this.state.quantity}</Text>
                      <TouchableOpacity onPress={() => this.setState({ quantity: this.state.quantity + 1 })}>
                        {/* this.addQuantity()}> */}
                        <Thumbnail
                          square
                          source={require('../../assets/icon_plus.png')}
                          style={styles.imgAdd}
                        // resizeMode="contain"
                        />
                      </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => {
                      this.state.isSinglePurchase ? this.handleCheckout() : this.handleAddToCart();
                    }} style={styles.btnAddCartCon}>
                      <Text style={styles.txtAddCart}>
                        {this.state.isSinglePurchase ? "Buy Now" : "Add To Cart"}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </TouchableHighlight>
              </Animatable.View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        {/* <Modal
          visible={this.state.imageModal}
          style={{
            height,
            backgroundColor: '#000005',
            alignItems:'center',
            justifyContent:'center',
          }}
        >
          <TouchableOpacity onPress={()=>this.setState({imageModal: false})} style={{position:'absolute',top:0,alignSelf:'flex-start',height: 50,justifyContent:'center',paddingHorizontal:20, paddingVertical:30}}>
          <Image source={require('../../assets/headericon/left-arrow-dark.png')}
          style={{height: 20,width: 20,tintColor:'#e58736',}}
          />
          </TouchableOpacity>
          <View style={{alignItems:'flex-end',height:height/1.9,justifyContent:'flex-end',}}>
            <Swiper
              style={{alignSelf:'flex-start',height:height/2,resizeMode:'contain',}}
              showsButtons={false}
              autoplay={false}
              loop={false}
              autoplayTimeout={3}
              autoplayDirection={true}
              automaticallyAdjustContentinsets={true}
              showPagination={true}
              renderPagination={
                this.renderPage
              }
              >
                {this.imageSliderList()}
            </Swiper>
          </View>
        </Modal> */}

        <Modal
          visible={this.state.referModal}
          style={{
            top: 0,
            bottom: 0,
            right: 0,
            left: 0,
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'rgba(0, 0, 0, .5)',
          }}
          transparent={true}
        >
          <TouchableWithoutFeedback onPress={() => { this.setReferModal(!this.state.referModal) }}>
            <View style={{
              flex: 1,
              justifyContent: 'center',
              backgroundColor: 'rgba(0,0,0,0.5)',
              opacity: 1,
              // width,
            }}>

              <View style={{
                width: width - 70,
                backgroundColor: '#FFF',
                alignItems: 'center',
                paddingLeft: 20,
                paddingRight: 20,
                borderRadius: 15,
                height: height / 1.4,
                alignSelf: 'center'
              }}>
                <View style={{
                  flexDirection: 'row',
                  // width:width -70
                  // backgroundColor:'pink',
                  // width:width/1.5,
                  // justifyContent:'space-between'
                }}>
                  <Text style={{
                    width: 210,
                    textAlign: 'center',
                    borderBottomColor: '#00a14b',
                    borderBottomWidth: 2,
                    // fontFamily: 'roboto-medium',
                    fontSize: 16,
                    color: '#231f20',
                    paddingVertical: 10,
                  }}>Refer an item.</Text>
                  <TouchableOpacity onPress={() => this.setState({ referModal: false })}
                    style={{
                      position: 'absolute',
                      right: 0,
                      marginRight: -35,
                      padding: 10,
                      alignSelf: 'center',
                    }}>
                    <Thumbnail
                      source={require('../../assets/internal/icon_ex.png')}
                      style={{
                        tintColor: '#7f8c8d',
                        width: 13,
                        height: 13,
                      }}
                    />
                  </TouchableOpacity>
                </View>

                <View style={{
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                  <View style={{ paddingTop: 5 }} />
                  <Text style={{
                    // fontFamily: 'roboto-medium',
                    fontSize: 16,
                    color: '#231f20',
                  }}>Share this product.</Text>
                  <View style={{ paddingTop: 5 }} />
                  <Text style={{
                    // fontFamily: 'Roboto',
                    fontSize: 16,
                    color: '#231f20',
                  }}>When someone buys this product that you referred, 25% of sales will be yours.</Text>
                  <View style={{ paddingTop: 10 }} />
                  <Text style={{
                    // fontFamily: 'Roboto',
                    fontSize: 16,
                    color: '#231f20',
                  }}>Keep Referring</Text>
                  <View style={{ paddingTop: 10 }} />
                  <View style={{
                    height: 50,
                    width: '100%',
                  }}>
                    <ScrollView
                      horizontal={true}
                    >
                      <View style={{ justifyContent: 'space-around', flexDirection: 'row' }}>
                        <TouchableOpacity
                          onPress={() => {

                            Share.open({
                              title: 'Share this product',
                              subject: 'ReferApps',
                              message: this.state.refer_url
                            })
                              .then((res) => { console.log(res) })
                              .catch((err) => { err && console.log(err); });

                            // Linking.canOpenURL('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url)).then(supported => {
                            //   if (!supported) {
                            //     console.log('Can\'t handle url: ' + 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url));
                            //   } else {
                            //     return Linking.openURL('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url));
                            //   }
                            // }).catch(err => console.error('An error occurred', err));
                          }}
                          style={{
                            paddingRight: 5,
                            paddingLeft: 5,
                          }}
                        >
                          <Thumbnail
                            source={require('../../assets/internal/product/icon_refer.png')}
                            style={{
                              marginTop: 2.5,
                              width: 40,
                              height: 35
                            }}
                            square
                          />
                        </TouchableOpacity>
                        {/* <TouchableOpacity
                    onPress={ () => {
                      alert("Under Development")
                      // Linking.canOpenURL('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url)).then(supported => {
                      //   if (!supported) {
                      //     console.log('Can\'t handle url: ' + 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url));
                      //   } else {
                      //     return Linking.openURL('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(this.state.refer_url));
                      //   }
                      // }).catch(err => console.error('An error occurred', err));
                    }}
                    style={{
                      paddingRight: 5,
                      paddingLeft: 5,
                    }}
                  >
                    <Thumbnail
                      source={require('../../assets/internal/social_media/icon_1.png')}
                      square
                      style={{
                        resizeMode: 'contain',
                        width: 40,
                        height: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={ () => {
                      alert("Under Development")
                      // Linking.canOpenURL('https://twitter.com/intent/tweet?url='+encodeURIComponent(this.state.refer_url)).then(supported => {
                      //   if (!supported) {
                      //     console.log('Can\'t handle url: ' + 'https://twitter.com/intent/tweet?url='+encodeURIComponent(this.state.refer_url));
                      //   } else {
                      //     return Linking.openURL('https://twitter.com/intent/tweet?url='+encodeURIComponent(this.state.refer_url));
                      //   }
                      // }).catch(err => console.error('An error occurred', err));
                    }}
                    style={{
                      paddingRight: 5,
                      paddingLeft: 5,
                    }}
                  >
                    <Thumbnail
                      source={require('../../assets/internal/social_media/icon_2.png')}
                      square
                      style={{
                        resizeMode: 'contain',
                        width: 40,
                        height: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={ () => {
                      alert("Under Development")
                      // Linking.canOpenURL('https://plus.google.com/share?url='+encodeURIComponent(this.state.refer_url)).then(supported => {
                      //   if(!supported) {
                      //     console.log('Can\'t handle url: '+ 'https://plus.google.com/share?url='+encodeURIComponent(this.state.refer_url));
                      //   }
                      //   else {
                      //     return Linking.openURL('https://plus.google.com/share?url='+encodeURIComponent(this.state.refer_url));
                      //   }
                      // }).catch(err => console.error('An error occured', err));
                    }}
                    style={{
                      paddingRight: 5,
                      paddingLeft: 5,
                    }}
                  >
                    <Thumbnail
                      source={require('../../assets/internal/social_media/icon_9.png')}
                      square
                      style={{
                        resizeMode: 'contain',
                        width: 40,
                        height: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={ () => {
                      alert("Under Development")
                      // Linking.canOpenURL('https://wa.me/?text='+encodeURIComponent(this.state.refer_url)).then(supported => {
                      //   if(!supported) {
                      //     console.log('Can\'t handle url: '+ 'https://wa.me/?text='+encodeURIComponent(this.state.refer_url));
                      //   }
                      //   else {
                      //     return Linking.openURL('https://wa.me/?text='+encodeURIComponent(this.state.refer_url));
                      //   }
                      // }).catch(err => console.error('An error occured', err));
                    }}
                    style={{
                      paddingRight: 5,
                      paddingLeft: 5,
                    }}
                  >
                    <Thumbnail
                      source={require('../../assets/internal/social_media/icon_4.png')}
                      square
                      style={{
                        resizeMode: 'contain',
                        width: 40,
                        height: 40,
                      }}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={ () => {
                      alert("Under Development")
                      // Linking.canOpenURL('fb-messenger://share?link='+encodeURIComponent(this.state.refer_url)+'&app_id=123456789').then(supported => {
                      //   if(!supported) {
                      //     console.log('Can\'t handle url: '+ 'fb-messenger://share?link='+encodeURIComponent(this.state.refer_url)+'&app_id=123456789');
                      //   }
                      //   else {
                      //     return Linking.openURL('fb-messenger://share?link='+encodeURIComponent(this.state.refer_url)+'&app_id=123456789');
                      //   }
                      // }).catch(err => console.error('An error occured', err));
                    }}
                    style={{
                      paddingRight: 5,
                      paddingLeft: 5,
                    }}
                  >
                    <Thumbnail
                      source={require('../../assets/internal/social_media/icon_12.png')}
                      square
                      style={{
                        resizeMode: 'contain',
                        width: 40,
                        height: 40,
                      }}
                    />
                  </TouchableOpacity> */}
                      </View>
                    </ScrollView>
                  </View>
                  <View style={{ paddingTop: 5 }} />
                  <Text style={{
                    // fontFamily: 'Roboto',
                    fontSize: 16,
                    color: '#231f20',
                  }}>or just share this link</Text>
                  <View style={{ paddingTop: 5 }} />
                  <TouchableOpacity
                    onPress={() => this._copyToClip()}
                    style={{
                      backgroundColor: '#ecf0f1',
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={{
                      // fontFamily: 'Roboto',
                      fontSize: 14,
                      color: '#5998ff',
                      paddingLeft: 5,
                      paddingRight: 5,
                    }}>{this.state.refer_url}</Text>
                  </TouchableOpacity>
                  <View style={{ paddingTop: 10 }} />
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>

        <Modal
          visible={this.state.specificationsModal}
          style={{
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            alignItems: 'center',
            backgroundColor: 'rgba(0, 0, 0, .5)',
            position: 'absolute',
            justifyContent: 'space-between',
          }}
        >

          <View style={{
            width: width,
            flex: 1,
          }} />

          <Animatable.View animation="fadeInUpBig" duration={1000}
            style={{
              flex: 0,
              backgroundColor: '#ffffff',
              width: width,
            }}
            onLayout={
              (event) => {
                this.find_dimesions(event.nativeEvent.layout)
              }
            }
          >

            <View
              style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                paddingLeft: 15,
                paddingRight: 15,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <View style={{
                justifyContent: 'space-around',
                flex: 0,
              }}>
                <Thumbnail
                  source={require('../../assets/internal/icon_ex.png')}
                  square
                  style={{
                    width: 16,
                    height: 16,
                    tintColor: '#ffffff',
                  }}
                />
              </View>
              <View style={{
                justifyContent: 'space-around',
                flex: 1,
              }}>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                  alignSelf: 'center',
                }}>Specifications</Text>
              </View>
              <TouchableOpacity
                onPress={() => this.setState({ specificationsModal: false })}
                style={{
                  justifyContent: 'space-around',
                  flex: 0,
                  width: 25,
                  height: 25,
                  borderRadius: 12.5,
                  backgroundColor: '#ecf0f1',
                  alignItems: 'center',
                }}
              >
                <Thumbnail
                  source={require('../../assets/internal/icon_ex.png')}
                  square
                  style={{
                    width: 14,
                    height: 14,
                    tintColor: '#231f20',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View style={{
              borderTopWidth: .7,
              width: width,
              borderColor: '#ecf0f1',
            }} />

            {/* {this.state.product.info.data.attributes &&  */}
            <View style={{
              height: (height / 2),
            }}>

              <ScrollView>

                {/* {this._renderAttributes()} */}

              </ScrollView>

            </View>

            {/* {!this.state.product.info.data.attributes &&  */}
            <View style={{
              height: (height / 2),
            }}>
              <Text style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#7f8c8d',
                paddingBottom: 5,
              }}>There are no attributes for this product.</Text>
            </View>
            {/* } */}

          </Animatable.View>
        </Modal>

        <Modal
          visible={this.state.addToCartModal}
          style={{
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            alignItems: 'center',
            backgroundColor: 'rgba(0, 0, 0, .5)',
            position: 'absolute',
            justifyContent: 'space-between',
          }}
        >

          <View style={{
            width: width,
            flex: 1,
          }} />

          <Animatable.View animation="fadeInUpBig" duration={1000}
            style={{
              flex: 0,
              backgroundColor: '#ffffff',
              width: width,
            }}
          >

            <View
              style={{
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                paddingLeft: 15,
                paddingRight: 15,
                alignItems: 'center',
                justifyContent: 'space-between',
              }}
            >
              <View style={{
                justifyContent: 'space-around',
                flex: 0,
              }}>
                <Thumbnail
                  source={require('../../assets/internal/icon_ex.png')}
                  square
                  style={{
                    width: 16,
                    height: 16,
                    tintColor: '#ffffff',
                  }}
                />
              </View>
              <View style={{
                justifyContent: 'space-around',
                flex: 1,
              }}>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                  alignSelf: 'center',
                }}></Text>
              </View>
              <TouchableOpacity
                onPress={() => this.setState({ addToCartModal: false })}
                style={{
                  justifyContent: 'space-around',
                  flex: 0,
                  width: 25,
                  height: 25,
                  borderRadius: 12.5,
                  backgroundColor: '#ecf0f1',
                  alignItems: 'center',
                }}
              >
                <Thumbnail
                  source={require('../../assets/internal/icon_ex.png')}
                  square
                  style={{
                    width: 14,
                    height: 14,
                    tintColor: '#231f20',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View style={{
              borderTopWidth: .7,
              width: width,
              borderColor: '#ecf0f1',
            }} />

            <View style={{
              flexDirection: 'row',
              paddingTop: 10,
              paddingBottom: 10,
              paddingLeft: 15,
              paddingRight: 15,
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
              {/* {this.renderProductImage()} */}
              <View style={{ flex: 1, paddingLeft: 5 }}>
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 16,
                    color: '#231f20',
                  }}
                >{this.state.product.product_name}</Text>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ea4123',
                }}>{this.state.product.country_code == 'PH' && '₱ '}{this.state.product.country_code == 'US' && '$ '}{this.state.product.price}</Text>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}>Stock: {this.state.stock}</Text>
              </View>
            </View>

            <View style={{
              borderTopWidth: .7,
              width: width,
              borderColor: '#ecf0f1',
            }} />

            <View style={{ paddingLeft: 10, paddingRight: 10 }}>
              {/* {this.state.product.info.data.variation_1 &&  */}
              <View>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}>
                  {/* {this.state.product.info.data.variation_1[0].variation_name} */}
                </Text>

                <ScrollView horizontal>
                  {/* {this._renderVariation1()} */}
                </ScrollView>
              </View>
              {/* } */}

              {/* {this.state.product.info.data.variation_2 &&  */}
              <View>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}>
                  {/* {this.state.product.info.data.variation_2[0].variation_name} */}
                </Text>

                <ScrollView horizontal>
                  {/* {this._renderVariation2()} */}
                </ScrollView>
              </View>
            </View>

            <View style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingTop: 20,
              paddingBottom: 20,
            }}>
              <View style={{ flex: 1 }} />
              <View style={{
                width: '35%',
                justifyContent: 'space-between',
                flexDirection: 'row'
              }}>
                <TouchableOpacity onPress={() => alert('Under Development')}>
                  {/* // this.addQuantity()}> */}
                  <Thumbnail
                    square
                    source={require('../../assets/icon_plus.png')}
                    style={{
                      width: 30,
                      height: 30,
                    }}
                  // resizeMode="contain"
                  />
                </TouchableOpacity>
                <Text style={{
                  // fontFamily: 'Roboto',
                  fontSize: 20,
                  color: '#231f20',
                }}>{this.state.quantity}</Text>
                <TouchableOpacity onPress={() => alert('Under Development')}>
                  {/* // this.minusQuantity()}> */}
                  <Thumbnail
                    square
                    source={require('../../assets/icon_minus.png')}
                    style={{
                      width: 30,
                      height: 30,
                    }}
                  // resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
              <View style={{ flex: 1 }} />
            </View>

            <View style={{ padding: 15 }}>
              {/* {this._renderAddToCart()} */}
            </View>

          </Animatable.View>
        </Modal>

        <Footer style={{}}>
          <FooterTab style={{ backgroundColor: '#FFF' }}>

            <TouchableOpacity onPress={() => {
              if (this.state.loggedIn) {
                Alert.alert('Coming Soon!', 'This feature will be available soon.')
                // this.props.navigation.navigate('Conversation', { reciever: { id: this.state.shop_id, name: this.state.shop.shop_name, avatar: this.state.shop.shop_image } })
              } else this.handleLogin()
            }} style={styles.chatCon}>
              <Thumbnail
                source={require('../../assets/internal/product/icon_chat.png')}
                style={styles.bottomTabImage}
                square
              />
              <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Chat</Text>
            </TouchableOpacity>

            <View style={styles.barrier} />

            <TouchableOpacity onPress={() => {
              if (this.state.loggedIn) {
                this.setState({ isSinglePurchase: false })
                this.setModalVisible(true)
              } else this.handleLogin()
            }} style={styles.btnModalAddCon}>
              <Thumbnail
                source={require('../../assets/internal/product/icon_add2cart.png')}
                style={styles.bottomTabImage}
                square
              />
              <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Add to Cart</Text>
            </TouchableOpacity>

            <View style={styles.barrier} />

            <TouchableOpacity
              onPress={() => {
                if (this.state.loggedIn) {
                  console.warn("REFER URL", this.state.refer_url);

                  // Alert.alert(
                  //   "Share Now",
                  //   "Share via QR Code or via link",
                  //   [
                  //     {
                  //       text: "QR Code",
                  //       onPress: () => {
                  //         this.props.navigation.navigate('QRGenerator', {
                  //           url: this.state.refer_url,
                  //           name: this.state.product_name,
                  //           image: this.state?.product_images[0]?.directory + '/' + this.state?.product_images[0]?.filename,
                  //         });
                  //       },
                  //     },
                  //     {
                  //       text: "Link",
                  //       onPress: () => {
                  //         Share.open({
                  //           title: 'Share this product',
                  //           subject: 'ReferApps',
                  //           message: this.state.refer_url
                  //         })
                  //           .then((res) => { console.log(res) })
                  //           .catch((err) => { err && console.log(err); });
                  //       },
                  //     },
                  //   ],
                  //   {
                  //     cancelable: true,
                  //   }
                  // );

                  if (this.state.refer_url != "" && this.state.refer_url != null) {
                    // console.log("this.state.refer_url", this.state.refer_url);
                    this.props.navigation.navigate('QRGenerator', {
                      url: this.state.refer_url,
                      name: this.state.product_name,
                      image: this.state?.product_images[0]?.directory + '/' + this.state?.product_images[0]?.filename,
                    });
                  }

                  // this.setReferModal(true);
                } else this.handleLogin()
              }}
              style={styles.btnModalAddCon}>
              {/* {this.setReferModal(true);}}  */}
              <Thumbnail
                source={require('../../assets/internal/product/icon_refer.png')}
                style={styles.bottomTabImage}
                square
              />
              <Text style={[styles.bottomTabText, { color: '#00a14b' }]}>Refer</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                if (this.state.loggedIn) {
                  // Alert.alert(
                  //   'Confirm Purchase',
                  //   'Are you sure you want to buy this product?',
                  //   [
                  //     {
                  //       text: 'CANCEL'
                  //     },
                  //     {
                  //       text: 'OK',
                  //       onPress: () => this.handleCheckout()
                  //     },
                  //   ],
                  //   { cancelable: true }
                  // );
                  this.setState({ isSinglePurchase: true })
                  this.setState({ modalVisible: true })

                  // this.props.navigation.navigate('Checkout', {url: 'https://www.google.com'})
                } else this.handleLogin()
              }}
              // alert('Coming Soon', 'This feature will be available soon')} 
              style={styles.btnBuynow}>
              <Text style={[styles.bottomTabBuyText, { color: '#ffffff' }]}>Buy Now</Text>
            </TouchableOpacity>

          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const HTMLstyles = StyleSheet.create({
  h1: {
    fontWeight: '300',
    color: '#231f20',
  },
  h2: {
    fontWeight: '300',
    color: '#231f20',
  },
  h3: {
    fontWeight: '300',
    color: '#231f20',
  },
  ul: {
    fontWeight: '100',
    color: '#231f20',
  },
  p: {
    fontWeight: '100',
    color: '#231f20',
  },
  i: {
    fontWeight: '100',
    color: '#231f20',
  }
});
