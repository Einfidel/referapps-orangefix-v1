import React from 'react';

import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import { Root, Container, Header, Left, Body, Right, Item, Input, Toast } from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux';
import { styles, sell } from './styles';
import Service from '../../components/api/service';
import Icon from '../../components/icons';

const { width, height } = Dimensions.get('window');

export default class SellProductScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      modalVisible1: false,
      modalVisible2: false,
      referrerModal: false,
      packagingModal: false,
      conditionModal: false,
      categoriesModal: false,
      subCategoriesModal: false,
      specificCategoryModal: false,
      shippingModal: false,
      packaging_size: [
        {
          width: '',
        },
        {
          length: '',
        },
        {
          height: '',
        },
      ],
      //to send data
      user: null,
      name: '',
      description: '',
      category_id: '',
      category: '',
      subcategory: '',
      specificcategory: '',
      price: '',
      stock: '',
      weight: '',
      to_send_packagingSize: [],
      to_send_attributes: [],
      condition: '',
      warranty: '',
      referrer_commission: '',
      lastCommission: '',
      text: '',
      value: '',
      images: [],
      variationOptions: [],
      shippingFeeOptions: [],
      main_categories: [],
      shipping_options: [],

      subCategories: [],
      specificCategories: [],
      categoryAttributes: [],

      countryCurrency: '',
    };
  }

  componentDidMount = async () => {
    try {
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) this.setState({ user: user });

      if (user?.data?.country_code === "PH") {
        console.log("is PH")
        this.setState({ countryCurrency: "₱" });
      } else {
        console.log("is not PH")
        this.setState({ countryCurrency: "$" });
      }

      console.log("compoentndid mount", this.countryCurrency)

      //MAIN CATEGORIES
      await Service.getMainCategories(
        (res) => {
          this.setState({ main_categories: res.data });
        },
        (err) => console.log(err)
      );

      await Service.getShippingOptions(
        (res) => {
          this.setState({ shipping_options: res.data });
        },
        (err) => console.log(err)
      );

      this.state.loading

    } catch (error) {
      console.warn(error);
    }
  };

  // Referrer Commission Modal
  setModalVisible1(visible) {
    this.setState({ modalVisible1: visible });
  }

  handleImagePicker = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }
        this.setState((prevState) => ({
          images: [...prevState.images, response],
          filePath: response.path,
          imageUpdated: true,
          fileData: response.data,
        }));
      }
    });
  };

  renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.modalVisible1} style={styles.modalCon}>
          {this._renderReferrModal()}
        </Modal>
      </View>
    );
  };

  handleInputChange = (text) => {
    this.setState({
      referrer_commission: text.replace(/[^0-9]/g, ''),
    });
  };

  _renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.setModalVisible1(!this.state.modalVisible1);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.ReferrerModalConatiner}>
            <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
            <Item>
              <Input
                style={sell.SetCommission}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                value={this.state.referrer_commission}
                onChangeText={(text) => this.handleInputChange(text)}
                maxLength={2}
              />
              <Text style={sell.percentText}>%</Text>
            </Item>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => this._renderReferrerSaveButton()}
              style={sell.referrerSaveBtn}
            >
              <Text style={sell.SaveText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  _renderReferrerSaveButton = () => {
    if (this.state.referrer_commission < 5 || this.state.referrer_commission > 50) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      this.setState({
        referrerModal: false,
        modalVisible1: false,
        // lastCommission: this.state.referrer_commission,
      });
    }
  };

  //Condition Modal

  onPressButton = (value) => {
    this.setState({ value });
    this.setState({ condition: value });
  };

  setConditionModal(visible) {
    this.setState({ conditionModal: visible });
  }

  getCountryCode = () => {
    // if (user?.data?.country_code == "PH") {
    //   return "₱"
    // } else {
    //   return "$"
    // }
    // user?.data?.country_code == "PH" ? countryCurrency = "₱" : countryCurrency = "$";
    // console.log("countryCurrency:", countryCurrency)
    // return countryCurrency;
  }

  conditionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.conditionModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setConditionModal(!this.state.conditionModal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.conditionModalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    this.onPressButton('New');
                    this.setConditionModal(!this.state.conditionModal);
                  }}
                >
                  New
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    this.onPressButton('Used ( Like new )');
                    this.setConditionModal(!this.state.conditionModal);
                  }}
                >
                  Used ( Like new )
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  //Packaging Size Modal
  setModalVisible2(visible) {
    this.setState({ modalVisible2: visible });
  }

  renderCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.categoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({ categoriesModal: !this.state.categoriesModal });
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {this.state.main_categories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSubCategory(
                            cat.id,
                            (res) => {
                              this.setState({
                                category: cat.category_name,
                                category_id: cat.id,
                                categoriesModal: false,
                                subCategories: res.sub.data,
                              });
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  renderSubCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.subCategoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({ subCategoriesModal: !this.state.subCategoriesModal });
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {this.state.subCategories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSpecificCategory(
                            this.state.category_id,
                            (res) => {
                              console.log(this.state.category_id, res);
                              this.setState({
                                subcategory: cat.category_name,
                                subCategoriesModal: false,
                                specificCategories: res.data,
                              });
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  renderSpecificCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.specificCategoryModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({ specificCategoryModal: !this.state.specificCategoryModal });
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {this.state.specificCategories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getCategoryAttribute(
                            this.state.category_id,
                            (res) => {
                              console.log(res);
                              this.setState({
                                subcategory: cat.category_name,
                                subCategoriesModal: false,
                                specificCategories: res.data,
                              });
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  renderPackageModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.modalVisible2} style={styles.modalCon}>
          {this._renderPackagingModal()}
        </Modal>
      </View>
    );
  };

  _renderPackagingModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.setModalVisible2(!this.state.modalVisible2);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.PackagingModalConatiner}>
            <Text style={sell.PackagingModalHeaderText}>Packaging Size</Text>
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Width (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 0;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[0].width}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Length (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 1;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[1].length}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Height (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 2;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[2].height}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.SizeNote}>Note: Use packaging size, not the product size</Text>

            <View style={sell.space} />
            <View style={sell.space} />

            {this._renderPackagingSaveButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  _renderPackagingSaveButton = () => {
    if (
      this.state.packaging_size[0].width.length > 0 &&
      this.state.packaging_size[1].length.length &&
      this.state.packaging_size[2].height.length
    ) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() =>
            this.setState(
              {
                packagingModal: false,
                modalVisible2: false,
                to_send_packagingSize: this.state.packaging_size,
              },
              function () {
                console.warn(this.state.to_send_packagingSize);
              }
            )}
          style={sell.SaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ packagingModal: false, modalVisible2: false })}
          disabled
          style={sell.disableSaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }
  };

  _handlePackagingInput = (index, value) => {
    var data = [...this.state.packaging_size];
    if (index === 0) {
      data.splice(index, 1, { width: value });
    }
    if (index === 1) {
      data.splice(index, 1, { length: value });
    }
    if (index === 2) {
      data.splice(index, 1, { height: value });
    }
    this.setState({
      packaging_size: data,
    });
    console.log(this.state.packaging_size);
  };

  sortShippingFee = () => {
    if (this.state.shippingFeeOptions.length == 0) return 'Set Shipping Fee';
    let sorted = this.state.shippingFeeOptions.sort(function (a, b) {
      return a.shipping_fee - b.shipping_fee;
    });
    return sorted[0].shipping_fee + ' - ' + sorted[sorted.length - 1].shipping_fee;
  };

  renderImages = () => {
    return this.state.images.map((img, i) => {
      return (
        <Image
          key={i}
          source={{ uri: img.uri }}
          style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
        />
      );
    });
  };

  renderCategoryAttributes = () => {
    return this.state.categoryAttributes.map((data, i) => {
      return (
        <View style={sell.whiteBorder}>
          <TouchableOpacity activeOpacity={0.4} onPress={() => this.setState({ categoriesModal: true })}>
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 10 }} />
                <Text style={sell.inputText}>Brand</Text>
              </View>
              <View style={{}}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'set Brand'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'decimal-pad'}
                  onChangeText={(x) => this.setState({ price: x })}
                  value={this.state.price}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    });
  };

  handleSubmit = async () => {
    this.setState({ loading: true });
    await Service.createProduct(
      this.state,
      (res) => {
        if (res.errors) {
          console.warn('Error', JSON.stringify(res.errors, null, '\t'));
          Alert.alert('Upload Error', 'Please try again');
        } else {
          console.log("Succesful Add Item", this.state)
          this.setState({
            responseData: res,
            loadingModal: false,
          });
          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  // await this.handleShowModal();
                  await this.setState({
                    packaging_size: [
                      {
                        width: '',
                      },
                      {
                        length: '',
                      },
                      {
                        height: '',
                      },
                    ],
                    //to send data
                    name: '',
                    description: '',
                    category_id: '',
                    category: '',
                    subcategory: '',
                    specificcategory: '',
                    price: '',
                    stock: '',
                    weight: '',
                    to_send_packagingSize: [],
                    to_send_attributes: [],
                    condition: '',
                    warranty: '',
                    referrer_commission: '',
                    lastCommission: '',
                    text: '',
                    value: '',
                    images: [],
                    main_categories: [],
                    shipping_options: [],
                    subCategories: [],
                    specificCategories: [],
                    categoryAttributes: [],
                    loading: false,
                  });
                },
              },
            ],
            { cancelable: false }
          );
        }
      },
      (err) => console.warn('Error', err.message),
    );
    // console.log(JSON.stringify(this.state));
  };

  handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={this.state.loading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <Root>
        <Container style={sell.container}>
          <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
            <Left>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
              </TouchableOpacity>
            </Left>
            <Body>
              <Text style={sell.headerText}> Sell Product </Text>
            </Body>
            <Right>
              <View style={sell.rightView} />
            </Right>
          </Header>
          {this.handleLoading()}
          <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
            <View>
              <View style={sell.uploadContainer}>
                <ScrollView horizontal={true} scrollEnabled={true} style={{ width: width / 2 - 2 }}>
                  {this.state.images.length > 0 ? (
                    this.renderImages()
                  ) : (
                    <TouchableOpacity activeOpacity={1} onPress={() => this.handleImagePicker()}>
                      <FastImage
                        source={require('../../assets/Sell_Product-09.png')}
                        large
                        style={sell.newProductImage}
                      />
                    </TouchableOpacity>
                  )}
                </ScrollView>
              </View>
              <View style={sell.whiteName}>
                <View style={{ flexDirection: 'row', height: hp('7%') }}>
                  <View style={{ flexDirection: 'column', width: wp('90%') }}>
                    <Input
                      style={{ fontSize: hp('2%'), marginLeft: 13.5 }}
                      onChangeText={(name) => this.setState({ name })}
                      value={this.state.name}
                      placeholder='Product Name'
                      placeholderTextColor='#7f8c8d'
                      maxLength={50}
                    />
                  </View>
                  <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                    <Text style={sell.Length50}>{this.state.name.length}/50</Text>
                  </View>
                </View>
              </View>

              <View style={sell.whiteDescription}>
                <View style={sell.descriptionContainer}>
                  <View style={sell.DescriptionItem}>
                    <TextInput
                      style={sell.descriptionText}
                      onChangeText={(description) => this.setState({ description })}
                      value={this.state.description}
                      placeholder='Description'
                      placeholderTextColor='#7f8c8d'
                      editable={true}
                      multiline={true}
                    />
                  </View>
                </View>
              </View>

              <View style={sell.itemBorder} />
              <View style={sell.space} />
              <View style={sell.space} />

              <View style={sell.whiteBorder}>
                {this.renderReferrerModal()}
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => this.setModalVisible1(true)}
                  style={sell.inputContainer}
                >
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={sell.spaceLeft} />
                      <View style={sell.spaceLeft} />
                      <Text style={[sell.hintText, { marginLeft: 38 }]}>Referrer Commission</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {this.state.referrer_commission == '' ? (
                          'Set Commission %'
                        ) : (
                          this.state.referrer_commission + '%'
                        )}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={sell.space} />
              <View style={sell.space} />
              {this.renderCategoriesModal()}
              <View style={sell.whiteBorder}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    this.setState({ categoriesModal: true, subcategory: '', specificcategory: '' });
                  }}
                >
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 27 }} />
                      <FastImage source={require('../../assets/internal/icon_category.png')} style={sell.sideIcon} />
                      <Text style={sell.inputText}>Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>{this.state.category ? this.state.category : 'Set Category'}</Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              {this.renderSubCategoriesModal()}
              {this.state.category ? (
                <View style={sell.whiteBorder}>
                  <TouchableOpacity
                    activeOpacity={0.4}
                    onPress={() => this.setState({ subCategoriesModal: true, specificcategory: '' })}
                  >
                    <View style={sell.inputContainer}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginLeft: 10 }} />
                        <Text style={sell.inputText}>Sub Category</Text>
                      </View>
                      <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                        <Text style={sell.hintText2}>
                          {this.state.subcategory ? this.state.subcategory : 'Set Sub Category'}
                        </Text>
                        <View style={sell.spaceRight} />
                        <FastImage
                          source={require('../../assets/icon_sub.png')}
                          style={sell.openIcon}
                          tintColor={'#7f8c8d'}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              ) : null}
              {this.renderSpecificCategoriesModal()}
              {this.state.subcategory && this.state.specificCategories.length > 0 ? (
                <View style={sell.whiteBorder}>
                  <TouchableOpacity activeOpacity={0.4} onPress={() => this.setState({ specificCategoryModal: true })}>
                    <View style={sell.inputContainer}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginLeft: 10 }} />
                        <Text style={sell.inputText}>Specific Category</Text>
                      </View>
                      <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                        <Text style={sell.hintText2}>
                          {this.state.specificcategory ? this.state.specificcategory : 'Set Specific Category'}
                        </Text>
                        <View style={sell.spaceRight} />
                        <FastImage
                          source={require('../../assets/icon_sub.png')}
                          style={sell.openIcon}
                          tintColor={'#7f8c8d'}
                        />
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>
              ) : null}

              {this.state.categoryAttributes.length > 0 ? this.renderCategoryAttributes() : null}

              <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
              <View style={sell.space} />
              <View style={sell.space} />
              <View style={sell.space} />

              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Price and Inventory</Text>
              </View>

              <View style={sell.white}>
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 27 }} />
                    <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Price</Text>
                  </View>
                  <Item style={sell.input_title}>
                    <View style={{ paddingLeft: 30 }}>
                      <Text style={sell.Kg}>{this.state.countryCurrency + " "}</Text>
                    </View>
                    <Input
                      style={sell.group_input_text}
                      placeholder={'Price'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'decimal-pad'}
                      onChangeText={(x) => this.setState({ price: x })}
                      value={this.state.price}
                    />
                  </Item>
                </View>
              </View>

              <View style={sell.white}>
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 27 }} />
                    <FastImage source={require('../../assets/internal/icon_stock.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Stock</Text>
                  </View>
                  <Item style={sell.input_title}>
                    <Input
                      style={sell.group_input_text}
                      placeholder={'Stock'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      onChangeText={(x) => this.setState({ stock: x })}
                      value={this.state.stock}
                    />
                  </Item>
                </View>
              </View>

              <View style={sell.white}>
                <TouchableOpacity
                  style={sell.inputContainer}
                  activeOpacity={0.4}
                  onPress={() =>
                    this.props.navigation.navigate('ProductVariations', {
                      onAdd: (variations) => {
                        // this.setState((prevState) => ({
                        //   variationOptions: [ ...prevState.variationOptions, variation ],
                        // }));
                        this.setState({ variationOptions: variations });
                      },
                    })}
                >
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Variations</Text>
                  </View>

                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>Set Variations</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </TouchableOpacity>
              </View>

              <View style={sell.itemBorder} />

              <View style={sell.space} />
              <View style={sell.space} />
              <View style={sell.space} />

              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Shipping</Text>
              </View>

              <View style={sell.white}>
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_weight.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Weight in</Text>
                  </View>
                  <Item style={sell.input_title}>
                    <Input
                      style={sell.weight_group_input_text}
                      placeholder={'Weight in '}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      onChangeText={(x) => this.setState({ weight: x })}
                    />
                    <View style={{ paddingRight: 30 }}>
                      <Text style={sell.Kg}>Kg</Text>
                    </View>
                  </Item>
                </View>
              </View>

              <View style={sell.white}>
                {this.renderPackageModal()}
                <TouchableOpacity onPress={() => this.setModalVisible2(true)} style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_size.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Packaging Size</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text numberOfLines={1} style={[sell.size_group_input_text, { width: width - 200 }]}>
                      {this.state.packaging_size[0].width.length > 0 &&
                        this.state.packaging_size[0].width +
                        'cm X ' +
                        this.state.packaging_size[1].length +
                        'cm X ' +
                        this.state.packaging_size[2].height +
                        'cm'}
                      {!this.state.packaging_size[0].width.length > 0 && 'Packaging Size'}
                    </Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={sell.white}>
                <TouchableOpacity
                  onPress={() => {
                    if (!this.state.weight) {
                      Toast.show({
                        text: 'Set your product Weight first',
                      });
                    } else {
                      this.props.navigation.navigate('ShippingOptions', {
                        logistics: this.state.shipping_options,
                        onDone: (options, logs) =>
                          this.setState({
                            shippingFeeOptions: options,
                            shipping_options: logs,
                          }),
                      });
                    }
                  }}
                  style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Shipping Fee</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{this.sortShippingFee()}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </TouchableOpacity>
              </View>

              <View style={sell.itemBorder} />
              <View style={sell.space} />
              <View style={sell.space} />
              <View style={sell.space} />

              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Others</Text>
              </View>

              <View style={sell.white}>
                {this.conditionsModal()}
                <TouchableOpacity style={sell.inputContainer} onPress={() => this.setConditionModal(true)}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_condition.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Condition</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.condition_group_input_text}>{this.state.value}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </TouchableOpacity>
              </View>

              <View style={sell.whiteDescription}>
                <View style={sell.descriptionContainer}>
                  <View style={sell.DescriptionItem}>
                    <TextInput
                      style={sell.descriptionText}
                      onChangeText={(warranty) => this.setState({ warranty })}
                      value={this.state.warranty}
                      placeholder='Set Warranty Information'
                      placeholderTextColor={'#7f8c8d'}
                      editable={true}
                      multiline={true}
                    />
                  </View>
                </View>
              </View>

              <View style={sell.itemBorder} />
              <View style={sell.space} />
              <View style={sell.space} />

              <View style={{ paddingTop: 10, paddingBottom: 5 }}>
                <TouchableOpacity
                  nativeOpacity={0.4}
                  onPress={() =>
                    this.handleSubmit().then(() =>
                      this.setState({ loading: false }),
                    )
                  }
                  style={sell.nextButton}>
                  <Text style={sell.SellText}>Sell</Text>
                </TouchableOpacity>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </Container>
      </Root>
    );
  }
}
