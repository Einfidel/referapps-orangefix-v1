import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import { Root, Container, Header, Left, Body, Right, Item, Input, Toast } from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux';
import { styles, sell } from './styles';
import Service from '../../components/api/service';
import Icon from '../../components/icons';

const { width, height } = Dimensions.get('window');

export default function SellProductScreen({ navigation, route }) {
  const [loading, setLoading] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [referrerModal, setReferrerModal] = useState(false);
  const [packagingModal, setPackagingModal] = useState(false);
  const [conditionModal, setConditionModal] = useState(false);
  const [categoriesModal, setCategoriesModal] = useState(false);
  const [subCategoriesModal, setSubCategoriesModal] = useState(false);
  const [specificCategoryModal, setSpecificCategoryModal] = useState(false);
  const [shippingModal, setShippingModal] = useState(false);
  const [packaging_size, setPackagingSize] = useState([{ width: '' }, { length: '' }, { height: '' }]);

  const [user, setUser] = useState(null);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [category_id, setCategoryId] = useState('');
  const [category, setCategory] = useState('');
  const [subcategory, setSubcategory] = useState('');
  const [specificcategory, setSpecificcategory] = useState('');
  const [price, setPrice] = useState('');
  const [stock, setStock] = useState('');
  const [weight, setWeight] = useState('');
  const [to_send_packagingSize, setToSendPackagingSize] = useState([]);
  const [to_send_attributes, setToSendAttributes] = useState([]);
  const [condition, setCondition] = useState('');
  const [warranty, setWarranty] = useState('');
  const [referrer_commission, setReferrerCommission] = useState('');
  const [lastCommission, setLastCommission] = useState('');
  const [text, setText] = useState('');
  const [value, setValue] = useState('');
  const [images, setImages] = useState([]);
  const [variationOptions, setVariationOptions] = useState([]);
  const [shippingFeeOptions, setShippingFeeOptions] = useState([]);
  const [main_categories, setMainCategories] = useState([]);
  const [shipping_options, setShippingOptions] = useState([]);

  const [subCategories, setSubCategories] = useState([]);
  const [specificCategories, setSpecificCategories] = useState([]);
  const [categoryAttributes, setCategoryAttributes] = useState([]);

  const [filePath, setFilePath] = useState(null);
  const [imageUpdated, setImageUpdated] = useState(null);
  const [fileData, setFileData] = useState(null);

  const [responseData, setResponseData] = useState(null);
  const [loadingModal, setLoadingModal] = useState(null);

  useEffect(() => {
    onLoad();
  }, [])

  const onLoad = async () => {
    try {
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) setUser(user);

      //MAIN CATEGORIES
      await Service.getMainCategories(
        (res) => {
          setMainCategories(res.data);
        },
        (err) => console.log(err)
      );

      await Service.getShippingOptions(
        (res) => {
          setShippingOptions(res.data);
        },
        (err) => console.log(err)
      );
    } catch (error) {
      console.warn(error);
    }
  }

  const handleImagePicker = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        setImages([images, response]);
        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });
  };

  const renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible1} style={styles.modalCon}>
          {_renderReferrModal()}
        </Modal>
      </View>
    );
  };

  const handleInputChange = (text) => {
    setReferrerCommission(text.replace(/[^0-9]/g, ''));
  };

  const _renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible1(!modalVisible1);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.ReferrerModalConatiner}>
            <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
            <Item>
              <Input
                style={sell.SetCommission}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                value={referrer_commission}
                onChangeText={(text) => handleInputChange(text)}
                maxLength={2}
              />
              <Text style={sell.percentText}>%</Text>
            </Item>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => _renderReferrerSaveButton()}
              style={sell.referrerSaveBtn}
            >
              <Text style={sell.SaveText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const _renderReferrerSaveButton = () => {
    if (referrer_commission < 5 || referrer_commission > 50) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      setModalVisible1(false);
      setReferrerModal(false);
      // setLastCommission(referrer_commission);
    }
  };

  //Condition Modal

  const onPressButton = (value) => {
    setValue(value);
    setCondition(value);
  };

  const conditionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={conditionModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setConditionModal(!conditionModal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.conditionModalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    onPressButton('New');
                    setConditionModal(!conditionModal);
                  }}
                >
                  New
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    onPressButton('Used ( Like new )');
                    setConditionModal(!conditionModal);
                  }}
                >
                  Used ( Like new )
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={categoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setCategoriesModal(!categoriesModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {main_categories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSubCategory(
                            cat.id,
                            (res) => {
                              setCategoriesModal(false);
                              setCategoryId(cat.id);
                              setCategory(cat.category_name);
                              setSubCategories(res.sub.data);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSubCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={subCategoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSubCategoriesModal(!subCategoriesModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {subCategories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSpecificCategory(
                            category_id,
                            (res) => {
                              console.log(category_id, res);
                              setSubCategoriesModal(false);
                              setSubcategory(cat.category_name);
                              setSpecificCategories(res.data);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSpecificCategoriesModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={specificCategoryModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSpecificCategoryModal(!specificCategoryModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Main Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {specificCategories.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getCategoryAttribute(
                            category_id,
                            (res) => {
                              console.log(res);
                              setSubCategoriesModal(false);
                              setSubcategory(cat.category_name);
                              setSpecificCategories(res.data);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderPackageModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible2} style={styles.modalCon}>
          {_renderPackagingModal()}
        </Modal>
      </View>
    );
  };

  const _renderPackagingModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setModalVisible2(!modalVisible2);
        }}
      >
        <View style={styles.containerModal}>
          <View style={sell.PackagingModalConatiner}>
            <Text style={sell.PackagingModalHeaderText}>Packaging Size</Text>
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Width (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 0;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[0].width}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Length (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 1;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[1].length}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Height (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 2;
                  var value = x;
                  _handlePackagingInput(index, value);
                }}
                value={packaging_size[2].height}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.SizeNote}>Note: Use packaging size, not the product size</Text>

            <View style={sell.space} />
            <View style={sell.space} />

            {_renderPackagingSaveButton()}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const _renderPackagingSaveButton = () => {
    if (
      packaging_size[0].width.length > 0 &&
      packaging_size[1].length.length &&
      packaging_size[2].height.length
    ) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() =>
            setModalVisible2(false),
            setPackagingModal(false),
            setToSendPackagingSize(packaging_size),
            console.warn(to_send_packagingSize)}
          style={sell.SaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity >
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() =>
            setModalVisible2(false),
            setPackagingModal(false)}
          disabled
          style={sell.disableSaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }
  };

  const _handlePackagingInput = (index, value) => {
    var data = [...packaging_size];
    if (index === 0) {
      data.splice(index, 1, { width: value });
    }
    if (index === 1) {
      data.splice(index, 1, { length: value });
    }
    if (index === 2) {
      data.splice(index, 1, { height: value });
    }
    setPackagingSize(data);
    console.log(packaging_size);
  };

  const sortShippingFee = () => {
    if (shippingFeeOptions.length == 0) return 'Set Shipping Fee';
    let sorted = shippingFeeOptions.sort(function (a, b) {
      return a.shipping_fee - b.shipping_fee;
    });
    return sorted[0].shipping_fee + ' - ' + sorted[sorted.length - 1].shipping_fee;
  };

  const renderImages = () => {
    return images.map((img, i) => {
      return (
        <Image
          key={i}
          source={{ uri: img.uri }}
          style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
        />
      );
    });
  };

  const renderCategoryAttributes = () => {
    return categoryAttributes.map((data, i) => {
      return (
        <View style={sell.whiteBorder}>
          <TouchableOpacity activeOpacity={0.4} onPress={() => setCategoriesModal(true)}>
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 10 }} />
                <Text style={sell.inputText}>Brand</Text>
              </View>
              <View style={{}}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'set Brand'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'decimal-pad'}
                  onChangeText={(x) => setPrice(x)}
                  value={price}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    });
  };

  const handleSubmit = async () => {
    setLoading(true);
    await Service.createProduct(
      state,
      (res) => {
        if (res.errors) {
          console.warn('Submit Error', JSON.stringify(res.errors, null, '\t'));
          Alert.alert('Upload Error', 'Please try again');
        } else {
          setResponseData(res);
          setLoadingModal(false);

          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  // await handleShowModal();
                  setPackagingSize([{ width: '' }, { length: '' }, { height: '' }]);
                  setName('');
                  setDescription('');
                  setCategoryId('');
                  setCategory('');
                  setSubcategory('');
                  setSpecificcategory('');
                  setPrice('');
                  setStock('');
                  setWeight('');
                  setToSendPackagingSize([]);
                  setToSendAttributes([]);
                  setCondition('');
                  setWarranty('');
                  setReferrerCommission('');
                  setLastCommission('');
                  setText('');
                  setValue('');
                  setImages([]);
                  setShippingFeeOptions([]);
                  setMainCategories([]);
                  setShippingOptions([]);
                  setSubCategories([]);
                  setSpecificCategories([]);
                  setCategoryAttributes([]);
                  setLoading(false);
                },
              },
            ],
            { cancelable: false }
          );
        }
      },
      (err) => console.warn('Error', err.message),
    );
    // console.log(JSON.stringify(state));
  };

  const handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={loading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };


  return (
    <Root>
      <Container style={sell.container}>
        <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
          <Left>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={sell.headerText}> Sell Product </Text>
          </Body>
          <Right>
            <View style={sell.rightView} />
          </Right>
        </Header>
        {handleLoading()}
        <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
          <View>
            <View style={sell.uploadContainer}>
              <ScrollView horizontal={true} scrollEnabled={true} style={{ width: width / 2 - 2 }}>
                {images.length > 0 ? (
                  renderImages()
                ) : (
                  <TouchableOpacity activeOpacity={1} onPress={() => handleImagePicker()}>
                    <FastImage
                      source={require('../../assets/Sell_Product-09.png')}
                      large
                      style={sell.newProductImage}
                    />
                  </TouchableOpacity>
                )}
              </ScrollView>
            </View>
            <View style={sell.whiteName}>
              <View style={{ flexDirection: 'row', height: hp('5.8%') }}>
                <View style={{ flexDirection: 'column', width: wp('90%') }}>
                  <Input
                    style={{ fontSize: hp('1.7%'), marginLeft: 15 }}
                    onChangeText={(name) => setName(name)}
                    value={name}
                    placeholder='Product Name'
                    placeholderTextColor='#7f8c8d'
                    maxLength={50}
                  />
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                  <Text style={sell.Length50}>{name.length}/50</Text>
                </View>
              </View>
            </View>

            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(description) => setDescription(description)}
                    value={description}
                    placeholder='Description'
                    placeholderTextColor='#7f8c8d'
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.whiteBorder}>
              {renderReferrerModal()}
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => setModalVisible1(true)}
                style={sell.inputContainer}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={sell.spaceLeft} />
                    <View style={sell.spaceLeft} />
                    <Text style={[sell.hintText, { marginLeft: 38 }]}>Referrer Commission</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>
                      {referrer_commission == '' ? (
                        'Set Commission %'
                      ) : (
                        referrer_commission + '%'
                      )}
                    </Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.space} />
            <View style={sell.space} />
            {renderCategoriesModal()}
            <View style={sell.whiteBorder}>
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => {
                  setCategoriesModal(true);
                  setSubcategory('');
                  setSpecificcategory('');
                }}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 27 }} />
                    <FastImage source={require('../../assets/internal/icon_category.png')} style={sell.sideIcon} />
                    <Text style={sell.inputText}>Category</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{category ? category : 'Set Category'}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            {renderSubCategoriesModal()}
            {category ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() =>
                    setSubCategoriesModal(true),
                    setSpecificcategory('')}
                >
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Sub Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {subcategory ? subcategory : 'Set Sub Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}
            {renderSpecificCategoriesModal()}
            {subcategory && specificCategories.length > 0 ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity activeOpacity={0.4} onPress={() => setSpecificCategoryModal(true)}>
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Specific Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {specificcategory ? specificcategory : 'Set Specific Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            {categoryAttributes.length > 0 ? renderCategoryAttributes() : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Price and Inventory</Text>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 27 }} />
                  <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Price</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.group_input_text}
                    placeholder={'Price'}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(x) => setPrice(x)}
                    value={price}
                  />
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 27 }} />
                  <FastImage source={require('../../assets/internal/icon_stock.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Stock</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.group_input_text}
                    placeholder={'Stock'}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'number-pad'}
                    onChangeText={(x) => setStock(x)}
                    value={stock}
                  />
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              <TouchableOpacity
                style={sell.inputContainer}
                activeOpacity={0.4}
                onPress={() =>
                  navigation.navigate('ProductVariations', {
                    onAdd: (variations) => {
                      setVariationOptions(variations);
                    },
                  })}
              >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Variations</Text>
                </View>

                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>Set Variations</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.itemBorder} />

            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Shipping</Text>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_weight.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Weight in</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.weight_group_input_text}
                    placeholder={'Weight in '}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'number-pad'}
                    onChangeText={(x) => setWeight(x)}
                  />
                  <View style={{ paddingRight: 30 }}>
                    <Text style={sell.Kg}>Kg</Text>
                  </View>
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              {renderPackageModal()}
              <TouchableOpacity onPress={() => setModalVisible2(true)} style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_size.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Packaging Size</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text numberOfLines={1} style={[sell.size_group_input_text, { width: width - 200 }]}>
                    {packaging_size[0].width.length > 0 &&
                      packaging_size[0].width +
                      'cm X ' +
                      packaging_size[1].length +
                      'cm X ' +
                      packaging_size[2].height +
                      'cm'}
                    {!packaging_size[0].width.length > 0 && 'Packaging Size'}
                  </Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>
            <View style={sell.white}>
              <TouchableOpacity
                onPress={() => {
                  if (!weight) {
                    Toast.show({
                      text: 'Set your product Weight first',
                    });
                  } else {
                    navigation.navigate('ShippingOptions', {
                      logistics: shipping_options,
                      onDone: (options, logs) => {
                        setShippingFeeOptions(options);
                        setShippingOptions(logs);
                      }
                    });
                  }
                }}
                style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Shipping Fee</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>{sortShippingFee()}</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Others</Text>
            </View>

            <View style={sell.white}>
              {conditionsModal()}
              <TouchableOpacity style={sell.inputContainer} onPress={() => setConditionModal(true)}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage source={require('../../assets/internal/icon_condition.png')} style={sell.sideIcon} />
                  <Text style={sell.inputText}>Condition</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.condition_group_input_text}>{value}</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(warranty) => setWarranty(warranty)}
                    value={warranty}
                    placeholder='Set Warranty Information'
                    placeholderTextColor={'#7f8c8d'}
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={{ paddingTop: 10, paddingBottom: 5 }}>
              <TouchableOpacity
                nativeOpacity={0.4}
                onPress={() =>
                  handleSubmit().then(() =>
                    setLoading(true),
                  )
                }
                style={sell.nextButton}>
                <Text style={sell.SellText}>Sell</Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </Container>
    </Root >
  );
}
