import React, {
  useState,
  useEffect,
  createRef,
} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  Dimensions,
  StyleSheet,
  Modal,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Radio,
} from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { styles as stylesx, sell as sellx } from '../styles';
import Icon from '../../../components/icons';

import FastImage from 'react-native-fast-image';
import { cos } from 'react-native-reanimated';

// import sell from './../../styles/ProductScreenStyles/sellproductstyles';

const { width, height } = Dimensions.get('window');

export default function ShippingOptions({ navigation, route }) {
  const [showmodal, setShowmodal] = useState(false);
  const [checked, setChecked] = useState(false);
  const [stock, setStock] = useState('');
  const [price, setPrice] = useState('');
  const [logistics, setLogistics] = useState([]);
  const [enabledLogistics, setEnabledLogistics] = useState([]);
  const [options, setOptions] = useState([]);
  const [selected, setSelected] = useState(null);
  const [fee, setFee] = useState(null);
  const [logiTmp, setLogiTmp] = useState(null);
  const [onLoad, setOnLoad] = useState(true);

  useEffect(() => {
    // console.warn(JSON.stringify(route.params.logistics, null, '\t'));
    // setLogistics(route.params.logistics);
    if (onLoad) {
      setEnabledLogistics(route.params.shippingFeeOptions);

      let logisticsTmp = [];
      let optionsTmp = [];

      route.params.logistics.map((item) => {
        let feeTmp = null;
        let enabled = false;

        // if (route.params.shippingFeeOptions != null) {
        route.params.shippingFeeOptions.map((enabledItem) => {
          feeTmp = item.slug == enabledItem.name ? enabledItem.shipping_fee.toString() : null;
          enabled = item.slug == enabledItem.name ? true : null;
        });
        // }

        feeTmp != null ? item.fee = feeTmp : null;
        if (enabled) {
          item.enabled = enabled

          optionsTmp.push({
            id: optionsTmp.length + 1,
            name: item.slug,
            shipping_fee: parseFloat(item.fee),
          });
        }

        logisticsTmp.push(item);
      });
      // console.log("optionsTmp", optionsTmp);

      setLogistics(logisticsTmp);
      setLogiTmp("Trigger");
      setOptions(optionsTmp);
      console.log("optionsTmp", optionsTmp)
      setOnLoad(false);
    }
  }, [])

  const renderLogistics = () => {
    let tmp = logiTmp;
    // console.log("logistics", logistics)
    return logistics.map((data, i) => {
      return (
        <ListItem key={i}>
          <Left style={{ flex: 4 }}>
            <Icon.FontAwesome
              name={data.enabled ? 'toggle-on' : 'toggle-off'}
              color={data.enabled ? '#66b545' : 'gray'}
              style={{ padding: 0, marginTop: 3.5, marginRight: 5 }}
            />
            <Text>{data.other_name}</Text>
          </Left>
          <Right style={{ flex: 2 }}>
            <TouchableOpacity
              onPress={() => {
                setFee(logistics[i].fee);
                setSelected(i);
                setShowmodal(true);
              }}>
              <View>
                {
                  options.map((deliveryOption, x) => {
                    return (
                      deliveryOption?.name === data.slug ?
                        <Text style={{ fontSize: 12 }}>
                          {deliveryOption?.shipping_fee >= 0 ? deliveryOption?.shipping_fee?.toString() : 'Set shipping fee'}
                        </Text>
                        :
                        <Text style={{ fontSize: 12 }}>
                          Set shipping fee
                        </Text>
                    );
                  })
                }
              </View>
            </TouchableOpacity>
          </Right>
        </ListItem >
      );
    });
  };

  const renderModal = () => {
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={showmodal}
        style={{ flex: 1 }}>
        <TouchableWithoutFeedback
          onPress={() => {
            setShowmodal(!showmodal);
          }}
          style={{ backgroundColor: 'red' }}>
          <View style={{ ...stylesx.containerModal }}>
            <View style={sellx.PackagingModalConatiner}>
              <Text
                style={{
                  fontWeight: 'normal',
                  paddingVertical: 5,
                  fontSize: 16,
                }}>
                {selected != null && logistics.length > 0
                  ? logistics[selected]?.other_name
                  : ''}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  paddingVertical: 5,
                  fontSize: 14,
                  marginBottom: 5,
                }}>
                Shipping Fee
              </Text>
              <TextInput
                // value={logistics[selected]?.fee}
                placeholder="0.00"
                keyboardType="numeric"
                style={{ borderColor: 'gray', borderWidth: 0.5, padding: 5 }}
                onChangeText={(val) => {
                  logistics[selected].fee = Number(val).toFixed(2);
                  setFee(Number(val).toFixed(2));
                }}
              />
              <ListItem selected={false}>
                <Left>
                  <TouchableOpacity
                    onPress={() => {
                      setFee(0);
                      setChecked(!checked);
                    }}
                    style={{ padding: 3.5 }}>
                    <Icon.Ionicons
                      name={
                        checked
                          ? 'ios-checkbox'
                          : 'ios-checkbox-outline'
                      }
                      size={20}
                      color={checked ? '#66b545' : 'gray'}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontWeight: 'normal',
                      paddingVertical: 5,
                      fontSize: 12,
                    }}>
                    I will cover the shipping fee
                  </Text>
                  <Icon.Ionicons
                    name="md-information-circle"
                    size={10}
                    style={{ paddingTop: 7, paddingLeft: 2 }}
                  />
                </Left>
                <Right />
              </ListItem>
              <ListItem>
                <Left style={{ flex: 2 }}>
                  <TouchableOpacity
                    onPress={() => {
                      setFee(null);
                      setChecked(false);
                      setShowmodal(false);
                    }}
                    style={{ width: 120, padding: 5, alignItems: 'center' }}>
                    <Text>Cancel</Text>
                  </TouchableOpacity>
                </Left>
                <Right style={{ flex: 2 }}>
                  <TouchableOpacity
                    onPress={() => apply()}
                    style={{
                      backgroundColor: '#66b515',
                      width: 120,
                      padding: 5,
                      alignItems: 'center',
                      borderRadius: 5,
                    }}>
                    <Text style={{ color: '#fff' }}>Appy & Enable</Text>
                  </TouchableOpacity>
                </Right>
              </ListItem>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  };

  const apply = () => {
    let copy = logistics,
      optionsCopy = options;
    if (checked) {
      copy[selected].fee = fee === null ? '0' : '0';
      copy[selected].enabled = true;
      let index = optionsCopy.findIndex(
        (i) => i.name === logistics[selected].slug,
      );
      index === -1
        ? optionsCopy.push({
          id: options.length + 1,
          name: logistics[selected].slug,
          shipping_fee: parseFloat('0'),
        })
        : (optionsCopy[index] = {
          id: options[index].id,
          name: options[index].name,
          shipping_fee: parseFloat('0'),
        });
    } else {
      copy[selected].fee = fee === null ? '0' : fee;
      copy[selected].enabled = true;
      let index = optionsCopy.findIndex(
        (i) => i.name === logistics[selected].slug,
      );
      index === -1
        ? optionsCopy.push({
          id: options.length + 1,
          name: logistics[selected].slug,
          shipping_fee: parseFloat(fee),
        })
        : (optionsCopy[index] = {
          id: options[index].id,
          name: options[index].name,
          shipping_fee: parseFloat(fee),
        });
    }
    // console.warn('To apply options', JSON.stringify(optionsCopy, null, '\t'));
    setLogistics(copy);
    setOptions(optionsCopy);
    // console.log("setOptions", optionsCopy);
    setShowmodal(false);
  };

  // handleOptions = () => {
  //   let enabledLogistics = logistics.filter((i) => i.enabled);
  //   enabledLogistics.map((item) => {
  //     let index = options.findIndex((option) => option.name === item.slug);
  //     index === -1 &&
  //       options.push({
  //         id: options.length + 1,
  //         name: item.slug,
  //         fee: item.fee,
  //       });
  //   });
  //   return options;
  // };

  return (
    <Container>
      <Header style={sell.header}>
        <Left style={{ flex: 0 }}>
          <TouchableOpacity
            onPress={() => {
              console.log("Passing this options back", options);
              route.params.onDone(
                options,
                logistics,
              );
              navigation.goBack();
            }}>
            <FastImage
              source={require('../../../assets/headericon/left-arrow-dark.png')}
              style={sell.backImage}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 0 }}>
          <Text style={sell.headerText}> Shipping Fee Options</Text>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity style={sell.rightView} />
        </Right>
      </Header>
      <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
        {showmodal && renderModal()}
        <List>{renderLogistics()}</List>
      </KeyboardAwareScrollView>
    </Container>
  );
}

const sell = StyleSheet.create({
  header: {
    ...Platform.select({
      ios: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: -20
      },
      android: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: 30,
      }
    })
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 25,
  },
  stockAndprice1stView: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
  },
  stockAndpriceViewContainer: {
    backgroundColor: '#ffffff',
    height: height / 5.3,
  },
  stockAndPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingLeft: 25,
  },
  space: {
    paddingTop: 5,
  },
  variationRow: {
    width: width / 3,
    justifyContent: 'space-around',
  },
  variationTitle1: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
    // fontFamily: 'italic',
  },
  variationTitle: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    // alignSelf: 'center',
    textAlign: 'left',
    // fontFamily: 'italic',
  },
  variationOption: {
    // fontFamily: 'Roboto',
    // fontSize: 16,
    // color: '#231f20',
    alignSelf: 'center',
  },
  optionInputContainer: {
    width: '80%',
    height: 36,
    borderWidth: 0.7,
    borderColor: '#e1e2e3',
    justifyContent: 'space-around',
  },
  StockPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  variationsOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  setStockAndpriceButton: {
    backgroundColor: '#00a14b',
    width: '100%',
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  addOptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
