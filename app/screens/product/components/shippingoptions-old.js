import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TextInput,
  Dimensions,
  StyleSheet,
  Modal,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Radio,
} from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { styles as stylesx, sell as sellx } from '../styles';
import Icon from '../../../components/icons';

import FastImage from 'react-native-fast-image';

// import sell from './../../styles/ProductScreenStyles/sellproductstyles';

const { width, height } = Dimensions.get('window');

class ShippingOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showmodal: false,
      checked: false,
      stock: '',
      price: '',
      logistics: [],
      options: [],
      selected: {},
      fee: null,
    };
  }

  componentDidMount = () => {
    // console.warn(JSON.stringify(this.props.route.params.logistics, null, '\t'));
    this.setState({ logistics: this.props.route.params.logistics }, () => {
      let { logistics } = this.state,
        options = [];
      let enabledLogistics = logistics.filter((i) => i.enabled);
      enabledLogistics.map((item) => {
        options.push({
          id: options.length + 1,
          name: item.slug,
          shipping_fee: item.fee,
        });
      });
      this.setState({ options });
    });
  };

  renderLogistics = () => {
    return this.state.logistics.map((data, i) => {
      return (
        <ListItem key={i}>
          <Left style={{ flex: 4 }}>
            <Icon.FontAwesome
              name={data.enabled ? 'toggle-on' : 'toggle-off'}
              color={data.enabled ? '#66b545' : 'gray'}
              style={{ padding: 0, marginTop: 3.5, marginRight: 5 }}
            />
            <Text>{data.other_name}</Text>
          </Left>
          <Right style={{ flex: 2 }}>
            <TouchableOpacity
              onPress={() => this.setState({ showmodal: true, selected: i })}>
              <Text style={{ fontSize: 12 }}>
                {data.fee >= 0 ? data.fee : 'Set shipping fee'}
              </Text>
            </TouchableOpacity>
          </Right>
        </ListItem>
      );
    });
  };

  renderModal = () => {
    let { logistics, selected } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={this.state.showmodal}
        style={{ flex: 1 }}>
        <TouchableWithoutFeedback
          onPress={() => {
            this.setState({ showmodal: !this.state.showmodal });
          }}
          style={{ backgroundColor: 'red' }}>
          <View style={{ ...stylesx.containerModal }}>
            <View style={sellx.PackagingModalConatiner}>
              <Text
                style={{
                  fontWeight: 'normal',
                  paddingVertical: 5,
                  fontSize: 16,
                }}>
                {this.state.selected && this.state.logistics.length > 0
                  ? this.state.logistics[this.state.selected]?.other_name
                  : ''}
              </Text>
              <Text
                style={{
                  fontWeight: 'normal',
                  paddingVertical: 5,
                  fontSize: 14,
                  marginBottom: 5,
                }}>
                Shipping Fee
              </Text>
              <TextInput
                value={logistics[selected]?.fee}
                placeholder="0.00"
                keyboardType="numeric"
                style={{ borderColor: 'gray', borderWidth: 0.5, padding: 5 }}
                onChangeText={(val) => this.setState({ fee: val })}
              />
              <ListItem selected={false}>
                <Left>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        fee: 0,
                        checked: this.state.checked ? false : true,
                      });
                    }}
                    style={{ padding: 3.5 }}>
                    <Icon.Ionicons
                      name={
                        this.state.checked
                          ? 'ios-checkbox'
                          : 'ios-checkbox-outline'
                      }
                      size={20}
                      color={this.state.checked ? '#66b545' : 'gray'}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontWeight: 'normal',
                      paddingVertical: 5,
                      fontSize: 12,
                    }}>
                    I will cover the shipping fee
                  </Text>
                  <Icon.Ionicons
                    name="md-information-circle"
                    size={10}
                    style={{ paddingTop: 7, paddingLeft: 2 }}
                  />
                </Left>
                <Right />
              </ListItem>
              <ListItem>
                <Left style={{ flex: 2 }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        fee: null,
                        checked: false,
                        showmodal: false,
                      });
                    }}
                    style={{ width: 120, padding: 5, alignItems: 'center' }}>
                    <Text>Cancel</Text>
                  </TouchableOpacity>
                </Left>
                <Right style={{ flex: 2 }}>
                  <TouchableOpacity
                    onPress={() => this.apply()}
                    style={{
                      backgroundColor: '#66b515',
                      width: 120,
                      padding: 5,
                      alignItems: 'center',
                      borderRadius: 5,
                    }}>
                    <Text style={{ color: '#fff' }}>Appy & Enable</Text>
                  </TouchableOpacity>
                </Right>
              </ListItem>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  };

  apply = () => {
    let { logistics, options, fee, selected } = this.state;
    let copy = logistics,
      optionsCopy = options;
    copy[selected].fee = fee === null ? '0' : fee;
    copy[selected].enabled = true;
    let index = optionsCopy.findIndex(
      (i) => i.name === logistics[selected].slug,
    );
    index === -1
      ? optionsCopy.push({
        id: options.length + 1,
        name: logistics[selected].slug,
        shipping_fee: fee,
      })
      : (optionsCopy[index] = {
        id: options[index].id,
        name: options[index].name,
        shipping_fee: fee,
      });
    // console.warn('To apply options', JSON.stringify(optionsCopy, null, '\t'));
    this.setState({
      logistics: copy,
      options: optionsCopy,
      showmodal: false,
    });
  };

  // handleOptions = () => {
  //   let {options, logistics} = this.state;
  //   let enabledLogistics = logistics.filter((i) => i.enabled);
  //   enabledLogistics.map((item) => {
  //     let index = options.findIndex((option) => option.name === item.slug);
  //     index === -1 &&
  //       options.push({
  //         id: options.length + 1,
  //         name: item.slug,
  //         fee: item.fee,
  //       });
  //   });
  //   return options;
  // };

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <Left style={{ flex: 0 }}>
            <TouchableOpacity
              onPress={() => {
                console.log(this.state.options);
                this.props.route.params.onDone(
                  this.state.options,
                  this.state.logistics,
                );
                this.props.navigation.goBack();
              }}>
              <FastImage
                source={require('../../../assets/headericon/left-arrow-dark.png')}
                style={sell.backImage}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 0 }}>
            <Text style={sell.headerText}> Shipping Fee Options</Text>
          </Body>
          <Right style={{ flex: 1 }}>
            <TouchableOpacity style={sell.rightView} />
          </Right>
        </Header>
        <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
          {this.state.showmodal && this.renderModal()}
          <List>{this.renderLogistics()}</List>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

export default ShippingOptions;

const sell = StyleSheet.create({
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 25,
  },
  stockAndprice1stView: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
  },
  stockAndpriceViewContainer: {
    backgroundColor: '#ffffff',
    height: height / 5.3,
  },
  stockAndPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingLeft: 25,
  },
  space: {
    paddingTop: 5,
  },
  variationRow: {
    width: width / 3,
    justifyContent: 'space-around',
  },
  variationTitle1: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
    // fontFamily: 'italic',
  },
  variationTitle: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    // alignSelf: 'center',
    textAlign: 'left',
    // fontFamily: 'italic',
  },
  variationOption: {
    // fontFamily: 'Roboto',
    // fontSize: 16,
    // color: '#231f20',
    alignSelf: 'center',
  },
  optionInputContainer: {
    width: '80%',
    height: 36,
    borderWidth: 0.7,
    borderColor: '#e1e2e3',
    justifyContent: 'space-around',
  },
  StockPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  variationsOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  setStockAndpriceButton: {
    backgroundColor: '#00a14b',
    width: '100%',
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  addOptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
