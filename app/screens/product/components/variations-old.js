import React, {
  useState,
  useEffect,
  createRef,
} from 'react';
import { Text, TouchableOpacity, View, Dimensions, StyleSheet, Image, TextInput, Alert } from 'react-native';
import { Header, Left, Container } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const { width, height } = Dimensions.get('window');

export default function VariationsScreen({ navigation, route }) {
  const [isClickedMenu, setIsClickedMenu] = useState(false);
  const [hasSubmited, setHasSubmited] = useState(false);
  const [variation1Name, setVariation1Name] = useState('');
  const [variation2Name, setVariation2Name] = useState('');
  const [variation1Options, setVariation1Options] = useState([]);
  const [variation2Options, setVariation2Options] = useState([]);
  const [oldVariation1Options, setOldVariation1Options] = useState([]);
  const [oldVariation2Options, setOldVariation2Options] = useState([]);
  const [tmp, setTmp] = useState('');

  useEffect(() => {
    // Check if there is already a variation
    if (route.params.variationOptions.length > 0) {
      setVariation1Name(route.params.variationOptions[0].variation_name)
      setVariation1Options([...route.params.variationOptions])
      setOldVariation1Options([...route.params.variationOptions])

      // Check if there is already a 2nd variation
      if (route.params.variationOptions[0].child.length > 0) {
        setVariation2Name(route.params.variationOptions[0].child[0].variation_name)
        setVariation2Options([...route.params.variationOptions[0].child])
        setOldVariation2Options([...route.params.variationOptions[0].child])
        setIsClickedMenu(true);
      } else {
        let childArray = [
          {
            "variation_name": '',
            "name": '',
            "stock": 0,
            "price": 0,
          }
        ]
        setVariation2Options(childArray)
      }
    } else {
      let variationArray = [
        {
          "variation_name": '',
          "parent_name": '',
          "stock": 0,
          "price": 0,
          "child": []
        }
      ];
      setVariation1Options(variationArray)

      let childArray = [
        {
          "variation_name": '',
          "name": '',
          "stock": 0,
          "price": 0,
        }
      ]
      setVariation2Options(childArray)
    }


    // setLabel(route.params.variationOptions[0]?.variation_name);
    // setValue(route.params.variationOptions[0]?.parent_name);
    // setPrice(route.params.variationOptions[0]?.price);
    // setStock(route.params.variationOptions[0]?.stock);
  }, [])

  const render1stVariationOptions = () => {
    // let tmpVariation1Options = variation1Options;
    // console.log("rendering 1st variations")

    return variation1Options.map((option1, index) => {
      return (
        <View key={index} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          {hasSubmited && (option1.parent_name === '' || option1.parent_name.replace(/\s/g, '').toLowerCase() == '') ?
            <Text style={{ color: "#FA3636", fontSize: 16, marginRight: 4, marginTop: 6 }}>
              *
            </Text>
            : null}
          <TextInput
            placeholder={'Enter Variation Options, eg: red, white etc.'}
            placeholderTextColor={'#231f20'}
            style={styles.txtInput2}
            value={option1.parent_name}
            onChangeText={(newOption) => {
              option1.parent_name = newOption;
              // console.log("variation1Options[" + index + "].parent_name", variation1Options[index].parent_name)

              // console.log("option1.child", option1.child)

              // Used to trigger
              setTmp(newOption);
              let tmp2 = tmp;
            }}
          />
          {index > 1 ?
            <TouchableOpacity onPress={() => {
              handleRemoveOptions1(index)
            }} style={styles.minusCon}>
              <Image source={require('../../../assets/icon_minus.png')} style={styles.imgMinus} />
            </TouchableOpacity>
            :
            <TouchableOpacity style={styles.minusCon}>
              <Image source={''} style={styles.imgMinus} />
            </TouchableOpacity>}
        </View>
      )
    })
  }

  const handleAddOptions1 = () => {
    let tmpVariation2Options = [...variation2Options];
    let tmpVariationArray = [];

    tmpVariation2Options.map((option, index) => {
      if (tmpVariationArray.length > 0) {
        let tmpOption = {};
        tmpOption.name = option.name;
        tmpOption.variation_name = option.variation_name;
        tmpOption.price = 0;
        tmpOption.stock = 0;
        tmpVariationArray = [...tmpVariationArray, tmpOption]
      } else {
        let tmpOption = {};
        tmpOption.name = option.name;
        tmpOption.variation_name = option.variation_name;
        tmpOption.price = 0;
        tmpOption.stock = 0;
        tmpVariationArray = [tmpOption]
      }
    })

    let variationArray = {
      "variation_name": variation1Name,
      "parent_name": '',
      "child": tmpVariationArray,
      // "child": variation2Options,
    };

    let tmpVariation1Options = variation1Options;
    // let tmp = (...tmpVariation1Options, variationArray)
    // console.log("variation1Options[0].child", variation1Options[0].child)
    // console.log("variationArray.child", variationArray.child)

    setVariation1Options([...tmpVariation1Options, variationArray])
  }

  const handleRemoveOptions1 = (index) => {
    console.log("index:", index)
    console.log("Removing:", variation1Options[index])
    const tmpVariation1Options = [...variation1Options]
    tmpVariation1Options.splice(index, 1);
    setVariation1Options(tmpVariation1Options);
    setTmp(index);
    let tmp2 = tmp;
    // console.log("variation1Options" + [index] + " should be empty:", variation1Options[index])
    // setVariation1Options(tmpVariation1Options);
  }

  const render2ndVariationOptions = () => {
    // let tmpVariation1Options = variation1Options;
    // console.log("rendering 2nd variations")

    return variation2Options.map((option, index) => {
      return (
        <View key={index} style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          {hasSubmited && (option.name === '' || option.name.replace(/\s/g, '').toLowerCase() == '') ?
            <Text style={{ color: "#FA3636", fontSize: 16, marginRight: 4, marginTop: 6 }}>
              *
            </Text>
            : null}
          <TextInput
            placeholder={'Enter Variation Options, eg: red, white etc.'}
            placeholderTextColor={'#231f20'}
            style={styles.txtInput2}
            value={option.name}
            onChangeText={(newOption) => {
              option.name = newOption;
              // console.log("variation2Options[index].name", variation2Options[index].name)

              // Used to trigger
              setTmp(newOption);
              let tmp2 = tmp;
            }}
          />
          {index > 1 ?
            <TouchableOpacity onPress={() => {
              handleRemoveOptions2(index)
            }} style={styles.minusCon}>
              <Image source={require('../../../assets/icon_minus.png')} style={styles.imgMinus} />
            </TouchableOpacity>
            :
            <TouchableOpacity style={styles.minusCon}>
              <Image source={''} style={styles.imgMinus} />
            </TouchableOpacity>}
        </View>
      )
    })
  }

  const handleAddOptions2 = () => {
    let variationArray = {
      "variation_name": variation2Name,
      "name": '',
      "stock": 0,
      "price": 0,
    };

    // let tmpVariation2Options = variation2Options;
    // let tmp = (...tmpVariation1Options, variationArray)

    // variation1Options.map((options1, index1) => {
    //   options1.child = [...options1.child, variationArray];
    // })

    setVariation2Options([...variation2Options, variationArray])
  }

  const handleRemoveOptions2 = (index) => {
    console.log("index:", index)
    console.log("Removing:", variation2Options[index])
    const tmpVariation2Options = [...variation2Options]
    tmpVariation2Options.splice(index, 1);
    setVariation2Options(tmpVariation2Options);
    setTmp(index);
    let tmp2 = tmp;
    console.log("variation2Options" + [index] + " should be empty:", variation2Options[index])
  }

  const handleSubmit = () => {
    // Add error handling here
    setHasSubmited(true);
    let variation1Error = '';
    let variation2Error = '';

    if (isClickedMenu) {
      if (variation1Options.length > variation2Options.length) {
        variation1Options.splice(variation2Options.length - 1, variation1Options.length - variation2Options.length);
      }

      variation1Options.map((option1, index1) => {
        if (option1.parent_name == null || option1.parent_name.replace(/\s/g, '').toLowerCase() == '') {
          variation1Error === '' ? variation1Error = (index1 + 1) : variation1Error += ", " + (index1 + 1)
        }
        // console.log("option1's child #" + index1, option1.child)

        variation2Options.map((option2, index2) => {
          // console.log("option2 #" + index2, option2)
          if (index1 == 0 && (option2.name == null || option2.name.replace(/\s/g, '').toLowerCase() == '')) {
            variation2Error === '' ? variation2Error = (index2 + 1) : variation2Error += ", " + (index2 + 1)
          }

          if (option1.child[index2] != null) {
            console.log(option1.child[index2].name, option2.name)
            console.log("option1.child[" + index2 + "] is not null")
            if (option1.child[index2].name != option2.name) {
              console.log(option1.child[index2].name, "is != to", option2.name)
              option1.child[index2].name = option2.name
              option1.child[index2].price = 0
              option1.child[index2].stock = 0
            }
          } else {
            console.log("added new variation to option 1")
            console.log("option2", option2)

            const newOption2 = {};

            newOption2.variation_name = option2.variation_name
            newOption2.name = option2.name
            newOption2.price = 0
            newOption2.stock = 0

            option1.child = [...option1.child, newOption2];
          }
        })
      })
    } else {
      setVariation2Options(null);
      variation1Options.map((option1, index1) => {
        // console.log("option1 #" + index1, option1)
        if (option1.price * 0 != 0) {
          option1.price = 0;
        }
        if (option1.stock * 0 != 0) {
          option1.stock = 0;
        }
      })
    }

    // console.log("variation1Options", variation1Options)
    // console.log("variation1Options[4]", variation1Options[4])
    // console.log("variation1Options[4].parent_name", variation1Options[4].parent_name)

    if (variation1Error != '' || variation2Error != '') {
      // console.log("variation1Error", variation1Error)
      // console.log("variation2Error", variation2Error)
      Alert.alert(
        'Error!',
        'Please fill up all the variation names that you have added',
        [
          {
            text: 'OK',
          },
        ],
        { cancelable: true }
      )
    } else {
      navigation.navigate('AddStockPrices', {
        onSubmit: route.params.onAdd,
        variation: variation1Options,
      });
    }
  }

  const handleRemove2ndVariation = () => {
    setIsClickedMenu(false);
  }

  return (
    < Container style={styles.container} >
      <Header style={styles.header}>
        <Left style={styles.left}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={require('../../../assets/headericon/left-arrow-dark.png')} style={styles.imgback} />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.txtAddVariation}> Add Variations </Text>
          </View>
        </Left>
      </Header>
      <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
        <View style={styles.Con}>
          <View style={styles.addOptCon}>

            <View>
              <Text style={{ fontWeight: "bold", fontSize: 17, color: '#231f20', alignSelf: 'center', marginBottom: 10, }}>
                1st Variation Name
              </Text>

              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                <TextInput
                  placeholder={'Enter Variation Name, eg: colour, size etc.'}
                  placeholderTextColor={'#f36e23'}
                  style={styles.txtInput}
                  value={variation1Name}
                  onChangeText={(newVariation1Name) => setVariation1Name(newVariation1Name)}
                />
                <TouchableOpacity style={styles.minusCon}>
                  <Image source={''} style={styles.imgMinus} />
                </TouchableOpacity>
              </View>
            </View>

            <View>
              <Text style={{ fontWeight: "bold", fontSize: 16, color: '#231f20', alignSelf: 'center', marginTop: 10, }}>
                Variation Options
              </Text>
              {render1stVariationOptions()}
            </View>

            <TouchableOpacity
              style={styles.btnAddOpt}
              onPress={() => {
                handleAddOptions1();
              }}
            >
              <Text style={styles.txtAddOpt}>Add Options</Text>
            </TouchableOpacity>

            {isClickedMenu == false ? (
              <TouchableOpacity onPress={() => {
                setIsClickedMenu(true);
                // alert('Coming Soon')
              }} style={styles.popCon}>
                <Text style={styles.txtAddVar}>Add 2nd Variation</Text>
              </TouchableOpacity>
            ) : (
              <View style={styles.conAddVar}>
                <View style={{ marginTop: 10, paddingTop: 15, borderTopWidth: 2.5, borderTopColor: "#4C66D0" }}>
                  <Text style={{ fontWeight: "bold", fontSize: 17, color: '#231f20', alignSelf: 'center', marginBottom: 10, }}>
                    2nd Variation Name
                  </Text>
                  <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                    <TextInput
                      placeholder={'Enter Variation Name, eg: colour, size etc.'}
                      placeholderTextColor={'#f36e23'}
                      style={styles.txtInput}
                      value={variation2Name}
                      onChangeText={(newVariation2Name) => setVariation2Name(newVariation2Name)}
                    />
                    <TouchableOpacity onPress={() => {
                      handleRemove2ndVariation()
                    }} style={styles.minusCon}>
                      <Image source={require('../../../assets/icon_minus.png')} style={styles.imgMinus} />
                    </TouchableOpacity>
                  </View>
                </View>

                <View>
                  <Text style={{ fontWeight: "bold", fontSize: 16, color: '#231f20', alignSelf: 'center', marginTop: 10, }}>
                    Variation Options
                  </Text>
                  {render2ndVariationOptions()}
                </View>

                <TouchableOpacity
                  style={styles.btnAddOpt}
                  onPress={() => {
                    handleAddOptions2();
                  }}
                >
                  <Text style={styles.txtAddOpt}>Add Options</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>

        <View style={styles.btnStockCon}>
          <TouchableOpacity
            style={styles.btnSetCon}
            onPress={() => {
              handleSubmit()
            }}
          >
            <Text style={styles.txtStock}>Set Stock and Price</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>

    </Container >
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width: width,
  },
  header: {
    ...Platform.select({
      ios: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: -20
      },
      android: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: 30,
      }
    })
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgback: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  addVariation: {
    marginLeft: 25,
  },
  txtAddVariation: {
    // fontFamily: 'roboto.medium',
    fontSize: 18,
  },
  Con: {
    backgroundColor: '#ecf0f1',
    flex: 1,
  },
  addOptCon: {
    backgroundColor: '#FFF',
    marginTop: 15,
    paddingVertical: 20,
  },
  txtInput: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.3,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
  },
  txtInput2: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.3,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
    marginTop: 10,
  },
  btnAddOpt: {
    backgroundColor: '#24ae5f',
    width: width / 1.15,
    height: 31,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  txtAddOpt: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  popCon: {
    width: width / 1.15,
    height: 31,
    borderRadius: 8,
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: '#f36e23',
    borderStyle: 'dashed',
  },
  txtAddVar: {
    // fontFamily: 'regular',
    fontSize: 14,
    color: '#f36e23',
    alignSelf: 'center',
  },
  conAddVar: {
    backgroundColor: '#FFF',
    width,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  minusCon: {
    alignSelf: 'flex-end',
    marginVertical: 10,
    marginLeft: 10,
  },
  imgMinus: {
    tintColor: '#e04f5f',
    resizeMode: 'contain',
    height: 15,
    width: 15,
  },
  btnStockCon: {
    height: 50,
    backgroundColor: "#fff",
    marginBottom: 5,
    // borderTopColor: "#aaa",
    // borderTopWidth: 2,
    // backgroundColor: '#fff',
    // marginHorizontal: 10,
  },
  btnSetCon: {
    // position: 'absolute',
    // bottom: 0,
    backgroundColor: '#F28647',
    width: width / 1.05,
    height: 31,
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'space-around',
    margin: 10,
  },
  txtStock: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
});
