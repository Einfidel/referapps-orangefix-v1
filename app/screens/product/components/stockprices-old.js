import React, {
  useState,
  useEffect,
} from 'react';

import { View, Text, TouchableOpacity, TextInput, Dimensions, StyleSheet } from 'react-native';

import { Container, Header, Left, Body, Right } from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import FastImage from 'react-native-fast-image';

// import sell from './../../styles/ProductScreenStyles/sellproductstyles';

const { width, height } = Dimensions.get('window');

export default function StockandPrice({ navigation, route }) {
  const [isClickedMenu, setIsClickedMenu] = useState(false);
  const [variations, setVariations] = useState([]);
  const [tmp, setTmp] = useState('');

  const [stock, setStock] = useState('');
  const [price, setPrice] = useState('');

  useEffect(() => {
    setVariations(route.params.variation);
    // console.log("route.params.variation[4]", route.params.variation[4])
    // console.log("route.params.variation[4].child", route.params.variation[4].child)
  }, [])

  const _renderVariations = () => {
    // console.log("variations", variations)
    if (variations.length > 0) {
      // console.log("variations[0].child.length", variations[0].child.length)
      if (variations[0].child.length > 0) {
        return variations.map((variation1, index1) => {
          // console.log("------------------")
          // console.log("variation1", index1 + ":", variation1)
          return variation1.child.map((variation2, index2) => {
            // console.log("variation2", variation2)
            return (
              <View key={index2}>
                <View style={sell.space} />
                <View style={sell.space} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                  <View style={sell.variationRow}>
                    <Text style={[sell.variationOption]}>
                      {variation1?.parent_name + ' ' + variation2.name}
                    </Text>
                  </View>
                  <View style={sell.variationRow}>
                    <View style={sell.optionInputContainer}>
                      <TextInput
                        style={sell.StockPriceText}
                        placeholder={'Stock'}
                        placeholderTextColor={'#7f8c8d'}
                        keyboardType={'decimal-pad'}
                        value={variation2.stock.toString()}
                        onChangeText={(newStock) => {
                          variation2.stock = newStock;

                          setTmp(newStock);
                          let tmp2 = tmp;
                        }}
                      />
                    </View>
                  </View>
                  <View style={sell.variationRow}>
                    <View style={sell.optionInputContainer}>
                      <TextInput
                        style={sell.StockPriceText}
                        placeholder={'price'}
                        placeholderTextColor={'#7f8c8d'}
                        keyboardType={'numeric'}
                        value={variation2.price.toString()}
                        onChangeText={(newPrice) => {
                          variation2.price = newPrice;

                          setTmp(newPrice);
                          let tmp2 = tmp;
                        }}
                      />
                    </View>
                  </View>
                </View>
              </View>
            )
          })
        })
      } else {
        return variations.map((option, index) => {
          console.log("option1 #" + index, option)
          return (
            <View key={index}>
              <View style={sell.space} />
              <View style={sell.space} />
              <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                <View style={sell.variationRow}>
                  <Text style={[sell.variationOption]}>
                    {option?.parent_name}
                  </Text>
                </View>
                <View style={sell.variationRow}>
                  <View style={sell.optionInputContainer}>
                    <TextInput
                      style={sell.StockPriceText}
                      placeholder={'Stock'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'decimal-pad'}
                      value={option.stock.toString()}
                      onChangeText={(newStock) => {
                        option.stock = newStock;

                        setTmp(newStock);
                        let tmp2 = tmp;
                      }}
                    />
                  </View>
                </View>
                <View style={sell.variationRow}>
                  <View style={sell.optionInputContainer}>
                    <TextInput
                      style={sell.StockPriceText}
                      placeholder={'price'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'numeric'}
                      value={option.price.toString()}
                      onChangeText={(newPrice) => {
                        option.price = newPrice;

                        setTmp(newPrice);
                        let tmp2 = tmp;
                      }}
                    />
                  </View>
                </View>
              </View>
            </View>
          )
        })
      }
    };
  };

  return (
    <Container>
      <Header style={sell.header}>
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <FastImage source={require('../../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 0 }}>
          <Text style={sell.headerText}> Stock And Price </Text>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity style={sell.rightView} />
        </Right>
      </Header>
      <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
        <View style={sell.space} />
        <View style={sell.space} />
        <View style={sell.space} />

        <View style={sell.stockAndprice1stView}>
          <View style={sell.stockAndpriceViewContainer}>
            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.stockAndPriceText}>Set variation stock and price here.</Text>

            <View style={sell.space} />
            <View style={sell.space} />
            <View style={{ borderBottomWidth: 1, borderBottomColor: '#e1e2e3' }} />
            <View style={sell.space} />

            <View style={{ flexDirection: 'row', justifyContent: 'space-around', paddingTop: 10 }}>
              <View style={sell.variationRow}>
                <Text style={sell.variationTitle1}>Variation</Text>
              </View>
              <View style={sell.variationRow}>
                <Text style={sell.variationTitle}>Stock</Text>
              </View>
              <View style={sell.variationRow}>
                <Text style={sell.variationTitle}>Price</Text>
              </View>
            </View>

            <View>
              <View style={sell.space} />
              <View style={sell.space} />

              {_renderVariations()}

              <View style={sell.space} />
              <View style={sell.space} />
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
      <View style={{ paddingHorizontal: 20, backgroundColor: '#ecf0f1', paddingBottom: 5 }}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            // variations.map((options1, index1) => {
            //   options1.child.map((options2, index2) => {
            //     console.log("variations #" + index2, options2);

            //   })
            //   // console.log("variations #" + index, variations);
            // })

            route.params.onSubmit(variations);
            navigation.goBack();
            navigation.goBack();
          }}
          style={sell.setStockAndpriceButton}
        >
          <Text style={sell.addOptionText}>Save</Text>
        </TouchableOpacity>
      </View>
    </Container>
  );
}

const sell = StyleSheet.create({
  header: {
    ...Platform.select({
      ios: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: -20
      },
      android: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: 30,
      }
    })
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 25,
  },
  stockAndprice1stView: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
  },
  stockAndpriceViewContainer: {
    backgroundColor: '#ffffff',
    // height: height / 5.3,
  },
  stockAndPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingLeft: 25,
  },
  space: {
    paddingTop: 5,
  },
  variationRow: {
    width: width / 3,
    justifyContent: 'space-around',
  },
  variationTitle1: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
    // fontFamily: 'italic',
  },
  variationTitle: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    // alignSelf: 'center',
    textAlign: 'left',
    // fontFamily: 'italic',
  },
  variationOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginLeft: 20,
    flexWrap: 'wrap',
    // color: '#231f20',
    // alignSelf: 'center',
  },
  variationOption2: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginLeft: 30,
    flexWrap: 'wrap',
    // color: '#231f20',
    // alignSelf: 'center',
  },
  optionInputContainer: {
    width: '80%',
    height: 36,
    borderWidth: 0.7,
    borderColor: '#e1e2e3',
    justifyContent: 'space-around',
  },
  StockPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  variationsOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  setStockAndpriceButton: {
    backgroundColor: '#00a14b',
    width: '100%',
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  addOptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
});
