import React, {
  useState,
  useEffect,
} from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { Header, Thumbnail, Item, Input, Left, Right, Body } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import * as Animatable from 'react-native-animatable';
import { MaterialIndicator } from 'react-native-indicators';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { styles, sell } from '../product/styles';
import Service from '../../components/api/service';

const rightArrow = require('../../assets/ProfileIcons/iconb_sub.png');
const info = require('../../assets/internal/icon_condition.png');
const sellImg = require('../../assets/sellProduct.png');

// const recurr =[{options: 'No' }, {options:'Yes'}]
const recurr = ['Yes', 'No'];

export default function ServiceScreen({ navigation, route }) {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [switch_status, setSwitch_status] = useState(false);
  const [user, setUser] = useState({});
  const [errors, setErrors] = useState({});
  const [responseData, setResponseData] = useState({});
  const [images, setImages] = useState([]);
  const [filePath, setFilePath] = useState({
    data: '',
    uri: '',
  });
  const [isLoading, setIsLoading] = useState(false);
  const [fileData, setFileData] = useState('');
  const [imageUpdated, setImageUpdated] = useState(false);
  const [nameFocused, setNameFocused] = useState(false);
  const [descriptionFocused, setDescriptionFocused] = useState(false);
  const [priceFocused, setPriceFocused] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [referrerModal, setReferrerModal] = useState(false);
  const [referrer_commission, setReferrer_commission] = useState('');
  const [option, setOption] = useState('');
  const [value, setValue] = useState('');
  const [categoryId, setCategoryId] = useState(1);
  const [optionModal, setOptionModal] = useState(false);
  const [loadingModal, setLoadingModal] = useState(false);


  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [discountedPriceModal, setDiscountedPriceModal] = useState(false);
  const [photoModal, setPhotoModal] = useState(false);
  const [referrerModal, setReferrerModal] = useState(false);
  const [packagingModal, setPackagingModal] = useState(false);
  const [conditionModal, setConditionModal] = useState(false);
  const [categoriesModal, setCategoriesModal] = useState(false);
  const [subCategoriesModal, setSubCategoriesModal] = useState(false);
  const [specificCategoryModal, setSpecificCategoryModal] = useState(false);
  const [shippingModal, setShippingModal] = useState(false);
  const [packaging_size, setPackagingSize] = useState([{ width: '' }, { length: '' }, { height: '' }]);

  useEffect(() => {
    onLoad();
  }, [])

  const onLoad = async () => {
    try {
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) {
        setUser(user);
      }
    } catch (error) {
      console.warn(error);
    }
  }

  const handleSubmitService = async () => {
    setLoadingModal(true);
    // handleShowModal();

    let data = {
      name: null,
    }

    await Service.createService(
      data,
      (res) => {
        // console.log(res);
        if (res.errors) {
          Alert.alert('Upload Error', res.errors.file[0]);
        } else {
          setLoadingModal(false);
          setResponseData(res);
          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  await handleShowModal();

                  setName('');
                  setDescription('');
                  setPrice('');
                  setReferrer_commission('');
                  setValue('');
                },
              },
            ],
            { cancelable: false }
          );
        }
      },
      (err) => alert(err)
    );
  };

  const handleClearFields = () => { };

  const handleShowModal = () => {
    setLoadingModal(true);
    setTimeout(() => {
      setLoadingModal(false);
    }, 1000);
  };

  const handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={loadingModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  const handleImagePicker = () => {
    if (images.length > 0) {
      return images.map((img, imgIndex) => {
        return (
          <TouchableOpacity activeOpacity={1} onPress={() => {
            setPhotoModal(true);
            setImageSelected(imgIndex);
          }}>
            <FastImage
              key={imgIndex}
              source={{ uri: img.uri }}
              style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        );
      });
    } else {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto();
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleUIAddPhoto = () => {
    if (images.length > 0 && images.length < 11) {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto()
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        setImages([...images, response]);
        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleChangePhoto = (index) => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        images.splice(index, 1, response);

        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleRemovePhoto = (index) => {
    images.splice(index, 1);
  };

  const renderPhotoModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={photoModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setPhotoModal(!photoModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    handleChangePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Change Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Remove Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    setPhotoModal(!photoModal);
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible} style={styles.modalCon}>
          {_renderReferrModal()}
        </Modal>
      </View>
    );
  };

  const _renderReferrModal = () => {
    // Fixed
    if (commissionIsFixed) {
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerFixedCommission}
                  onChangeText={(text) => handleFixedCommissionInputChange(text)}
                // maxLength={2}
                />
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else { // Percentage
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerPercentageCommission}
                  onChangeText={(text) => handlePercentageCommissionInputChange(text)}
                  maxLength={2}
                />
                <Text style={sell.percentText}>%</Text>
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  const _renderReferrerSaveButton = () => {
    if (!commissionIsFixed && (referrerPercentageCommission < 5 || referrerPercentageCommission > 50)) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      setModalVisible1(false);
      // setReferrerModal(false);
      // setLastCommission(referrerFixedCommission);
    }
  };

  const renderImages = () => {
    return images.map((img, i) => {
      return (
        <Image
          key={i}
          source={{ uri: img.uri }}
          style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
        />
      );
    });
  };

  return (
    <View style={sell.serviceContainer}>
      <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
        <Left>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
          </TouchableOpacity>
        </Left>
        <Body>
          <Text style={sell.headerText}> Sell Services </Text>
        </Body>
        <Right>
          <View style={sell.rightView} />
        </Right>
      </Header>
      <ScrollView style={{ flex: 1 }}>
        <View style={{ backgroundColor: '#ecf0f1', height: 210 }}>
          {images && images.length > 0 ? (
            renderImages()
          ) : (
            <TouchableOpacity
              onPress={() => {
                handleImagePicker();
              }}
              style={{ justifyContent: 'center' }}
            >
              <Image
                source={images.length === 0 ? sellImg : images}
                style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
              />
            </TouchableOpacity>
          )}
        </View>

        <View style={styles.concon}>
          <View style={styles.txtItem}>
            <Input
              style={styles.txtInputService}
              onChangeText={(name) => {
                // this.setState({ name: name })
              }}
              value={name}
              placeholder='Service Name'
              placeholderTextColor='#7f8c8d'
              maxLength={50}
              onFocus={() => {
                // if (errors.product_name) {
                //   this.setState({
                //     nameFocused: true,
                //   });
                // }
              }}
            />
          </View>
          <Text style={styles.txtLength}>{name.length}/50</Text>
        </View>

        <View style={styles.des}>
          <TextInput
            style={styles.txtInputDescription}
            onChangeText={(description) => {
              // this.setState({ description: description })
            }}
            value={description}
            placeholder='Description'
            editable={true}
            multiline={true}
            onFocus={() => {
              // if (errors.description) {
              //   this.setState({
              //     descriptionFocused: true,
              //   });
              // }
            }}
          />
        </View>
        <View style={styles.ref}>
          {renderModal()}
          <TouchableOpacity style={styles.referrBtn} onPress={() => setModalVisible(true)}>
            <Text style={sell.inputText}>Referrer Commission</Text>
            <View style={styles.setCommissionCon}>
              <Text style={styles.txtSetCommission}>
                {referrer_commission == '' ? 'Set Commission %' : referrer_commission + '%'}
              </Text>
              <Image source={rightArrow} style={styles.imgRight} />
            </View>
          </TouchableOpacity>
        </View>
        <View style={[styles.concon, { marginTop: 10 }]}>
          <TouchableOpacity onPress={() => {
            // setOptionModal(true)
          }} style={styles.imgRecurringCon}>
            <View style={styles.imgCon}>
              <Image source={info} style={styles.imgInfo} />
              <Text style={sell.inputText}>Recurring</Text>
            </View>
            <View style={styles.pickerCon}>
              <Text style={styles.txtValue}>{value}</Text>
              <Image source={rightArrow} style={styles.imgRight} />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.concons}>
          <View style={styles.priceCon}>
            <View style={{ flexDirection: 'row' }}>
              <View style={sell.spaceLeft} />
              <Thumbnail
                source={require('../../assets/internal/icon_price.png')}
                square
                small
                style={styles.cheese}
              />
              <View style={sell.spaceLeft} />
              <Text style={sell.inputText}>Price ({user.country_code === 'PH' ? '₱' : '$'})</Text>
            </View>
            <Item style={sell.input_title}>
              <Input
                style={sell.group_input_text}
                placeholder={'Price'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'decimal-pad'}
                onChangeText={(x) => {
                  // this.setState({ price: x })
                }}
                value={price}
                onFocus={() => {
                  // if (errors.price) {
                  //   this.setState({
                  //     priceFocused: true,
                  //   });
                  // }
                }}
              />
            </Item>
          </View>
        </View>
      </ScrollView>
      <View style={{ height: 50 }}>
        <TouchableOpacity
          // onPress={() => handleShowModal()}
          onPress={() => {
            handleSubmitService();
          }}
          style={styles.btnSell}
        >
          <Text style={styles.txtSell}>Sell</Text>
        </TouchableOpacity>
      </View>
      {handleLoading()}
    </View>
  );
}
