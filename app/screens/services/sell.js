import React, {
  useState,
  useEffect,
} from 'react';
import Moment from 'moment';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import { Root, Container, Header, Left, Body, Right, Item, Input, Toast } from 'native-base';

import Switch from 'react-native-switch-pro';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import { Picker } from '@react-native-picker/picker';
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux';
import { styles, sell } from '../product/styles';
import Service from '../../components/api/service';
import appleAuth from '@invertase/react-native-apple-authentication';
import Icon from '../../components/icons';
import { paddingTop } from 'styled-system';
import DateTimePicker from 'react-native-modal-datetime-picker';

const { width, height } = Dimensions.get('window');

export default function SellProductScreen({ navigation, route }) {
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [discountedPriceModal, setDiscountedPriceModal] = useState(false);
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [date, setDate] = useState(new Date().date);
  const [photoModal, setPhotoModal] = useState(false);
  const [referrerModal, setReferrerModal] = useState(false);
  const [packagingModal, setPackagingModal] = useState(false);
  const [conditionModal, setConditionModal] = useState(false);
  const [categoriesModal, setCategoriesModal] = useState(false);
  const [subCategoriesModal, setSubCategoriesModal] = useState(false);
  const [specificCategoryModal, setSpecificCategoryModal] = useState(false);
  const [shippingModal, setShippingModal] = useState(false);
  const [packaging_size, setPackagingSize] = useState([{ width: '' }, { length: '' }, { height: '' }]);

  const [user, setUser] = useState(null);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [finePrint, setFinePrint] = useState('');
  const [contactDetails, setContactDetails] = useState('');
  const [category_id, setCategoryId] = useState(1);
  const [category, setCategory] = useState('Services');
  const [subcategory, setSubcategory] = useState('');
  const [specificcategory, setSpecificCategory] = useState('');
  const [attributes, setAttributes] = useState([]);
  const [hasVariation, setHasVariation] = useState(false);
  const [variationType, setVariationType] = useState('branch');
  const [variationOptions, setVariationOptions] = useState([]);
  const [price, setPrice] = useState(0);
  const [hasDiscount, setHasDiscount] = useState(false);
  const [discountIsFixed, setDiscountIsFixed] = useState(false);
  const [discountFixedAmmount, setDiscountFixedAmmount] = useState(0);
  const [discountPercentageAmmount, setDiscountPercentageAmmount] = useState(0);
  const [discountedPrice, setDiscountedPrice] = useState(0);
  const [isRecurring, setIsRecurring] = useState(false);
  const [canExpire, setCanExpire] = useState(false);
  const [expirationDate, setExpirationDate] = useState('');
  const [commissionIsFixed, setCommissionIsFixed] = useState(false);
  const [referrerFixedCommission, setReferrerFixedCommission] = useState('');
  const [referrerPercentageCommission, setReferrerPercentageCommission] = useState('');
  const [lastCommission, setLastCommission] = useState('');
  const [text, setText] = useState('');
  const [value, setValue] = useState('');
  const [images, setImages] = useState([]);
  const [main_categories, setMainCategories] = useState([]);

  const [subCategoriesList, setSubCategoriesList] = useState([]);
  const [specificCategoriesList, setSpecificCategoriesList] = useState([]);
  const [categoryAttributes, setCategoryAttributes] = useState([]);

  // const commissionInput = createRef();

  const [filePath, setFilePath] = useState(null);
  const [imageUpdated, setImageUpdated] = useState(null);
  const [fileData, setFileData] = useState(null);

  const [responseData, setResponseData] = useState(null);
  const [loadingModal, setLoadingModal] = useState(null);
  const [imageSelected, setImageSelected] = useState(null);

  const [attributeTmp, setAttributeTmp] = useState("");

  const [submitTapped, setSubmitTapped] = useState(false);

  const [iosMargin, setiosMargin] = useState(0);



  const [createServiceData, setCreateServiceData] = useState([
    { userDataId: '' },
    { name: '' },
    { commissionIsFixed: false },
    { referrerFixedCommission: '' },
    { referrerPercentageCommission: '' },
    { to_send_attributes: [] },
    { category_id: '' },
    { description: '' },
    { finePrint: '' },
    { contactDetails: '' },
    { country_code: '' },
    { variationOptions: [] },
    { price: '' },
    { hasDiscount: false },
    { discountAmmount: '' },
    { images: [] },
  ]);

  //errors
  const [fieldErrors, setFieldErrors] = useState([
    { imageEmpty: false },
    { imageMorethan5mb: false },
    { nameEmpty: false },
    { descriptionEmpty: false },
    { commission: false },
    { mainCategoryEmpty: false },
    { conditionEmpty: false },
    { weightEmpty: false },
    { packageSizeEmpty: false },
  ])

  const [currency, setCurrency] = useState(null);

  useEffect(() => {
    onLoad();
  }, [])

  useEffect(() => {
    // console.log("modalVisible1", modalVisible1)
    let percentage = parseFloat(referrerPercentageCommission)
    // console.log("percentage", percentage)
    if (percentage < 5) {
      // console.log("referrerPercentageCommission is less than 5", referrerPercentageCommission)
      setReferrerPercentageCommission('5')
    } else if (percentage < 50) {
      // console.log("referrerPercentageCommission is greater than 5", referrerPercentageCommission)
      setReferrerPercentageCommission(percentage.toString())
    }
    if (percentage > 50) {
      // console.log("referrerPercentageCommission is greater than 50", referrerPercentageCommission)
      setReferrerPercentageCommission('50')
    } else if (percentage > 5) {
      // console.log("referrerPercentageCommission is less than 50", referrerPercentageCommission)
      setReferrerPercentageCommission(percentage.toString())
    }
  }, [modalVisible1])

  const onLoad = async () => {
    try {
      appleAuth.isSupported ? setiosMargin(0) : setiosMargin(0);
      Moment.locale('en');

      // Set User
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) setUser(user);

      // Set Currency
      if (user?.data?.country_code === "PH") {
        setCurrency("₱");
      } else if (user?.data?.country_code === "US") {
        setCurrency("$");
      } else {
        setCurrency("");
      }

      await Service.getSubCategory(
        category_id,
        (res) => {
          setSubcategory('');
          setSpecificCategory('');
          setCategoryAttributes([]);
          setCategoryId(1);
          setSubCategoriesList(res.sub.data);
          // console.log("cat.category_name", cat.category_name, 1);
          setCategory(category);
          setCategoriesModal(false);
        },
        (err) => console.log(err)
      );
    } catch (error) {
      console.warn(error);
    }
  }

  const handleImagePicker = () => {
    if (images.length > 0) {
      return images.map((img, imgIndex) => {
        return (
          <TouchableOpacity activeOpacity={1} onPress={() => {
            setPhotoModal(true);
            setImageSelected(imgIndex);
          }}>
            <FastImage
              key={imgIndex}
              source={{ uri: img.uri }}
              style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
        );
      });
    } else {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto();
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleUIAddPhoto = () => {
    if (images.length > 0 && images.length < 11) {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto()
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        setImages([...images, response]);
        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleChangePhoto = (index) => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        images.splice(index, 1, response);

        setFilePath(response.path);
        setImageUpdated(true);
        setFileData(response.data);
      }
    });

    fieldErrors.imageMorethan5mb = false;

    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
      }
    });
  };

  const handleRemovePhoto = (index) => {
    images.splice(index, 1);
  };

  const renderPhotoModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={photoModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setPhotoModal(!photoModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    handleChangePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Change Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected);
                    setPhotoModal(!photoModal);
                  }}
                >
                  Remove Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    setPhotoModal(!photoModal);
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={modalVisible1} style={styles.modalCon}>
          {_renderReferrModal()}
        </Modal>
      </View>
    );
  };

  const handlePercentageCommissionInputChange = (text) => {
    setReferrerPercentageCommission(text.replace(/[^0-9]/g, ''));
  };

  const handleFixedCommissionInputChange = (text) => {
    setReferrerFixedCommission(text);
  };

  const _renderReferrModal = () => {
    // Fixed
    if (commissionIsFixed) {
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerFixedCommission}
                  onChangeText={(text) => handleFixedCommissionInputChange(text)}
                // maxLength={2}
                />
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else { // Percentage
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            setModalVisible1(!modalVisible1);
          }}
        >
          <View style={styles.containerModal}>
            <View style={sell.ReferrerModalConatiner}>
              <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
              <Item>
                <Input
                  style={sell.SetCommission}
                  placeholder={'Set Commission'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'number-pad'}
                  value={referrerPercentageCommission}
                  onChangeText={(text) => handlePercentageCommissionInputChange(text)}
                  maxLength={2}
                />
                <Text style={sell.percentText}>%</Text>
              </Item>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => _renderReferrerSaveButton()}
                style={sell.referrerSaveBtn}
              >
                <Text style={sell.SaveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    }
  };

  const _renderReferrerSaveButton = () => {
    if (!commissionIsFixed && (referrerPercentageCommission < 5 || referrerPercentageCommission > 50)) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      setModalVisible1(false);
      // setReferrerModal(false);
      // setLastCommission(referrerFixedCommission);
    }
  };

  const renderPrice = () => {
    if (variationType !== 'deal' || (variationType === 'deal' && !hasVariation)) {
      return (
        <View style={sell.white}>
          <View style={sell.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginLeft: 25 }} />
              <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
              <Text style={sell.inputText}>Price</Text>
            </View>
            <Item style={sell.input_title}>
              <View style={{ paddingLeft: 20 }}>
                <Text style={sell.Kg}>{currency}</Text>
              </View>
              <Input
                style={sell.group_input_text}
                placeholder={'Price'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'decimal-pad'}
                onChangeText={(price) => {
                  let tmpPrice = parseFloat(price);
                  if (tmpPrice * 0 != 0) {
                    if (!discountIsFixed) {
                      setDiscountedPrice(parseFloat(1) * (parseFloat(discountPercentageAmmount) * 0.01));
                    } else if (discountIsFixed) {
                      if (discountFixedAmmount > 1) {
                        setDiscountFixedAmmount(0);
                        setDiscountedPrice(0);
                      } else {
                        setDiscountedPrice(parseFloat(1) - parseFloat(discountFixedAmmount));
                      }
                    }
                    setPrice(1);
                  } else {
                    if (!discountIsFixed) {
                      setDiscountedPrice(parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01));
                    } else if (discountIsFixed) {
                      if (discountFixedAmmount > price) {
                        setDiscountFixedAmmount(price);
                        setDiscountedPrice(0);
                      } else {
                        setDiscountedPrice(parseFloat(price) - parseFloat(discountFixedAmmount));
                      }
                    }
                    setPrice(tmpPrice);
                  }
                }}
                value={price.toString()}
              />
            </Item>
          </View>
        </View>
      )
    }
  }

  const renderDiscountType = () => {
    if (hasDiscount && (variationType !== 'deal' || (variationType === 'deal' && !hasVariation))) {
      return (
        // DISCOUNT TYPE
        <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
          <TouchableOpacity
            activeOpacity={0.4}
            // onPress={() => setModalVisible1(true)}
            style={sell.inputPickerContainer}
          >
            <View style={sell.inputPickerContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                <Text style={sell.inputText}>Discount Type</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <Picker
                  selectedValue={discountIsFixed}
                  mode='dropdown'
                  style={sell.datePicker}
                  itemStyle={{
                    // fontFamily: 'Roboto',
                    fontSize: 12,
                    color: '#231f20',
                  }}
                  onValueChange={(value) => {
                    setDiscountIsFixed(value);
                    if (!value) {
                      setDiscountedPrice(parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01));
                    } else if (discountIsFixed) {
                      if (discountFixedAmmount > price) {
                        setDiscountFixedAmmount(price);
                      } else {
                        setDiscountedPrice(parseFloat(price) - parseFloat(discountFixedAmmount));
                      }
                    }
                  }}
                >
                  <Picker.Item label='% Percent' value={false} />
                  <Picker.Item label='Fixed' value={true} />
                </Picker>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      )
    };
  }

  const renderDiscountAmmount = () => {
    if (hasDiscount && (variationType !== 'deal' || (variationType === 'deal' && !hasVariation))) {
      return (
        <View style={sell.whiteBorder}>
          <TouchableOpacity
            activeOpacity={0.4}
            onPress={() => setDiscountedPriceModal(true)}
            style={sell.inputContainer}
          >
            <View style={sell.inputContainer}>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ marginLeft: 25 }} />
                <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                <Text style={sell.inputText}>Discount Amt.</Text>
              </View>
              <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                <Text style={sell.hintText2}>
                  {getDiscountText()}
                </Text>
                <View style={sell.spaceRight} />
                <FastImage
                  source={require('../../assets/icon_sub.png')}
                  style={sell.openIcon}
                  tintColor={'#7f8c8d'}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  }

  const renderDiscountedPrice = () => {
    if (hasDiscount && (variationType !== 'deal' || (variationType === 'deal' && !hasVariation))) {
      return (
        // DISCOUNTED PRICE 
        <View style={sell.white}>
          <View style={sell.inputContainer}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ marginLeft: 25 }} />
              <FastImage source={require('../../assets/internal/icon_price.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
              <Text style={sell.inputText}>New Price</Text>
            </View>
            <Item style={sell.input_title}>
              <View style={{ paddingLeft: 50 }}>
                <Text style={sell.Kg}>{currency} {discountedPrice}</Text>
              </View>
            </Item>
          </View>
        </View>
      );
    }
  }

  const renderDiscountedPriceModal = () => {
    if (discountIsFixed) {// Fixed
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Modal animationType='slide' transparent={true} visible={discountedPriceModal} style={styles.modalCon}>
            <TouchableWithoutFeedback
              onPress={() => {
                setDiscountedPriceModal(!discountedPriceModal);
              }}
            >
              <View style={styles.containerModal}>
                <View style={sell.ReferrerModalConatiner}>
                  <Text style={sell.ReferrerModalHeaderText}>Set Discounted Price (1 - {price});</Text>
                  <Item>
                    <Input
                      style={sell.SetCommission}
                      placeholder={'Set Discount'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      value={discountFixedAmmount}
                      onChangeText={(text) => {
                        setDiscountFixedAmmount(text);
                      }}
                    // maxLength={2}
                    />
                  </Item>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => renderDiscountedPriceSaveBtn()}
                    style={sell.referrerSaveBtn}
                  >
                    <Text style={sell.SaveText}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    } else { // Percentage
      return (
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Modal animationType='slide' transparent={true} visible={discountedPriceModal} style={styles.modalCon}>
            <TouchableWithoutFeedback
              onPress={() => {
                setDiscountedPriceModal(!discountedPriceModal);
              }}
            >
              <View style={styles.containerModal}>
                <View style={sell.ReferrerModalConatiner}>
                  <Text style={sell.ReferrerModalHeaderText}>Set Discounted Price(1% - 100%)</Text>
                  <Item>
                    <Input
                      style={sell.SetCommission}
                      placeholder={'Set Discount'}
                      placeholderTextColor={'#7f8c8d'}
                      keyboardType={'number-pad'}
                      value={discountPercentageAmmount}
                      onChangeText={(text) => {
                        setDiscountPercentageAmmount(text.replace(/[^0-9]/g, ''));
                      }}
                      maxLength={2}
                    />
                    <Text style={sell.percentText}>%</Text>
                  </Item>
                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => renderDiscountedPriceSaveBtn()}
                    style={sell.referrerSaveBtn}
                  >
                    <Text style={sell.SaveText}>Save</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </Modal>
        </View>
      );
    }
  };

  const renderDiscountedPriceSaveBtn = () => {
    console.log("discountIsFixed", discountIsFixed)
    if (!discountIsFixed && (parseFloat(discountPercentageAmmount) < 1 || parseFloat(discountPercentageAmmount) > 100)) {
      return alert('Invalid Percentage! (1% - 100%)');
    } else if (!discountIsFixed) {
      console.log("parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01)", parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01))
      let tmpPrice = parseFloat(price) - (parseFloat(price) * (parseFloat(discountPercentageAmmount) * 0.01));
      if (tmpPrice * 0 != 0) {
        setDiscountedPrice(0);
      } else {
        setDiscountedPrice(tmpPrice);
      }
      setDiscountedPriceModal(false);
    }

    if (discountIsFixed && (parseFloat(discountFixedAmmount) < 1 || parseFloat(discountFixedAmmount) > parseFloat(price))) {
      return alert('Invalid Ammount! (1 - ' + price + ')');
    } else if (discountIsFixed) {
      let tmpPrice = parseFloat(price) - parseFloat(discountFixedAmmount);
      if (tmpPrice * 0 != 0) {
        setDiscountedPrice(0);
      } else {
        setDiscountedPrice(tmpPrice);
      }
      setDiscountedPriceModal(false);
    }
  };

  const getDiscountText = () => {
    // console.log(discountFixedAmmount);
    if (discountIsFixed) { // Fixed
      if (discountFixedAmmount == 0) {
        return 'Set Discount';
      } else {
        return discountFixedAmmount;
      }
    } else { // Percentage
      if (discountPercentageAmmount == 0) {
        return 'Set Discount ' + "%";
      } else {
        return discountPercentageAmmount + "%";
      }
    }
  }

  const renderSubCategoriesModal = (data) => {
    // console.log("Sub Category ID", category_id);
    // console.log("Sub Category Name", subcategory);
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={subCategoriesModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSubCategoriesModal(!subCategoriesModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Sub Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {subCategoriesList.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getSpecificCategory(
                            cat.id,
                            (res) => {
                              if (subcategory != cat.category_name) {
                                setCategoriesModal(false);
                                setSpecificCategory('');
                                setCategoryAttributes([]);
                                setCategoryId(cat.id);
                                // console.log("getSpecificCategory", res.data);
                                setSubcategory(cat.category_name);
                                setSpecificCategoriesList(res.data);
                              }
                              setSubCategoriesModal(false);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSpecificCategoriesModal = (data) => {
    // console.log("Specific Category ID", category_id);
    // console.log("Specific Category Name", specificcategory);
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          animationType='slide'
          transparent={false}
          visible={specificCategoryModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSpecificCategoryModal(!specificCategoryModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PackagingModalConatiner}>
                <Text style={{ fontSize: 13, fontWeight: 'normal' }}>Select Specific Category</Text>
                <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
                  <View style={{ padding: 10 }} />
                  {specificCategoriesList.map((cat, i) => {
                    return (
                      <TouchableOpacity
                        onPress={async () => {
                          await Service.getCategoryAttribute(
                            cat.id,
                            (res) => {
                              setAttributes([]);
                              setCategoryAttributes([]);
                              setCategoryId(cat.id);
                              setSpecificCategory(cat.category_name);
                              // setCategoryAttributes(res.data);
                              setSpecificCategoryModal(false);

                              remapCategoryAttributes(res.data, cat.id);
                            },
                            (err) => console.log(err)
                          );
                        }}
                        key={i}
                        style={{ flex: 1, padding: 10 }}
                      >
                        <Text>{cat.category_name}</Text>
                      </TouchableOpacity>
                    );
                  })}
                  <View style={{ padding: 20 }} />
                </ScrollView>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const remapCategoryAttributes = (data, categoryId) => {
    data.map((attribute, index) => {
      attribute.value = "";
      attribute.attribute = attribute.attribute_name;
      attribute.required = attribute.is_required;
      attribute.category_id = categoryId;
      categoryAttributes[index] = attribute;

      if ((data.length - 1) == index) {
        setCategoryAttributes(categoryAttributes);
      }
    });
  }

  const renderCategoryAttributes = () => {
    // console.log("categoryAttributes", categoryAttributes);
    // console.log("check");
    return categoryAttributes.map((attribute, index) => {
      return (
        <View>
          {renderAttributeErrors(attribute)}
          <View style={sell.whiteBorder}>
            <TouchableOpacity activeOpacity={0.4}
            // onPress={() => setCategoriesModal(true)}
            >
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 10 }} />
                  <Text style={sell.inputText}>{attribute.attribute_name}</Text>
                </View>
                <View style={{}}>
                  <Input
                    style={sell.group_input_text}
                    placeholder={'Set ' + attribute.attribute_name}
                    placeholderTextColor={'#7f8c8d'}
                    onChangeText={(value) => {
                      attribute.value = value;
                      setAttributeTmp(value);
                    }}
                  // value={attribute.value}
                  />
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  const handleDatePicked = (date) => {
    setExpirationDate(Moment(date).format('YYYY-MM-DD'));
    // console.log("expirationDate", expirationDate)
    setIsDateTimePickerVisible(false)
  };

  const handleSubmit = async () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // reset all errors to false
    fieldErrors.map((error) => {
      error = false;
    });

    // If Image Empty
    if (images.length <= 0) {
      fieldErrors.imageEmpty = true;
      submitCancelled = true;
    }

    // If Image more than 5mb
    images.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        fieldErrors.imageMorethan5mb = true;
        submitCancelled = true;
      }
    });

    // If Name Empty
    if (!name.trim()) {
      fieldErrors.nameEmpty = true;
      submitCancelled = true;
    }

    // If Description Empty
    if (!description.trim()) {
      fieldErrors.descriptionEmpty = true;
      submitCancelled = true;
    }

    // If fieldErrors Empty
    if (!finePrint.trim()) {
      fieldErrors.descriptionEmpty = true;
      submitCancelled = true;
    }

    // If contactDetails Empty
    if (!contactDetails.trim()) {
      fieldErrors.descriptionEmpty = true;
      submitCancelled = true;
    }

    // If Commision Empty
    // console.log("referrerFixedCommission", referrerFixedCommission)
    if (commissionIsFixed) {
      if (!referrerFixedCommission.trim()) {
        fieldErrors.commissionEmpty = true;
        submitCancelled = true;
      }
    } else {
      if (!referrerPercentageCommission.trim()) {
        fieldErrors.commissionEmpty = true;
        submitCancelled = true;
      }
    }

    // console.log("category_id", category_id);
    // If Main Category Empty
    if (category_id == '' || category_id == null) {
      fieldErrors.mainCategoryEmpty = true;
      submitCancelled = true;
    }

    if (!hasVariation || (hasVariation && variationType === 'branch')) {
      if (price === 0 || price == null) {
        fieldErrors.price = true;
        submitCancelled = true;
      }
    }

    if (hasDiscount) {
      if (discountIsFixed) {
        if (discountFixedAmmount === 0 || discountFixedAmmount == null) {
          fieldErrors.price = true;
          submitCancelled = true;
        }
      } else {
        if (discountPercentageAmmount === 0 || discountPercentageAmmount == null) {
          fieldErrors.price = true;
          submitCancelled = true;
        }
      }
    }

    // If Required attributes are empty
    // console.log("checking categoryAttributes");
    categoryAttributes.map((attribute, index) => {
      // console.log("checking", attribute.attribute_name);
      if (!attribute.value.trim() && attribute.is_required == "yes") {
        // console.log(attribute.attribute_name, "is empty");
        submitCancelled = true;
      }
    });

    // If hasVariation and variationOptions Empty
    if (hasVariation) {
      if (variationOptions.length <= 0 || variationOptions === null) {
        fieldErrors.variationOptions = true;
        submitCancelled = true;
      }
    }

    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    createServiceData.user = user;
    createServiceData.name = name;
    createServiceData.commissionIsFixed = commissionIsFixed;
    createServiceData.referrerFixedCommission = referrerFixedCommission;
    createServiceData.referrerPercentageCommission = referrerPercentageCommission;
    createServiceData.to_send_attributes = categoryAttributes;
    createServiceData.category_id = category_id;
    createServiceData.description = description;
    createServiceData.fine_prints = finePrint;
    createServiceData.contact_details = contactDetails;
    createServiceData.country_code = user.data.country_code;
    createServiceData.hasVariation = hasVariation;
    createServiceData.variationOptions = variationOptions;
    createServiceData.price = parseFloat(price);
    createServiceData.is_recurring = isRecurring ? 'yes' : 'no';
    if (canExpire) {
      createServiceData.expiration_date = Moment(expirationDate).format('YYYY-MM-DD');
    }

    createServiceData.expiration_date = null;

    createServiceData.hasDiscount = hasDiscount;
    if (hasDiscount) {
      if (discountIsFixed) { // Old Price divided by Fixed Discount is = to Percntage Discount
        createServiceData.discountFixedAmmount = discountFixedAmmount;
        createServiceData.discountPercentageAmmount = price / discountFixedAmmount
      } else {// Old Price divided by Fixed Discount is = to Percntage Discount
        createServiceData.discountFixedAmmount = price * discountPercentageAmmount
        createServiceData.discountPercentageAmmount = discountPercentageAmmount;
      }
    }
    createServiceData.discountedPrice = discountedPrice;

    createServiceData.images = images;

    console.log("createServiceData", createServiceData)
    console.log("createServiceData.userDataId", createServiceData.user.data.id);
    console.log("createServiceData.name", createServiceData.name);
    console.log("createServiceData.commissionIsFixed", createServiceData.commissionIsFixed);
    console.log("createServiceData.referrerFixedCommission", createServiceData.referrerFixedCommission);
    console.log("createServiceData.referrerPercentageCommission", createServiceData.referrerPercentageCommission);
    console.log("createServiceData.to_send_attributes", createServiceData.to_send_attributes);
    console.log("createServiceData.category_id", createServiceData.category_id);
    console.log("createServiceData.description", createServiceData.description);
    console.log("createServiceData.fine_prints", createServiceData.finePrint);
    console.log("createServiceData.contact_details", createServiceData.contactDetails);
    console.log("createServiceData.hasVariation", createServiceData.hasVariation);
    console.log("createServiceData.variationOptions", createServiceData.variationOptions);
    console.log("createServiceData.price", createServiceData.price);
    console.log("createServiceData.is_recurring", createServiceData.is_recurring);
    console.log("createServiceData.hasDiscount", createServiceData.hasDiscount);
    console.log("createServiceData.discountFixedAmmount", createServiceData.discountFixedAmmount);
    console.log("createServiceData.discountPercentageAmmount", createServiceData.discountPercentageAmmount);
    console.log("createServiceData.images", createServiceData.images);

    // if no errors then go
    setSubmitLoading(true);

    console.log("tmp check");

    try {
      await Service.createService(
        createServiceData,
        (res) => {
          if (res.errors) {
            console.warn('Submit Error', JSON.stringify(res.errors, null, '\t'));
            setSubmitLoading(false);
            Alert.alert('Upload Error', 'Please try again');
          } else {
            setResponseData(res);
            // setLoadingModal(false);

            Alert.alert(
              'Succesfully!',
              res.msg,
              [
                {
                  text: 'OK',
                  onPress: async () => {
                    // await handleShowModal();
                    setName('');
                    setDescription('');
                    setFinePrint('');
                    setContactDetails('');
                    setCategoryId('');
                    setCategory('');
                    setSubcategory('');
                    setSpecificCategory('');
                    setHasVariation(false);
                    setVariationType('branch')
                    setVariationOptions([]);
                    setPrice(0);
                    setHasDiscount(false);
                    setDiscountIsFixed(false);
                    setDiscountFixedAmmount(1);
                    setDiscountPercentageAmmount(1);
                    setDiscountedPrice('');
                    setIsRecurring(false);
                    setReferrerFixedCommission('');
                    setReferrerPercentageCommission('');
                    setLastCommission('');
                    setText('');
                    setValue('');
                    setImages([]);
                    setMainCategories([]);
                    setSubCategoriesList([]);
                    setSpecificCategoriesList([]);
                    setCategoryAttributes([]);
                    setAttributes([]);

                    setCurrency(null);
                    setCommissionIsFixed(false);

                    setFilePath(null);
                    setImageUpdated(null);
                    setFileData(null);

                    setResponseData(null);
                    setLoadingModal(null);

                    setPhotoModal(false);
                    setImageSelected(null);

                    setSubmitLoading(false);
                    setSubmitTapped(false);

                    setCreateProductData([
                      { userDataId: '' },
                      { name: '' },
                      { commissionIsFixed: false },
                      { referrerFixedCommission: '' },
                      { referrerPercentageCommission: '' },
                      { to_send_attributes: [] },
                      { category_id: '' },
                      { description: '' },
                      { finePrint: '' },
                      { contactDetails: '' },
                      { variationOptions: [] },
                      { price: '' },
                      { hasDiscount: false },
                      { discountAmmount: '' },
                      { images: [] },
                    ]);
                  },
                },
              ],
              { cancelable: false }
            );
          }
        },
        (err) => {
          console.warn('Error', err.message)
          setSubmitLoading(false);
        },
      );
    } catch (error) {
      Alert.alert('Upload Error', 'Please try again');
      setSubmitLoading(false);
    }
  };

  const handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={submitLoading}
        onRequestClose={() => {
          // console.log('Modal has been closed.');
        }}>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              // backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  const getCommissionText = () => {
    let percent;

    // Fixed
    if (commissionIsFixed) {
      if (referrerFixedCommission == '') {
        return 'Set Commission';
      } else {
        return referrerFixedCommission;
      }
    } else { // Percentage
      if (referrerPercentageCommission == '') {
        return 'Set Commission ' + "%";
      } else {
        return referrerPercentageCommission + "%";
      }
    }
  }

  const renderImageError = () => {
    if (submitTapped) {
      if (images.length <= 0) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Atleast 1 Image is required.
            </Text>
          </View>
        );
      } else if (fieldErrors.imageMorethan5mb) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * The maximum size for an image is 5mb
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderNameError = () => {
    if (submitTapped) {
      if (!name.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Name is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderDescriptionError = () => {
    if (submitTapped) {
      if (!description.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Description is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderFinePrintError = () => {
    if (submitTapped) {
      if (!finePrint.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Fine Print is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderContactDetailsError = () => {
    if (submitTapped) {
      if (!contactDetails.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Contact Details is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderCommissionError = () => {
    if (submitTapped) {
      if (commissionIsFixed) {
        if (!referrerFixedCommission.trim()) {
          return (
            <View style={sell.fieldErrors}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * Referrer Commission is required.
              </Text>
            </View>
          );
        }
      } else {
        if (!referrerPercentageCommission.trim()) {
          return (
            <View style={sell.fieldErrors}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * Referrer Commission is required.
              </Text>
            </View>
          );
        }
      }
    }
    return <View />;
  };

  const renderVariationError = () => {
    if (submitTapped && hasVariation) {
      if (variationOptions.length <= 0 || variationOptions === null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Variations are required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderPriceError = () => {
    if (submitTapped && (!hasVariation || (hasVariation && variationType === 'branch'))) {
      if (price === 0 || price == null) {
        return (
          <View style={[sell.fieldErrors, { paddingTop: 0 }]}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Price is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderDiscountedPriceError = () => {
    if (submitTapped && hasDiscount) {
      if (discountedPrice === 0 || discountedPrice == null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Discounted Price is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderMainCategoryError = () => {
    if (submitTapped) {
      if (category_id == '' || category_id == null) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Main Category is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderAttributeErrors = (currentAttribute) => {
    let tmp = attributeTmp;

    return categoryAttributes.map((attribute, index) => {
      if (submitTapped) {
        if (!attribute.value.trim() && attribute.is_required == "yes" && currentAttribute == attribute) {
          return (
            <View style={sell.fieldErrors}>
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#ff1a1a',
                }}
              >
                * {attribute.attribute_name} is required.
              </Text>
            </View>
          );
        }
      }
      return <View />;
    });
  };

  return (
    <Root>
      <Container style={sell.container}>
        <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
          <Left>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <FastImage source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} resizeMode={FastImage.resizeMode.contain} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={sell.headerText}> Sell Service </Text>
          </Body>
          <Right>
            <View style={sell.rightView} />
          </Right>
        </Header>
        {handleLoading()}
        <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
          <View>

            {renderPhotoModal()}

            {renderImageError()}
            <View style={sell.uploadContainer}>
              <ScrollView horizontal={true} scrollEnabled={true} style={{ width: width / 2 - 2 }}>
                {handleImagePicker()}
                {handleUIAddPhoto()}
              </ScrollView>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {renderNameError()}
            <View style={sell.whiteName}>
              <View style={{ flexDirection: 'row', height: hp('5.8%') }}>
                <View style={{ flexDirection: 'column', width: wp('90%') }}>
                  <Input
                    style={{ fontSize: hp('1.7%'), marginLeft: 15 }}
                    onChangeText={(name) => setName(name)}
                    value={name}
                    placeholder='Product Name'
                    placeholderTextColor='#7f8c8d'
                    maxLength={50}
                  />
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                  <Text style={sell.Length50}>{name.length}/50</Text>
                </View>
              </View>
            </View>

            {renderDescriptionError()}
            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(description) => setDescription(description)}
                    value={description}
                    placeholder='Description'
                    placeholderTextColor='#7f8c8d'
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            {renderFinePrintError()}
            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(finePrint) => setFinePrint(finePrint)}
                    value={finePrint}
                    placeholder='Fine Print'
                    placeholderTextColor='#7f8c8d'
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            {renderContactDetailsError()}
            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(contactDetails) => setContactDetails(contactDetails)}
                    value={contactDetails}
                    placeholder='Contact Details'
                    placeholderTextColor='#7f8c8d'
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />


            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Referer Commission</Text>
            </View>

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Commission  Type</Text>
                  </View>
                  {/* <View style={{ flexDirection: 'row' }}>
                    <Text style={[sell.hintText2, { marginLeft: 100 }]}>{commissionIsFixed ? "Fixed" : "Percentage"}</Text>
                  </View> */}
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={commissionIsFixed}
                      mode='dropdown'
                      style={[sell.datePicker, { width: height / 5, }]}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setCommissionIsFixed(value);
                      }}
                    >
                      <Picker.Item label='% Percentage' value={false} />
                      <Picker.Item label='Fixed' value={true} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {renderCommissionError()}
            <View style={sell.whiteBorder}>
              {renderReferrerModal()}
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => setModalVisible1(true)}
                style={sell.inputContainer}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Referer Commission</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>
                      {getCommissionText()}
                    </Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Categories</Text>
            </View>

            {/* MAIN CATEGORY */}
            <View style={sell.whiteBorder}>
              <View>
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_category.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Category</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{category ? category : 'Set Category'}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                      resizeMode={FastImage.resizeMode.contain}
                    />
                  </View>
                </View>
              </View>
            </View>

            {/* SUB CATEGORY */}
            {renderSubCategoriesModal()}
            {category && subCategoriesList.length > 0 ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    setSubCategoriesModal(true);
                  }}
                >
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Sub Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {subcategory ? subcategory : 'Set Sub Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            {/* SPECIFIC CATEGORY */}
            {renderSpecificCategoriesModal()}
            {subcategory && specificCategoriesList.length > 0 ? (
              <View style={sell.whiteBorder}>
                <TouchableOpacity activeOpacity={0.4} onPress={() => {
                  setSpecificCategoryModal(true)
                }}>
                  <View style={sell.inputContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 10 }} />
                      <Text style={sell.inputText}>Specific Category</Text>
                    </View>
                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>
                        {specificcategory ? specificcategory : 'Set Specific Category'}
                      </Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            ) : null}

            {/* ATTRIBUTES */}
            {categoryAttributes.length > 0 ? renderCategoryAttributes() : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {/* VARIATIONS */}
            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Variations</Text>
            </View>

            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Has Variation</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={hasVariation}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setHasVariation(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {hasVariation ?
              <>
                {renderVariationError()}
                <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
                  <TouchableOpacity
                    activeOpacity={0.4}
                    // onPress={() => setModalVisible1(true)}
                    style={sell.inputPickerContainer}
                  >
                    <View style={sell.inputPickerContainer}>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ marginLeft: 25 }} />
                        <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                        <Text style={sell.inputText}>Variation Type</Text>
                      </View>
                      <View style={{ flexDirection: 'row' }}>
                        <Picker
                          selectedValue={variationType}
                          mode='dropdown'
                          style={[sell.datePicker, { width: height / 5, }]}
                          itemStyle={{
                            // fontFamily: 'Roboto',
                            fontSize: 12,
                            color: '#231f20',
                          }}
                          onValueChange={(value) => {
                            console.log('value', value);
                            setVariationType(value);
                          }}
                        >
                          <Picker.Item label='Branches' value={'branch'} />
                          <Picker.Item label='Service Deals' value={'deal'} />
                        </Picker>
                      </View>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={sell.white}>
                  <TouchableOpacity
                    style={sell.inputContainer}
                    activeOpacity={0.4}
                    /**
                    navigation.navigate('ProductVariations', {
                      onAdd: (variations) => {
                        setVariationOptions(variations);
                      },
                    })} */

                    onPress={() => {
                      let data = {
                        variationType: variationType,
                        price: price,
                        variationOptions: variationOptions,
                      };
                      navigation.navigate('ServiceVariations', {
                        data,
                        onAdd: (variations) => {
                          setVariationOptions(variations);
                        },
                      })
                    }}
                  >
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 25 }} />
                      <FastImage source={require('../../assets/internal/icon_Variations.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                      <Text style={sell.inputText}>Variations</Text>
                    </View>

                    <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                      <Text style={sell.hintText2}>Set Variations</Text>
                      <View style={sell.spaceRight} />
                      <FastImage
                        source={require('../../assets/icon_sub.png')}
                        style={sell.openIcon}
                        tintColor={'#7f8c8d'}
                        resizeMode={FastImage.resizeMode.contain}
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </>
              : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {variationType !== 'deal' || (variationType === 'deal' && !hasVariation) ?
              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Price and Inventory</Text>
              </View>
              : null}

            {/* PRICE */}
            {renderPriceError()}
            {renderPrice()}

            {/* HAS DISCOUNT */}
            {variationType !== 'deal' || (variationType === 'deal' && !hasVariation) ?
              <View style={[sell.whiteBorder, { marginBottom: iosMargin, marginTop: iosMargin },]}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    if (price == 0) {
                      Toast.show({
                        text: 'Set your product Price first',
                      });
                    }
                  }}
                  style={sell.inputPickerContainer}
                >
                  <View style={sell.inputPickerContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 25 }} />
                      <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                      <Text style={sell.inputText}>Has Discount</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }} pointerEvents={price == 0 ? "none" : "auto"}>
                      <Picker
                        selectedValue={hasDiscount}
                        mode='dropdown'
                        style={sell.datePicker}
                        itemStyle={{
                          // fontFamily: 'Roboto',
                          fontSize: 12,
                          color: '#231f20',
                        }}
                        onValueChange={(value) => {
                          setHasDiscount(value);
                          if (price == 0) {
                            Toast.show({
                              text: 'Set your product Price first',
                            });
                          }
                        }}
                      >
                        <Picker.Item label='Yes' value={true} />
                        <Picker.Item label='No' value={false} />
                      </Picker>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              : null}

            {/* DISCOUNT SERIES */}
            {renderDiscountedPriceModal()}
            {renderDiscountType()}
            {renderDiscountedPriceError()}
            {renderDiscountAmmount()}
            {renderDiscountedPrice()}

            {variationType !== 'deal' || (variationType === 'deal' && !hasVariation) ?
              <View>
                <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
                <View style={sell.space} />
                <View style={sell.space} />
                <View style={sell.space} />
              </View>
              : null}

            {variationType !== 'deal' || (variationType === 'deal' && !hasVariation) ?
              <View style={sell.spaceLeft}>
                <Text style={sell.group_title}>Recurring</Text>
              </View>
              : null}

            {/* IS RECURRING */}
            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                // onPress={() => setModalVisible1(true)}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Is Recurring</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={isRecurring}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setIsRecurring(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            {/* CAN EXPIRE */}
            <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
              <TouchableOpacity
                activeOpacity={0.4}
                style={sell.inputPickerContainer}
              >
                <View style={sell.inputPickerContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 25 }} />
                    <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                    <Text style={sell.inputText}>Can Expire</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Picker
                      selectedValue={canExpire}
                      mode='dropdown'
                      style={sell.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setCanExpire(value);
                      }}
                    >
                      <Picker.Item label='Yes' value={true} />
                      <Picker.Item label='No' value={false} />
                    </Picker>
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            {/* EXPIRATION DATE */}
            {canExpire ?
              <View style={[sell.whiteBorder, { marginBottom: iosMargin },]}>
                <TouchableOpacity
                  activeOpacity={0.4}
                  onPress={() => {
                    setIsDateTimePickerVisible(true)
                  }}
                  style={sell.inputPickerContainer}
                >
                  <View style={sell.inputPickerContainer}>
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ marginLeft: 25 }} />
                      <FastImage source={require('../../assets/internal/icon_shipping_fee.png')} style={sell.sideIcon} resizeMode={FastImage.resizeMode.contain} />
                      <Text style={sell.inputText}>Expiration Date</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 15, marginRight: 25 }}>
                      {expirationDate ?
                        <Text style={{ textAlign: 'center', color: '#555' }}>
                          {Moment(expirationDate, 'DD-MM-YYYY').format('MMMM DD, YYYY')}
                        </Text>
                        :
                        <Text style={{ textAlign: 'center', color: '#555' }}>
                          Date
                        </Text>
                      }
                      <DateTimePicker
                        date={date}
                        mode='date'
                        isVisible={isDateTimePickerVisible}
                        onConfirm={handleDatePicked}
                        onCancel={() => {
                          setIsDateTimePickerVisible(false)
                        }}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              : null}

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={{ paddingTop: 10, paddingBottom: 5 }}>
              <TouchableOpacity
                nativeOpacity={0.4}
                onPress={() =>
                  handleSubmit()
                }
                style={sell.nextButton}>
                <Text style={sell.SellText}>Sell</Text>
              </TouchableOpacity>
            </View>
          </View>

        </KeyboardAwareScrollView>
      </Container>
    </Root >
  );
}
