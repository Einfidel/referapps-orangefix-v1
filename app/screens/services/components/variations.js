import React, {
  useState,
  useEffect,
  createRef,
} from 'react';
import { Text, TouchableOpacity, View, Dimensions, StyleSheet, Image, TextInput, Alert } from 'react-native';
import { Header, Left, Container } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FastImage from 'react-native-fast-image'

const { width, height } = Dimensions.get('window');

export default function VariationsScreen({ navigation, route }) {
  const [hasSubmited, setHasSubmited] = useState(false);
  const [variationType, setVariationType] = useState('branch');
  const [variationPlaceHolder, setVariationPlaceHolder] = useState('');
  const [variation1Options, setVariation1Options] = useState([]);
  const [tmp, setTmp] = useState('');

  useEffect(() => {
    let variationOptions = route.params.data.variationOptions
    let variationTypeTmp = route.params.data.variationType
    let price = route.params.data.price

    console.log('route.params.data.variationOptions', route.params.data.variationOptions)

    setVariationType(variationTypeTmp)
    if (variationTypeTmp === 'branch') {
      setVariationPlaceHolder('Ex: Manila, New York')
    } else {
      setVariationPlaceHolder('Ex: 50% More Content, Additional Serving')
    }

    // Check if there is already a variation
    if (variationOptions.length > 0 && variationOptions != null) {
      if (variationTypeTmp === 'branch') {
        variationOptions.map((option, index) => {
          option.price = price
        })
        setVariation1Options([...variationOptions])
      } else {
        setVariation1Options([...variationOptions])
      }
    } else {
      let variationArray = [
        {
          "variation_type": variationTypeTmp,
          "option": "",
          "price": price,
        },
        {
          "variation_type": variationTypeTmp,
          "option": "",
          "price": price,
        }
      ];
      setVariation1Options(variationArray)
    }
  }, [])

  const render1stVariationOptions = () => {
    let variation1OptionsTmp = [...variation1Options]
    return variation1OptionsTmp.map((option1, index) => {
      return (
        <>
          {index > 0 ?
            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3', marginHorizontal: 20, marginVertical: 4, }} />
            :
            null}
          <View key={index} style={{ marginBottom: 10, }}>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', }}>
              <Text style={{ textAlign: 'center', marginBottom: 2, fontSize: 15, }}>
                Name:
              </Text>
              <TouchableOpacity style={styles.minusCon}>
                <FastImage source={''} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
              {hasSubmited && (option1.option === '' || option1.option.toString().replace(/\s/g, '').toLowerCase() == '') ?
                <Text style={{ color: "#FA3636", fontSize: 16, marginRight: 4, marginTop: 6 }}>
                  *
                </Text>
                : null}
              <TextInput
                placeholder={variationPlaceHolder}
                placeholderTextColor={'#231f20'}
                style={styles.txtInput2}
                value={option1.option}
                onChangeText={(newOption) => {
                  option1.option = newOption;

                  // Used to trigger
                  setTmp(newOption);
                  let tmp2 = tmp;
                }}
              />
              {index > 1 ?
                <TouchableOpacity onPress={() => {
                  handleRemoveOptions1(index)
                }} style={[styles.minusCon, { marginVertical: 10 }]}>
                  <FastImage source={require('../../../assets/icon_minus.png')} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
                :
                <TouchableOpacity style={styles.minusCon}>
                  <FastImage source={''} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>}
            </View>

            {variationType === 'deal' ?
              <>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                  <Text style={{ textAlign: 'center', marginVertical: 2, fontSize: 15, }}>
                    Price:
                  </Text>
                  <TouchableOpacity style={styles.minusCon}>
                    <FastImage source={''} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                  </TouchableOpacity>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                  {hasSubmited && (option1.price === '' || option1.price.toString().replace(/\s/g, '').toLowerCase() == '') ?
                    <Text style={{ color: "#FA3636", fontSize: 16, marginRight: 4, marginTop: 6 }}>
                      *
                    </Text>
                    : null}
                  <TextInput
                    placeholder={'Ex: 4999'}
                    placeholderTextColor={'#231f20'}
                    style={styles.txtInput3}
                    value={option1.price.toString()}
                    onChangeText={(newPrice) => {
                      let parsedPrice = parseFloat(newPrice);
                      if (parsedPrice * 0 != 0 || parsedPrice === NaN) {
                        option1.price = 1;
                      } else {
                        option1.price = parsedPrice;
                      }
                      setVariation1Options(variation1OptionsTmp);

                      // Used to trigger
                      setTmp(parsedPrice);
                      let tmp2 = tmp;
                    }}
                  />
                  <TouchableOpacity style={styles.minusCon}>
                    <FastImage source={''} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                  </TouchableOpacity>
                </View>
              </>
              :
              null}
          </View>
        </>
      )
    })
  }

  const handleAddOptions1 = () => {

    let variationArray = {
      "variation_type": route.params.data.variationType,
      "option": "",
      "price": route.params.data.price,
    };

    let tmpVariation1Options = variation1Options;

    setVariation1Options([...tmpVariation1Options, variationArray])
  }

  const handleRemoveOptions1 = (index) => {
    let tmpVariation1Options = [...variation1Options]
    tmpVariation1Options.splice(index, 1);
    setVariation1Options([]);
    setVariation1Options(tmpVariation1Options);
    setTmp(index);
    let tmp2 = tmp;
    // console.log("variation1Options" + [index] + " should be empty:", variation1Options[index])
    // setVariation1Options(tmpVariation1Options);
  }

  const handleSubmit = () => {
    // Add error handling here
    setHasSubmited(true);
    let variation1Error = '';
    let variation2Error = '';

    variation1Options.map((option1, index1) => {
      // console.log("option1", option1)
      if (option1.option == null || option1.option.toString().replace(/\s/g, '').toLowerCase() == '') {
        variation1Error === '' ? variation1Error = (index1 + 1) : variation1Error += ", " + (index1 + 1)
      }
      if (option1.price * 0 != 0) {
        option1.price = 0;
      }
    })

    if (variation1Error != '' || variation2Error != '') {
      Alert.alert(
        'Error!',
        'Please fill up all the variation names that you have added',
        [
          {
            text: 'OK',
          },
        ],
        { cancelable: true }
      )
    } else {
      // console.log("variation1Options", variation1Options)
      // return

      route.params.onAdd(variation1Options);
      navigation.goBack();
    }
  }

  return (
    < Container style={styles.container} >
      <Header style={styles.header}>
        <Left style={styles.left}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <FastImage source={require('../../../assets/headericon/left-arrow-dark.png')} style={styles.imgback} />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.txtAddVariation}> Add Variations </Text>
          </View>
        </Left>
      </Header>
      <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
        <View style={styles.Con}>
          <View style={styles.addOptCon}>
            <View>
              <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                {variationType === 'branch' ?
                  <Text style={{ fontWeight: "bold", fontSize: 18, color: '#231f20', alignSelf: 'center', marginBottom: 10, }}>
                    Branches
                  </Text>
                  :
                  <Text style={{ fontWeight: "bold", fontSize: 18, color: '#231f20', alignSelf: 'center', marginBottom: 10, }}>
                    Service Deals
                  </Text>
                }
                <TouchableOpacity style={styles.minusCon}>
                  <FastImage source={''} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                </TouchableOpacity>
              </View>
              {render1stVariationOptions()}
            </View>

            <TouchableOpacity
              style={styles.btnAddOpt}
              onPress={() => {
                handleAddOptions1();
              }}
            >
              <Text style={styles.txtAddOpt}>Add Options</Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.btnStockCon}>
          <TouchableOpacity
            style={styles.btnSetCon}
            onPress={() => {
              handleSubmit()
            }}
          >
            <Text style={styles.txtStock}>Save</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>

    </Container >
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width: width,
  },
  header: {
    ...Platform.select({
      ios: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: -20
      },
      android: {
        backgroundColor: '#ffffff',
        justifyContent: 'center',
        marginTop: 30,
      }
    })
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  imgback: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  addVariation: {
    marginLeft: 25,
  },
  txtAddVariation: {
    // fontFamily: 'roboto.medium',
    fontSize: 18,
  },
  Con: {
    backgroundColor: '#ecf0f1',
    flex: 1,
  },
  addOptCon: {
    backgroundColor: '#FFF',
    marginTop: 15,
    paddingTop: 10,
    paddingBottom: 20,
  },
  txtInput: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.3,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
  },
  txtInput2: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.3,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
    // marginTop: 10,
  },
  txtInput3: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.3,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
  },
  btnAddOpt: {
    backgroundColor: '#24ae5f',
    width: width / 1.15,
    height: 31,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginTop: 10,
  },
  txtAddOpt: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  popCon: {
    width: width / 1.15,
    height: 31,
    borderRadius: 8,
    alignSelf: 'center',
    justifyContent: 'space-around',
    marginTop: 20,
    borderWidth: 1.5,
    borderColor: '#f36e23',
    borderStyle: 'dashed',
  },
  txtAddVar: {
    // fontFamily: 'regular',
    fontSize: 14,
    color: '#f36e23',
    alignSelf: 'center',
  },
  conAddVar: {
    backgroundColor: '#FFF',
    width,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  minusCon: {
    alignSelf: 'flex-end',
    // marginVertical: 10,
    marginLeft: 10,
  },
  imgMinus: {
    tintColor: '#e04f5f',
    resizeMode: 'contain',
    height: 15,
    width: 15,
  },
  btnStockCon: {
    height: 50,
    backgroundColor: "#fff",
    marginBottom: 5,
    // borderTopColor: "#aaa",
    // borderTopWidth: 2,
    // backgroundColor: '#fff',
    // marginHorizontal: 10,
  },
  btnSetCon: {
    // position: 'absolute',
    // bottom: 0,
    backgroundColor: '#F28647',
    width: width / 1.05,
    height: 31,
    borderRadius: 10,
    alignSelf: 'center',
    justifyContent: 'space-around',
    margin: 10,
  },
  txtStock: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#ffffff',
    alignSelf: 'center',
  },
  inputTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginRight: 5,
  },
});
