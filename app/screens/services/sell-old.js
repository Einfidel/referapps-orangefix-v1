import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Modal,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import { Header, Thumbnail, Item, Input, Left, Right, Body } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import * as Animatable from 'react-native-animatable';
import { MaterialIndicator } from 'react-native-indicators';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { styles, sell } from '../product/styles';
import Service from '../../components/api/service';

const rightArrow = require('../../assets/ProfileIcons/iconb_sub.png');
const info = require('../../assets/internal/icon_condition.png');
const sellImg = require('../../assets/sellProduct.png');

// const recurr =[{options: 'No' }, {options:'Yes'}]
const recurr = ['Yes', 'No'];

export default class ServiceScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      price: '',
      switch_status: false,
      user: {},
      errors: {},
      responseData: {},
      images: [],
      filePath: {
        data: '',
        uri: '',
      },
      isLoading: false,
      fileData: '',
      imageUpdated: false,
      nameFocused: false,
      descriptionFocused: false,
      priceFocused: false,

      modalVisible: false,
      referrerModal: false,
      referrer_commission: '',
      option: '',
      value: '',
      categoryId: '1',
      optionModal: false,
      loadingModal: false,
    };
  }

  componentDidMount = async () => {
    try {
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) this.setState({ user: user });
    } catch (error) {
      console.warn(error);
    }
  };

  handleSubmitService = async () => {
    this.setState({ loadingModal: true });
    // this.handleShowModal();
    await Service.createService(
      this.state,
      (res) => {
        // console.log(res);
        if (res.errors) {
          Alert.alert('Upload Error', res.errors.file[0]);
        } else {
          this.setState({
            responseData: res,
            loadingModal: false,
          });
          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  await this.handleShowModal();
                  await this.setState({
                    name: '',
                    description: '',
                    price: '',
                    referrer_commission: '',
                    value: '',
                  });
                },
              },
            ],
            { cancelable: false }
          );
        }
      },
      (err) => alert(err)
    );
  };

  handleClearFields = () => { };

  handleShowModal = () => {
    this.setState({
      loadingModal: true,
    });
    setTimeout(() => {
      this.setState({
        loadingModal: false,
      });
    }, 1000);
  };

  handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={this.state.loadingModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: hp('80%') }}>
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Please wait...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  handleImagePicker = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }
        this.setState((prevState) => ({
          images: [...prevState.images, response],
          filePath: response.path,
          imageUpdated: true,
          fileData: response.data,
        }));
      }
    });
  };

  onPressButton = (value) => {
    this.setState({ value: value });
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setOptionModal(visible) {
    this.setState({ optionModal: visible });
  }

  renderModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.modalVisible} style={styles.modalCon}>
          {this._renderReferrModal()}
        </Modal>
      </View>
    );
  };

  optionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.optionModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setOptionModal(!this.state.optionModal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.modalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    this.onPressButton('Yes');
                    this.setOptionModal(!this.state.optionModal);
                  }}
                >
                  Yes
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    this.onPressButton('No');
                    this.setOptionModal(!this.state.optionModal);
                  }}
                >
                  No
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  _renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.setModalVisible(!this.state.modalVisible);
          this.setState({ referrer_commission: '' });
        }}
      >
        <View style={styles.containerModal}>
          <View style={styles.referrContainer}>
            <Text style={styles.txtSet}>Set Referrer Commission(5% - 50%)</Text>
            <Item style={styles.txtInputsCon}>
              <Input
                style={styles.txtInput}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7F8C8D'}
                keyboardType={'number-pad'}
                value={this.state.referrer_commission}
                onChangeText={(text) => this.setState({ referrer_commission: text })}
                maxLength={2}
              />
              <Text style={styles.txtPercent}>%</Text>
            </Item>
            <TouchableOpacity style={styles.btnCon} onPress={() => this._renderReferrerSaveButton()}>
              <Text style={{ color: '#FFF', textAlign: 'center' }}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  _renderReferrerSaveButton = () => {
    if (this.state.referrer_commission < 5 || this.state.referrer_commission > 50) {
      alert('Invalid Percentage! (5% - 50%)');
      return false;
    } else {
      this.setState({
        referrerModal: false,
        modalVisible: false,
      });
    }
  };

  renderImages = () => {
    return this.state.images.map((img, i) => {
      return (
        <Image
          key={i}
          source={{ uri: img.uri }}
          style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
        />
      );
    });
  };

  render() {
    return (
      <View style={sell.serviceContainer}>
        <Header style={[sell.headerElevate, { backgroundColor: '#fff' }]}>
          <Left>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={sell.backImage} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Text style={sell.headerText}> Sell Services </Text>
          </Body>
          <Right>
            <View style={sell.rightView} />
          </Right>
        </Header>
        {/* <Header style={styles.headerCon}>
          <View style={styles.imgBtnCon}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={require('../../assets/headericon/left-arrow-dark.png')} style={styles.imgBtnBack} />
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: 'center' }}>
            <Text style={styles.txtService}> Sell Service </Text>
          </View>
        </Header> */}
        <ScrollView style={{ flex: 1 }}>
          <View style={{ backgroundColor: '#ecf0f1', height: 210 }}>
            {this.state.images && this.state.images.length > 0 ? (
              this.renderImages()
            ) : (
              <TouchableOpacity
                onPress={() => {
                  this.handleImagePicker();
                }}
                style={{ justifyContent: 'center' }}
              >
                <Image
                  source={this.state.images.length === 0 ? sellImg : this.state.images}
                  style={{ height: 210, width: 180, resizeMode: 'contain', justifyContent: 'center' }}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.concon}>
            <View style={styles.txtItem}>
              <Input
                style={styles.txtInputService}
                onChangeText={(name) => this.setState({ name: name })}
                value={this.state.name}
                placeholder='Service Name'
                placeholderTextColor='#7f8c8d'
                maxLength={50}
                onFocus={() => {
                  if (this.state.errors.product_name) {
                    this.setState({
                      nameFocused: true,
                    });
                  }
                }}
              />
            </View>
            <Text style={styles.txtLength}>{this.state.name.length}/50</Text>
          </View>
          <View style={styles.des}>
            <TextInput
              style={styles.txtInputDescription}
              onChangeText={(description) => this.setState({ description: description })}
              value={this.state.description}
              placeholder='Description'
              editable={true}
              multiline={true}
              onFocus={() => {
                if (this.state.errors.description) {
                  this.setState({
                    descriptionFocused: true,
                  });
                }
              }}
            />
          </View>
          <View style={styles.ref}>
            {this.renderModal()}
            <TouchableOpacity style={styles.referrBtn} onPress={() => this.setModalVisible(true)}>
              <Text style={sell.inputText}>Referrer Commission</Text>
              <View style={styles.setCommissionCon}>
                <Text style={styles.txtSetCommission}>
                  {this.state.referrer_commission == '' ? 'Set Commission %' : this.state.referrer_commission + '%'}
                </Text>
                <Image source={rightArrow} style={styles.imgRight} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.concon, { marginTop: 10 }]}>
            {this.optionsModal()}
            <TouchableOpacity onPress={() => this.setOptionModal(true)} style={styles.imgRecurringCon}>
              <View style={styles.imgCon}>
                <Image source={info} style={styles.imgInfo} />
                <Text style={sell.inputText}>Recurring</Text>
              </View>
              <View style={styles.pickerCon}>
                <Text style={styles.txtValue}>{this.state.value}</Text>
                <Image source={rightArrow} style={styles.imgRight} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.concons}>
            <View style={styles.priceCon}>
              <View style={{ flexDirection: 'row' }}>
                <View style={sell.spaceLeft} />
                <Thumbnail
                  source={require('../../assets/internal/icon_price.png')}
                  square
                  small
                  style={styles.cheese}
                />
                <View style={sell.spaceLeft} />
                <Text style={sell.inputText}>Price ({this.state.user.country_code === 'PH' ? '₱' : '$'})</Text>
              </View>
              <Item style={sell.input_title}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'Price'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'decimal-pad'}
                  onChangeText={(x) => this.setState({ price: x })}
                  value={this.state.price}
                  onFocus={() => {
                    if (this.state.errors.price) {
                      this.setState({
                        priceFocused: true,
                      });
                    }
                  }}
                />
              </Item>
            </View>
          </View>
        </ScrollView>
        <View style={{ height: 50 }}>
          <TouchableOpacity
            // onPress={() => this.handleShowModal()}
            onPress={() => {
              this.handleSubmitService();
            }}
            style={styles.btnSell}
          >
            <Text style={styles.txtSell}>Sell</Text>
          </TouchableOpacity>
        </View>
        {this.handleLoading()}
      </View>
    );
  }
}
