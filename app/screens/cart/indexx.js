import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, Image, StyleSheet, SafeAreaView, RefreshControl, TouchableOpacity, ScrollView, Alert, Platform } from 'react-native'
import { Container, Header, Thumbnail, Footer, FooterTab, Content, Body, Left, Right, Card, CardItem, Icon, ListItem, List } from 'native-base'

// import { TouchableOpacity } from 'react-native-gesture-handler';
import CheckBox from 'react-native-modest-checkbox'
import styles from '../../styles/cart/cartScreenStyles'
import Assets from '../../components/assets.manager';
import Endpoints from '../../components/api/endpoints';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Icons from '../../components/icons';
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

function CartScreen(navigation) {
  const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
  const [selectAll, setSelectAll] = useState(false)
  const [allCartItems, setAllCartItems] = useState('')
  const [loading, setloading] = useState(false)
  const [isSelected, setSelection] = useState(false);
  const [currentUserdata, setCurrentUserdata] = useState({})
  const [stockHandler, setStockHandler] = useState([])
  const [qtyHandler, setQtyHandler] = useState([])

  const init = async () => {
    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = ud ? JSON.parse(ud) : null

    try {
      let formData = new FormData();
      formData.append("user_id", _userdata.data.id);
      formData.append("api_token", token)
      const res = await fetch(Endpoints.show_added + `?include=date,info`, {
        method: 'post',
        headers: { 'content-type': 'multipart/form-data' },
        body: formData
      });
      const cartDetail = await res.json();
      const response = cartDetail
      let data = []
      let mstocks = []
      let mqty = []
      // console.log('respos', response)
      response.data.map((result, index) => {
        // console.warn('ressss', result.info.data.buyer_info)
        mstocks.push(result.max_stock)
        mqty.push(result.qty)
        data.push({
          data: {
            id: result.id,
            user_id: result.user_id,
            product_id: result.product_id,
            product_name: result.product_name,
            qty: result.qty,
            price: result.price,
            currency: result.currency,
            is_selected: result.is_selected,
            max_stock: result.max_stock,
            seller_id: result.seller_id,
            seller_name: result.seller_name,
            delivery_courier: result.delivery_courier,
            delivery_courier_slug: result.delivery_courier_slug,
            delivery_courier_fee: result.delivery_courier_fee,
            variations: result.variations,
            variation_1: result.variation_1,
            variation_2: result.variation_2,
            variation_stock: result.variation_stock,
            total: result.total,
          },
          buyer_info: {
            id: result.info.data.buyer_info.id,
            fname: result.info.data.buyer_info.fname,
            lname: result.info.data.buyer_info.lname,
            email: result.info.data.buyer_info.email,
            username: result.info.data.buyer_info.username,
            contact_number: result.info.data.buyer_info.contact_number,
            avatar: result.info.data.buyer_info.avatar,
          },
          seller_info: {
            id: result.info.data.seller_info.id,
            fname: result.info.data.seller_info.fname,
            lname: result.info.data.seller_info.lname,
            shop_name: result.info.data.seller_info.shop_name,
            email: result.info.data.seller_info.email,
            username: result.info.data.seller_info.username,
            contact_number: result.info.data.seller_info.contact_number,
            avatar: result.info.data.seller_info.avatar
          },
          product_info: {
            product_image: result.info.data.product_info.product_image,
            product_name: result.info.data.product_info.product_name,
            description: result.info.data.product_info.description,
            country_code: result.info.data.product_info.country_code,
            condition: result.info.data.product_info.condition,
            type: result.info.data.product_info.type,
            category_id: result.info.data.product_info.category_id,
            category_name: result.info.data.product_info.category_name,
            attributes: result.info.data.product_info.attributes,
            with_varation: result.info.data.product_info.with_varation,
            variation_1: result.info.data.product_info.variation_1,
            variation_2: result.info.data.product_info.variation_2
          },
          shipping_info: {
            weight: result.info.data.shipping_info.weight,
            width: result.info.data.shipping_info.width,
            length: result.info.data.shipping_info.length,
            height: result.info.data.shipping_info.height,
            delivery_option: result.info.data.shipping_info.delivery_options[0].delivery_option,
            delivery_option_name: result.info.data.shipping_info.delivery_options[0].delivery_option_name,
            cover: result.info.data.shipping_info.delivery_options[0].delivery_option_info.cover,
            fee: result.info.data.shipping_info.delivery_options[0].delivery_option_info.fee
          }
        })
      })
      setStockHandler(mstocks)
      setQtyHandler(mqty)
      setAllCartItems(JSON.stringify(data))
      setCurrentUserdata(_userdata)
      // console.warn('person', currentUserdata)
      // console.warn('all cart', allCartItems)
    }
    catch (e) {
      console.log(e)
    }
  }

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      // console.warn(state.isConnected, 'connected ba?')
      if (state.isConnected == true) {
        init();
      } else {
        setloading(false);
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
    setloading(false)
  }, [])

  const checkFunction = () => {
    if (selectAll == false) {
      setSelectAll(true)
    }
    else {
      setSelectAll(false)
    }
  }

  function noCartItem() {
    return (
      <View style={styles.cartContent}>
        <Text style={[styles.cartContentText, { fontWeight: '100' }]}> There are no items in this cart</Text>
        <TouchableOpacity onPress={() => navigation.navigation.navigate('Home')}
          style={styles.goBackHomeButton}>
          <Text style={styles.goBackHomeText}>Go back home</Text>
        </TouchableOpacity>
      </View>
    )
  }

  function withCartItem() {
    // console.log('carts', allCartItems)
    const image = { uri: "https://reactjs.org/logo-og.png" };
    const data = JSON.parse(allCartItems)
    // console.warn('daataa', data)

    return (
      <SafeAreaView style={{ height: hp('69%') }}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => {
                NetInfo.fetch().then((state) => {
                  if (state.isConnected) {
                    init();
                  } else {
                    setloading(false);
                    Alert.alert('Not Connected', 'You have no internet connection');
                  }
                });
              }}
            />
          }
        >
          <Text>{`You have ${data.length} item(s) in your cart`}</Text>
          {
            data.map((result, i) => {
              // console.log(result.data.seller_name, 'resultss'),
              return (
                // <Container style={stylings.viewContainer}>
                // <Content>
                <List>
                  <ListItem>
                    {/* <Card> */}
                    <View>
                      <Card>
                        <CardItem>
                          <CheckBox
                            checkedImage={Assets.cartFunction.checkbox}
                            uncheckedImage={Assets.cartFunction.uncheck}
                            checkboxStyle={{
                              width: 18,
                              height: 18
                            }}
                            label=''
                            // label={result.data.seller_name}
                            // labelStyle={{
                            //   fontSize: 12,
                            //   color:'#000000',
                            //   paddingLeft:10,
                            // }}
                            checked={selectAll}
                            onChange={() => { checkFunction() }}
                          />
                          <TouchableOpacity onPress={() => { navigation.navigation.navigate('Seller Shop', { result }) }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 260 }}>
                              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12, color: '#000000', paddingLeft: 10 }}>{result.data.seller_name}</Text>
                              </View>
                              <View style={{ alignItems: 'flex-end' }}>
                                <Right>
                                  <Icon name="arrow-forward" />
                                </Right>
                              </View>
                            </View>
                          </TouchableOpacity>
                        </CardItem>
                      </Card>
                      <View style={{ marginVertical: 10, justifyContent: 'center' }}>
                        <View>
                          <View style={{ height: 100 }}>
                            <View style={{ flexDirection: 'row' }}>
                              <View style={{}}>
                                <FastImage source={result.product_info.product_image != null || result.product_info.product_image != '' ? { uri: result.product_info.product_image } : Assets.myProfile.defProductImg} style={{ height: 100, width: 100, marginHorizontal: 2 }} />
                              </View>
                              <View style={{ marginHorizontal: 10 }}>
                                <View style={{ flexDirection: 'row', flex: 1 }}>
                                  <View style={{ width: wp('45%') }}>
                                    <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 15, fontWeight: '600' }}>{result.product_info.product_name}</Text>
                                  </View>
                                  <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                    <TouchableOpacity onPress={() => { handleRemoveCartItem(result.data.id) }} style={{ cursor: 'pointer' }}>
                                      <Icons.EvilIcons name="trash" size={30} />
                                    </TouchableOpacity>
                                  </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                  <View style={{ backgroundColor: 'blue', width: 18, height: 18, borderRadius: 100, alignItems: 'center', justifyContent: 'center' }}>
                                    <TouchableOpacity onPress={() => {
                                      // alert("Add")
                                      let nqty = qtyHandler
                                      nqty[i] = nqty[i] + 1
                                      setloading(true)
                                      setQtyHandler(nqty)
                                      setloading(false)
                                      setSelectAll(false)
                                    }}>
                                      <Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', color: '#fff', fontWeight: '900' }}>+</Text>
                                    </TouchableOpacity>
                                  </View>
                                  <View style={{ marginHorizontal: 30 }}>
                                    <Text>{qtyHandler[i]}</Text>
                                  </View>
                                  <View style={{ backgroundColor: 'blue', width: 18, height: 18, borderRadius: 100, alignItems: 'center', justifyContent: 'center', }}>
                                    <TouchableOpacity onPress={() => {
                                      // alert("Minus")
                                      let nqty = qtyHandler
                                      nqty[i] = nqty[i] - 1
                                      setloading(true)
                                      setQtyHandler(nqty)
                                      setloading(false)
                                      setSelectAll(false)
                                    }}>
                                      <Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', color: '#fff', fontWeight: '900' }}>-</Text>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                                <View>
                                  <Text style={{ color: '#f00' }}>{`\u20B1 ` + `${result.data.price}`}</Text>
                                </View>
                                <View>
                                  <Text>{`Available stock:` + `${stockHandler[i]}`}</Text>
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                    {/* </Card> */}
                  </ListItem>
                </List>
                // </Content>
                // </Container>
              )
            })
          }
        </ScrollView>
      </SafeAreaView>
    )
  }

  function compareCart() {
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          allCartItems == null || allCartItems == '' ? noCartItem() : withCartItem()
        }
      </View>
    )
  }

  const handleRemoveCartItem = async (result) => {
    // console.warn('rexultx', result)
    try {
      let formData = new FormData();
      formData.append("id", result);
      formData.append("user_id", currentUserdata.data.id);
      formData.append("api_token", token);
      const data = await fetch(Endpoints.remove_cart + `?include=date,info`, {
        method: 'post',
        headers: { 'content-type': 'multipart/form-data' },
        body: formData
      })
      const response = await data.json();
      if (response.status == true) {
        Alert.alert(
          'Success',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                init()
              }
            },
          ],
          { cancelable: true }
        );
      }
      else {
        Alert.alert(
          'Error',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: true }
        );
      }
    }
    catch (e) {
      console.log(e)
    }
  }

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={styles.headerStyle}>
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.headerText}>Your Cart</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>

      <Content>
        <Card style={stylings.cardHeight}>
          <CardItem>
            <Body>
              {compareCart()}
            </Body>
          </CardItem>
        </Card>
      </Content>
      <Footer style={styles.footer1}>
        <FooterTab style={styles.footerTab}>
          <CheckBox
            checkedImage={Assets.cartFunction.checkbox}
            uncheckedImage={Assets.cartFunction.uncheck}
            checkboxStyle={{
              width: 18,
              height: 18
            }}
            label='Select All'
            labelStyle={{
              fontSize: 12,
              color: '#000000',
              paddingLeft: 10,
              // fontFamily:'Roboto'
            }}
            checked={selectAll}
            onChange={() => { checkFunction() }}
          />
          {/* <TouchableOpacity style={{
              flexDirection:'row'
          }}> */}
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.txtSubTotal}>Sub Total:      </Text>
            <Text style={styles.txtPrice}>₱ 00.00  </Text>
          </View>
          {/* </TouchableOpacity> */}
        </FooterTab>
      </Footer>
      <Footer style={styles.footer2}>
        <FooterTab style={styles.footerTab2}>
          <TouchableOpacity onPress={() => navigation.navigation.navigate('PaypalCheckout')}
            style={[{ width: Dimensions.get('window').width - 40, }, styles.btnCheckout]}>
            <Text style={styles.btnTextCheckout}>Check Out</Text>
          </TouchableOpacity>
        </FooterTab>
      </Footer>
    </Container>
  )
}

const stylings = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  viewContainer: {
    ...Platform.select({
      android: {
        // height: hp('100%')
        height: 215
      },
      ios: {
        height: 215
      }
    })
  },
  cardHeight: {
    ...Platform.select({
      android: {
        height: Dimensions.get('screen').height - 210
      },
      ios: {

      }
    })
  },
  footerTab: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
  },
})

export default CartScreen;