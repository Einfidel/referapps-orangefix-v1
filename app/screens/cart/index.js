import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, Image, StyleSheet, SafeAreaView, RefreshControl, TouchableOpacity, ScrollView, Alert, Platform, Modal, TouchableWithoutFeedback, FlatList } from 'react-native'
import { Container, Header, Thumbnail, Footer, FooterTab, Content, Body, Left, Right, Card, CardItem, Icon, ListItem, List, } from 'native-base'

// import { TouchableOpacity } from 'react-native-gesture-handler';
import CheckBox from 'react-native-modest-checkbox'
import styles from '../../styles/cart/cartScreenStyles'
import Assets from '../../components/assets.manager';
import Endpoints from '../../components/api/endpoints';
import Service, { getUserData } from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Icons from '../../components/icons';
import Auth from '../auth/index';
import FastImage from 'react-native-fast-image'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { paddingLeft } from 'styled-system';

const { width, height } = Dimensions.get('window');

export default function CartScreen({ navigation, route }) {
  const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
  const [selectAll, setSelectAll] = useState(false)
  const [allCartItems, setAllCartItems] = useState('loading')
  const [loading, setloading] = useState(true)
  const [isSelected, setSelection] = useState(false);
  const [currentUserdata, setCurrentUserdata] = useState({})
  const [stockHandler, setStockHandler] = useState([])
  const [qtyHandler, setQtyHandler] = useState([])
  const [user, setUser] = useState(true)
  const [userCheck, setUserCheck] = useState(true)
  const [selectedItem, setSelectedItem] = useState('')
  const [courierModalVisibility, setCourierModalVisibility] = useState(false)

  const init = async () => {
    // console.log("Cart Initialized")
    setloading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      setUserCheck(ud)
      return
    }
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata);

    try {
      let formData = new FormData();
      formData.append("user_id", _userdata.data.id);
      formData.append("api_token", token)
      const res = await fetch(Endpoints.show_added + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      });
      const cartDetail = await res.json();
      const response = cartDetail
      let data = []
      let mstocks = []
      let mqty = []

      response.data.map((result, index) => {
        // console.warn('ressss', result.info.data.buyer_info)
        mstocks.push(result.max_stock)
        mqty.push(result.qty)
        data.push({
          data: result,
          buyer_info: result.info.data.buyer_info,
          seller_info: result.info.data.seller_info,
          product_info: result.info.data.product_info,
          shipping_info: result.info.data.shipping_info
        })
      })
      setStockHandler(mstocks)
      setQtyHandler(mqty)
      setAllCartItems(data)
      setCurrentUserdata(_userdata)
      setloading(false)

      // console.warn('person', currentUserdata)

    }
    catch (e) {
      setloading(false)
      console.log(e)
    }
  }

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      // console.warn(state.isConnected, 'connected ba?')
      if (state.isConnected == true) {
        init();
        // console.warn('all cart', allCartItems)
        // console.log("Total", getSubTotal())
      } else {
        setloading(false);
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
    // setloading(false)
  }, [])

  const checkFunction = () => {
    if (selectAll == false) {
      setSelectAll(true)
    }
    else {
      setSelectAll(false)
    }
  }

  const renderCourierModal = () => {
    return (
      <Modal
        style={{ flex: 1 }}
        animationType="fade"
        transparent={true}
        visible={courierModalVisibility}
        onRequestClose={() => setCourierModalVisibility(false)}>
        <TouchableOpacity
          onPress={() => setCourierModalVisibility(false)}
          style={stylings.modalBackgroundContainer}>
          <TouchableWithoutFeedback>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 5,
                width: width - 40,
                padding: 10,
                marginBottom: 80,
                marginTop: 80,
              }}>
              <Text style={[stylings.courierFeeText, { textAlign: 'center', fontSize: 18 }]}>
                Select Courier
              </Text>
              {/* <View style={{ paddingBottom: 8, borderBottomWidth: 0.5, borderTopColor: '#e1e2e3' }} /> */}
              <FlatList
                data={selectedItem?.shipping_info?.delivery_options}
                renderItem={renderCouriers}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    );
  };

  const renderCouriers = ({ item, index }) => {
    console.log("selectedItem.data.delivery_courier_slug", selectedItem.data.delivery_courier_slug)
    console.log("item.delivery_option", item.delivery_option)
    return (
      <TouchableOpacity
        style={[
          { padding: 10, margin: 10, marginBottom: 0, borderWidth: 2, },
          selectedItem.data.delivery_courier_slug == item.delivery_option ? { borderColor: '#00a14b' } : { borderColor: '#dadada' }
        ]}
        onPress={() => {
          if (selectedItem.data.delivery_courier_slug != item.delivery_option) {
            handleUpdateItemCourier(selectedItem, item);
          } else {
            setCourierModalVisibility(false);
          }
        }}>
        <Body style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 260 }}>
            <Text style={stylings.addresNameText}>
              {item.delivery_option_name}
            </Text>
            <Text style={[stylings.courierFeeText, { fontSize: 13 }]}>
              {item.delivery_option_info.cover == "yes" ?
                `\u20B1 ` + 0
                :
                `\u20B1 ` + item.delivery_option_info.fee}
            </Text>
          </View>
        </Body>
      </TouchableOpacity >
    );
  };

  function noCartItem() {
    return (
      <View style={styles.cartContent}>
        <Text style={[styles.cartContentText, { fontWeight: '100' }]}> There are no items in this cart</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}
          style={styles.goBackHomeButton}>
          <Text style={styles.goBackHomeText}>Go back home</Text>
        </TouchableOpacity>
      </View>
    )
  }

  function withCartItem() {
    // console.log('carts', allCartItems)
    const image = { uri: "https://reactjs.org/logo-og.png" };
    // console.warn('daataa', data)

    return (
      //   <SafeAreaView style={{ height: hp('100%') }}>
      <View>
        <Text style={{ padding: 15 }}>{`You have ${allCartItems.length} item(s) in your cart`}</Text>
        <List style={{ height: '100%', alignItems: 'center' }}>
          <ScrollView
            contentContainerStyle={{ alignItems: 'center' }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                refreshing={loading}
                onRefresh={() => {
                  NetInfo.fetch().then((state) => {
                    if (state.isConnected) {
                      init();
                    } else {
                      setloading(false);
                      Alert.alert('Not Connected', 'You have no internet connection');
                    }
                  });
                }}
              />
            }
          >
            <View style={{ width: '100%' }}>
              {
                allCartItems.map((item, i) => {
                  // console.log("item #", i, item);
                  return (
                    <ListItem style={{ alignItems: 'center' }}>
                      <Card style={{ alignItems: 'center' }}>
                        <CardItem>
                          <CheckBox
                            checkedImage={Assets.cartFunction.checkbox}
                            uncheckedImage={Assets.cartFunction.uncheck}
                            checkboxStyle={{
                              width: 18,
                              height: 18
                            }}
                            label=''
                            checked={item.data.is_selected == 'yes' ? true : false}
                            onChange={(value) => {
                              let is_selected = value.checked ? 'yes' : 'no';
                              handleUpdateItem(item.data.qty, is_selected, item)
                            }}
                          />
                          <TouchableOpacity onPress={() => { navigation.navigate('Seller Shop', { item }) }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: 260 }}>
                              <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 12, color: '#000000', paddingLeft: 10 }}>{item.data.seller_name}</Text>
                              </View>
                              <View style={{ alignItems: 'flex-end' }}>
                                <Right>
                                  <Icon name="arrow-forward" />
                                </Right>
                              </View>
                            </View>
                          </TouchableOpacity>
                        </CardItem>
                        <View style={{ marginVertical: 10, justifyContent: 'center' }}>
                          <View>
                            <View style={{ marginTop: 5, paddingLeft: 5, }}>
                              <View style={{ flexDirection: 'row' }}>
                                <View>
                                  <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                    <FastImage source={item.product_info.product_image != null || item.product_info.product_image != '' ? { uri: item.product_info.product_image } : Assets.myProfile.defProductImg}
                                      style={{ height: 100, width: 100, marginHorizontal: 2 }} />
                                  </View>
                                </View>
                                <View style={{ marginHorizontal: 10, }}>
                                  <View style={{ flexDirection: 'row', flex: 1 }}>
                                    <View style={{ width: wp('45%') }}>
                                      <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 15, fontWeight: '600' }}>{item.product_info.product_name}</Text>
                                      {item.data.variations != null && item.data.variations != '' ? <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 15, fontWeight: '600' }}>{item.data.variations}</Text>
                                        : null}
                                    </View>
                                    <View style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                      <TouchableOpacity onPress={() => {
                                        Alert.alert(
                                          'Confirm Remove',
                                          'Are you sure you want to remove this product to your cart?',
                                          [
                                            {
                                              text: 'CANCEL'
                                            },
                                            {
                                              text: 'OK',
                                              onPress: () => handleRemoveCartItem(item.data.id)
                                            },
                                          ],
                                          { cancelable: true }
                                        );
                                      }} style={{ cursor: 'pointer' }}>
                                        <Icons.EvilIcons name="trash" size={30} />
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginVertical: 10 }}>
                                    <TouchableOpacity onPress={() => {
                                      // alert("Minus")
                                      let qty = parseInt(item.data.qty) - 1
                                      if (parseInt(item.data.max_stock) >= qty && qty > 0) {
                                        handleUpdateItem(qty, "yes", item)
                                      }
                                    }} style={{ backgroundColor: 'blue', width: 18, height: 18, borderRadius: 100, alignItems: 'center', justifyContent: 'center', }}>
                                      <Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', color: '#fff', fontWeight: '900' }}>-</Text>
                                    </TouchableOpacity>
                                    <View style={{ marginHorizontal: 30 }}>
                                      <Text>{item.data.qty}</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => {
                                      // alert("Add")
                                      let qty = parseInt(item.data.qty) + 1
                                      if (parseInt(item.data.max_stock) >= qty) {
                                        handleUpdateItem(qty, "yes", item)
                                      }
                                    }} style={{ backgroundColor: 'blue', width: 18, height: 18, borderRadius: 100, alignItems: 'center', justifyContent: 'center', }}>
                                      <Text style={{ textAlign: 'center', alignItems: 'center', justifyContent: 'center', color: '#fff', fontWeight: '900' }}>+</Text>
                                    </TouchableOpacity>
                                  </View>
                                  <View>
                                    <Text style={{ color: '#f00' }}>{'Price: ' + `\u20B1 ` + `${item.data.price}`}</Text>
                                  </View>
                                  <View>
                                    <Text style={{ color: '#f00' }}>{`Delivery Fee: ` + `\u20B1 ` + item.data.delivery_courier_fee}</Text>
                                  </View>
                                  <View>
                                    <Text>{`Courier: ` + item.data.delivery_courier}</Text>
                                  </View>
                                  <View>
                                    <Text>{`Available stock: ` + `${stockHandler[i]}`}</Text>
                                  </View>
                                  {item.shipping_info.delivery_options.length > 1 ?
                                    <TouchableOpacity onPress={() => {
                                      setSelectedItem(item);
                                      setCourierModalVisibility(true);
                                    }}
                                      style={{
                                        alignItems: 'center', justifyContent: 'space-around', backgroundColor: '#7F7F7F', padding: 1, borderRadius: 5, marginTop: 2,
                                      }}>
                                      <Text style={{ fontSize: 15, color: '#ffffff', alignSelf: 'center', }}>
                                        Change Courier
                                      </Text>
                                    </TouchableOpacity>
                                    :
                                    null}
                                </View>
                              </View>
                            </View>
                          </View>
                        </View>
                        {/* {item.shipping_info.delivery_options.length > 1 ?
                          <CardItem style={{ marginHorizontal: 10, borderTopWidth: 1, borderTopColor: '#444' }}>
                            <TouchableOpacity onPress={() => {
                              // navigation.navigate('Seller Shop', { item })
                            }}>
                              <View style={{ flexDirection: 'row', width: 260 }}>
                                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                  <Text style={{ fontSize: 14, color: '#000000', paddingLeft: 10 }}>Change Courier</Text>
                                </View>
                                <View style={{ alignItems: 'flex-end' }}>
                                  <Right>
                                    <Icon name="arrow-forward" />
                                  </Right>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </CardItem>
                          :
                          null} */}
                      </Card>
                    </ListItem>
                  )
                })
              }
            </View>
            <View style={{ height: Dimensions.get('screen').height }}></View>
          </ScrollView>
        </List>
      </View>
      //   </SafeAreaView>
    )
  }

  function compareCart() {
    // console.log("allCartItems", allCartItems);
    return (
      <View style={{ flexDirection: 'row' }}>
        {
          allCartItems == 'loading' ?
            <View style={styles.cartContent}>
              <Text style={[styles.cartContentText, { fontWeight: '100' }]}> Loading Cart</Text>
              {/* <TouchableOpacity onPress={() => navigation.navigate('Home')}
                style={styles.goBackHomeButton}>
                <Text style={styles.goBackHomeText}>Go back home</Text>
              </TouchableOpacity> */}
            </View>
            :
            allCartItems == null || allCartItems == '' ? noCartItem() : withCartItem()
        }
      </View>
    )
  }

  const handleRemoveCartItem = async (id) => {
    // console.warn('rexultx', result)
    try {
      let formData = new FormData();
      formData.append("id", id);
      formData.append("user_id", currentUserdata.data.id);
      formData.append("api_token", token);
      const data = await fetch(Endpoints.remove_cart + `?include=date,info`, {
        method: 'post',
        headers: { 'content-type': 'multipart/form-data' },
        body: formData
      })
      const response = await data.json();
      if (response.status == true) {
        Alert.alert(
          'Success',
          response.msg,
          [
            {
              text: 'OK',
              onPress: () => {
                init()
              }
            },
          ],
          { cancelable: true }
        );
      }
      else {
        Alert.alert(
          'Error',
          response.msg,
          [
            {
              text: 'OK'
            },
          ],
          { cancelable: true }
        );
      }
    }
    catch (e) {
      console.log(e)
    }
  }

  const handleUpdateItem = async (qty, isSelected, item) => {
    try {
      let formData = new FormData()
      formData.append("id", item.data.id)
      formData.append("user_id", currentUserdata.data.id)
      formData.append("api_token", token)
      formData.append("delivery_courier_fee", item.data.delivery_courier_fee)
      formData.append("delivery_courier_slug", item.data.delivery_courier_slug)
      formData.append("qty", qty)
      if (isSelected) formData.append("is_selected", isSelected)
      // console.log(formData)
      const res = await fetch(Endpoints.update_cart_single + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data' },
        body: formData
      })
      const response = await res.json();
      // console.log("response", response)
      if (response) {
        init()
      }
    } catch (err) {
      console.log(err)
    }
  }

  const handleUpdateItemCourier = async (item, courier) => {
    try {
      let formData = new FormData()
      formData.append("id", item.data.id)
      formData.append("user_id", currentUserdata.data.id)
      formData.append("delivery_courier_fee", courier.delivery_option_info.cover == "yes" ? 0 : courier.delivery_option_info.fee)
      formData.append("delivery_courier_slug", courier.delivery_option)
      formData.append("qty", item.qty)
      formData.append("is_selected", item.is_selected)
      formData.append("api_token", token)
      console.log("handleUpdateItemCourier formData", formData)
      const res = await fetch(Endpoints.update_cart_single + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data' },
        body: formData
      })
      const response = await res.json();
      setCourierModalVisibility(false);
      if (response) init()
    } catch (err) {
      console.log(err)
    }
  }

  const handleSelectAllItems = async (isSelected) => {
    try {

      let ids = []
      let slugs = []
      let qtys = []
      let is_selected = []
      let fees = []

      for (var x = 0; x < allCartItems.length; x++) {
        ids.push(allCartItems[x].data.id)
        slugs.push(allCartItems[x].data.delivery_courier_slug)
        fees.push(allCartItems[x].data.delivery_courier_fee)
        qtys.push(allCartItems[x].data.qty)
        is_selected.push("yes")
      }

      let formData = new FormData()
      formData.append("id", JSON.stringify(ids))
      formData.append("user_id", currentUserdata.data.id)
      formData.append("api_token", token)
      formData.append("delivery_courier_fee", JSON.stringify(fees))
      formData.append("delivery_courier_slug", JSON.stringify(slugs))
      formData.append("qty", JSON.stringify(qtys))
      if (isSelected) formData.append("is_selected", JSON.stringify(is_selected))
      // console.log(JSON.stringify(formData))
      const res = await fetch(Endpoints.update_cart_all + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      // console.log(response)
      if (response) init()
    } catch (err) {
      console.log(err)
    }
  }

  const handleCheckout = async () => {
    // console.log("allCartItems", allCartItems);    
    let splitEmail = user.data.info.data.email.split('@');

    if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
      // user.data.is_verify_contact_number === "no" ||
      user.data.is_verify === 'no') {
      Alert.alert(
        'Warning!',
        'Your account not yet been verified.\n' +
        'Verify Now!',
        [
          {
            text: 'Cancel'
          },
          {
            text: 'Verify',
            onPress: () => {
              if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                navigation.navigate("AddEmailMobile");
              } else if (user.data.is_verify === 'no') {
                navigation.navigate('EmailVerification')
              }
              // else if (user.data.is_verify_contact_number === "no") {
              //   navigation.navigate('PhoneVerification')
              // }
            }
          },
        ],
        { cancelable: true }
      );
      return
    }

    let checkoutData = [];
    checkoutData.items = [];

    // allCartItems.map((item, index) => {
    //   let itemToPush = [];
    //   itemToPush.pid = item.data.id;
    //   itemToPush.amount = item.data.qty;

    //   if (item?.product_info?.with_variation.toString().toLowerCase() === "no") {
    //     itemToPush.variations = null;
    //     itemToPush.variation_1 = null;
    //     itemToPush.variation_2 = null;
    //   } else if (item?.product_info?.with_variation.toString().toLowerCase() === "yes") {
    //     itemToPush.variations = item.data.variations;
    //     itemToPush.variation_1 = item.data.variation_1;
    //     itemToPush.variation_2 = item.data.variation_2;
    //   }

    //   checkoutData.items.push(itemToPush);
    // })
    // console.log("user", user);

    checkoutData.uid = user.data.id;
    checkoutData.checkoutType = "multiple";

    navigation.navigate('Checkout',
      {
        checkoutData
      }
    );

    // try {
    //   await Service.checkoutPaypal(
    //     checkoutData,
    //     (res) => {
    //       if (res.errors) {
    //         console.log('checkoutPaypal error 3', res.errors);
    //         Alert.alert('Upload Error', 'Please try again');
    //       } else {
    //         console.log('checkoutPaypal success', res);
    //         navigation.navigate('PaypalCheckout',
    //           {
    //             url: res.redirectUrl
    //           }
    //         );
    //       }
    //     },
    //     (err) => {
    //       console.log("checkoutPaypal error 2", err);
    //       Alert.alert('Upload Error', 'Please try again');
    //     },
    //   );
    // } catch (error) {
    //   console.log("checkoutPaypal error 1", error);
    //   Alert.alert('Upload Error', 'Please try again');
    // }

  }

  const getSubTotal = () => {
    let res = 0;
    if (allCartItems.length == 0 || allCartItems == 'loading') return res
    for (var x = 0; x < allCartItems.length; x++) {
      if (allCartItems[x].data.is_selected == 'yes') {
        // console.log("")
        res += parseFloat(allCartItems[x].data.total) + (parseFloat(allCartItems[x].data.delivery_courier_fee) * parseFloat(allCartItems[x].data.qty));
      }
    }
    return res.toFixed(2);
  }

  const countItems = () => {
    let res = 0;
    for (var x = 0; x < allCartItems.length; x++) {
      res += allCartItems[x].data.is_selected == 'yes' ? 1 : 0
    }
    return res
  }

  if (!userCheck) {
    return <Auth navigation={navigation} />
  }

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={styles.headerStyle}>
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.headerText}>Your Cart</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>

      {renderCourierModal()}

      <Content scrollEnabled={false}>
        <View style={{ height: Dimensions.get('screen').height + 120 }}>
          {compareCart()}
        </View>
        {/* <Card style={stylings.cardHeight}>
          <CardItem>
            <Body>
              
            </Body>
          </CardItem>
        </Card> */}
      </Content>
      <Footer style={styles.footer1}>
        <FooterTab style={styles.footerTab}>
          <CheckBox
            checkedImage={Assets.cartFunction.checkbox}
            uncheckedImage={Assets.cartFunction.uncheck}
            checkboxStyle={{
              width: 18,
              height: 18
            }}
            label='Select All'
            labelStyle={{
              fontSize: 12,
              color: '#000000',
              paddingLeft: 10,
              // fontFamily:'Roboto'
            }}
            checked={selectAll}
            onChange={(value) => {
              handleSelectAllItems(value.checked)
              setSelectAll(value.checked)
            }}
          />
          {/* <TouchableOpacity style={{
              flexDirection:'row'
          }}> */}
          <View style={{ flexDirection: 'row' }}>
            <Text style={styles.txtSubTotal}>Total:      </Text>
            <Text style={styles.txtPrice}>₱ {getSubTotal()}  </Text>
          </View>
          {/* </TouchableOpacity> */}
        </FooterTab>
      </Footer>
      <Footer style={styles.footer2}>
        <FooterTab style={styles.footerTab2}>
          <TouchableOpacity onPress={() => {
            handleCheckout();
            // Alert.alert(
            //   'Confirm Purchase',
            //   'Are you sure you want to buy this product?',
            //   [
            //     {
            //       text: 'CANCEL'
            //     },
            //     {
            //       text: 'OK',
            //       onPress: () => handleCheckout()
            //     },
            //   ],
            //   { cancelable: true }
            // );
          }}
            style={[{ width: Dimensions.get('window').width - 40, }, styles.btnCheckout]}>
            <Text style={styles.btnTextCheckout}>Check Out</Text>
          </TouchableOpacity>
        </FooterTab>
      </Footer>
    </Container>
  )
}

const stylings = StyleSheet.create({
  checkboxContainer: {
    flexDirection: "row",
  },
  checkbox: {
    alignSelf: "center",
  },
  label: {
    margin: 8,
  },
  viewContainer: {
    ...Platform.select({
      android: {
        // height: hp('100%')
        height: 215
      },
      ios: {
        height: 215
      }
    })
  },
  cardHeight: {
    ...Platform.select({
      android: {
        height: Dimensions.get('screen').height - 210
      },
      ios: {

      }
    })
  },
  modalBackgroundContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  courierFeeText: {
    fontSize: 14,
    color: '#333',
  },
  addresNameText: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
  },
})