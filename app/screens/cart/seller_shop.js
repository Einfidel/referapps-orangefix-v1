import React from 'react';

import {
  Dimensions,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';

import { Container, Input, Item, Content, Thumbnail } from 'native-base';

import FastImage from 'react-native-fast-image';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../components/assets.manager'
import AsyncStorage from '@react-native-community/async-storage';
import { ScrollView } from 'react-native-gesture-handler';
import MyProducts from './components/my_products'

class ViewYourShop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'allproducts', title: 'All Products' },
        { key: 'referred', title: 'Referred' },
        { key: 'overview', title: 'Overview' },
      ],

      data: {},
      sellerData: {},
      productData: {},
      shippingData: {},
      userData: {},
      buyerData: {},
      pinsData: [],
    }
  }

  handlePinItems = async () => {
    let user = await AsyncStorage.getItem('user_data');
    let _userdata = user ? JSON.parse(user) : null
    const api_url = 'https://www.referapps.com';
    const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
    const pinned = '/api/pin/all.json';

    const res = await fetch(api_url + pinned + `?user_id=` + _userdata.data.id + `&include=info,date&page=1&per_page=10&api_token=${token}`, {
      method: 'get',
      headers: {
        "content-type": "application/json",
      }
    })
    let response = await res.json()
    let data = []
    response.data.map((result, index) => {
      data.push({
        data: {
          index: index,
          pin_id: result.id,
          product_id: result.product_id,
          product_name: result.info.data.product_info.product_name,
          price: result.info.data.product_info.price,
          country: result.info.data.product_info.country_code,
          rating: result.ratings,
          username: result.info.data.product_info.user_name,
          user_id: result.info.data.product_info.user_id,
        },
        uri: result.info.data.product_info.image.full_path,
        has_morepages: result.has_morepages,
      })
    })
    this.setState({
      userData: _userdata.data,
      pinsData: data
    })
  }

  componentDidMount = async () => {
    const response = this.props.route.params.result
    this.setState({
      data: response.data,
      sellerData: response.seller_info,
      productData: response.product_info,
      shippingData: response.shipping_info,
      buyerData: response.buyer_info,
    })
    await this.handlePinItems()
    // console.warn('sett', this.state.buyerData)
  }

  _renderLabel = props => ({ route, focused, }) => {
    const color = focused ? '#231f20' : '#7f8c8d'
    return (
      <Text style={[{ fontSize: 16 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  renderAvatar = () => {
    return (
      <>
        <FastImage
          source={this.state.userData != null || this.state.userData != '' ? { uri: this.state.userData.image } : Assets.myProfile.face}
          style={styles.defaultProfile}
        />
      </>
    );
  }

  handleNoPins = () => {
    return (
      <View>
        <Text>
          No pin items!
        </Text>
      </View>
    )
  }

  handleWithPins = () => {
    const data = this.state.pinsData
    return (
      <View style={{ flex: 1, flexDirection: 'row' }}>
        <ScrollView
          horizontal={true}
          showsVerticalScrollIndicator={false}
        >
          {
            data.map((result, i) => {
              // console.warn('rs', result.data.product_id)
              return (
                <View style={{ paddingVertical: 15, paddingRight: 15 }}>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProductDetails', { product_id: result.data.product_id }) }}>
                    <FastImage
                      source={data != null || data != '' ? { uri: result.uri } : Assets.myProfile.face}
                      style={{ height: 60, width: 60, borderColor: 'red', borderWidth: .5, borderRadius: 100 }}
                    />
                  </TouchableOpacity>
                </View>
              )
            })
          }
        </ScrollView>
      </View>
    )
  }

  handleCheckPins = () => {
    return (
      <>
        {
          this.state.pinsData == null || this.state.pinsData == '' ? this.handleNoPins() : this.handleWithPins()
        }
      </>
    )
  }

  AllProducts = () => {
    return (
      <>
        <MyProducts />
      </>
    );
  }

  Referred = () => {
    return (
      <View style={styles.referredSection}>
        <Text style={styles.referredText}>This user have no Referred items yet.</Text>
      </View>
    )
  }

  Overview = () => {
    return (
      <View style={styles.overviewSection}>
        <View>
          <View style={{ flexDirection: 'row' }}>
            <View style={{ flexDirection: 'column' }}>
              <Text style={styles.text1}>Member since </Text>
            </View>
            <View style={{ flexDirection: 'column' }}>
              <Text style={styles.dateText}>Mar 2020</Text>
            </View>
          </View>
          <View style={styles.locationSection}>
            <FastImage
              source={Assets.stores.locationIcon}
              style={styles.locationIcon}
              tintColor={'#080808'}
            />
            <Text style={styles.locationText}>Asia/Manila</Text>
          </View>
          <View style={{ paddingTop: 25 }}>
            <Text style={styles.text1}>Interesting</Text>
            <View style={styles.emailSection}>
              <FastImage
                source={Assets.accountSettings.emailIcon}
                style={styles.emailIcon}
                tintColor='black'
              />
              <Text style={styles.emailText}>ardianonavarro.viray@yahoo.com</Text>
            </View>
          </View>
          <View style={{ paddingTop: 12 }}>
            <Text style={styles.text1}>Other Accounts</Text>
            <View style={{ paddingTop: 5 }}>
              <FastImage
                source={Assets.accountSettings.socialIcon1}
                style={styles.facebookIcon}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'allproducts':
        return this.AllProducts(); // passing data as data prop
      case 'referred':
        return this.Referred();
      case 'overview':
        return this.Overview();
      default:
        return null;
    }
  };

  render() {
    return (
      <Container>
        <ImageBackground
          source={Assets.myProfile.bg}
          style={styles.headerCoverImage} >
          <View style={styles.backgroundImageContainer}>
            <Content>
              <View>
                <View style={styles.MainContainer}>
                  <View style={styles.headerRow}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.touchableBackButton}>
                      <FastImage
                        source={Assets.accountSettings.arrowDarkIcon}
                        style={styles.backIcon}
                        tintColor={'#ffffff'}
                      />
                    </TouchableOpacity>
                    <View style={{ paddingLeft: hp('2.50%'), }}>
                      <Item style={styles.ItemContainer} last>
                        <FastImage
                          source={Assets.accountSettings.searchHelpcenter}
                          style={styles.searchIcon}
                        />
                        <Input
                          placeholder='search'
                          placeholderTextColor='#7f8c8d'
                          style={styles.searchText}
                          onChangeText={(search) => this.setState({ search })}
                        />
                      </Item>
                    </View>
                    <TouchableOpacity style={styles.touchableChatIcon} onPress={() => {
                      Alert.alert('Coming Soon!', 'This feature will be available soon.')
                      // this.props.navigation.navigate("Messages")
                    }}>
                      <FastImage
                        source={Assets.myProfile.chatIcon}
                        style={[styles.cartIcon, { marginLeft: 10, marginRight: 5 }]}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchableCartIcon} onPress={() => this.props.navigation.navigate('Cart Screen')}>
                      <FastImage
                        source={Assets.myProfile.cartIcon}
                        style={styles.cartIcon}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={[styles.profileRow]}>
                  {this.renderAvatar()}
                  <View style={[styles.column1, { paddingBottom: hp('0%') }]}>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.userNameText, { width: wp('30%') }]}>{this.state.userData.shop_name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Products : </Text>
                      <Text style={styles.productValues}>{this.state.userData ? this.state.userData?.products : 0}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Services : </Text>
                      <Text style={styles.Values}>{this.state.userData ? this.state.userData?.services : 0}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Reviews :  </Text>
                      <Text style={styles.Values}>{this.state.userData ? this.state.userData?.reviews : 0}</Text>
                    </View>
                  </View>
                  <View style={[styles.column2, { paddingBottom: 0, width: wp('50%') }]}>
                    <Text style={styles.ShopRatingText}>Shop Rating</Text>
                    <FastImage
                      source={Assets.stores.starIcon}
                      style={styles.starIcon}
                      tintColor={'#ffa012'}
                    />
                    <View style={{ flexDirection: 'row', }}>
                      <Text style={styles.ratingText}>N/A out of 5</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Content>
          </View>
        </ImageBackground>
        <Content>
          <View style={{ padding: 15 }}>
            <View style={{ flexDirection: 'row' }}>
              <Thumbnail
                source={require('../../assets/internal/icon_Unpin.png')}
                style={{ height: 25, width: 25, marginRight: 5 }}
                square
              />
              <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 18, fontWeight: '300', color: 'orange' }}>
                  Pinned items
                </Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              {this.handleCheckPins()}
            </View>
          </View>
          <TabView
            navigationState={this.state}
            renderScene={this.renderScene}
            // renderScene={SceneMap({
            //   allproducts: AllProducts,
            //   referred: Referred,
            //   overview: Overview,
            // })}
            renderTabBar={props =>
              <TabBar
                {...props}
                labelStyle={{
                  // fontFamily: 'Roboto', 
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 2.50, borderBottomColor: '#f36e23' }}
                style={{ backgroundColor: '#ffffff', elevation: 0 }}
                renderLabel={this._renderLabel(props)}
              />
            }
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
          />
        </Content>
      </Container>
    );
  }

}

export default ViewYourShop;