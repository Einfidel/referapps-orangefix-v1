import { Container, Content } from 'native-base';
import React, { useEffect, useState } from 'react';
import { View, Text, Dimensions, TouchableOpacity, RefreshControl, ScrollView, StyleSheet, Image } from 'react-native';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import styles from '../../../styles/profile/view_your_shop'
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-community/async-storage';
import { FlatGrid } from 'react-native-super-grid';
import Icons from '../../../components/icons'
import Assets from '../../../components/assets.manager'
import FastImage from 'react-native-fast-image'

class MyProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'popularity', title: 'Popular' },
        { key: 'latest', title: 'Latest' },
        { key: 'topsales', title: 'Top Sales' },
        { key: 'price', title: 'Price' },
      ],
      myProducts: [],
      myServices: [],
      loading: false
    }
  }

  handleMyProducts = async () => {
    let user = await AsyncStorage.getItem('user_data');
    let _userdata = user ? JSON.parse(user) : null
    const api_url = 'https://www.referapps.com';
    const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
    const _myproduct = '/api/product/user-products.json'

    const res = await fetch(api_url + _myproduct + `?user_id=` + _userdata.data.id + `&include=info,date&page=1&per_page=10&api_token=${token}&type=product`)
    const response = await res.json()
    let datas = []
    response.data.map(async (result, index) => {
      datas.push({
        data: {
          index: index,
          pin_id: result.id,
          product_id: result.product_id,
          product_name: result.info.data.product_name,
          price: result.info.data.price,
          country: result.info.data.country_code,
          rating: result.ratings,
          username: result.info.data.user_name,
          user_id: result.info.data.user_id,
        },
        uri: result.info.data.image.full_path,
        renderFooter: (data) => {
          return (
            <View style={{
              backgroundColor: '#ffffff',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              paddingBottom: 5,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 5,
                paddingRight: 5,
              }}>
                <View style={{ flex: 1, justifyContent: 'space-around' }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: '#231f20',
                    }}
                  >{data.username}</Text>
                </View>

                <View style={{ flex: 0 }}>

                </View>
              </View>
              <Text
                style={{
                  fontFamily: 'Roboto',
                  paddingLeft: 2,
                  paddingTop: 2,
                  color: '#231f20',
                  fontSize: 14,
                }}
                numberOfLines={1}
                ellipsizeMode='tail'
              >{data.product_name}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ alignSelf: 'flex-start' }}>
                  <Text style={{
                    fontFamily: 'Roboto',
                    fontSize: 10,
                    paddingLeft: 2,
                    color: '#ea4123',
                  }}>
                    {data.country === 'PH' && '₱'}{data.country === 'US' && '$'} {data.price}
                  </Text>
                </View>
                <View style={{ alignSelf: 'flex-end' }}>
                  {data.rating >= 0 ? <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor={'#f36e23'}
                    halfStarColor={'#f36e23'}
                    starSize={10}
                    style={{ justifyContent: 'flex-end' }}
                  /> : <Text style={{
                    fontFamily: 'Roboto',
                    color: '#7f8c8d',
                    fontSize: 10,
                  }}>({data.rating})</Text>}
                </View>
              </View>
            </View>
          )
        },
      })
    })
    // console.log('asda', JSON.stringify(response))
    this.setState({
      myProducts: datas
    })
  }

  handleMyServices = async () => {
    let user = await AsyncStorage.getItem('user_data');
    let _userdata = user ? JSON.parse(user) : null
    const api_url = 'https://www.referapps.com';
    const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
    const _myproduct = '/api/product/user-products.json'

    const res = await fetch(api_url + _myproduct + `?user_id=` + _userdata.data.id + `&include=info,date&page=1&per_page=10&api_token=${token}&type=service`)
    const response = await res.json()
    let datas = []
    response.data.map(async (result, index) => {
      datas.push({
        data: {
          index: index,
          pin_id: result.id,
          product_id: result.product_id,
          product_name: result.info.data.product_name,
          price: result.info.data.price,
          country: result.info.data.country_code,
          rating: result.ratings,
          username: result.info.data.user_name,
          user_id: result.info.data.user_id,
        },
        uri: result.info.data.image.full_path,
        renderFooter: (data) => {
          return (
            <View style={{
              backgroundColor: '#ffffff',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              paddingBottom: 5,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 5,
                paddingRight: 5,
              }}>
                <View style={{ flex: 1, justifyContent: 'space-around' }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: '#231f20',
                    }}
                  >{data.username}</Text>
                </View>

                <View style={{ flex: 0 }}>

                </View>
              </View>
              <Text
                style={{
                  fontFamily: 'Roboto',
                  paddingLeft: 2,
                  paddingTop: 2,
                  color: '#231f20',
                  fontSize: 14,
                }}
                numberOfLines={1}
                ellipsizeMode='tail'
              >{data.product_name}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ alignSelf: 'flex-start' }}>
                  <Text style={{
                    fontFamily: 'Roboto',
                    fontSize: 10,
                    paddingLeft: 2,
                    color: '#ea4123',
                  }}>
                    {data.country === 'PH' && '₱'}{data.country === 'US' && '$'} {data.price}
                  </Text>
                </View>
                <View style={{ alignSelf: 'flex-end' }}>
                  {data.rating >= 0 ? <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor={'#f36e23'}
                    halfStarColor={'#f36e23'}
                    starSize={10}
                    style={{ justifyContent: 'flex-end' }}
                  /> : <Text style={{
                    fontFamily: 'Roboto',
                    color: '#7f8c8d',
                    fontSize: 10,
                  }}>({data.rating})</Text>}
                </View>
              </View>
            </View>
          )
        },
      })
    })
    this.setState({
      myServices: datas
    })
  }

  _renderLabel = props => ({ route, focused }) => {
    const color = focused ? '#231f20' : '#7f8c8d'
    return (
      <Text style={[{ fontSize: 16 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  componentDidMount() {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        this.handleMyProducts()
        this.handleMyServices()
      } else {
        this.setState({
          loading: false
        })
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
    this.setState({
      loading: false
    })
  }

  AllProducts() {
    return (
      <View style={{ backgroundColor: '#ffffff', }}>
        <View style={styles.allProductsView}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={() => {
                  NetInfo.fetch().then((state) => {
                    if (state.isConnected) {
                      this.handleMyProducts();
                      this.handleMyServices();
                    } else {
                      this.setState({
                        loading: false
                      })
                      Alert.alert('Not Connected', 'You have no internet connection');
                    }
                  });
                }}
              />
            }
          >
            <View>
              <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 15 }}>
                  <Text style={{ fontSize: 16 }}>My Products</Text>
                </View>
                <FlatGrid
                  itemDimension={150}
                  data={this.state.myProducts}
                  style={stylings.gridView}
                  // staticDimension={300}
                  // fixed
                  spacing={10}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ProductDetails', { product_id: item.data.product_id })}
                    >
                      <View style={[stylings.itemContainer, { backgroundColor: '#fff' }]}>
                        <View style={{ flexDirection: 'column' }}>
                          <FastImage source={item != null || item != '' ? { uri: item.uri } : Assets.myProfile.defProductImg} style={{ height: 175, resizeMode: 'cover' }} resizeMode={FastImage.resizeMode.cover} />
                        </View>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={stylings.itemName}>{item.data.product_name}</Text>
                        <View style={{ flexDirection: 'row' }}>
                          <Text numberOfLines={1} ellipsizeMode='tail' style={stylings.itemCode}>{`\u20B1 ` + item.data.price}</Text>
                          {
                            item.data.rating == 'No ratings yet' ? (
                              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 9, color: '#000', alignItems: 'center' }}>{item.data.rating}</Text>
                              </View>
                            )
                              :
                              (
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                  {[1, 2, 3, 4, 5].map((rate, i) => {
                                    return (
                                      <Icons.AntDesign key={i} name={item.data.rating >= rate ? 'star' : 'staro'} size={10} color='orange' />
                                    );
                                  })}
                                </View>
                              )
                          }
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                />
              </View>
              <View style={{ flex: 1 }}>
                <View style={{ paddingHorizontal: 15 }}>
                  <Text style={{ fontSize: 16 }}>My Services</Text>
                </View>
                <FlatGrid
                  itemDimension={150}
                  data={this.state.myServices}
                  style={stylings.gridView}
                  // staticDimension={300}
                  // fixed
                  spacing={10}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate('ProductDetails', { product_id: item.data.product_id })}
                    >
                      <View style={[stylings.itemContainer, { backgroundColor: '#fff' }]}>
                        <View style={{ flexDirection: 'column' }}>
                          <FastImage source={item != null || item != '' ? { uri: item.uri } : Assets.myProfile.defProductImg} style={{ height: 175, resizeMode: 'cover' }} resizeMode={FastImage.resizeMode.cover} />
                        </View>
                        <Text numberOfLines={1} ellipsizeMode='tail' style={stylings.itemName}>{item.data.product_name}</Text>
                        <View style={{ flexDirection: 'row' }}>
                          <Text numberOfLines={1} ellipsizeMode='tail' style={stylings.itemCode}>{item.data.price}</Text>
                          {
                            item.data.rating == 'No ratings yet' ? (
                              <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                                <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 9, color: '#000', alignItems: 'center' }}>{item.data.rating}</Text>
                              </View>
                            )
                              :
                              (
                                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                  {[1, 2, 3, 4, 5].map((rate, i) => {
                                    return (
                                      <Icons.AntDesign key={i} name={item.data.rating >= rate ? 'star' : 'staro'} size={10} color='orange' />
                                    );
                                  })}
                                </View>
                              )
                          }
                        </View>
                      </View>
                    </TouchableOpacity>
                  )}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    )
  }

  Latest() {
    return (
      <View style={{ backgroundColor: '#ffffff', }}>
        <View style={styles.allProductsView}>
        </View>
        <View style={{ paddingVertical: 25 }}>
          <Text style={{ fontSize: 12, color: '#7f8c8d', fontStyle: 'italic', textAlign: 'center' }}>There are no items yet.</Text>
        </View>
      </View>
    )
  }

  TopSales() {
    return (
      <View style={{ backgroundColor: '#ffffff', }}>
        <View style={styles.allProductsView}>
        </View>
        <View style={{ paddingVertical: 25 }}>
          <Text style={{ fontSize: 12, color: '#7f8c8d', fontStyle: 'italic', textAlign: 'center' }}>There are no items yet.</Text>
        </View>
      </View>
    )
  }

  Price() {
    return (
      <View style={{ backgroundColor: '#ffffff', }}>
        <View style={styles.allProductsView}>
        </View>
        <View style={{ paddingVertical: 25 }}>
          <Text style={{ fontSize: 12, color: '#7f8c8d', fontStyle: 'italic', textAlign: 'center' }}>There are no items yet.</Text>
        </View>
      </View>
    )
  }

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'popularity':
        return this.AllProducts() // passing data as data prop
      case 'latest':
        return this.Latest();
      case 'topsales':
        return this.TopSales();
      case 'price':
        return this.Price();
      default:
        return null;
    }
  };

  render() {
    return (
      <Container>
        <Content>
          <TabView
            navigationState={this.state}
            renderScene={this.renderScene}
            renderTabBar={props =>
              <TabBar
                {...props}
                labelStyle={{
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 2.50, borderBottomColor: '#f36e23', color: '#f36e23' }}
                style={{ backgroundColor: '#ffffff', elevation: 0, color: '#f36e23' }}
                renderLabel={this._renderLabel(props)}
              />
            }
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
          />
        </Content>
      </Container>
    )
  }
}

const stylings = StyleSheet.create({
  gridView: {
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    paddingVertical: 10,
    elevation: 5,
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  itemName: {
    fontSize: 14,
    color: '#000',
    fontWeight: '400',
    paddingTop: 5
  },
  itemCode: {
    fontWeight: '400',
    fontSize: 12,
    color: '#f00',
    flex: 1,
    alignSelf: 'flex-start',
  },
})

export default MyProducts;