import React from 'react';
import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
  con: {
    flex: 1,
    backgroundColor: 'white'
  },
  arrowCon: {
    position: 'absolute',
    left: 0,
    alignSelf: 'center'
  },
  imgBack: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  txtMessages: {
    ...Platform.select({
      ios: {
        // fontFamily: 'roboto.medium',
        fontSize: 18,
        alignSelf: 'center',
        marginTop: -15
      },
      android: {
        // fontFamily: 'roboto.medium',
        fontSize: 18,
        alignSelf: 'center'
      }
    })
  },
  searchCon: {
    position: 'absolute',
    right: 0,
    alignSelf: 'center'
  },
  imgSearch: {
    width: 23,
    height: 23,
    resizeMode: 'contain',
    tintColor: '#7f8c8d',
    marginRight: 17,
  },
  chatCon: {
    ...Platform.select({
      ios: {
        flexDirection: 'row',
        padding: 0,
      },
      android: {
        flexDirection: 'row',
        padding: 0
      }
    })
  },
  profile: {
    height: 50,
    width: 50,
    borderRadius: 25,
    paddingTop: 10
  },
  textCon: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10
  },
  txtName: {
    textAlign: 'left',
    fontSize: 16,
    fontWeight: 'bold',
    // fontFamily:'Roboto'
  },
  txtChat: {
    color: '#7f8c8d',
    // fontFamily:'Roboto',
    fontSize: 11,
    marginTop: 5
  },
})

export default styles