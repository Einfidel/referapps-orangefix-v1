import React, { useState, useCallback, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, BackHandler, ActivityIndicator } from 'react-native';
import { Header, Thumbnail, Icon } from 'native-base';
import Assets from '../../components/assets.manager.js';
import { GiftedChat, Send } from 'react-native-gifted-chat';
import Notifications from '../../components/Notifications';
import Endpoints from '../../components/api/endpoints';
import Fire from '../../../FireChat';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'

//START A CONVERSATION

export default function Conversation(props) {
  const [loading, setloading] = useState(false)
  const [messages, setMessages] = useState([]);
  const [userId, setUserId] = useState(null);
  const [userName, setUserName] = useState(null);
  const [userAvatar, setUserAvatar] = useState(null);
  const [recieverId, setRecieverId] = useState(null)
  const [recieverName, setRecieverName] = useState(null)
  const [recieverAvatar, setRecieverAvatar] = useState(null)
  const [threadId, setThreadId] = useState(null);

  const onSend = async (messages) => {
    // alert(messages[messages.length - 1].text)
    // if (props.route.params) {
    // await Notifications.getReceiverToken(
    //   props.route.params.reciever.id,
    //   async (receiverToken) => {
    //     await Notifications.sendNotification(
    //       userId,
    //       `You have received a new message from ${userName}.`,
    //       receiverToken,
    //       'chat',
    //     );
    //     console.warn('Receiver token', receiverToken);
    //   },
    // );
    // }

    if (threadId) {
      handleSendMessage(messages[messages.length - 1].text)
    } else {
      let thread = await handleCreateThread()
      if (thread) {

      }
    }

  }

  const handleGetThreadList = async () => {
    try {
      let formData = new FormData();
      formData.append("user_id", userId)
      formData.append("api_token", Endpoints.token)
      const res = await fetch(Endpoints.thread_list + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      console.log(response)
      if (response) return response
      else return false;
    } catch (err) {
      console.log(err)
    }
  }

  const handleGetMessages = async () => {
    try {
      let formData = new FormData();
      formData.append("thread_name", recieverName)
      formData.append("thread_id", threadId)
      formData.append("api_token", Endpoints.token)
      formData.append("page", 1)
      formData.append("per_page", 100)
      const res = await fetch(Endpoints.thread_messages + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      console.log(response)
      if (response) return response
      else return false;
    } catch (err) {
      console.log(err)
    }
  }

  const handleCreateThread = async () => {
    try {
      if (!userId && !recieverId || !userId || !recieverId) return false;
      let formData = new FormData();
      formData.append('user_id', userId);
      formData.append('reciever_id', recieverId);
      // formData.append('page', 1);
      // formData.append('per_page', 100);
      formData.append('api_token', Endpoints.token);
      console.log("FORMDATA", formData)
      const res = await fetch(Endpoints.create_thread + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      console.log(response)
      if (response) return response
      else return false
    } catch (err) {
      console.log(err)
    }
  }

  const handleSendMessage = async (message) => {
    try {
      let formData = new FormData();
      formData.append("user_id", userId)
      formData.append("thread_id", threadId)
      formData.append("message", message)
      formData.append("api_token", Endpoints.token)
      const res = await fetch(Endpoints.send_message + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      console.log(response)
      // if(response) await renderMessages()
    } catch (err) {
      console.log(err)
    }
  }

  const init = async () => {
    setloading(true)
    let ud = await AsyncStorage.getItem("user_data")
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    if (user) {
      setUserId(user.data.id)
      setUserAvatar(user.data.image)
      setUserName(user.data.fname + " " + user.data.lname)
    } else {
      return false
    }
    setRecieverId(props.route.params.reciever.id)
    setRecieverName(props.route.params.reciever.name)
    setRecieverAvatar(props.route.params.reciever.avatar)

    await renderMessages()
    // console.log("THREAD LIST", threadlist)
  }

  const renderMessages = async () => {
    let threadlist = await handleGetThreadList()
    if (threadlist) {
      let thread = threadlist.data.findIndex((t, i) => t.thread_name == props.route.params.reciever.name)
      if (thread >= 0) {
        await setThreadId(threadlist.data[thread].data.id)
        let msg = await handleGetMessages()
        if (msg) {
          let msgs = []
          for (var x = 0; x < msg.data.length; x++) {
            msgs.push({
              _id: msg.data[x].id,
              text: msg.data[x].message,
              createdAt: msg.data[x].date.data.added_in.timestamp.date.split(".")[0],
              user: {
                _id: msg.data[x].sender_id,
                name: userName,
                avatar: msg.data[x].avatar,
              },
            })
          }
          setMessages(msgs)
        }
      }
    }
    setloading(false)
  }

  useEffect(() => {
    init()
  }, [])

  // useEffect(() => {
  //   console.warn('desc', props.navigation.goBack())
  //   if (convo_id) {
  //     console.log('listening');
  //     Fire.shared.on(convo_id, (message) => {
  //       setMessages((previousMessages) =>
  //         GiftedChat.append(previousMessages, message),
  //       );
  //     });
  //   }
  //   return Fire.shared.off()
  // }, [convo_id]);

  const handleBack = () => {
    props.navigation.goBack();
    try {
      let { refresh } = props.route.params;
      refresh !== undefined && refresh();
    } catch (error) {
      console.warn('Error', error.message);
    }
  };

  useEffect(() => {
    const backAction = () => {
      handleBack();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  return (
    <View style={styles.con}>
      <Header style={{ backgroundColor: '#FFF' }}>
        <TouchableOpacity
          onPress={handleBack}
          style={[styles.arrowCon, { zIndex: 1 }]}>
          <FastImage
            source={Assets.accountSettings.arrowDarkIcon}
            style={styles.imgBack}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Thumbnail small source={{ uri: props.route.params.reciever.avatar }} />
          <Text style={{ ...styles.txtMessages, marginHorizontal: 5 }}>
            {props.route.params.reciever.name || 'Messages'}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => alert('Coming Soon')}
          style={styles.searchCon}>
          {/* <Image source={{uri: props.route.params.reciever.avatar} || Assets.landing.icon_search} style={styles.imgSearch}/> */}
          {/* <Thumbnail small source={{uri: props.route.params.reciever.avatar}} /> */}
        </TouchableOpacity>
      </Header>
      <View style={{ flex: 1 }}>
        <GiftedChat
          messages={messages}
          onSend={onSend}
          placeholder={'Type a message'}
          user={{
            _id: userId,
            name: userName,
            avatar: userAvatar == '' ? null : userAvatar,
          }}
          renderSend={(props) => (
            <Send
              {...props}
              containerStyle={{
                height: 60,
                width: 60,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon active name="send" />
            </Send>
          )}
          renderChatEmpty={() => (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ scaleY: -1 }],
              }}>
              <Text>Start a conversation</Text>
            </View>
          )}
          showAvatarForEveryMessage={true}
          renderUsernameOnMessage={true}
          scrollToBottom={true}
          loadEarlier={true}
          isLoadingEarlier={loading}
          onLoadEarlier={handleGetMessages}
          renderLoadEarlier={() => (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{ scaleY: -1 }],
              }}>
              <ActivityIndicator />
            </View>
          )}
        />
      </View>
    </View>
  );
}
