import React, { useEffect, useState } from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {
  Header,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Input,
  Item,
  Icon,
} from 'native-base';
import Fire from '../../../FireChat';
import Assets from '../../components/assets.manager.js';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import Endpoints from '../../components/api/endpoints';
import styles from './styles';
import FastImage from 'react-native-fast-image'

const { width, height } = Dimensions.get('window');

export default function ChatScreen(props) {

  const [userId, setUserId] = useState('');
  const [referId, setReferId] = useState('');
  const [loading, setloading] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [conversations, setconversations] = useState([]);
  const [searched, setsearched] = useState([]);

  const init = async () => {
    setloading(true);
    let ud = await AsyncStorage.getItem("user_data")
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    if (!user) return
    // console.log("USER", user)
    setUserId(user.data.id)
    setReferId(user.data.refer_id)
  };

  useEffect(() => {
    isLoaded && getNewMessages();
  }, [isLoaded]);

  useEffect(() => {
    init().then(() => setIsLoaded(true));
  }, []);

  const getNewMessages = async () => {
    try {
      let formData = new FormData();
      formData.append("user_id", userId)
      formData.append("api_token", Endpoints.token)
      const res = await fetch(Endpoints.thread_list + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      })
      const response = await res.json();
      console.log(response)
      if (response) {
        setconversations(response.data)
        setloading(false)
      }
      else return false;
    } catch (err) {
      getNewMessages()
      console.log(err)
    }
  }

  const handleSearch = (term) => {
    let searches = []
    for (var x = 0; x < conversations.length; x++) {
      if (conversations[x].thread_name.includes(term)) {
        searches.push(conversations[x])
      }
    }
    setsearched(searches)
  }

  const renderContact = ({ item, index }) => {
    console.log("item", item)
    let sender = item.data.participants_id.split(":")[1] == referId ? 'You: ' : ''
    let elapsed = moment(item.data.updated_at).fromNow()
    return (
      <ListItem
        noIndent
        style={{ backgroundColor: item.unread_messages > 0 ? '#d5f0d5' : null, paddingBottom: 10 }}
        avatar
        onPress={() => {
          console.log(item)
          // Fire.shared.readAll(id);
          props.navigation.navigate('ConvoMessages', {
            thread: {
              id: item.data.id,
              avatar: item.thread_image,
              name: item.thread_name
            },
            refresh: init,
          });
        }}>
        <Left noIndent>
          <Thumbnail
            source={{
              uri:
                item.thread_image == ''
                  ? 'https://i.pinimg.com/originals/82/d8/6a/82d86ac15e271d9d1fea30436bfc9bfd.jpg'
                  : item.thread_image,
            }}
          />
        </Left>
        <Body style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ width: width - 200 }}>
            <Text style={styles.txtName}>{item.thread_name || 'Name'}</Text>
            <Text
              numberOfLines={1}
              note
              style={{
                fontWeight: item.unread_messages > 0 ? 'bold' : 'normal',
              }}>
              {item.data.last_message ? sender + ' ' + item.data.last_message : 'No messages'}
            </Text>
          </View>
          <Text style={{ fontSize: 11, width: 100, textAlign: 'right' }} note>
            {/* {elapsed.substr(0, 1).toUpperCase() + elapsed.substr(1, elapsed.length - 1)} */}
            {elapsed}
          </Text>
        </Body>
      </ListItem>
    );
  };

  const noMessages = () => {
    return (
      <View style={{ flex: 1, height: height - 170, justifyContent: 'center', alignItems: 'center' }}>
        <Text>No messages</Text>
      </View>
    );
  };

  return (
    <View style={styles.con}>
      <Header style={{ backgroundColor: '#FFF' }}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.arrowCon}>
          <FastImage
            source={Assets.accountSettings.arrowDarkIcon}
            style={styles.imgBack}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
        <Text style={styles.txtMessages}>Messages</Text>
        <TouchableOpacity
          onPress={() => alert('Coming Soon')}
          style={styles.searchCon}></TouchableOpacity>
      </Header>
      <View style={{ padding: 10 }}>
        <Item rounded>
          <Icon active name="search" />
          <Input style={{ fontSize: 14 }} placeholder="Search" onChangeText={(val) => handleSearch(val)} />
        </Item>
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={getNewMessages} />
        }>
        <FlatList
          data={searched.length > 0 ? searched : conversations}
          renderItem={renderContact}
          listKey={(index) => `conversation${index}-${Date.now()}`}
          ListEmptyComponent={noMessages}
          extraData={searched.length > 0 ? searched : conversations}
        />
      </ScrollView>
    </View>
  );
}
