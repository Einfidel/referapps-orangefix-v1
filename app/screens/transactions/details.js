import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Alert,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native';
import { Container, Header, Thumbnail, Content, Right, Left, Spinner, Card, CardItem, Body, Input } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../components/assets.manager';
import Icons from '../../components/icons';
import Service from '../../components/api/service';
import Endpoints from '../../components/api/index';
import { Picker } from '@react-native-picker/picker';
const { width, height } = Dimensions.get('window');


export default function History({ navigation, route }) {

  const [transactions, settransactions] = useState({})
  const [billing, setbilling] = useState({})
  const [shipping, setshipping] = useState({})
  const [orders, setorders] = useState([])
  const [stores, setstores] = useState([])
  const [statusText, setStatusText] = useState('')
  const [user, setUser] = useState('')
  const [loading, setLoading] = useState(false)
  const [trackingNumberModalVisible, setTrackingNumberModalVisible] = useState(false)
  const [lbcModal, setlbcModal] = useState(false)
  const [rcdModal, setrcdModal] = useState(false)
  const [courier, setCourier] = useState('')
  const [trackingNumber, setTrackingNumber] = useState('')
  const [streetAddress, setStreetAddress] = useState('')
  const [province, setProvince] = useState([])
  const [city, setCity] = useState([])
  const [barangay, setBarangay] = useState([])
  const [selectedProvince, setSelectedProvince] = useState('')
  const [selectedCity, setSelectedCity] = useState('')
  const [selectedBarangay, setSelectedBarangay] = useState('')
  const [shipping_options, setShippingOptions] = useState([]);
  const [selectedItem, setSelectedItem] = useState({});
  const [submitTapped, setSubmitTapped] = useState(false);

  useEffect(() => {
    try {
      init()
    } catch (err) {
      console.log(err)
    }
  }, [])

  const init = async () => {
    setLoading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(user);

    try {
      let data = {};
      data.user_id = route.params.user.user_id;
      data.order_id = route.params.id;
      await Service.transactionDetails(
        data,
        (response) => {
          if (response.length > 0) {
            settransactions(response)
            let st = []
            let stf = []
            let or = route.params.orders
            let of = []
            for (var x = 0; x < response.length; x++) {
              st.push(response[x].info.data.seller_info)
            }
            stf = getUniqueListBy(st, 'seller_id')
            // console.log(stf)
            for (var x = 0; x < stf.length; x++) {
              let orders = or.filter((val) => { return val.seller_name == stf[x].seller_name })
              of.push({ items: orders, store: stf[x] })
            }
            setorders(of)
          }
        },
        (err) => {
          console.warn('Error', err.message)
        },

      );
    } catch (error) {
      // alert('transactionReceive error', error);
      console.log('transactionReceive error', error)
    }

    let Province = await Service.getAllProvince();
    if (Province) {
      setProvince(Province.data)
    }

    // handleGetCity(Province.data[0])

    await Service.getShippingOptions(
      (res) => {
        setShippingOptions(res.data);
        // console.log('shipping_options', shipping_options)
      },
      (err) => console.log(err)
    );

    setbilling(route.params.billing)
    setshipping(route.params.shipping)
    setLoading(false)
    // console.log("route.params.orders", route.params.orders)
    // console.log("ORDERS ", JSON.stringify(orders))
  }

  const handleGetCity = async (prov) => {
    let City = await Service.getAllCity(prov);
    if (City) {
      setCity(City.data)
    }

    if (barangay.length == 0) {
      console.log("City.data[0]", City.data[0])
      handleGetBarangay(City.data[0])
    }
  }

  const handleGetBarangay = async (city) => {
    let Barangay = await Service.getAllBarangay(city);
    if (Barangay) {
      setBarangay(Barangay.data)
    }
  }

  const renderBtn = (item) => {
    if (route.params.user.user_id !== user.data.id) {
      return (
        <View>
          {
            (item.status === 'pending') ?
              // <CardItem style={{ flexDirection: 'row', borderWidth: 2, borderColor: '#bababa' }}>
              //   <View style={{ flex: 1, alignItems: 'center', borderWidth: 2, borderColor: '#bababa' }}>
              <CardItem style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <TouchableOpacity
                    nativeOpacity={0.4}
                    onPress={() => {
                      setSelectedItem(item);
                      setSubmitTapped(false);
                      setTrackingNumberModalVisible(true);
                    }}
                    style={styles.receiveButton}>
                    <Text style={styles.receiveText}>
                      Ship Item
                    </Text>
                  </TouchableOpacity>
                </View>
              </CardItem>
              : null
          }
        </View>
      )
    } else {
      return (
        <View>
          {
            // (item.status === 'pending' || item.status === 'shipped' || item.status === 'received') ?
            (item.status === 'shipped' || item.status === 'received') ?
              // <CardItem style={{ flexDirection: 'row', borderWidth: 2, borderColor: '#bababa' }}>
              //   <View style={{ flex: 1, alignItems: 'center', borderWidth: 2, borderColor: '#bababa' }}>
              <CardItem style={{ flexDirection: 'row' }}>
                <View style={{ flex: 1, alignItems: 'center' }}>
                  <TouchableOpacity
                    nativeOpacity={0.4}
                    onPress={() => {
                      handleBuyerSubmit(item)
                    }}
                    style={styles.receiveButton}>
                    <Text style={styles.receiveText}>
                      {item.status === 'pending' ? "Cancel Item" : item.status === 'shipped' ? "Confirm Delivery" : "Return Item"}
                    </Text>
                  </TouchableOpacity>
                </View>
              </CardItem>
              : null
          }
        </View>
      )
    }
  }

  const handleBuyerSubmit = async (item) => {
    let data = {};

    // console.log("item", item)

    switch (item.status) {
      case 'pending':
        // navigation.navigate('transactionCancel');

        Alert.alert(
          "Coming Soon",
          "Currently in development!",
          [
            {
              text: "Dismiss",
              style: "cancel",
            },
          ],
          {
            cancelable: true,
          }
        );
        break;
      case 'shipped':
        Alert.alert(
          "Warning",
          "Confirming your item's delivery cannot be undone.",
          [
            {
              text: "Confirm Delivery",
              onPress: async () => {
                try {
                  data.order_id = item.id;
                  data.product_id = item.product_id;
                  await Service.transactionReceive(
                    data,
                    (res) => {
                      Alert.alert(
                        "Success",
                        "Item has been successfully received",
                        [
                          {
                            text: "Dismiss",
                            style: "cancel",
                          },
                        ],
                        {
                          cancelable: true,
                          // onDismiss: () =>
                          // Alert.alert(
                          //   "This alert was dismissed by tapping outside of the alert dialog."
                          // ),
                        }
                      );
                      console.log('transactionReceive success', res)
                      navigation.goBack();
                    },
                    (err) => {
                      console.warn('Error', err.message)
                    },

                  );
                } catch (error) {
                  alert('transactionReceive error', error);
                  console.log('transactionReceive error', error)
                }
              }
            },
            {
              text: "Exit",
              style: "cancel",
            },
          ],
          {
            cancelable: true,
          }
        )
        break;
      case 'received':
        // navigation.navigate('transactionReturn');

        Alert.alert(
          "Coming Soon",
          "Currently in development!",
          [
            {
              text: "Dismiss",
              style: "cancel",
            },
          ],
          {
            cancelable: true,
          }
        );
        break;

      default:
        break;
    }
  }

  const getUniqueListBy = (arr, key) => {
    return [...new Map(arr.map(item => [item[key], item])).values()]
  }

  const handleStandardCourierSubmit = () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // If Courier is Empty
    if (courier == '' || courier == null) {
      submitCancelled = true;
    }
    // If Tracking Number is Empty
    if (trackingNumber == '' || trackingNumber == null) {
      submitCancelled = true;
    }

    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    Alert.alert(
      "Caution",
      "Please confirm that you have entered the right tracking number for this order",
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "Confirm and add Tracking Number",
          onPress: async () => {
            try {
              let data = {};
              data.user_id = user.data.id;
              data.order_id = selectedItem.id;
              data.courier = courier;
              data.tracking_number = trackingNumber;
              await Service.updateToShip(
                data,
                (res) => {
                  Alert.alert(
                    "Success",
                    "Item has been Successfully Shipped!",
                    [
                      {
                        text: "Dismiss",
                        style: "cancel",
                        onPress: () => {
                          navigation.goBack();
                        },
                      },
                    ],
                  );
                },
                (err) => {
                  console.warn('updateToShip 2', err.message)
                },

              );
            } catch (error) {
              alert('updateToShip error 1', error);
              console.log('updateToShip error 1', error)
            }
            // MobileTestTrackingNum

            // navigation.navigate("")
            setTrackingNumberModalVisible(false);
          }
        },
      ],
      {
        cancelable: true,
      }
    );
    return null
  };

  const handleLBCSubmit = () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // If Courier is Empty
    if (streetAddress == '' || streetAddress == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedProvince == '' || selectedProvince == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedCity == '' || selectedCity == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedBarangay == '' || selectedBarangay == null) {
      submitCancelled = true;
    }

    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    Alert.alert(
      "Caution",
      "Please confirm that you have entered the address for this order",
      [
        {
          text: "Confirm",
          onPress: async () => {
            try {
              let data = {};
              data.user_id = user.data.id;
              data.order_id = selectedItem.id;
              data.street_address = streetAddress;
              data.barangay = selectedBarangay;
              data.municipality = selectedCity;
              data.province = selectedProvince;
              await Service.updateToShipLBC(
                data,
                (res) => {
                  console.log("updateToShipLBC", res)
                  Alert.alert(
                    "Success",
                    res.msg + "\n\n" + res.data.info.data.order_info.reference_number,
                    [
                      {
                        text: "Dismiss",
                        style: "cancel",
                        onPress: () => {
                          navigation.goBack();
                        },
                      },
                    ],
                  );
                },
                (err) => {
                  console.warn('updateToShip 2', err.message)
                },

              );
            } catch (error) {
              alert('updateToShip error 1', error);
              console.log('updateToShip error 1', error)
            }
            // MobileTestTrackingNum

            // navigation.navigate("")
            setTrackingNumberModalVisible(false);
          }
        },
        {
          text: "Cancel",
          style: "cancel",
        },
      ],
      {
        cancelable: true,
      }
    );
    return null
  };

  const handleRCDSubmit = () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // If Courier is Empty
    if (streetAddress == '' || streetAddress == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedProvince == '' || selectedProvince == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedCity == '' || selectedCity == null) {
      submitCancelled = true;
    }

    // If Courier is Empty
    if (selectedBarangay == '' || selectedBarangay == null) {
      submitCancelled = true;
    }

    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    Alert.alert(
      "Caution",
      "Please confirm that you have entered the address for this order",
      [
        {
          text: "Confirm",
          onPress: async () => {
            try {
              let data = {};
              data.user_id = user.data.id;
              data.order_id = selectedItem.id;
              data.street_address = streetAddress;
              data.barangay = selectedBarangay;
              data.municipality = selectedCity;
              data.province = selectedProvince;
              await Service.updateToShipRCD(
                data,
                (res) => {
                  console.log("updateToShipRCD", res)
                  Alert.alert(
                    "Success",
                    res.msg + "\n\n" + res.data.info.data.order_info.reference_number,
                    [
                      {
                        text: "Dismiss",
                        style: "cancel",
                        onPress: () => {
                          navigation.goBack();
                        },
                      },
                    ],
                  );
                },
                (err) => {
                  console.warn('updateToShip 2', err.message)
                },

              );
            } catch (error) {
              alert('updateToShip error 1', error);
              console.log('updateToShip error 1', error)
            }
            // MobileTestTrackingNum

            // navigation.navigate("")
            setTrackingNumberModalVisible(false);
          }
        },
        {
          text: "Cancel",
          style: "cancel",
        },
      ],
      {
        cancelable: true,
      }
    );
    return null
  };

  const renderTrackingNumberModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={trackingNumberModalVisible} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setTrackingNumberModalVisible(!trackingNumberModalVisible);
            }}
          >
            <View style={styles.containerModal}>
              <TouchableWithoutFeedback
                onPress={() => {
                  // setTrackingNumberModalVisible(!trackingNumberModalVisible);
                }}
              >
                <View style={styles.ReferrerModalContainer}>
                  <Text style={styles.ReferrerModalHeaderText}>
                    Ship with your preferred courier
                  </Text>

                  {renderCourierError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    Courier
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={courier}
                      mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(value) => {
                        setCourier(value);
                      }}
                    >
                      {/* <Picker.Item label='Option 1' value={'option1'} />
                      <Picker.Item label='Option 2' value={'option2'} /> */}
                      <Picker.Item label={'Select a Courier'} value={''} />
                      {shipping_options.map((item, i) => {
                        return (
                          <Picker.Item label={item.name} value={item.name} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  {renderTrackingNumberError()}
                  <Text style={styles.ReferrerModalOptionText2}>
                    Tracking Number
                  </Text>

                  <View style={styles.inputContainer}>
                    <Input
                      style={styles.group_input_text}
                      placeholder={'Tracking Number'}
                      placeholderTextColor={'#7f8c8d'}
                      onChangeText={(newTrackingNumber) => {
                        setTrackingNumber(newTrackingNumber);
                      }}
                      value={trackingNumber}
                    />
                  </View>

                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      handleStandardCourierSubmit();
                      // renderDiscountedPriceSaveBtn()
                    }}
                    style={styles.referrerSaveBtn}
                  >
                    <Text style={styles.SaveText}>Ship Item</Text>
                  </TouchableOpacity>

                  <View style={{ borderTopWidth: 2, borderColor: '#ddd', marginVertical: 10, marginBottom: 15, }}>
                  </View>

                  <Text style={styles.ReferrerModalHeaderText}>
                    Or ship with our partner courier
                  </Text>

                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      setTrackingNumberModalVisible(!trackingNumberModalVisible);
                      setlbcModal(!lbcModal);
                      // renderDiscountedPriceSaveBtn()
                    }}
                    style={styles.lbcBtn}
                  >
                    <Text style={styles.LBCSaveText}>LBC</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      setTrackingNumberModalVisible(!trackingNumberModalVisible);
                      setrcdModal(!rcdModal);
                      // renderDiscountedPriceSaveBtn()
                    }}
                    style={styles.rcdBtn}
                  >
                    <Text style={styles.LBCSaveText}>RCD</Text>
                  </TouchableOpacity>

                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderLBCModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={lbcModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setlbcModal(!lbcModal);
            }}
          >
            <View style={styles.containerModal}>
              <TouchableWithoutFeedback
                onPress={() => {
                  // setlbcModal(!lbcModal);
                }}
              >
                <View style={styles.ReferrerModalContainer}>
                  <Text style={styles.ReferrerModalHeaderText}>
                    Ship with LBC
                  </Text>

                  {renderStreetAddressError()}
                  <Text style={styles.ReferrerModalOptionText2}>
                    House Number, Building and Street Name
                  </Text>

                  <View style={styles.inputContainer}>
                    <Input
                      style={styles.group_input_text}
                      placeholder={'House #, Bldg and St. Name'}
                      placeholderTextColor={'#7f8c8d'}
                      onChangeText={(newStreetAddress) => {
                        setStreetAddress(newStreetAddress);
                      }}
                      value={streetAddress}
                    />
                  </View>

                  {renderProvinceError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    Province
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedProvince}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newProvince) => {
                        setSelectedProvince(newProvince);
                        handleGetCity(newProvince)
                      }}
                    >
                      {province.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  {renderCityError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    City
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedCity}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newCity) => {
                        setSelectedCity(newCity);
                        handleGetBarangay(newCity)
                      }}
                    >
                      {city.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  {renderBarangayError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    Barangay
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedBarangay}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newBarangay) => {
                        setSelectedBarangay(newBarangay);
                      }}
                    >
                      {barangay.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      handleLBCSubmit();
                      setlbcModal(!lbcModal);
                      // renderDiscountedPriceSaveBtn()
                    }}
                    style={styles.referrerSaveBtn}
                  >
                    <Text style={styles.SaveText}>Ship Item</Text>
                  </TouchableOpacity>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderRCDModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={rcdModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              setrcdModal(!rcdModal);
            }}
          >
            <View style={styles.containerModal}>
              <TouchableWithoutFeedback
                onPress={() => {
                  // setrcdModal(!rcdModal);
                }}
              >
                <View style={styles.ReferrerModalContainer}>
                  <Text style={styles.ReferrerModalHeaderText}>
                    Ship with RCD
                  </Text>

                  {renderStreetAddressError()}
                  <Text style={styles.ReferrerModalOptionText2}>
                    House Number, Building and Street Name
                  </Text>

                  <View style={styles.inputContainer}>
                    <Input
                      style={styles.group_input_text}
                      placeholder={'House #, Bldg and St. Name'}
                      placeholderTextColor={'#7f8c8d'}
                      onChangeText={(newStreetAddress) => {
                        setStreetAddress(newStreetAddress);
                      }}
                      value={streetAddress}
                    />
                  </View>

                  {renderProvinceError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    Province
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedProvince}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newProvince) => {
                        setSelectedProvince(newProvince);
                        handleGetCity(newProvince)
                      }}
                    >
                      {province.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  {renderCityError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    City
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedCity}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newCity) => {
                        setSelectedCity(newCity);
                        handleGetBarangay(newCity)
                      }}
                    >
                      {city.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  {renderBarangayError()}
                  <Text style={styles.ReferrerModalOptionText}>
                    Barangay
                  </Text>

                  <View style={{ fontSize: 12, }}>
                    <Picker
                      selectedValue={selectedBarangay}
                      // mode='dropdown'
                      style={styles.datePicker}
                      itemStyle={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#231f20',
                      }}
                      onValueChange={(newBarangay) => {
                        setSelectedBarangay(newBarangay);
                      }}
                    >
                      {barangay.map((item, i) => {
                        return (
                          <Picker.Item label={item} value={item} key={i} />
                        )
                      })}
                    </Picker>
                  </View>

                  <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                      handleRCDSubmit();
                      setrcdModal(!rcdModal);
                      // renderDiscountedPriceSaveBtn()
                    }}
                    style={styles.referrerSaveBtn}
                  >
                    <Text style={styles.SaveText}>Ship Item</Text>
                  </TouchableOpacity>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderCourierError = () => {
    if (submitTapped) {
      if (courier == '' || courier == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Courier is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderTrackingNumberError = () => {
    if (submitTapped) {
      if (trackingNumber == '' || trackingNumber == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Tracking Number is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderStreetAddressError = () => {
    if (submitTapped) {
      if (streetAddress == '' || streetAddress == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * StreetAddress is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderProvinceError = () => {
    if (submitTapped) {
      if (selectedProvince == '' || selectedProvince == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Province is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderCityError = () => {
    if (submitTapped) {
      if (selectedCity == '' || selectedCity == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * City is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderBarangayError = () => {
    if (submitTapped) {
      if (selectedBarangay == '' || selectedBarangay == null) {
        return (
          <View style={styles.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Barangay is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  return (
    <Container style={{ backgroundColor: '#FFF' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <Body style={styles.left}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.headerText}>Order Details</Text>
          </View>
        </Body>
      </Header>
      {loading ? <Spinner /> : null}
      <Content padder style={{ backgroundColor: '#F5F5F5' }}>

        {renderTrackingNumberModal()}
        {renderLBCModal()}
        {renderRCDModal()}


        <Card transparent>
          <CardItem>
            <Body>
              {/* <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold' }}>
                Status:
                <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', textTransform: 'capitalize' }}>
                  {" "}{orders[0]?.items[0]?.status}
                </Text>
              </Text> */}
              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold' }}>Order ID: {route.params.id}</Text>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>Placed on {route.params.date.date_db}</Text>
            </Body>
          </CardItem>
          <CardItem footer style={{ flexDirection: 'row-reverse' }}>
            <Text style={{ fontSize: 22, fontWeight: 'bold', color: '#363f4d' }}>Total: {route.params.total.toFixed(2)}</Text>
          </CardItem>
        </Card>

        {orders.map((order, i) => {
          // console.log("order", order)

          return (
            <Card transparent key={i}>
              <CardItem header>
                <Left><Text>Sold by {order.store.seller_username}</Text></Left>
                {/* <Right style={{ flexDirection: 'row-reverse' }}><Text style={{ color: '#363f4d', fontSize: 18 }}>Chat Now</Text><Icons.EvilIcons name="comment" size={35} /></Right> */}
              </CardItem>
              {order.items.map((item, j) => {
                // console.log("item", item)
                // return null
                return (
                  <View key={j}>
                    <View style={{ marginHorizontal: 10, borderBottomWidth: 1, borderBottomColor: '#aaa' }} />
                    <CardItem style={{ marginBottom: 0, paddingBottom: 0 }}>
                      <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', }}>
                          Status:
                          <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', textTransform: 'capitalize' }}>
                            {" "}{item.status === 'for_processing' ? 'For Processing' : item.status}
                          </Text>
                        </Text>
                      </View>
                    </CardItem>

                    {item.status === 'for_processing' ?
                      <CardItem style={{ marginBottom: 0, paddingBottom: 0 }}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                          <Text style={{ fontSize: 16, color: '#363f4d', }}>
                            Reference ID:
                            <Text style={{ fontSize: 16, color: '#363f4d', }}>
                              {" "}{item.reference_number}
                            </Text>
                          </Text>
                        </View>
                      </CardItem>
                      : null}
                    <CardItem>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 2, alignItems: 'center' }}>
                          <Thumbnail square source={{ uri: item.product_image }} />
                        </View>
                        <View style={{ flex: 4 }}>
                          <Text>{item.description}</Text>
                        </View>
                      </View>
                    </CardItem>
                    <CardItem>
                      <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                          <Text>{'\u20B1'} {item.price.toFixed(2)}</Text>
                        </View>
                        <View style={{ flex: 1, alignItems: 'center' }}>
                          <Text>Qty: {item.qty}</Text>
                        </View>
                      </View>
                    </CardItem>
                    {renderBtn(item)}
                  </View>
                )
              })}
              {/* <CardItem footer>
              </CardItem> */}
            </Card>
          )
        })}

        <Card transparent>
          <CardItem header>
            <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold' }}>Description</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>{route.params.description}</Text>
            </Body>
          </CardItem>
          <CardItem footer style={{ flexDirection: 'row-reverse' }}></CardItem>
        </Card>

        <Card transparent>
          <CardItem header>
            <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold' }}>Billing</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>Ship to</Text>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#363f4d' }}>{shipping.shipping_contact_person}</Text>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>{shipping.shipping_address}</Text>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>{shipping.shipping_contact_number}</Text>
            </Body>
          </CardItem>
          <CardItem>
            <Body>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>Bill to</Text>
              <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#363f4d' }}>{billing.billing_contact_person}</Text>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>{billing.billing_address}</Text>
              <Text style={{ fontSize: 16, color: '#7A7A7A' }}>{billing.billing_contact_number}</Text>
            </Body>
          </CardItem>
          <CardItem footer style={{ flexDirection: 'row-reverse' }}></CardItem>
        </Card>

      </Content>
    </Container >
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  receiveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto', 
    fontSize: 18,
    color: '#ffffff',
    // padding: 5,
  },
  receiveButton: {
    justifyContent: 'space-around',
    alignSelf: 'center',
    // width: '50%',
    backgroundColor: '#e67300',
    // height: 30,
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    // paddingTop: 10,
    // paddingBottom: 5,
  },
  modalCon: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  containerModal: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  ReferrerModalContainer: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    // width: wp('90%'),
    borderRadius: 5,
    justifyContent: 'space-around',
    paddingVertical: 20,
    paddingHorizontal: 30,
    // paddingLeft: 20,
    // paddingRight: 20,
    // paddingTop: 20,
    // paddingBottom: 20,
    // height: height / 2,
    // paddingHorizontal: 50
  },
  ReferrerModalHeaderText: {
    // fontFamily: 'Roboto',
    fontSize: 18,
    textAlign: "left",
    color: '#e67300',
    paddingBottom: 5,
    // paddingTop: hp('2%'),
  },
  ReferrerModalOptionText: {
    // fontFamily: 'Roboto',
    // fontSize: hp('1.50%'),
    fontSize: 14,
    textAlign: "left",
    color: '#444',
    paddingTop: 5,
  },
  ReferrerModalOptionText2: {
    // fontFamily: 'Roboto',
    // fontSize: hp('1.50%'),
    fontSize: 14,
    textAlign: "left",
    color: '#444',
    paddingTop: 5,
  },
  referrerSaveBtn: {
    backgroundColor: '#00a14b',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  lbcBtn: {
    backgroundColor: '#F54040',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: hp('2%'),
    marginBottom: hp('1.50%'),
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  rcdBtn: {
    backgroundColor: '#F66206',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderRadius: 15,
    justifyContent: 'center',
    // marginTop: hp('2%'),
    marginBottom: hp('1.50%'),
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  SaveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#ffffff',
  },
  LBCSaveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#ffffff',
    fontWeight: "bold",
  },
  group_input_text: {
    maxHeight: hp('8  %'),
    textAlign: 'center',
    fontSize: 16,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ddd',
    marginTop: 10,
    // fontFamily: 'Roboto',
    // color: '#7f8c8d',
    // marginRight: 30,
  },
  inputContainer: {
    // width: width,
    // height: 34,
    // flex: 1,
    paddingTop: 0,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  datePicker: {
    color: '#7f8c8d',
    // width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {
        transform: [
          { scaleX: 0.9 },
          { scaleY: 0.9 }
        ]
      },
    })
  },
  fieldErrors: {
    padding: 10,
    // paddingLeft: 10,
    paddingBottom: 5,
    paddingTop: 13,
  },
});
