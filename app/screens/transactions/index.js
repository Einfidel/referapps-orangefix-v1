import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { Container, Header, Thumbnail, Content, Body, Right, Spinner } from 'native-base';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../components/assets.manager';
import Icons from '../../components/icons';
import Endpoints from '../../components/api/index';
import AsyncStorage from '@react-native-community/async-storage';
const { width, height } = Dimensions.get('window');

// depreciated file
// use profile/orders/purchasehistory.js 

const Transaction = (props) => {
  let dt = props.data.date.data.added_in
  return (
    <View style={{ marginBottom: 15 }}>
      <View style={{ width: '100%', padding: 15, borderWidth: 0, borderRadius: 5, borderBottomWidth: 0, backgroundColor: '#66B548' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Order ID: {props.data.order_id}</Text>
        <Text style={{ color: '#fff' }}>{`${dt.month_year} @ ${dt.time_passed}`}</Text>
      </View>
      <View style={{ width: '100%', padding: 15, borderWidth: 0.2, flexDirection: 'row', backgroundColor: '#fff', borderBottomStartRadius: 5, borderBottomEndRadius: 5 }}>
        <View style={{ flex: 4, padding: 15 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{props.data.payment_type.toUpperCase()}</Text>
          <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }}>STATUS: {props.data.payment_status.toUpperCase()}</Text>
        </View>
        <View style={{ flex: 8, padding: 15 }}>
          <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }} numberOfLines={3}>{props.data.description}</Text>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Total: {props.data.total_amount.toFixed(2)}</Text>
        </View>
        <TouchableOpacity onPress={() => props.navigator.navigate("TransactionDetails", {
          id: props.data.order_id,
          shipping: props.data.info.data.shipping_info,
          billing: props.data.info.data.billing_info,
          orders: props.data.order_list,
          user: props.data.info.data.user_info,
          date: dt,
          total: props.data.total_amount,
          description: props.data.description,
          status: props.data.order_list.status,
        })} style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
          <Icons.EvilIcons name="chevron-right" size={45} color="#66B548" />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default function History(props) {

  const [transactions, settransactions] = useState([])
  const [loading, setLoading] = useState(false)

  const init = async () => {
    setLoading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    let formData = new FormData()
    formData.append("api_token", 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=')
    formData.append("user_id", user.data.id)
    console.log("BODY", formData, user)
    let get = await fetch("https://www.referapps.com/api/account/transaction/index.json?include=date,info", {
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData
    })
    let response = await get.json()
    if (response) settransactions(response.data)
    console.log("TRANSACTIONS ", response)
    setLoading(false)
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <Container style={{ backgroundColor: '#FFF' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <Body style={styles.left}>
          <TouchableOpacity onPress={() => props.navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.headerText}>Transactions</Text>
          </View>
        </Body>
      </Header>
      <Content>
        {loading ? <Spinner /> : null}
        <View style={{ flex: 1, padding: 15 }}>

          {transactions.map((data, i) => {
            return <Transaction key={i} data={data} navigator={props.navigation} />
          })}

        </View>

      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
});
