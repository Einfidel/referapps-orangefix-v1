import React, { useState, useEffect } from 'react';
import {
  View,
  Image,
  Dimensions,
  FlatList,
  ScrollView,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import {
  ListItem,
  Body,
  Thumbnail,
  Text,
} from 'native-base';
import { IGNORED_TAGS, alterNode, makeTableRenderer, defaultTableStylesSpecs, cssRulesFromSpecs } from 'react-native-render-html-table-bridge';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import CustomHeader from '../../components/header';
import HTML from 'react-native-render-html';
import { useFocusEffect } from '@react-navigation/native';
import FastImage from 'react-native-fast-image'

const { width, height } = Dimensions.get('window');


const cssRules = `
table {
  width: 100%;
  background-color: #FFF;
}
`

const htmlConfig = {
  alterNode,
  // renderers,
  ignoredTags: IGNORED_TAGS,
  cssRules
};

/*
Notification References:
order
product
service
check_request

Notification Type:
new_order
cancel_order
new_service
suspend_product
return_order
declined_check_request
*/

export default function HomeScreen({ navigation, route }) {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState('');
  const [notifications, setNotifications] = useState([]);

  const getChatsNotifications = async (callBack) => {
    setLoading(true);
    let chats = await AsyncStorage.getItem('chat');
    chats && callBack(chats);
  };


  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      setLoading(true);

      async function reload() {
        init().then(() => setLoading(false));
      };

      reload();

      return () => {
        isActive = false;
      };
    }, [])
  );

  const init = async () => {
    try {
      let ud = await AsyncStorage.getItem('user_data');
      let _userdata = ud ? JSON.parse(ud) : null;
      // console.log("_userdata.data.id", _userdata.data.id)
      setUser(_userdata);
      let data = {};
      data.user_id = _userdata.data.id;
      await Service.getNotificationsList(data,
        (res) => {
          // console.log("getNotificationsList", res)
          setNotifications(res.data);
          // console.log("item", res.data[0])
        },
        (err) => {
          console.log("Service.getNotificationsList error", err)
        }
      );

      // getChatsNotifications((chats) => {
      //   setNotifications(JSON.parse(chats));
      // }).then(() => setLoading(false));
    } catch (error) {
      console.warn('Error', error.message);
    }
  }

  const renderNotifications = ({ item }) => {
    let { description, type, reference, is_viewed, reference_id, id } = item;
    let date = item.date.data.added_in.timestamp.date;

    if (date == null || date == '') {
      // date = item.date.data.added_in.date_db + ' ' + item.date.data.added_in.time_passed
      // date = moment(date, 'YYYY-MM-DD hh:mm a')
      date = item.date.data.added_in.time_passed
      date = moment(date, 'DD/MMM at hh:mm a').set('year', item.date.data.added_in.month_year.split(' ')[1]);
    }

    let notifSource = null;
    // console.log("type", type);

    switch (type) {
      case "approved_check_request":
        notifSource = Assets.notifications.approved_check_request
        break;
      case "cancel_order":
        notifSource = Assets.notifications.cancel_order
        break;
      case "declined_check_request":
        notifSource = Assets.notifications.declined_check_request
        break;
      case "new_order":
        notifSource = Assets.notifications.new_order
        break;
      case "new_service":
        notifSource = Assets.notifications.new_service
        break;
      case "refund_service":
        notifSource = Assets.notifications.refund_service
        break;
      case "return_order":
        notifSource = Assets.notifications.return_order
        break;
      case "suspend_product":
        notifSource = Assets.notifications.suspend_product
        break;

      default:
        break;
    }

    let backgroundColorString = '';
    if (is_viewed != "yes") {
      backgroundColorString = "#D7F2FF"
    } else {
      backgroundColorString = "#F0F0F0"
    }
    // console.log("Assets.notifications.new_order", Assets.notifications.new_order);
    // console.log("notifSource", notifSource);
    return (
      <ListItem
        avatar
        noIndent
        style={{ backgroundColor: backgroundColorString }}>
        <TouchableOpacity onPress={() => {
          console.log("handleRedirect 1")
          console.log("item 1", item)
          handleRedirect(type, reference_id, id, is_viewed);
        }}>
          <FastImage
            style={{
              // tintColor: '#aaa'
              width: 51,
              height: 51,
            }}
            // small
            // square
            source={
              // messageType === 'chat'
              //   ? Assets.myProfile.chatIcon
              //   : Assets.notifications.notification
              notifSource
            }
          />
        </TouchableOpacity>
        <Body>
          <TouchableOpacity onPress={() => {
            console.log("handleRedirect 2")
            // console.log("item 2", item)
            console.log("item", item)
            handleRedirect(type, reference_id, id, is_viewed);
          }}>
            <HTML html={description} {...htmlConfig} />
            {/* <Text note>{item.description}</Text> */}
            <Text note style={{ marginTop: 4 }}>
              {new moment(date).fromNow()}
              {/* {new moment(date).format('YYYY-MM-DD hh:mm a')} */}
            </Text>
          </TouchableOpacity>
        </Body>
      </ListItem >

    );
  };

  const handleRedirect = async (type, reference_id, notifID, is_viewed) => {
    setLoading(true)
    let data = {};
    let data2 = {};
    switch (type) {
      case "approved_check_request":
        data.user_id = user.data.id;

        await Service.getCheckRequestList(
          data,
          async (res2) => {
            // console.log("setLoading(false)")
            if (res2 != null && res2 != '') {
              if (res2?.data?.length > 0) {
                console.log("res2.data.length", res2.data.length)
                for (let index = 0; index < res2.data.length; index++) {
                  // console.log("index", index)
                  if (res2.data[index].id == reference_id) {
                    data2.id = notifID;
                    data2.user_id = user.data.id;

                    if (is_viewed != "yes") {
                      await Service.viewNotification(
                        data2,
                        (res2) => {
                          console.log("Service.viewNotification: notif is now viewed")
                        },
                        (err) => {
                          console.log("Service.viewNotification error", err)
                        }
                      );
                    }

                    console.log("CheckRequestDetails", res2.data[index])
                    navigation.navigate("CheckRequestDetails", { checkRequest: res2.data[index] })
                    return
                  }
                }
              }
            }

            setLoading(false);
          },
          (err) => {
            setLoading(false);
            console.log("Service.getCheckRequestList error", err)
          });


        setLoading(false)
        break;
      case "declined_check_request":
        data.user_id = user.data.id;

        await Service.getCheckRequestList(
          data,
          async (res2) => {
            // console.log("setLoading(false)")
            if (res2 != null && res2 != '') {
              if (res2?.data?.length > 0) {
                console.log("res2.data.length", res2.data.length)
                for (let index = 0; index < res2.data.length; index++) {
                  // console.log("index", index)
                  if (res2.data[index].id == reference_id) {
                    data2.id = notifID;
                    data2.user_id = user.data.id;

                    if (is_viewed != "yes") {
                      await Service.viewNotification(
                        data2,
                        (res2) => {
                          console.log("Service.viewNotification: notif is now viewed")
                        },
                        (err) => {
                          console.log("Service.viewNotification error", err)
                        }
                      );
                    }

                    console.log("CheckRequestDetails", res2.data[index])
                    navigation.navigate("CheckRequestDetails", { checkRequest: res2.data[index] })
                    return
                  }
                }
              }
            }

            setLoading(false);
          },
          (err) => {
            setLoading(false);
            console.log("Service.getCheckRequestList error", err)
          });


        setLoading(false)
        break;
      case "new_order":
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = user.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "new_service":
        // console.log("reference_id", reference_id)
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = user.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "cancel_order":
        // console.log("reference_id", reference_id)
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = user.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "refund_service":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        setLoading(false)
        break;
      case "return_order":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        setLoading(false)
        break;
      case "suspend_product":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        setLoading(false)
        break;

      default:

        setLoading(false)
        break;
    }
  }

  const renderEmptyNotifications = () => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: height / 2
            // marginTop: width / 1.5,
          }}>
          <FastImage
            source={Assets.notifications.notification}
            style={{
              width: 120,
              height: 120,
              alignSelf: 'center',
              resizeMode: 'contain',
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
          <View style={{ paddingVertical: 5 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: '#231f20' }}>
              No Notification
            </Text>
          </View>
          <View style={styles.notifContent}>
            <Text style={styles.notifContentText}>
              {' '}
              In this technological age, let us help one another to profit from
              every sale or service brought about by you using our ReferApps
              tool. It's your time! It's time for a ReferApps ecolution!
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <CustomHeader title="Notifications" navigation={navigation} />
      <ScrollView
        contentContainerStyle={{}}
        style={{ height: height / 1.2 }}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() =>
              init().then(() => setLoading(false))
            }
          />
        }>
        <FlatList
          data={notifications}
          renderItem={renderNotifications}
          listKey={(index) => `notification${index}-${Date.now()}`}
          ListEmptyComponent={renderEmptyNotifications}
          extraData={notifications}
        />
      </ScrollView>
    </View>
  );
}

const styles = {
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 20,
    color: '#231f20',
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  notifContainer: {
    justifyContent: 'space-around',
    paddingTop: Dimensions.get('window').height / 2 - 170,
  },
  notifContentText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    marginBottom: 20,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  notifContent: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
};
