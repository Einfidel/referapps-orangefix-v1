import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
  Alert,
  SafeAreaView,
} from 'react-native';

import {
  Header,
  Left,
  Body,
  Right,
  Thumbnail,
  Container,
} from 'native-base';


const width = Dimensions.get('window').width;

export default class ManageShop extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }

  render() {
    return (
      // <SafeAreaView >
      <Container style={{
        backgroundColor: '#EDEDEB',
      }}>
        <Header
          transparent
          style={{
            backgroundColor: '#ffffff',
            paddingLeft: 10,
            paddingRight: 10,
            // marginBottom: -5,
          }}>
          <Left style={{ flex: 1, marginHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
            >
              <Thumbnail
                source={require('../../assets/internal/icon_back.png')}
                square
                style={{
                  resizeMode: 'contain',
                  width: 20,
                  height: 20,
                }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 20,
              color: '#231f20',
            }}>My Shop</Text>
          </Body>
          <Right style={{ flex: 1, marginHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            // onPress={() => this.props.navigation.navigate("Messages")}
            >
              <Thumbnail
                source={require('../../assets/internal/product/icon_chat.png')}
                square
                style={{
                  width: 22,
                  height: 22,
                  resizeMode: 'contain',
                  tintColor: '#00a14b',

                }}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <ScrollView>
          <TouchableOpacity
            style={{
              borderBottomWidth: .7,
              borderColor: '#ecf0f1',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}
            onPress={() =>
              // Alert.alert('Coming Soon!', 'This feature will be available soon.')}
              this.props.navigation.navigate('Your Products')}
          >
            <Thumbnail
              source={require('../../assets/internal/products.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Products</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              // Alert.alert('Coming Soon!', 'This feature will be available soon.')}
              this.props.navigation.navigate('Your Services')}
            style={{
              borderBottomWidth: .7,
              borderColor: '#ecf0f1',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}>
            <Thumbnail
              source={require('../../assets/internal/services.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Services</Text>
          </TouchableOpacity>

          <TouchableOpacity
            // onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            onPress={() => this.props.navigation.navigate('EditShop')}
            // this.props.navigation.navigate('ShopProfile')}
            style={{
              borderBottomWidth: .7,
              borderColor: '#ecf0f1',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}>
            <Thumbnail
              source={require('../../assets/internal/shop_profile.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Shop Profile</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            style={{
              borderBottomWidth: .7,
              borderColor: '#ecf0f1',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}>
            <Thumbnail
              source={require('../../assets/internal/referred_items.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Referred Items</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            style={{
              borderBottomWidth: .7,
              borderColor: '#ecf0f1',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}>
            <Thumbnail
              source={require('../../assets/internal/pending_services.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Pending Services</Text>
          </TouchableOpacity>

          {/* <TouchableOpacity
            onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            // this.props.navigation.navigate('Customers')}
            style={{
              borderBottomWidth: .7,
              // borderColor: '#ecf0f1',
              borderColor: '#ffffff',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
            backgroundColor: '#ffffff',
            }}
          >
            <Thumbnail
              source={require('../../assets/internal/customer.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Customer</Text>
          </TouchableOpacity> */}

          <TouchableOpacity
            onPress={() => {
              // this.props.navigation.navigate('CreateService')
              this.props.navigation.navigate('Add Product')
            }}
            // this.props.navigation.navigate('Customers')}
            style={{
              borderBottomWidth: .7,
              // borderColor: '#ecf0f1',
              borderColor: '#ffffff',
              width: width,
              flexDirection: 'row',
              paddingLeft: 10,
              paddingRight: 10,
              paddingTop: 13,
              paddingBottom: 13,
              backgroundColor: '#ffffff',
            }}
          >
            <Thumbnail
              source={require('../../assets/internal/icon_delivered.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
                paddingHorizontal: 20,
              }}
            />
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 12,
              color: '#231f20',
              alignSelf: 'center',
              paddingLeft: 10,
            }}>Sell New Products</Text>
          </TouchableOpacity>

        </ScrollView>
      </Container>
      // </SafeAreaView>
    );
  }
}
