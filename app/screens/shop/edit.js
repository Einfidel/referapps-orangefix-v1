import React from 'react';
import { useState, useEffect } from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  RefreshControl,
  TouchableOpacity,
  Alert,
  Button,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import Icons from '../../components/icons';
import { Header, Left, Body, Right, Thumbnail, Container } from 'native-base';

const { width, height } = Dimensions.get('window');

export default function EditShop(navigation) {
  const [loading, setloading] = useState(false);
  const [modalVisible, setmodalVisible] = useState(false);
  const [vmodalVisible, setvmodalVisible] = useState(false);
  const [userdata, setuserdata] = useState(null);
  const [cover, setcover] = useState(require('../../assets/internal/default-cover.jpg'));
  const [profile, setprofile] = useState(require('../../assets/face0.jpg'));

  const [name, setname] = useState('');
  const [overview, setoverview] = useState('');
  const [password, setpassword] = useState('gbHxKK14');
  const [code, setcode] = useState('');

  useEffect(() => {
    init();
  }, []);

  const init = async () => {
    setloading(true);
    let ud = await AsyncStorage.getItem('user_data');
    // let _userdata = ud.data;
    let _userdata = ud ? JSON.parse(ud) : null;
    // console.warn('Edit Shop ud', ud)
    // console.warn('Edit Shop unparsed ud - ', ud)
    // console.warn('Edit Shop parsed user data', _userdata)
    // console.warn('Edit Shop user ID', _userdata.data.id)

    try {
      await Service.userData(_userdata.data.id,
        (res) => {
          let userdata = res.data;

          // console.log('Edit Shop Service.userData', res)
          // console.log('Edit Shop Service.userData.data', userdata)
          // console.log('Edit Shop Service.userData.data.info', userdata.info)
          setuserdata(userdata)

          // console.log('Edit Shop userdata shop directory:', userdata.info.data.shop.directory)
          // console.log('Edit Shop userdata cover_photo directory:', userdata.info.data.cover_photo.full_path)
          // console.log('Edit Shop userdata shop full_path:', userdata.info.data.shop.full_path)
          if (res) {
            if (userdata.info.data.cover_photo.full_path) {
              setcover({ uri: userdata.info.data.cover_photo.full_path })
              console.log('{ uri: userdata.info.data.cover_photo.full_path }', { uri: userdata.info.data.cover_photo.full_path })
            }
            if (userdata.info.data.shop.directory) {
              setprofile({ uri: userdata.info.data.shop.full_path })
            }
            setname(userdata.shop_name)
            setoverview(userdata.info.data.description);
            setloading(false)
          }
          else {
            setloading(false)
          }
        },
        (err) => console.log(err)
      );
    } catch (error) {
      console.log("Service.userData error", error)
    }
  };

  const chooseFile = (dist) => {
    var options = {
      title: 'Select Image',
      customButtons: [{ name: 'customOptionKey', title: 'Choose Photo from Custom Option' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.warn('res', response)
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }
        response.data = null;
        if (dist == 'profile') {
          setprofile(response);
        } else if (dist == 'cover') {
          setcover(response);
        }
      }
    });
    console.warn('image fetch');
  };

  const handleUpdate = async () => {
    setloading(true);
    console.log('images', cover.uri, profile.uri);
    await Service.updateShop(
      {
        id: userdata.id,
        shop_name: name,
        description: overview,
        password: password,
        // cover: cover.uri,
        // profile: profile.uri,
        cover: cover,
        profile: profile,
      },
      (res) => {
        setloading(false)
        setmodalVisible(false);
        Alert.alert('Update Shop', res.msg);
        init();
        // navigation.navigation.goBack()
      },
      (err) => {
        setloading(false)
        console.log(err)
      }
    );
  };

  return (
    <View style={{}}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}
      >
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Thumbnail
              source={require('../../assets/internal/icon_back.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 20,
              color: '#231f20',
            }}
          >
            Shop Profile
          </Text>
        </Body>
        <Right style={{ flex: 1 }}>
          <TouchableOpacity onPress={() => setmodalVisible(!modalVisible)}>
            <Thumbnail
              source={require('../../assets/headericon/continue.png')}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
            {/* {
                modalStatus === true ? <SaveModal /> : null 
              } */}
          </TouchableOpacity>
          <TouchableWithoutFeedback onPress={() => setmodalVisible(false)}>
            <Modal
              style={{ flex: 1 }}
              animationType='fade'
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => setmodalVisible(false)}
            >
              <View style={styles.mainContainer}>
                <View style={styles.textContainer}>
                  <Text style={styles.title}>Enter your password to apply changes</Text>
                  <View>
                    <TextInput
                      placeholder='Enter here. . .'
                      style={styles.inputPassword}
                      secureTextEntry={true}
                      onChangeText={(value) => setpassword(value)}
                    />
                  </View>
                  <View style={{ paddingVertical: 20 }}>
                    {
                      loading ?
                        <View
                          style={styles.btnSave}
                        >
                          <Text style={styles.btnTextSave}>Saving...</Text>
                        </View>
                        :
                        <TouchableOpacity
                          onPress={() => {
                            handleUpdate();
                          }}
                          style={styles.btnSave}
                        >
                          <Text style={styles.btnTextSave}>Save Changes</Text>
                        </TouchableOpacity>
                    }
                  </View>
                </View>
              </View>
            </Modal>
          </TouchableWithoutFeedback>
        </Right>
      </Header>
      <ImageBackground source={cover} style={styles.headerCoverImage}>
        <TouchableOpacity
          onPress={() => chooseFile('cover')}
          style={{
            position: 'absolute',
            zIndex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            right: 0,
            paddingHorizontal: 20,
            padding: 3,
            width: 80,
            top: 15,
            backgroundColor: '#FFFFFF90',
          }}
        >
          <Icons.AntDesign name='camera' size={20} />
          <Text style={{ marginLeft: 5, fontSize: 14 }}>Edit</Text>
        </TouchableOpacity>
        <View
          style={{
            backgroundColor: 'rgba(0, 0, 0, .4)',
            alignItems: 'center',
            justifyContent: 'space-around',
            height: '100%',
            paddingTop: 5,
            paddingBottom: 5,
          }}
        >
          <View style={styles.avatar}>
            <ImageBackground
              // source={require('../../../../assets/images/face0.jpg')}
              // source={{ uri: 'data:image/jpeg;base64,' + this.state.image}}
              source={profile}
              style={{ alignItems: 'center' }}
              imageStyle={{ borderRadius: 70 }}
            >
              <TouchableOpacity style={{ flex: 0 }} onPress={() => chooseFile('profile')}>
                <FastImage
                  // source={this.state.filePath ? this.state.filePath : require('../../../../assets/images/internal/icon_picframe.png')}
                  source={require('../../assets/internal/icon_picframe.png')}
                  large
                  style={styles.avatarImage}
                />
              </TouchableOpacity>
            </ImageBackground>
          </View>
        </View>
      </ImageBackground>

      <View style={{ flex: 0, marginVertical: 15 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', backgroundColor: '#FFF' }}>
          <View style={{ flex: 2, justifyContent: 'center', paddingHorizontal: 15 }}>
            <Text style={{ fontSize: 12 }}>Shop Name</Text>
          </View>
          <View style={{ flex: 1 }} />
          <View style={{ flex: 2, paddingHorizontal: 15, }}>
            <TextInput
              placeholder={userdata ? userdata.shop_name : 'Set Shop Name'}
              // value={name != "" ? userdata ? userdata.shop_name : "" : name}
              style={{ fontSize: 12, color: '#000' }}
              onChangeText={(e) => setname(e)}
            />
          </View>
        </View>
      </View>

      <View style={{ flex: 0, marginVertical: 5 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', backgroundColor: '#FFF', height: 200 }}>
          <View style={{ flex: 2, paddingHorizontal: 15 }}>
            <TextInput placeholder='Shop Overview' style={{ fontSize: 12 }} onChangeText={(e) => setoverview(e)} value={overview ? overview : ''} />
          </View>
        </View>
      </View>

      <View style={{ flex: 0, marginVertical: 10 }}>
        <View style={{ flexDirection: 'row', padding: 15, justifyContent: 'center', backgroundColor: '#FFF' }}>
          <TouchableOpacity onPress={() => alert('Under Development')} style={{ flex: 2, paddingHorizontal: 15 }}>
            <Text style={{ color: 'darkblue', fontSize: 12 }}>Social Media Accounts</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = {
  container: {
    backgroundColor: '#ecf0f1',
  },
  // not Authenticated
  redirectContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  redirectTitleText: {
    color: '#231f20',
    // fontFamily: 'Roboto',
    fontSize: 25,
  },
  redirectButton: {
    width: '80%',
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#00a14b',
  },
  redirectButtonText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 20,
  },

  //Authenticated
  headerContainer: {
    height: 190,
    width: Dimensions.get('window').width,
  },
  headerCoverImage: {
    width: Dimensions.get('window').width,
    height: 225,
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  searchText: {
    // fontFamily: 'Roboto',
    color: '#7f8c8d',
  },
  // pinned products
  itemRow: {
    justifyContent: 'space-around',
  },
  pinIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  pinItemText: {
    color: '#231f20',
    fontSize: 15,
  },
  historyIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  forwardIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 10,
    marginLeft: 5,
  },

  // Information
  informationContainer: {
    backgroundColor: '#ffffff',
    height: height / 2,
  },

  // contentContainer: {
  //   height: height/2+50
  // },

  // Transaction
  historyText: {
    color: '#231f20',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText1: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText2: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 25,
  },
  transactionHistoryText: {
    color: '#f36e23',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },

  // custom
  space: {
    paddingTop: 5,
  },
  space10: {
    paddingTop: 10,
  },
  space20: {
    paddingTop: 20,
  },
  space65: {
    paddingTop: 65,
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  white: {
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 30,
  },
  userNameText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    alignSelf: 'flex-start',
    paddingBottom: 5,
  },
  userInfoText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 12,
    alignSelf: 'flex-start',
    paddingBottom: 4,
  },
  ratingText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 12,
    alignSelf: 'center',
    paddingBottom: 4,
  },
  ratingScore: {
    color: '#f36e23',
    // fontFamily: 'Roboto',
    fontSize: 15,
    alignSelf: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
  avatar: {
    justifyContent: 'space-around',
  },
  avatarImage: {
    alignSelf: 'center',
    height: 90,
    width: 90,
    borderRadius: 45,
  },
  searchItem: {
    paddingLeft: 5,
    backgroundColor: '#ffffff',
    width: '100%',
    height: 30,
  },
  cartIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  profileRow: {
    flexDirection: 'row',
    // justifyContent: 'space-around',
    width: width,
  },
  coverImageContainer: {
    alignItems: 'flex-start',
    height: '100%',
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'space-around',
  },
  backgroundImageContainer: {
    backgroundColor: 'rgba(0, 0, 0, .4)',
    alignItems: 'flex-start',
    height: '100%',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },

  overviewIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
  },

  backIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  submitIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    // marginRight: 10,
  },
  changePassword: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialmedia: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#5394fc',
  },
  headText: {
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    color: '#231f20',
    fontWeight: 'bold',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
  },
  errorText: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: 'red',
    paddingLeft: 20,
    paddingBottom: 2,
  },
  inputText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    flex: 0,
  },
  space: {
    paddingTop: 15,
  },
  border: {
    height: 1,
    backgroundColor: '#ecf0f1',
  },
  textContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    width: width,
    height: 40,
  },
  headerContainer: {
    height: 190,
    width,
  },
  headerCoverImage: {
    width,
    height: 230,
    justifyContent: 'center',
  },
  titleView: {
    flex: 0,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 15,
    paddingBottom: 0,
  },
  titles: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  textInput: {
    // fontFamily: 'robot-light',
    fontSize: 14,
    color: '#231f20',
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  password: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialAccounts: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#45bcef',
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    height: 200,
  },
  verifyTextContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    height: 170,
  },
  verifyTitle: {
    color: 'black',
    paddingTop: 25,
    // fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingVertical: 10,
  },
  title: {
    color: '#f36e23',
    // paddingVertical:40,
    paddingTop: 40,
    paddingHorizontal: 15,
    // fontFamily: 'Roboto',
    fontSize: 10,
  },
  inputPassword: {
    borderBottomColor: '#e1e2e3',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    width: '90%',
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    color: '#000'
  },
  inputVerify: {
    borderColor: '#e1e2e3',
    borderWidth: 1,
    width: '80%',
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    // paddingBottom:20
  },
  btnSave: {
    width: '80%',
    // height:'40%',
    backgroundColor: '#ababab',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  btnVerify: {
    width: '80%',
    backgroundColor: '#00a14b',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 3,
  },
  headerMyProfile: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15,
      },
      android: {},
    }),
  },
  btnTextSave: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 15,
    // fontFamily: 'Roboto',
  },
};
