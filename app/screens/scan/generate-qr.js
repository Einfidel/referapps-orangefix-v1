import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView, Image, Alert, PermissionsAndroid } from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import QRCode from 'react-native-qrcode-svg';
import { QRscanner } from "react-native-qr-decode-image-camera";
import ImagePicker from "react-native-image-picker";
import Assets from '../../components/assets.manager.js';
import FastImage from 'react-native-fast-image';
import Share from 'react-native-share';
import { captureScreen } from 'react-native-view-shot';
import CameraRoll from "@react-native-community/cameraroll";
import appleAuth from '@invertase/react-native-apple-authentication';

// import RNFS from "@react-native-fs";
import * as RNFS from 'react-native-fs';
const { width, height } = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import {
  Header, Left, Body, Right, Container, Thumbnail
} from 'native-base'

export default function QRGenerator({ navigation, route }) {

  // const [url, setUrl] = useState("https://www.referapps.com/products-services/michael-kors-saffiano-small-mini-leather-crossbody-satchel-35s5gsas1l-luggage/NjMz?referrer=test5")
  const [url, setUrl] = useState('placeholder');
  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [isCameraVisible, setIsCameraVisible] = useState(false);
  const [imageURI, setImageURI] = useState('');
  const [savedImagePath, setSavedImagePath] = useState('');

  useEffect(() => {
    try {
      init()
    } catch (err) {
      console.log(err)
    }
  }, [])

  const init = () => {
    setUrl(route.params.url);
    setImage(route.params.image)
    setName(route.params.name)
    // console.log(route.params.image);
  }

  const shareImage = () => {
    if (imageURI == '') {
      takeScreenShot().then((res) => {
        RNFS.readFile(imageURI, 'base64').then((res) => {
          let urlString = 'data:image/jpeg;base64,' + res;
          let options = {
            title: 'Share Title',
            message: 'Share Message',
            url: urlString,
            type: 'image/jpeg',
          };

          Share.open(options)
            .then((res) => {
              console.log(res);
            })
            .catch((err) => {
              err && console.log(err);
            });
        });
      })
    } else {
      RNFS.readFile(imageURI, 'base64').then((res) => {
        let urlString = 'data:image/jpeg;base64,' + res;
        let options = {
          title: 'Share Title',
          message: 'Share Message',
          url: urlString,
          type: 'image/jpeg',
        };

        Share.open(options)
          .then((res) => {
            console.log(res);
          })
          .catch((err) => {
            err && console.log(err);
          });
      });
    }
  }

  const takeScreenShot = () => {
    // To capture Screenshot
    captureScreen({
      // Either png or jpg (or webm Android Only), Defaults: png
      format: 'jpg',
      // Quality 0.0 - 1.0 (only available for jpg)
      quality: 0.8,
    }).then(
      //callback function to get the result URL of the screnshot
      async (uri) => {
        setSavedImagePath(uri);
        setImageURI(uri);

        try {
          if (appleAuth.isSupported) {
            CameraRoll.save(imageURI, { type: 'photo', album: 'ReferApps' });

            Alert.alert(
              "Success!",
              `Image Saved to Gallery`,
              [
                {
                  text: "Dismiss",
                  style: "cancel",
                },
              ],
              {
                cancelable: true,
              }
            );
          } else {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: "Write on Storage",
                message:
                  "Save the photo on storage",
                buttonNegative: "Cancel",
                buttonPositive: "OK"
              }
            );

            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              CameraRoll.save(imageURI, { type: 'photo', album: 'ReferApps' });

              Alert.alert(
                "Success!",
                `Image Saved to Gallery`,
                [
                  {
                    text: "Dismiss",
                    style: "cancel",
                  },
                ],
                {
                  cancelable: true,
                }
              );
            } else {
              Alert.alert(
                "Error!",
                `User did not allow the app to save the screenshot.`,
                [
                  {
                    text: "Dismiss",
                    style: "cancel",
                  },
                ],
                {
                  cancelable: true,
                }
              );
            }
          }

        } catch (err) {
          console.warn(err);
        }
      },
      (error) => console.error('Oops, Something Went Wrong', error),
    );

    return Promise.resolve('123');;
  };

  const renderQRGenerator = () => {
    console.log("url", url)
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <QRCode
          size={200}
          value={url}
        />
      </View >
    )
  }

  return (
    <Container style={{ backgroundColor: '#FFFFFF' }}>
      <Header style={{ backgroundColor: '#FFFFFF' }}>
        <Left style={{ flex: 0, paddingHorizontal: 3 }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={require('../../assets/headericon/left-arrow-dark.png')}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontFamily: 'roboto-medium', fontSize: 20, color: '#231f20', textAlign: 'center' }}>
            {/* Your Referral QR Code for
            {"\n"} */}
            <Text style={{ fontFamily: 'roboto-medium', fontSize: 20, color: '#231f20', textAlign: 'center' }}>
              {name.length > 29 ? name.substr(0, 29) + "..." : name}
            </Text>
          </Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>

      <ScrollView vertical={true} style={{ flexDirection: 'column', height: width }}>
        <Text style={styles.centerText}>
          <Text style={{ fontWeight: 'bold' }}>
            {"\n"}
            Scan this Image using the Referapps QR Code Scanner
          </Text>
        </Text>

        {renderQRGenerator()}

        <View style={[styles.centerText, { marginTop: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }]}>
          <Text style={{ textAlign: 'center', paddingHorizontal: 15, fontWeight: 'bold', fontSize: 16, }}>
            {name}
          </Text>
        </View>

        <View style={{ marginBottom: 30, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
          <FastImage
            source={{ uri: image === "" ? route.params.image : image }}
            style={{ width: 100, height: 100, borderWidth: 3, borderColor: '#666' }}
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            takeScreenShot()
          }} style={styles.btnCon}
        >
          <Text style={styles.txtBtn}>
            Save this Image
          </Text>
        </TouchableOpacity>

        <View style={{ padding: 5, }} />

        <TouchableOpacity
          onPress={() => {
            shareImage()
          }} style={styles.btnCon}
        >
          <Text style={styles.txtBtn}>
            Save {`&`} Share this Image
          </Text>
        </TouchableOpacity>

        <View style={{ padding: 5, }} />

        <TouchableOpacity
          onPress={() => {
            Share.open({
              title: 'Share this product',
              subject: 'ReferApps',
              message: url,
            })
              .then((res) => { console.log(res) })
              .catch((err) => { err && console.log(err); });
          }} style={styles.btnCon}
        >
          <Text style={styles.txtBtn}>
            Refer via Link
          </Text>
        </TouchableOpacity>

      </ScrollView>
    </Container >
  )

}

const styles = StyleSheet.create({
  centerText: {
    textAlign: 'center',
    marginHorizontal: 20,
    marginVertical: 10,
    fontSize: 16,
    padding: 5,
    bottom: 10,
    color: '#777',
    fontFamily: 'Roboto',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16,
    top: 30
  },
  imgUploadContainer: {
    // flex: 1,
    alignItems: 'center',
    marginTop: height / 2 - 180,
    justifyContent: 'center',
    alignSelf: 'center',
    // position:'absolute',
    // backgroundColor:'pink'
  },
  imgUpload: {
    // backgroundColor:'blue',
    width: 130,
    height: 130,
    resizeMode: 'contain'
  },
  txtNote: {
    // fontFamily:'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    textAlign: 'center',
    paddingBottom: 20,
    width: width / 1.4,
    paddingVertical: 40,
  },
  btnCon: {
    backgroundColor: '#f36e23',
    borderWidth: .7,
    alignSelf: 'center',
    borderRadius: 4,
    width: width / 2,
    paddingVertical: 3,
    borderColor: '#f36e23',
    shadowColor: '#383a3d',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  txtBtn: {
    fontSize: 16,
    color: '#ffffff',
    alignSelf: 'center',
    // fontFamily:'Roboto'
  }
});