import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ScrollView, Image, Alert, BackHandler, } from 'react-native';
import { Spinner } from 'native-base';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import QRCode from 'react-native-qrcode-svg';
import { QRreader } from "react-native-qr-decode-image-camera";
import ImagePicker from "react-native-image-picker";
import Assets from '../../components/assets.manager.js';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';
import FastImage from 'react-native-fast-image'
const { width, height } = Dimensions.get('window');

import {
  Header, Left, Body, Right, Container, Thumbnail
} from 'native-base'

export default function QRScanner({ navigation, route }) {

  const [code, setCode] = useState("")
  const [isCameraVisible, setIsCameraVisible] = useState(false);
  const [loading, setLoading] = useState(false)
  const [user, setUser] = useState("")

  useEffect(() => {
    try {
      init()
    } catch (err) {
      console.log(err)
    }
  }, [])

  const init = async () => {
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)
  }

  const onDone = () => {
    navigation.goBack()
  }

  const onSuccess = e => {
    console.log("Scan from camera url", e.data)
    checkQRValue(e.data)
    // setCode(e.data)
  };

  const renderScanner = () => {
    // if (code != "") return null
    // if (isCameraVisible)
    return (
      <View style={{ flex: 1 }}>
        <QRCodeScanner
          onRead={onSuccess}
          showMarker={true}
          flashMode={RNCamera.Constants.FlashMode.auto}
          topContent={
            <Text style={styles.centerText}>
              Align the QR Code within the frame to scan
            </Text>
          }
          bottomContent={
            <TouchableOpacity onPress={onDone} style={styles.buttonTouchable}>
              <Text style={styles.buttonText}>Done</Text>
            </TouchableOpacity>
          }
        />
      </View>)
  }

  const openPhotoFromGallery = () => {
    // console.log("ImagePicker");
    ImagePicker.launchImageLibrary({}, response => {
      // console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        if (response.uri) {
          console.log("Response = ", response.uri);
          var path = response.path;
          if (!path) {
            path = response.uri;
          }

          QRreader(path)
            .then(data => {
              // alert(data)
              console.log("Scan from gallery url", data)
              checkQRValue(data)
            })
            .catch(err => {
              console.log("Error", err)
            });
        }
      }
    });
  }

  const checkQRValue = async (qrCodeValue) => {
    if (qrCodeValue.toString().includes("referrer")) {
      redirectToItem(qrCodeValue)
    } else {
      let splitEmail = user.data.info.data.email.split('@');

      if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
        // user.data.is_verify_contact_number === "no" ||
        user.data.is_verify === 'no') {
        Alert.alert(
          'Warning!',
          'Your account not yet been verified.\n' +
          'Verify Now!',
          [
            {
              text: 'Cancel'
            },
            {
              text: 'Verify',
              onPress: () => {
                if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                  navigation.navigate("AddEmailMobile");
                } else if (user.data.is_verify === 'no') {
                  navigation.navigate('EmailVerification')
                }
                // else if (user.data.is_verify_contact_number === "no") {
                //   navigation.navigate('PhoneVerification')
                // }
              }
            },
          ],
          { cancelable: true }
        );
        return
      }

      useServiceVoucher(qrCodeValue, 'no')
    }
  }

  const useServiceVoucher = async (voucherID, verified = 'no', reloads = 0) => {
    setLoading(true)
    let data = {
      user_id: user.data.id,
      service_id: voucherID,
      verified: verified,
    };

    await Service.useVoucher(
      data,
      (res) => {
        if (verified === 'no') {
          if (res.status) {
            setLoading(false);
            console.log('verification_code', res.verification_code);
            let verifData = {
              verifyCode: res.verification_code,
              medium: res.sent_to,
              expiration: res.verification_expiration,
              user: user,
            }
            navigation.push("VerifyCode", {
              verifData,
              reloads,
              onVerified: (verified, reloads = 0) => {
                useServiceVoucher(voucherID, verified, reloads);
              },
            })
          } else {
            setLoading(false);
            alert(res.msg);
          }
        } else {
          if (res.status) {
            setLoading(false);
            console.log('Data: ', res);
            Alert.alert(
              'Success',
              'Voucher has been successfully used!',
              [
                {
                  text: 'Check Ledger',
                  onPress: async () => {
                    // navigation.goBack();
                    // navigation.goBack();
                    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
                    navigation.navigate("My Performance")
                  },
                },
                {
                  text: 'Scan again',
                  onPress: async () => {
                    // navigation.goBack();
                    // navigation.goBack();
                    BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
                    navigation.navigate("Scanner")
                  },
                },
              ],
              { cancelable: false },
            );
          } else {
            setLoading(false);
            alert(res.msg);
          }
        }
      },
      (err) => {
        console.warn('useVoucher 1', err.message)
      },
    );
  }

  const redirectToItem = async (url) => {
    setLoading(true)

    try {
      let urlSplit = url.split("/");
      let itemNameWithDashes = urlSplit[4]
      let item_name = itemNameWithDashes.split("-").join(" ");
      let data = {};
      data.name = item_name;
      console.log("item_name", item_name)
      console.log("----------------------------")

      await Service.getItemByName(
        data,
        (res) => {
          res.data.map(async (item) => {
            console.log('item.product_name === item_name', item.product_name.toLowerCase(), "||", item_name)
            if (item.product_name.toLowerCase() === item_name) {

              try {
                let urlSplit = url.split("?");
                let referrerField = urlSplit[urlSplit.length - 1]
                let referrerFieldSplit = referrerField.split("=");
                let referrerUsername = referrerFieldSplit[1]
                console.log("urlSplit", urlSplit)
                console.log("referrerUsername", referrerUsername)

                let data = {};
                data.item_id = item.id;
                data.referrer_username = referrerUsername;
                data.user_id = user.data.id;
                console.log("item_name", item_name)
                console.log("----------------------------")

                await Service.getItemReferralData(
                  data,
                  (res) => {
                    if (res.referrer_data.refer_id === "") {
                      setLoading(false)
                      Alert.alert(
                        "Success",
                        "User now has " + referrerUsername + "'s referral for" + item.product_name,
                        [
                          {
                            text: "Visit Item now",
                            onPress: (() => {
                              navigation.navigate('ProductDetails', {
                                product_id: item.id,
                              });
                            })
                          },
                        ],
                      )
                    } else {
                      setLoading(false)
                      Alert.alert(
                        "User already has a referral for this item",
                        `User: ` + res.referrer_data.username + ' has already reffered you to this item.',
                        [
                          {
                            text: "Visit Item anyways",
                            onPress: (() => {
                              navigation.navigate('ProductDetails', {
                                product_id: item.id,
                              });
                            })
                          },
                          {
                            text: "Dismiss",
                            style: "cancel",
                          },
                        ],
                        {
                          cancelable: true,
                        }
                      );
                    }
                  },
                  (err) => {
                    console.warn('getItemReferralData 2', err.message)
                  },
                );
              } catch (error) {
                alert('getItemReferralData error 1', error);
                console.log('getItemReferralData error 1', error)
              }
            }
          })
        },
        (err) => {
          console.warn('getItemByName 2', err.message)
        },
      );
    } catch (error) {
      alert('getItemByName error 1', error);
      console.log('getItemByName error 1', error)
    }

    setLoading(false)
    return null
  }

  return (
    <Container style={{ backgroundColor: '#FFFFFF' }}>
      <Header style={{ backgroundColor: '#FFFFFF' }}>
        <Left style={{ flex: 0, paddingHorizontal: 3 }}>
          <TouchableOpacity onPress={() => {
            if (isCameraVisible) {
              setIsCameraVisible(false);
            } else {
              navigation.goBack()
            }
          }}>
            <Thumbnail
              source={require('../../assets/headericon/left-arrow-dark.png')}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ fontFamily: 'roboto-medium', fontSize: 20, color: '#231f20' }}>Scan QR Code</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>
      {loading ? <Spinner /> : null}

      {isCameraVisible ?
        renderScanner()
        :
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <View style={styles.imgUploadContainer}>
            <Text style={styles.txtNote}>
              Click the QR Code below to scan using your Camera
              {"\n"}
            </Text>
            <TouchableOpacity
              onPress={() => {
                loading ? null : setIsCameraVisible(true);
              }}>
              <View style={styles.qrCodeContainer}>
                <View style={styles.qrCodeContainer2}>
                  <FastImage
                    source={Assets.qr.qr_code}
                    style={styles.qrCode}
                    resizeMode={FastImage.resizeMode.contain}
                  />
                </View>
              </View>
            </TouchableOpacity>
            <Text style={styles.txtNote}>
              {/* {"\n"} */}
            </Text>
            {/* <Text style={styles.txtNote}>To provide marketing tools to companies and individuals and create a perpetual income.</Text> */}
            {/* <View style={{ padding: 5, }} /> */}

            <View style={{ flexDirection: 'row', marginHorizontal: 20, }}>
              {/* <View style={{ marginHorizontal: 10, }}> */}
              <View style={{ backgroundColor: '#777', height: 2, flex: 1, alignSelf: 'center' }} />
              <Text style={{ alignSelf: 'center', paddingHorizontal: 5, fontSize: 24, marginHorizontal: 10, color: '#777', fontFamily: 'Roboto', }}>
                Or
              </Text>
              <View style={{ backgroundColor: '#777', height: 2, flex: 1, alignSelf: 'center' }} />
              {/* </View> */}
            </View>

            <Text style={{ paddingBottom: 10, }}>
            </Text>

            <TouchableOpacity onPress={() => {
              loading ? null : openPhotoFromGallery();
            }} style={styles.btnCon}>
              <Text style={styles.txtBtn}>
                Scan from Gallery
              </Text>
            </TouchableOpacity>
          </View>
        </View>}
    </Container >
  )

}

const styles = StyleSheet.create({
  centerText: {
    textAlign: 'center',
    marginHorizontal: 20,
    marginVertical: 10,
    fontSize: 16,
    padding: 5,
    bottom: 10,
    color: '#777',
    fontFamily: 'Roboto',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  textBold: {
    fontWeight: '500',
    color: '#000'
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)'
  },
  buttonTouchable: {
    padding: 16,
    top: 30
  },
  imgUploadContainer: {
    // flex: 1,
    alignItems: 'center',
    // marginTop: height / 2 - 180,
    marginTop: height / 3 - 180,
    justifyContent: 'center',
    alignSelf: 'center',
    // position:'absolute',
    // backgroundColor:'pink'
  },
  qrCode: {
    // backgroundColor:'blue',
    width: 130,
    height: 130,
    resizeMode: 'contain',
  },
  qrCodeContainer: {
    backgroundColor: '#555',
    borderRadius: 5,
    borderWidth: 3,
    borderColor: "#555",
  },
  qrCodeContainer2: {
    backgroundColor: '#92D062',
    borderRadius: 1,
    borderWidth: 8,
    borderColor: "#92D062",
  },
  txtNote: {
    // fontFamily:'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
    textAlign: 'center',
    paddingBottom: 20,
    width: width / 1.4,
    // paddingVertical: 40,
  },
  btnCon: {
    backgroundColor: '#f36e23',
    borderWidth: .7,
    alignSelf: 'center',
    borderRadius: 4,
    width: width / 2,
    paddingVertical: 4,
    borderColor: '#f36e23',
    shadowColor: '#383a3d',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  txtBtn: {
    fontSize: 20,
    color: '#ffffff',
    alignSelf: 'center',
    // fontFamily:'Roboto'
  },
});