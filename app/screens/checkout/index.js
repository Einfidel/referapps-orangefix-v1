import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Modal,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  Alert,
  Image,
} from 'react-native';
import {
  Container,
  Header,
  Thumbnail,
  Footer,
  FooterTab,
  CheckBox,
  Body,
  Left,
  Right,
  ListItem,
  Content,
  Card,
  CardItem,
} from 'native-base';
import {
  CreditCardInput,
  LiteCreditCardInput,
} from 'react-native-credit-card-input';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { TextInput } from 'react-native-gesture-handler'
import styles from '../../styles/cart/cartScreenStyles';
import Assets from '../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import Service from '../../components/api/service';
import { acc } from 'react-native-reanimated';
import { fontSize } from 'styled-system';
import FastImage from 'react-native-fast-image';

const { width, height } = Dimensions.get('window');
const PAYMENT_OPTIONS = [
  {
    option: 'paypal',
    icon: Assets.cartFunction.payment_paypal,
    header: 'Pay Through Paypal',
    description:
      'Select this only if you have a Paypal account. Otherwise, your order will not be processed. You will be redirected to Paypal to complete payment.',
  },
  {
    option: 'xendit',
    icon: Assets.cartFunction.payment_xendit,
    header: 'Pay Through Xendit',
    // description:
    //   'Select this only if you have a Paypal account. Otherwise, your order will not be processed. You will be redirected to Paypal to complete payment.',
  },
  // {
  //   option: 'referapps',
  //   icon: Assets.landing.header_icon,
  // },
  // {
  //   option: 'card',
  //   icon: Assets.cartFunction.payment_card,
  // },
  // {
  //   option: 'cod',
  //   icon: Assets.cartFunction.payment_cod,
  //   header: 'Cash On Delivery',
  //   description:
  //     'Pay using our Cash on Delivery Service. Payment is done directly to the courier upon receiving the item.',
  // },
];

function CustomHeader(props) {
  return (
    <Header style={{ backgroundColor: '#00a14b' }}>
      <Body>
        <Text style={{ marginLeft: 10, color: 'white' }}>{props.title}</Text>
      </Body>
    </Header>
  );
}

function CustomEditButton(props) {
  let { onPress } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.goBackHomeButton,
        { width: null, height: null, paddingHorizontal: 5 },
      ]}>
      <Text style={{ color: '#00a14b', fontSize: 12 }}>Change</Text>
    </TouchableOpacity>
  );
}

function Address(props) {
  let { type, data, onPressChange, navigation, noAddress, initAddresses, setAddressModalVisibility } = props;
  let addressType = type === 'Ship' ? 'Add shipping address' : 'Add billing address';
  // console.log("noAddress", noAddress);

  return (
    <View>
      {data === null ? (
        <TouchableOpacity
          onPress={() =>
            noAddress ? navigation.navigate('Add Address', {
              onAdd: () => {
                // console.log("calling initAddresses")
                initAddresses();
                setAddressModalVisibility(true);
              },
              from: "Checkout"
            }) : onPressChange(type)
          }
          style={checkoutStyles.addAddress}>
          <Text style={[checkoutStyles.grayText, { paddingHorizontal: 20 }]}>
            + {addressType}
          </Text>
        </TouchableOpacity>
      ) : (
        <View style={{ padding: 10, margin: 10, borderWidth: 2, borderColor: '#bababa' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={checkoutStyles.grayText}>{type} to</Text>
            <CustomEditButton onPress={() => onPressChange(type)} />
          </View>
          <Text style={checkoutStyles.addresNameText}>{data.info.data.contact_person}</Text>
          {/* <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_person}
          </Text> */}
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.street_address}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.province_code} - {data.info.data.city_code} - {data.info.data.barangay_code}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.country_code == "PH" ? "Philippines" : "USA"}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_number}
          </Text>
        </View>
      )}
    </View>
  );
}

export default function CheckoutScreen({ navigation, route }) {
  const [selectedBillingAddress, setselectedBillingAddress] = useState(null);
  const [selectedShippingAddress, setselectedShippingAddress] = useState(null);
  const [isAddressModalVisible, setAddressModalVisibility] = useState(null);
  const [isAddressSame, setSameAddress] = useState(false);
  const [loading, setloading] = useState(true)
  const [addresses, setAddresses] = useState([
    // {
    //   name: 'Millet, Othan Binalay',
    //   address: 'Maramba Boulevard, Poblacion',
    //   postcode: 'PANGASINAN - LINGAYEN (CAPITAL) - POBLACION',
    //   phonenumber: '09369410878',
    //   country: 'Philippines',
    // },
    // {
    //   name: 'Altezo, Bryan',
    //   address: 'Maramba Boulevard, Poblacion',
    //   postcode: 'PANGASINAN - LINGAYEN (CAPITAL) - POBLACION',
    //   phonenumber: '09123456789',
    //   country: 'Philippines',
    // },
  ]);
  const [addressToEdit, setAddressToEdit] = useState('');
  const [selectedPaymentOption, setPaymentOption] = useState('paypal');
  const [testingPassword, setTestingPassword] = useState('');
  const [cart, setCart] = useState([]);
  const [isSinglePurchase, setIsSinglePurchase] = useState(false);

  useEffect(() => {
    NetInfo.fetch().then((state) => {
      // console.warn("net connection", state.isConnected)
      if (state.isConnected == true) {
        init();
        initAddresses();
      } else {
        setloading(false);
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
  }, [])

  const init = async () => {
    setloading(true)
    let tmp = route.params.checkoutData;
    let ud = await AsyncStorage.getItem('user_data');
    let _userdata = ud ? JSON.parse(ud) : null;
    await Service.getAccountCart(_userdata.data.id,
      (res) => {
        setCart(res.data)
        setloading(false)

        if (route.params?.checkoutData?.pid != null) {
          setIsSinglePurchase(true)
        }
        // console.log('cart data', res.data)
      },
      (err) => {
        setloading(false)
        console.log("Service.getAccountCart error", err)
      }
    );
  }

  const initAddresses = async () => {
    let ud = await AsyncStorage.getItem('user_data');
    let _userdata = ud ? JSON.parse(ud) : null;
    await Service.getSpecificAddress(_userdata.data.id,
      (res) => {
        setAddresses(res.data)
        // console.log('address data', res.data)

        res.data.map((address) => {
          if (address.info.data.is_shipping === "yes") {
            setselectedShippingAddress(address);
          }
          if (address.info.data.is_billing === "yes") {
            setselectedBillingAddress(address);
          }
        });
      },
      (err) => {
        console.log(err)
      }
    );
  }

  const setShippingBillingAddress = async (item, type) => {
    try {
      let ud = await AsyncStorage.getItem("user_data")
      if (ud == null) {
        return
      }
      let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null

      let data = {};
      data.uid = _userdata.data.id;
      data.pid = item.id;
      data.fName = item.info.data.contact_person_fname;
      data.mName = item.info.data.contact_person_mname;
      data.lName = item.info.data.contact_person_lname;
      data.number = item.info.data.contact_number;
      data.notes = item.info.data.notes;
      data.street = item.info.data.street_address;
      data.selectedProvince = item.info.data.province_code;
      data.selectedCity = item.info.data.city_code;
      data.selectedBarangay = item.info.data.barangay_code;

      if (type === "Shipping") {
        data.is_shipping = "yes";
        data.is_billing = item.info.data.is_billing;
      } else if (type === "Billing") {
        data.is_billing = "yes";
        data.is_shipping = item.info.data.is_shipping;
      }

      await Service.updateAddress(data,
        (response) => {
          if (response.status) {
            // console.log("updateAddress with new " + type + " address", response.msg)
          } else {
            console.log(response.msg)
          }
        },
        (err) => {
          console.log(err)
        })
    } catch (e) {
      console.log(e)
    }
  }

  const handleAddress = (item) => {
    if (addressToEdit === 'Ship') {
      // console.log("set shipping to" + item.id)
      addresses.map((address) => {
        address.info.data.is_shipping = "no";
      })
      item.info.data.is_shipping = "yes";
      setselectedShippingAddress(item);
      setShippingBillingAddress(item, "Shipping");
    }

    if (addressToEdit === 'Bill') {
      // console.log("set billing to" + item.id)
      addresses.map((address) => {
        address.info.data.is_billing = "no";
      })
      item.info.data.is_billing = "yes";
      setselectedBillingAddress(item);
      setShippingBillingAddress(item, "Billing");
    }

    if (isAddressSame) {
      // console.log("set shipping to" + item.id)
      // console.log("set billing to" + item.id)
      addresses.map((address) => {
        address.info.data.is_shipping = "no";
        address.info.data.is_billing = "no";
      })
      item.info.data.is_shipping = "yes";
      item.info.data.is_billing = "yes";
      setselectedBillingAddress(item);
      setShippingBillingAddress(item, "Billing");
      setselectedShippingAddress(item);
      setShippingBillingAddress(item, "Shipping");
    }

    setAddressModalVisibility(false);
    // initAddresses();
  };

  const handleSameAddress = () => {
    setSameAddress(!isAddressSame);
    setTimeout(() => {
      if (isAddressSame) {
        if (addressToEdit === 'Ship' && selectedShippingAddress !== null) {
          setselectedShippingAddress(selectedShippingAddress);
        } else if (
          addressToEdit === 'Bill' &&
          selectedBillingAddress !== null
        ) {
          setselectedBillingAddress(selectedShippingAddress);
        }
      }
    }, 1000);
  };

  const renderAddress = ({ item, index }) => {
    let data = item;
    return (
      <TouchableOpacity
        style={{ padding: 10, margin: 10, marginBottom: 0, borderWidth: 2, borderColor: '#dadada' }}
        onPress={() => {
          handleAddress(data);
        }}>
        <Body>
          <Text style={checkoutStyles.addresNameText}>{data.info.data.contact_person}</Text>
          {/* <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_person}
          </Text> */}
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.street_address}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.province_code} - {data.info.data.city_code} - {data.info.data.barangay_code}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.country_code == "PH" ? "Philippines" : "USA"}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_number}
          </Text>

          <TouchableOpacity
            onPress={() => {
              setAddressModalVisibility(false);
              navigation.navigate('Edit Address', {
                onEdit: () => {
                  // console.log("calling initAddresses")
                  initAddresses();
                  setAddressModalVisibility(true);
                },
                from: "Checkout",
                value: data
              })
            }
            }
            style={[
              styles.goBackHomeButton,
              { width: null, height: null, paddingHorizontal: 5 },
            ]}>
            <Text style={{ color: '#00a14b', fontSize: 12 }}>Edit</Text>
          </TouchableOpacity>
        </Body>
      </TouchableOpacity >
    );
  };

  const renderAddressModal = () => {
    return (
      <Modal
        style={{ flex: 1 }}
        animationType="fade"
        transparent={true}
        visible={isAddressModalVisible}
        onRequestClose={() => setAddressModalVisibility(false)}>
        <TouchableOpacity
          onPress={() => setAddressModalVisibility(false)}
          style={checkoutStyles.modalBackgroundContainer}>
          <TouchableWithoutFeedback>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 5,
                width: width - 40,
                padding: 10,
                marginBottom: 80,
                marginTop: 80,
              }}>
              <Text style={[checkoutStyles.grayText, { textAlign: 'center', fontSize: 18 }]}>
                Select Address
              </Text>
              {/* <View style={{ paddingBottom: 8, borderBottomWidth: 0.5, borderTopColor: '#e1e2e3' }} /> */}
              <FlatList
                data={addresses}
                renderItem={renderAddress}
                extraData={addresses}
                keyExtractor={(item, index) => index.toString()}
              />
              <TouchableOpacity
                onPress={() => {
                  // console.log("addresses", addresses);
                  navigation.navigate('Add Address', {
                    onAdd: () => {
                      // console.log("calling initAddresses")
                      initAddresses();
                      setAddressModalVisibility(true);
                    },
                    from: "Checkout"
                  });
                  // navigation.navigate('Add Address',
                  //   {
                  //     route: "checkout"
                  //   }
                  // );
                  setAddressModalVisibility(false);
                }}
                style={{ padding: 10, margin: 10, marginBottom: 0, borderWidth: 2, borderColor: '#dadada' }}>
                <Text style={[checkoutStyles.grayText, { paddingHorizontal: 20 }]}>
                  + Add Address
                </Text>
              </TouchableOpacity>
              <ListItem>
                <CheckBox
                  checked={isAddressSame}
                  color="green"
                  onPress={handleSameAddress}
                />
                <Body>
                  <Text style={[checkoutStyles.grayText, { paddingLeft: 10 }]}>
                    Set same shipping and billing address
                  </Text>
                </Body>
              </ListItem>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    );
  };

  const renderSummary = () => {
    // console.log("item.info.data.product_info", item.info.data.product_info);
    // let { data } = item;
    let totalCount = 0;
    let totalSubAmount = 0;
    let totalShippingFee = 0;
    let totalAmount = 0;
    let item = {};

    if (isSinglePurchase) {
      item = route.params.checkoutData;
      // console.log("item", item)
      totalCount++;
      totalSubAmount += parseFloat(item.price) * parseFloat(item.qty);
      totalShippingFee += parseFloat(item.delivery_courier_fee) * parseFloat(item.qty);
    } else {
      cart.map((item, i) => {
        // console.log("item", item);
        if (item.is_selected == "yes" || item.is_selected) {
          totalCount++;
          totalSubAmount += parseFloat(item.price) * parseFloat(item.qty);
          totalShippingFee += parseFloat(item.delivery_courier_fee) * parseFloat(item.qty);
        }
      });
    }

    totalAmount = totalSubAmount + totalShippingFee;

    return (
      <ListItem style={{ alignItems: 'center' }} >
        <Card>
          <CardItem>
            <View style={{ marginHorizontal: 10, width: wp('77%') }}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>Subtotal:</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${totalSubAmount.toFixed(2)}`}</Text>
                </View>
              </View>
              <View style={{ borderWidth: 0.5, borderColor: '#111', margin: 3, padding: 5, marginBottom: 10 }}>
                {
                  isSinglePurchase ?
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#000000' }}>{item.product_name.length > 20 ? item.product_name.substr(0, 19) + "...  x  " + item.qty : item.product_name + "  x  " + item.qty}</Text>
                      </View>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${totalSubAmount.toFixed(2)}`}</Text>
                        {/* <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${item.total.toFixed(2)}`}</Text> */}
                      </View>
                    </View>
                    :
                    cart.map((item, i) => {
                      if (item.is_selected == "yes" || item.is_selected) {
                        return (
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ fontSize: 16, color: '#000000' }}>{item.product_name.length > 20 ? item.product_name.substr(0, 19) + "...  x  " + item.qty : item.product_name + "  x  " + item.qty}</Text>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${item.total.toFixed(2)}`}</Text>
                            </View>
                          </View>
                        )
                      }
                    })
                }
              </View>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3 }}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>Shipping Fee Total:</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${totalShippingFee.toFixed(2)}`}</Text>
                </View>
              </View>
              <View style={{ borderWidth: 0.5, borderColor: '#111', margin: 3, padding: 5, marginBottom: 5 }}>
                {
                  isSinglePurchase ?
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#000000' }}>{item.product_name.length > 20 ? item.product_name.substr(0, 19) + "...  x  " + item.qty : item.product_name + "  x  " + item.qty}</Text>
                      </View>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + totalShippingFee.toFixed(2)}</Text>
                        {/* <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + item.delivery_courier_fee}</Text> */}
                      </View>
                    </View>
                    :
                    cart.map((item, i) => {
                      if (item.is_selected == "yes" || item.is_selected) {
                        return (
                          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ fontSize: 16, color: '#000000' }}>{item.product_name.length > 20 ? item.product_name.substr(0, 19) + "...  x  " + item.qty : item.product_name + "  x  " + item.qty}</Text>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                              <Text style={{ fontSize: 16, color: '#000000' }}>{`\u20B1 ` + item.delivery_courier_fee}</Text>
                            </View>
                          </View>
                        )
                      }
                    })
                }
              </View>
              <View style={checkoutStyles.space} />
              <View style={checkoutStyles.itemBorder} />
              <View style={checkoutStyles.space} />
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>Total Amount</Text>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#000000' }}>{`\u20B1 ` + `${totalAmount.toFixed(2)}`}</Text>
                </View>
              </View>
            </View>
          </CardItem>
        </Card>
      </ListItem>
    )
  };

  const renderPaymentOptions = ({ item, index }) => {
    let { option, icon } = item;
    return (
      <TouchableOpacity
        disabled={option === 'referapps'}
        onPress={() => {
          console.log("option", option)
          setPaymentOption(option)
        }}
        style={[
          { opacity: option === 'referapps' ? 0.5 : 1 },
          checkoutStyles.optionStyle,
          selectedPaymentOption === option &&
          checkoutStyles.selectedOptionStyle,
        ]}>
        <FastImage
          source={icon}
          resizeMode={FastImage.resizeMode.contain}
          style={{
            resizeMode: 'contain',
            width: 50,
            height: 50,
            tintColor: selectedPaymentOption === option ? 'white' : 'gray',
          }}
        />

        {/* <Thumbnail
          source={icon}
          style={{
            height: width / 6,
            resizeMode: 'contain',
            // tintColor: 'black',
            tintColor: selectedPaymentOption === option ? 'white' : 'black',
          }}
        /> */}
      </TouchableOpacity>
    );
  };

  const _onChange = (form) => {
    const accepted = ['visa', 'master-card', 'american-express'];
    // console.log(form);
    !accepted.includes(form.values.type) &&
      Alert.alert(
        'Error',
        'We only accept Visa, Mastercard and American Express.',
      );
  };

  const renderPaymentDetails = () => {
    let index = PAYMENT_OPTIONS.findIndex(
      (i) => i.option === selectedPaymentOption,
    );
    let data = PAYMENT_OPTIONS[index];
    switch (selectedPaymentOption) {
      case 'card':
        return (
          <View style={{ paddingBottom: 10 }}>
            <Text style={[checkoutStyles.grayText, { margin: 10, fontSize: 11 }]}>
              NOTE: We only accept Visa, Mastercard and American Express.
            </Text>
            <CreditCardInput
              onChange={_onChange}
              cardScale={0.75}
              labelStyle={checkoutStyles.addresNameText}
            />
          </View>
        );
      default:
        return (
          <View style={{ padding: 10 }}>
            <Text style={checkoutStyles.addresNameText}>{data.header}</Text>
            <Text style={[checkoutStyles.grayText, { marginVertical: 10 }]}>
              {data.description}
            </Text>
          </View>
        );
    }
  };

  const handleCheckout = async (form) => {
    const accepted = ['visa', 'master-card', 'american-express'];
    // console.log(form);

    // if (!accepted.includes(form.values.type)) {
    //   Alert.alert(
    //     'Error',
    //     'We only accept Visa, Mastercard and American Express.',
    //   );
    // }
    // console.log(form);

    if (addresses.length == 0) {
      Alert.alert(
        'Address Required',
        'Please input your Shipping and Billing address',
        [
          {
            text: 'OK',
            style: 'cancel'
          },
        ],
        { cancelable: false }
      );
      return
    }

    let has_billing = false;
    let has_shipping = false;

    addresses.map((address) => {
      if (address.info.data.is_billing == 'yes') {
        has_billing = true;
      }

      if (address.info.data.is_shipping == 'yes') {
        has_shipping = true;
      }
    })

    if (!has_billing || !has_shipping) {
      Alert.alert(
        'Address Required',
        'Please input your Shipping and Billing address',
        [
          {
            text: 'OK',
            style: 'cancel'
          },
        ],
        { cancelable: false }
      );
      return
    }

    switch (selectedPaymentOption) {
      case 'paypal':
        try {
          await Service.checkoutPaypal(
            route.params.checkoutData,
            (res) => {
              if (res.errors) {
                console.log('checkoutPaypal error 3', res.errors);
                Alert.alert('Upload Error 3', 'Please try again ' + res.errors);
              } else {
                console.log('checkoutPaypal success', res);
                navigation.navigate('PaypalCheckout',
                  {
                    url: res.redirectUrl
                  }
                );
              }
            },
            (err) => {
              console.log("checkoutPaypal error 2", err);
              Alert.alert('Upload Error 2', 'Please try again ' + err);
            },
          );
        } catch (error) {
          console.log("checkoutPaypal error 1", error);
          Alert.alert('Upload Error 1', 'Please try again ' + error);
        }
        break;

      case 'xendit':
        try {
          await Service.checkoutXendit(
            route.params.checkoutData,
            (res) => {
              if (res.errors) {
                console.log('checkoutXendit error 3', res.errors);
                Alert.alert('Upload Error 3', 'Please try again ' + res.errors);
              } else {
                console.log('checkoutXendit success', res);
                navigation.navigate('XenditCheckout',
                  {
                    url: res.data.invoice_url
                  }
                );
              }
            },
            (err) => {
              console.log("checkoutXendit error 2", err);
              Alert.alert('Upload Error 2', 'Please try again ' + err);
            },
          );
        } catch (error) {
          console.log("checkoutXendit error 1", error);
          Alert.alert('Upload Error 1', 'Please try again ' + error);
        }
        break;

      default:
        break;
    }
  };

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={styles.headerStyle}>
        {isAddressModalVisible && renderAddressModal()}
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.headerText}>Checkout</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>
      <Content>
        <CustomHeader title="Shipping & Billing" />
        <Address
          noAddress={addresses.length === 0}
          navigation={navigation}
          onPressChange={(selectedAddress) => {
            setAddressModalVisibility(true);
            setAddressToEdit(selectedAddress);
          }}
          initAddresses={() => {
            initAddresses();
          }}
          setAddressModalVisibility={(bool) => {
            setAddressModalVisibility(bool);
          }}
          data={selectedShippingAddress}
          type="Ship"
        />
        <Address
          noAddress={addresses.length === 0}
          navigation={navigation}
          onPressChange={(selectedAddress) => {
            setAddressModalVisibility(true);
            setAddressToEdit(selectedAddress);
          }}
          initAddresses={() => {
            initAddresses();
          }}
          setAddressModalVisibility={(bool) => {
            setAddressModalVisibility(bool);
          }}
          data={selectedBillingAddress}
          type="Bill"
        />
        <CustomHeader title="Summary" />
        {/* <FlatList
          data={cart}
          renderItem={renderSummary}
          // horizontal
          keyExtractor={(item, index) => index.toString()}
        /> */}

        {renderSummary()}

        <CustomHeader title="Payment" />
        <FlatList
          data={PAYMENT_OPTIONS}
          renderItem={renderPaymentOptions}
          horizontal
          extraData={selectedPaymentOption}
          keyExtractor={(item, index) => index.toString()}
        />
        {selectedPaymentOption !== '' && renderPaymentDetails()}

        {/* <View style={{ marginBottom: 50, margin: 5, padding: 5, borderWidth: 1 }}>
          <Text style={{ fontSize: 17 }}>Payment Testing Password</Text>
          <TextInput style={{ fontSize: 17, margin: 5, padding: 5, borderWidth: 1 }}
            placeholder='Secret Password'
            placeholderTextColor='#7f8c8d'
            value={testingPassword}
            onChangeText={(e) => { setTestingPassword(e) }}
          />
        </View> */}
      </Content>
      <Footer style={styles.footer2}>
        <FooterTab style={styles.footerTab2}>
          <TouchableOpacity
            // disabled={
            //   selectedBillingAddress === '' ||
            //   selectedShippingAddress === '' ||
            //   selectedPaymentOption === ''
            // }
            onPress={async () => {
              // console.log(testingPassword);
              // if (testingPassword === "Test123@!") {
              // console.log("test passed");
              handleCheckout();
              // } else {
              //   alert("Please input the correct testing password to proceed to payment")
              // }
            }}
            style={[
              { width: width - 40 },
              styles.btnCheckout,
              // {
              //   opacity: selectedBillingAddress === '' ||
              //   selectedShippingAddress === '' ||
              //   selectedPaymentOption === '' ? 0.5 : 1
              // }
            ]}>
            <Text style={styles.btnTextCheckout}>Proceed</Text>
          </TouchableOpacity>
        </FooterTab>
      </Footer>
    </Container >
  );
}

const checkoutStyles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    alignSelf: 'center',
  },
  addAddress: {
    width: width,
    borderWidth: 0.7,
    borderColor: '#d0dbdb',
    paddingVertical: 20,
  },
  grayText: {
    fontSize: 14,
    color: 'gray',
  },
  addresNameText: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
  },
  modalBackgroundContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  optionStyle: {
    width: width / 4 - 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    margin: 5,
    borderWidth: 1,
    borderColor: '#00a14b',
  },
  selectedOptionStyle: {
    backgroundColor: '#579847',
  },
  itemBorder: {
    borderTopWidth: 1,
    borderTopColor: '#626262',
  },
  space: {
    paddingTop: 5,
  },
});