import React, { Component } from 'react';
import { Alert } from 'react-native';
import { WebView } from 'react-native-webview';

export default function CheckOut(navigation) {

  return (
    <WebView
      originWhitelist={['*']}
      source={{ uri: navigation.route.params.url }}
      style={{ width: '100%', height: '100%' }}
      onNavigationStateChange={(webViewState) => {
        console.log("Current URL: ", webViewState.url)
        let url = webViewState.url
        if (url.includes("mobile-service-done") || url.includes("mobile-done")) {
          if (url.includes("#cancel")) {
            Alert.alert(
              "Warning",
              "Your payment was cancelled!",
              [
                {
                  text: 'OK',
                  onPress: () => {
                    navigation.navigation.navigate("Home")
                  },
                },
              ],
              { cancelable: false })
          } else if (url.includes("#continue")) {
            navigation.navigation.navigate("PurchaseHistory")
          }
        } else if (url.includes("paypal/continue")) {
          navigation.navigation.navigate("Home")
          Alert.alert(
            "Alert",
            "Something unexpected has occured!",
            [
              {
                text: 'OK',
                onPress: () => {
                  navigation.navigation.navigate("Home")
                },
              },
            ],
            { cancelable: false }) 
        }
      }}

    />
  );
}