import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Modal,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  Alert,
} from 'react-native';
import {
  Container,
  Header,
  Thumbnail,
  Footer,
  FooterTab,
  CheckBox,
  Body,
  Left,
  Right,
  ListItem,
  Content,
} from 'native-base';
import {
  CreditCardInput,
  LiteCreditCardInput,
} from 'react-native-credit-card-input';

import styles from '../../styles/cart/cartScreenStyles';
import Assets from '../../components/assets.manager';
import { acc } from 'react-native-reanimated';

const { width, height } = Dimensions.get('window');
const PAYMENT_OPTIONS = [
  {
    option: 'paypal',
    icon: Assets.cartFunction.payment_paypal,
    header: 'Pay Through Paypal',
    description:
      'Select this only if you have a Paypal account. Otherwise, your order will not be processed. You will be redirected to Paypal to complete payment.',
  },
  // {
  //   option: 'referapps',
  //   icon: Assets.landing.header_icon,
  // },
  // {
  //   option: 'card',
  //   icon: Assets.cartFunction.payment_card,
  // },
  {
    option: 'cod',
    icon: Assets.cartFunction.payment_cod,
    header: 'Cash On Delivery',
    description:
      'Pay using our Cash on Delivery Service. Payment is done directly to the courier upon receiving the item.',
  },
];

function CustomHeader(props) {
  return (
    <Header style={{ backgroundColor: '#00a14b' }}>
      <Body>
        <Text style={{ marginLeft: 10, color: 'white' }}>{props.title}</Text>
      </Body>
    </Header>
  );
}

function CustomEditButton(props) {
  let { onPress } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.goBackHomeButton,
        { width: null, height: null, paddingHorizontal: 5 },
      ]}>
      <Text style={{ color: '#00a14b', fontSize: 12 }}>Edit</Text>
    </TouchableOpacity>
  );
}

function Address(props) {
  let { type, data, onPressEdit, navigation, noAddress } = props;

  return (
    <View>
      {data === null ? (
        <TouchableOpacity
          onPress={() =>
            noAddress ? navigation.navigate('Add Address') : onPressEdit(type)
          }
          style={checkoutStyles.addAddress}>
          <Text style={[checkoutStyles.grayText, { paddingHorizontal: 20 }]}>
            + {noAddress ? 'Add ' : 'Choose '}
            {type === 'Ship' ? 'shipping' : 'billing'} address
          </Text>
        </TouchableOpacity>
      ) : (
        <View style={{ padding: 20 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={checkoutStyles.grayText}>{type} to</Text>
            <CustomEditButton onPress={() => onPressEdit(type)} />
          </View>
          <Text style={checkoutStyles.addresNameText}>{data.name}</Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.address}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.postcode}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.country}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.phonenumber}
          </Text>
        </View>
      )}
    </View>
  );
}

export default function CheckoutScreen(navigation) {
  const [selectedBillingAddress, setselectedBillingAddress] = useState(null);
  const [selectedShippingAddress, setselectedShippingAddress] = useState(null);
  const [isAddressModalVisible, setAddressModalVisibility] = useState(null);
  const [isAddressSame, setSameAddress] = useState(false);
  const [addresses, setAddresses] = useState([
    {
      name: 'Millet, Othan Binalay',
      address: 'Maramba Boulevard, Poblacion',
      postcode: 'PANGASINAN - LINGAYEN (CAPITAL) - POBLACION',
      phonenumber: '09369410878',
      country: 'Philippines',
    },
    {
      name: 'Altezo, Bryan',
      address: 'Maramba Boulevard, Poblacion',
      postcode: 'PANGASINAN - LINGAYEN (CAPITAL) - POBLACION',
      phonenumber: '09123456789',
      country: 'Philippines',
    },
  ]);
  const [addressToEdit, setAddressToEdit] = useState('');
  const [selectedPaymentOption, setPaymentOption] = useState('');

  const handleAddress = (item) => {
    addressToEdit === 'Ship'
      ? setselectedShippingAddress(item)
      : setselectedBillingAddress(item);
    isAddressSame &&
      setselectedBillingAddress(item) &&
      setselectedShippingAddress(item);
    setAddressModalVisibility(false);
  };

  const handleSameAddress = () => {
    setSameAddress(!isAddressSame);
    setTimeout(() => {
      if (isAddressSame) {
        if (addressToEdit === 'Ship' && selectedShippingAddress !== null) {
          setselectedShippingAddress(selectedShippingAddress);
        } else if (
          addressToEdit === 'Bill' &&
          selectedBillingAddress !== null
        ) {
          setselectedBillingAddress(selectedShippingAddress);
        }
      }
    }, 1000);
  };

  const renderAddress = ({ item, index }) => {
    let data = item;
    return (
      <TouchableOpacity
        style={{ padding: 10 }}
        onPress={() => handleAddress(item)}>
        <Body>
          <Text style={checkoutStyles.addresNameText}>{data.name}</Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.address}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.postcode}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.country}
          </Text>
          <Text style={[checkoutStyles.grayText, { fontSize: 13 }]}>
            {data.phonenumber}
          </Text>
        </Body>
      </TouchableOpacity>
    );
  };

  const renderAddressModal = () => {
    return (
      <Modal
        style={{ flex: 1 }}
        animationType="fade"
        transparent={true}
        visible={isAddressModalVisible}
        onRequestClose={() => setAddressModalVisibility(false)}>
        <TouchableOpacity
          onPress={() => setAddressModalVisibility(false)}
          style={checkoutStyles.modalBackgroundContainer}>
          <TouchableWithoutFeedback>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 5,
                width: width - 40,
                padding: 10,
              }}>
              <Text style={[checkoutStyles.grayText, { textAlign: 'center' }]}>
                Select Address
              </Text>
              <FlatList
                data={addresses}
                renderItem={renderAddress}
                extraData={addresses}
              />
              <ListItem>
                <CheckBox
                  checked={isAddressSame}
                  color="green"
                  onPress={handleSameAddress}
                />
                <Body>
                  <Text style={[checkoutStyles.grayText, { paddingLeft: 10 }]}>
                    Set same shipping and billing address
                  </Text>
                </Body>
              </ListItem>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    );
  };

  const renderPaymentOptions = ({ item }) => {
    let { option, icon } = item;
    return (
      <TouchableOpacity
        disabled={option === 'referapps'}
        onPress={() => setPaymentOption(option)}
        style={[
          { opacity: option === 'referapps' ? 0.5 : 1 },
          checkoutStyles.optionStyle,
          selectedPaymentOption === option &&
          checkoutStyles.selectedOptionStyle,
        ]}>
        <Thumbnail
          source={icon}
          style={{
            height: width / 6,
            resizeMode: 'contain',
            tintColor: selectedPaymentOption === option ? 'white' : null,
          }}
        />
      </TouchableOpacity>
    );
  };

  const _onChange = (form) => {
    const accepted = ['visa', 'master-card', 'american-express'];
    console.log(form);
    !accepted.includes(form.values.type) &&
      Alert.alert(
        'Error',
        'We only accept Visa, Mastercard and American Express.',
      );
  };

  const renderPaymentDetails = () => {
    let index = PAYMENT_OPTIONS.findIndex(
      (i) => i.option === selectedPaymentOption,
    );
    let data = PAYMENT_OPTIONS[index];
    switch (selectedPaymentOption) {
      case 'card':
        return (
          <View style={{ paddingBottom: 10 }}>
            <Text style={[checkoutStyles.grayText, { margin: 10, fontSize: 11 }]}>
              NOTE: We only accept Visa, Mastercard and American Express.
            </Text>
            <CreditCardInput
              onChange={_onChange}
              cardScale={0.75}
              labelStyle={checkoutStyles.addresNameText}
            />
          </View>
        );
      default:
        return (
          <View style={{ padding: 10 }}>
            <Text style={checkoutStyles.addresNameText}>{data.header}</Text>
            <Text style={[checkoutStyles.grayText, { marginVertical: 10 }]}>
              {data.description}
            </Text>
          </View>
        );
    }
  };

  const handleCheckout = (form) => {
    const accepted = ['visa', 'master-card', 'american-express'];
    console.log(form);
    !accepted.includes(form.values.type) &&
      Alert.alert(
        'Error',
        'We only accept Visa, Mastercard and American Express.',
      );
  };

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={styles.headerStyle}>
        {isAddressModalVisible && renderAddressModal()}
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.headerText}>Checkout</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>
      <Content>
        <CustomHeader title="Shipping & Billing" />
        <Address
          noAddress={addresses.length === 0}
          navigation={navigation.navigation}
          onPressEdit={(toEdit) => {
            setAddressModalVisibility(true);
            setAddressToEdit(toEdit);
          }}
          data={selectedShippingAddress}
          type="Ship"
        />
        <Address
          noAddress={addresses.length === 0}
          navigation={navigation.navigation}
          onPressEdit={(toEdit) => {
            setAddressModalVisibility(true);
            setAddressToEdit(toEdit);
          }}
          data={selectedBillingAddress}
          type="Bill"
        />
        <CustomHeader title="Payment" />
        <FlatList
          data={PAYMENT_OPTIONS}
          renderItem={renderPaymentOptions}
          horizontal
          extraData={selectedPaymentOption}
        />
        {selectedPaymentOption !== '' && renderPaymentDetails()}
      </Content>
      <Footer style={styles.footer2}>
        <FooterTab style={styles.footerTab2}>
          <TouchableOpacity
            // disabled={
            //   selectedBillingAddress === '' ||
            //   selectedShippingAddress === '' ||
            //   selectedPaymentOption === ''
            // }
            onPress={() => {
              handleCheckout();
              navigation.navigation.navigate('PaypalCheckout', navigation.route.params.url)
            }}
            style={[
              { width: width - 40 },
              styles.btnCheckout,
              // {
              //   opacity: selectedBillingAddress === '' ||
              //   selectedShippingAddress === '' ||
              //   selectedPaymentOption === '' ? 0.5 : 1
              // }
            ]}>
            <Text style={styles.btnTextCheckout}>Proceed</Text>
          </TouchableOpacity>
        </FooterTab>
      </Footer>
    </Container>
  );
}

const checkoutStyles = StyleSheet.create({
  headerText: {
    fontSize: 20,
    alignSelf: 'center',
  },
  addAddress: {
    width: width,
    borderWidth: 0.7,
    borderColor: '#d0dbdb',
    paddingVertical: 20,
  },
  grayText: {
    fontSize: 14,
    color: 'gray',
  },
  addresNameText: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
  },
  modalBackgroundContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  optionStyle: {
    width: width / 4 - 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    margin: 5,
    borderWidth: 1,
    borderColor: '#00a14b',
  },
  selectedOptionStyle: {
    backgroundColor: '#579847',
  },
});
