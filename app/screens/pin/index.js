import React, {
  useState,
  useEffect,
} from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View, Text, TouchableOpacity, Image, StyleSheet, SafeAreaView, Dimensions, RefreshControl, Alert, } from 'react-native';
import CustomHeader from '../../components/header';
import Assets from '../../components/assets.manager.js';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../components/api/service'
import { ScrollView } from 'react-native-gesture-handler';
import { FlatGrid } from 'react-native-super-grid';
import Icons from '../../components/icons'
import NetInfo from '@react-native-community/netinfo';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image'

export default function HomeScreen(navigation) {
  const api_url = 'https://www.referapps.com';
  const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
  const pinned = '/api/pin/all.json';
  const [loading, setloading] = useState(false)
  const [pinnedItems, setPinnedItems] = useState([])

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  const init = async () => {
    console.log("~~~~~~~~~~~~~~~~~~~UPDATE~~~~~~~~~~~~~~~~~~~");
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        console.log('loading Pinned items');
        loadPinnedItems();
      } else {
        console.log('error loading Pinned items', state);
        setloading(false);
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
    setloading(false);
  }

  const loadPinnedItems = async () => {
    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = ud ? JSON.parse(ud) : null
    const res = await fetch(api_url + pinned + `?user_id=` + _userdata.data.id + `&include=info,date&page=1&per_page=10&api_token=${token}`, {
      method: 'get',
      headers: {
        "content-type": "application/json",
      }
    })
    let response = await res.json()
    let datas = []
    response.data.map(async (result, index) => {
      // console.warn('pinsss', response)
      datas.push({
        data: {
          index: index,
          pin_id: result.id,
          product_id: result.product_id,
          product_name: result.info.data.product_info.product_name,
          price: result.info.data.product_info.price,
          country: result.info.data.product_info.country_code,
          rating: result.ratings,
          username: result.info.data.product_info.user_name,
          user_id: result.info.data.product_info.user_id,
        },
        uri: result.info.data.product_info.image.full_path,
        has_morepages: result.has_morepages,
        renderFooter: (data) => {
          return (
            <View style={{
              backgroundColor: '#ffffff',
              borderBottomLeftRadius: 10,
              borderBottomRightRadius: 10,
              paddingBottom: 5,
            }}>
              <View style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingLeft: 5,
                paddingRight: 5,
              }}>
                <View style={{ flex: 1, justifyContent: 'space-around' }}>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="tail"
                    style={{
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: '#231f20',
                    }}
                  >{data.username}</Text>
                </View>

                <View style={{ flex: 0 }}>

                </View>
              </View>
              <Text
                style={{
                  fontFamily: 'Roboto',
                  paddingLeft: 2,
                  paddingTop: 2,
                  color: '#231f20',
                  fontSize: 14,
                }}
                numberOfLines={1}
                ellipsizeMode='tail'
              >{data.product_name}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View style={{ alignSelf: 'flex-start' }}>
                  <Text style={{
                    fontFamily: 'Roboto',
                    fontSize: 10,
                    paddingLeft: 2,
                    color: '#ea4123',
                  }}>
                    {data.country === 'PH' && '₱'}{data.country === 'US' && '$'} {data.price}
                  </Text>
                </View>
                <View style={{ alignSelf: 'flex-end' }}>
                  {data.rating >= 0 ? <StarRating
                    disabled={true}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={data.rating}
                    fullStarColor={'#f36e23'}
                    halfStarColor={'#f36e23'}
                    starSize={10}
                    style={{ justifyContent: 'flex-end' }}
                  /> : <Text style={{
                    fontFamily: 'Roboto',
                    color: '#7f8c8d',
                    fontSize: 10,
                  }}>({data.rating})</Text>}
                </View>
              </View>
            </View>
          )
        },
      })
    })
    setPinnedItems(datas)
    console.log('pinned items', pinnedItems)
    setloading(false)
  }

  function handleNoPinnedItems() {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
          <View style={styles.contentCon}>
            <Text style={styles.txtContent}>No pinned items</Text>
          </View>
        </View>
      </View>
    )
  }

  function handlePinnedItems() {
    return (
      <SafeAreaView style={styles.safeContainer}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={loading}
              onRefresh={() => {
                NetInfo.fetch().then((state) => {
                  if (state.isConnected) {
                    loadPinnedItems();
                  } else {
                    setloading(false);
                    Alert.alert('Not Connected', 'You have no internet connection');
                  }
                });
              }}
            />
          }
        >
          <FlatGrid
            itemDimension={150}
            data={pinnedItems}
            style={styles.gridView}
            // staticDimension={300}
            // fixed
            spacing={10}
            renderItem={({ item }) => (
              <TouchableOpacity
                onPress={() => navigation.navigation.navigate('ProductDetails', { product_id: item.data.product_id })}
              >
                <View style={[styles.itemContainer, { backgroundColor: '#fff' }]}>
                  <View style={{ flexDirection: 'column' }}>
                    <FastImage source={{ uri: item.uri }} style={{ height: 175, resizeMode: 'cover' }} resizeMode={FastImage.resizeMode.cover} />
                  </View>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={styles.itemName}>{item.data.product_name}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={styles.itemCode}>{item.data.price}</Text>
                    {
                      item.data.rating == 'No ratings yet' ? (
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
                          <Text numberOfLines={1} ellipsizeMode='tail' style={{ fontSize: 9, color: '#000', alignItems: 'center' }}>{item.data.rating}</Text>
                        </View>
                      )
                        :
                        (
                          <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                            {[1, 2, 3, 4, 5].map((rate, i) => {
                              return (
                                <Icons.AntDesign key={i} name={item.data.rating >= rate ? 'star' : 'staro'} size={10} color='orange' />
                              );
                            })}
                          </View>
                        )
                    }
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </ScrollView>
      </SafeAreaView>
    )
  }

  function handleCheckPinnedItems() {
    return (
      <View>
        {
          pinnedItems != null || pinnedItems != '' ? handlePinnedItems() : handleNoPinnedItems()
        }
      </View>
    )
  }

  return (
    <View>
      <CustomHeader title="Pinned Items" navigation={navigation} />
      <View>
        {handleCheckPinnedItems()}
      </View>
      <TouchableOpacity onPress={() => {
        Alert.alert('Coming Soon!', 'This feature will be available soon.')
        // navigation.navigation.navigate('Messages')
      }} style={styles.chatCon}>
        <FastImage source={Assets.myProfile.chatIcon} style={styles.imgChat} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#FFF'
  },
  txtHeader: {
    textAlign: 'center',
    alignSelf: 'center',
    // fontFamily:'Roboto',
    fontSize: 18
  },
  contentCon:
  {
    flex: 1,
    justifyContent: 'center',
    paddingBottom: 0
  },
  txtContent: {
    textAlign: 'center',
    // fontFamily:'Roboto',
    fontSize: 13,
    color: '#7f8c8d'
  },
  chatCon: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    elevation: 5,
    height: 50,
    width: 50,
    margin: 20,
    borderRadius: 50,
    backgroundColor: '#00a14b',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  imgChat: {
    height: 20,
    width: 20,
    tintColor: '#FFF'
  },
  tinyLogo: {
    height: 100,
    width: 100
  },
  gridView: {
    // marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    paddingVertical: 10,
    // height: 230,
    elevation: 5,
    paddingHorizontal: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
  },
  itemName: {
    fontSize: 14,
    color: '#000',
    fontWeight: '400',
    paddingTop: 5
  },
  itemCode: {
    fontWeight: '400',
    fontSize: 12,
    color: '#f00',
    flex: 1,
    alignSelf: 'flex-start',
  },
  safeContainer: {
    height: hp('80%'),
    // height: Dimensions.get('window').height - 0,
    // marginTop: 100,
    // marginBottom: 200
  }
})