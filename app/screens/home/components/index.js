import * as React from 'react';
import { View, TouchableOpacity, Dimensions, Image, FlatList, SafeAreaView } from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import GridView from 'react-native-super-grid';
import { SectionGrid, FlatGrid } from 'react-native-super-grid';
import Icons from '../../../components/icons';
import Assets from '../../../components/assets.manager';
import { Container, Header, Content, List, ListItem, Left, Right, Icon, Text } from 'native-base';

const Ads = ({ data, ref }) => {
  if (!data.length > 0) return false;
  let newData = {
    type: 'video',
    url: 'https://youtu.be/ue6mlx6JX3Y'
  }
  // data = [...data, newData]
  return (
    <Carousel
      layout={'default'}
      // ref={ref}
      data={data}
      enableSnap={true}
      loop={true}
      autoplay={true}
      enableMomentum={false}
      lockScrollWhileSnapping={true}
      renderItem={(item) => {
        // console.log("item.item?.type", item.item?.type);
        if (item.item?.type === 'video') {
          // 
          return (
            <TouchableOpacity activeOpacity={1} style={{ width: Dimensions.get('window').width }}>
              <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
                <Text>
                  Youtube Teaser here!
                </Text>
              </View>
            </TouchableOpacity>
          );
        }
        return (
          <TouchableOpacity activeOpacity={1} style={{ width: Dimensions.get('window').width }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffffff' }}>
              <FastImage
                style={{ width: '100%', height: 150 }}
                source={{
                  uri: item.item.directory + '/' + item.item.filename,
                  headers: { Authorization: 'someAuthToken' },
                  priority: FastImage.priority.normal,
                }}
              // resizeMode={FastImage.resizeMode.contain}
              />
            </View>
          </TouchableOpacity>
        );
      }}
      sliderWidth={Dimensions.get('window').width}
      itemWidth={Dimensions.get('window').width}
    />
  );
};

const Categories = ({ categories, navigate }) => {
  if (!categories.length > 0) return false;
  return (
    <GridView
      data={categories}
      horizontal={true}
      scrollEnabled={true}
      showsHorizontalScrollIndicator={false}
      renderItem={(data) => {
        const uri = data.item.directory + '/' + data.item.filename;
        return (
          <TouchableOpacity
            onPress={() => {
              navigate('Search Results', { categorize_by: data.item.id });
            }}
            style={{ alignContent: 'center', width: 70, height: 90 }}
          >
            <FastImage
              style={{ width: 50, height: 50, borderRadius: 30, alignSelf: 'center' }}
              source={{
                uri: uri,
                headers: { Authorization: 'someAuthToken' },
                priority: FastImage.priority.normal,
              }}
            // // resizeMode={FastImage.resizeMode.contain}
            />
            <View style={{ paddingTop: 10 }} />
            <Text style={{ fontSize: 12, color: '#231f20', alignSelf: 'center', textAlign: 'center' }}>
              {data.item.category_name}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const PopularItems = () => {
  if (!popularItems.length > 0) return false;
  return (
    <GridView
      data={popularItems}
      horizontal={true}
      scrollEnabled={true}
      showsHorizontalScrollIndicator={false}
      renderItem={(data) => {
        const uri = data.item.info.data.image.directory + '/' + data.item.info.data.image.filename;
        return (
          <TouchableOpacity
            onPress={() => navigation.navigation.navigate('ProductDetails', { product_id: data.item.id })}
            style={{ alignContent: 'center', width: 135, height: 150, backgroundColor: '#fff' }}
          >
            <FastImage
              style={{ width: 135, height: 150, borderRadius: 5, alignSelf: 'center', backgroundColor: '#fff' }}
              source={{
                uri: uri,
                headers: { Authorization: 'someAuthToken' },
                priority: FastImage.priority.normal,
              }}
            // // resizeMode={FastImage.resizeMode.contain}
            />
            <View style={{ paddingTop: 10, backgroundColor: '#fff' }} />
            <Text
              numberOfLines={2}
              style={{
                fontSize: 12,
                padding: 5,
                width: '100%',
                color: '#231f20',
                alignSelf: 'flex-start',
                textAlign: 'left',
                backgroundColor: '#fff',
              }}
            >
              {data.item.product_name}
            </Text>
            <View style={{ flexDirection: 'row', padding: 5, top: 0, paddingTop: 5, backgroundColor: '#fff' }}>
              <Text
                style={{ flex: 1, alignSelf: 'flex-start', fontSize: 13, color: 'red', backgroundColor: '#fff' }}
              >{`\u20B1 ${data.item.price}`}</Text>
              {[1, 2, 3, 4, 5].map((rate, i) => {
                return (
                  <Icons.AntDesign
                    name={parseInt(data.item.ratings) >= rate ? 'star' : 'staro'}
                    size={10}
                    color='orange'
                  />
                );
              })}
            </View>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const AllItems = () => {
  if (!allItems.length > 0) return false;
  return (
    <GridView
      data={allItems}
      vertical={true}
      horizontal={false}
      scrollEnabled={true}
      showsVerticalScrollIndicator={false}
      renderItem={(data) => {
        const uri = data.item.info.data.image.directory + '/' + data.item.info.data.image.filename;
        return (
          <TouchableOpacity
            onPress={() => navigation.navigation.navigate('Search Results', { categorize_by: data.item.id })}
            style={{ alignContent: 'center', width: 150, height: 200, marginTop: 15, backgroundColor: '#fff' }}
          >
            <FastImage source={{ uri: uri } || Thumb}
              style={{
                // resizeMode: 'stretch',
                width: '100%',
                height: 150
              }} />
            <Text
              numberOfLines={2}
              style={{
                fontSize: 12,
                padding: 5,
                width: '100%',
                color: '#231f20',
                alignSelf: 'flex-start',
                textAlign: 'left',
                backgroundColor: '#fff',
              }}
            >
              {data.item.product_name}
            </Text>
            <View style={{ flexDirection: 'row', padding: 5, top: 0, paddingTop: 5, backgroundColor: '#fff' }}>
              <Text style={{ flex: 1, alignSelf: 'flex-start', fontSize: 13, color: 'red' }}>{`\u20B1 ${data.item
                .price}`}</Text>
              {[1, 2, 3, 4, 5].map((rate, i) => {
                return (
                  <Icons.AntDesign
                    name={parseInt(data.item.ratings) >= rate ? 'star' : 'staro'}
                    size={10}
                    color='orange'
                  />
                );
              })}
            </View>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const PopularItemsx = ({ data, navigate }) => {
  // console.log('popularity item', data)
  return data.map((item, i) => {
    item.image = item.info.data.image.directory + '/' + item.info.data.image.filename;
    return <Card item={item} navigate={navigate} key={i} />;
  });
};

const AllItemsx = ({ items, navigate }) => {
  // console.log('all items', items)
  for (var x = 0; x < items.length; x++) {
    items[x].image = items[x].info.data.image.directory + '/' + items[x].info.data.image.filename;
  }
  return (
    <FlatGrid
      itemDimension={130}
      numColumns={2}
      data={items}
      style={{ flex: 1 }}
      spacing={10}
      renderItem={({ item }) => <Card item={item} navigate={navigate} />}
    />
  );
};

const Card = ({ item, navigate }) => {
  // console.log(item);
  return (
    <TouchableOpacity
      onPress={() => navigate('ProductDetails', { product_id: item.id })}
      style={{ flex: 1, backgroundColor: '#fff', width: 160, margin: 5 }}
    >
      <FastImage
        source={item.image != '' || item.image != null ? { uri: item.image } : Assets.myProfile.defProductImg}
        style={{ width: '100%', height: 150 }}
      />
      <View
        style={{
          padding: 10,
          bottom: 2,
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'flex-start',
        }}
      >
        <Text numberOfLines={1} style={{ fontSize: 13 }}>
          {item.product_name}
        </Text>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Text style={{ flex: 1, color: 'red', fontSize: 12 }}>{`\u20B1 ${item.price}`}</Text>
          {item.ratings == 'No ratings yet' ? (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
              <Text style={{ fontSize: 9 }}>{item.ratings}</Text>
            </View>
          ) : (
            <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
              {[1, 2, 3, 4, 5].map((rate, i) => {
                return (
                  <Icons.AntDesign key={i} name={item.ratings >= rate ? 'star' : 'staro'} size={10} color='orange' />
                );
              })}
            </View>
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default {
  Ads: Ads,
  Categories: Categories,
  PopularItems,
  AllItems: AllItems,
  PopularItemsx: PopularItemsx,
  AllItemsx: AllItemsx,
};
