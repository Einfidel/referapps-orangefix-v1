import * as React from 'react';
import { useState, useEffect, useRef } from 'react';
import { SafeAreaView, ScrollView, View, Text, RefreshControl, Linking, Alert, Dimensions, TouchableOpacity } from 'react-native';
import CustomHeader from '../../components/header';
import messaging from '@react-native-firebase/messaging';
import FastImage from 'react-native-fast-image';
import Service from '../../components/api/service';
import Carousel from 'react-native-snap-carousel';
import GridView from 'react-native-super-grid';
import Icons from '../../components/icons';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import CustomComponent from './components/index';
import { Left, Right, Icon, Grid, Col, Row, Content, Spinner } from 'native-base';
import Modal from 'react-native-root-modal';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';
import Notifications from '../../components/Notifications';
import { call } from 'react-native-reanimated';
import { useFocusEffect } from '@react-navigation/native';

const Thumb = {
  uri:
    'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8Xw8AAoMBgDTD2qgAAAAASUVORK5CYII=',
};

const styles = {
  modalLoading: {
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .3)',
  },
  processingContainer: {
    backgroundColor: '#231f20',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: 160,
    height: 160,
    borderRadius: 30,
  },
  txtProcessing: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
  },
};

export default function HomeScreen(navigation) {
  const [loading, setloading] = useState(false);
  const [resources, setresources] = useState(true);
  const carouserRef = useRef(null);
  const [slider, setslider] = useState([]);
  const [categories, setcategories] = useState([]);
  const [popularItems, setPopularItems] = useState([]);
  const [allItems, setAllItems] = useState([]);

  const [page, setpage] = useState(1)
  const [pagecontent, setpagecontent] = useState(10)
  const [bottomTouch, setbottomtouch] = useState(false)

  const init = async () => {
    await setloading(true);
    await loadResources();
    try {
      await loadPopularProducts()
    } catch (err) {
      await loadPopularProducts()
    }
    try {
      await loadInitialProducts()
    } catch (err) {
      await loadInitialProducts()
    }
    await setloading(false);
    await setbottomtouch(false);
    await setresources(false);
  }

  const preinit = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        requestUserPermission();
        init()
      } else {
        setloading(false);
        Alert.alert('Not Connected', 'You have no internet connection');
      }
    });
  }

  const loadResources = async () => {
    setresources(true);
    try {
      await Service.ads((res) => setslider(res.data), (err) => console.log(err));
      await Service.categories((res) => setcategories(res.data), (err) => console.log(err));
      await setresources(false);
    } catch (error) {
      console.log("loadResources", error)
    }
  }

  const loadPopularProducts = async () => {
    try {
      await Service.popularProducts(
        'popularity', 1,
        (res) => {
          setPopularItems(res.data)
        }, (err) => {
          console.log(err)
        });
    } catch (error) {
      console.log("loadPopularProducts", error)
    }
  }

  const loadInitialProducts = async () => {
    try {
      await Service.products(page, pagecontent, (res) => setAllItems(res.data), (err) => console.log(err));
    } catch (error) {
      console.log("loadInitialProducts", error)
    }
  }

  const loadProducts = async () => {
    try {
      await Service.products(page, pagecontent, (res) => {
        console.log("PRoducts", res.data)
        let items = allItems
        for (var x = 0; x < res.data.length; x++) {
          items.push(res.data[x])
        }
        setAllItems(items)
      }, (err) => {
        setpagecontent(10)
        console.log(err)
      });
      await setbottomtouch(false);
    } catch (error) {
      console.log("loadProducts", error)
    }

  }

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  };

  const handleOpenURL = (event) => {
    // D
    navigate(event);
  };

  const navigate = async (url) => {
    // E
    const { navigate } = navigation.navigation;
    const route = url.replace(/.*?:\/\//g, '');
    // const id = route.match(/\/([^\/]+)\/?$/)[1];
    const routeName = route.split('/')[0];
    const id = route.split('/')[1];
    const referrer = route.split('/')[2];
    let referState = await AsyncStorage.getItem('app_refer_state');
    console.log('REFERSTATE', referState, routeName);
    // alert('TRIGGERED 1');
    if (routeName === 'referral' && !referState) {
      // alert('TRIGGERED 2');
      await AsyncStorage.setItem('app_refer_state', '1');
      navigate('ProductDetails', { product_id: id, referrer: referrer, referState: true });
    }
  };

  useEffect(() => {
    Linking.getInitialURL()
      .then((url) => {
        if (url) {
          console.warn('Initial url is: ' + url);
          navigate(url);
        }
      })
      .catch((err) => console.error('An error occurred', err));
    Linking.addEventListener('url', handleOpenURL);
    preinit()
    return () => {
      Linking.removeEventListener('url', handleOpenURL);
    };
  }, []);

  const registerForNotification = async () => {
    if (!messaging().isDeviceRegisteredForRemoteMessages) {
      await messaging().registerDeviceForRemoteMessages();
      getToken(async (token) => {
        let ud = JSON.parse(await AsyncStorage.getItem('user_data'));
        if (token) {
          await AsyncStorage.setItem('fcmtoken', token);
          console.warn('Token', token);
          Notifications.registerToken(token, ud.data.id);
        }
      });
    }
  };

  const getAuth = async (callback) => {
    let auth = await AsyncStorage.getItem('auth');
    callback(auth);
  };

  useEffect(() => {
    // getAuth((auth) => {
    //   auth & registerForNotification();
    // });
    // const unsubscribe = messaging().onMessage(async (remoteMessage) => {
    //   //save chats to local storage
    //   let { data } = remoteMessage;
    //   let chats = await AsyncStorage.getItem(data.messageType),
    //     auth = await AsyncStorage.getItem('auth');
    //   if (auth) {
    //     if (!chats) {
    //       await AsyncStorage.setItem(
    //         data.messageType,
    //         JSON.stringify([remoteMessage]),
    //       );
    //     } else {
    //       chats = JSON.parse(chats);
    //       chats.push(remoteMessage);
    //       await AsyncStorage.setItem(data.messageType, JSON.stringify(chats));
    //     }
    //   }
    // });
    // messaging().onNotificationOpenedApp(async (remoteMessage) => {
    //   // console.log('Message', JSON.stringify(remoteMessage, null, '\t'));
    //   navigation.navigate('ChattScreen', { data: remoteMessage });
    // });
    // return unsubscribe;
  }, []);

  const getToken = async (callback) => {
    let token = await messaging().getToken();
    console.log('Token', token);
    callback(token);
  };

  const renderPopularItems = () => {
    return null;
  };

  const renderAllItems = () => {
    return null;
  };

  return (
    <SafeAreaView style={{}}>
      <CustomHeader title='ReferApps' home_header={true} navigation={navigation} />
      {!resources ? (
        <Modal visible={resources} style={styles.modalLoading}>
          <View style={styles.processingContainer}>
            <MaterialIndicator color={'#ffffff'} size={70} />
            <Text style={styles.txtProcessing}>Loading resources...</Text>
          </View>
        </Modal>
      ) : null}
      <ScrollView
        showsVerticalScrollIndicator={false}
        onScrollToTop={() => setbottomtouch(false)}
        onScroll={(e) => {
          var windowHeight = Dimensions.get('window').height,
            height = e.nativeEvent.contentSize.height,
            offset = e.nativeEvent.contentOffset.y;
          if (windowHeight + offset >= height && allItems.length > 0) {
            let newpagecontent = pagecontent
            if (!bottomTouch) {
              if (pagecontent < 30) {
                newpagecontent += 5
                setpagecontent(newpagecontent)
                setbottomtouch(true)
                loadProducts()
                console.log('Will Load New Items: ', newpagecontent)
              }
            }
          }
        }}
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={() => {
              preinit()
            }}
          />
        }
      >
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          {/* {renderSlider()} */}

          {/* Warning: Function components cannot be given refs. Attempts to access this ref will fail. Did you mean to use React.forwardRef()? */}
          {/* <CustomComponent.Ads data={slider || []} ref={carouserRef} /> */}
          <CustomComponent.Ads data={slider || []} />

          <View style={{ backgroundColor: '#fff', width: '100%', paddingHorizontal: 15, paddingVertical: 0 }}>
            <Text style={{ fontSize: 14, alignSelf: 'flex-start', fontWeight: 'bold', color: 'darkorange' }}>
              CATEGORIES
            </Text>
          </View>
          <ScrollView
            horizontal={true}
            style={{ height: 110, backgroundColor: '#fff' }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            {/* {renderCategories()} */}
            <CustomComponent.Categories categories={categories || []} navigate={navigation.navigation.navigate} />
          </ScrollView>
          <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <Left>
              <Text style={{ flex: 1, fontSize: 14, alignSelf: 'flex-start', fontWeight: 'bold', color: 'darkorange' }}>
                POPULAR ITEMS
              </Text>
            </Left>
            <Right>
              <TouchableOpacity
                onPress={() => navigation.navigation.navigate('Search Results', { sort_by: 'best_seller' })}
                style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ flex: 3 }} />
                <Text style={{ flex: 2, color: 'darkorange' }}>See more</Text>
                <Icons.AntDesign name='right' size={15} color='darkorange' style={{ paddingTop: 2.5 }} />
              </TouchableOpacity>
            </Right>
          </View>
          <ScrollView
            horizontal={true}
            style={{ height: 230 }}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
          >
            {/* {renderPopularItems()} */}

            <CustomComponent.PopularItemsx data={popularItems || []} navigate={navigation.navigation.navigate} />
          </ScrollView>
          <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 15, paddingVertical: 10 }}>
            <Text style={{ flex: 1, fontSize: 14, alignSelf: 'flex-start', fontWeight: 'bold', color: 'darkorange' }}>
              ALL ITEMS
            </Text>
          </View>
          {/* {renderAllItems()} */}
          <ScrollView
            vertical={true}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ justifyContent: 'flex-start', flexDirection: 'column' }}
          >
            <CustomComponent.AllItemsx items={allItems || []} navigate={navigation.navigation.navigate} />
          </ScrollView>
          {bottomTouch ? <Spinner size="small" /> : null}
          <View style={{ height: 120 }} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
