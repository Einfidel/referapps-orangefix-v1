

import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  FlatList,
  TextInput,
} from 'react-native';

// NativeBase
import {
  Header,
  Left,
  Body,
  Right,
  Thumbnail,
  Container,
  Content,
  Footer,
  FooterTab
} from 'native-base';

import {
  PulseIndicator,
} from 'react-native-indicators';


// import { connect } from 'react-redux';
import * as Animatable from 'react-native-animatable';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-root-modal';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';

import FlatListComponent from '../../components/flatlist';
import RenderItem from '../../components/renderItem';
import styles from '../../styles/home/searchStyle'
import Assets from '../../components/assets.manager'

const { width, height } = Dimensions.get('window')

const api_token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
const products_api = 'https://www.referapps.com/api/product/all.json';
const categories_api = 'https://www.referapps.com/api/product/main-category.json';

const Latest = () => (
  <View />
);

const Price = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);


const BestSeller = () => (
  <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const initialLayout = { width: Dimensions.get('window').width };

class SearchResultsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);

    // alert(JSON.stringify(this.props));

    this.state = {

      index: 0,
      routes: [
        { key: 'latest', title: 'Latest' },
        { key: 'price', title: 'Price(Low-High)' },
        { key: 'bestseller', title: 'Best Seller' },
      ],

      products: [],
      productsLoaded: true,
      hasFiltered: false,
      hasMorePage: false,
      isLoadingMore: false,
      isRefreshing: false,

      //   search: this.props.route.params.search,
      search: "",

      sortModal: false,
      sort_by: null,
      initial_sort_by: null,

      priceSortModal: false,
      priceSort: null,
      initial_priceSort: null,

      categorize_by: null,
      categories: [],

      condition_by: null,
      condition_options: [
        {
          name: 'New',
          id: 0,
        },
        {
          name: 'Used',
          id: 1,
        },
      ],

      min_Price: null,
      max_Price: null,

      rated_by: null,
      rating_options: [
        {
          name: '5 stars',
          id: 5,
        },
        {
          name: '4 stars and up',
          id: 4,
        },
        {
          name: '3 stars and up',
          id: 3,
        },
        {
          name: '2 stars and up',
          id: 2,
        },
        {
          name: '1 star and up',
          id: 1,
        },
        {
          name: '0 star',
          id: 0,
        },
      ],

      filterOpen: false,
    };

    this.page = 1;
  }

  componentDidMount = async () => {
    if (this.props.route.params.sort_by) {
      await this.setState(
        {
          sort_by: this.props.route.params.sort_by,
        },
        () => {
          console.log('sort_by: ' + this.state.sort_by);
        }
      );
    }
    if (this.props.route.params.categorize_by) {
      await this.setState(
        {
          hasFiltered: true,
          categorize_by: this.props.route.params.categorize_by,
        },
        () => {
          console.log('categorize_by: ' + this.state.categorize_by);
        }
      );
    }
    this._fetchCategories();
    this._fetchProducts();
  };

  _fetchCategories = async () => {
    try {
      let formData = new FormData();

      formData.append('page', '1');
      formData.append('per_page', '0');
      formData.append('api_token', api_token);

      const data = await fetch(categories_api, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      });

      const response = await data.json();

      this.setState({
        categories: response.data,
      });
    } catch (e) {
      console.warn(e);
    }
  };

  _fetchProducts = async () => {
    try {
      this.page = 1;
      this.setState({
        productsLoaded: false,
      });
      let formData = new FormData();

      if (this.state.search) {
        formData.append('search', this.state.search);
      }
      if (this.state.sort_by) {
        formData.append('sort_by', this.state.sort_by);
      }
      formData.append('page', this.page);
      formData.append('include', 'info');
      formData.append('per_page', '10');
      if (this.state.hasFiltered) {
        if (this.state.condition_by) {
          formData.append('condition', this.state.condition_by);
          console.log('condition : ' + this.state.condition_by);
        }
        if (this.state.categorize_by) {
          formData.append('categories', this.state.categorize_by);
          console.log('categories : ' + this.state.categorize_by);
        }
        if (this.state.min_Price) {
          formData.append('min', this.state.min_Price);
          console.log('min : ' + this.state.min_Price);
        }
        if (this.state.max_Price) {
          formData.append('max', this.state.max_Price);
          console.log('max : ' + this.state.max_Price);
        }
        if (this.state.rated_by) {
          formData.append('rating', this.state.rated_by);
          console.log('rating : ' + this.state.rated_by);
        }
      }
      formData.append('api_token', api_token);

      console.log("formData", formData);
      console.log("search", this.state.search);
      let data = '';

      try {
        data = await fetch(products_api, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
          },
          body: formData,
        });
      } catch (error) {
        console.warn("fetch error", error);
      }

      console.log("data._bodyBlob._data.__collector", data._bodyBlob._data.__collector);
      const response = await data.json();
      console.log("data.json()", data.json());

      var products = [...response.data];

      if (products.length % 2 === 1) {
        products.push({
          id: null,
        });
      }

      this.setState(
        {
          products: products,
          hasMorePage: response.has_morepages,
        },
        () => {
          console.log('products: ' + this.state.products);
          console.log('hasMorePage: ' + this.state.hasMorePage);
          this.setState({
            productsLoaded: true,
          });
        }
      );
    } catch (e) {
      console.warn("_fetchProducts", e);
    }
  };

  _fetchMoreProducts = () => {
    if (this.state.hasMorePage) {
      console.log('fetching more data.');
      this.setState({ isLoadingMore: true }, async () => {
        this.page = this.page + 1;
        try {
          let formData = new FormData();

          if (this.state.search) {
            formData.append('search', this.state.search);
          }
          if (this.state.sort_by) {
            formData.append('sort_by', this.state.sort_by);
          }
          formData.append('page', this.page);
          formData.append('include', 'info');
          formData.append('per_page', '10');
          if (this.state.hasFiltered) {
            if (this.state.condition_by) {
              formData.append('condition', this.state.condition_by);
              console.log('condition : ' + this.state.condition_by);
            }
            if (this.state.categorize_by) {
              formData.append('categories', this.state.categorize_by);
              console.log('categories : ' + this.state.categorize_by);
            }
            if (this.state.min_Price) {
              formData.append('min', this.state.min_Price);
              console.log('min : ' + this.state.min_Price);
            }
            if (this.state.max_Price) {
              formData.append('max', this.state.max_Price);
              console.log('max : ' + this.state.max_Price);
            }
            if (this.state.rated_by) {
              formData.append('rating', this.state.rated_by);
              console.log('rating : ' + this.state.rated_by);
            }
          }
          formData.append('api_token', api_token);

          const data = await fetch(products_api, {
            method: 'POST',
            headers: {
              'Content-Type': 'multipart/form-data',
            },
            body: formData,
          });

          const response = await data.json();

          var products = [...this.state.products, ...response.data];

          if (products.length % 2 === 1) {
            products.push({
              id: null,
            });
          }

          this.setState(
            {
              products: products,
              hasMorePage: response.has_morepages,
            },
            () => {
              this.setState({
                isLoadingMore: false,
              });
            }
          );
        } catch (e) {
          console.warn(e);
        }
      });
    } else {
      this.setState({ isLoadingMore: false });
    }
  };

  _onRefresh = async () => {
    try {
      this.page = 1;
      this.setState({
        hasFiltered: false,
        isRefreshing: true,
      });
      let formData = new FormData();

      if (this.state.search) {
        formData.append('search', this.state.search);
      }
      if (this.state.sort_by) {
        formData.append('sort_by', this.state.sort_by);
      }
      formData.append('page', this.page);
      formData.append('include', 'info');
      formData.append('per_page', '10');
      if (this.state.hasFiltered) {
        if (this.state.condition_by) {
          formData.append('condition', this.state.condition_by);
          console.log('condition : ' + this.state.condition_by);
        }
        if (this.state.categorize_by) {
          formData.append('categories', this.state.categorize_by);
          console.log('categories : ' + this.state.categorize_by);
        }
        if (this.state.min_Price) {
          formData.append('min', this.state.min_Price);
          console.log('min : ' + this.state.min_Price);
        }
        if (this.state.max_Price) {
          formData.append('max', this.state.max_Price);
          console.log('max : ' + this.state.max_Price);
        }
        if (this.state.rated_by) {
          formData.append('rating', this.state.rated_by);
          console.log('rating : ' + this.state.rated_by);
        }
      }
      formData.append('api_token', api_token);

      const data = await fetch(products_api, {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      });

      const response = await data.json();

      var products = [...response.data];

      if (products.length % 2 === 1) {
        products.push({
          id: null,
        });
      }

      this.setState(
        {
          products: products,
          hasMorePage: response.has_morepages,
        },
        () => {
          this.setState({
            isRefreshing: false,
          });
        }
      );
    } catch (e) {
      console.warn(e);
    }
  };

  _handleSorting = () => {
    this.setState(
      {
        sort_by: this.state.initial_sort_by,
        sortModal: false,
      },
      () => {
        this._fetchProducts();
      }
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#ffffff' }}>
        <Header transparent
          style={styles.header}>
          <Left style={styles.left}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.leftArrow}>
              </Thumbnail>
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <TextInput style={styles.txtSearch}
              placeholder='Categories'
              onChangeText={(search) => this.setState({ search })}
              value={this.state.search}
              onSubmitEditing={() => {
                this._fetchProducts();
              }}>
            </TextInput>
          </Body>
          <Right style={styles.right}>
            <TouchableOpacity onPress={() => alert('Coming Soon')}>
              <Thumbnail
                source={Assets.accountSettings.iconFilter}
                square
                style={styles.rightIcon}
              />
            </TouchableOpacity>
          </Right>
        </Header>

        <Content>
          <View style={styles.informationContainer}>
            <TabView
              navigationState={this.state}
              renderScene={SceneMap({
                latest: Latest,
                price: Price,
                bestseller: BestSeller
              })}
              renderTabBar={props =>
                <TabBar
                  {...props}
                  position={this.props.route.params?.sort_by == 'best_seller' ? 2 : this.props.route.params?.sort_by == 'lowHigh' ? 1 : 0}
                  labelStyle={{
                    // fontFamily: 'Roboto', 
                    // fontSize: 10,
                    color: '#231F20',
                    textTransform: 'capitalize',
                  }}
                  indicatorStyle={{ backgroundColor: '#F36E23' }}
                  style={{
                    backgroundColor: '#7D7D7D',
                    color: '#231F20',
                  }}
                  onTabPress={({ route, preventDefault }) => {

                    // alert(JSON.stringify(route));
                    if (route.key === 'latest') {
                      this.setState({
                        initial_sort_by: 'latest',
                        sort_by: 'latest'
                      })
                      return (
                        this._fetchProducts()
                      )
                    }
                    if (route.key === 'price') {
                      this.setState({
                        initial_priceSort: 'lowHigh',
                        sort_by: 'lowHigh'
                      })
                      return (
                        this._fetchProducts()
                      )
                    }
                    if (route.key === 'bestseller') {
                      this.setState({
                        initial_sort_by: 'best_seller',
                        sort_by: 'best_seller'
                      })
                      return (
                        this._fetchProducts()
                      )
                    }
                  }}

                />
              }
              onIndexChange={index => this.setState({ index })}
              initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
            />
          </View>
          <View
            style={{
              backgroundColor: '#f5f6fa',
              flex: 1,
            }}
          >
            {this._renderProductList()}
          </View>
        </Content>
      </Container>
    );
  }

  _renderProductList = () => {
    if (this.state.productsLoaded) {
      return (
        <FlatListComponent
          data={this.state.products}
          keyExtractor={(item, index) => index.toString()}
          numColumns={2}
          refreshing={this.state.isRefreshing}
          onRefresh={this._onRefresh}
          onEndReachedThreshold={0}
          onEndReached={this._fetchMoreProducts}
          renderItem={this._renderProducts}
          ListFooterComponent={this._renderFooter}
          removeClippedSubviews={false}
          windowSize={18}
          maxToRenderPerBatch={8}
          scrollEventThrottle={16}
        />
      );
    } else {
      return (
        <View style={{ paddingTop: 20 }}>
          <PulseIndicator color='#00a14b' count={5} />
        </View>
      );
    }
  };

  _renderFooter = () => {
    if (this.state.isLoadingMore) {
      return (
        <View style={{ paddingBottom: 10 }}>
          <PulseIndicator color='#00a14b' count={5} />
        </View>
      );
    } else {
      return <View />;
    }
  };

  _renderProducts = ({ item }) => {
    if (!item.id) {
      return (
        <View
          style={{
            width: '48%',
          }}
        />
      );
    } else {
      return (
        <RenderItem
          onPress={() => this.props.navigation.navigate('ProductDetails', { product_id: item.id })}
          uri={item.info.data.image.full_path}
          id={item.id}
          product_name={item.product_name}
          country_code={item.country_code}
          price={item.price}
          ratings={item.ratings}
        />
      );
    }
  };

  _renderSortOptions = () => {
    if (this.state.sort_by == 'latest') {
      return 'Latest';
    } else if (this.state.sort_by == 'popularity') {
      return 'Popularity';
    } else if (this.state.sort_by == 'best_seller') {
      return 'Best Seller';
    } else {
      return 'Sort by';
    }
  };

  _renderSortModal = () => {
    return (
      <Modal
        visible={this.state.sortModal}
        style={{
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, .3)',
        }}
      >
        <View
          style={{
            width: width - 60,
            borderRadius: 10,
            backgroundColor: '#ffffff',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: 15,
          }}
        >
          <View
            style={{
              paddingBottom: 10,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <Text
                style={{
                  // fontFamily: 'roboto-medium',
                  fontSize: 18,
                  color: '#231f20',
                  marginBottom: 5,
                  alignSelf: 'flex-start',
                }}
              >
                Sort by
              </Text>

              <TouchableOpacity
                onPress={() => this.setState({ sortModal: false, initial_sort_by: this.state.sort_by })}
              >
                <Thumbnail
                  square
                  source={Assets.accountSettings.iconEx}
                  style={{
                    width: 16,
                    height: 16,
                    tintColor: '#231f20',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: width - 85,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    initial_sort_by: 'latest',
                  })}
                style={{
                  width: (width - 120) / 2,
                  paddingLeft: 2,
                  paddingRight: 2,
                  paddingTop: 5,
                  paddingBottom: 5,
                  borderWidth: 0.7,
                  borderColor: '#7f8c8d',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: this.state.initial_sort_by == 'latest' ? '#0a7aaa' : 'transparent',
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: this.state.initial_sort_by == 'latest' ? '#ffffff' : '#7f8c8d',
                  }}
                >
                  Latest
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    initial_sort_by: 'popularity',
                  })}
                style={{
                  width: (width - 120) / 2,
                  paddingLeft: 2,
                  paddingRight: 2,
                  paddingTop: 5,
                  paddingBottom: 5,
                  borderWidth: 0.7,
                  borderColor: '#7f8c8d',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: this.state.initial_sort_by == 'popularity' ? '#0a7aaa' : 'transparent',
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: this.state.initial_sort_by == 'popularity' ? '#ffffff' : '#7f8c8d',
                  }}
                >
                  Popularity
                </Text>
              </TouchableOpacity>
            </View>

            <View
              style={{
                paddingTop: 10,
                width: width - 85,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    initial_sort_by: 'best_seller',
                  })}
                style={{
                  width: (width - 120) / 2,
                  paddingLeft: 2,
                  paddingRight: 2,
                  paddingTop: 5,
                  paddingBottom: 5,
                  borderWidth: 0.7,
                  borderColor: '#7f8c8d',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: this.state.initial_sort_by == 'best_seller' ? '#0a7aaa' : 'transparent',
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: this.state.initial_sort_by == 'best_seller' ? '#ffffff' : '#7f8c8d',
                  }}
                >
                  Best Seller
                </Text>
              </TouchableOpacity>

              <View
                style={{
                  width: (width - 120) / 2,
                }}
              />
            </View>
          </View>

          <View>
            <TouchableOpacity
              onPress={() => this._handleSorting()}
              style={{
                backgroundColor: '#00a14b',
                borderRadius: 5,
                width: width - 85,
                alignItems: 'center',
                justifyContent: 'space-around',
                padding: 7,
              }}
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#ffffff',
                }}
              >
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  _renderPriceSortModal = () => {
    return (
      <Modal
        visible={this.state.priceSortModal}
        style={{
          top: 0,
          bottom: 0,
          right: 0,
          left: 0,
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, .3)',
        }}
      >
        <View
          style={{
            width: width - 60,
            borderRadius: 10,
            backgroundColor: '#ffffff',
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'space-between',
            padding: 15,
          }}
        >
          <View
            style={{
              paddingBottom: 10,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <Text
                style={{
                  // fontFamily: 'roboto-medium',
                  fontSize: 18,
                  color: '#231f20',
                  marginBottom: 5,
                  alignSelf: 'flex-start',
                }}
              >
                Sort by
              </Text>

              <TouchableOpacity
                onPress={() => this.setState({ priceSortModal: false, initial_priceSort: this.state.priceSort })}
              >
                <Thumbnail
                  square
                  source={Assets.accountSettings.iconEx}
                  style={{
                    width: 16,
                    height: 16,
                    tintColor: '#231f20',
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                width: width - 85,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around',
              }}
            >
              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    initial_priceSort: 'lowHigh',
                  })}
                style={{
                  width: (width - 120) / 2,
                  paddingLeft: 2,
                  paddingRight: 2,
                  paddingTop: 5,
                  paddingBottom: 5,
                  borderWidth: 0.7,
                  borderColor: '#7f8c8d',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: this.state.initial_priceSort == 'lowHigh' ? '#0a7aaa' : 'transparent',
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: this.state.initial_priceSort == 'lowHigh' ? '#ffffff' : '#7f8c8d',
                  }}
                >
                  Low to High
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={() =>
                  this.setState({
                    initial_priceSort: 'highLow',
                  })}
                style={{
                  width: (width - 120) / 2,
                  paddingLeft: 2,
                  paddingRight: 2,
                  paddingTop: 5,
                  paddingBottom: 5,
                  borderWidth: 0.7,
                  borderColor: '#7f8c8d',
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  backgroundColor: this.state.initial_priceSort == 'highLow' ? '#0a7aaa' : 'transparent',
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 14,
                    color: this.state.initial_priceSort == 'highLow' ? '#ffffff' : '#7f8c8d',
                  }}
                >
                  High to Low
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View>
            <TouchableOpacity
              onPress={() => this._handlePriceSorting()}
              style={{
                backgroundColor: '#00a14b',
                borderRadius: 5,
                width: width - 85,
                alignItems: 'center',
                justifyContent: 'space-around',
                padding: 7,
              }}
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#ffffff',
                }}
              >
                Apply
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  };

  _renderFilterModal = () => {
    return (
      <Modal
        visible={this.state.filterOpen}
        style={{
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          justifyContent: 'space-around',
          alignItems: 'center',
          backgroundColor: 'rgba(0, 0, 0, .5)',
          position: 'absolute',
          flexDirection: 'row',
        }}
      >
        <TouchableOpacity
          onPress={() => {
            if (this.state.hasFiltered) {
              this.setState({
                filterOpen: false,
              });
            } else {
              this.setState(
                {
                  condition_by: null,
                  categorize_by: null,
                  rated_by: null,
                  min_Price: null,
                  max_Price: null,
                  hasFiltered: false,
                },
                () => {
                  this.setState({
                    filterOpen: false,
                  });
                }
              );
            }
          }}
          style={{
            backgroundColor: 'transparent',
            width: 50,
            height: height,
          }}
        >
          <View />
        </TouchableOpacity>
        <Animatable.View
          animation='fadeInRightBig'
          duration={500}
          style={{
            width: width - 50,
            height: height,
            marginLeft: 20,
            backgroundColor: '#ffffff',
          }}
        >
          <KeyboardAwareScrollView
            keyboardDismissMode='interactive'
            style={{
              marginTop: 20,
            }}
          >
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                margin: 10,
              }}
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 16,
                  color: '#231f20',
                }}
              >
                Search Filter
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.setState(
                    {
                      hasFiltered: false,
                      condition_by: null,
                      categorize_by: null,
                      rated_by: null,
                      min_Price: null,
                      max_Price: null,
                    },
                    () => {
                      this._fetchProducts();
                    }
                  );
                }}
              >
                <Text
                  style={{
                    // fontFamily: 'Roboto',
                    fontSize: 12,
                    color: '#f36e23',
                  }}
                >
                  RESET
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                width: '100%',
                borderTopWidth: 0.7,
                borderColor: '#95a5a6',
              }}
            />
            <View
              style={{
                padding: 10,
                flex: 1,
              }}
            >
              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#231f20',
                  paddingBottom: 5,
                }}
              >
                Category
              </Text>

              <FlatList
                data={this.state.categories}
                numColumns={3}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderCategories}
                columnWrapperStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  margin: 5,
                }}
              />

              <View
                style={{
                  width: '100%',
                  borderTopWidth: 0.7,
                  borderColor: '#95a5a6',
                  marginTop: 10,
                  marginBottom: 10,
                }}
              />

              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#231f20',
                  paddingBottom: 5,
                }}
              >
                Condition
              </Text>

              <FlatList
                data={this.state.condition_options}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderConditionOptions}
                columnWrapperStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  margin: 5,
                }}
              />

              <View
                style={{
                  width: '100%',
                  borderTopWidth: 0.7,
                  borderColor: '#95a5a6',
                  marginTop: 10,
                  marginBottom: 10,
                }}
              />

              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#231f20',
                  paddingBottom: 5,
                }}
              >
                Price
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                }}
              >
                <View
                  style={{
                    padding: 5,
                    borderWidth: 0.7,
                    borderColor: '#231f20',
                    width: '45%',
                  }}
                >
                  <TextInput
                    style={{
                      // fontFamily: 'Roboto',
                      fontSize: 14,
                      color: '#231f20',
                      paddingLeft: 10,
                      flex: 1,
                    }}
                    placeholder='MIN'
                    placeholderTextColor='#7f8c8d'
                    onChangeText={(x) => this.setState({ min_Price: x })}
                    keyboardAppearance='dark'
                  />
                </View>
                <View
                  style={{
                    width: '10%',
                    padding: 5,
                  }}
                >
                  <Text
                    style={{
                      // fontFamily: 'Roboto',
                      fontSize: 14,
                      color: '#231f20',
                      paddingLeft: 10,
                    }}
                  >
                    {' '}
                    -{' '}
                  </Text>
                </View>
                <View
                  style={{
                    padding: 5,
                    borderWidth: 0.7,
                    borderColor: '#231f20',
                    width: '45%',
                  }}
                >
                  <TextInput
                    style={{
                      // fontFamily: 'Roboto',
                      fontSize: 14,
                      color: '#231f20',
                      paddingLeft: 10,
                      flex: 1,
                    }}
                    placeholder='MAX'
                    placeholderTextColor='#7f8c8d'
                    onChangeText={(x) => this.setState({ max_Price: x })}
                    keyboardAppearance='dark'
                  />
                </View>
              </View>

              <View
                style={{
                  width: '100%',
                  borderTopWidth: 0.7,
                  borderColor: '#95a5a6',
                  marginTop: 10,
                  marginBottom: 10,
                }}
              />

              <Text
                style={{
                  // fontFamily: 'Roboto',
                  fontSize: 14,
                  color: '#231f20',
                  paddingBottom: 5,
                }}
              >
                Rating
              </Text>

              <FlatList
                data={this.state.rating_options}
                numColumns={3}
                keyExtractor={(item, index) => index.toString()}
                renderItem={this._renderRatings}
                columnWrapperStyle={{
                  alignItems: 'center',
                  justifyContent: 'space-around',
                  margin: 5,
                }}
              />

              <Footer
                style={{
                  backgroundColor: '#ffffff',
                  marginTop: 20,
                }}
              >
                <FooterTab
                  style={{
                    margin: 10,
                    backgroundColor: '#ffffff',
                  }}
                >
                  <TouchableOpacity
                    onPress={() => this.setState({ filterOpen: false, hasFiltered: true }, () => this._fetchProducts())}
                    style={{
                      backgroundColor: '#00a14b',
                      flex: 1,
                      alignItems: 'center',
                      justifyContent: 'space-around',
                    }}
                  >
                    <Text
                      style={{
                        // fontFamily: 'Roboto',
                        fontSize: 12,
                        color: '#ffffff',
                      }}
                    >
                      APPLY
                    </Text>
                  </TouchableOpacity>
                </FooterTab>
              </Footer>
            </View>
          </KeyboardAwareScrollView>
        </Animatable.View>
      </Modal>
    );
  };

  _renderCategories = ({ item }) => {
    if (this.state.categorize_by === item.id) {
      return (
        <View
          style={{
            width: '27%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#0a7aaa',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#ffffff',
              textAlign: 'center',
            }}
          >
            {item.category_name}
          </Text>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ categorize_by: item.id })}
          style={{
            width: '27%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#ecf0f1',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#7f8c8d',
              textAlign: 'center',
            }}
          >
            {item.category_name}
          </Text>
        </TouchableOpacity>
      );
    }
  };

  _renderConditionOptions = ({ item }) => {
    if (this.state.condition_by === item.id) {
      return (
        <View
          style={{
            width: '37%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#0a7aaa',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#ffffff',
              textAlign: 'center',
            }}
          >
            {item.name}
          </Text>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ condition_by: item.id })}
          style={{
            width: '37%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#ecf0f1',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#7f8c8d',
              textAlign: 'center',
            }}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      );
    }
  };

  _renderRatings = ({ item }) => {
    if (this.state.rated_by === item.id) {
      return (
        <View
          style={{
            width: '27%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#0a7aaa',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#ffffff',
              textAlign: 'center',
            }}
          >
            {item.name}
          </Text>
        </View>
      );
    } else {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ rated_by: item.id })}
          style={{
            width: '27%',
            height: 45,
            alignItems: 'center',
            justifyContent: 'space-around',
            padding: 10,
            backgroundColor: '#ecf0f1',
            borderRadius: 5,
          }}
        >
          <Text
            style={{
              // fontFamily: 'Roboto',
              fontSize: 10,
              color: '#7f8c8d',
              textAlign: 'center',
            }}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      );
    }
  };
}

// const mapStateToProps = (state) => {
//   return {
//     ...state,
//   };
// };

export default SearchResultsScreen;