import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Container, Header, Thumbnail, Content, Spinner } from 'native-base'
import AsyncStorage from '@react-native-community/async-storage';
import Asset from '../../components/assets.manager'
import Icons from '../../components/icons'
import Service from '../../components/api/service'
import { useFocusEffect } from '@react-navigation/native';

export default function ToShip({ navigation, route }) {

  const [deliveries, setDeliveries] = useState([])
  const [loading, setLoading] = useState(false)

  const ToShipItems = async () => {
    setDeliveries([])
    setLoading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    let data = {}
    data.id = user.data.id
    // data.type = "ship"
    data.type = "ship+processing"

    await Service.getDeliveries(
      data,
      (res) => {
        setLoading(false)
        setDeliveries(res.data)
      },
      (err) => {
        setLoading(false);
        console.log("Service.getDeliveries error", err)
      });
  }

  useFocusEffect(
    React.useCallback(() => {
      ToShipItems()
    }, [])
  )

  const Transaction = (props) => {
    return (
      <View style={{ marginBottom: 15 }}>
        <View style={{ width: '100%', padding: 15, borderWidth: 0, borderRadius: 5, borderBottomWidth: 0, backgroundColor: '#66B548' }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Order #{props.data.order_id}</Text>
        </View>
        <View style={{ width: '100%', padding: 15, borderWidth: 0.2, flexDirection: 'row', backgroundColor: '#fff', borderBottomStartRadius: 5, borderBottomEndRadius: 5 }}>
          <View style={{ flex: 4, padding: 15 }}>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{props.data.payment_type.toUpperCase()}</Text>
            <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }}>STATUS: {props.data.payment_status.toUpperCase()}</Text>
          </View>
          <View style={{ flex: 8, padding: 15 }}>
            <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }} numberOfLines={3}>{props.data.description}</Text>
            <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Total: {props.data.total_amount.toFixed(2)}</Text>
          </View>
          <TouchableOpacity style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}
            onPress={() => {
              // console.log("props", props)
              navigation.navigate("TransactionDetails", {
                id: props.data.order_id,
                shipping: props.data.info.data.shipping_info,
                billing: props.data.info.data.billing_info,
                orders: props.data.order_list,
                user: props.data.info.data.user_info,
                date: props.data.date.data.added_in,
                total: props.data.total_amount,
                description: props.data.description
              })
            }}>
            <Icons.EvilIcons name="chevron-right" size={45} color="#66B548" />
          </TouchableOpacity>
        </View>
      </View >
    )
  }

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={[styles.headerStyles, { left: 20 }]}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Asset.accountSettings.arrowDarkIcon}
              style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>To Ship</Text>
        <View style={[styles.headerStyles, { right: 20 }]}>
        </View>
      </Header>
      <Content padder>
        {loading ? <Spinner /> :
          deliveries.length > 0 ? deliveries.map((item, i) => {
            return <Transaction data={item} key={i} />
          })
            :
            <View style={styles.referredSection}>
              <Text style={styles.referredText}>No To Ship Items yet.</Text>
            </View>}
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18
  },
  headerStyles: {
    position: 'absolute',
    alignSelf: 'center'
  },
  headerText: {
    // fontFamily:'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  forwardIcon: {
    width: 20,
    height: 20
  },
  title: {
    // fontFamily:'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 15,
  },
  categoryContainer: {
    paddingHorizontal: 20,
    paddingVertical: 13,
    flexDirection: 'row',
    backgroundColor: '#ffffff'
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#d3d3d3'
  },
  referredSection: {
    // backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },

})