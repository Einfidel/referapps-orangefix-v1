import React, {
  useState,
  useEffect,
} from 'react';
import Moment from 'moment';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  StatusBar,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { Picker } from '@react-native-picker/picker';
import { Thumbnail, Left, Header, Spinner } from 'native-base';
import Modal from 'react-native-root-modal';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';

import { loginWithFacebook } from '../../components/Facebook.js';
import { loginWithGoogle } from '../../components/Google.js';
import Assets from '../../components/assets.manager';
import styles from '../../styles/signup/signup';
import AsyncStorage from '@react-native-community/async-storage';
import AppleSignin from '../../components/AppleSignin';
import FastImage from 'react-native-fast-image'
import Icons from '../../components/icons';

import appleAuth from '@invertase/react-native-apple-authentication';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore'
// import storage from '@react-native-firebase/storage'
// import functions from '@react-native-firebase/functions'

const API_URL = 'https://www.referapps.com';
const URL_REGISTER = '/api/auth/register.json';
const API_TOKEN = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';


export default function Signup({ navigation, route }) {

  const [fname, setFname] = useState('');
  const [lname, setLname] = useState('');
  const [email, setEmail] = useState('');
  const [emailConfirmation, setEmailConfirmation] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [loginData, setLoginData] = useState(null);
  const [date, setDate] = useState(new Date().date);
  const [passwordType, setPasswordType] = useState(true);
  const [mode, setMode] = useState('date');
  const [isDateTimePickerVisible, setIsDateTimePickerVisible] = useState(false);
  const [birthdate, setBirthdate] = useState('');
  const [countryCode, setCountryCode] = useState('PH');
  const [contactNumber, setContactNumber] = useState('');
  const [signupTapped, setSignupTapped] = useState(false);
  const [validated, setValidated] = useState(false);
  const [passwordHasError, setPasswordHasError] = useState(false);
  const [value, setValue] = useState('');
  const [maxDate, setMaxDate] = useState('');
  const [initialDate, setInitialDate] = useState('');
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState('');
  const [isLoggingIn, setIsLoggingIn] = useState(false);
  const [username, setUsername] = useState('');
  const [passwordIsVisible1, setPasswordIsVisible1] = useState(false);
  const [passwordIsVisible2, setPasswordIsVisible2] = useState(false);
  const [iosMargin, setiosMargin] = useState(0);

  useEffect(() => {
    appleAuth.isSupported ? setiosMargin(0) : setiosMargin(0);
    Moment.locale('en');
  }, [])

  const handleDatePicked = (date) => {
    setBirthdate(Moment(date).format('DD-MM-YYYY'));
    setIsDateTimePickerVisible(false)
  };

  const handleSignUp = async () => {
    if (fname === null || fname.trim() === '') {
      Alert.alert('First Name is required');
      setLoading(false)
      return;
    } else if (lname === null || lname.trim() === '') {
      Alert.alert('Last Name is required');
      setLoading(false)
      return;
    } else if (email === null || email.trim() === '') {
      Alert.alert('Email is required');
      setLoading(false)
      return;
    } else if (!isEmailValid(email)) {
      Alert.alert('Email must be valid');
      setLoading(false)
      return;
    } else if (email != emailConfirmation) {
      Alert.alert('Emails does not match');
      setLoading(false)
      return;
    } else if (birthdate === null || birthdate.trim() === '') {
      Alert.alert('Birthdate is required');
      setLoading(false)
      return;
    } else if (contactNumber === null || contactNumber.trim() === '') {
      Alert.alert('Contact Number is required');
      setLoading(false)
      return;
    } else if (password === null || password.trim() === '') {
      Alert.alert('Password is required');
      setLoading(false)
      return;
    } else if (!isValidPassword(password)) {
      let lowerCaseReg = new RegExp("^(?=.*[a-z])");
      let upperCaseReg = new RegExp("^(?=.*[A-Z])");
      let digitsReg = new RegExp("^(?=.*[0-9])");
      let specialCharsReg = new RegExp("^(?=.*[!@#\$%\^&\*])");
      // let strongRegex = new RegExp("^(?=.{8,})");
      // let strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      if (lowerCaseReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 Lowercase character');
        console.warn('password is has no lowerCaseReg');
        setLoading(false)
        return;
      }
      if (upperCaseReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 Uppercase character');
        console.warn('password is has no upperCaseReg');
        setLoading(false)
        return;
      }
      if (digitsReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 number');
        console.warn('password is has no digitsReg');
        setLoading(false)
        return;
      }
      if (specialCharsReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 special character');
        console.warn('password is has no specialCharsReg');
        setLoading(false)
        return;
      }
      setLoading(false)
      return;
    } else if (lname === null || lname.trim() === '') {
      Alert.alert('Last Name is required');
      setLoading(false)
      return;
    } else if (lname === null || lname.trim() === '') {
      Alert.alert('Last Name is required');
      setLoading(false)
      return;
    } else if (lname === null || lname.trim() === '') {
      Alert.alert('Last Name is required');
      setLoading(false)
      return;
    }



    let formData = new FormData();
    formData.append('fname', fname);
    formData.append('lname', lname);
    formData.append('email', email);
    formData.append('birthdate', birthdate);
    formData.append('country_code', countryCode);
    formData.append('contact_number', contactNumber);
    formData.append('username', username);
    formData.append('password', password);
    formData.append('password_confirmation', passwordConfirmation);
    formData.append('api_token', API_TOKEN);

    let signedInApi = false;
    let signedInFirebase = false;

    // Laravel API Signup to get and set user_data
    fetch(API_URL + URL_REGISTER, {
      method: 'POST',
      body: formData,
    })
      .then((response) => response.json())
      .then(async (responseJson) => {
        if (responseJson.status) {
          // Set user_data on Async here
          await AsyncStorage.setItem('user_data', JSON.stringify(responseJson));
          signedInApi = true;

          // Firebase Register and Login 
          if (signedInApi) {
            try {
              await auth()
                .createUserWithEmailAndPassword(email, password)
                .then((response) => {
                  console.log("success creating account")
                  auth()
                    .signInWithEmailAndPassword(email, password)
                    .then(async (response) => {
                      setLoading(false)
                      // Set Firebase user data collection here
                      // setFirebaseUserData(response);
                      signedInFirebase = true;

                      // Reset Screen back to Home
                      if (signedInApi && signedInFirebase) {
                        navigation.reset({
                          index: 0,
                          routes: [{ name: 'Tab' }],
                        });
                      }
                    })
                    .catch(async (error) => {
                      alert("C. Firebase Signup Error:\n\n " + error)
                      console.error("C. Firebase Signup Error:", error);

                      signedInFirebase = false;
                      setLoading(false)
                      await AsyncStorage.removeItem('user_data');
                    })
                })
                .catch(async (error) => {
                  alert("B. Firebase Signup Error:\n\n " + error)
                  console.error("B. Firebase Signup Error:", error);

                  signedInFirebase = false;
                  await AsyncStorage.removeItem('user_data');
                  setLoading(false)
                })
            } catch (error) {
              alert("A. Firebase Signup Error: \n\n" + error);
              console.error("A. Firebase Signup Error:", error);

              signedInFirebase = false;
              await AsyncStorage.removeItem('user_data');
              setLoading(false)
            }
          } else {
            alert("C. Laravel API Signup Error: Uncatched Error");
            console.error("C. Laravel API Signup Error: Uncatched Error");

            await AsyncStorage.removeItem('user_data');
            setLoading(false)
          }

          alert(responseJson.msg);
        } else {
          setLoading(false)
          signedInApi = false;
          let errorString = '';

          console.error("B. Laravel API Signup Error", responseJson);
          Object.keys(responseJson.errors).map((key) => {
            responseJson.errors[key].map((error, index) => {
              errorString += "* " + error + '\n';
            })
          });

          alert("B. Laravel API Signup Error: \n\n" + errorString);
          // console.error("B. Laravel API Signup Erro  r:", errorString);
          // console.error("B.2 Laravel API Signup Error:", responseJson);
        }
      })
      .catch((error) => {
        setLoading(false)
        alert("A. Laravel API Signup Error: \n\n" + error);
        console.error("A. Laravel API Signup Error:", error);
      });
  };

  const setFirebaseUserData = async (response) => {
    try {
      const user = response.user
      const uid = user.uid

      var fname = fname;
      var lname = lname;
      var email = email;
      var birthdate = birthdate;
      var countryCode = countryCode;
      var contactNumber = contactNumber;
      var username = username;

      const newUserData = {
        id: uid,
        refer_id: uid,
        fname: fname,
        lname: lname,
        email: email,
        birthdate: birthdate,
        countryCode: countryCode,
        contactNumber: contactNumber,
        shop_name: null,
        username: username,
        is_verify: "yes",
        is_security_question_set: true,
        fb_id: null,
        image: "",
        ratings: "N/A",
        products: 0,
        services: 0,
        reviews: 0,
        countryCode: "PH",
        notifications: 0,
        total_cash: 0,
        product_sales: 0,
        sales_commission: 0,
        downline_commission: 0,
        accountCreater: new Date(),
      }
      console.log("this is newUserData", newUserData)

      await firestore()
        .collection('user_data')
        .doc(uid)
        .set(newUserData)
        .then(async () => {
          console.log("newUserData is set in firestore")
          const response = {
            "msg": "Congratulations" + newUserData.fname + " " + newUserData.lname + "a you successfully signed up.",
            "status": true,
            "status_code": "LOGIN_SUCCESS",
            "verified_user": "no",
            "two_factor_authenticated": "no",
            "first_login": true,
            "data": JSON.stringify(newUserData),
          }
          console.log("this is response", response)

          await AsyncStorage.setItem('user_data', JSON.stringify(response));
        })
        .catch((e) => console.log("error", e))
    } catch (error) {
      console.log("register setFirebaseUserData error", error)
    }
  }

  const renderUsernameError = () => {
    if (signupTapped) {
      if (errors.username) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.username[0]}
            </Text>
          </View>
        );
      }
      if (!username) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderFnameError = () => {
    if (signupTapped) {
      if (errors.fname) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.fname[0]}
            </Text>
          </View>
        );
      }
      if (!fname) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderLnameError = () => {
    if (signupTapped) {
      if (errors.lname) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.lname[0]}
            </Text>
          </View>
        );
      }
      if (!lname) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderEmailError = () => {
    if (signupTapped) {
      if (errors.email) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.email[0]}
            </Text>
          </View>
        );
      }
      if (!email) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
      if (!isEmailValid(email)) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This email must be a valid email address.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderEmailConfirmationError = () => {
    if (signupTapped) {
      if (!emailConfirmation) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
      if (email != emailConfirmation) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *Emails does not match.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderBirthdateError = () => {
    if (signupTapped) {
      if (errors.birthdate) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.birthdate[0]}
            </Text>
          </View>
        );
      }
      if (!birthdate) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderContactNumberError = () => {
    if (signupTapped) {
      if (errors.contactNumber) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.contactNumber[0]}
            </Text>
          </View>
        );
      }
      if (!contactNumber) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderPasswordError = () => {
    if (signupTapped) {
      if (errors.password) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *{errors.password[0]}
            </Text>
          </View>
        );
      }
      if (!password) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
      if (!isValidPassword(password)) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *Password must contain 1 Uppercase character, 1 Lowercase character, 1 Number, and 1 Special Character.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderPasswordConfirmationError = () => {
    if (signupTapped) {
      if (!passwordConfirmation) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *This field is required.
            </Text>
          </View>
        );
      }
      if (password != passwordConfirmation) {
        return (
          <View>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 12,
                color: '#ff1a1a',
                marginTop: 5,
              }}
            >
              *Password does not match.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const isEmailValid = (email) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(email) === false) {
      console.warn('Email is Not Correct');
      return false;
    } else {
      console.warn('Email is Correct');
      return true;
    }
  }

  const isValidPassword = (password) => {
    let lowerCaseReg = new RegExp("^(?=.*[a-z])");
    let upperCaseReg = new RegExp("^(?=.*[A-Z])");
    let digitsReg = new RegExp("^(?=.*[0-9])");
    let specialCharsReg = new RegExp("^(?=.*[!@#\$%\^&\*])");
    // let strongRegex = new RegExp("^(?=.{8,})");
    // let strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    if (lowerCaseReg.test(password) === false) {
      console.warn('password is has no lowerCaseReg');
      return false;
    }
    if (upperCaseReg.test(password) === false) {
      console.warn('password is has no upperCaseReg');
      return false;
    }
    if (digitsReg.test(password) === false) {
      console.warn('password is has no digitsReg');
      return false;
    }
    if (specialCharsReg.test(password) === false) {
      console.warn('password is has no specialCharsReg');
      return false;
    }
    return true;
  }

  const isApple = () => {
    if (appleAuth.isSupported) {
      return <AppleSignin navigation={navigation} />;
    }
  }

  return (
    <ScrollView showsHorizontalScrollIndicator={false}>
      <ImageBackground source={Assets.signIn.signinBg} style={styles.bgImage}>
        <StatusBar hidden />
        <KeyboardAwareScrollView>
          <View style={styles.headerContainer}>
            <View style={styles.arrowBackContainer}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <FastImage source={Assets.accountSettings.arrowDarkIcon} style={styles.imgArrow} resizeMode={FastImage.resizeMode.contain} />
              </TouchableOpacity>
              <View style={{ paddingLeft: 10 }} />
              <Text style={styles.txtSignUp}>Sign Up</Text>
            </View>
            <View style={{ flex: 1 }} />
          </View>

          {loading ? <Spinner /> : null}

          <View style={styles.fieldContainer}>
            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderFnameError()}
              <Animatable.View animation='fadeInLeft' duration={500} style={styles.txtInputContainer}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(fname) => setFname(fname)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='First Name'
                  value={fname}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderLnameError()}
              <Animatable.View animation='fadeInLeft' duration={700} style={styles.txtInputContainer}>
                <TextInput
                  textContentType='emailAddress'
                  style={styles.txtInput}
                  onChangeText={(lname) => setLname(lname)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Last Name'
                  value={lname}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderEmailError()}
              <Animatable.View animation='fadeInLeft' duration={900} style={styles.txtInputContainer}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(email) => {
                    setEmail(email.trim())
                  }}
                  // validate(text)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Email'
                  value={email}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderEmailConfirmationError()}
              <Animatable.View animation='fadeInLeft' duration={1100} style={styles.txtInputContainer}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(emailConfirmation) =>
                    setEmailConfirmation(emailConfirmation.trim())}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Email Confirmation'
                  value={emailConfirmation}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderBirthdateError()}
              <Animatable.View animation='fadeInLeft' duration={1300}>
                <TouchableOpacity onPress={() => {
                  setIsDateTimePickerVisible(true)
                }} style={styles.birthdateContainer}>
                  {birthdate ? (
                    <Text style={styles.txtInputs}>{birthdate}</Text>
                  ) : (
                    <Text style={styles.txtInputs}>Birthdate</Text>
                  )}
                  <DateTimePicker
                    date={date}
                    mode='date'
                    isVisible={isDateTimePickerVisible}
                    onConfirm={handleDatePicked}
                    onCancel={() => {
                      setIsDateTimePickerVisible(false)
                    }}
                  />
                </TouchableOpacity>
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60, marginBottom: iosMargin, marginTop: iosMargin }}>
              {renderContactNumberError()}
              <Animatable.View animation='fadeInLeft' duration={1500} style={styles.cnumber}>
                <View style={{ resizeMode: 'contain' }}>
                  <Picker
                    selectedValue={countryCode}
                    mode='dropdown'
                    style={styles.datePicker}
                    textStyle={{
                      // fontFamily: 'Roboto',
                      fontSize: 12,
                      color: '#231f20',
                    }}
                    onValueChange={(value) => setCountryCode(value)}
                  >
                    <Picker.Item label='PH (+63)' value='PH' />
                    <Picker.Item label='US (+1)' value='US' />
                  </Picker>
                </View>
                <TextInput
                  style={styles.txtInput}
                  maxLength={10}
                  keyboardType='numeric'
                  onChangeText={(value) => setContactNumber(value)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Cellphone number'
                  value={contactNumber}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderUsernameError()}
              <Animatable.View animation='fadeInLeft' duration={1800} style={styles.txtInputContainer}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(username) => setUsername(username)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Username'
                  value={username}
                />
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderPasswordError()}
              <Animatable.View animation='fadeInLeft' duration={2100}
                style={[styles.txtInputContainer, { flex: 1, flexDirection: 'row' }]}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(password) => setPassword(password.trim())}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Password'
                  secureTextEntry={!passwordIsVisible1}
                  value={password}
                />
                <View>
                  <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <TouchableOpacity onPress={() => {
                      setPasswordIsVisible1(!passwordIsVisible1)
                    }}>
                      {passwordIsVisible1 ?
                        <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                        :
                        <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                      }
                    </TouchableOpacity>
                  </View>
                </View>
              </Animatable.View>
            </View>

            <View style={{ width: Dimensions.get('window').width - 60 }}>
              {renderPasswordConfirmationError()}
              <Animatable.View animation='fadeInLeft' duration={2400} style={styles.txtInputContainer}>
                <TextInput
                  style={styles.txtInput}
                  onChangeText={(passwordConfirmation) =>
                    setPasswordConfirmation(passwordConfirmation)}
                  placeholderTextColor='#7f8c8d'
                  placeholder='Password Confirmation'
                  secureTextEntry={!passwordIsVisible2}
                  value={passwordConfirmation}
                />
                <View>
                  <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                    <TouchableOpacity onPress={() => {
                      setPasswordIsVisible2(!passwordIsVisible2)
                    }}>
                      {passwordIsVisible2 ?
                        <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                        :
                        <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                      }
                    </TouchableOpacity>
                  </View>
                </View>
              </Animatable.View>
            </View>

            <Animatable.View animation='fadeInUp' duration={2700}>
              <TouchableOpacity
                onPress={() => {
                  if (!loading) {
                    setLoading(true)
                    setSignupTapped(true)
                    handleSignUp();
                  }
                }}
              >
                <LinearGradient
                  colors={['#f89522', '#f58223', '#f36e23']}
                  style={styles.gradientBtnSignup}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Text style={styles.txtGradientBtnSignup}>Sign Up</Text>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>
          </View>

          <View style={styles.connectContainer}>

            <Animatable.View animation='fadeInUp' duration={3000}>
              {isApple()}
            </Animatable.View>

            <View style={{ paddingTop: 5 }} />

            <Animatable.View animation='fadeInUp' duration={3300}>
              <TouchableOpacity onPress={() => loginWithFacebook(navigation)}>
                <LinearGradient
                  colors={['#1aa3ff', '#008ae6']}
                  style={styles.gradient}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <View style={{ flex: 0 }}>
                    <Thumbnail source={Assets.signIn.fbIcon} square style={styles.icons} />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.txtConnect}>Connect with Facebook</Text>
                  </View>
                  <View style={{ flex: 0 }}>
                    <View style={{ width: 20, height: 20 }} />
                  </View>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>

            <Animatable.View animation='fadeInUp' duration={3600}>
              <TouchableOpacity onPress={() => {
                Alert.alert("Under Maintenance")
                // loginWithGoogle()
              }}>
                <LinearGradient
                  colors={['#1aa3ff', '#008ae6']}
                  style={styles.gradient}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <View style={{ flex: 0 }}>
                    <Thumbnail source={Assets.signIn.googleIcon} square style={styles.icons} />
                  </View>
                  <View style={{ flex: 1 }}>
                    {!isLoggingIn ? (
                      <Text style={styles.txtConnect}>Connect with Google</Text>
                    ) : (
                      <UIActivityIndicator color={'#ffffff'} count={12} size={30} />
                    )}
                  </View>
                  <View style={{ flex: 0 }}>
                    <View style={{ width: 20, height: 20 }} />
                  </View>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>

            <Animatable.View animation='fadeInUp' duration={3900} style={styles.noteContainer}>
              <TouchableOpacity onPress={() => navigation.navigate('Sign In')} style={{ flex: 0 }}>
                <Text style={styles.txtNote}>Already have an account?</Text>
              </TouchableOpacity>
            </Animatable.View>

            <Animatable.View animation='fadeInUp' duration={4200} style={{ padding: 10 }} />
          </View>
        </KeyboardAwareScrollView>

        {/* <Modal visible={loading} style={styles.modalLoading}>
          <View style={styles.processingContainer}>
            <MaterialIndicator color={'#ffffff'} size={70} />
            <Text style={styles.txtProcessing}>Processing...</Text>
          </View>
        </Modal> */}
      </ImageBackground>
    </ScrollView >
  );
}
