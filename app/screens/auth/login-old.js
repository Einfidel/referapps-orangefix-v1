import React from 'react';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  StatusBar,
  Alert,
  NativeModules,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import { Thumbnail } from 'native-base';
import styles from '../../styles/login/loginStyles';

import AsyncStorage from '@react-native-community/async-storage';
import { loginWithFacebook } from '../../components/Facebook.js';
import { loginWithGoogle } from '../../components/Google.js';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
// import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import AppleSignin from '../../components/AppleSignin';
import appleAuth from '@invertase/react-native-apple-authentication';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';


// GoogleSignin.configure({
//   webClientId: '75162238761-ftkbdfm6c8ket50v6ki1mml4qsgf6a1u.apps.googleusercontent.com', 
//   iosClientId: '75162238761-ftkbdfm6c8ket50v6ki1mml4qsgf6a1u.apps.googleusercontent.com',
// });
// const { signIn } = React.useContext(AuthContext);

class Login extends React.Component {
  headerMode = false;
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: '',
      login_data: null,
      msg: '',
      userId: 0,
      is_LoggedIn: true,
      refer_id: '',
      first_login: '',
      country_code: '',
      is_logging_in: false,
      user_token: '',
    };
  }

  componentDidMount() {
    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '75162238761-oeds0c4roo7ordic2m2lt3tn7dqsindl.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      // offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      // hostedDomain: '', // specifies a hosted domain restriction
      // loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      // forceCodeForRefreshToken: true, // [Android] related to `serverAuthCode`, read the docs link below *.
      // accountName: '', // [Android] specifies an account name on the device that should be used
      iosClientId: '75162238761-oeds0c4roo7ordic2m2lt3tn7dqsindl.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }

  // _signIn = async () => {
  //   try {
  //     await GoogleSignin.hasPlayServices();
  //     const userInfo = await GoogleSignin.signIn().then(async (data) => {
  //       console.log('google sing in data', data)
  //       // let formData = new FormData();
  //       // formData.append('api_token', Endpoints.token);
  //       // formData.append('access_token', data.idToken)

  //       try {
  //         const { idToken } = await GoogleSignin.signIn();

  //         // Create a Google credential with the token
  //         const googleCredential = auth.GoogleAuthProvider.credential(idToken);

  //         // Sign-in the user with the credential
  //         return auth().signInWithCredential(googleCredential);

  //         // await AsyncStorage.setItem('auth', JSON.stringify(data.idToken));
  //         await AsyncStorage.setItem('user_data', JSON.stringify(data.user));
  //         // await AsyncStorage.removeItem("app_refer_state");
  //         this.props.navigation.reset({
  //           index: 0,
  //           routes: [{ name: 'Tab' }]
  //         });
  //       }
  //       catch (e) {
  //         console.warn(e)
  //       }
  //     }).catch((e) => console.warn(e))
  //     console.warn(userInfo)

  //     // try{
  //     //   let responseJson = userInfo
  //     //   console.warn('sign in', responseJson)
  //     // }
  //     // catch(error){
  //     //   console.log(error)
  //     // }
  //     // this.setState({ userInfo });
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_CANCELLED) {
  //       // user cancelled the login flow
  //     } else if (error.code === statusCodes.IN_PROGRESS) {
  //       // operation (e.g. sign in) is in progress already
  //     } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
  //       // play services not available or outdated
  //     } else {
  //       // some other error happened
  //     }
  //     console.log(error)
  //   }
  // };

  // signinGoogle = () => {
  //   auth()
  //   .createUserWithEmailAndPassword('sarah.lane@gmail.com', 'SuperSecretPassword!')
  //   .then(() => {
  //     console.log('User account created & signed in!');
  //   })
  //   .catch(error => {
  //     if (error.code === 'auth/email-already-in-use') {
  //       console.log('That email address is already in use!');
  //     }

  //     if (error.code === 'auth/invalid-email') {
  //       console.log('That email address is invalid!');
  //     }

  //     console.error(error);
  //   });
  // }

  async onGoogleButtonPress() {
    console.log('onGoogleButtonPress')
    // Get the users ID token
    try {
      console.log('try')
      await GoogleSignin.hasPlayServices();
      console.log("tried")
      const { idToken } = await GoogleSignin.signIn();
      console.log("google token", idToken)

      // Create a Google credential with the token
      const googleCredential = auth.GoogleAuthProvider.credential(idToken);

      // Sign-in the user with the credential
      auth().signInWithCredential(googleCredential)
        .then(async (response) => {
          this.getFirebaseUserData(response)
        })

    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log("google sign in: user has cancelled login")
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log("google sign in: sign in is in progress already")
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("google sign in: play services not available or outdated")
      } else {
        alert(error.message)
        console.log("google sign in: error", error)
      }
    }
  }

  componentDidUpdate = async (prevProps, prevState) => {
    console.log("prevprops", prevProps);
    alert(prevProps);
    if (this.props.userId !== prevProps.id) {
      this.props.navigation.goBack();
    }
  };

  ValidateFields() {
    let uid = this.state.username == '' ? null : this.state.username;
    let pwd = this.state.password == '' ? null : this.state.password;
    if (uid == null && pwd == null) {
      // alert('Username and password is required');
      alert('Email and password is required');
      return false;
    } else if (uid == null && pwd != null) {
      // alert('Username is required');
      alert('Email is required');
      return false;
    } else if (pwd == null && uid != null) {
      alert('Password is required');
      return false;
    } else {
      return true;
    }
  }

  IsApple() {
    if (appleAuth.isSupported) {
      return <AppleSignin navigation={this.props} />;
    }
  }

  //DEFAULT LOGIN
  handleLogin() {
    console.warn('click');
    if (this.ValidateFields()) {
      //DITO KAPAG MAY USERNAME AT PASSWORD
      // this.RequestAPILogin(this.state.username, this.state.password);
      this.firebaseLogin(this.state.username, this.state.password);
    }
  }
  //DEFAULT LOGIN

  // RequestAPILogin = async (uid, pwd) => {
  //   await Service.login(
  //     { uid: uid, pwd: pwd },
  //     (res) => {
  //       if (res.status) {
  //         Alert.alert(
  //           'Success!',
  //           res.msg,
  //           [
  //             {
  //               text: 'OK',
  //               onPress: async () => {
  //                 // await this.setState({userId: responseJson.id});
  //                 // await this.setState({ user_token: responseJson.token })
  //                 // await this.setState({is_LoggedIn: true});
  //                 try {
  //                   await AsyncStorage.setItem('auth', JSON.stringify(res.token));
  //                   await AsyncStorage.setItem('user_data', JSON.stringify(res));
  //                   console.log("this is user_data", JSON.stringify(res))
  //                   await AsyncStorage.removeItem('app_refer_state');
  //                   // this.props.navigation.navigate('Initializer', { task: 'login', token: res.token });

  //                   // Firebase Login Start
  //                   auth()
  //                     .signInWithEmailAndPassword(uid, password)
  //                     .then((response) => {
  //                       console.log("THIS IS FIREBASE SIGN IN RESPONSE", response)
  //                     })
  //                     .catch(error => {
  //                       alert(error)
  //                     })
  //                   // Firebase Login End

  //                   this.props.navigation.reset({
  //                     index: 0,
  //                     routes: [{ name: 'Tab' }],
  //                   });
  //                 } catch (e) {
  //                   console.warn(e);
  //                 }
  //               },
  //             },
  //           ],
  //           { cancelable: false }
  //         );
  //       } else {
  //         alert(res.msg);
  //         console.warn(responseJson);
  //       }
  //     },
  //     (err) => {
  //       alert(JSON.parse(err));
  //     }
  //   );
  // };

  firebaseLogin = async (eml, pwd) => {
    const email = eml
    const password = pwd
    console.log(email, password)

    // Firebase Login
    try {
      await auth()
        .signInWithEmailAndPassword(email, password)
        .then(async (response) => {
          // Firebase collection get
          // this.getFirebaseUserData(response)
        })
        .catch(error => {
          alert(error)
        })
    } catch (e) {
      console.log("firebase login error", e)
    }

    // Laravel API Login + Get User Data
    try {
      await Service.login(
        { uid: uid, pwd: pwd },
        (res) => {
          if (res.status) {
            Alert.alert(
              'Success!',
              res.msg,
              [
                {
                  text: 'OK',
                  onPress: async () => {
                    try {
                      await AsyncStorage.setItem('user_data', JSON.stringify(res));
                      await AsyncStorage.removeItem('app_refer_state');
                    } catch (e) {
                      console.warn(e);
                    }
                  },
                },
              ],
              { cancelable: false }
            );
          } else {
            alert(res.msg);
            console.warn(responseJson);
          }
        },
        (err) => {
          alert(JSON.parse(err));
        }
      );
    } catch (error) {
      console.log("Laravel login error", error)
    }

    this.props.navigation.reset({
      index: 0,
      routes: [{ name: 'Tab' }],
    });

  }

  async getFirebaseUserData(response) {
    try {
      const user = response.user
      const uid = user.uid

      const allUserData = firestore()
        .collection('user_data')
        .doc(uid)
        .get()
        .then(async (userData) => {
          if (userData.exists) {
            const userDataJson = {
              id: userData.data()?.id,
              refer_id: userData.data()?.refer_id,
              fname: userData.data()?.fname,
              lname: userData.data()?.lname,
              email: userData.data()?.email,
              birthdate: userData.data()?.birthdate,
              country_code: userData.data()?.country_code,
              contact_number: userData.data()?.contact_number,
              shop_name: userData.data()?.shop_name,
              username: userData.data()?.username,
              is_verify: userData.data()?.is_verify,
              is_security_question_set: userData.data()?.is_security_question_set,
              fb_id: userData.data()?.fb_id,
              image: userData.data()?.image,
              ratings: userData.data()?.ratings,
              products: userData.data()?.products,
              services: userData.data()?.services,
              reviews: userData.data()?.reviews,
              country_code: userData.data()?.country_code,
              notifications: userData.data()?.notifications,
              total_cash: userData.data()?.total_cash,
              product_sales: userData.data()?.product_sales,
              sales_commission: userData.data()?.sales_commission,
              downline_commission: userData.data()?.downline_commission,
            }
            console.log("userDataJson", userDataJson)

            const userDataResponse = {
              "msg": "Congratulations " + userData.data()?.fname + " " + userData.data()?.lname + " you successfully signed in.",
              "status": true,
              "status_code": "LOGIN_SUCCESS",
              "verified_user": "no",
              "two_factor_authenticated": "no",
              "first_login": false,
              "data": userDataJson,
            }
            console.log("this is userDataResponse", userDataResponse)

            await AsyncStorage.setItem('user_data', JSON.stringify(userDataResponse));

            // Move to home screen
            this.props.navigation.reset({
              index: 0,
              routes: [{ name: 'Tab' }],
            });
          } else {
            console.log("userData is not set")
          }
        })
        .catch(error => {
          alert("logging error", error)
          console.log("user_data get error", error)
        })

      // // Logout for testing
      // auth()
      //   .signOut()
      //   .then((response) => {
      //     console.log("THIS IS FIREBASE SIGN OUT RESPONSE", response)
      //   })
      //   .catch(error => {
      //     alert(error)
      //   })
    } catch (e) {
      console.log("error in moving to dashboard", e)
    }
  }

  //Login Designs
  render() {
    return (
      <ImageBackground source={Assets.signIn.signinBg} style={styles.imageContainer}>
        <StatusBar hidden />
        <KeyboardAwareScrollView>
          <View style={styles.loginHeaderContainer}>
            <View style={styles.headerContainer}>
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Image source={Assets.accountSettings.arrowDarkIcon} style={styles.goBack} resizeMode='contain' />
              </TouchableOpacity>
              <View style={{ paddingLeft: 10 }} />
              <Text style={styles.txtLogin}>Log In</Text>
            </View>
            <View style={{ flex: 1 }} />
          </View>

          <View style={styles.connectContainer}>
            <View style={{ paddingTop: 30 }} />

            <Animatable.View animation='fadeInUp' duration={100}>
              <TouchableOpacity onPress={() => loginWithFacebook(this.props)}>
                <LinearGradient
                  colors={['#1aa3ff', '#008ae6']}
                  style={styles.gradient}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Thumbnail source={Assets.signIn.fbIcon} square style={styles.imgConnect} />
                  <View style={{ flex: 1 }}>
                    <Text style={styles.txtConnect}>Connect with Facebook</Text>
                  </View>
                  <View style={{ width: 20, height: 20 }} />
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>

            <View style={{ paddingTop: 10 }} />
            <Animatable.View animation='fadeInUp' duration={300}>
              {/* <TouchableOpacity onPress={() => loginWithGoogle(this.props)}> */}
              <TouchableOpacity onPress={() => this.onGoogleButtonPress()}>
                <LinearGradient
                  colors={['#1aa3ff', '#008ae6']}
                  style={styles.gradient}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Thumbnail source={Assets.signIn.googleIcon} square style={styles.imgConnect} />
                  <View style={{ flex: 1 }}>
                    {!this.state.is_logging_in ? (
                      <Text style={styles.txtConnect}>Connect with Google</Text>
                    ) : (
                      <UIActivityIndicator color={'#ffffff'} count={12} size={30} />
                    )}
                  </View>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>

            <View style={{ paddingTop: 10 }} />
            <Animatable.View animation='fadeInUp' duration={300}>
              {this.IsApple()}
            </Animatable.View>

            <View style={{ paddingTop: 10 }} />
            <Animatable.View animation='fadeInLeft' duration={500} style={styles.inputContainer}>
              <Image source={Assets.signIn.person} style={styles.imgUser} />
              <TextInput
                style={styles.txtInput}
                onChangeText={(username) => this.setState({ username: username })}
                placeholderTextColor='#7f8c8d'
                // placeholder='Username or Email'
                placeholder='Email'
                value={this.state.username}
              // onChangeText={(email) => this.setState({ email: email })}
              // placeholderTextColor='#7f8c8d'
              // placeholder='Email'
              // value={this.state.email}
              />
            </Animatable.View>

            <View style={{ paddingTop: 10 }} />

            <Animatable.View animation='fadeInLeft' duration={700} style={styles.inputContainer}>
              <Image source={Assets.signIn.lock} style={styles.imgUser} />
              <TextInput
                style={styles.txtInput}
                onChangeText={(password) => this.setState({ password: password })}
                placeholderTextColor='#7f8c8d'
                placeholder='Password'
                secureTextEntry={true}
                value={this.state.password}
              />
            </Animatable.View>

            <View style={{ paddingTop: 10 }} />

            <Animatable.View animation='fadeInUp' duration={900}>
              <TouchableOpacity onPress={() => this.handleLogin()}>
                <LinearGradient
                  colors={['#f89522', '#f36e23']}
                  style={styles.btnLoginContainer}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Text style={styles.txtBtnLogin}>Log In</Text>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View>
            {/* <Animatable.View animation='fadeInUp' duration={900}>
              <TouchableOpacity onPress={() => this.GetDataFromFirebase()}>
                <LinearGradient
                  colors={['#f89522', '#f36e23']}
                  style={styles.btnLoginContainer}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Text style={styles.txtBtnLogin}>Log In</Text>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View> */}
            <View style={{ paddingTop: 10 }} />

            <View style={styles.container}>
              <Animatable.View animation='fadeInLeft' duration={1300}>
                <TouchableOpacity onPress={async () => {
                  this.props.navigation.navigate("Forgot Password")
                }} style={{ flex: 0 }}>
                  <Text style={styles.txtForgot}>Forgot Password?</Text>
                </TouchableOpacity>
              </Animatable.View>
              <View style={{ flex: 1 }} />
              <Animatable.View animation='fadeInRight' duration={1300}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Sign Up')} style={{ flex: 0 }}>
                  <Text style={styles.txtForgot}>New Here? Sign Up</Text>
                </TouchableOpacity>
              </Animatable.View>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ...state,
  };
};

export default Login;
