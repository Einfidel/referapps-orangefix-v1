import React from 'react'

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  BackHandler
} from 'react-native'
import { Thumbnail, Header } from 'native-base'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from './../../components/assets.manager'

import LinearGradient from 'react-native-linear-gradient';
const { width, height } = Dimensions.get('window')
class ResetPasswordSuccess extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentWillMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return true
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={Assets.signIn.signinBg} style={styles.imageContainer}>
          {/* <StatusBar hidden/> */}
          <View style={{ alignSelf: 'center', paddingTop: 80 }}>
            <Text style={styles.forgotText}>Success! </Text>
          </View>
          <View style={{ paddingVertical: 15, paddingHorizontal: 40 }}>
            <Text style={styles.forgotSubtext}>Your password has been reset successfully, you can now login your account</Text>
          </View>

          <TouchableOpacity onPress={() => this.props.navigation.navigate('Sign In')}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>Login</Text>
            </LinearGradient>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    )
  }
}

export default (ResetPasswordSuccess)

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: hp('3%'),
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: hp('1.7%'),
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    marginLeft: 15,
    marginRight: 40,
    fontFamily: 'Roboto'
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})
