import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal
} from 'react-native'
import { Thumbnail } from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from '../../components/assets.manager'
import auth from '@react-native-firebase/auth'
import Service from '../../components/api/service'
import LinearGradient from 'react-native-linear-gradient';
import WebView from 'react-native-webview'

export default function ForgotPassword({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [disabled, setDisabled] = useState(true);
  const [_csrf, setCsrf] = useState('');
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [emailValid, setEmailValid] = useState('');
  const [isFocus, setisFocus] = useState('');
  const [emailError, setEmailError] = useState('');
  const [textError, setTextError] = useState('');
  const [error, setError] = useState('');

  // ComponentDidMount
  useEffect(() => {
    setEmailValid(validateInputColor())
  }, [])

  const isEmail = (val) => {
    return val == '' ? false : true
  }

  const onChangeText = (email) => {
    setEmail(email);
    setEmailError(true);
    setError(false);
    validateInput();
  }

  const validateInput = () => {
    if (isEmail(email)) {
      return setDisabled(false);
    }
    setDisabled(true);
  }

  const validateInputColor = () => {
    let emailValid = isEmail(email);
    // let textError = emailError && (emailValid ? '' : 'The email must be a valid email address');
    let borderBottomColor = isFocus ? emailError ? (emailValid ? '#005880' : '#FF0000') : '#005880' : '#989696';
    let borderBottomWidth = isFocus ? 5 : 2
    return {
      borderBottomColor,
      borderBottomWidth,
      textError
    }
  }

  const handleOnBlur = () => {
    setisFocus(false);
  }

  const onFocusChange = () => {
    setisFocus(true);
  }

  const handleVerificationCode = async () => {
    if (isEmail(email)) {
      setModalVisible(true);
    } else {
      alert(JSON.stringify('The email must be a valid email address'))
    }

    return
    if (isEmail(email)) {
      const res = await Service.findAccountByEmail(email)
      console.log(res.data.info.data.contact_number)
      if (res) {
        if (res.status) {
          const confirmation = await auth().signInWithPhoneNumber("+63" + res.data.info.data.contact_number);
          // props.navigation.navigate("VerifyCode", {email: email, onConfirm: async (code) => {
          // 	let res = await confirmation.confirm(code)
          // 	alert(JSON.stringify(res))
          // }})
          navigation.navigate("VerifyCode", { email: email, number: "+63" + res.data.info.data.contact_number, verification: confirmation })
        }
      }
      // navigation.navigate('VerifyCode')
    }
    else {
      alert(JSON.stringify('The email must be a valid email address'))
    }

  }

  const handleRequestSendCode = async (token) => {
    if (isEmail(email)) {
      const res = await Service.findAccountByEmail(email)
      console.log(res.data.info.data.contact_number)
      if (res) {
        if (res.status) {
          let otp = await Service.authGoogleSendOtp(token, '+63' + res.data.info.data.contact_number)
          if (otp.error) {
            console.log(otp.error.errors)
            alert("Unable to send verification code. Try again later.")
          } else {
            console.log(otp)
            navigation.navigate("VerifyCode", {
              email: email,
              number: "+63" + res.data.info.data.contact_number,
              session: otp.sessionInfo,
              userId: res.data.id
            })
          }
        }
      }
    }
  }

  return (
    <View style={{ flex: 1 }}>

      {/* <Modal visible={true} style={{flex: 1}} >
						<WebView source={{uri: 'https://www.google.com'}} style={{flex: 1, width: '100%', height: 200}} />
					</Modal> */}

      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        {/* <StatusBar hidden/> */}
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity>

        <View style={{ alignSelf: 'center', paddingTop: 30 }}>
          <Text style={styles.forgotText}>Forgot your password? </Text>
        </View>
        <View style={{ paddingVertical: 10, paddingHorizontal: 40 }}>
          <Text style={styles.forgotSubtext}>Enter your email address and we'll send you a verification code to reset your password.</Text>
        </View>

        <View style={styles.inputStyles}>
          {/* <Thumbnail
                  source={require('../../../../assets/images/user.png')}
                  square
                  style={{
                    width:20,
                    height:20,
                    alignSelf:'center',
                    marginLeft:10
                  }}
                /> */}
          <TextInput
            autoCapitalize="none"
            style={styles.inputEmail}
            placeholderTextColor='#7f8c8d'
            placeholder='Email Address'
            onChangeText={(email) => onChangeText(email)}
            value={email}
            inputTextStyle={{
              borderBottomColor: emailValid.borderBottomColor,
              borderBottomWidth: emailValid.borderBottomWidth
            }}
            onBlur={() => handleOnBlur()}
            onFocus={() => onFocusChange()}

          >
          </TextInput>
          <Text style={styles.errorText}>{emailValid.textError}</Text>

        </View>
        {/* <View style={styles.error}> */}
        {/* </View> */}

        <TouchableOpacity onPress={() => handleVerificationCode()}
        >
          <LinearGradient
            colors={['#f89522', '#f58223', '#f36e23']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Submit</Text>
          </LinearGradient>
        </TouchableOpacity>
      </ImageBackground>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <WebView
              source={{ uri: 'https://oilredirectoryios.000webhostapp.com/smsverify.php' }}
              originWhitelist={['*']}
              javaScriptEnabledAndroid={true}
              onNavigationStateChange={(state) => {
                if (state.url.includes('?token')) {
                  let token = state.url.split('token=')[1]
                  console.log(token)
                  setModalVisible(false);
                  handleRequestSendCode(token)
                }
              }}
            />
          </View>
        </View>
      </Modal>
    </View>
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    marginLeft: 10,
    marginRight: 35,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginVertical: 20,
    height: 40,
    flexDirection: 'row',
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  error: {
    paddingTop: 5,
    backgroundColor: '#000',
    width: 200,
    fontSize: 14,
  },
  errorText: {
    color: '#000',
    fontFamily: 'Roboto',
    fontSize: 12,
    paddingTop: 3
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15
    // marginTop: 22
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    // padding: 35,
    // alignItems: "center",
    // elevation: 5
    resizeMode: 'contain',
    height: '22%',
    width: '70%'
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
})
