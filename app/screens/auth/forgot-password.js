import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
  Alert,
} from 'react-native'
import { Thumbnail } from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from '../../components/assets.manager'
import Service from '../../components/api/service'
import LinearGradient from 'react-native-linear-gradient';
import WebView from 'react-native-webview'
import AsyncStorage from '@react-native-community/async-storage';

import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import FastImage from 'react-native-fast-image'
import Icons from '../../components/icons';
import { marginTop } from 'styled-system';

export default function ForgotPassword({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [disabled, setDisabled] = useState(true);
  const [_csrf, setCsrf] = useState('');
  const [loading, setLoading] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [emailValid, setEmailValid] = useState('');
  const [isFocus, setisFocus] = useState('');
  const [emailError, setEmailError] = useState('');
  const [textError, setTextError] = useState('');
  const [error, setError] = useState('');

  const [user, setUser] = useState('');
  const [canResetPassword, setCanResetPassword] = useState(false);
  const [passwordIsVisible1, setPasswordIsVisible1] = useState(false);
  const [passwordIsVisible2, setPasswordIsVisible2] = useState(false);

  // ComponentDidMount
  useEffect(() => {
    init()
  }, [])

  const init = async () => {
    setLoading(true)
    setEmailValid(validateInputColor())
    console.log("route.params", route.params)

    if (route.params != undefined) {
      console.log("route.params.canResetPassword", route.params.canResetPassword)
      if (route.params.canResetPassword == true) {
        setCanResetPassword(route.params.canResetPassword)
      }
      setUser(route.params.user)
      setEmail(route.params.email)
    }

    setLoading(false)
  }

  const isEmail = (val) => {
    // return val == '' ? false : true
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,10})+$/;
    if (reg.test(email) === false) {
      console.warn('Email is Not Correct');
      return false;
    } else {
      console.warn('Email is Correct');
      return true;
    }
  }

  const onChangeText = (email) => {
    setEmail(email.trim());
    setEmailError(true);
    setError(false);
    // validateInput();
  }

  const validateInput = () => {
    if (isEmail(email)) {
      return setDisabled(false);
    }
    setDisabled(true);
  }

  const validateInputColor = () => {
    let emailValid = isEmail(email);
    // let textError = emailError && (emailValid ? '' : 'The email must be a valid email address');
    let borderBottomColor = isFocus ? emailError ? (emailValid ? '#005880' : '#FF0000') : '#005880' : '#989696';
    let borderBottomWidth = isFocus ? 5 : 2
    return {
      borderBottomColor,
      borderBottomWidth,
      textError
    }
  }

  const handleOnBlur = () => {
    setisFocus(false);
  }

  const onFocusChange = () => {
    setisFocus(true);
  }

  const handleVerificationCode = async () => {
    if (!loading) {
      if (isEmail(email)) {
        console.log("find account by email:", email)
        console.log('The email is valid')
        handleVerifyClick()
      } else {
        console.log('The email must be a valid email address')
        alert('The email must be a valid email address')
      }
    }
  }

  const handleVerifyClick = async (verified = 'no', reloads = 0) => {
    if (!loading) {
      setLoading(true);

      try {
        console.log('handleVerifyClick 1');
        await Service.findAccountByEmail(
          { email },
          (res) => {
            console.log('handleVerifyClick 2');
            if (verified === 'no') {
              if (res.status) {
                setLoading(false);
                console.log('verification_code', res.data.info.data.verification_code);
                setUser(res.data)
                console.log('res.data', res.data);
                let verifData = {
                  verifyCode: res.data.info.data.verification_code,
                  medium: res.sent_to,
                  expiration: res.verification_expiration,
                  user: user,
                }
                navigation.push("VerifyCode", {
                  verifData,
                  reloads,
                  onVerified: (verified, reloads = 0) => {
                    handleVerifyClick(verified, reloads);
                  },
                })
              } else {
                setLoading(false);
                alert(res.msg);
              }
            } else {
              if (res.status) {
                console.log('res.data', res.data);
                navigation.pop(1)
                navigation.pop(2)
                navigation.navigate("Forgot Password", { canResetPassword: true, userID: res.data.id, email: email })
                setCanResetPassword(true)

                setLoading(false);
              } else {
                setLoading(false);
                alert(res.msg);
              }
            }
          },
          (err) => {
            console.log(err)
            setLoading(false);
          },
        );
      } catch (e) {
        console.log(e)
        setLoading(false);
      }
    }
  }


  const handleResetPassword = async () => {
    if (!loading) {
      setLoading(true);

      var invalidPass = false;

      let lowerCaseReg = new RegExp("^(?=.*[a-z])");
      let upperCaseReg = new RegExp("^(?=.*[A-Z])");
      let digitsReg = new RegExp("^(?=.*[0-9])");
      let specialCharsReg = new RegExp("^(?=.*[!@#\$%\^&\*])");
      // let strongRegex = new RegExp("^(?=.{8,})");
      // let strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
      if (lowerCaseReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 Lowercase character');
        console.warn('password is has no lowerCaseReg');
        invalidPass = true
      }
      if (upperCaseReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 Uppercase character');
        console.warn('password is has no upperCaseReg');
        invalidPass = true
      }
      if (digitsReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 number');
        console.warn('password is has no digitsReg');
        invalidPass = true
      }
      if (specialCharsReg.test(password) === false) {
        Alert.alert('Password must have atleast 1 special character');
        console.warn('password is has no specialCharsReg');
        invalidPass = true
      }
      if (password !== password) {
        Alert.alert('Password does not match Confirm Password');
        console.warn('passwords are not the same');
        invalidPass = true
      }

      if (invalidPass) {
        setLoading(false)
        return;
      }

      console.log('route.params.userID', route.params.userID);


      try {
        await Service.authNewPassword(
          {
            id: route.params.userID,
            pwd: password,
            confirmed: confirmPassword
          },
          (res) => {
            console.log("authNewPassword res", res)
            if (res.status) {
              alert('Password successfully changed!');

              navigation.reset({
                index: 0,
                routes: [{ name: 'Sign In' }],
              });

              setLoading(false);
            } else {
              setLoading(false);
              alert(res.msg);
            }
          },
          (err) => console.log(err),
        );
      } catch (e) {
        console.log(e)
      }

      setLoading(false)
      return;
    }
  }


  return (
    <View style={{ flex: 1 }}>

      {/* <Modal visible={true} style={{flex: 1}} >
						<WebView source={{uri: 'https://www.google.com'}} style={{flex: 1, width: '100%', height: 200}} />
					</Modal> */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <WebView
              source={{ uri: 'https://oilredirectoryios.000webhostapp.com/smsverify.php' }}
              originWhitelist={['*']}
              javaScriptEnabledAndroid={true}
              onNavigationStateChange={(state) => {
                if (state.url.includes('?token')) {
                  let token = state.url.split('token=')[1]
                  console.log(token)
                  setModalVisible(false);
                  handleRequestSendCode(token)
                }
              }}
            />
          </View>
        </View>
      </Modal>


      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        {/* <StatusBar hidden/> */}
        <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity>

        {canResetPassword ?
          <View>
            <View style={{ alignSelf: 'center', paddingTop: 30 }}>
              <Text style={styles.forgotText}>Reset Password </Text>
            </View>
            <View style={{ paddingVertical: 10, paddingHorizontal: 40 }}>
              <Text style={styles.forgotSubtext}>Enter your new password.</Text>
            </View>

            {/* dupe text input to bypass secureTextEntry not working */}
            <View style={[styles.inputStyles, { display: 'none' }]}>
              <TextInput
                name="password"
                autoCapitalize="none"
                style={[styles.inputEmail, { flex: 1, flexDirection: 'row' }]}
                placeholderTextColor='#7f8c8d'
                placeholder='Password'
                onChangeText={(val) => setPassword(val.trim())}
                value={password}
                keyboardType='password'
                inputTextStyle={{
                  borderBottomColor: emailValid.borderBottomColor,
                  borderBottomWidth: emailValid.borderBottomWidth
                }}
                // onBlur={() => handleOnBlur()}
                // onFocus={() => onFocusChange()}
                secureTextEntry={!passwordIsVisible1}>
              </TextInput>
              <View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                  <TouchableOpacity onPress={() => {
                    setPasswordIsVisible1(!passwordIsVisible1)
                  }}>
                    {passwordIsVisible1 ?
                      <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                      :
                      <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                    }
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={styles.inputStyles}>
              <TextInput
                name="password"
                autoCapitalize="none"
                style={[styles.inputEmail, { flex: 1, flexDirection: 'row' }]}
                placeholderTextColor='#7f8c8d'
                placeholder='Password'
                onChangeText={(val) => setPassword(val.trim())}
                value={password}
                keyboardType='password'
                inputTextStyle={{
                  borderBottomColor: emailValid.borderBottomColor,
                  borderBottomWidth: emailValid.borderBottomWidth
                }}
                // onBlur={() => handleOnBlur()}
                // onFocus={() => onFocusChange()}
                secureTextEntry={!passwordIsVisible1}>
              </TextInput>
              <View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                  <TouchableOpacity onPress={() => {
                    setPasswordIsVisible1(!passwordIsVisible1)
                  }}>
                    {passwordIsVisible1 ?
                      <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                      :
                      <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                    }
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={[styles.inputStyles, { marginTop: 0 }]}>
              <TextInput
                name="confirmPassword"
                autoCapitalize="none"
                style={[styles.inputEmail, { flex: 1, flexDirection: 'row' }]}
                placeholderTextColor='#7f8c8d'
                placeholder='Confirm Password'
                onChangeText={(val) => setConfirmPassword(val.trim())}
                value={confirmPassword}
                keyboardType='password'
                // inputTextStyle={{
                //   borderBottomColor: emailValid.borderBottomColor,
                //   borderBottomWidth: emailValid.borderBottomWidth
                // }}
                // onBlur={() => handleOnBlur()}
                // onFocus={() => onFocusChange()}
                secureTextEntry={!passwordIsVisible2}>
              </TextInput>
              <View>
                <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                  <TouchableOpacity onPress={() => {
                    setPasswordIsVisible2(!passwordIsVisible2)
                  }}>
                    {passwordIsVisible2 ?
                      <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                      :
                      <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                    }
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <TouchableOpacity onPress={() => handleResetPassword()}>
              <LinearGradient
                colors={['#f89522', '#f58223', '#f36e23']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={styles.btnSubmit}>
                <Text style={styles.btnTextSubmit}>Submit</Text>
              </LinearGradient>
            </TouchableOpacity>

          </View>
          :
          <View>
            <View style={{ alignSelf: 'center', paddingTop: 30 }}>
              <Text style={styles.forgotText}>Forgot your password? </Text>
            </View>
            <View style={{ paddingVertical: 10, paddingHorizontal: 40 }}>
              <Text style={styles.forgotSubtext}>Enter your email address and we'll send you a verification code to reset your password.</Text>
            </View>

            <View style={styles.inputStyles}>

              <TextInput
                autoCapitalize="none"
                style={styles.inputEmail}
                placeholderTextColor='#7f8c8d'
                placeholder='Email Address'
                onChangeText={(email) => onChangeText(email)}
                value={email}
                keyboardType='email-address'
                inputTextStyle={{
                  borderBottomColor: emailValid.borderBottomColor,
                  borderBottomWidth: emailValid.borderBottomWidth
                }}
                onBlur={() => handleOnBlur()}
                onFocus={() => onFocusChange()}
              >
              </TextInput>
              <Text style={styles.errorText}>{emailValid.textError}</Text>

            </View>
            {/* <View style={styles.error}> */}
            {/* </View> */}

            <TouchableOpacity onPress={() => handleVerificationCode()}
            >
              <LinearGradient
                colors={['#f89522', '#f58223', '#f36e23']}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                style={styles.btnSubmit}>
                <Text style={styles.btnTextSubmit}>Submit</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        }
      </ImageBackground >

    </View >
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    marginLeft: 10,
    marginRight: 35,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginVertical: 20,
    height: 40,
    flexDirection: 'row',
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  error: {
    paddingTop: 5,
    backgroundColor: '#000',
    width: 200,
    fontSize: 14,
  },
  errorText: {
    color: '#000',
    fontFamily: 'Roboto',
    fontSize: 12,
    paddingTop: 3
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15
    // marginTop: 22
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    // padding: 35,
    // alignItems: "center",
    // elevation: 5
    resizeMode: 'contain',
    height: '22%',
    width: '70%'
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
})
