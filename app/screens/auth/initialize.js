import * as React from 'react';
import { View, Text } from 'react-native';
import { CommonActions } from '@react-navigation/native';

export default function Initialize(navigation) {
  React.useEffect(() => {
    // navigation.navigation.navigate('Tab');
    // navigation.navigation.dispatch(
    //   CommonActions.navigate({
    //     name: 'Home',
    //     params: { type: 'SIGN_IN', token: 'asdsdffsfafasfasf' },
    //   })
    // );
    navigation.navigation.reset({
      index: 0,
      routes: [ { name: 'Tab' } ],
    });
  }, []);

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Initializing...</Text>
    </View>
  );
}
