import React, { useState, useEffect } from 'react';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  StatusBar,
  Alert,
  NativeModules,
} from 'react-native';

function Test(props, { navigation, route }) {
  // ComponentDidMount
  useEffect(() => {
    console.log("props", props)
  }, [])

  //Login Designs
  return (
    <View>
      <Text>
        {props.testProps}
      </Text>
    </View>
  );
}

export default Test