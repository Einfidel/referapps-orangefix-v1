import React from 'react';
import { useState, useEffect } from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from '../../styles/profile/profileStyle';
import ReferAssets from '../../components/assets.manager';
import FastImage from 'react-native-fast-image'

export default function Unauthorized({ navigation, route }) {

  return (
    <View style={{ flex: 1, }}>
      <StatusBar backgroundColor="#00a14b" barStyle="light-content" />
      <ImageBackground source={ReferAssets.unAuth.unauthBg} style={styles.BG}>
        <View style={styles.bgOpacity}>
          <TouchableOpacity style={styles.btnSignIn} onPress={() => navigation.navigate('Sign In')}>
            <Text style={styles.txtSignIn}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnSignIn} onPress={() => navigation.navigate('Sign Up')}>
            <Text style={styles.txtSignIn}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={styles.authorizeContainer}>
        <FastImage source={ReferAssets.unAuth.unauthIcon} style={styles.imgAuthorize} resizeMode={FastImage.resizeMode.contain}></FastImage>
        <Text style={styles.txtJoin}>Join ReferApps Now</Text>
      </View>
    </View>
  )
}