import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal
} from 'react-native'
import { Thumbnail, Spinner } from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from '../../components/assets.manager'
import auth from '@react-native-firebase/auth'
import Service from '../../components/api/service'
import LinearGradient from 'react-native-linear-gradient';
import WebView from 'react-native-webview'
import stylings from '../../styles/signup/signup';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';

const { width, height } = Dimensions.get('window')

function TwoFactorAuth(props) {
  const [modalVisible, setmodalVisible] = useState(false)
  const [twoFactAuth, settwoFactorAuth] = useState('')

  const submit = async () => {

    if (!twoFactAuth) return

    setmodalVisible(true)
    let user = props.route.params
    console.log("User Id", user, user.id)

    let verify = await Service.TwoFactorAuth(user.id, twoFactAuth)
    console.log("Authentication", verify)

    try {
      await AsyncStorage.setItem('auth', JSON.stringify(verify.token));
      await AsyncStorage.setItem('user_data', JSON.stringify(verify));
      await AsyncStorage.removeItem('app_refer_state');
      this.props.navigation.reset({
        index: 0,
        routes: [{ name: 'Tab' }],
      });
    } catch (e) {
      console.warn(e);
    }

    setmodalVisible(false)
  }

  const onChangeText = (code) => {
    settwoFactorAuth(code)
  }

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        <TouchableOpacity onPress={() => props.navigation.goBack()} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={Assets.accountSettings.iconBack}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity>
        <View style={{ alignSelf: 'center', paddingTop: 30 }}>
          <Text style={styles.forgotText}>Two Factor Authentication </Text>
        </View>
        <View style={{ paddingVertical: 10, paddingHorizontal: 40 }}>
          <Text style={styles.forgotSubtext}>Enter your two factor authentication code.</Text>
        </View>
        <View style={styles.inputStyles}>
          <TextInput
            autoCapitalize="none"
            style={styles.inputEmail}
            placeholderTextColor='#7f8c8d'
            placeholder='Verification code'
            onChangeText={(code) => onChangeText(code)}
            value={twoFactAuth}
          // onBlur={() => this.handleOnBlur()}
          // onFocus={() => this.onFocusChange()}
          >
          </TextInput>
          {/* <Text style={{}}>{'Error message'}</Text> */}
        </View>
        <TouchableOpacity onPress={() => { submit() }}>
          <LinearGradient
            colors={['#f89522', '#f58223', '#f36e23']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Submit</Text>
          </LinearGradient>
        </TouchableOpacity>
        {modalVisible ? <Spinner /> : null}
      </ImageBackground>
    </View>
  )
}

export default TwoFactorAuth

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    marginLeft: 10,
    marginRight: 35,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginVertical: 20,
    height: 40,
    flexDirection: 'row',
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  error: {
    paddingTop: 5,
    backgroundColor: '#000',
    width: 200,
    fontSize: 14,
  },
  errorText: {
    color: '#000',
    fontFamily: 'Roboto',
    fontSize: 12,
    paddingTop: 3
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },

  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15
    // marginTop: 22
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    // padding: 35,
    // alignItems: "center",
    // elevation: 5
    resizeMode: 'contain',
    height: '22%',
    width: '70%'
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
})
