import React, { useState, useEffect } from 'react';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
  TextInput,
  StatusBar,
  Alert,
  NativeModules,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import { Thumbnail, Spinner } from 'native-base';
import styles from '../../styles/login/loginStyles';

import AsyncStorage from '@react-native-community/async-storage';
import { loginWithFacebook } from '../../components/Facebook.js';
import { loginWithGoogle } from '../../components/Google.js';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import { LoginButton, AccessToken, LoginManager, Profile } from 'react-native-fbsdk-next';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import AppleSignin from '../../components/AppleSignin';
import appleAuth from '@invertase/react-native-apple-authentication';
import FastImage from 'react-native-fast-image'
import Icons from '../../components/icons';

import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
import firestore from '@react-native-firebase/firestore';

import Test from './test.js';

export default function Login({ navigation, route }) {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [login_data, setLoginData] = useState(null);
  const [msg, setMsg] = useState('');
  const [userId, setUserId] = useState('');
  const [is_LoggedIn, setIsLoggedIn] = useState(true);
  const [refer_id, setReferId] = useState('');
  const [first_login, setFirstLogin] = useState('');
  const [country_code, setCountryCode] = useState('');
  const [is_logging_in, setIsLoggingIn] = useState(false);
  const [user_token, setUserToken] = useState('');
  const [passwordIsVisible, setPasswordIsVisible] = useState(false);
  const [loading, setLoading] = useState(false);

  const [users, setUsers] = useState(["one", "two", "three"]);


  // ComponentDidMount
  useEffect(() => {
    GoogleSignin.configure({
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '75162238761-1ap1o48ba5u13gu1abvr19764oo33r0o.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      iosClientId: '75162238761-oeds0c4roo7ordic2m2lt3tn7dqsindl.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }, [])

  useEffect(() => {

    init();
    // let data = await AsyncStorage.getItem('user_data');
    // let userData = data ? JSON.parse(data) : null;
    // setUserId(userData.data.id);

    // if (userId !== null) {
    //   navigation.goBack();
    // }
  })

  const init = async () => {
    // console.log("Profile.getCurrentProfile()", await Profile.getCurrentProfile());
  }

  //DEFAULT LOGIN
  const handleLogin = () => {
    if (ValidateFields()) {
      // RequestAPILogin(username, password);
      setLoading(true)
      systemLogin(username, password);
    }
  }
  //DEFAULT LOGIN

  const systemLogin = async (eml, pwd) => {
    const email = eml
    const password = pwd
    // console.log(email, password)

    // Laravel API Login + Get User Data
    try {
      await Service.login(
        { uid: email, pwd: password },
        async (res) => {
          if (res.status) {
            // Alert.Alert.alert(
            //   'Success!',
            //   res.msg,
            //   [
            //     {
            //       text: 'OK',
            //       onPress: async () => {
            try {
              await AsyncStorage.setItem('user_data', JSON.stringify(res));
              await AsyncStorage.removeItem('app_refer_state');

              // Firebase Register then Login
              // Implemented for migration of Web App 
              try {
                await auth()
                  .createUserWithEmailAndPassword(email, password)
                  .then(async (response) => {
                    console.log("Firebase account not found. Creating a new Firebase account for this user.")

                    // Firebase Login
                    try {
                      await auth()
                        .signInWithEmailAndPassword(email, password)
                        .then(async (response) => {
                          // Firebase collection get
                          // getFirebaseUserData(response)

                          // Reset Screen back to Home
                          setLoading(false)
                          navigation.reset({
                            index: 0,
                            routes: [{ name: 'Tab' }],
                          });
                        })
                        .catch(async (error) => {
                          setLoading(false)
                          Alert.alert("C1. Firebase Server Error:", error + "\n\nPlease try again later");
                          console.error("C1. Firebase Server Error:", error + "\n\nPlease try again later");

                          // If firebase did not successfully log in, sign out laravel
                          await AsyncStorage.removeItem('auth');
                          await AsyncStorage.removeItem('user_data');
                          await AsyncStorage.removeItem('chat');
                          await AsyncStorage.removeItem('fcmtoken');


                          await Profile.getCurrentProfile().then(async (res) => {
                            if (res != null) {
                              // FB signout start
                              await AsyncStorage.removeItem('fb_user_id');
                              LoginManager.logOut();
                              // FB signout end
                            }
                          })
                        })
                    } catch (e) {
                      setLoading(false)
                      Alert.alert("B1. Firebase Server Error:", error + "\n\nPlease try again later");
                      console.error("B1. Firebase Server Error:", error + "\n\nPlease try again later");
                    }

                  })
                  .catch(async (error) => {
                    console.log("Email already exists. Bypassing Firebase account creation.", error)

                    // Firebase Login
                    try {
                      await auth()
                        .signInWithEmailAndPassword(email, password)
                        .then(async (response) => {
                          // Firebase collection get
                          // getFirebaseUserData(response)


                          await messaging()
                            .getToken()
                            .then(async (token) => {
                              console.log("Firebase device token:", token);

                              await Service.login(
                                { uid: email, pwd: password, deviceToken: token },
                                async (res) => {
                                  console.log("Firebase login success:", response);
                                  setLoading(false)

                                  // Reset Screen back to Home

                                  // return

                                  navigation.reset({
                                    index: 0,
                                    routes: [{ name: 'Tab' }],
                                  });
                                },
                                (error) => {
                                  setLoading(false)
                                  Alert.alert("C. API Server Error: " + error + "\n\nPlease try again later");
                                  console.error("C. API Server Error:", error + "\n\nPlease try again later");
                                }
                              );
                            })
                            .catch(async (error) => {
                              setLoading(false)
                              Alert.alert("C3. Firebase Server Error:", error + "\n\nPlease try again later");
                              console.error("C3. Firebase Server Error:", error + "\n\nPlease try again later");
                            });
                        })
                        .catch(async (error) => {
                          setLoading(false)
                          Alert.alert("C2. Firebase Server Error:", error + "\n\nPlease try again later");
                          console.error("C2. Firebase Server Error:", error + "\n\nPlease try again later");

                          // If firebase did not successfully log in, sign out laravel
                          await AsyncStorage.removeItem('auth');
                          await AsyncStorage.removeItem('user_data');
                          await AsyncStorage.removeItem('chat');
                          await AsyncStorage.removeItem('fcmtoken');

                          await Profile.getCurrentProfile().then(async (res) => {
                            if (res != null) {
                              // FB signout start
                              await AsyncStorage.removeItem('fb_user_id');
                              LoginManager.logOut();
                              // FB signout end
                            }
                          })
                        })
                    } catch (error) {
                      setLoading(false)
                      Alert.alert("B2. Firebase Server Error:", error + "\n\nPlease try again later");
                      console.error("B2. Firebase Server Error:", error + "\n\nPlease try again later");
                    }
                  })
              } catch (error) {
                setLoading(false)
                Alert.alert("A. Firebase Server Error:", error + "\n\nPlease try again later");
                console.error("A. Firebase Server Error:", error + "\n\nPlease try again later");
              }
            } catch (error) {
              setLoading(false)
              Alert.alert("D. API Server Error:", error + "\n\nPlease try again later");
              console.error("D. API Server Error:", error + "\n\nPlease try again later");
            }
            //       },
            //     },
            //   ],
            //   { cancelable: false }
            // );
          } else {
            let errorString = '';

            if (res?.errors?.length > 0) {
              Object.keys(res.errors).map((key) => {
                res.errors[key].map((error, index) => {
                  errorString += "* " + error + '\n';
                })

                setLoading(false)
                Alert.alert("C.1 Signup Error:", errorString);
                console.error("C.1 Signup Error:", errorString);
                console.error("C.1 Signup Error:", res);
              });
            } else {
              setLoading(false)
              Alert.alert("C.2 Signup Error:", res.msg);
              console.error("C.2 Signup Error:", res.msg);
            }
          }
        },
        (error) => {
          setLoading(false)
          Alert.alert("B. API Server Error: " + error + "\n\nPlease try again later");
          console.error("B. API Server Error:", error + "\n\nPlease try again later");
        }
      );
    } catch (error) {
      setLoading(false)
      Alert.alert("A. API Server Error: " + error + "\n\nPlease try again later");
      console.error("A. API Server Error:", error + "\n\nPlease try again later");
    }

    setLoading(false)
  }

  // const getFirebaseUserData = async (response) => {
  //   try {
  //     const user = response.user
  //     const uid = user.uid

  //     const allUserData = firestore()
  //       .collection('user_data')
  //       .doc(uid)
  //       .get()
  //       .then(async (userData) => {
  //         if (userData.exists) {
  //           const userDataJson = {
  //             id: userData.data()?.id,
  //             refer_id: userData.data()?.refer_id,
  //             fname: userData.data()?.fname,
  //             lname: userData.data()?.lname,
  //             email: userData.data()?.email,
  //             birthdate: userData.data()?.birthdate,
  //             country_code: userData.data()?.country_code,
  //             contact_number: userData.data()?.contact_number,
  //             shop_name: userData.data()?.shop_name,
  //             username: userData.data()?.username,
  //             is_verify: userData.data()?.is_verify,
  //             is_security_question_set: userData.data()?.is_security_question_set,
  //             fb_id: userData.data()?.fb_id,
  //             image: userData.data()?.image,
  //             ratings: userData.data()?.ratings,
  //             products: userData.data()?.products,
  //             services: userData.data()?.services,
  //             reviews: userData.data()?.reviews,
  //             country_code: userData.data()?.country_code,
  //             notifications: userData.data()?.notifications,
  //             total_cash: userData.data()?.total_cash,
  //             product_sales: userData.data()?.product_sales,
  //             sales_commission: userData.data()?.sales_commission,
  //             downline_commission: userData.data()?.downline_commission,
  //           }
  //           console.log("userDataJson", userDataJson)

  //           const userDataResponse = {
  //             "msg": "Congratulations " + userData.data()?.fname + " " + userData.data()?.lname + " you successfully signed in.",
  //             "status": true,
  //             "status_code": "LOGIN_SUCCESS",
  //             "verified_user": "no",
  //             "two_factor_authenticated": "no",
  //             "first_login": false,
  //             "data": userDataJson,
  //           }
  //           console.log("this is userDataResponse", userDataResponse)

  //           await AsyncStorage.setItem('user_data', JSON.stringify(userDataResponse));

  //           // Move to home screen
  //           navigation.reset({
  //             index: 0,
  //             routes: [{ name: 'Tab' }],
  //           });
  //         } else {
  //           console.log("userData is not set")
  //         }
  //       })
  //       .catch(error => {
  //         Alert.alert("logging error", error)
  //         console.log("user_data get error", error)
  //       })
  //   } catch (e) {
  //     console.log("error in moving to dashboard", e)
  //   }
  // }

  const ValidateFields = () => {
    let uid = username == '' ? null : username;
    let pwd = password == '' ? null : password;
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)+$/;
    setLoading(false)

    if (uid == null && pwd == null) {
      // Alert.alert('Username and password is required');
      Alert.alert('Email and password is required');
      return false;
    } else if (uid == null && pwd != null) {
      // Alert.alert('Username is required');
      Alert.alert('Email is required');
      return false;
    } else if (pwd == null && uid != null) {
      Alert.alert('Password is required');
      return false;
    } else if (reg.test(uid) === false) {
      Alert.alert('User must enter a valid email');
      return false;
    } else {
      return true;
    }
  }

  const IsApple = () => {
    if (appleAuth.isSupported) {
      return <AppleSignin navigation={navigation} />;
    }
  }

  //Login Designs
  return (
    <ImageBackground source={Assets.signIn.signinBg} style={styles.imageContainer}>
      <StatusBar hidden />
      <KeyboardAwareScrollView>

        {/* {users.map((name, index) => {
          return <Test testProps={`testProps ${index} ${name}`} testNum={0} key={index} />
        }
        )} */}

        <View style={styles.loginHeaderContainer}>
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <FastImage source={Assets.accountSettings.arrowDarkIcon} style={styles.goBack} resizeMode={FastImage.resizeMode.contain} />
            </TouchableOpacity>
            <View style={{ paddingLeft: 10 }} />
            <Text style={styles.txtLogin}>Log In</Text>
          </View>
          <View style={{ flex: 1 }} />
        </View>
        {loading ? <Spinner /> : null}

        <View style={styles.connectContainer}>
          <View style={{ paddingTop: 30 }} />

          <Animatable.View animation='fadeInUp' duration={100}>
            <TouchableOpacity onPress={async () => {
              if (!loading) {
                setLoading(true)
                await loginWithFacebook(navigation)
                  .then((res) => {
                    console.log("facebook login res", res)
                    setLoading(false)
                  })
                  .catch((err) => {
                    console.log("facebook login err", err)
                    setLoading(false)
                  })
              }
            }}>
              {/* <TouchableOpacity onPress={() => onFacebookButtonPress()}> */}
              <LinearGradient
                colors={['#1aa3ff', '#008ae6']}
                style={styles.gradient}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              >
                <Thumbnail source={Assets.signIn.fbIcon} square style={styles.imgConnect} />
                <View style={{ flex: 1 }}>
                  <Text style={styles.txtConnect}>Connect with Facebook</Text>
                </View>
                <View style={{ width: 20, height: 20 }} />
              </LinearGradient>
            </TouchableOpacity>
          </Animatable.View>

          <View style={{ paddingTop: 10 }} />
          <Animatable.View animation='fadeInUp' duration={300}>
            {/* <TouchableOpacity onPress={() => Alert.alert("Under Maintenance")}> */}
            <TouchableOpacity onPress={async () => {
              // Alert.alert("Under Maintenance")
              if (!loading) {
                setLoading(true)
                await loginWithGoogle(navigation).then((res) => {
                  console.log("google login res", res)
                  setLoading(false)
                })
              }
            }}>
              {/* <TouchableOpacity onPress={() => onGoogleButtonPress()}> */}
              <LinearGradient
                colors={['#1aa3ff', '#008ae6']}
                style={styles.gradient}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              >
                <Thumbnail source={Assets.signIn.googleIcon} square style={styles.imgConnect} />
                <View style={{ flex: 1 }}>
                  {!is_logging_in ? (
                    <Text style={styles.txtConnect}>Connect with Google</Text>
                  ) : (
                    <UIActivityIndicator color={'#ffffff'} count={12} size={30} />
                  )}
                </View>
              </LinearGradient>
            </TouchableOpacity>
          </Animatable.View>

          <View style={{ paddingTop: 10 }} />
          <Animatable.View animation='fadeInUp' duration={300}>
            {IsApple()}
          </Animatable.View>

          <View style={{ paddingTop: 10 }} />
          <Animatable.View animation='fadeInLeft' duration={500} style={styles.inputContainer}>
            <FastImage source={Assets.signIn.person} style={styles.imgUser} />
            <TextInput
              style={styles.txtInput}
              onChangeText={(username) => setUsername(username.trim())}
              placeholderTextColor='#7f8c8d'
              // placeholder='Username or Email'
              placeholder='Email'
              value={username}
              keyboardType='email-address'
            // onChangeText={(email) => setEmail(email)}
            // placeholderTextColor='#7f8c8d'
            // placeholder='Email'
            // value={email}
            />
          </Animatable.View>

          <View style={{ paddingTop: 10 }} />

          <Animatable.View animation='fadeInLeft' duration={700} style={styles.inputContainer}>
            <FastImage source={Assets.signIn.lock} style={styles.imgUser} />
            <TextInput
              style={styles.txtInput}
              onChangeText={(password) => setPassword(password.trim())}
              placeholderTextColor='#7f8c8d'
              placeholder='Password'
              secureTextEntry={!passwordIsVisible}
              value={password}
            />
            <View>
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                <TouchableOpacity onPress={() => {
                  setPasswordIsVisible(!passwordIsVisible)
                }}>
                  {passwordIsVisible ?
                    <Icons.MaterialCommunityIcons name="eye-off" size={20} color="#7f8c8d" />
                    :
                    <Icons.MaterialCommunityIcons name="eye" size={20} color="#7f8c8d" />
                  }
                </TouchableOpacity>
              </View>
            </View>
          </Animatable.View>

          <View style={{ paddingTop: 10 }} />

          <Animatable.View animation='fadeInUp' duration={900}>
            <TouchableOpacity onPress={() => {
              if (!loading) {
                setLoading(true)
                handleLogin()
              }
            }}>
              <LinearGradient
                colors={['#f89522', '#f36e23']}
                style={styles.btnLoginContainer}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              >
                <Text style={styles.txtBtnLogin}>Log In</Text>
              </LinearGradient>
            </TouchableOpacity>
          </Animatable.View>
          {/* <Animatable.View animation='fadeInUp' duration={900}>
              <TouchableOpacity onPress={() => GetDataFromFirebase()}>
                <LinearGradient
                  colors={['#f89522', '#f36e23']}
                  style={styles.btnLoginContainer}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Text style={styles.txtBtnLogin}>Log In</Text>
                </LinearGradient>
              </TouchableOpacity>
            </Animatable.View> */}
          <View style={{ paddingTop: 10 }} />

          <View style={styles.container}>
            <Animatable.View animation='fadeInLeft' duration={1300}>
              <TouchableOpacity onPress={async () => {
                // Alert.alert("Under Maintenance")
                if (!loading) {
                  // setLoading(true)
                  // Alert.alert('Coming Soon!', 'This feature will be available soon.')
                  navigation.navigate("Forgot Password")
                }
              }} style={{ flex: 0 }}>
                <Text style={styles.txtForgot}>Forgot Password?</Text>
              </TouchableOpacity>
            </Animatable.View>
            <View style={{ flex: 1 }} />
            <Animatable.View animation='fadeInRight' duration={1300}>
              <TouchableOpacity onPress={() => {
                if (!loading) {
                  // setLoading(true)
                  navigation.navigate('Sign Up')
                }
              }} style={{ flex: 0 }}>
                <Text style={styles.txtForgot}>New Here? Sign Up</Text>
              </TouchableOpacity>
            </Animatable.View>
          </View>

        </View>
      </KeyboardAwareScrollView>
    </ImageBackground>
  );
}

