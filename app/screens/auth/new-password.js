// import React, {useState, useEffect} from 'react'
// import {View, Text, TextInput, TouchableOpacity} from 'react-native'
// import auth from '@react-native-firebase/auth'
// import Service from '../../components/api/service'

// export default function ForgotPassword(props){

//     const [pwd, setpwd] = useState('')
//     const [confirm, setconfirm] = useState('')
// 	const [valid, setvalid] = useState(false)

// 	const sendSMS = async () => {
// 		const confirmation = await auth().signInWithPhoneNumber("+639101738451");		
// 	}

// 	const Verify = async () => {
//         console.log("PROPS", props.route.params)
// 		if(!code){
// 			setvalid(false)
// 		}else{
// 			const res = await Service.authNewPassword(pwd, confirm)
//             console.log(res)
//             if(!res){
//                 alert("Sorry you entered the wrong password reset code. We will send you a new reset code for security purposes.")
//             }
// 		}
// 	}

//   return (
//      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
//         <Text>Create New Password</Text>

// 				<View>
// 					<Text>New Password</Text>
// 					<TextInput onChangeText={(res) => {
// 						setpwd(res)
// 						if(res) setvalid(true)
// 						else setvalid(false)
// 					}} value={code} />

//                     <Text>Confirm Password</Text>
//                     <TextInput onChangeText={(res) => {
// 						setconfirm(res)
// 						if(res) setvalid(true)
// 						else setvalid(false)
// 					}} value={code} />
// 					{/* {!valid ? <Text> Please enter the password reset code we sent to your mobile number.</Text> : null} */}
// 				</View>

// 				<TouchableOpacity style={{padding: 10, borderWidth: 1}} onPress={() => Verify()}>
// 					<Text>Save</Text>
// 				</TouchableOpacity>

//       </View>
// 	)
// }

import React from 'react'

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  BackHandler
} from 'react-native'
import { Thumbnail, Header } from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import LinearGradient from 'react-native-linear-gradient';
import Assets from '../../components/assets.manager'
import Service from '../../components/api/service'

const { width, height } = Dimensions.get('window')
class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      pwd1: '',
      pwd2: '',
      errors: false
    }

  }

  componentWillMount = async () => {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return true
  }

  render() {
    return (
      <View>
        <ImageBackground
          source={Assets.signIn.signinBg} style={styles.imageContainer}
        >
          {/* <StatusBar hidden/> */}
          <TouchableOpacity onPress={() => {
            if (this.props.route.params.onBackPress) {
              this.props.route.params.onBackPress()
            } else {
              this.props.navigation.navigate("Forgot Password")
            }
          }} style={{ marginLeft: 20, paddingTop: 50 }}>
            <Thumbnail
              source={require('../../assets/internal/icon_back.png')}
              style={{
                width: 18,
                height: 18,
              }} />
          </TouchableOpacity>
          <View style={{ alignSelf: 'center', paddingTop: 30 }}>
            <Text style={styles.forgotText}>Reset your password </Text>
          </View>
          <View style={{ paddingVertical: 15, alignSelf: 'center' }}>
            <Text style={styles.forgotSubtext}>Make sure your password is unpredictable</Text>
          </View>

          <View style={styles.inputStyles}>
            <TextInput
              autoCapitalize="none" autoCorrect={false}
              style={styles.inputEmail}
              placeholderTextColor='#7f8c8d'
              placeholder='New Password'
              value={this.state.pwd1}
              onChangeText={(val) => this.setState({ pwd1: val })}
              secureTextEntry={true}
            >
            </TextInput>
            <Text style={{ fontSize: 9, color: "red" }}>{this.state.errors && this.state.errors['password'] ? "* " + this.state.errors['password'][0] : ''}</Text>
          </View>
          <View style={[styles.inputStyles, { marginTop: -5 }]}>
            <TextInput
              autoCapitalize="none" autoCorrect={false}
              style={styles.inputEmail}
              placeholderTextColor='#7f8c8d'
              placeholder='Confirm Password'
              value={this.state.pwd2}
              onChangeText={(val) => this.setState({ pwd2: val })}
              secureTextEntry={true}
            >
            </TextInput>
            <Text style={{ fontSize: 9, color: "red" }}>{this.state.errors && this.state.errors['password_confirmation'] ? "* " + this.state.errors['password_confirmation'][0] : ''}</Text>
          </View>

          <TouchableOpacity onPress={async () => {
            const res = await Service.authNewPassword(this.props.route.params.userId, this.state.pwd1, this.state.pwd2)
            console.log(res)
            if (res.errors) {
              this.setState({ errors: res.errors })
            } else if (res.status) {
              this.props.navigation.navigate('New Password Success')
            }
          }}>
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>Reset Password</Text>
            </LinearGradient>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    )
  }
}

export default (ResetPassword)

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})