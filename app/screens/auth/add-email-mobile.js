import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Alert,
} from 'react-native'
import {
  Thumbnail,
  Header,
  Spinner,
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from './../../components/assets.manager'
import LinearGradient from 'react-native-linear-gradient';
import { Picker } from '@react-native-picker/picker';
import Service from '../../components/api/service'
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import moment from 'moment';

const { width, height } = Dimensions.get('window')

export default function AddEmailMobile({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [countryCode, setCountryCode] = useState('PH');
  const [contactNumber, setContactNumber] = useState('');
  const [loading, setLoading] = useState(false);
  const [emailHasBeenSent, setEmailHasBeenSent] = useState(false);
  const [userInputEmailCode, setUserInputEmailCode] = useState('');
  const [emailCode, setEmailCode] = useState('');
  const [emailCodeExpiration, setEmailCodeExpiration] = useState('');
  const [emailVerified, setEmailVerified] = useState(false);
  const [phoneHasBeenSent, setPhoneHasBeenSent] = useState(false);
  const [userInputPhoneCode, setUserInputPhoneCode] = useState('');
  const [phoneCode, setPhoneCode] = useState('');
  const [phoneCodeExpiration, setPhoneCodeExpiration] = useState('');
  const [phoneVerified, setPhoneVerified] = useState(false);
  const [submitTapped, setSignupTapped] = useState(false);
  const [user, setUser] = useState('');
  const [iosMargin, setiosMargin] = useState(0);
  const [timer, setTimer] = useState(0);
  const [emailTimeMark, setEmailTimeMark] = useState(0);
  const [emailTimerIsActive, setEmailTimerIsActive] = useState(false);
  const [phoneTimeMark, setPhoneTimeMark] = useState(0);
  const [phoneTimerIsActive, setPhoneTimerIsActive] = useState(false);

  useEffect(() => {
    init();
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    let interval = null;
    interval = setInterval(() => {
      // console.log("timer seconds", timer)
      setTimer((seconds) => seconds + 1);

      if (emailTimerIsActive) {
        if (timer >= (emailTimeMark + 30)) {
          console.log("emailTimeMark + 30", emailTimeMark + 30)
          setEmailTimerIsActive(false)
        }
      }

      if (phoneTimerIsActive) {
        if (timer >= (phoneTimeMark + 30)) {
          console.log("phoneTimeMark + 30", phoneTimeMark + 30)
          setPhoneTimerIsActive(false)
        }
      }
    }, 1000);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
      clearInterval(interval);
      // clearInterval(phoneInterval);
    }
  }, [timer])

  const resetEmailTimer = () => {
    setTimer(0);
  }

  const init = async () => {
    // setLoading(true);

    // console.log("init")
    if (timer < 10) {
      let ud = await AsyncStorage.getItem("user_data")
      let user = typeof ud == 'string' ? JSON.parse(ud) : null
      setUser(user)

      if (user.data.is_verify_contact_number === 'yes') {
        setPhoneVerified(true)
      }

      if (user.data.is_verify === 'yes') {
        setEmailVerified(true)
      }
    }

    // console.log("user", user)
  }

  const handleBackButtonClick = () => {
    // console.log("back")
    if (route.params?.from === "sign-up") {
      // navigation.navigate('Tab')
      navigation.reset({
        index: 0,
        routes: [{ name: 'Tab' }],
      });
    } else {
      return false;
    }
    // navigation.goBack();
    // return true
  }

  const checkOrSendEmailOrPhoneCode = async (type = 'email') => {
    setLoading(true);
    let data = {
      user_id: user.data.id,
      verified: 'no',
    }

    if (type === 'email') {
      data.email = email;
    } else if (type === 'contact_number') {
      data.contact_number = contactNumber
    }

    console.log("type", type)

    try {
      await Service.updateEmailOrContact(
        data,
        (res) => {
          if (res.msg === 'Email already exist.') {
            Alert.alert(
              'Alert',
              `The Email: ${email} has already been taken.\n` +
              "Please use another email.",
              {
                text: 'OK',
              }
            )
            setEmail('')
            setLoading(false);
            return
          } else if (res.msg === 'Contact number already exist.') {
            Alert.alert(
              'Alert',
              `The Number: ${contactNumber} has already been taken.\n` +
              "Please use another Contact Number.",
              {
                text: 'OK',
              }
            )
            setContactNumber('')
            setLoading(false);
            return
          }

          if (type === 'email') {
            setEmailTimerIsActive(true)
            setEmailTimeMark(timer)
            setEmailHasBeenSent(true)
            setEmailCode(res.verification_code)
            setEmailCodeExpiration(res.verification_expiration)
            setLoading(false);

            console.log("Email Code:", res.verification_code)
            console.log("Email Code Exp:", res.verification_expiration)

            Alert.alert(
              'Alert',
              "The verification code was successfully sent.\n" +
              "Please check your email for the Code.",
              {
                text: 'OK'
              }
            )

          } else if (type === 'contact_number') {
            setPhoneTimerIsActive(true)
            setPhoneTimeMark(timer)
            setPhoneHasBeenSent(true)
            setPhoneCode(res.verification_code)
            setPhoneCodeExpiration(res.verification_expiration)
            setLoading(false);

            console.log("Phone Code:", res.verification_code)
            console.log("Phone Code Exp:", res.verification_expiration)

            Alert.alert(
              'Alert',
              "The verification code was successfully sent.\n" +
              "Please check your contact number for the Code.",
              {
                text: 'OK'
              }
            )

          }
        },
        (err) => {
          setLoading(false);
          console.log(err)
        },
      );
    } catch (e) {
      setLoading(false);
      console.log(e)
    }
  }

  const verifyEmailOrPhoneCode = async (type = 'email') => {
    setLoading(true);
    let data = {
      user_id: user.data.id,
    }

    if (type === 'email') {
      data.email = email;

      if (emailCode != userInputEmailCode) {
        Alert.alert(
          'Alert',
          "Sorry you entered the wrong verification code,\n" +
          "Please make sure you entered the EXACT code.",
          {
            text: 'OK'
          }
        )
        setLoading(false);
        return
      }

      data.verified = 'yes';
    } else if (type === 'contact_number') {
      data.contact_number = contactNumber

      if (phoneCode != userInputPhoneCode) {
        Alert.alert(
          'Alert',
          "Sorry you entered the wrong verification code,\n" +
          "Please make sure you entered the EXACT code.",
          {
            text: 'OK'
          }
        )
        setLoading(false);
        return
      }

      data.verified = 'yes';
    }

    try {
      await Service.updateEmailOrContact(
        data,
        (res) => {

          if (res.status) {
            setLoading(false);
            if (type === 'email') {
              setEmailVerified(true)
              if (phoneVerified) {
                Alert.alert(
                  'Success!',
                  "You have successfully veirifed your Email!\n" +
                  "You have now verified both your Email and Phone!\n" +
                  "Congratulations and enjoy your journey!",
                  {
                    text: 'OK',
                    onPress: () => {
                      navigation.navigate('Home')
                    }
                  }
                )
              } else {
                Alert.alert(
                  'Success!',
                  "You have successfully veirifed your Email!\n" +
                  "If it's okay, verify your phone now too!",
                  {
                    text: 'OK'
                  }
                )
              }
            } else if (type === 'contact_number') {
              setPhoneVerified(true)
              if (emailVerified) {
                Alert.alert(
                  'Success!',
                  "You have successfully veirifed your Phone!\n" +
                  "You have now verified both your Email and Phone!\n" +
                  "Congratulations and enjoy your journey!",
                  {
                    text: 'OK',
                    onPress: () => {
                      navigation.navigate('Home')
                    }
                  }
                )
              } else {
                Alert.alert(
                  'Success!',
                  "You have successfully veirifed your Phone!\n" +
                  "If it's okay, verify your email now too!",
                  {
                    text: 'OK'
                  }
                )
              }
            }
          } else {
            setLoading(false);
            alert(res.msg);
          }

          if (type === 'email') {
            setEmailTimerIsActive(true)
            setEmailTimeMark(timer)
            setEmailHasBeenSent(true)
            setEmailCode(res.verification_code)
            setEmailCodeExpiration(res.verification_expiration)

            console.log("Email Code:", res.verification_code)
            console.log("Email Code Exp:", res.verification_expiration)

            Alert.alert(
              'Alert',
              "The verification code was successfully sent.\n" +
              "Please check your email for the Code.",
              {
                text: 'OK'
              }
            )

          } else if (type === 'contact_number') {
            setPhoneTimerIsActive(true)
            setPhoneTimeMark(timer)
            setPhoneHasBeenSent(true)
            setPhoneCode(res.verification_code)
            setPhoneCodeExpiration(res.verification_expiration)

            console.log("Phone Code:", res.verification_code)
            console.log("Phone Code Exp:", res.verification_expiration)

            Alert.alert(
              'Alert',
              "The verification code was successfully sent.\n" +
              "Please check your contact number for the Code.",
              {
                text: 'OK'
              }
            )

          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  const renderCheckEmailVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(email) => {
                setEmail(email);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email'
              value={email}
            />
          </View>
        </View>

        {emailTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {(emailTimeMark + 30) - timer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!emailIsValid(email)) {
                Alert.alert(
                  'Warning',
                  'Email must be valid!'
                )
              } else {
                setSignupTapped(true)
                checkOrSendEmailOrPhoneCode('email')
              }
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {emailHasBeenSent ?
                  "Resend Email Verification"
                  :
                  "Send Email Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderEmailOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <Text style={styles.forgotSubtext}>
            The code expires on {new moment(emailCodeExpiration, 'YYYY-MM-DD hh:mm').format('MMMM Do YYYY, h:mm a')}
          </Text>
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setUserInputEmailCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email Code'
              value={userInputEmailCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            verifyEmailOrPhoneCode('email')
            // emailVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Email Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const renderCheckPhoneVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60, marginBottom: iosMargin, marginTop: iosMargin }}>
          {/* {renderContactNumberError()} */}
          <View style={styles.cnumber}>
            <View style={{ resizeMode: 'contain' }}>
              <Picker
                selectedValue={countryCode}
                mode='dropdown'
                style={styles.datePicker}
                textStyle={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}
                onValueChange={(value) => setCountryCode(value)}
              >
                <Picker.Item label='PH (+63)' value='PH' />
                <Picker.Item label='US (+1)' value='US' />
              </Picker>
            </View>
            <TextInput
              style={styles.txtInput}
              maxLength={12}
              keyboardType='numeric'
              onChangeText={(value) => {
                if (+value || value == '' || value == 0) {
                  setContactNumber(value)
                }
              }}
              placeholderTextColor='#7f8c8d'
              placeholder='Cellphone number'
              value={contactNumber}
            />
          </View>
        </View>

        {phoneTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {(phoneTimeMark + 30) - timer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!contactNumber.trim()) {
                Alert.alert(
                  'Warning',
                  'Please enter your Contact Number first.'
                )
              } else {
                setSignupTapped(true)
                checkOrSendEmailOrPhoneCode('contact_number')
              }
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {phoneHasBeenSent ?
                  "Resend Contact Number Verification"
                  :
                  "Send Contact Number Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderPhoneOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <Text style={styles.forgotSubtext}>
            The code expires on {new moment(phoneCodeExpiration, 'YYYY-MM-DD hh:mm').format('MMMM Do YYYY, h:mm a')}
          </Text>
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setPhoneCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Contact Number Code'
              value={userInputPhoneCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            verifyEmailOrPhoneCode('contact_number')
            // phoneVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Contact Number Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const emailIsValid = (email) => {
    console.warn('Email', email);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      console.warn('Email is Not Correct');
      return false;
    } else {
      console.warn('Email is Correct');
      return true
    }
  }

  return (
    <View>
      <ImageBackground
        style={styles.imageContainer}
        source={Assets.signIn.signinBg}>
        {/* <StatusBar hidden/> */}

        {/* <TouchableOpacity onPress={() => {
          // navigation.goBack()
          // handleBackButtonClick()
        }} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity> */}
        <KeyboardAwareScrollView
          keyboardDismissMode="interactive"
          extraScrollHeight={20}
          style={{ paddingHorizontal: 5 }}>

          {loading ? <Spinner /> : null}

          <View style={{ alignSelf: 'center', paddingTop: 30 }}>
            <Text style={styles.forgotText}>Security wise! </Text>
          </View>

          <TouchableOpacity onPress={() => {
            // console.log('route.params.verifData.verifyCode', route.params.verifData.verifyCode);
          }} style={{ marginLeft: 20, paddingTop: 50 }}>
            <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
              <Text style={[styles.forgotSubtext, { fontSize: 18, }]}>
                Add your Email and Contact Number here to complete your registration process!
              </Text>
            </View>
          </TouchableOpacity>


          <View style={styles.fieldContainer}>

            {emailVerified ?
              null
              :
              <>
                {renderCheckEmailVerification()}

                {emailHasBeenSent ?
                  renderEmailOTPVerification()
                  : null}
              </>}

            <View style={{ paddingTop: 20, }} />
            <View style={{ borderTopWidth: 3, borderTopColor: '#8A8C8D', width: "100%" }} />
            <View style={{ paddingTop: 20, }} />

            {emailVerified ?
              null
              :
              <>
                {renderCheckPhoneVerification()}

                {phoneHasBeenSent ?
                  renderPhoneOTPVerification()
                  : null}
              </>}

            <TouchableOpacity style={{ marginTop: 20, marginBottom: 40 }}
              onPress={async () => {
                if (!loading) {
                  if (route.params?.from === "sign-up") {
                    // navigation.navigate('Tab')
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Tab' }],
                    });
                  } else {
                    navigation.navigate('My Profile');
                  }
                  // navigation.navigate('My Profile')
                  // navigation.goBack()
                  // navigation.reset({
                  //   index: 0,
                  //   routes: [{ name: 'Tab' }],
                  // });
                }
              }}
            >
              <Text style={{
                marginTop: 10,
                color: 'royalblue',
                fontSize: 14,
                textDecorationLine: "underline",
                textDecorationStyle: "solid",
                textDecorationColor: "#000",
                fontWeight: 'bold',
              }}>
                Skip for now
              </Text>
            </TouchableOpacity>

          </View>
        </KeyboardAwareScrollView>

      </ImageBackground>
    </View >
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  resendCodetext: {
    marginTop: 10,
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#414FCA',
    textAlign: 'center',
  },
  cooldownText: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    // borderRadius: 5,
    // marginHorizontal: 30,
    // height: 38,
    // backgroundColor: 'transparent',
    // justifyContent: 'center',
    // alignItems: 'center'

    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10
      },
    }),
    borderRadius: 5,
    width: width - 60,
    marginVertical: 10
  },
  btnTextSubmit: {
    // fontSize: 16,
    // fontFamily: 'Roboto',
    // textAlign: 'center',
    // color: '#ffffff',

    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  txtInputContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5
  },
  txtInput: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    paddingLeft: 10,
    ...Platform.select({
      ios: {
        paddingVertical: 15,
      },
      android: {
        // paddingVertical: 0,
      },
    }),
  },
  cnumber: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingRight: 10,
    paddingLeft: 5,
    borderRadius: 5,
    marginVertical: 5,
  },
  datePicker: {
    color: '#7f8c8d',
    width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {

      },
    })
  },
  fieldContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
})
