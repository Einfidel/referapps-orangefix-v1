import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Alert,
} from 'react-native'
import {
  Thumbnail,
  Header,
  Spinner,
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from './../../components/assets.manager'
import LinearGradient from 'react-native-linear-gradient';
import { Picker } from '@react-native-picker/picker';
import Service from '../../components/api/service'
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const { width, height } = Dimensions.get('window')

export default function AddEmailMobile({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [countryCode, setCountryCode] = useState('PH');
  const [contactNumber, setContactNumber] = useState('');
  const [loading, setLoading] = useState(true);
  const [emailHasBeenSent, setEmailHasBeenSent] = useState(false);
  const [emailCode, setEmailCode] = useState('');
  const [emailVerified, setEmailVerified] = useState(false);
  const [phoneHasBeenSent, setPhoneHasBeenSent] = useState(false);
  const [phoneCode, setPhoneCode] = useState(false);
  const [phoneVerified, setPhoneVerified] = useState(false);
  const [submitTapped, setSignupTapped] = useState(false);
  const [user, setUser] = useState('');
  const [iosMargin, setiosMargin] = useState(0);
  const [emailTimer, setEmailTimer] = useState(0);
  const [emailTimerIsActive, setEmailTimerIsActive] = useState(false);
  const [phoneTimer, setPhoneTimer] = useState(0);
  const [phoneTimerIsActive, setPhoneTimerIsActive] = useState(false);

  useEffect(() => {
    init();
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    console.log("timerIsActive", emailTimerIsActive)

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
      // clearInterval(phoneInterval);
      // clearInterval(emailInterval);
    }
  }, [])

  const startEmailTimer = () => {
    let emailInterval = null;
    console.log("emailTimerIsActive", emailTimerIsActive)
    if (emailTimerIsActive) {
      emailInterval = setInterval(() => {
        console.log("emailTimer seconds", emailTimer)
        // console.log("emailTimerIsActive", emailTimerIsActive)
        if (emailTimer >= 30) {
          resetEmailTimer();
          clearInterval(emailInterval);
        } else {
          setEmailTimer((seconds) => seconds + 1);
        }
      }, 1000);
    } else if (!emailTimerIsActive && emailTimer !== 0) {
      clearInterval(emailInterval);
    }
  }

  const startPhoneTimer = () => {
    let phoneInterval = null;
    if (phoneTimerIsActive) {
      phoneInterval = setInterval(() => {
        console.log("phoneTimer seconds", phoneTimer)
        // console.log("phoneTimerIsActive", phoneTimerIsActive)
        if (phoneTimer >= 30) {
          resetPhoneTimer();
          clearInterval(phoneInterval);
        } else {
          setPhoneTimer((seconds) => seconds + 1);
        }
      }, 1000);
    } else if (!phoneTimerIsActive && phoneTimer !== 0) {
      clearInterval(phoneInterval);
    }
  }

  const resetEmailTimer = () => {
    setEmailTimer(0);
    setEmailTimerIsActive(false);
  }

  const resetPhoneTimer = () => {
    setPhoneTimer(0);
    setPhoneTimerIsActive(false);
  }

  const init = async () => {
    setLoading(true);

    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)
    // console.log("_userdata", _userdata)

    setLoading(false);
  }

  const handleBackButtonClick = () => {
    // console.log("back")
    if (route.params?.from === "sign-up") {
      // navigation.navigate('Tab')
      navigation.reset({
        index: 0,
        routes: [{ name: 'Tab' }],
      });
    } else {
      return false;
    }
    // navigation.goBack();
    // return true
  }

  const handleAddEmailMobile = async () => {
    if (!emailIsValid(email)) {
      Toast.showWithGravity("Email must be a valid email address", Toast.SHORT, Toast.BOTTOM);
      return
    }

    submitAddEmailAndContact('no');
  }

  const submitAddEmailAndContact = async (verified = 'no', reloads = 0) => {
    let newEmail;
    let newContactNumber;

    if (email != '') {
      newEmail = email;
    } else {
      newEmail = null;
    }

    if (contactNumber != '') {
      newContactNumber = contactNumber;
    } else {
      newContactNumber = null;
    }

    let data = {
      user_id: user.data.id,
      email: newEmail,
      contact_number: newContactNumber,
    }

    try {
      await Service.resendAccountVerifCode(
        data,
        (res) => {
          if (verified === 'no') {
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.verification_code);
              let verifData = {
                verifyCode: res.verification_code,
                medium: res.sent_to,
                expiration: res.verification_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  submitAddEmailAndContact(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          } else {
            if (res.status) {
              setLoading(false);
              console.log('Data: ', res);
              Alert.alert(
                'Success',
                'New Address was added Successfully!',
                [
                  {
                    text: 'OK',
                    onPress: async () => {
                      if (route.params?.from == "Checkout") {
                        route.params.onAdd();
                      }
                      // navigation.goBack();
                      // navigation.goBack();
                      navigation.navigate('Addresses')
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              alert(res.msg);
            }
          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  const renderCheckEmailVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(email) => {
                setEmail(email);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email'
              value={email}
            />
          </View>
        </View>

        {emailTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {30 - emailTimer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!emailIsValid(email)) {
                Alert.alert(
                  'Warning',
                  'Email must be valid!'
                )
              } else {
                console.log("emailTimerIsActive is now true")
                setEmailTimerIsActive(true)
                // startEmailTimer();
                setEmailHasBeenSent(true)
                setSignupTapped(true)
              }
              // handleAddEmailMobile()
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {emailHasBeenSent ?
                  "Resend Email Verification"
                  :
                  "Send Email Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderEmailOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setEmailCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email Code'
              value={emailCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            // emailVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Email Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const renderCheckPhoneVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60, marginBottom: iosMargin, marginTop: iosMargin }}>
          {/* {renderContactNumberError()} */}
          <View style={styles.cnumber}>
            <View style={{ resizeMode: 'contain' }}>
              <Picker
                selectedValue={countryCode}
                mode='dropdown'
                style={styles.datePicker}
                textStyle={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}
                onValueChange={(value) => setCountryCode(value)}
              >
                <Picker.Item label='PH (+63)' value='PH' />
                <Picker.Item label='US (+1)' value='US' />
              </Picker>
            </View>
            <TextInput
              style={styles.txtInput}
              maxLength={10}
              keyboardType='numeric'
              onChangeText={(value) => {
                if (+value || value == '' || value == 0) {
                  setContactNumber(value)
                }
              }}
              placeholderTextColor='#7f8c8d'
              placeholder='Cellphone number'
              value={contactNumber}
            />
          </View>
        </View>

        {phoneTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {30 - phoneTimer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!contactNumber.trim()) {
                Alert.alert(
                  'Warning',
                  'Please enter your Contact Number first.'
                )
              } else {
                setPhoneTimerIsActive(true)
                startPhoneTimer()
                setSignupTapped(true)
                setPhoneHasBeenSent(true)
              }
              // handleAddEmailMobile()
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {phoneHasBeenSent ?
                  "Resend Contact Number Verification"
                  :
                  "Send Contact Number Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderPhoneOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setPhoneCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Contact Number Code'
              value={phoneCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            // phoneVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Contact Number Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const emailIsValid = (email) => {
    console.warn('Email', email);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      console.warn('Email is Not Correct');
      return false;
    } else {
      console.warn('Email is Correct');
      return true
    }
  }

  return (
    <View>
      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        {/* <StatusBar hidden/> */}

        {/* <TouchableOpacity onPress={() => {
          // navigation.goBack()
          // handleBackButtonClick()
        }} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity> */}
        <ScrollView>

          {loading ? <Spinner /> : null}

          <View style={{ alignSelf: 'center', paddingTop: 30 }}>
            <Text style={styles.forgotText}>Security wise! </Text>
          </View>

          <TouchableOpacity onPress={() => {
            // console.log('route.params.verifData.verifyCode', route.params.verifData.verifyCode);
          }} style={{ marginLeft: 20, paddingTop: 50 }}>
            <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
              <Text style={[styles.forgotSubtext, { fontSize: 18, }]}>
                Add your Email and Contact Number here to complete your registration process!
              </Text>
            </View>
          </TouchableOpacity>


          <View style={styles.fieldContainer}>

            {startEmailTimer()}
            {startPhoneTimer()}

            {renderCheckEmailVerification()}

            {emailHasBeenSent ?
              renderEmailOTPVerification()
              : null}

            <View style={{ paddingTop: 20, }} />
            <View style={{ borderTopWidth: 3, borderTopColor: '#8A8C8D', width: "100%" }} />
            <View style={{ paddingTop: 20, }} />

            {renderCheckPhoneVerification()}


            {phoneHasBeenSent ?
              renderPhoneOTPVerification()
              : null}

            <TouchableOpacity style={{ marginTop: 20, marginBottom: 40 }}
              onPress={async () => {
                if (!loading) {
                  if (route.params?.from === "sign-up") {
                    // navigation.navigate('Tab')
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Tab' }],
                    });
                  } else {
                    navigation.navigate('My Profile');
                  }
                  // navigation.navigate('My Profile')
                  // navigation.goBack()
                  // navigation.reset({
                  //   index: 0,
                  //   routes: [{ name: 'Tab' }],
                  // });
                }
              }}
            >
              <Text style={{
                marginTop: 10,
                color: 'royalblue',
                fontSize: 14,
                textDecorationLine: "underline",
                textDecorationStyle: "solid",
                textDecorationColor: "#000",
                fontWeight: 'bold',
              }}>
                Skip for now
              </Text>
            </TouchableOpacity>

          </View>
        </ScrollView>

      </ImageBackground>
    </View >
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  resendCodetext: {
    marginTop: 10,
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#414FCA',
    textAlign: 'center',
  },
  cooldownText: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    // borderRadius: 5,
    // marginHorizontal: 30,
    // height: 38,
    // backgroundColor: 'transparent',
    // justifyContent: 'center',
    // alignItems: 'center'

    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10
      },
    }),
    borderRadius: 5,
    width: width - 60,
    marginVertical: 10
  },
  btnTextSubmit: {
    // fontSize: 16,
    // fontFamily: 'Roboto',
    // textAlign: 'center',
    // color: '#ffffff',

    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  txtInputContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5
  },
  txtInput: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    paddingLeft: 10,
    ...Platform.select({
      ios: {
        paddingVertical: 15,
      },
      android: {
        // paddingVertical: 0,
      },
    }),
  },
  cnumber: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingRight: 10,
    paddingLeft: 5,
    borderRadius: 5,
    marginVertical: 5,
  },
  datePicker: {
    color: '#7f8c8d',
    width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {

      },
    })
  },
  fieldContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
})
import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Alert,
} from 'react-native'
import {
  Thumbnail,
  Header,
  Spinner,
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from './../../components/assets.manager'
import LinearGradient from 'react-native-linear-gradient';
import { Picker } from '@react-native-picker/picker';
import Service from '../../components/api/service'
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

const { width, height } = Dimensions.get('window')

export default function AddEmailMobile({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [countryCode, setCountryCode] = useState('PH');
  const [contactNumber, setContactNumber] = useState('');
  const [loading, setLoading] = useState(false);
  const [emailHasBeenSent, setEmailHasBeenSent] = useState(false);
  const [emailCode, setEmailCode] = useState('');
  const [emailVerified, setEmailVerified] = useState(false);
  const [phoneHasBeenSent, setPhoneHasBeenSent] = useState(false);
  const [phoneCode, setPhoneCode] = useState(false);
  const [phoneVerified, setPhoneVerified] = useState(false);
  const [submitTapped, setSignupTapped] = useState(false);
  const [user, setUser] = useState('');
  const [iosMargin, setiosMargin] = useState(0);
  const [emailTimer, setEmailTimer] = useState(0);
  const [emailTimerIsActive, setEmailTimerIsActive] = useState(false);
  const [phoneTimer, setPhoneTimer] = useState(0);
  const [phoneTimerIsActive, setPhoneTimerIsActive] = useState(false);

  useEffect(() => {
    init();
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    console.log("timerIsActive", emailTimerIsActive)

    let emailInterval = null;
    if (emailTimerIsActive) {
      emailInterval = setInterval(() => {
        console.log("emailTimer seconds", emailTimer)
        // console.log("emailTimerIsActive", emailTimerIsActive)
        if (emailTimer >= 30) {
          resetEmailTimer();
          clearInterval(emailInterval);
        } else {
          setEmailTimer((seconds) => seconds + 1);
        }
      }, 1000);
    } else if (!emailTimerIsActive && emailTimer !== 0) {
      clearInterval(emailInterval);
    }

    // let phoneInterval = null;
    // if (phoneTimerIsActive) {
    //   phoneInterval = setInterval(() => {
    //     console.log("phoneTimer seconds", phoneTimer)
    //     // console.log("phoneTimerIsActive", phoneTimerIsActive)
    //     if (phoneTimer >= 30) {
    //       resetPhoneTimer();
    //       clearInterval(phoneInterval);
    //     } else {
    //       setPhoneTimer((seconds) => seconds + 1);
    //     }
    //   }, 1000);
    // } else if (!phoneTimerIsActive && phoneTimer !== 0) {
    //   clearInterval(phoneInterval);
    // }

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
      clearInterval(emailInterval);
      // clearInterval(phoneInterval);
    }
  }, [emailTimer, phoneTimer])

  const resetEmailTimer = () => {
    setEmailTimer(0);
    setEmailTimerIsActive(false);
  }

  const resetPhoneTimer = () => {
    setPhoneTimer(0);
    setEmailTimerIsActive(false);
  }

  const init = async () => {
    // setLoading(true);

    console.log("init")
    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)
    // console.log("_userdata", _userdata)

    setLoading(false);
  }

  const handleBackButtonClick = () => {
    // console.log("back")
    if (route.params?.from === "sign-up") {
      // navigation.navigate('Tab')
      navigation.reset({
        index: 0,
        routes: [{ name: 'Tab' }],
      });
    } else {
      return false;
    }
    // navigation.goBack();
    // return true
  }

  const handleAddEmailMobile = async () => {
    if (!emailIsValid(email)) {
      Toast.showWithGravity("Email must be a valid email address", Toast.SHORT, Toast.BOTTOM);
      return
    }

    submitAddEmailAndContact('no');
  }

  const submitAddEmailAndContact = async (verified = 'no', reloads = 0) => {
    let newEmail;
    let newContactNumber;

    if (email != '') {
      newEmail = email;
    } else {
      newEmail = null;
    }

    if (contactNumber != '') {
      newContactNumber = contactNumber;
    } else {
      newContactNumber = null;
    }

    let data = {
      user_id: user.data.id,
      email: newEmail,
      contact_number: newContactNumber,
    }

    try {
      await Service.resendAccountVerifCode(
        data,
        (res) => {
          if (verified === 'no') {
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.verification_code);
              let verifData = {
                verifyCode: res.verification_code,
                medium: res.sent_to,
                expiration: res.verification_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  submitAddEmailAndContact(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          } else {
            if (res.status) {
              setLoading(false);
              console.log('Data: ', res);
              Alert.alert(
                'Success',
                'New Address was added Successfully!',
                [
                  {
                    text: 'OK',
                    onPress: async () => {
                      if (route.params?.from == "Checkout") {
                        route.params.onAdd();
                      }
                      // navigation.goBack();
                      // navigation.goBack();
                      navigation.navigate('Addresses')
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              alert(res.msg);
            }
          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  const renderCheckEmailVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(email) => {
                setEmail(email);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email'
              value={email}
            />
          </View>
        </View>

        {emailTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {30 - emailTimer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!emailIsValid(email)) {
                Alert.alert(
                  'Warning',
                  'Email must be valid!'
                )
              } else {
                setEmailTimerIsActive(true)
                setEmailHasBeenSent(true)
                setSignupTapped(true)
              }
              // handleAddEmailMobile()
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {emailHasBeenSent ?
                  "Resend Email Verification"
                  :
                  "Send Email Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderEmailOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setEmailCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Email Code'
              value={emailCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            // emailVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Email Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const renderCheckPhoneVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60, marginBottom: iosMargin, marginTop: iosMargin }}>
          {/* {renderContactNumberError()} */}
          <View style={styles.cnumber}>
            <View style={{ resizeMode: 'contain' }}>
              <Picker
                selectedValue={countryCode}
                mode='dropdown'
                style={styles.datePicker}
                textStyle={{
                  // fontFamily: 'Roboto',
                  fontSize: 12,
                  color: '#231f20',
                }}
                onValueChange={(value) => setCountryCode(value)}
              >
                <Picker.Item label='PH (+63)' value='PH' />
                <Picker.Item label='US (+1)' value='US' />
              </Picker>
            </View>
            <TextInput
              style={styles.txtInput}
              maxLength={10}
              keyboardType='numeric'
              onChangeText={(value) => {
                if (+value || value == '' || value == 0) {
                  setContactNumber(value)
                }
              }}
              placeholderTextColor='#7f8c8d'
              placeholder='Cellphone number'
              value={contactNumber}
            />
          </View>
        </View>

        {phoneTimerIsActive ?
          <TouchableOpacity>
            <LinearGradient
              colors={['#959595', '#979797', '#979797']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                Resend a new code in {30 - phoneTimer} seconds
              </Text>
            </LinearGradient>
          </TouchableOpacity>
          :
          <TouchableOpacity onPress={async () => {
            if (!loading) {
              if (!contactNumber.trim()) {
                Alert.alert(
                  'Warning',
                  'Please enter your Contact Number first.'
                )
              } else {
                setPhoneTimerIsActive(true)
                setSignupTapped(true)
                setPhoneHasBeenSent(true)
              }
              // handleAddEmailMobile()
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>
                {phoneHasBeenSent ?
                  "Resend Contact Number Verification"
                  :
                  "Send Contact Number Verification"}
              </Text>
            </LinearGradient>
          </TouchableOpacity>}
      </View>
    )
  }

  const renderPhoneOTPVerification = () => {
    return (
      <View>
        <View style={{ width: Dimensions.get('window').width - 60 }}>
          {/* {renderEmailError()} */}
          <View style={styles.txtInputContainer}>
            <TextInput
              style={styles.txtInput}
              onChangeText={(code) => {
                setPhoneCode(code);
              }}
              // validate(text)}
              placeholderTextColor='#7f8c8d'
              placeholder='Contact Number Code'
              value={phoneCode}
            />
          </View>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            // phoneVerified(true)
            // handleAddEmailMobile()
          }
        }}
        >
          <LinearGradient
            colors={['#22C75F', '#35AB60', '#21AB54']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Verify Contact Number Code</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    )
  }

  const emailIsValid = (email) => {
    console.warn('Email', email);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!reg.test(email)) {
      console.warn('Email is Not Correct');
      return false;
    } else {
      console.warn('Email is Correct');
      return true
    }
  }

  return (
    <View>
      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        {/* <StatusBar hidden/> */}

        {/* <TouchableOpacity onPress={() => {
          // navigation.goBack()
          // handleBackButtonClick()
        }} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity> */}
        <ScrollView>

          {loading ? <Spinner /> : null}

          <View style={{ alignSelf: 'center', paddingTop: 30 }}>
            <Text style={styles.forgotText}>Security wise! </Text>
          </View>

          <TouchableOpacity onPress={() => {
            // console.log('route.params.verifData.verifyCode', route.params.verifData.verifyCode);
          }} style={{ marginLeft: 20, paddingTop: 50 }}>
            <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
              <Text style={[styles.forgotSubtext, { fontSize: 18, }]}>
                Add your Email and Contact Number here to complete your registration process!
              </Text>
            </View>
          </TouchableOpacity>


          <View style={styles.fieldContainer}>

            {renderCheckEmailVerification()}

            {emailHasBeenSent ?
              renderEmailOTPVerification()
              : null}

            <View style={{ paddingTop: 20, }} />
            <View style={{ borderTopWidth: 3, borderTopColor: '#8A8C8D', width: "100%" }} />
            <View style={{ paddingTop: 20, }} />

            {renderCheckPhoneVerification()}


            {phoneHasBeenSent ?
              renderPhoneOTPVerification()
              : null}

            <TouchableOpacity style={{ marginTop: 20, marginBottom: 40 }}
              onPress={async () => {
                if (!loading) {
                  if (route.params?.from === "sign-up") {
                    // navigation.navigate('Tab')
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Tab' }],
                    });
                  } else {
                    navigation.navigate('My Profile');
                  }
                  // navigation.navigate('My Profile')
                  // navigation.goBack()
                  // navigation.reset({
                  //   index: 0,
                  //   routes: [{ name: 'Tab' }],
                  // });
                }
              }}
            >
              <Text style={{
                marginTop: 10,
                color: 'royalblue',
                fontSize: 14,
                textDecorationLine: "underline",
                textDecorationStyle: "solid",
                textDecorationColor: "#000",
                fontWeight: 'bold',
              }}>
                Skip for now
              </Text>
            </TouchableOpacity>

          </View>
        </ScrollView>

      </ImageBackground>
    </View >
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  resendCodetext: {
    marginTop: 10,
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#414FCA',
    textAlign: 'center',
  },
  cooldownText: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    // borderRadius: 5,
    // marginHorizontal: 30,
    // height: 38,
    // backgroundColor: 'transparent',
    // justifyContent: 'center',
    // alignItems: 'center'

    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10
      },
    }),
    borderRadius: 5,
    width: width - 60,
    marginVertical: 10
  },
  btnTextSubmit: {
    // fontSize: 16,
    // fontFamily: 'Roboto',
    // textAlign: 'center',
    // color: '#ffffff',

    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  txtInputContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5
  },
  txtInput: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    paddingLeft: 10,
    ...Platform.select({
      ios: {
        paddingVertical: 15,
      },
      android: {
        // paddingVertical: 0,
      },
    }),
  },
  cnumber: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingRight: 10,
    paddingLeft: 5,
    borderRadius: 5,
    marginVertical: 5,
  },
  datePicker: {
    color: '#7f8c8d',
    width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {

      },
    })
  },
  fieldContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
})
