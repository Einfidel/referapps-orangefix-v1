import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  Text,
  ImageBackground,
  Dimensions,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  BackHandler,
  Alert,
} from 'react-native'
import {
  Thumbnail,
  Header,
  Spinner,
} from 'native-base'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from './../../components/assets.manager'
import LinearGradient from 'react-native-linear-gradient';
import Service from '../../components/api/service'
import moment from 'moment';

const { width, height } = Dimensions.get('window')

export default function VerificationCode({ navigation, route }) {
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [codeToVerify, setCodeToVerify] = useState('');
  const [verificationCode, setVerificationCode] = useState('');
  const [loading, setLoading] = useState(true);
  const [seconds, setSeconds] = useState(0);
  const [timerIsActive, setTimerIsActive] = useState(route.params.reloads > 0);

  useEffect(() => {
    init();
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    console.log("route.params.reloads > 0", route.params.reloads > 0)
    console.log("route.params.reloads", route.params.reloads)
    console.log("timerIsActive", timerIsActive)
    // console.log("seconds", seconds)

    let interval = null;

    if (timerIsActive) {
      interval = setInterval(() => {
        console.log("seconds", seconds)
        // console.log("timerIsActive", timerIsActive)
        if (seconds >= 30) {
          resetTimer();
          clearInterval(interval);
        } else {
          setSeconds((seconds) => seconds + 1);
        }
      }, 1000);
    } else if (!timerIsActive && seconds !== 0) {
      clearInterval(interval);
    }

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
      clearInterval(interval);
    }
  }, [seconds])

  const init = () => {
    setLoading(true);
    console.log('route.params.verifData.verifyCode', route.params.verifData.verifyCode);
    setVerificationCode(route.params.verifData.verifyCode)
    setLoading(false);
  }

  const resetTimer = () => {
    setSeconds(0);
    setTimerIsActive(false);
  }

  const handleBackButtonClick = () => {
    navigation.pop(route.params.reloads + 1)
    // return true
  }

  return (
    <View>
      <ImageBackground
        source={Assets.signIn.signinBg} style={styles.imageContainer}>
        {/* <StatusBar hidden/> */}

        <TouchableOpacity onPress={() => {
          // navigation.goBack()
          handleBackButtonClick()
        }} style={{ marginLeft: 20, paddingTop: 50 }}>
          <Thumbnail
            source={require('../../assets/internal/icon_back.png')}
            style={{
              width: 18,
              height: 18,
            }} />
        </TouchableOpacity>

        {loading ? <Spinner /> : null}

        <View style={{ alignSelf: 'center', paddingTop: 30 }}>
          <Text style={styles.forgotText}>Security wise! </Text>
        </View>

        <TouchableOpacity onPress={() => {
          console.log('route.params.verifData.verifyCode', route.params.verifData.verifyCode);
        }} style={{ marginLeft: 20, paddingTop: 50 }}>
          <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
            <Text style={styles.forgotSubtext}>
              We have sent a verification code on your {route.params.verifData.medium}
            </Text>
            {route.params.verifData.expiration === 'infinite' ? null :
              <Text style={styles.forgotSubtext}>
                The code expires on {new moment(route.params.verifData.expiration, 'YYYY-MM-DD hh:mm').format('MMMM Do YYYY, h:mm a')}
              </Text>
            }

            {/* <Text style={styles.forgotSubtext}>{mobile}</Text> */}
          </View>
        </TouchableOpacity>

        <View style={styles.inputStyles}>
          <TextInput
            autoCapitalize="none" autoCorrect={false}
            style={styles.inputEmail}
            placeholderTextColor='#7f8c8d'
            placeholder='Verification Code'
            value={codeToVerify}
            onChangeText={(val) => {
              setCodeToVerify(val)
            }}
          >
          </TextInput>
        </View>

        <TouchableOpacity onPress={async () => {
          if (!loading) {
            if (codeToVerify !== verificationCode) {
              alert("Sorry you entered the wrong verification code")
            } else {
              setLoading(true);
              let verifData = {
                code: codeToVerify
              }
              route.params.onVerified('yes', 0, verifData);
            }
          }
        }}
        >
          <LinearGradient
            colors={['#f89522', '#f58223', '#f36e23']}
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 0 }}
            style={styles.btnSubmit}>
            <Text style={styles.btnTextSubmit}>Submit</Text>
          </LinearGradient>
        </TouchableOpacity>

        {timerIsActive ?
          <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
            <Text style={styles.cooldownText}>
              Resend a new code in {30 - seconds} seconds
            </Text>
            {/* <Text style={styles.forgotSubtext}>{mobile}</Text> */}
          </View>
          :
          <TouchableOpacity onPress={() => {
            if (!loading) {
              setLoading(true);
              resetTimer();
              route.params.onVerified('no', route.params.reloads + 1);
              Alert.alert(
                'Alert',
                'A new Verification Code has been sent to ' +
                route.params.verifData.medium + "."
              )
            }
          }} style={{ marginLeft: 20 }}>
            <View style={{ marginBottom: 10, paddingVertical: 15, paddingHorizontal: 20, alignSelf: 'center' }}>
              <Text style={styles.resendCodetext}>
                {
                  "Didn't receive a code?\n" +
                  "Click here to receive a new code."
                }
              </Text>
              {/* <Text style={styles.forgotSubtext}>{mobile}</Text> */}
            </View>
          </TouchableOpacity>}
      </ImageBackground>
    </View >
  )
}

const styles = StyleSheet.create({
  forgotText: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: '#7f8c8d',
  },
  forgotSubtext: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  resendCodetext: {
    marginTop: 10,
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#414FCA',
    textAlign: 'center',
  },
  cooldownText: {
    fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    textAlign: 'center',
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})
