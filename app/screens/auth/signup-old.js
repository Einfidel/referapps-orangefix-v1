import React, {
    useState,
} from 'react';
import Moment from 'moment';
import {
    View,
    ImageBackground,
    TouchableOpacity,
    Text,
    Image,
    TextInput,
    StatusBar,
    Dimensions,
    ScrollView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as Animatable from 'react-native-animatable';
import DateTimePicker from 'react-native-modal-datetime-picker';
// import { Thumbnail, Picker, Left, Header } from 'native-base';
import { Picker } from '@react-native-picker/picker';
import { Thumbnail, Left, Header } from 'native-base';
import Modal from 'react-native-root-modal';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';

import { loginWithFacebook } from '../../components/Facebook.js';
import { loginWithGoogle } from '../../components/Google.js';
import Assets from '../../components/assets.manager';
import styles from '../../styles/signup/signup';
import AsyncStorage from '@react-native-community/async-storage';
import AppleSignin from '../../components/AppleSignin';

import appleAuth from '@invertase/react-native-apple-authentication';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore'
// import storage from '@react-native-firebase/storage'
// import functions from '@react-native-firebase/functions'

const API_URL = 'https://www.referapps.com';
const URL_REGISTER = '/api/auth/register.json';
const API_TOKEN = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';

// const [isFetchingData, setIsFetchingData] = useState(false)

const { width, height } = Dimensions.get('window');
export default class Signup extends React.Component {
    headerMode = false;
    state = {
        fname: '',
        lname: '',
        email: '',
        username: '',
        password: '',
        password_confirmation: '',
        login_data: null,
        date: new Date(),
        passwordType: true,
        mode: 'date',
        isDateTimePickerVisible: false,
        birthdate: '',
        country_code: 'PH',
        contact_number: '',
        signup_tapped: false,
        validated: false,
        password_has_error: false,
        value: '',

        max_date: '',
        initial_date: '',

        loading: false,
        errors: [],
        is_logging_in: false,
    };

    _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

    _handleDatePicked = (date) => {
        this.setState({
            birthdate: Moment(date).format('Y-MM-DD'),
        });
        this._hideDateTimePicker();
    };

    _handleSignUp = async () => {
        // await this.setState({
        //   loading: true,
        // });

        let formData = new FormData();
        formData.append('fname', this.state.fname);
        formData.append('lname', this.state.lname);
        formData.append('email', this.state.email);
        formData.append('birthdate', this.state.birthdate);
        formData.append('country_code', this.state.country_code);
        formData.append('contact_number', this.state.contact_number);
        formData.append('username', this.state.username);
        formData.append('password', this.state.password);
        formData.append('password_confirmation', this.state.password_confirmation);
        formData.append('api_token', API_TOKEN);

        // console.log(API_URL + URL_REGISTER);

        fetch(API_URL + URL_REGISTER, {
            method: 'POST',
            body: formData,
        })
            .then((response) => response.json())
            .then(async (responseJson) => {
                // await this.setState({
                // loading: true,
                // });
                if (responseJson.status) {
                    alert(responseJson.msg);
                    // await AsyncStorage.setItem('auth', JSON.stringify(responseJson.token));
                    // Set user_data on Async here
                    await AsyncStorage.setItem('user_data', JSON.stringify(responseJson));
                    await this.setState({
                        loading: false,
                    });

                    // Firebase Login Start
                    try {
                        auth()
                            .createUserWithEmailAndPassword(this.state.email, this.state.password)
                            .then((response) => {
                                console.log("success creating account")
                                auth()
                                    .signInWithEmailAndPassword(this.state.email, this.state.password)
                                    .then(async (response) => {
                                        // Set Firebase user data collection here
                                        // this.setFirebaseUserData(response);
                                    })
                                    .catch((error) => {
                                        console.log("not loading properly setting")
                                        alert("Encountered a setting error.", error)
                                    })
                            })
                            .catch((error) => {
                                alert("Email already exists.", error)
                            })
                    } catch (e) {
                        console.log("error signup", e)
                    }
                    // Firebase Login End

                    // this.props.navigation.navigate('Home');
                    this.props.navigation.reset({
                        index: 0,
                        routes: [{ name: 'Tab' }],
                    });
                } else {
                    this.setState({ errors: responseJson.errors });
                    return false;
                }
            })
            .catch((error) => {
                console.error(error);
            });
    };

    async setFirebaseUserData(response) {
        try {
            const user = response.user
            const uid = user.uid

            var fname = this.state.fname;
            var lname = this.state.lname;
            var email = this.state.email;
            var birthdate = this.state.birthdate;
            var country_code = this.state.country_code;
            var contact_number = this.state.contact_number;
            var username = this.state.username;

            const newUserData = {
                id: uid,
                refer_id: uid,
                fname: fname,
                lname: lname,
                email: email,
                birthdate: birthdate,
                country_code: country_code,
                contact_number: contact_number,
                shop_name: null,
                username: username,
                is_verify: "yes",
                is_security_question_set: true,
                fb_id: null,
                image: "",
                ratings: "N/A",
                products: 0,
                services: 0,
                reviews: 0,
                country_code: "PH",
                notifications: 0,
                total_cash: 0,
                product_sales: 0,
                sales_commission: 0,
                downline_commission: 0,
                accountCreater: new Date(),
            }
            console.log("this is newUserData", newUserData)

            await firestore()
                .collection('user_data')
                .doc(uid)
                .set(newUserData)
                .then(async () => {
                    console.log("newUserData is set in firestore")
                    const response = {
                        "msg": "Congratulations" + newUserData.fname + " " + newUserData.lname + "a you successfully signed up.",
                        "status": true,
                        "status_code": "LOGIN_SUCCESS",
                        "verified_user": "no",
                        "two_factor_authenticated": "no",
                        "first_login": true,
                        "data": JSON.stringify(newUserData),
                    }
                    console.log("this is response", response)

                    await AsyncStorage.setItem('user_data', JSON.stringify(response));
                })
                .catch((e) => console.log("error", e))
        } catch (error) {
            console.log("register setFirebaseUserData error", error)
        }
    }


    // _handleSignUpFirebase = async () => {

    //   // Firebase Login Start
    //   try {
    //     auth()
    //       .createUserWithEmailAndPassword(this.state.email, this.state.password)
    //       .then((response) => {
    //         console.log("success creating account")
    //         auth()
    //           .signInWithEmailAndPassword(this.state.email, this.state.password)
    //           .then(async (response) => {
    //             const user = response.user
    //             const uid = user.uid

    //             var fname = this.state.fname;
    //             var lname = this.state.lname;
    //             var email = this.state.email;
    //             var birthdate = this.state.birthdate;
    //             var country_code = this.state.country_code;
    //             var contact_number = this.state.contact_number;
    //             var username = this.state.username;

    //             const newUserData = {
    //               id: uid,
    //               refer_id: uid,
    //               fname: fname,
    //               lname: lname,
    //               email: email,
    //               birthdate: birthdate,
    //               country_code: country_code,
    //               contact_number: contact_number,
    //               shop_name: null,
    //               username: username,
    //               is_verify: "yes",
    //               is_security_question_set: true,
    //               fb_id: null,
    //               image: "",
    //               ratings: "N/A",
    //               products: 0,
    //               services: 0,
    //               reviews: 0,
    //               country_code: "PH",
    //               notifications: 0,
    //               total_cash: 0,
    //               product_sales: 0,
    //               sales_commission: 0,
    //               downline_commission: 0,
    //               accountCreater: new Date(),
    //             }
    //             console.log("this is newUserData", newUserData)

    //             await firestore()
    //               .collection('user_data')
    //               .doc(uid)
    //               .set(newUserData)
    //               .then(async () => {
    //                 console.log("newUserData is set in firestore")
    //                 const response = {
    //                   "msg": "Congratulations" + newUserData.fname + " " + newUserData.lname + "a you successfully signed up.",
    //                   "status": true,
    //                   "status_code": "LOGIN_SUCCESS",
    //                   "verified_user": "no",
    //                   "two_factor_authenticated": "no",
    //                   "first_login": true,
    //                   "data": JSON.stringify(newUserData),
    //                 }
    //                 console.log("this is response", response)

    //                 await AsyncStorage.setItem('user_data', JSON.stringify(response));
    //               })
    //               .catch((e) => console.log("error", e))

    //             console.log("THIS IS FIREBASE SIGN IN RESPONSE", response)
    //           })
    //           .catch((error) => {
    //             // setIsFetchingData(false)
    //             console.log("not loading properly setting")
    //             alert("Encountered a setting error.", error)
    //           })
    //       })
    //       .catch((error) => {
    //         // setIsFetchingData(false)
    //         //prompts when duplicate email
    //         alert("Email already exist.", error)
    //       })
    //   } catch (e) {
    //     console.log("error sigup", e)
    //   }
    //   // Firebase Login End

    //   // this.props.navigation.navigate('Home');
    //   this.props.navigation.reset({
    //     index: 0,
    //     routes: [{ name: 'Tab' }],
    //   });
    // };

    _renderUsernameError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.username) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.username[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.username) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderFnameError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.fname) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.fname[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.fname) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderLnameError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.lname) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.lname[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.lname) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderEmailError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.email) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.email[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.email) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderBirthdateError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.birthdate) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.birthdate[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.birthdate) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderContactNumberError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.contact_number) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.contact_number[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.contact_number) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderPasswordError = () => {
        if (this.state.signup_tapped) {
            if (this.state.errors.password) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *{this.state.errors.password[0]}
                        </Text>
                    </View>
                );
            }
            if (!this.state.password) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    _renderPasswordConfirmationError = () => {
        if (this.state.signup_tapped) {
            if (!this.state.password_confirmation) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *This field is required.
                        </Text>
                    </View>
                );
            }
            if (this.state.password != this.state.password_confirmation) {
                return (
                    <View>
                        <Text
                            style={{
                                // fontFamily: 'Roboto',
                                fontSize: 12,
                                color: '#ff1a1a',
                            }}
                        >
                            *Password does not match.
                        </Text>
                    </View>
                );
            }
        }
        return <View />;
    };

    handleEmail2(email) {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(email) === false) {
            console.warn('Email is Not Correct');
            this.setState({ email: email });
            return false;
        } else {
            this.setState({ email: email });
            console.warn('Email is Correct');
        }
    }

    IsApple() {
        if (appleAuth.isSupported) {
            return <AppleSignin navigation={this.props} />;
        }
    }

    render() {
        Moment.locale('en');
        return (
            <ScrollView showsHorizontalScrollIndicator={false}>
                <ImageBackground source={Assets.signIn.signinBg} style={styles.bgImage}>
                    <StatusBar hidden />
                    <KeyboardAwareScrollView>
                        <View style={styles.headerContainer}>
                            <View style={styles.arrowBackContainer}>
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                    <Image source={Assets.accountSettings.arrowDarkIcon} style={styles.imgArrow} resizeMode='contain' />
                                </TouchableOpacity>
                                <View style={{ paddingLeft: 10 }} />
                                <Text style={styles.txtSignUp}>Sign Up</Text>
                            </View>
                            <View style={{ flex: 1 }} />
                        </View>

                        <View style={styles.fieldContainer}>
                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderFnameError()}
                                <Animatable.View animation='fadeInLeft' duration={500} style={styles.txtInputContainer}>
                                    <TextInput
                                        style={styles.txtInput}
                                        onChangeText={(fname) => this.setState({ fname: fname })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='First Name'
                                        value={this.state.fname}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderLnameError()}
                                <Animatable.View animation='fadeInLeft' duration={700} style={styles.txtInputContainer}>
                                    <TextInput
                                        textContentType='emailAddress'
                                        style={styles.txtInput}
                                        onChangeText={(lname) => this.setState({ lname: lname })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Last Name'
                                        value={this.state.lname}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderEmailError()}
                                <Animatable.View animation='fadeInLeft' duration={900} style={styles.txtInputContainer}>
                                    <TextInput
                                        style={styles.txtInput}
                                        onChangeText={(email) => this.handleEmail2(email)}
                                        // this.validate(text)}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Email'
                                        value={this.state.email}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderBirthdateError()}
                                <Animatable.View animation='fadeInLeft' duration={1100}>
                                    <TouchableOpacity onPress={this._showDateTimePicker} style={styles.birthdateContainer}>
                                        {this.state.birthdate ? (
                                            <Text style={styles.txtInputs}>{this.state.birthdate}</Text>
                                        ) : (
                                            <Text style={styles.txtInputs}>Birthdate</Text>
                                        )}
                                        <DateTimePicker
                                            date={this.state.date}
                                            mode='date'
                                            isVisible={this.state.isDateTimePickerVisible}
                                            onConfirm={this._handleDatePicked}
                                            onCancel={this._hideDateTimePicker}
                                        />
                                    </TouchableOpacity>
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderContactNumberError()}
                                <Animatable.View animation='fadeInLeft' duration={1300} style={styles.cnumber}>
                                    <View style={{ resizeMode: 'contain' }}>
                                        <Picker
                                            selectedValue={this.state.country_code}
                                            mode='dropdown'
                                            style={styles.datePicker}
                                            textStyle={{
                                                // fontFamily: 'Roboto',
                                                fontSize: 12,
                                                color: '#231f20',
                                            }}
                                            onValueChange={(value) => this.setState({ country_code: value })}
                                        >
                                            <Picker.Item label='PH (+63)' value='PH' />
                                            <Picker.Item label='US (+1)' value='US' />
                                        </Picker>
                                    </View>
                                    <TextInput
                                        style={styles.txtInput}
                                        maxLength={10}
                                        keyboardType='numeric'
                                        onChangeText={(value) => this.setState({ contact_number: value })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Cellphone number'
                                        value={this.state.contact_number}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderUsernameError()}
                                <Animatable.View animation='fadeInLeft' duration={1500} style={styles.txtInputContainer}>
                                    <TextInput
                                        style={styles.txtInput}
                                        onChangeText={(username) => this.setState({ username: username })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Username'
                                        value={this.state.username}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderPasswordError()}
                                <Animatable.View animation='fadeInLeft' duration={1800} style={styles.txtInputContainer}>
                                    <TextInput
                                        style={styles.txtInput}
                                        onChangeText={(password) => this.setState({ password: password })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Password'
                                        secureTextEntry={true}
                                        value={this.state.password}
                                    />
                                </Animatable.View>
                            </View>

                            <View style={{ width: Dimensions.get('window').width - 60 }}>
                                {this._renderPasswordConfirmationError()}
                                <Animatable.View animation='fadeInLeft' duration={2100} style={styles.txtInputContainer}>
                                    <TextInput
                                        style={styles.txtInput}
                                        onChangeText={(password_confirmation) =>
                                            this.setState({ password_confirmation: password_confirmation })}
                                        placeholderTextColor='#7f8c8d'
                                        placeholder='Password Confirmation'
                                        secureTextEntry={true}
                                        value={this.state.password_confirmation}
                                    />
                                </Animatable.View>
                            </View>

                            <Animatable.View animation='fadeInUp' duration={2400}>
                                <TouchableOpacity
                                    onPress={() => {
                                        this.setState(
                                            {
                                                signup_tapped: true,
                                            },
                                            () => {
                                                this._handleSignUp();
                                            }
                                        );
                                    }}
                                >
                                    <LinearGradient
                                        colors={['#f89522', '#f58223', '#f36e23']}
                                        style={styles.gradientBtnSignup}
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                    >
                                        <Text style={styles.txtGradientBtnSignup}>Sign Up</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Animatable.View>
                        </View>

                        <View style={styles.connectContainer}>

                            <Animatable.View animation='fadeInUp' duration={300}>
                                {this.IsApple()}
                            </Animatable.View>

                            <View style={{ paddingTop: 5 }} />

                            <Animatable.View animation='fadeInUp' duration={2700}>
                                <TouchableOpacity onPress={() => loginWithFacebook(this.props)}>
                                    <LinearGradient
                                        colors={['#1aa3ff', '#008ae6']}
                                        style={styles.gradient}
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                    >
                                        <View style={{ flex: 0 }}>
                                            <Thumbnail source={Assets.signIn.fbIcon} square style={styles.icons} />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.txtConnect}>Connect with Facebook</Text>
                                        </View>
                                        <View style={{ flex: 0 }}>
                                            <View style={{ width: 20, height: 20 }} />
                                        </View>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Animatable.View>

                            <Animatable.View animation='fadeInUp' duration={3000}>
                                <TouchableOpacity onPress={() => loginWithGoogle(this.props)}>
                                    <LinearGradient
                                        colors={['#1aa3ff', '#008ae6']}
                                        style={styles.gradient}
                                        start={{ x: 0, y: 0 }}
                                        end={{ x: 1, y: 0 }}
                                    >
                                        <View style={{ flex: 0 }}>
                                            <Thumbnail source={Assets.signIn.googleIcon} square style={styles.icons} />
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            {!this.state.is_logging_in ? (
                                                <Text style={styles.txtConnect}>Connect with Google</Text>
                                            ) : (
                                                <UIActivityIndicator color={'#ffffff'} count={12} size={30} />
                                            )}
                                        </View>
                                        <View style={{ flex: 0 }}>
                                            <View style={{ width: 20, height: 20 }} />
                                        </View>
                                    </LinearGradient>
                                </TouchableOpacity>
                            </Animatable.View>

                            <Animatable.View animation='fadeInUp' duration={3300} style={styles.noteContainer}>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Sign In')} style={{ flex: 0 }}>
                                    <Text style={styles.txtNote}>Already have an account?</Text>
                                </TouchableOpacity>
                            </Animatable.View>

                            <Animatable.View animation='fadeInUp' duration={3000} style={{ padding: 10 }} />
                        </View>
                    </KeyboardAwareScrollView>

                    <Modal visible={this.state.loading} style={styles.modalLoading}>
                        <View style={styles.processingContainer}>
                            <MaterialIndicator color={'#ffffff'} size={70} />
                            <Text style={styles.txtProcessing}>Processing...</Text>
                        </View>
                    </Modal>
                </ImageBackground>
            </ScrollView>
        );
    }
}
