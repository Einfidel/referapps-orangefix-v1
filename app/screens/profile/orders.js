import React from 'react';
import { ScrollView, View, Text, Image, ImageBackground, Dimensions, RefreshControl, TouchableOpacity } from 'react-native';
import Assets from '../../components/assets.manager';
import Icons from '../../components/icons';

export default function HomeScreen(props) {
  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
          <Icons.Foundation name="ticket" size={20} color="#66b545" />
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>Service Vouchers</Text>
        </View>
        <View style={{ flex: 3.5 }}></View>
        <TouchableOpacity onPress={() => props.navigation.navigate("Vouchers")} style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
          <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>Service Vouchers</Text>
          <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
        </TouchableOpacity>
      </View>
      <View style={{ flex: 0, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4.25, flexDirection: 'row', padding: 15 }}>
          <Icons.AntDesign name="profile" size={20} color="#66b545" />
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>Product Orders</Text>
        </View>
        <View style={{ flex: 1 }}></View>
        <TouchableOpacity onPress={() => props.navigation.navigate("PurchaseHistory")} style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
          <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View Purchase History</Text>
          <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
        </TouchableOpacity>
      </View>
      {/* <View style={{flex: 0, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff"}}>
        <View style={{flex: 15, flexDirection: 'row', padding: 15}}>
          <Icons.AntDesign name="clockcircleo" size={20} color="#66b545" />
          <Text style={{marginLeft: 10, fontSize: 12, color: "#00000095"}}>Transactions</Text>
        </View>
        <View style={{flex: 1}}></View>
        <TouchableOpacity onPress={() => props.navigation.navigate("Transactions")} style={{flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center'}}>
          <Text style={{textAlign: 'right', fontSize: 12, color: "darkorange"}}>History</Text>
          <Icons.AntDesign name="right" style={{flex: 0, padding: 2.5}} color="darkorange" />
        </TouchableOpacity>
      </View>    */}
    </View>
  );
}