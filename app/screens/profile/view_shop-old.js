import React from 'react';

import {
  Dimensions,
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import { Container, Input, Item, Content, } from 'native-base';
import FastImage from 'react-native-fast-image';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import styles from '../../styles/profile/view_your_shop';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';

const AllProducts = () => (
  <View style={{ backgroundColor: '#ffffff', }}>
    <View style={styles.allProductsView}>
      <View style={styles.titleBorderWidth}>
        <TouchableOpacity style={styles.titlesPosition} >
          <Text style={styles.titles}>
            Popular
          </Text>
        </TouchableOpacity>
      </View>

      <View style={styles.titleBorderWidth}>
        <TouchableOpacity style={styles.titlesPosition}>
          <Text style={styles.titles}>
            Latest
          </Text>
        </TouchableOpacity>
      </View>

      <View style={styles.titleBorderWidth}>
        <TouchableOpacity style={styles.titlesPosition}>
          <Text style={styles.titles}>
            Top Sales
          </Text>
        </TouchableOpacity>
      </View>

      <View style={styles.priceBorderWidth}>
        <TouchableOpacity style={styles.titlesPosition}>
          <Text style={styles.titles}>
            Price
          </Text>
        </TouchableOpacity>
      </View>
    </View>
    <View style={{ paddingVertical: 25 }}>
      <Text style={{ fontSize: 12, color: '#7f8c8d', fontStyle: 'italic', textAlign: 'center' }}>There are no items yet.</Text>
    </View>
  </View>
);

const Referred = () => (
  <View style={styles.referredSection}>
    <Text style={styles.referredText}>This user have no Referred items yet.</Text>
  </View>
);

const Overview = () => (
  <View style={styles.overviewSection}>
    <View>
      <View style={{ flexDirection: 'row' }}>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.text1}>Member since </Text>
        </View>
        <View style={{ flexDirection: 'column' }}>
          <Text style={styles.dateText}>Mar 2020</Text>
        </View>
      </View>
      <View style={styles.locationSection}>
        <FastImage
          source={Assets.stores.locationIcon}
          style={styles.locationIcon}
          tintColor={'#080808'}
        />
        <Text style={styles.locationText}>Asia/Manila</Text>
      </View>
      <View style={{ paddingTop: 25 }}>
        <Text style={styles.text1}>Interesting</Text>
        <View style={styles.emailSection}>
          <FastImage
            source={Assets.accountSettings.emailIcon}
            style={styles.emailIcon}
            tintColor='black'
          />
          <Text style={styles.emailText}>ardianonavarro.viray@yahoo.com</Text>
        </View>
      </View>
      <View style={{ paddingTop: 12 }}>
        <Text style={styles.text1}>Other Accounts</Text>
        <View style={{ paddingTop: 5 }}>
          <FastImage
            source={Assets.accountSettings.socialIcon1}
            style={styles.facebookIcon}
          />
        </View>
      </View>
    </View>
  </View>
);

class ViewYourShop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'allproducts', title: 'All Products' },
        { key: 'referred', title: 'Referred' },
        { key: 'overview', title: 'Overview' },
      ],
      shop: {}
    }
  }

  componentWillMount = async () => {
    let db = await AsyncStorage.getItem('user_data');
    let user = typeof db == 'string' ? JSON.parse(db) : null
    if (!user) return
    let id = this.props.route.params?.shop_id || user.data.id
    let shop = await Service.getShopProfile(id)
    console.log("SHOP DATA", shop)
    this.setState({ shop: shop.data })
  }

  _renderLabel = props => ({ route, focused, }) => {
    const color = focused ? '#231f20' : '#7f8c8d'
    return (
      <Text style={[{ fontSize: 16 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  renderAvatar = () => {
    let shop = this.state.shop?.info?.data?.shop
    return (
      <Image
        source={shop ? { uri: shop.full_path } : Assets.myProfile.face}
        style={styles.defaultProfile}
      />
    );
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={Assets.myProfile.bg}
          style={styles.headerCoverImage} >
          <View style={styles.backgroundImageContainer}>
            <Content>
              <View>
                <View style={styles.MainContainer}>
                  <View style={styles.headerRow}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.touchableBackButton}>
                      <FastImage
                        source={Assets.accountSettings.arrowDarkIcon}
                        style={styles.backIcon}
                        tintColor={'#ffffff'}
                      />
                    </TouchableOpacity>
                    <View style={{ paddingLeft: hp('2.50%'), }}>
                      <Item style={styles.ItemContainer} last>
                        <FastImage
                          source={Assets.accountSettings.searchHelpcenter}
                          style={styles.searchIcon}
                        />
                        <Input
                          placeholder='search'
                          placeholderTextColor='#7f8c8d'
                          style={styles.searchText}
                          onChangeText={(search) => this.setState({ search })}
                        />
                      </Item>
                    </View>
                    <TouchableOpacity style={styles.touchableChatIcon} onPress={() => {
                      Alert.alert('Coming Soon!', 'This feature will be available soon.')
                      // this.props.navigation.navigate("Messages")
                    }}>
                      <FastImage
                        source={Assets.myProfile.chatIcon}
                        style={[styles.cartIcon, { marginLeft: 10, marginRight: 5 }]}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchableCartIcon} onPress={() => this.props.navigation.navigate('Cart Screen')}>
                      <FastImage
                        source={Assets.myProfile.cartIcon}
                        style={styles.cartIcon}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={[styles.profileRow]}>
                  {this.renderAvatar()}
                  <View style={[styles.column1, { paddingBottom: hp('0%') }]}>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.userNameText, { width: wp('30%') }]}>{this.state.shop.shop_name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Products : </Text>
                      <Text style={styles.productValues}>{this.state.shop.products}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Services : </Text>
                      <Text style={styles.Values}>{this.state.shop.services}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Reviews : </Text>
                      <Text style={styles.Values}>{this.state.shop.reviews}</Text>
                    </View>
                  </View>
                  <View style={[styles.column2, { paddingBottom: 0, width: wp('50%') }]}>
                    <Text style={styles.ShopRatingText}>Shop Rating</Text>
                    <FastImage
                      source={Assets.stores.starIcon}
                      style={styles.starIcon}
                      tintColor={'#ffa012'}
                    />
                    <View style={{ flexDirection: 'row', }}>
                      <Text style={styles.ratingText}>{this.state.shop.ratings} out of 5</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Content>
          </View>
        </ImageBackground>
        <Content>
          <TabView
            navigationState={this.state}
            renderScene={SceneMap({
              allproducts: AllProducts,
              referred: Referred,
              overview: Overview,
            })}
            renderTabBar={props =>
              <TabBar
                {...props}
                labelStyle={{
                  // fontFamily: 'Roboto', 
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 2.50, borderBottomColor: '#f36e23' }}
                style={{ backgroundColor: '#ffffff', elevation: 0 }}
                renderLabel={this._renderLabel(props)}
              />
            }
            onIndexChange={index => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
          />
        </Content>
      </Container>
    );
  }

}

export default ViewYourShop;