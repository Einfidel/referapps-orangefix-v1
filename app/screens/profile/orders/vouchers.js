import React from 'react';
import { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Text, Modal, TouchableOpacity, Dimensions } from 'react-native';
import { Container, Header, Thumbnail, Content, Card, Spinner } from 'native-base';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../../components/assets.manager';
import Icons from '../../../components/icons';
import QRCode from 'react-native-qrcode-svg';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../../components/api/service';
import FastImage from 'react-native-fast-image';
const { width, height } = Dimensions.get('window');
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default function Vouchers({ navigation, route }) {
  let logoFromFile = Assets.specificProduct.icon1;

  const [availableVouchers, setAvailableVouchers] = useState();
  const [unavailableVouchers, setUnavailableVouchers] = useState();
  const [recurringVouchers, setRecurringVouchers] = useState();
  const [modal, setModal] = useState(false);
  const [code, setCode] = useState(null);
  const [user, setUser] = useState(null);
  const [index, setIndex] = useState(0);
  const [routes, setRoutes] = useState([
    { key: 'avaialable', title: 'Available' },
    { key: 'unavailable', title: 'Used/Cancelled' },
    { key: 'recurring', title: 'Recurring' },
  ]);
  const [loading, setloading] = useState(false)

  useEffect(() => {
    init();
  }, []);

  const init = async () => {
    setloading(true)
    const _user = await AsyncStorage.getItem('user_data');
    let user = _user ? JSON.parse(_user) : null;
    setUser(user);

    try {
      let data = {};
      data.user_id = user.data.id
      await Service.getAvailableVouchers(
        data,
        async (res) => {
          setAvailableVouchers(res.data)

          try {
            let data2 = {};
            data2.user_id = user.data.id
            await Service.getUnavailableVouchers(
              data2,
              async (res2) => {
                setUnavailableVouchers(res2.data)

                try {
                  let data3 = {};
                  data3.user_id = user.data.id
                  await Service.getRecurringServices(
                    data3,
                    async (res3) => {
                      setRecurringVouchers(res3.data)
                      setloading(false)
                    },
                    (error) => {
                      console.log('getRecurringServices 2', error)
                    }
                  )
                } catch (error) {
                  console.log('getRecurringServices 1', error)
                }

                setloading(false)
              },
              (error) => {
                console.log('getUnavailableVouchers 2', error)
              }
            )
          } catch (error) {
            console.log('getUnavailableVouchers 1', error)
          }

        },
        (error) => {
          console.log('getAvailableVouchers 2', error)
        }
      )
    } catch (error) {
      console.log('getAvailableVouchers 1', error)
    }
  };

  const AvailableVouchers = () => {
    return (
      <Content>
        {loading ? <Spinner /> :
          <View style={{ flex: 1, padding: 15 }}>
            {availableVouchers?.length > 0 ? (
              availableVouchers.map((voucher, i) => {
                let productImage = null;
                productImage = voucher?.info?.data?.product_info?.product_image;
                console.log("productImage", productImage)
                return (
                  <Card style={{ backgroundColor: '#ffffff', alignItems: 'center' }}>
                    <View style={{ justifyContent: 'center' }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          paddingVertical: 25,
                          // marginLeft: 20,
                        }}
                      >

                        <View>
                          <FastImage
                            source={productImage != null ? { uri: productImage } : productImageAssets.specificProduct.icon1}
                            style={{
                              width: 100,
                              height: 100,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                          />
                        </View>

                        <View style={{ marginHorizontal: 20, width: width - 200 }}>
                          {/* <Text>
                    {voucher.info.data.service_info.name}
                    </Text> */}
                          <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                            {voucher.info.data.seller_info.seller_shop_name}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.description}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.currency === 'PHP' ? '₱' : '$'}
                            {/* {voucher.info.data.service_info.currency === 'PHP' && '₱'}
                      {voucher.info.data.service_info.currency === 'US' && '$'} */}
                            {voucher.info.data.service_info.price}
                          </Text>
                        </View>

                      </View>
                      <View style={{ flexDirection: 'row', height: 55 }}>
                        <TouchableOpacity
                          onPress={async () => {
                            // console.log("voucher.info.data.qr_code_info.service_id", voucher.info.data.qr_code_info.service_id)
                            await setCode(voucher.info.data.qr_code_info.service_id.toString());
                            await setModal(true);
                          }}
                          style={{
                            borderColor: '#EDEDEB',
                            width: '50%',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopWidth: 1.5,
                            borderBottomWidth: 1.5,
                            borderLeftWidth: 1.5,
                            borderRightWidth: 0.5,
                          }}
                        >
                          <Icons.MaterialCommunityIcons name='qrcode-scan' size={20} color='gray' />
                          <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>SCAN</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => navigation.navigate('ProductDetails', { product_id: 2 })}
                          style={{
                            borderColor: '#EDEDEB',
                            width: '50%',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopWidth: 1.5,
                            borderBottomWidth: 1.5,
                            borderLeftWidth: 1.5,
                            borderRightWidth: 0.5,
                          }}
                        >
                          <Icons.SimpleLineIcons name='folder-alt' size={20} color='gray' />
                          <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>DETAILS</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Card>
                );
              })
            ) :
              <View style={styles.referredSection}>
                <Text style={styles.referredText}>No availableVouchers yet.</Text>
              </View>
            }
          </View>
        }
      </Content>
    )
  }

  const UnavailableVouchers = () => {
    return (
      <Content>
        {loading ? <Spinner /> :
          <View style={{ flex: 1, padding: 15 }}>
            {unavailableVouchers?.length > 0 ? (
              unavailableVouchers.map((voucher, i) => {
                let productImage = null;
                productImage = voucher?.info?.data?.product_info?.product_image;
                return (
                  <Card style={{ backgroundColor: '#ffffff', alignItems: 'center' }}>
                    <View style={{ justifyContent: 'center' }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          paddingVertical: 25,
                          // marginLeft: 20,
                        }}
                      >

                        <View>
                          <FastImage
                            source={productImage != null ? { uri: productImage } : productImageAssets.specificProduct.icon1}
                            style={{
                              width: 100,
                              height: 100,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                          />
                        </View>

                        <View style={{ marginHorizontal: 20, width: width - 200 }}>
                          {/* <Text>
                    {voucher.info.data.service_info.name}
                    </Text> */}
                          <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                            {voucher.info.data.seller_info.seller_shop_name}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.description}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.currency === 'PHP' ? '₱' : '$'}
                            {/* {voucher.info.data.service_info.currency === 'PHP' && '₱'}
                      {voucher.info.data.service_info.currency === 'US' && '$'} */}
                            {voucher.info.data.service_info.price}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', height: 55 }}>
                        <TouchableOpacity
                          onPress={() => navigation.navigate('ProductDetails', { product_id: 2 })}
                          style={{
                            borderColor: '#EDEDEB',
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopWidth: 1.5,
                            borderBottomWidth: 1.5,
                            borderLeftWidth: 1.5,
                            borderRightWidth: 0.5,
                          }}
                        >
                          <Icons.SimpleLineIcons name='folder-alt' size={20} color='gray' />
                          <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>DETAILS</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Card>
                );
              })
            ) :
              <View style={styles.referredSection}>
                <Text style={styles.referredText}>No availableVouchers yet.</Text>
              </View>
            }
          </View>
        }
      </Content>
    )
  }

  const RecurringVouchers = () => {
    return (
      <Content>
        {loading ? <Spinner /> :
          <View style={{ flex: 1, padding: 15 }}>
            {recurringVouchers?.length > 0 ? (
              recurringVouchers.map((voucher, i) => {
                let productImage = null;
                productImage = voucher?.info?.data?.product_info?.product_image;
                return (
                  <Card style={{ backgroundColor: '#ffffff', alignItems: 'center' }}>
                    <View style={{ justifyContent: 'center' }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'center',
                          paddingVertical: 25,
                          // marginLeft: 20,
                        }}
                      >

                        <View>
                          <FastImage
                            source={productImage != null ? { uri: productImage } : productImageAssets.specificProduct.icon1}
                            style={{
                              width: 100,
                              height: 100,
                            }}
                            resizeMode={FastImage.resizeMode.contain}
                          />
                        </View>
                        <View style={{ marginHorizontal: 20 }}>
                          {/* <Text>
                    {voucher.info.data.service_info.name}
                    </Text> */}
                          <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                            {voucher.info.data.seller_info.seller_shop_name}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.description}
                          </Text>
                          <Text>
                            {voucher.info.data.service_info.currency === 'PHP' ? '₱' : '$'}
                            {/* {voucher.info.data.service_info.currency === 'PHP' && '₱'}
                      {voucher.info.data.service_info.currency === 'US' && '$'} */}
                            {voucher.info.data.service_info.price}
                          </Text>
                        </View>
                      </View>
                      <View style={{ flexDirection: 'row', height: 55 }}>
                        <TouchableOpacity
                          onPress={() => navigation.navigate('ProductDetails', { product_id: 2 })}
                          style={{
                            borderColor: '#EDEDEB',
                            width: '100%',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderTopWidth: 1.5,
                            borderBottomWidth: 1.5,
                            borderLeftWidth: 1.5,
                            borderRightWidth: 0.5,
                          }}
                        >
                          <Icons.SimpleLineIcons name='folder-alt' size={20} color='gray' />
                          <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>DETAILS</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Card>
                );
              })
            ) :
              <View style={styles.referredSection}>
                <Text style={styles.referredText}>No availableVouchers yet.</Text>
              </View>
            }
          </View>
        }
      </Content>
    )
  }

  const _renderLabel = (props) => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d';

    return <Text style={[{ fontSize: 14 }, { color }]}>{route.title}</Text>;
  };

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={[styles.headerStyles, { left: 20 }]}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail source={require('../../../assets/headericon/left-arrow-dark.png')} style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>Vouchers</Text>
        <View style={[styles.headerStyles, { right: 20 }]}>
          <TouchableOpacity onPress={() => alert('Coming Soon !')}>
            <Thumbnail source={require('../../../assets/internal/product/icon_chat.png')} square style={styles.backIcon} />
          </TouchableOpacity>
        </View>
      </Header>
      <Content>
        <TabView
          navigationState={{ index, routes }}
          renderScene={SceneMap({
            avaialable: () => <AvailableVouchers />,
            unavailable: () => <UnavailableVouchers />,
            recurring: () => <RecurringVouchers />,
          })}
          renderTabBar={(props) => (
            <TabBar
              {...props}
              renderLabel={_renderLabel(props)}
              labelStyle={{
                // fontFamily: 'regular',
                fontSize: 11,
                textTransform: 'capitalize',
                color: '#231f20',
              }}
              indicatorStyle={{
                backgroundColor: '#f36e23',
                borderBottomWidth: 3,
                borderBottomColor: '#f36e23',
              }}
              indicatorContainerStyle={{
                borderBottomWidth: 1,
                borderBottomColor: '#EDEDEB',
              }}
              style={{ backgroundColor: '#ffffff', elevation: 0 }}

            // onTabPress={({ route, preventDefault }) => {
            //   // alert(JSON.stringify(route));
            //   if (route.key === 'avaialable') {
            //     setFocusedOn('avaialable')
            //   }
            //   if (route.key === 'unavaialable') {
            //     setFocusedOn('unavaialable')
            //   }
            // }}
            />
          )}
          onIndexChange={setIndex}
          initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
        />
      </Content>

      <Modal
        animationType='slide'
        transparent={true}
        visible={modal}
        onRequestClose={() => {
          setModal(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={{ padding: 35, borderRadius: 5, backgroundColor: '#FFF' }}>
              {code ? <QRCode value={code} size={200} /> : null}
              <View style={{ padding: 15 }} />
              <TouchableOpacity
                style={{ ...styles.openButton, backgroundColor: '#66b545' }}
                onPress={() => {
                  setModal(false);
                }}
              >
                <Text style={styles.textStyle}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

    </Container>
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
  },
  headerStyles: {
    position: 'absolute',
    alignSelf: 'center',
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  forwardIcon: {
    width: 20,
    height: 20,
  },
  title: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 15,
  },
  categoryContainer: {
    paddingHorizontal: 20,
    paddingVertical: 13,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#d3d3d3',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height,
  },
  modalView: {
    margin: 20,
    backgroundColor: '#00000099',
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#66b545',
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    width: '100%',
    padding: 15,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  referredSection: {
    // backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },
});
