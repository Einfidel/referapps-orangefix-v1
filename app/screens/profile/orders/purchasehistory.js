import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Container, Header, Thumbnail, Content, Body, Right, Spinner } from 'native-base';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Icons from '../../../components/icons';
const { width, height } = Dimensions.get('window');

const Pending = () => {
  return null
};

const Delivered = () => {
  return null
};

const Cancelled = () => {
  return null
};

export default function History({ navigation, route }) {

  const [index, setIndex] = useState(0);
  const [routes, setRoutes] = useState([
    { key: 'pending', title: 'Pending/To Receive' },
    { key: 'received', title: 'Delivered' },
    { key: 'cancelled', title: 'Cancelled' },
  ]);
  const [focusedOn, setFocusedOn] = useState('pending');
  const [transactions, setTransactions] = useState([])
  const [loading, setLoading] = useState(false)

  const init = async () => {
    // console.log('navigation.dangerouslyGetParent()', navigation));
    // console.log('navigation.dangerouslyGetParent().state.routes', navigation.dangerouslyGetParent().state.routes);


    setLoading(true)
    setTransactions([])
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    let formData = new FormData()
    formData.append("api_token", 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=')
    formData.append("user_id", user.data.id)
    // console.log("BODY", formData, user)
    let get = await fetch("https://www.referapps.com/api/account/transaction/index.json?include=date,info", {
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData
    })
    let response = await get.json()
    if (response) setTransactions(response.data)
    // console.log("TRANSACTIONS ", response)
    setLoading(false)
  }

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  )

  const _renderLabel = (props) => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d';

    return <Text style={[{ fontSize: 12 }, { color }]}>{route.title}</Text>;
  };

  const Transaction = (transaction) => {
    // console.log("focusedOn", focusedOn)
    // console.log("transaction.data.order_list", transaction.data.order_list)

    if (transaction.data.order_list.length > 0 && transaction.data.order_list.length != null) {
      if (focusedOn === transaction.data.order_list[0].status || (focusedOn === "pending" && (transaction.data.order_list[0].status === "shipped" || transaction.data.order_list[0].status === "for_processing"))) {
        return (
          <View style={{ marginBottom: 15 }}>
            <View style={{ width: '100%', padding: 15, borderWidth: 0, borderRadius: 5, borderBottomWidth: 0, backgroundColor: '#66B548' }}>
              <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Order ID: {transaction.data.order_id}</Text>
              <Text style={{ color: '#fff' }}>{`${transaction.data.date.data.added_in.month_year} @ ${transaction.data.date.data.added_in.time_passed}`}</Text>
            </View>
            <View style={{ width: '100%', padding: 15, borderWidth: 0.2, flexDirection: 'row', backgroundColor: '#fff', borderBottomStartRadius: 5, borderBottomEndRadius: 5 }}>
              <View style={{ flex: 4, padding: 15 }}>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{transaction.data.payment_type.toUpperCase()}</Text>
                <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }}>STATUS: {transaction.data.payment_status.toUpperCase()}</Text>
              </View>
              <View style={{ flex: 8, padding: 15 }}>
                <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }} numberOfLines={3}>{transaction.data.description}</Text>
                <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Total: {transaction.data.total_amount.toFixed(2)}</Text>
              </View>
              <TouchableOpacity onPress={() => {
                // console.log("transaction.data", transaction.data)

                navigation.navigate("TransactionDetails", {
                  id: transaction.data.order_id,
                  shipping: transaction.data.info.data.shipping_info,
                  billing: transaction.data.info.data.billing_info,
                  orders: transaction.data.order_list,
                  user: transaction.data.info.data.user_info,
                  date: transaction.data.date.data.added_in,
                  total: transaction.data.total_amount,
                  description: transaction.data.description,
                })
              }} style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
                <Icons.EvilIcons name="chevron-right" size={45} color="#66B548" />
              </TouchableOpacity>
            </View>
          </View>
        )
      } else {
        return null
      }
    } else {
      return null
    }
  }


  return (
    <Container style={{ backgroundColor: '#FFF' }} >
      <Header style={{ backgroundColor: '#ffffff' }}>
        <Body style={styles.left}>
          {/* <TouchableOpacity onPress={() => navigation.goBack()}> */}
          <TouchableOpacity onPress={() => {
            navigation.navigate('My Profile');
          }}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.headerText}>Purchase History</Text>
          </View>
        </Body>
      </Header>
      <Content>
        <TabView
          navigationState={{ index, routes }}
          renderScene={SceneMap({
            pending: Pending,
            received: Delivered,
            cancelled: Cancelled,
          })}
          renderTabBar={(props) => (
            <TabBar
              {...props}
              labelStyle={{
                // fontFamily: 'regular',
                fontSize: 11,
                textTransform: 'capitalize',
                color: '#f36e23',
              }}
              indicatorStyle={{
                backgroundColor: '#f36e23',
                borderBottomWidth: 3,
                borderBottomColor: '#f36e23',
              }}
              indicatorContainerStyle={{
                borderBottomWidth: 1,
                borderBottomColor: '#EDEDEB',
              }}
              style={{ backgroundColor: '#ffffff', elevation: 0 }}
              renderLabel={_renderLabel(props)}

              onTabPress={({ route, preventDefault }) => {
                // alert(JSON.stringify(route));
                if (route.key === 'pending') {
                  setFocusedOn('pending')
                }
                if (route.key === 'received') {
                  setFocusedOn('received')
                }
                if (route.key === 'cancelled') {
                  setFocusedOn('cancelled')
                }
              }}
            />
          )}
          onIndexChange={(index) => {
            setIndex(index);
          }}
          initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
        />
        <Content>
          {loading ? <Spinner /> :
            transactions.length > 0 ?
              <View style={{ flex: 1, padding: 15 }}>
                {transactions.map((data, i) => {
                  return <Transaction key={i} data={data} />
                })}
              </View>
              :
              <View style={styles.referredSection}>
                <Text style={styles.referredText}>No Transactions yet.</Text>
              </View>
          }
        </Content>
      </Content>
    </Container >
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  referredSection: {
    // backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },
});
