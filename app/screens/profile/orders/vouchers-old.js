import React from 'react';
import { useState, useEffect } from 'react';
import { StyleSheet, ScrollView, View, Text, Modal, TouchableOpacity, Dimensions } from 'react-native';
import { Container, Header, Thumbnail, Content } from 'native-base';
import Assets from '../../../components/assets.manager';
import Icons from '../../../components/icons';
import QRCode from 'react-native-qrcode-svg';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../../components/api/service';
const { width, height } = Dimensions.get('window');

const SAMPLEDATA = [
  {
    name: 'Service 1',
    description: 'Sample service description',
    price: '1500',
    id: 12345,
  },
  {
    name: 'Service 2',
    description: 'Sample service description',
    price: '1500',
    id: 54321,
  },
  {
    name: 'Service 3',
    description: 'Sample service description',
    price: '1500',
    id: 57454572,
  },
  {
    name: 'Service 4',
    description: 'Sample service description',
    price: '1500',
    id: 1352,
  },
  {
    name: 'Service 5',
    description: 'Sample service description',
    price: '1500',
    id: 8888,
  },
];

export default function Vouchers(navigation) {
  let logoFromFile = Assets.specificProduct.icon1;

  const [vouchers, setVouchers] = useState();
  const [modal, setmodal] = useState(false);
  const [code, setcode] = useState('test');
  const [user, setUser] = useState(null);

  const init = async () => {
    const _user = await AsyncStorage.getItem('user_data');
    let user = _user ? JSON.parse(_user) : null;
    setUser(user);

    try {
      let data = {};
      data.user_id = user.data.id
      await Service.getAvailableVouchers(
        data,
        async (res) => {
          setVouchers(res.data)
        },
        (error) => {
          console.log('getAvailableVouchers 2', error)
        }
      )
    } catch (error) {
      console.log('getAvailableVouchers 1', error)
    }
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <Container style={{ backgroundColor: '#f5f6fa' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={[styles.headerStyles, { left: 20 }]}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Thumbnail source={require('../../../assets/headericon/left-arrow-dark.png')} style={styles.backIcon} />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>Vouchers</Text>
        <View style={[styles.headerStyles, { right: 20 }]}>
          <TouchableOpacity onPress={() => alert('Coming Soon !')}>
            <Thumbnail source={require('../../../assets/internal/product/icon_chat.png')} square style={styles.backIcon} />
          </TouchableOpacity>
        </View>
      </Header>
      <Content>
        <ScrollView>
          {vouchers?.length > 0 ? (
            vouchers.map((voucher, i) => {
              let productImage = null;
              productImage = voucher?.info?.data?.product_info?.product_image;
              return (
                <View style={{ backgroundColor: '#ffffff' }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingVertical: 25,
                    }}
                  >
                    <Thumbnail
                      source={productImage != null ? { uri: productImage } : productImageAssets.specificProduct.icon1}
                      style={{
                        width: 100,
                        height: 100,
                        resizeMode: 'contain',
                      }}
                    />
                    <View style={{ marginHorizontal: 20 }}>
                      {/* <Text>
                      {voucher.info.data.service_info.name}
                      </Text> */}
                      <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>
                        {voucher.info.data.seller_info.seller_shop_name}
                      </Text>
                      <Text>
                        {voucher.info.data.service_info.description}
                      </Text>
                      <Text>
                        {voucher.info.data.service_info.currency === 'PHP' ? '₱' : '$'}
                        {/* {voucher.info.data.service_info.currency === 'PHP' && '₱'}
                        {voucher.info.data.service_info.currency === 'US' && '$'} */}
                        {voucher.info.data.service_info.price}
                      </Text>
                    </View>
                  </View>
                  <View style={{ flexDirection: 'row', height: 55 }}>
                    <TouchableOpacity
                      onPress={async () => {
                        await setcode(voucher.id.toString());
                        await setmodal(true);
                      }}
                      style={{
                        borderColor: '#EDEDEB',
                        width: '50%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderTopWidth: 1.5,
                        borderBottomWidth: 1.5,
                        borderLeftWidth: 1.5,
                        borderRightWidth: 0.5,
                      }}
                    >
                      <Icons.MaterialCommunityIcons name='qrcode-scan' size={20} color='gray' />
                      <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>SCAN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => navigation.navigation.navigate('ProductDetails', { product_id: 2 })}
                      style={{
                        borderColor: '#EDEDEB',
                        width: '50%',
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderTopWidth: 1.5,
                        borderBottomWidth: 1.5,
                        borderLeftWidth: 1.5,
                        borderRightWidth: 0.5,
                      }}
                    >
                      <Icons.SimpleLineIcons name='folder-alt' size={20} color='gray' />
                      <Text style={{ fontSize: 13, paddingHorizontal: 15 }}>DETAILS</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })
          ) :
            <View style={styles.referredSection}>
              <Text style={styles.referredText}>No vouchers yet.</Text>
            </View>
          }
        </ScrollView>
      </Content>
      <Modal
        animationType='slide'
        transparent={true}
        visible={modal}
        onRequestClose={() => {
          setmodal(false);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={{ padding: 35, borderRadius: 5, backgroundColor: '#FFF' }}>
              {code ? <QRCode value={code} size={200} /> : null}
              <View style={{ padding: 15 }} />
              <TouchableOpacity
                style={{ ...styles.openButton, backgroundColor: '#66b545' }}
                onPress={() => {
                  setmodal(false);
                }}
              >
                <Text style={styles.textStyle}>Done</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </Container>
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
  },
  headerStyles: {
    position: 'absolute',
    alignSelf: 'center',
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  forwardIcon: {
    width: 20,
    height: 20,
  },
  title: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 15,
  },
  categoryContainer: {
    paddingHorizontal: 20,
    paddingVertical: 13,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#d3d3d3',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height,
  },
  modalView: {
    margin: 20,
    backgroundColor: '#00000099',
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height,
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#66b545',
    borderRadius: 10,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    width: '100%',
    padding: 15,
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  referredSection: {
    // backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },
});
