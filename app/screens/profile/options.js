import React from 'react';
import FastImage from 'react-native-fast-image';

import { ScrollView, Text, TouchableOpacity, View, Alert, Dimensions, NativeModules } from 'react-native';
import { Header, Container } from 'native-base';
import Icon from '../../components/icons';
import AsyncStorage from '@react-native-community/async-storage';
import Notification from '../../components/Notifications';
import { LoginManager, Profile, } from 'react-native-fbsdk-next';
// import accountSettings from '../../styles/AccountSettingStyles/AccountSettingStyles';

import auth from '@react-native-firebase/auth';

const { width } = Dimensions.get('window')
class AccountSettingsScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      userLoad: null,
      verified: null,
      userData: {}
    }
  }

  render() {
    return (
      <Container style={{}}>
        <Header style={{ backgroundColor: '#FFF', height: 60 }}>
          <View style={{ position: 'absolute', left: 0, alignItems: 'flex-start', alignSelf: 'center', justifyContent: 'flex-start' }}>
            <TouchableOpacity
              onPress={() => {
                this.props.route.params?.onBackPress ? this.props.route.params.onBackPress() : null
                this.props.navigation.goBack()
                // console.log(this.props.route.params)
              }}
            >
              {/* <FastImage
                source={require('../../assets/images/headericon/left-arrow-dark.png')}
                square
                small
                style={accountSettings.backIcon}
              /> */}
              <Icon.AntDesign name="arrowleft" size={20} style={accountSettings.backIcon} />
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: 'center' }}>
            <Text style={accountSettings.headText}>Account Settings</Text>
          </View>
        </Header>

        <ScrollView style={{ backgroundColor: '#F7F5F6', paddingTop: 10 }}>

          <View style={accountSettings.textHeaderContainer}>
            <Text style={accountSettings.titleText}>My Account</Text>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('EditProfile')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>My Profile</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('EmailVerification')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Email Verification</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                    source={require('../../../assets/images/icon_sub.png')}
                    style={accountSettings.openIcon}
                  /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('PhoneVerification')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Contact Number Verification</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                    source={require('../../../assets/images/icon_sub.png')}
                    style={accountSettings.openIcon}
                  /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          {/* <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('Devices')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>My Devices</Text>
              </View>
              <View style={accountSettings.right}>
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View> */}

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('Addresses')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>My Addresses</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('BankAccounts')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Bank Accounts / Cards</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('DeleteAccount')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Delete Account</Text>
              </View>
              <View style={accountSettings.right}>
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textHeaderContainer}>
            <Text style={accountSettings.titleText}>Settings</Text>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('ChatSettings')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Chat Settings</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('PrivacySettings')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Privacy Settings</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('LanguageSettings')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Language</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textHeaderContainer}>
            <Text style={accountSettings.titleText}>Help Settings</Text>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('HelpCenter')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>Help Centre</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity style={accountSettings.touchable}
              onPress={() => this.props.navigation.navigate('Policies')}
            >
              <View style={accountSettings.left}>
                <Text style={accountSettings.headerText}>ReferApps Policies</Text>
              </View>
              <View style={accountSettings.right}>
                {/* <FastImage
                  source={require('../../../assets/images/icon_sub.png')}
                  style={accountSettings.openIcon}
                /> */}
                <Icon.AntDesign name="right" size={20} color="#00000050" />
              </View>
            </TouchableOpacity>
          </View>

          <View style={accountSettings.textContainer}>
            <TouchableOpacity
              style={accountSettings.touchable}
              onPress={async () =>
                Alert.alert(
                  'Warning!',
                  'Are you sure you want to logout?',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {
                      text: 'OK', onPress: async () => {
                        console.log("logout start")
                        // await AsyncStorage.removeItem(API_TOKEN)
                        await AsyncStorage.removeItem('auth');
                        await AsyncStorage.removeItem('user_data');
                        await AsyncStorage.removeItem('chat');
                        await AsyncStorage.removeItem('fcmtoken');
                        // await Notification.removeToken();

                        // Firebase Signout start                        
                        auth()
                          .signOut()
                          .then((response) => {
                            console.log("THIS IS FIREBASE SIGN OUT RESPONSE", response)
                          })
                          .catch(error => {
                            alert(error)
                          })
                        // Firebase Signout end  

                        await Profile.getCurrentProfile().then(async (res) => {
                          if (res != null) {
                            // FB signout start
                            await AsyncStorage.removeItem('fb_user_id');
                            LoginManager.logOut();
                            // FB signout end
                          }
                        })

                        console.log("logout end")


                        this.props.navigation.reset({
                          index: 0,
                          routes: [{ name: 'Tab' }],
                        });
                        // this.props.navigation.navigate("UnauthTabs");
                        // NativeModules.DevSettings.reload();
                      }
                    },
                  ],
                  { cancelable: false },
                )
              }
            >
              <Text style={accountSettings.logoutText}>Log Out</Text>
            </TouchableOpacity>
          </View>

          <View style={{ paddingTop: 40, }} />
        </ScrollView>
      </Container>
    );
  }
}

export default (AccountSettingsScreen)


const accountSettings = {

  // Header
  container: {
    backgroundColor: '#ecf0f1',
  },
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  textHeaderContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    width,
    backgroundColor: '#F7F5F6',
    borderBottomColor: '#d0dbdb',
    borderBottomWidth: 1,
    // borderTopColor: '#A9AFAF',
    // borderTopWidth: 1.5,
  },
  textContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    width,
    backgroundColor: '#F7F5F6',
    borderBottomColor: '#d0dbdb',
    borderBottomWidth: 1,
  },
  touchable: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headText: {
    // fontFamily: 'roboto-bold',
    fontSize: 17,
    color: '#231f20',
    fontWeight: 'bold',
    ...Platform.select({
      ios: {
        paddingBottom: 18
      }
    })
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  deleteAccountText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: 'red',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#3145AC',
    fontWeight: "bold",
  },
  logoutText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#e85451',
    // fontWeight: "bold",
  },
  // border: {
  // 	height: .5,
  // 	width: width,
  // 	backgroundColor: '#d0dbdb',
  // },
  left: {
    alignSelf: 'flex-start',
    justifyContent: 'center'
  },
  right: {
    alignSelf: 'flex-end',
  },
  openIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    alignSelf: 'center',
    tintColor: '#7f8c8d',
  },
}
