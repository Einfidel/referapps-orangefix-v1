import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
} from 'react-native';

import {
  PulseIndicator,
} from 'react-native-indicators';

import { Container, Input, Item, Content, } from 'native-base';
import FastImage from 'react-native-fast-image';
import FlatListComponent from '../../components/flatlist';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import RenderItem from '../../components/renderItem';
import styles from '../../styles/profile/view_your_shop';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import Moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';


const AllProducts = () => {
  return null
};

const Referred = () => {
  return null
};

const Overview = () => {
  return null
};

export default function ViewYourShop({ navigation, route }) {
  const [user, setUser] = useState(null);
  const [index, setIndex] = useState(0);
  const [routes, setRoutes] = useState([
    { key: 'allproducts', title: 'All Products' },
    { key: 'overview', title: 'Overview' },
  ]);
  const [shop, setShop] = useState({});
  const [cover, setCover] = useState(require('../../assets/internal/default-cover.jpg'));

  const [products, setProducts] = useState([]);
  const [productsLoaded, setProductsLoaded] = useState(true);
  const [sortBy, setSortBy] = useState('latest');
  const [hasMorePage, setHasMorePage] = useState(false);
  const [isLoadingMore, setIsLoadingMore] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [focusedOn, setFocusedOn] = useState('allproducts');
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState(null);


  useEffect(() => {
    // console.log("ViewYourShop useEffect")
    init();
  }, [])

  const init = async () => {
    let db = await AsyncStorage.getItem('user_data');
    let user = typeof db == 'string' ? JSON.parse(db) : null

    if (user.data.id == null) return
    setUser(user)

    let id = route.params?.shop_id || user.data.id;

    try {
      const shopProfile = await Service.getShopProfile(id);
      setCover({ uri: shopProfile.data.info.data.cover_photo.full_path })
      setShop(shopProfile.data)
    } catch (error) {
      console.log("Service.getShopProfile Error in View As Screen", error)
    }

    fetchProducts('latest');
  }

  const renderAllProducts = () => {
    if (focusedOn == "allproducts") {
      return (
        <View style={{ backgroundColor: '#ffffff', }}>
          <View style={styles.allProductsView}>
            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                setSortBy('popularity')
                fetchProducts('popularity');
              }}>
                <Text style={sortBy === "popularity" ? styles.selectedTitle : styles.titles}>
                  Popular
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                setSortBy('latest')
                fetchProducts('latest');
              }}>
                <Text style={sortBy === "latest" ? styles.selectedTitle : styles.titles}>
                  Latest
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                setSortBy('popular')
                fetchProducts('popular');
              }}>
                <Text style={sortBy === "popular" ? styles.selectedTitle : styles.titles}>
                  Top Sales
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.priceBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                setSortBy('lowHigh')
                fetchProducts('lowHigh');
              }}>
                <Text style={sortBy === "lowHigh" ? styles.selectedTitle : styles.titles}>
                  Price
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* {renderProductList()} */}
        </View>
      );
    } else {
      return null
    }
  };

  const renderReferred = () => {
    if (focusedOn == "referred") {
      return (
        <View style={styles.referredSection}>
          <Text style={styles.referredText}>This user have no Referred items yet.</Text>
        </View>
      );
    } else {
      return null
    }
  };

  const renderOverview = () => {
    Moment
    if (focusedOn == "overview") {
      return (
        <View style={styles.overviewSection}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.text1}>Member since </Text>
              </View>
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.dateText}>{user?.data?.info?.data?.member_since?.month_year}</Text>
              </View>
            </View>
            <View style={styles.locationSection}>
              <FastImage
                source={Assets.stores.locationIcon}
                style={styles.locationIcon}
                tintColor={'#080808'}
              />
              <Text style={styles.locationText}>{user?.data?.info?.data?.member_since?.timestamp?.timezone}</Text>
            </View>
            {/* <View style={{ paddingTop: 25 }}>
              <Text style={styles.text1}>Interesting</Text>
              <View style={styles.emailSection}>
                <FastImage
                  source={Assets.accountSettings.emailIcon}
                  style={styles.emailIcon}
                  tintColor='black'
                />
                <Text style={styles.emailText}>ardianonavarro.viray@yahoo.com</Text>
              </View>
            </View> */}
            {/* <View style={{ paddingTop: 12 }}>
              <Text style={styles.text1}>Other Accounts</Text>
              <View style={{ paddingTop: 5 }}>
                <FastImage
                  source={Assets.accountSettings.socialIcon1}
                  style={styles.facebookIcon}
                />
              </View>
            </View> */}
          </View>
        </View>
      );
    } else {
      return null
    }
  };

  const _renderLabel = props => ({ route, focused, }) => {
    const color = focused ? '#231f20' : '#7f8c8d'
    return (
      <Text style={[{ fontSize: 16 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  const renderAvatar = () => {
    let shop = shop?.info?.data?.shop;
    // console.log("shop", shop);
    // console.log("shop.filename", shop?.filename);
    // console.log("profile pic: ", shop?.filename ? { uri: shop.full_path } : Assets.myProfile.face);
    return (
      <FastImage
        source={shop?.filename ? { uri: shop.full_path } : Assets.myProfile.face}
        style={styles.defaultProfile}
      />
    );
  }

  const fetchProducts = async () => {
    try {
      setPage(1)
      setProductsLoaded(false)
      let productsNew = [];
      let viewType = 'profile';

      if (route.params?.shop_id == null || route.params?.shop_id == undefined) {
        id = user?.data?.id
        if (id == null || id == undefined) {
          let db = await AsyncStorage.getItem('user_data');
          let tmpUser = typeof db == 'string' ? JSON.parse(db) : null
          id = tmpUser.data.id
          viewType = 'profile';
        }
      } else {
        id = route.params?.shop_id
      }

      console.log("id", id)

      let data = {
        user_id: id,
        viewType: viewType,
        sort_by: sortBy,
      }

      console.log("data", data)

      await Service.getAccountProductsAndServices(
        data,
        (res) => {
          // console.log('Response', JSON.stringify(res));
          // if (res.length > 0) {
          productsNew = [...res];

          if (productsNew.length % 2 === 1) {
            productsNew.push({
              id: null,
            });
          }

          setProducts(productsNew)
          setHasMorePage(res.has_morepages)

          // console.log('productsNew: ' + productsNew);
          // console.log('hasMorePage: ' + hasMorePage);
          // console.log('productsLoaded: true');

          setProductsLoaded(true)

          // } else {
          //   Alert.alert("Error", "Please try again later");
          //   console.log('Service.getAccountProductsAndServices Error 3', res)
          // }
        },
        (err) => {
          Alert.alert('Error', err.message)
          console.log('Service.getAccountProductsAndServices Error 2', err)
        },
      );
    } catch (error) {
      Alert.alert('Fetch Error', error);
      console.log('Service.getAccountProductsAndServices Error 1', error)
    }
  };

  const renderProductList = () => {
    if (focusedOn == "allproducts") {
      if (productsLoaded) {
        return (
          <FlatListComponent
            data={products}
            keyExtractor={(item, index) => index.toString()}
            numColumns={2}
            refreshing={isRefreshing}
            onRefresh={onRefresh}
            onEndReachedThreshold={0}
            onEndReached={fetchMoreProducts}
            renderItem={renderProducts}
            ListFooterComponent={renderFooter}
            removeClippedSubviews={false}
            windowSize={18}
            maxToRenderPerBatch={8}
            scrollEventThrottle={16}
          />
        );
      } else {
        return (
          <View style={{ paddingTop: 20 }}>
            <PulseIndicator color='#00a14b' count={5} />
          </View>
        );
      }
    } else {
      return null
    }
  };

  const onRefresh = async () => {
    try {
      setPage(1)
      setIsRefreshing(true)
      let productsNew = [];
      let id = route.params?.shop_id || user.data.id;

      let data = {}
      data.user_id = id
      data.viewType = 'profile'
      data.sort_by = sortBy

      await Service.getAccountProductsAndServices(
        data,
        (res) => {
          // console.log('Response', JSON.stringify(res));
          if (res.status) {
            productsNew = [...res.data];

            if (productsNew.length % 2 === 1) {
              productsNew.push({
                id: null,
              });
            }

            setProducts(productsNew)
            setHasMorePage(res.has_morepages)
            setIsRefreshing(false)
          } else {
            Alert.alert(res.status, res.msg);
            console.log('Service.getAccountProducts Error 3', res)
          }
        },
        (err) => {
          Alert.alert('Error', err.message)
          console.log('Service.getAccountProducts Error 2', err)
        },
      );
    } catch (error) {
      Alert.alert('Fetch Error', error);
      console.log('Service.getAccountProducts Error 1', error)
    }
  };

  const fetchMoreProducts = async () => {
    if (hasMorePage) {
      // console.log('fetching more data.');
      setIsLoadingMore(true)
      setPage(page + 1)
      try {
        let data = {}
        data.user_id = id
        data.viewType = 'profile'
        data.sort_by = 'latest'

        await Service.getAccountProductsAndServices(
          data,
          (res) => {
            // console.log('Response', JSON.stringify(res));
            if (res.status) {
              let productsNew = [...products, ...res.data];

              if (productsNew.length % 2 === 1) {
                productsNew.push({
                  id: null,
                });
              }

              setProducts(productsNew)
              setHasMorePage(res.has_morepages)
              setIsLoadingMore(false)
            } else {
              Alert.alert(res.status, res.msg);
              console.log('Service.getAccountProducts Error 3', res)
            }
          },
          (err) => {
            Alert.alert('Error', err.message)
            console.log('Service.getAccountProducts Error 2', err)
          },
        );
      } catch (e) {
        console.warn(e);
      }
    } else {
      setIsLoadingMore(false)
    }
  };

  const renderProducts = ({ item }) => {
    if (!item.id) {
      return (
        <View
          style={{
            width: '48%',
          }}
        />
      );
    } else {
      return (
        <RenderItem
          onPress={() => {
            console.log("item.product_name", item.product_name)
            console.log("item.id", item.id)
            // navigation.reset({
            //   index: 0,
            //   routes: [{ name: 'Tab' }],
            // })
            navigation.push('ProductDetails', { product_id: item.id })
            // navigation.navigate('ProductDetails', { product_id: item.id })
            // navigation.navigate('Cart Screen')
          }}
          uri={item.info.data.image.full_path}
          id={item.id}
          product_name={item.product_name}
          country_code={item.country_code}
          price={item.price}
          ratings={item.ratings}
        />
      );
    }
  };

  const renderFooter = () => {
    if (isLoadingMore) {
      return (
        <View style={{ paddingBottom: 10 }}>
          <PulseIndicator color='#00a14b' count={5} />
        </View>
      );
    } else {
      return <View />;
    }
  };

  return (
    <Container>
      <ImageBackground
        source={cover}
        // source={Assets.myProfile.bg}
        style={styles.headerCoverImage} >
        <View style={styles.backgroundImageContainer}>
          <Content>
            <View>
              <View style={styles.MainContainer}>
                <View style={styles.headerRow}>
                  <TouchableOpacity onPress={() => navigation.goBack()} style={styles.touchableBackButton}>
                    <FastImage
                      source={Assets.accountSettings.arrowDarkIcon}
                      style={styles.backIcon}
                      tintColor={'#ffffff'}
                    />
                  </TouchableOpacity>
                  <View style={{ paddingLeft: hp('2.50%'), }}>
                    <Item style={styles.ItemContainer} last>
                      <FastImage
                        source={Assets.accountSettings.searchHelpcenter}
                        style={styles.searchIcon}
                      />
                      <Input
                        placeholder='search'
                        placeholderTextColor='#7f8c8d'
                        style={styles.searchText}
                        onChangeText={(search) => {
                          setSearch(search)
                        }}
                      />
                    </Item>
                  </View>
                  <TouchableOpacity style={styles.touchableChatIcon} onPress={() => {
                    Alert.alert('Coming Soon!', 'This feature will be available soon.')
                    // navigation.navigate("Messages")
                  }}>
                    <FastImage
                      source={Assets.myProfile.chatIcon}
                      style={[styles.cartIcon, { marginLeft: 10, marginRight: 5 }]}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.touchableCartIcon} onPress={() => navigation.navigate('Cart Screen')}>
                    <FastImage
                      source={Assets.myProfile.cartIcon}
                      style={styles.cartIcon}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <View style={[styles.profileRow]}>
                {renderAvatar()}
                <View style={[styles.column1, { paddingBottom: hp('0%') }]}>
                  <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.userNameText, { width: wp('30%') }]}>{shop.shop_name}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Products : </Text>
                    <Text style={styles.productValues}>{shop.products}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Services : </Text>
                    <Text style={styles.Values}>{shop.services}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Reviews : </Text>
                    <Text style={styles.Values}>{shop.reviews}</Text>
                  </View>
                </View>
                <View style={[styles.column2, { paddingBottom: 0, width: wp('50%'), }]}>
                  <Text style={styles.ShopRatingText}>Shop Rating</Text>
                  <FastImage
                    source={Assets.stores.starIcon}
                    style={styles.starIcon}
                    tintColor={'#ffa012'}
                  />
                  <View style={{ flexDirection: 'row', }}>
                    {shop.ratings * 0 == 0 ?
                      <Text style={styles.ratingText}>{shop.ratings} out of 5</Text>
                      :
                      <Text style={styles.ratingText}>No ratings yet</Text>
                    }
                  </View>
                </View>
              </View>
            </View>
          </Content>
        </View>
      </ImageBackground>
      <Content>
        <View >
          <TabView
            navigationState={{ index, routes }}
            renderScene={SceneMap({
              allproducts: AllProducts,
              referred: Referred,
              overview: Overview,
            })}
            renderTabBar={props =>
              <TabBar
                {...props}
                labelStyle={{
                  // fontFamily: 'Roboto', 
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 2.50, borderBottomColor: '#f36e23' }}
                style={{ backgroundColor: '#ffffff', elevation: 0 }}
                renderLabel={_renderLabel(props)}
                onTabPress={({ route, preventDefault }) => {
                  // alert(JSON.stringify(route));
                  if (route.key === 'allproducts') {
                    setFocusedOn('allproducts')
                  }
                  if (route.key === 'referred') {
                    setFocusedOn('referred')
                  }
                  if (route.key === 'overview') {
                    setFocusedOn('overview')
                  }
                }}
              />
            }

            onIndexChange={index => setIndex(index)}
            initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
          />
        </View>
        <View
          style={{
            backgroundColor: '#f5f6fa',
            flex: 1,
          }}
        >
        </View>
        <View style={{ marginBottom: 15, }}>
          {renderAllProducts()}
          {renderReferred()}
          {renderOverview()}
          {renderProductList()}
          {Overview()}
        </View>
      </Content>
    </Container>
  );

}