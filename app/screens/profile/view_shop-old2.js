import React from 'react';

import {
  Text,
  View,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Image,
  Dimensions,
} from 'react-native';

import {
  PulseIndicator,
} from 'react-native-indicators';

import { Container, Input, Item, Content, } from 'native-base';
import FastImage from 'react-native-fast-image';
import FlatListComponent from '../../components/flatlist';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import RenderItem from '../../components/renderItem';
import styles from '../../styles/profile/view_your_shop';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import Moment from 'moment';
import AsyncStorage from '@react-native-community/async-storage';

class ViewYourShop extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      index: 0,
      routes: [
        { key: 'allproducts', title: 'All Products' },
        // { key: 'referred', title: 'Referred' },
        { key: 'overview', title: 'Overview' },
      ],
      shop: {},
      cover: require('../../assets/internal/default-cover.jpg'),

      products: [],
      productsLoaded: true,
      sort_by: 'popularity',
      hasMorePage: false,
      isLoadingMore: false,
      isRefreshing: false,
      focusedOn: 'allproducts',
    }

    this.page = 1;
  }

  componentDidMount = async () => {
    let db = await AsyncStorage.getItem('user_data');
    let user = typeof db == 'string' ? JSON.parse(db) : null

    if (user.data.id == null) return
    this.setState({ user: user })

    let id = this.props.route.params?.shop_id || user.data.id;

    try {
      const shopProfile = await Service.getShopProfile(id);
      this.setState({ cover: { uri: shopProfile.data.info.data.cover_photo.full_path } })
      this.setState({ shop: shopProfile.data })
    } catch (error) {
      console.log("Service.getShopProfile Error in View As Screen", error)
    }

    this.fetchProducts('popularity');
  }

  AllProducts = () => {
    return null
  };

  Referred = () => {
    return null
  };

  Overview = () => {
    return null
  };

  renderAllProducts = () => {
    if (this.state.focusedOn == "allproducts") {
      return (
        <View style={{ backgroundColor: '#ffffff', }}>
          <View style={styles.allProductsView}>
            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                this.setState({
                  sort_by: 'popularity'
                })
                this.fetchProducts('popularity');
              }}>
                <Text style={styles.titles}>
                  Popular
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                this.setState({
                  sort_by: 'latest'
                })
                this.fetchProducts('latest');
              }}>
                <Text style={styles.titles}>
                  Latest
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.titleBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                this.setState({
                  sort_by: 'best_seller'
                })
                this.fetchProducts('best_seller');
              }}>
                <Text style={styles.titles}>
                  Top Sales
                </Text>
              </TouchableOpacity>
            </View>

            <View style={styles.priceBorderWidth}>
              <TouchableOpacity style={styles.titlesPosition} onPress={() => {
                this.setState({
                  sort_by: 'lowHigh'
                })
                this.fetchProducts('lowHigh');
              }}>
                <Text style={styles.titles}>
                  Price
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          {/* {this.renderProductList()} */}
        </View>
      );
    } else {
      return null
    }
  };

  renderReferred = () => {
    if (this.state.focusedOn == "referred") {
      return (
        <View style={styles.referredSection}>
          <Text style={styles.referredText}>This user have no Referred items yet.</Text>
        </View>
      );
    } else {
      return null
    }
  };

  renderOverview = () => {
    Moment
    if (this.state.focusedOn == "overview") {
      return (
        <View style={styles.overviewSection}>
          <View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.text1}>Member since </Text>
              </View>
              <View style={{ flexDirection: 'column' }}>
                <Text style={styles.dateText}>{this.state.user?.data?.info?.data?.member_since?.month_year}</Text>
              </View>
            </View>
            <View style={styles.locationSection}>
              <FastImage
                source={Assets.stores.locationIcon}
                style={styles.locationIcon}
                tintColor={'#080808'}
              />
              <Text style={styles.locationText}>{this.state.user?.data?.info?.data?.member_since?.timestamp?.timezone}</Text>
            </View>
            {/* <View style={{ paddingTop: 25 }}>
              <Text style={styles.text1}>Interesting</Text>
              <View style={styles.emailSection}>
                <FastImage
                  source={Assets.accountSettings.emailIcon}
                  style={styles.emailIcon}
                  tintColor='black'
                />
                <Text style={styles.emailText}>ardianonavarro.viray@yahoo.com</Text>
              </View>
            </View> */}
            {/* <View style={{ paddingTop: 12 }}>
              <Text style={styles.text1}>Other Accounts</Text>
              <View style={{ paddingTop: 5 }}>
                <FastImage
                  source={Assets.accountSettings.socialIcon1}
                  style={styles.facebookIcon}
                />
              </View>
            </View> */}
          </View>
        </View>
      );
    } else {
      return null
    }
  };

  _renderLabel = props => ({ route, focused, }) => {
    const color = focused ? '#231f20' : '#7f8c8d'
    return (
      <Text style={[{ fontSize: 16 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  renderAvatar = () => {
    let shop = this.state.shop?.info?.data?.shop;
    // console.log("shop", shop);
    // console.log("shop.filename", shop?.filename);
    // console.log("profile pic: ", shop?.filename ? { uri: shop.full_path } : Assets.myProfile.face);
    return (
      <Image
        source={shop?.filename ? { uri: shop.full_path } : Assets.myProfile.face}
        style={styles.defaultProfile}
      />
    );
  }

  fetchProducts = async (sort_by) => {
    try {
      this.page = 1;
      this.setState({
        productsLoaded: false,
      });
      var products = [];
      let id = this.props.route.params?.shop_id || this.state.user.data.id;


      let data = {}
      data.user_id = id
      data.viewType = 'profile'
      data.sort_by = 'latest'

      await Service.getAccountProducts(
        data,
        (res) => {
          // console.log('Response', JSON.stringify(res));
          if (res.length > 0) {
            products = [...res];

            if (products.length % 2 === 1) {
              products.push({
                id: null,
              });
            }

            this.setState(
              {
                products: products,
                hasMorePage: res.has_morepages,
              },
              () => {
                console.log('products: ' + this.state.products);
                console.log('hasMorePage: ' + this.state.hasMorePage);
                console.log('productsLoaded: true');
                this.setState({
                  productsLoaded: true,
                });
              }
            )
          } else {
            Alert.alert("Error", "Please try again later");
            console.log('Service.getAccountProducts Error 3', res)
          }
        },
        (err) => {
          Alert.alert('Error', err.message)
          console.log('Service.getAccountProducts Error 2', err)
        },
      );
    } catch (error) {
      Alert.alert('Fetch Error', error);
      console.log('Service.getAccountProducts Error 1', error)
    }
  };

  renderProductList = () => {
    if (this.state.focusedOn == "allproducts") {
      if (this.state.productsLoaded) {
        return (
          <FlatListComponent
            data={this.state.products}
            keyExtractor={(item, index) => index.toString()}
            numColumns={2}
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh}
            onEndReachedThreshold={0}
            onEndReached={this.fetchMoreProducts}
            renderItem={this.renderProducts}
            ListFooterComponent={this.renderFooter}
            removeClippedSubviews={false}
            windowSize={18}
            maxToRenderPerBatch={8}
            scrollEventThrottle={16}
          />
        );
      } else {
        return (
          <View style={{ paddingTop: 20 }}>
            <PulseIndicator color='#00a14b' count={5} />
          </View>
        );
      }
    } else {
      return null
    }
  };

  onRefresh = async () => {
    try {
      this.page = 1;
      this.setState({
        isRefreshing: true,
      });
      var products = [];
      let id = this.props.route.params?.shop_id || this.state.user.data.id;

      let data = {}
      data.user_id = id
      data.viewType = 'profile'
      data.sort_by = 'latest'

      await Service.getAccountProducts(
        data,
        (res) => {
          // console.log('Response', JSON.stringify(res));
          if (res.status) {
            products = [...res.data];

            if (products.length % 2 === 1) {
              products.push({
                id: null,
              });
            }

            this.setState(
              {
                products: products,
                hasMorePage: res.has_morepages,
              },
              () => {
                this.setState({
                  isRefreshing: false,
                });
              }
            );

          } else {
            Alert.alert(res.status, res.msg);
            console.log('Service.getAccountProducts Error 3', res)
          }
        },
        (err) => {
          Alert.alert('Error', err.message)
          console.log('Service.getAccountProducts Error 2', err)
        },
      );
    } catch (error) {
      Alert.alert('Fetch Error', error);
      console.log('Service.getAccountProducts Error 1', error)
    }
  };

  fetchMoreProducts = () => {
    if (this.state.hasMorePage) {
      console.log('fetching more data.');
      this.setState({ isLoadingMore: true }, async () => {
        this.page = this.page + 1;
        try {
          let data = {}
          data.user_id = id
          data.viewType = 'profile'
          data.sort_by = 'latest'

          await Service.getAccountProducts(
            data,
            (res) => {
              // console.log('Response', JSON.stringify(res));
              if (res.status) {
                products = [...this.state.products, ...res.data];

                if (products.length % 2 === 1) {
                  products.push({
                    id: null,
                  });
                }

                this.setState(
                  {
                    products: products,
                    hasMorePage: res.has_morepages,
                  },
                  () => {
                    this.setState({
                      isLoadingMore: false,
                    });
                  }
                );
              } else {
                Alert.alert(res.status, res.msg);
                console.log('Service.getAccountProducts Error 3', res)
              }
            },
            (err) => {
              Alert.alert('Error', err.message)
              console.log('Service.getAccountProducts Error 2', err)
            },
          );
        } catch (e) {
          console.warn(e);
        }
      });
    } else {
      this.setState({ isLoadingMore: false });
    }
  };

  renderProducts = ({ item }) => {
    if (!item.id) {
      return (
        <View
          style={{
            width: '48%',
          }}
        />
      );
    } else {
      return (
        <RenderItem
          onPress={() => {
            console.log("item.product_name", item.product_name)
            console.log("item.id", item.id)
            this.props.navigation.navigate('ProductDetails', { product_id: item.id })
          }}
          uri={item.info.data.image.full_path}
          id={item.id}
          product_name={item.product_name}
          country_code={item.country_code}
          price={item.price}
          ratings={item.ratings}
        />
      );
    }
  };

  renderFooter = () => {
    if (this.state.isLoadingMore) {
      return (
        <View style={{ paddingBottom: 10 }}>
          <PulseIndicator color='#00a14b' count={5} />
        </View>
      );
    } else {
      return <View />;
    }
  };

  render() {
    return (
      <Container>
        <ImageBackground
          source={this.state.cover}
          // source={Assets.myProfile.bg}
          style={styles.headerCoverImage} >
          <View style={styles.backgroundImageContainer}>
            <Content>
              <View>
                <View style={styles.MainContainer}>
                  <View style={styles.headerRow}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.touchableBackButton}>
                      <FastImage
                        source={Assets.accountSettings.arrowDarkIcon}
                        style={styles.backIcon}
                        tintColor={'#ffffff'}
                      />
                    </TouchableOpacity>
                    <View style={{ paddingLeft: hp('2.50%'), }}>
                      <Item style={styles.ItemContainer} last>
                        <FastImage
                          source={Assets.accountSettings.searchHelpcenter}
                          style={styles.searchIcon}
                        />
                        <Input
                          placeholder='search'
                          placeholderTextColor='#7f8c8d'
                          style={styles.searchText}
                          onChangeText={(search) => this.setState({ search })}
                        />
                      </Item>
                    </View>
                    <TouchableOpacity style={styles.touchableChatIcon} onPress={() => {
                      Alert.alert('Coming Soon!', 'This feature will be available soon.')
                      // this.props.navigation.navigate("Messages")
                    }}>
                      <FastImage
                        source={Assets.myProfile.chatIcon}
                        style={[styles.cartIcon, { marginLeft: 10, marginRight: 5 }]}
                      />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.touchableCartIcon} onPress={() => this.props.navigation.navigate('Cart Screen')}>
                      <FastImage
                        source={Assets.myProfile.cartIcon}
                        style={styles.cartIcon}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={[styles.profileRow]}>
                  {this.renderAvatar()}
                  <View style={[styles.column1, { paddingBottom: hp('0%') }]}>
                    <Text numberOfLines={1} ellipsizeMode='tail' style={[styles.userNameText, { width: wp('30%') }]}>{this.state.shop.shop_name}</Text>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Products : </Text>
                      <Text style={styles.productValues}>{this.state.shop.products}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Services : </Text>
                      <Text style={styles.Values}>{this.state.shop.services}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                      <Text style={styles.userInfoText}>Reviews : </Text>
                      <Text style={styles.Values}>{this.state.shop.reviews}</Text>
                    </View>
                  </View>
                  <View style={[styles.column2, { paddingBottom: 0, width: wp('50%'), }]}>
                    <Text style={styles.ShopRatingText}>Shop Rating</Text>
                    <FastImage
                      source={Assets.stores.starIcon}
                      style={styles.starIcon}
                      tintColor={'#ffa012'}
                    />
                    <View style={{ flexDirection: 'row', }}>
                      {this.state.shop.ratings * 0 == 0 ?
                        <Text style={styles.ratingText}>{this.state.shop.ratings} out of 5</Text>
                        :
                        <Text style={styles.ratingText}>No ratings yet</Text>
                      }
                    </View>
                  </View>
                </View>
              </View>
            </Content>
          </View>
        </ImageBackground>
        <Content>
          <View >
            <TabView
              navigationState={this.state}
              renderScene={SceneMap({
                allproducts: this.AllProducts,
                referred: this.Referred,
                overview: this.Overview,
              })}
              renderTabBar={props =>
                <TabBar
                  {...props}
                  labelStyle={{
                    // fontFamily: 'Roboto', 
                    fontSize: 11,
                    textTransform: 'capitalize',
                    color: '#f36e23'
                  }}
                  indicatorStyle={{ backgroundColor: '#f36e23', borderBottomWidth: 2.50, borderBottomColor: '#f36e23' }}
                  style={{ backgroundColor: '#ffffff', elevation: 0 }}
                  renderLabel={this._renderLabel(props)}
                  onTabPress={({ route, preventDefault }) => {
                    // alert(JSON.stringify(route));
                    if (route.key === 'allproducts') {
                      this.setState({
                        focusedOn: 'allproducts',
                      })
                    }
                    if (route.key === 'referred') {
                      this.setState({
                        focusedOn: 'referred',
                      })
                    }
                    if (route.key === 'overview') {
                      this.setState({
                        focusedOn: 'overview',
                      })
                    }
                  }}
                />
              }

              onIndexChange={index => this.setState({ index })}
              initialLayout={{ width: Dimensions.get('window').width, height: 50 }}
            />
          </View>
          <View
            style={{
              backgroundColor: '#f5f6fa',
              flex: 1,
            }}
          >
          </View>
          <View>
            {this.renderAllProducts()}
            {this.renderReferred()}
            {this.renderOverview()}
            {this.renderProductList()}
            {this.Overview()}
          </View>
        </Content>
      </Container>
    );
  }

}

export default ViewYourShop;