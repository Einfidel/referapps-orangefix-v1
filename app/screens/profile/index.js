import * as React from 'react';
import { useState, useEffect } from 'react';
import { ScrollView, View, Text, Image, ImageBackground, Dimensions, RefreshControl, TouchableOpacity, Platform, Alert, } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import FastImage from 'react-native-fast-image';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
import GridView from 'react-native-super-grid';
import Orders from './orders';
import Store from './store';
import Icons from '../../components/icons';
import DB from '../../components/storage';
import { Content, Container } from 'native-base';
import { useFocusEffect } from '@react-navigation/native';
import HTML from "react-native-render-html";


const { width, height } = Dimensions.get('window');
const initialLayout = { width: Dimensions.get('window').width };

export default function ProfileScreen({ navigation, route }) {

  const [loading, setloading] = useState(false)
  const [loaded, setloaded] = useState(false)
  const [session, setsession] = useState(null)
  const [userdata, setuserdata] = useState(null)
  const [products, setproducts] = useState([])
  const [cover, setCover] = useState(require('../../assets/internal/default-cover.jpg'));
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    { key: 'orders', title: 'ORDERS' },
    { key: 'store', title: 'STORE' },
  ]);
  const [emailVerified, setEmailVerified] = useState(true)
  const [phoneVerified, setPhoneVerified] = useState(true)
  const [hasEmailOrContact, setHasEmailOrContact] = useState(true)

  useEffect(() => {
    init()
    // console.log("CHECK USER DATA IN PROFILE SCREEN", userdata?.info.data.shop)
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      setloading(true);

      async function reload() {
        init().then(() => setloading(false));
      };

      reload();

      return () => {
        isActive = false;
      };
    }, [])
  );

  useEffect(() => {
    const init = async () => {
      var htmlCode = "<b>I am <i>Italic</i></b>";
      // <HTML source={{html: htmlCode}}/>
      Alert.alert(
        remoteMessage.notification.title,
        <HTML source={{ html: htmlCode }} />,
      );
    }

    init();
  }, []);

  const renderScene = SceneMap({
    orders: () => <Orders navigation={navigation} />,
    store: () => <Store navigation={navigation} />,
  });

  const init = async () => {
    setloading(true)

    let ud = await AsyncStorage.getItem("user_data")
    // console.log("this is user_data", ud)
    let _userdata = ud ? JSON.parse(ud) : null
    // console.log('_userdata get in profile screen', _userdata)
    if (_userdata) {
      await Service.userData(
        _userdata.data.id,
        async (res) => {
          console.log('----------------------------------');
          console.log('User Data Fetch in Profile index', res.data);
          console.log('----------------------------------');
          setuserdata(res.data)
          _userdata = res;

          await AsyncStorage.setItem('user_data', JSON.stringify(res));
          // console.log('res.data shop directory:', res.data.info.data.shop.directory)
          // console.log('res.data cover_photo directory:', res.data.info.data.cover_photo.full_path)
          // console.log('res.data shop full_path:', res.data.info.data.shop.full_path)
          setCover({ uri: res.data.info.data.cover_photo.full_path })
        },
        (err) => console.log("Service.userData Error in Profile Screen", err)
      )
      await Service.userProducts(_userdata.data.id,
        (res) => setproducts(res.data),
        (err) => console.log("Service.userProducts Error in Profile Screen", err)
      )

      let splitEmail = _userdata.data.info.data.email.split('@');
      if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
        setHasEmailOrContact(false)
        console.log("user has no email", _userdata.data.info.data.email)
      } else {
        console.log("user has email", _userdata.data.info.data.email)
        setHasEmailOrContact(true)

        if (_userdata.data.is_verify === 'no') {
          console.log("user is not email verified", _userdata.data.is_verify)
          setEmailVerified(false)
        } else {
          console.log("user is email verified", _userdata.data.is_verify)
          setEmailVerified(true)
        }
      }

      if (_userdata.data.is_verify_contact_number === 'no') {
        console.log("user is not phone verified", _userdata.data.is_verify_contact_number)
        setPhoneVerified(false)
      } else {
        console.log("user is phone verified", _userdata.data.is_verify_contact_number)
        setPhoneVerified(true)
      }

      // await console.log("USER DATA", userdata)
      setloading(false)
    } else {
      setloading(false)
    }
  }


  const checkImageUrl = (url) => {
    fetch(url)
      .then(res => {
        console.log("Image Existence", res.status)
        if (res.status == 404) {
          console.log("Profile image does not exist")
        } else {
          this.setState({
            profileImage: <FastImage
              source={{ uri: url }}
              style={{ borderRadius: 55, height: 90, width: 90, marginHorizontal: 18, marginTop: 5 }}
            />
          })
        }
      })
      .catch(err => {
        console.log("Error loading profile image")
      })
  }

  const renderProfileImage = () => {
    if (!userdata && !userdata?.image || !userdata?.image) return <FastImage source={Assets.myProfile.face} style={{ borderRadius: 55, height: 90, width: 90, marginHorizontal: 18, marginTop: 5 }} />
    return (
      <View style={{ flex: 0 }}>
        <FastImage
          source={{ uri: userdata.image }}
          style={{ borderRadius: 55, height: 90, width: 90, marginHorizontal: 18, marginTop: 5 }}
        />
        {userdata?.is_verify == 'yes' ?
          <FastImage source={Assets.myProfile.certifiedIcon} style={{ width: 20, height: 20, marginTop: -10, marginLeft: 55 }} /> : null}
      </View>
    )
  }

  const renderProducts = () => {
    if (!products || products.length == 0) return false
    return (
      <GridView
        data={products}
        vertical={true}
        horizontal={false}
        scrollEnabled={true}
        showsVerticalScrollIndicator={false}
        renderItem={(data) => {
          console.log('datas', products[0].info.data.image.full_path)
          const uri = data.item.info.data.image.full_path;
          return (
            <TouchableOpacity
              onPress={() => navigation.navigate('Search Results', { categorize_by: data.item.id })}
              style={{ alignContent: 'center', width: 150, height: 200, marginTop: 15, backgroundColor: '#fff' }}>
              <FastImage source={products[0].info.data.image.full_path == '' || products[0].info.data.image.full_path == null ? Assets.myProfile.defProductImg : { uri: uri }} style={{ resizeMode: 'stretch', width: '100%', height: 150 }} />
              <Text numberOfLines={2} style={{ fontSize: 12, padding: 5, width: '100%', color: '#231f20', alignSelf: 'flex-start', textAlign: 'left', backgroundColor: '#fff' }}>
                {data.item.product_name}
              </Text>
              <View style={{ flexDirection: 'row', padding: 5, top: 0, paddingTop: 5, backgroundColor: "#fff" }}>
                <Text style={{ flex: 1, alignSelf: 'flex-start', fontSize: 13, color: "red" }}>{`\u20B1 ${data.item.price}`}</Text>
                {[1, 2, 3, 4, 5].map((rate, i) => {
                  return <Icons.AntDesign name={parseInt(data.item.ratings) >= rate ? "star" : "staro"} size={10} color="orange" />
                })}
              </View>
            </TouchableOpacity>
          );
        }}
      />
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={loading}
            onRefresh={init}
            colors={['#00a14b']}
          />
        }>
        <ImageBackground
          // source={Assets.myProfile.bg}
          source={cover}
          style={styles.headerCoverImage} >
          <Container style={styles.backgroundImageContainer}>
            <Content>
              <View style={{ flexDirection: 'row', paddingTop: 10 }}>
                <TouchableOpacity
                  style={{ alignSelf: 'flex-start', paddingLeft: 10 }}
                  onPress={() => navigation.navigate('ProfileOptions', { onBackPress: init })}>
                  <FastImage
                    source={Assets.myProfile.settings}
                    square
                    small
                    style={[styles.cartIcon, { marginLeft: 10, marginRight: 10 }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity style={{ alignSelf: 'flex-start', paddingLeft: 10 }} onPress={() => {
                  Alert.alert('Coming Soon!', 'This feature will be available soon.')
                  // navigation.navigate('Messages')
                }}>
                  <FastImage
                    source={Assets.myProfile.chatIcon}
                    square
                    small
                    style={[styles.cartIcon, { marginLeft: 10, marginRight: 10 }]}
                  />
                </TouchableOpacity>
                <TouchableOpacity style={{ alignSelf: 'flex-start', paddingLeft: 10 }} onPress={() => navigation.navigate('Cart Screen')}>
                  <FastImage
                    source={Assets.myProfile.cartIcon}
                    square
                    small
                    style={[styles.cartIcon, { marginLeft: 10, marginRight: 10 }]}
                  />
                </TouchableOpacity>
              </View>
              <View style={[styles.profileRow, { paddingVertical: 30 }]}>
                {renderProfileImage()}
                <View style={styles.column}>
                  <Text Text style={[styles.userNameText, { width: 200 }]}>{userdata ? userdata?.fname + ' ' + userdata?.lname : ''}</Text>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Products : </Text>
                    <Text style={{ width: 47, textAlign: 'right', color: '#ffff', fontSize: 13 }}>{userdata ? userdata?.products : 0}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Services : </Text>
                    <Text style={{ width: 50, textAlign: 'right', color: '#ffff', fontSize: 13 }}>{userdata ? userdata?.services : 0}</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.userInfoText}>Reviews : </Text>
                    <Text style={{ width: 50, textAlign: 'right', color: '#ffff', fontSize: 13 }}>{userdata ? userdata?.reviews : 0}</Text>
                  </View>
                </View>
              </View>
            </Content>
          </Container>
        </ImageBackground>
        {!hasEmailOrContact || !emailVerified || !phoneVerified ?
          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff", justifyContent: 'center', }}>
            <TouchableOpacity onPress={() => {
              if (!hasEmailOrContact) {
                navigation.navigate("AddEmailMobile");
              } else if (!emailVerified) {
                navigation.navigate('EmailVerification')
              } else if (!phoneVerified) {
                navigation.navigate('PhoneVerification')
              }
            }} style={{
              marginVertical: 3,
              padding: 8,
              backgroundColor: "lightpink",
              borderWidth: 1,
              borderRadius: 8,
              borderColor: 'gray',
              width: width - 10,
            }}>
              <Text style={{ textAlign: 'center', fontSize: 12, color: "#070505", }}>
                Click here to complete your registration process to access more content!
              </Text>
            </TouchableOpacity>
          </View>
          :
          null}
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
            <FastImage source={Assets.myProfile.qrIcon} style={{ width: 20, height: 20, resizeMode: 'contain' }} resizeMode={FastImage.resizeMode.contain} />
            <Text style={{ marginLeft: 10, fontSize: 12, color: "#070505" }}>QR Code</Text>
          </View>
          <View style={{ flex: 1 }}></View>
          <TouchableOpacity onPress={() => {
            // () => navigation.navigate("Scanner")
            console.log("Scanner")
            var htmlCode = "<b>I am <i>Italic</i></b>";
            Alert.alert(
              'sample',
              <HTML source={{ html: htmlCode }} />,
            );
          }} style={{ flex: 2, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>Scan QR Code</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1 }}>
          <TabView
            navigationState={{ index, routes }}
            renderScene={renderScene}
            onIndexChange={setIndex}
            initialLayout={initialLayout}
            renderTabBar={props =>
              <TabBar
                {...props}
                // key={'orders'}
                // title={'ORDERS'}
                labelStyle={{
                  // fontFamily: 'Roboto', 
                  fontSize: 15, color: '#231f20'
                }}
                indicatorStyle={{ backgroundColor: '#f36e23' }}

                style={{
                  backgroundColor: '#7D7D7D',
                  color: '#231f20',
                  elevation: 0,
                  // activeTintColor: '#000',
                  // inactiveTintColor: '#fff',
                }}
              />
            }
          />
        </View>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
            <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>View your shop</Text>
          </View>
          <View style={{ flex: 1 }}></View>
          <TouchableOpacity onPress={() => {
            navigation.navigate('ViewShop')
          }}>
            <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
              <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View your product as</Text>
              <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
            </View>
          </TouchableOpacity>
        </View>
        {/* <View style={{ flex: 1 }}>
          {renderProducts()}
        </View> */}
      </ScrollView >
    </View >
  );
}

const styles = {
  container: {
    backgroundColor: '#ecf0f1',
  },
  // not Authenticated
  redirectContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  redirectTitleText: {
    color: '#231f20',
    // fontFamily: 'Roboto',
    fontSize: 25,
  },
  redirectButton: {
    width: '80%',
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#00a14b',
  },
  redirectButtonText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 20,
  },

  //Authenticated
  headerContainer: {
    height: 190,
    width: Dimensions.get('window').width,
  },
  headerCoverImage: {
    ...Platform.select({
      ios: {
        width: Dimensions.get('window').width,
        height: height / 3,
      },
      android: {
        width: Dimensions.get('window').width,
        height: height / 4,
      }
    })
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  searchText: {
    // fontFamily: 'Roboto',
    color: '#7f8c8d',
  },
  // pinned products
  itemRow: {
    justifyContent: "space-around",
  },
  pinIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  pinItemText: {
    color: '#231f20',
    fontSize: 15,
  },
  historyIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  forwardIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 10,
    marginLeft: 5,
  },

  // Information
  informationContainer: {
    backgroundColor: '#ffffff',
    height: height / 2
  },

  // contentContainer: {
  //   height: height/2+50
  // },

  // Transaction
  historyText: {
    color: '#231f20',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText1: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText2: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 25,
  },
  transactionHistoryText: {
    color: '#f36e23',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },

  // custom
  space: {
    paddingTop: 5,
  },
  space10: {
    paddingTop: 10,
  },
  space20: {
    paddingTop: 20,
  },
  space65: {
    paddingTop: 65,
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  white: {
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 30,
  },
  userNameText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    alignSelf: 'flex-start',
    paddingBottom: 5,
  },
  userInfoText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 12,
    alignSelf: 'flex-start',
    paddingBottom: 4,
  },
  ratingText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 12,
    alignSelf: 'center',
    paddingBottom: 4,
  },
  ratingScore: {
    color: '#f36e23',
    // fontFamily: 'Roboto',
    fontSize: 15,
    alignSelf: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
  avatar: {
    justifyContent: 'space-around',
  },
  avatarImage: {
    alignSelf: 'center',
    height: 90,
    width: 90,
    borderRadius: 45,
  },
  searchItem: {
    paddingLeft: 5,
    backgroundColor: '#ffffff',
    width: '100%',
    height: 30,
  },
  cartIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  profileRow: {
    flexDirection: 'row',
    // justifyContent: 'space-around',
    width: width,
  },
  coverImageContainer: {
    alignItems: 'flex-start',
    height: '100%',
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'space-around',
  },
  backgroundImageContainer: {
    backgroundColor: 'rgba(0, 0, 0, .4)',
    alignItems: 'flex-start',
    height: '100%',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },

  overviewIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
  },
}
