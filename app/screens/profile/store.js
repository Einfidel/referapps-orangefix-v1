import React from 'react';
import { ScrollView, View, Text, Image, ImageBackground, Dimensions, RefreshControl, TouchableOpacity } from 'react-native';
import Assets from '../../components/assets.manager';
import Icons from '../../components/icons';


export default function HomeScreen(props) {

  // console.log("NAV", props.navigation)

  return (
    <View style={{ flex: 1 }}>

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Icons.Entypo name="shop" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>My Shop</Text>
        </View>
        <View style={{ flex: 3 }}></View>
        <TouchableOpacity onPress={() => props.navigation.navigate("ShopOptions")} style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
          <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>Manage your shop</Text>
          <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
        </TouchableOpacity>
      </View>

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 5, flexDirection: 'row', padding: 15 }}>
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Icons.MaterialCommunityIcons name="wallet-outline" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>My Performance</Text>
        </View>
        <View style={{ flex: 3.1 }}></View>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("My Performance")
        }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View Performance</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 5, flexDirection: 'row', padding: 15 }}>
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Icons.MaterialCommunityIcons name="bank" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>Check Request</Text>
        </View>
        <View style={{ flex: 6.1 }}></View>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("CheckRequests")
        }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>Request Withdrawals</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </View>
        </TouchableOpacity>
      </View>

      {/* <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
          <Icons.Octicons name="graph" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>My Performance</Text>
        </View>
        <View style={{ flex: 0.5 }}></View>
        <TouchableOpacity onPress={() => {
          props.navigation.navigate("My Performance")
        }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View Shop Performance</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </View>
        </TouchableOpacity>
      </View> */}

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Icons.MaterialCommunityIcons name="truck-fast" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>Shipping</Text>
        </View>
        <View style={{ flex: 5.5 }}></View>
        <TouchableOpacity onPress={() => props.navigation.navigate("My Shipping")}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View Shipping</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </View>
        </TouchableOpacity>
      </View>

      <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', backgroundColor: "#fff" }}>
        <View style={{ flex: 4, flexDirection: 'row', padding: 15 }}>
          {/* <Image source={Assets.myProfile.qrIcon} style={{width: 20, height: 20, resizeMode: 'contain'}} /> */}
          <Icons.MaterialIcons name="history" size={20} color="#66b545" />
          <Text style={{ marginLeft: 10, fontSize: 12, color: "#00000095" }}>History</Text>
        </View>
        <View style={{ flex: 2.5 }}></View>
        <TouchableOpacity onPress={() => {
          // alert('Coming soon!')
          props.navigation.navigate("History")
        }}>
          <View style={{ flex: 4, flexDirection: 'row', padding: 15, alignSelf: 'center' }}>
            <Text style={{ textAlign: 'right', fontSize: 12, color: "darkorange" }}>View Sales History</Text>
            <Icons.AntDesign name="right" style={{ flex: 0, padding: 2.5 }} color="darkorange" />
          </View>
        </TouchableOpacity>
      </View>

    </View>
  );
}