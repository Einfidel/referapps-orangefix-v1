import React from 'react';
import { useState, useEffect } from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import styles from '../../styles/profile/profileStyle';
import AsyncStorage from '@react-native-community/async-storage';
import Assets from '../../components/assets.manager';
import FastImage from 'react-native-fast-image';

// const BG = require('../../assets/images/internal/default-cover.jpg')
// const authorize = require('../../assets/images/internal/icon_unauthorized.png')

export default function Unauthorized(navigation) {

  const [auth, setauth] = useState(null)
  const [loaded, setloaded] = useState(false)

  const init = async () => {
    // let token = await AsyncStorage.getItem("auth")
    // // console.log("PROFILE TAB TOKEN", token)
    // if (!token) {
    //   setloaded(true)
    // } else {
    //   setauth(token)
    //   setloaded(true)
    //   // navigation.navigation.navigate("ProfileScreen")
    // }

    setloaded(true)
  }

  useEffect(() => {
    init()
  }, [])

  return (
    <View style={{ flex: 1, }}>
      <StatusBar backgroundColor="#00a14b" barStyle="light-content" />
      <ImageBackground source={Assets.unAuth.unauthBg} style={styles.BG}>
        <View style={styles.bgOpacity}>
          <TouchableOpacity style={styles.btnSignIn} onPress={() => navigation.navigation.navigate('Sign In')}>
            <Text style={styles.txtSignIn}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnSignIn} onPress={() => navigation.navigation.navigate('Sign Up')}>
            <Text style={styles.txtSignIn}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={styles.authorizeContainer}>
        <FastImage source={Assets.unAuth.unauthIcon} style={styles.imgAuthorize} resizeMode={FastImage.resizeMode.contain}></FastImage>
        <Text style={styles.txtJoin}>Join ReferApps Now</Text>
      </View>
    </View>
  )
}