import React from 'react';
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, Modal, Platform } from 'react-native';
import { Container, Header, Thumbnail, Content, Left, Right, Body, Title } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import * as Animatable from 'react-native-animatable';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../../components/api/service';
import { Alert } from 'react-native';
const { width, height } = Dimensions.get('window');

export default class YourServices extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'recent', title: 'Recent' },
        { key: 'popular', title: 'Popular' },
        // { key: 'soldout', title: 'Sold Out' },
        // { key: 'stock', title: 'Stock' },
      ],
      loading: true,
      latestProducts: [],
      popularProducts: [],
      // latestProducts: [
      //   {
      //     id: 1,
      //     name: 'Poster',
      //     price: 'P 50.00',
      //     stocks: 138,
      //     sold: 12,
      //   },
      // ],
    };
  }

  componentDidMount() {
    this.handleServices();
  }

  componentDidUpdate() {
    // this.handleServices();
  }

  handleServices = async () => {
    console.log("this.handleServices()")
    const _user = await AsyncStorage.getItem('user_data');
    let user = _user ? JSON.parse(_user) : null;
    if (user) {

      let data = {}
      data.user_id = user.data.id
      data.viewType = 'profile'
      data.sort_by = 'latest'

      await Service.getAccountServices(
        data,
        async (res) => {
          console.log('latest');
          this.setState({
            latestProducts: res,
            user: user,
          });

        },
        (err) => console.log('err', err)
      );

      data.sort_by = 'popularity'
      await Service.getAccountServices(
        data,
        (res2) => {
          console.log('popularity');
          this.setState({
            popularProducts: res2,
            loading: false,
          });
        },
        (err) => console.log('err', err)
      );
    }
  };

  _renderLabel = (props) => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d';

    return <Text style={[{ fontSize: 12 }, { color }]}>{route.title}</Text>;
  };

  handleDeleteService = async (service) => {
    Alert.alert(
      'Warning!',
      "Are you sure you want to delete " + service.name + "?",
      [
        {
          text: 'Yes',
          onPress: async () => {
            this.setState({ loading: true });
            let _user = await AsyncStorage.getItem('user_data');
            let user = _user ? JSON.parse(_user) : null;
            if (user) {
              await Service.deleteAccountService(
                user.id,
                service.id,
                (res) => {
                  Alert.alert('Success', 'Deleted successfully');
                  this.handleServices();
                },
                (err) => console.log(err)
              );
            }
          },
        },
        {
          text: 'No',
        },
      ],
      { cancelable: false }
    );
  };

  renderScene = ({ route }) => {
    switch (route.key) {
      case 'recent':
        return this.Recent(); // passing data as data prop
      case 'popular':
        return this.Popular();
      case 'soldout':
        return this.Soldout();
      case 'stock':
        return this.Stock();
      default:
        return null;
    }
  };

  Popular = () => {
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    this.state.latestProducts.map((service, index) => {
      let inventory = service.info.data.inventory;


      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              highestStockVariant = variant.stock
            }
          }
        })
      }
    })

    return this.state.popularProducts.map((service, index) => {
      let inventory = service.info.data.inventory;
      // console.log(service.info.data);
      return (
        <View key={service.id} style={{ backgroundColor: '#ffffff' }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 25,
            }}
          >
            <Thumbnail
              source={
                service.info.data.image && service.info.data.image.directory ? (
                  { uri: service.info.data.image.thumb_path }
                ) : (
                  Assets.specificProduct.icon1
                )
              }
              style={{
                width: 100,
                height: 100,
                resizeMode: 'contain',
              }}
            />
            <View style={{ marginHorizontal: 20, width: width - 200 }}>
              <Text>{service.product_name}</Text>
              <Text>{`\u20B1 ` + service.price}</Text>
              {/* <Text>Stocks : {inventory.stock != null ?
                inventory.stock
                :
                lowestStockVariant === highestStockVariant ?
                  lowestStockVariant
                  :
                  inventory[0].stock != null ?
                    lowestStockVariant + " - " + highestStockVariant
                    :
                    'N/A'}</Text> */}
              <Text>Ratings: {service.ratings || 'N/A'}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 55 }}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('EditService', { service: service, onBackPress: this.handleServices })
              }
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>EDIT</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleDeleteService(service)}
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>DELETE</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  Soldout = () => {
    let stock = 0;
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    this.state.latestProducts.map((service, index) => {
      let inventory = service.info.data.inventory;

      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            stock = variant.stock;
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              stock = variant.stock;
              highestStockVariant = variant.stock
            }
          }
        })
      } else {
        stock = inventory.stock;
      }
    })

    return this.state.latestProducts.map((service, index) => {
      if (stock === 0) {
        let inventory = service.info.data.inventory;
        // console.log(service.info.data);
        return (
          <View key={service.id} style={{ backgroundColor: '#ffffff' }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 25,
              }}
            >
              <Thumbnail
                source={
                  service.info.data.image && service.info.data.image.directory ? (
                    { uri: service.info.data.image.thumb_path }
                  ) : (
                    Assets.specificProduct.icon1
                  )
                }
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <View style={{ marginHorizontal: 20, width: width - 200 }}>
                <Text>{service.product_name}</Text>
                <Text>{`\u20B1 ` + service.price}</Text>
                {/* <Text>Stocks : {inventory.stock != null ?
                  inventory.stock
                  :
                  lowestStockVariant === highestStockVariant ?
                    lowestStockVariant
                    :
                    inventory[0].stock != null ?
                      lowestStockVariant + " - " + highestStockVariant
                      :
                      'N/A'}</Text> */}
                <Text>Ratings: {service.ratings || 'N/A'}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', height: 55 }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('EditService', { service: service, onBackPress: this.handleServices })
                }
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>EDIT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleDeleteService(service)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    });
  };

  Stock = () => {
    let stock = 0;
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    this.state.latestProducts.map((service, index) => {
      let inventory = service.info.data.inventory;

      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            stock = variant.stock;
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              stock = variant.stock;
              highestStockVariant = variant.stock
            }
          }
        })
      } else {
        stock = inventory.stock;
      }
    })

    return this.state.latestProducts.map((service, index) => {
      if (stock != 0) {
        let inventory = service.info.data.inventory;
        // console.log(service.info.data);
        return (
          <View key={service.id} style={{ backgroundColor: '#ffffff' }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 25,
              }}
            >
              <Thumbnail
                source={
                  service.info.data.image && service.info.data.image.directory ? (
                    { uri: service.info.data.image.thumb_path }
                  ) : (
                    Assets.specificProduct.icon1
                  )
                }
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <View style={{ marginHorizontal: 20, width: width - 200 }}>
                <Text>{service.product_name}</Text>
                <Text>{`\u20B1 ` + service.price}</Text>
                {/* <Text>Stocks : {inventory.stock != null ?
                  inventory.stock
                  :
                  lowestStockVariant === highestStockVariant ?
                    lowestStockVariant
                    :
                    inventory[0].stock != null ?
                      lowestStockVariant + " - " + highestStockVariant
                      :
                      'N/A'}</Text> */}
                <Text>Ratings: {service.ratings || 'N/A'}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', height: 55 }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('EditService', { service: service, onBackPress: this.handleServices })
                }
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>EDIT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.handleDeleteService(service)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    });
  };

  Recent = () => {
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    this.state.latestProducts.map((service, index) => {
      let inventory = service.info.data.inventory;


      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              highestStockVariant = variant.stock
            }
          }
        })
      }
    })

    return this.state.latestProducts.map((service, index) => {
      let inventory = service.info.data.inventory;
      // console.log(service.info.data);
      return (
        <View key={service.id} style={{ backgroundColor: '#ffffff' }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 25,
            }}
          >
            <Thumbnail
              source={
                service.info.data.image && service.info.data.image.directory ? (
                  { uri: service.info.data.image.thumb_path }
                ) : (
                  Assets.specificProduct.icon1
                )
              }
              style={{
                width: 100,
                height: 100,
                resizeMode: 'contain',
              }}
            />
            <View style={{ marginHorizontal: 20, width: width - 200 }}>
              <Text>{service.product_name}</Text>
              <Text>{`\u20B1 ` + service.price}</Text>
              {/* <Text>Stocks : {inventory.stock != null ?
                inventory.stock
                :
                lowestStockVariant === highestStockVariant ?
                  lowestStockVariant
                  :
                  inventory[0].stock != null ?
                    lowestStockVariant + " - " + highestStockVariant
                    :
                    'N/A'}</Text> */}
              <Text>Ratings: {service.ratings || 'N/A'}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 55 }}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('EditService', { service: service, onBackPress: this.handleServices })
              }
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>EDIT</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => this.handleDeleteService(service)}
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>DELETE</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={this.state.loading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: hp('80%'),
          }}
        >
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Loading...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  render() {
    return (
      <Container>
        <Header style={[{ backgroundColor: '#ffffff' }, styles.elevateHeader]}>
          <Left style={styles.left}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail source={Assets.accountSettings.arrowDarkIcon} style={styles.backIcon} />
            </TouchableOpacity>
            <View style={styles.addVariation}>
              <Text style={styles.headerText}>Services</Text>
            </View>
          </Left>
          <Right>
            <TouchableOpacity onPress={() => alert('Under Development')}>
              <Thumbnail source={Assets.specificProduct.iconChat} square style={styles.chatIcon} />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <TabView
            navigationState={this.state}
            renderScene={this.renderScene}
            renderTabBar={(props) => (
              <TabBar
                {...props}
                labelStyle={{
                  // fontFamily: 'regular',
                  fontSize: 11,
                  textTransform: 'capitalize',
                  color: '#f36e23',
                }}
                indicatorStyle={{
                  backgroundColor: '#f36e23',
                  borderBottomWidth: 3,
                  borderBottomColor: '#f36e23',
                }}
                indicatorContainerStyle={{
                  borderBottomWidth: 1,
                  borderBottomColor: '#EDEDEB',
                }}
                style={{ backgroundColor: '#ffffff', elevation: 0 }}
                renderLabel={this._renderLabel(props)}
              />
            )}
            onIndexChange={(index) => this.setState({ index })}
            initialLayout={{ width: Dimensions.get('window').width }}
          />
        </Content>
        {this.handleLoading()}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily: 'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  elevateHeader: {
    ...Platform.select({
      ios: {
        justifyContent: 'center',
        marginTop: 0
      },
      android: {
        justifyContent: 'center',
        marginTop: 30
      }
    })
  }
});
