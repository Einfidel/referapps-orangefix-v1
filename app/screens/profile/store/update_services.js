import React from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Modal,
  TouchableWithoutFeedback,
  Alert,
  StyleSheet,
  Platform
} from 'react-native';
import { Container, Header, Thumbnail, Item, Input } from 'native-base';
import { sell, styles } from './styles';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../../components/api/service';
import Assets from '../../../components/assets.manager';

const { width, height } = Dimensions.get('window');

const rightArrow = require('../../../assets/ProfileIcons/iconb_sub.png');
const info = require('../../../assets/internal/icon_condition.png');
const sellImg = require('../../../assets/sellProduct.png');
// const rightArrow = Assets.accountSettings.arrowDarkIcon;
// const info = Assets.uploadProduct.conditionIcon;
// const sellImg = Assets.

// const recurr =[{options: 'No' }, {options:'Yes'}]
const recurr = ['Yes', 'No'];

export default class UpdateServiceScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      productName: '',
      description: '',
      price: '',
      switch_status: false,
      user: {},
      errors: {},
      responseData: {},
      images: [],
      filePath: {
        data: '',
        uri: '',
      },
      isLoading: false,
      fileData: '',
      imageUpdated: false,
      nameFocused: false,
      descriptionFocused: false,
      priceFocused: false,

      modalVisible: false,
      referrerModal: false,
      referrer_commission: '',
      option: '',
      isRecurring: '',
      categoryId: '1',
      optionModal: false,
      userServices: '',
    };
    this.page = 1;
  }

  componentDidMount() {
    this.fetchService();
    this.setState({
      isRecurring: this.props.route.params.item.is_recurring,
      referrer_commission: this.props.route.params.item.referrer_commission,
      productName: this.props.route.params.item.product_name,
      description: this.props.route.params.item.info.data.description,
      price: this.props.route.params.item.price,
    });
    this.setState((prevState) => ({
      images: [
        ...prevState.images,
        {
          uri: this.props.route.params.item.info.data.image.full_path,
        },
      ],
    }));
    console.log(JSON.stringify(this.props.route.params, null, '\t'));
  }

  fetchService = async () => {
    const _user = await AsyncStorage.getItem('user_data');
    let user = _user ? JSON.parse(_user) : null;
    if (user) {
      await Service.getAccountServices(
        user.data.id,
        1,
        (res) => {
          this.setState({
            userServices: res,
            user: user,
          });
        },
        (err) => console.log(err)
      );
    }
  };

  handleUpdateService = async () => {
    let ctegID = this.props.route.params.item.info.data.category_id;
    // console.log(this.state.user.data);
    await Service.updateAccountService(
      {
        uid: this.state.user.data.id,
        pid: this.props.route.params.item.id,
        cid: this.props.route.params.item.info.data.category_id,
        ccode: this.state.user.data.country_code,
        service: this.state,
        images: this.state.images,
        // attributes: [
        //   {
        //     attribute: 'Model',
        //     value: '10',
        //     required: 'yes',
        //     category_id: ctegID,
        //   },
        //   {
        //     attribute: 'Storage Capacity',
        //     value: '20',
        //     required: 'yes',
        //     category_id: ctegID,
        //   },
        // ],
        attributes: '',
      },
      (res) => {
        if (res.status) {
          Alert.alert(
            'Succesfully!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  await this.fetchService();
                  await this.props.route.params.onBackPress();
                  await this.props.navigation.goBack();
                },
              },
            ],
            { cancelable: false }
          );
        } else {
          alert(res.msg);
          return false;
        }
      },
      (err) => console.log(err)
    );
  };

  handleImagePicker = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }
        this.setState((prevState) => ({
          images: [...prevState.images, response],
          filePath: response.path,
          imageUpdated: true,
          fileData: response.data,
        }));
      }
    });
  };

  onPressButton = (value) => {
    this.setState({ isRecurring: value });
  };

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  setOptionModal(visible) {
    this.setState({ optionModal: visible });
  }

  renderModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.modalVisible} style={styles.modalCon}>
          {this._renderReferrModal()}
          {/* {this.state.referrerModal && this._renderRefferModal()} */}
        </Modal>
      </View>
    );
  };

  optionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal animationType='slide' transparent={true} visible={this.state.optionModal} style={styles.modalCon}>
          <TouchableWithoutFeedback
            onPress={() => {
              this.setOptionModal(!this.state.optionModal);
            }}
          >
            <View style={styles.modalOption}>
              <View style={styles.modalContainer}>
                <Text
                  style={styles.yes}
                  onPress={() => {
                    this.onPressButton('yes');
                    this.setOptionModal(!this.state.optionModal);
                  }}
                >
                  Yes
                </Text>
                <Text
                  style={styles.no}
                  onPress={() => {
                    this.onPressButton('no');
                    this.setOptionModal(!this.state.optionModal);
                  }}
                >
                  No
                </Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  _renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.setModalVisible(!this.state.modalVisible);
        }}
      >
        <View style={styles.containerModal}>
          <View style={styles.referrContainer}>
            <Text style={styles.txtSet}>Set Referrer Commission(5% - 50%)</Text>
            <Item style={styles.txtInputsCon}>
              <Input
                style={styles.txtInput}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7F8C8D'}
                keyboardType={'number-pad'}
                value={this.state.referrer_commission}
                onChangeText={(text) => this.setState({ referrer_commission: text })}
                maxLength={2}
              />
              <Text style={styles.txtPercent}>%</Text>
            </Item>
            <TouchableOpacity style={styles.btnCon} onPress={() => this._renderReferrerSaveButton()}>
              <Text style={{ color: '#FFF', textAlign: 'center' }}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  _renderReferrerSaveButton = () => {
    if (this.state.referrer_commission < 5 || this.state.referrer_commission > 50) {
      return alert('Invalid Percentage! (5% - 50%)');
    } else {
      this.setState({
        referrerModal: false,
        modalVisible: false,
        // lastCommission: this.state.referrer_commission,
      });
    }
  };

  render() {
    return (
      <View style={sell.serviceContainer}>
        <Header style={[styles.headerCon, stylings.headerShow]}>
          <View style={styles.imgBtnCon}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={require('../../../assets/headericon/left-arrow-dark.png')} style={[styles.imgBtnBack, stylings.arrowImg]} />
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: 'center' }}>
            <Text style={styles.txtService}> Edit Service </Text>
          </View>
        </Header>
        <ScrollView style={{ flex: 1 }}>
          <View style={{ backgroundColor: '#ecf0f1', height: 210 }}>
            <TouchableOpacity
              onPress={() => {
                this.handleImagePicker();
              }}
              style={{ justifyContent: 'center' }}
            >
              <Image
                source={this.state.images == '' ? sellImg : this.state.images}
                style={{
                  height: 210,
                  width: 180,
                  resizeMode: 'contain',
                  justifyContent: 'center',
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.concon}>
            <View style={styles.txtItem}>
              <Input
                style={styles.txtInputService}
                onChangeText={(name) => this.setState({ productName: name })}
                value={this.state.productName}
                placeholder='Service Name'
                placeholderTextColor='#7f8c8d'
                maxLength={50}
                onFocus={() => {
                  if (this.state.errors.product_name) {
                    this.setState({
                      nameFocused: true,
                    });
                  }
                }}
              />
            </View>
            {/* <Text style={styles.txtLength}>{this.state.productName.length}/50</Text> */}
          </View>
          <View style={styles.des}>
            <TextInput
              style={styles.txtInputDescription}
              onChangeText={(description) => this.setState({ description: description })}
              value={this.state.description}
              placeholder='Description'
              editable={true}
              multiline={true}
              onFocus={() => {
                if (this.state.errors.description) {
                  this.setState({
                    descriptionFocused: true,
                  });
                }
              }}
            />
          </View>
          <View style={styles.ref}>
            {this.renderModal()}
            <TouchableOpacity style={styles.referrBtn} onPress={() => this.setModalVisible(true)}>
              <Text style={sell.inputText}>Referrer Commission</Text>
              <View style={styles.setCommissionCon}>
                <Text style={styles.txtSetCommission}>
                  {this.state.referrer_commission == '' ? 'Set Commission %' : this.state.referrer_commission + '%'}
                </Text>
                <Image source={rightArrow} style={styles.imgRight} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={[styles.concon, { marginTop: 10 }]}>
            {this.optionsModal()}
            <TouchableOpacity onPress={() => this.setOptionModal(true)} style={styles.imgRecurringCon}>
              <View style={styles.imgCon}>
                <Image source={info} style={styles.imgInfo} />
                <Text style={sell.inputText}>Recurring</Text>
              </View>
              <View style={styles.pickerCon}>
                <Text style={styles.txtValue}>{this.state.isRecurring}</Text>
                <Image source={rightArrow} style={styles.imgRight} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.concons}>
            <View style={styles.priceCon}>
              <View style={{ flexDirection: 'row' }}>
                <View style={sell.spaceLeft} />
                <Thumbnail
                  source={require('../../../assets/internal/icon_price.png')}
                  square
                  small
                  style={styles.cheese}
                />
                <View style={sell.spaceLeft} />
                <Text style={sell.inputText}>Price ({this.state.user.country_code === 'PH' ? '₱' : '$'})</Text>
              </View>
              <Item style={sell.input_title}>
                <Input
                  style={sell.group_input_text}
                  placeholder={'Price'}
                  placeholderTextColor={'#7f8c8d'}
                  keyboardType={'decimal-pad'}
                  onChangeText={(x) => this.setState({ price: x })}
                  value={this.state.price}
                  onFocus={() => {
                    if (this.state.errors.price) {
                      this.setState({
                        priceFocused: true,
                      });
                    }
                  }}
                />
              </Item>
            </View>
          </View>
        </ScrollView>
        <View style={{ height: 50 }}>
          <TouchableOpacity
            // onPress={() => alert('Under Development Stage')}
            onPress={() => {
              this.handleUpdateService();
            }}
            style={styles.btnSell}
          >
            <Text style={styles.txtSell}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const stylings = StyleSheet.create({
  headerShow: {
    ...Platform.select({
      ios: {
        marginTop: 0
      },
      android: {
        marginTop: 30
      }
    })
  },
  arrowImg: {
    ...Platform.select({
      ios: {
        marginTop: 15
      },
      android: {
        marginTop: 3
      }
    })
  }
})