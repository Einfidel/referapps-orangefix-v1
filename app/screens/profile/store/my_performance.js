
import React, {
  useState,
  useEffect,
} from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
  RefreshControl,
  ScrollView,
  Alert,
  FlatList,
} from 'react-native';

import {
  ListItem,
  Header,
  Left,
  Body,
  Right,
  Thumbnail,
  Container,
  Spinner,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Assets from '../../../components/assets.manager'
import Icon from '../../../components/icons';
import FastImage from 'react-native-fast-image';
import Service from '../../../components/api/service';
import moment from 'moment';
import { error } from 'react-native-gifted-chat/lib/utils';

const { width, height } = Dimensions.get('window');

export default function MyPerformance({ navigation, route }) {
  const [credit, setCredit] = useState(0.0);
  const [debit, setDebit] = useState(0.0);
  const [commission, setCommission] = useState(0.0);
  const [totalIncome, setTotalIncome] = useState(0.0);
  const [transactions, setTransactions] = useState([]);
  const [orderHistoryList, setOrderHistoryList] = useState([]);
  const [userData, setUserData] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    init();
  }, [])

  const init = async () => {
    setLoading(true);
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    setUserData(user);

    let data = {};
    data.user_id = user.data.id;

    await Service.getIncomeLedger(
      data,
      (res) => {
        // console.log("res", res)
        // console.log("res.data[0]", res.data[0])
        // console.log("res.data", res.data)
        setCredit(res.credit);
        setDebit(res.debit);
        setCommission(res.commission);
        setTotalIncome(res.total_income);
        setTransactions(res.data);
        // console.log("setTransactions", res.data)
        setLoading(false);
      },
      (err) => {
        setLoading(false);
        console.log("Service.getIncomeLedger error", err)
      });

    let data2 = {}
    data2.user_id = user.data.id;

    try {
      await Service.transactionSellerHistory(
        data2,
        (res2) => {
          console.log("res2", res2)
          setOrderHistoryList(res2)
        },
        (err) => {
          setLoading(false);
          console.log("transactionSellerHistory error", err)
        });
    } catch (error) {
      // alert('transactionReceive error', error);
      console.log('transactionSellerHistory error', error)
    }
  }

  const alertProductSales = () => {
    Alert.alert(
      "Product Sales",
      "The total amount of sales you acquired.",
      [
        {
          text: 'Close',
          style: 'cancel'
        }
      ],
      { cancelable: true }
    )
  }

  const alertSalesCommissions = () => {
    Alert.alert(
      "Sales Commissions",
      "The total amount of commissions you acquired.",
      [
        {
          text: 'Close',
          style: 'cancel'
        }
      ],
      { cancelable: true }
    )
  }

  const renderTranscations = (transactionItem) => {
    let transaction = transactionItem.item;
    let symbol = '+';
    let type = transaction?.reference;
    let color = 'gray';
    let itemType = 'product';


    if (type === 'sales' &&
      orderHistoryList.find((item) => item.order_number.includes(transaction?.order_id)) != null) {
      itemType = 'Item';
    } else {
      itemType = 'Service';
    }

    switch (type) {
      case 'sales':
        color = 'green'
        break;
      case 'commission':
        color = 'orange'
        break;

      default:
        color = 'crimson'
        break;
    }

    if (transaction?.item?.reference === 'credit') {
      symbol = '-';
    }

    // console.log("transaction", transaction)
    // console.log("transaction?.reference", transaction?.reference)
    // console.log("transaction?.reference", transaction?.item?.reference)
    // console.log("transaction?.item?.date.data.added_in.timestamp", transaction?.item?.date.data.added_in.timestamp.date)
    return (
      // <ListItem
      //   avatar
      //   noIndent
      //   style={{}}>
      <View key={transaction?.id}
        style={{
          flex: 1,
          flexDirection: 'row',
          marginHorizontal: 10,
          marginBottom: 10,
          paddingBottom: 5,
          padding: 5,
          borderRadius: 5,
          borderWidth: 1,
          borderBottomWidth: 2,
          borderColor: color,
          backgroundColor: '#E4F0F6',
        }}>
        <View style={{ justifyContent: 'flex-start', }}>
          <View>
            <Text style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>
              <Text >
                {"Type: "}
                <Text style={{ fontWeight: 'bold' }}>
                  {transaction?.reference}
                </Text>
              </Text>
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14 }}>
              {"Product Type: "}{itemType}
            </Text>
          </View>
          <View>
            <Text style={{ color: '#000' }}>
              {"Amount: "}
              <Text style={{ fontSize: 14, color: color, fontWeight: 'bold' }}>
                {symbol + " "}{transaction?.amount}
              </Text>
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14 }}>
              {"Ref-ID: "}{transaction?.reference_id}
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14 }}>
              {"Order-ID: "}{transaction?.order_id}
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14 }}>
              {"Date: "} {new moment(transaction?.date.data.added_in.timestamp.date).format('MMMM Do YYYY, h:mm:ss a')}
            </Text>
          </View>
          {renderViewTransactionBtn(transaction, itemType)}
        </View>
      </View>
      // </ListItem >
    )
  }

  const renderViewTransactionBtn = (transaction, itemType) => {

    if (itemType === 'Item') {
      // orderHistoryList.some(item => item.order_number === transaction?.order_id)) {
      return (
        <View style={{ alignItems: 'baseline' }}>
          <TouchableOpacity style={{
            // justifyContent: 'space-around',
            // alignSelf: 'center',
            backgroundColor: '#5B9857',
            borderRadius: 5,
            paddingVertical: 2,
            paddingHorizontal: 10,
          }} onPress={async () => {
            setLoading(true);

            let data = {}
            data.user_id = userData.data.id;
            data.order_id = transaction?.order_id;
            data.user_type = 'seller';

            try {
              await Service.transactionSearchOrder(
                data,
                (res) => {
                  console.log("res[0]", res[0])
                  setLoading(false);

                  navigation.navigate("TransactionDetails", {
                    id: res[0].order_id,
                    shipping: res[0].info.data.shipping_info,
                    billing: res[0].info.data.billing_info,
                    orders: res[0].order_list,
                    user: res[0].info.data.user_info,
                    date: res[0].date.data.added_in,
                    total: res[0].total_amount,
                    description: res[0].description
                  })
                },
                (err) => {
                  setLoading(false);
                  console.log("transactionSellerHistory error", err)
                });
            } catch (error) {
              // alert('transactionReceive error', error);
              console.log('transactionSellerHistory error', error)
            }
          }}>
            <Text style={{
              alignSelf: 'center',
              fontSize: 14,
              color: '#ffffff',
            }}>
              View Transaction
            </Text>
          </TouchableOpacity>
        </View>
      )
    }
  }

  const renderEmptyNotifications = () => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: height / 2
            // marginTop: width / 1.5,
          }}>
          <View style={{ paddingVertical: 5 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: '#231f20' }}>
              No Transactions yet
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <Container style={{
      backgroundColor: '#EDEDEB',
    }}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}>
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Thumbnail
              source={Assets.accountSettings.iconBack}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
          <Text style={{
            // fontFamily: 'Roboto',
            fontSize: 20,
            color: '#231f20',
          }}>My Performance</Text>
        </Body>
        <Right style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
          >
            <Thumbnail
              source={Assets.specificProduct.iconChat}
              square
              style={{
                width: 22,
                height: 22,
                resizeMode: 'contain',
                tintColor: '#00a14b',

              }}
            />
          </TouchableOpacity>
        </Right>
      </Header>
      {/* <Body> */}
      <Container style={{ backgroundColor: '#ccc' }}>
        <View style={{ paddingVertical: 15, backgroundColor: 'white' }}>
          <View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text>Product Sales</Text>
              </View>
              <View style={{ flex: 1, paddingTop: 3, alignItems: 'flex-end' }}>
                <TouchableOpacity onPress={() => { alertProductSales() }}>
                  <Icon.EvilIcons name="question" size={20} color="#66b545" />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
              <Text style={{ color: 'orange', fontSize: 24, fontWeight: '300' }}>₱ {Number(debit).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}</Text>
            </View>
          </View>
          <View
            style={{
              borderBottomColor: '#ccc',
              borderBottomWidth: 1,
              marginHorizontal: 15,
              paddingTop: 20,
            }}
          />
          <View>
            <View style={{ paddingHorizontal: 20, paddingTop: 20, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text>Sales Commissions</Text>
              </View>
              <View style={{ flex: 1, paddingTop: 3, alignItems: 'flex-end' }}>
                <TouchableOpacity onPress={() => { alertSalesCommissions() }}>
                  <Icon.EvilIcons name="question" size={20} color="#66b545" />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
              <Text style={{ color: 'green', fontSize: 24, fontWeight: '300' }}>₱ {Number(commission).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}</Text>
            </View>
          </View>

          <View
            style={{
              borderBottomColor: '#ccc',
              borderBottomWidth: 1,
              marginHorizontal: 15,
              paddingVertical: 10,
              marginBottom: 10,
            }}
          />

          <View style={{ paddingVertical: 15, backgroundColor: 'white' }}>
            <View style={{ marginBottom: 10 }}>
              <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
                <View style={{}}>
                  <FastImage
                    source={Assets.stores.ledgerIcon}
                    style={{ width: 20, height: 20, marginRight: 10 }}
                  />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={{ fontSize: 16, color: '#7a7a7a' }}>Ledger</Text>
                </View>
              </View>
            </View>

            {!loading ?
              <ScrollView
                // contentContainerStyle={{}}
                nestedScrollEnabled={true}
                style={{ height: height / 1.8 }}
                refreshControl={
                  <RefreshControl
                    refreshing={loading}
                    onRefresh={() =>
                      init().then(() => setLoading(false))
                    }
                  />
                }>
                <FlatList
                  nestedScrollEnabled
                  data={transactions}
                  renderItem={renderTranscations}
                  //   // console.log("item", item);
                  //   return item.id;
                  // }}
                  // listKey={(index) => `notification${index}-${Date.now()}`}
                  ListEmptyComponent={renderEmptyNotifications}
                // extraData={notifications}
                />
              </ScrollView >
              : <Spinner />}

            {/* <View>
                  <View style={{ paddingHorizontal: 20, paddingTop: 10, flexDirection: 'row' }}>
                    <View style={{}}>
                      <FastImage
                        source={Assets.stores.earningIcon}
                        style={{ width: 20, height: 20, marginRight: 10 }}
                      />
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ color: '#7a7a7a' }}>Overal Earning</Text>
                    </View>
                  </View>
                </View> */}
          </View>
        </View>

      </Container>
      {/* </Body> */}


    </Container >
  );
}