import React from 'react'
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, ScrollView } from 'react-native'
import { Container, Header, Thumbnail, Content, Left, Right, Spinner } from 'native-base'
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../../components/assets.manager'
import AsyncStorage from '@react-native-community/async-storage';
import Icons from '../../../components/icons';
import Endpoints from '../../../components/api/endpoints';
import Service from '../../../components/api/service';
const { width, height } = Dimensions.get('window')

const Transaction = (props) => {
  return (
    <View style={{ marginBottom: 15 }}>
      <View style={{ width: '100%', padding: 15, borderWidth: 0, borderRadius: 5, borderBottomWidth: 0, backgroundColor: '#66B548' }}>
        <Text style={{ fontWeight: 'bold', fontSize: 16, color: '#fff' }}>Order #{props.data.order_id}</Text>
        <Text style={{ color: '#fff' }}>{`${props.data.date.data.added_in.month_year} @ ${props.data.date.data.added_in.time_passed}`}</Text>
      </View>
      <View style={{ width: '100%', padding: 15, borderWidth: 0.2, flexDirection: 'row', backgroundColor: '#fff', borderBottomStartRadius: 5, borderBottomEndRadius: 5 }}>
        <View style={{ flex: 4, padding: 15 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>{props.data.payment_type.toUpperCase()}</Text>
          <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }}>STATUS: {props.data.payment_status.toUpperCase()}</Text>
        </View>
        <View style={{ flex: 8, padding: 15 }}>
          <Text style={{ color: 'rgba(0, 0, 0, 0.5)' }} numberOfLines={3}>{props.data.description}</Text>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Total: {props.data.total_amount.toFixed(2)}</Text>
        </View>
        <TouchableOpacity onPress={() => props.props.navigation.navigate("TransactionDetails", {
          id: props.data.order_id,
          shipping: props.data.info.data.shipping_info,
          billing: props.data.info.data.billing_info,
          orders: props.data.order_list,
          user: props.data.info.data.user_info,
          date: props.data.date.data.added_in,
          total: props.data.total_amount,
          description: props.data.description
        })} style={{ flex: 2, justifyContent: 'center', alignItems: 'flex-end' }}>
          <Icons.EvilIcons name="chevron-right" size={45} color="#66B548" />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default class History extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      transactions: [],
      loading: false,
    }
  }

  componentDidMount() {
    this.getAllHistory()
  }

  getAllHistory = async () => {
    this.setState({ loading: true })

    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    let data = {};
    data.user_id = user.data.id;

    try {
      await Service.transactionSellerHistory(
        data,
        (response) => {
          this.setState({ transactions: response })
        },
        (err) => {
          console.warn('Error', err.message)
        },
      );
    } catch (error) {
      // alert('transactionReceive error', error);
      console.log('transactionReceive error', error)
    }

    this.setState({ loading: false })
  }

  _renderLabel = props => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d'

    return (
      <Text style={[{ fontSize: 12 }, { color }]}>
        {route.title}
      </Text>
    );
  };

  renderTransactions = () => {
    return (
      <View style={{ marginHorizontal: 20, marginVertical: 15 }}>
        {this.state.transactions.map((data, i) => {
          return (
            <Transaction key={i} data={data} props={this.props} />
          )
        })}
      </View>
    )
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#FFF' }}>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <Left style={styles.left}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                style={styles.backIcon} />
            </TouchableOpacity>
            <View style={styles.addVariation}>
              <Text style={styles.headerText}>History</Text>
            </View>
          </Left>
          <Right style={{ right: 15, alignSelf: 'center' }}>
            <TouchableOpacity onPress={() => alert('Under Development')}>
              <Thumbnail
                source={Assets.specificProduct.iconChat}
                square
                style={styles.chatIcon}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <View style={{ backgroundColor: '#ffffff' }}>
            {this.state.loading ? <Spinner /> :
              this.state.transactions.length > 0 ?
                this.renderTransactions()
                :
                <View style={styles.referredSection}>
                  <Text style={styles.referredText}>No Store Transactions yet.</Text>
                </View>
            }
          </View>


        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left'
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  addVariation: {
    marginLeft: 25
  },
  chatIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  referredSection: {
    // backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },
})