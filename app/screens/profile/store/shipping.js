import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import { Container, Header, Thumbnail, Content } from 'native-base'
import Assets from '../../../components/assets.manager'
class Shipping extends React.Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#f5f6fa' }}>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <View style={[styles.headerStyles, { left: 20 }]}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                style={styles.backIcon} />
            </TouchableOpacity>
          </View>
          <Text style={styles.headerText}>Deliveries</Text>
          <View style={[styles.headerStyles, { right: 20 }]}>
            {/* <TouchableOpacity onPress ={() => alert('Coming Soon !')}>
              <Thumbnail
                source={Assets.specificProduct.iconChat}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity> */}
          </View>
        </Header>
        <Content>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('To Ship')}
            style={styles.categoryContainer}>
            <Thumbnail
              source={Assets.stores.shipIcon}
              square
              style={styles.forwardIcon}
            />
            <Text style={styles.title}>To Ship</Text>
          </TouchableOpacity>
          <View style={styles.border} />
          <TouchableOpacity onPress={() => this.props.navigation.navigate('In Transit')}
            style={styles.categoryContainer}>
            <Thumbnail
              source={Assets.stores.transitIcon}
              square
              style={styles.forwardIcon}
            />
            <Text style={styles.title}>In Transit</Text>
          </TouchableOpacity>
          <View style={styles.border} />
          <TouchableOpacity onPress={() => this.props.navigation.navigate('Delivered')}
            style={styles.categoryContainer}>
            <Thumbnail
              source={Assets.stores.deliveredIcon}
              square
              style={styles.forwardIcon}
            />
            <Text style={styles.title}>Delivered</Text>
          </TouchableOpacity>
          <View style={styles.border} />
        </Content>

      </Container>
    )
  }
}
export default (Shipping)

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18
  },
  headerStyles: {
    position: 'absolute',
    alignSelf: 'center'
  },
  headerText: {
    // fontFamily:'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    alignSelf: 'center'
  },
  forwardIcon: {
    width: 20,
    height: 20
  },
  title: {
    // fontFamily:'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 15,
  },
  categoryContainer: {
    paddingHorizontal: 20,
    paddingVertical: 13,
    flexDirection: 'row',
    backgroundColor: '#ffffff'
  },
  border: {
    borderBottomWidth: 1,
    borderBottomColor: '#d3d3d3'
  }

})