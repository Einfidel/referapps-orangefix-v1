
import React, {
  useState,
  useEffect,
} from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Image,
  Dimensions,
  RefreshControl,
  ScrollView,
  Alert,
  FlatList,
} from 'react-native';

import {
  ListItem,
  Header,
  Left,
  Body,
  Right,
  Thumbnail,
  Container,
  Spinner,
} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import Assets from '../../../components/assets.manager'
import Icon from '../../../components/icons';
import FastImage from 'react-native-fast-image';
import Service from '../../../components/api/service';
import { useFocusEffect } from '@react-navigation/native';
import moment from 'moment';

const { width, height } = Dimensions.get('window');

export default function MyPerformance({ navigation, route }) {
  const [credit, setCredit] = useState(0.0);
  const [income, setIncome] = useState(0.0);
  const [totalIncome, setTotalIncome] = useState(0.0);
  // const [checkRequests, setCheckRequests] = useState([]);
  const [checkRequests, setCheckRequests] = useState([
    // {
    //   check_id: '3F7C66D5FBD2',
    //   address: "H1, B1, S1, ALGA, CATARMAN, CAMIGUIN",
    //   mo_transaction: "cs_pickup",
    //   tin_ssn: "123456",
    //   amount: "1500.00",
    //   status: "declined",
    //   authentication_code: "OXU49P",
    //   authentication_expiration: "2021-12-18 11:14:00",
    //   reason: "test reason 1",
    // },
    // {
    //   check_id: '5BA144F2046F',
    //   address: "H3,B3, S3, SAN JOSE, LIBJO (ALBOR), DINAGAT ISLANDS",
    //   mo_transaction: "cs_pickup",
    //   tin_ssn: "111222",
    //   amount: "3000.00",
    //   status: "expired",
    //   authentication_code: "QT8PS4",
    //   authentication_expiration: "2021-12-12 18:14:00",
    //   reason: "test reason 2",
    // },
    // {
    //   check_id: '3F7C66D5FBD2',
    //   address: "H1, B1, S1, ALGA, CATARMAN, CAMIGUIN",
    //   mo_transaction: "cs_pickup",
    //   tin_ssn: "123456",
    //   amount: "1500.00",
    //   status: "approved",
    //   authentication_code: "OXU49P",
    //   authentication_expiration: "2021-12-18 11:14:00",
    //   reason: "test reason 1",
    // },
    // {
    //   check_id: '5BA144F2046F',
    //   address: "H3,B3, S3, SAN JOSE, LIBJO (ALBOR), DINAGAT ISLANDS",
    //   mo_transaction: "cs_pickup",
    //   tin_ssn: "111222",
    //   amount: "3000.00",
    //   status: "needs_authentication",
    //   authentication_code: "QT8PS4",
    //   authentication_expiration: "2021-12-12 18:14:00",
    //   reason: "test reason 2",
    // },
    // {
    //   check_id: '3F7C66D5FBD2',
    //   address: "H1, B1, S1, ALGA, CATARMAN, CAMIGUIN",
    //   mo_transaction: "cs_pickup",
    //   tin_ssn: "123456",
    //   amount: "1500.00",
    //   status: "pending",
    //   authentication_code: "OXU49P",
    //   authentication_expiration: "2021-12-18 11:14:00",
    //   reason: "test reason 1",
    // },
  ]);
  const [userData, setUserData] = useState({});
  const [loading, setLoading] = useState(true);
  const [canWithdraw, setCanWithdraw] = useState(false);

  useEffect(() => {
    init();
  }, [])

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      setLoading(true);

      async function reload() {
        init().then(() => setLoading(false));
      };

      reload();

      return () => {
        isActive = false;
      };
    }, [])
  );

  const init = async () => {
    setLoading(true);
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    setUserData(user);

    let data = {};
    data.user_id = user.data.id;

    await Service.getIncomeLedger(
      data,
      async (res) => {
        // console.log("res", res)
        // console.log("res.data[0]", res.data[0])
        setCredit(res.credit);
        setIncome(res.total_income);
        setTotalIncome(res.total_income);
        // console.log("user.data.country_code === 'PH'", user.data.country_code === 'PH')
        if (user.data.country_code === 'PH') {
          // console.log("parseFloat(res.total_income) >= 500", parseFloat(res.total_income) >= 500)
          if (parseFloat(res.total_income) >= 500) {
            // console.log("setCanWithdraw", true)
            setCanWithdraw(true)
          }
        } else if (user.data.country_code === 'US') {
          if (parseFloat(res.total_income) >= 10) {
            setCanWithdraw(true)
          }
        }

        await Service.getCheckRequestList(
          data,
          (res2) => {
            // console.log("setLoading(false)")
            setCheckRequests(res2.data);

            setLoading(false);
          },
          (err) => {
            setLoading(false);
            console.log("Service.getCheckRequestList error", err)
          });
      },
      (err) => {
        setLoading(false);
        console.log("Service.getIncomeLedger error", err)
      });
    // console.log("checkRequests", checkRequests)

    // setCheckRequests(res.data);
  }

  const alertProductSales = () => {
    Alert.alert(
      "Product Sales",
      "The total amount of Income you acquired.",
      [
        {
          text: 'Close',
          style: 'cancel'
        }
      ],
      { cancelable: true }
    )
  }

  const handleWithdrawRequest = () => {
    if (canWithdraw) {
      setLoading(false);
      let splitEmail = userData.data.info.data.email.split('@');

      if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
        // userData.data.is_verify_contact_number === "no" ||
        userData.data.is_verify === 'no') {
        Alert.alert(
          'Warning!',
          'Your account not yet been verified.\n' +
          'Verify Now!',
          [
            {
              text: 'Cancel'
            },
            {
              text: 'Verify',
              onPress: () => {
                if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                  navigation.navigate("AddEmailMobile");
                } else if (userData.data.is_verify === 'no') {
                  navigation.navigate('EmailVerification')
                } else if (userData.data.is_verify_contact_number === "no") {
                  navigation.navigate('PhoneVerification')
                }
              }
            },
          ],
          { cancelable: true }
        );
        return
      }

      let cancel = false;
      let status;

      // checkRequests.map((request, index) => {
      //   if (request.status === 'pending') {
      //     status = request.status
      //     cancel = true;
      //     return
      //   } else if (request.status === 'needs_authentication') {
      //     status = request.status
      //     cancel = true;
      //     return
      //   }
      // })

      if (!cancel) {
        navigation.navigate("CheckRequestForm")
      } else {
        if (status === 'pending') {
          Alert.alert(
            'Warning!',
            'You already have a pending Check Request.\n' +
            "Please wait for it's approval before requesting a new one.",
            [
              {
                text: 'ok'
              },
            ],
            { cancelable: false }
          );
          cancel = true;
          return
        } else if (status === 'needs_authentication') {
          Alert.alert(
            'Warning!',
            "You already have a Check Request that's waiting for verification.\n" +
            "Please verify that request so that we can review it for approval.",
            [
              {
                text: 'ok'
              },
            ],
            { cancelable: false }
          );
          cancel = true;
          return
        }
      }

    } else {
      setLoading(false);
      let symbol = '₱';
      let minAmount = '500';
      if (userData.data.country_code === 'PH') {
        symbol = '₱';
        minAmount = '500';
      } else if (userData.data.country_code === 'US') {
        symbol = '$';
        minAmount = '10';
      }
      Alert.alert(
        "Alert",
        "Seller does not have the minimum amount of funds to withdraw (" + symbol + minAmount + ")"
      )
    }
  }

  const renderCheckRequests = (checkRequestItem) => {
    let checkRequest = checkRequestItem.item;
    let status = checkRequest?.status;
    let color = 'gray';

    switch (status) {
      case 'expired':
        color = 'slategray'
        break;
      case 'approved':
        color = 'green'
        break;
      case 'declined':
        color = 'crimson'
        break;
      case 'pending':
        color = 'darkcyan'
        break;
      case 'needs_authentication':
        color = 'orange'
        break;

      default:
        break;
    }

    return (
      <View key={checkRequest?.id}
        style={{
          flex: 1,
          flexDirection: 'row',
          marginHorizontal: 10,
          marginBottom: 10,
          paddingBottom: 5,
          padding: 5,
          borderRadius: 5,
          borderWidth: 1,
          borderBottomWidth: 2,
          borderColor: color,
          backgroundColor: '#E4F0F6',
        }}>
        <View style={{ justifyContent: 'flex-start', }}>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
              {"Check ID: "}
              <Text style={{ textTransform: 'uppercase', fontWeight: 'normal' }}>
                {checkRequest?.check_id}
              </Text>
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14, color: '#000', fontWeight: 'bold' }}>
              {"Status: "}
              <Text style={{ textTransform: 'capitalize', color: color, fontWeight: 'bold' }}>
                {status === 'needs_authentication' ? 'Needs Authentication' : status}
              </Text>
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
              {"Mode: "}
              <Text style={{ fontWeight: 'normal' }}>
                {checkRequest?.mo_transaction}
              </Text>
            </Text>
          </View>
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
              {"Amount: "}
              <Text style={{ fontWeight: 'normal' }}>
                {checkRequest?.amount}
              </Text>
            </Text>
          </View>
          {/* <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
              {"Address: "}
              <Text style={{ fontWeight: 'normal' }}>
                {checkRequest?.address}
              </Text>
            </Text>
          </View> */}
          <View>
            <Text style={{ fontSize: 14, fontWeight: 'bold' }}>
              {"Expiration: "}
              <Text style={{ fontWeight: 'normal' }}>
                {new moment(checkRequest?.authentication_expiration, 'YYYY-MM-DD hh:mm:ss').format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
            </Text>
          </View>
          <View style={{ alignItems: 'baseline' }}>
            <TouchableOpacity style={{
              // justifyContent: 'space-around',
              // alignSelf: 'center',
              backgroundColor: 'gray',
              borderRadius: 5,
              marginTop: 3,
              paddingVertical: 2,
              paddingHorizontal: 10,
            }} onPress={async () => {
              setLoading(true);
              navigation.navigate("CheckRequestDetails", { checkRequest })
              setLoading(false);
            }}>
              <Text style={{
                alignSelf: 'center',
                fontSize: 14,
                color: '#ffffff',
              }}>
                View Request
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

  const renderEmptyNotifications = () => {
    return (
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: height / 2
            // marginTop: width / 1.5,
          }}>
          <View style={{ paddingVertical: 5 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: '#231f20' }}>
              No Requests yet
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <Container style={{
      backgroundColor: '#EDEDEB',
    }}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}>
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Thumbnail
              source={Assets.accountSettings.iconBack}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
          <Text style={{
            // fontFamily: 'Roboto',
            fontSize: 20,
            color: '#231f20',
          }}>Check Requests</Text>
        </Body>
        <Right style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
          >
            <Thumbnail
              source={Assets.specificProduct.iconChat}
              square
              style={{
                width: 22,
                height: 22,
                resizeMode: 'contain',
                tintColor: '#00a14b',

              }}
            />
          </TouchableOpacity>
        </Right>
      </Header>
      {/* <Body> */}
      <Container style={{ backgroundColor: '#ccc' }}>
        <View style={{ paddingVertical: 15, backgroundColor: 'white' }}>
          <View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text>Total Income</Text>
              </View>
              <View style={{ flex: 1, paddingTop: 3, alignItems: 'flex-end' }}>
                <TouchableOpacity onPress={() => { alertProductSales() }}>
                  <Icon.EvilIcons name="question" size={20} color="#66b545" />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
              <Text style={{ color: 'orange', fontSize: 24, fontWeight: '300' }}>₱ {Number(income).toLocaleString(navigator.language, { minimumFractionDigits: 2 })}</Text>
            </View>
          </View>

          <View
            style={{
              borderBottomColor: '#ccc',
              borderBottomWidth: 1,
              marginHorizontal: 15,
              paddingVertical: 5,
              marginBottom: 10,
            }}
          />

          <View style={{ flexDirection: 'row', }}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <TouchableOpacity style={{
                // justifyContent: 'space-around',
                // alignSelf: 'center',
                backgroundColor: 'green',
                borderRadius: 5,
                marginTop: 3,
                paddingVertical: 5,
                marginHorizontal: 17,
                paddingHorizontal: 10,
              }} onPress={async () => {
                console.log("loading", loading)
                if (!loading) {
                  setLoading(true);
                  handleWithdrawRequest();
                }
              }}>
                <Text style={{
                  alignSelf: 'center',
                  fontSize: 18,
                  color: '#ffffff',
                }}>
                  Request Withdrawal
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={{
              borderBottomColor: '#ccc',
              borderBottomWidth: 1,
              marginHorizontal: 15,
              paddingVertical: 5,
              marginBottom: 10,
            }}
          />

          <View style={{ paddingVertical: 15, backgroundColor: 'white' }}>
            <View style={{ marginBottom: 10 }}>
              <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
                <View style={{}}>
                  <FastImage
                    source={Assets.stores.ledgerIcon}
                    style={{ width: 20, height: 20, marginRight: 10 }}
                  />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                  <Text style={{ fontSize: 16, color: '#7a7a7a' }}>Check Requests</Text>
                </View>
              </View>
            </View>

            {!loading ?
              <ScrollView
                // contentContainerStyle={{}}
                nestedScrollEnabled={true}
                style={{ height: height / 1.5 }}
                refreshControl={
                  <RefreshControl
                    refreshing={loading}
                    onRefresh={() =>
                      init().then(() => setLoading(false))
                    }
                  />
                }>
                <FlatList
                  nestedScrollEnabled
                  data={checkRequests}
                  renderItem={renderCheckRequests}
                  //   // console.log("item", item);
                  //   return item.id;
                  // }}
                  // listKey={(index) => `notification${index}-${Date.now()}`}
                  ListEmptyComponent={renderEmptyNotifications}
                // extraData={notifications}
                />
              </ScrollView >
              : <Spinner />}
          </View>
        </View>

      </Container>
      {/* </Body> */}


    </Container >
  );
}