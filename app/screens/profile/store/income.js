
import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';

import {
  Header,
  Left,
  Body,
  Right,
  Thumbnail,
  Container,
} from 'native-base';
import Assets from '../../../components/assets.manager'
import Icon from '../../../components/icons';
import FastImage from 'react-native-fast-image';

const width = Dimensions.get('window').width;

export default class Income extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: []
    }
  }

  alertProductSales = () => {
    Alert.alert(
      "Product Sales",
      "The total amount of sales you acquired.",
      [
        {
          text: 'Close',
          style: 'cancel'
        }
      ],
      { cancelable: true }
    )
  }

  render() {
    return (
      <Container style={{
        backgroundColor: '#EDEDEB',
      }}>
        <Header
          transparent
          style={{
            backgroundColor: '#ffffff',
            paddingLeft: 10,
            paddingRight: 10,
            // marginBottom: -5,
          }}>
          <Left style={{ flex: 1, marginHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.goBack()}
            >
              <Thumbnail
                source={Assets.accountSettings.iconBack}
                square
                style={{
                  resizeMode: 'contain',
                  width: 20,
                  height: 20,
                }}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
            <Text style={{
              // fontFamily: 'Roboto',
              fontSize: 20,
              color: '#231f20',
            }}>Income</Text>
          </Body>
          <Right style={{ flex: 1, marginHorizontal: 10 }}>
            <TouchableOpacity
              onPress={() => Alert.alert('Coming Soon!', 'This feature will be available soon.')}
            >
              <Thumbnail
                source={Assets.specificProduct.iconChat}
                square
                style={{
                  width: 22,
                  height: 22,
                  resizeMode: 'contain',
                  tintColor: '#00a14b',

                }}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <ScrollView>
          <Container style={{ backgroundColor: '#ddd' }}>
            <View style={{ paddingVertical: 15, backgroundColor: 'white' }}>
              <View style={{ paddingTop: 0 }}>
                <View style={{ paddingHorizontal: 20, flexDirection: 'row' }}>
                  <View style={{}}>
                    <FastImage
                      source={Assets.stores.ledgerIcon}
                      style={{ width: 20, height: 20, marginRight: 10 }}
                    />
                  </View>
                  <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ color: '#7a7a7a' }}>Ledger</Text>
                  </View>
                </View>
              </View>
              <View
                style={{
                  borderBottomColor: '#ccc',
                  borderBottomWidth: 1,
                  marginHorizontal: 15,
                  paddingTop: 10,
                }}
              />
              <View>
                <View style={{ paddingHorizontal: 20, paddingTop: 10, flexDirection: 'row' }}>
                  <View style={{}}>
                    <FastImage
                      source={Assets.stores.earningIcon}
                      style={{ width: 20, height: 20, marginRight: 10 }}
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <Text style={{ color: '#7a7a7a' }}>Overal Earning</Text>
                  </View>
                </View>
              </View>
            </View>
          </Container>
        </ScrollView>
      </Container>
    );
  }
}