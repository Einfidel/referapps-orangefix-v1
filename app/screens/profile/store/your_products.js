import React from 'react';
import { useState, useEffect } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View, Text, TouchableOpacity, Dimensions, StyleSheet, Modal, Platform } from 'react-native';
import { Container, Header, Thumbnail, Content, Left, Right, Body, Title } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import * as Animatable from 'react-native-animatable';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../../components/api/service';
import { Alert } from 'react-native';
import FastImage from 'react-native-fast-image';
const { width, height } = Dimensions.get('window');

export default function YourProducts({ navigation, route }) {
  const [index, setIndex] = useState(0);
  const [loading, setLoading] = useState(true);
  const [latestProducts, setLatestProducts] = useState([]);
  const [popularProducts, setPopularProducts] = useState([]);
  const [user, setUser] = useState(null);
  const [routes, setRoutes] = useState([
    { key: 'recent', title: 'Recent' },
    { key: 'popular', title: 'Popular' },
    { key: 'soldout', title: 'Sold Out' },
    { key: 'stock', title: 'Stock' },
  ]);

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      setLoading(true);

      async function reload() {
        handleProducts();
      };

      reload();

      return () => {
        isActive = false;
      };
    }, [])
  );

  useEffect(() => {
    handleProducts();
  }, [])

  const handleProducts = async () => {
    const _user = await AsyncStorage.getItem('user_data');
    let user = _user ? JSON.parse(_user) : null;
    if (user) {
      let data = {}
      data.user_id = user.data.id
      data.viewType = 'profile'
      data.sort_by = 'latest'

      await Service.getAccountProducts(
        data,
        async (res) => {
          console.log('latest');
          setLatestProducts(res)
          setUser(user)

        },
        (err) => console.log('err', err)
      );

      data.sort_by = 'popularity'
      await Service.getAccountProducts(
        data,
        (res2) => {
          console.log('popularity');
          setPopularProducts(res2)
          setLoading(false)
        },
        (err) => console.log('err', err)
      );
    }
  };

  const _renderLabel = (props) => ({ route, focused, index }) => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    const color = focused ? '#f36e23' : '#7f8c8d';

    return <Text style={[{ fontSize: 12 }, { color }]}>{route.title}</Text>;
  };

  const handleDelete = async (product) => {
    Alert.alert(
      'Warning!',
      "Are you sure you want to delete " + product.name + "?",
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: async () => {
            setLoading(true)
            let _user = await AsyncStorage.getItem('user_data');
            let user = _user ? JSON.parse(_user) : null;
            if (user) {
              await Service.deleteProduct(
                user.id,
                product.id,
                (res) => {
                  Alert.alert('Success', 'Deleted successfully');
                  handleProducts();
                },
                (err) => console.log(err)
              );
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  const renderScene = ({ route }) => {
    switch (route.key) {
      case 'recent':
        return Recent(); // passing data as data prop
      case 'popular':
        return Popular();
      case 'soldout':
        return Soldout();
      case 'stock':
        return Stock();
      default:
        return null;
    }
  };

  const Popular = () => {
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    latestProducts.map((product, index) => {
      let inventory = product.info.data.inventory;


      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              highestStockVariant = variant.stock
            }
          }
        })
      }
    })

    return popularProducts.map((product, index) => {
      let inventory = product.info.data.inventory;
      // console.log(product.info.data);
      return (
        <View key={product.id} style={{ backgroundColor: '#ffffff' }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 25,
            }}
          >
            <FastImage
              source={
                product.info.data.image && product.info.data.image.directory ? (
                  { uri: product.info.data.image.full_path }
                ) : (
                  Assets.specificProduct.icon1
                )
              }
              style={{
                width: 100,
                height: 100,
                resizeMode: 'contain',
              }}
            />
            <View style={{ marginHorizontal: 20, width: width - 200 }}>
              <Text>{product.product_name}</Text>
              <Text>{`\u20B1 ` + product.price}</Text>
              <Text>Stocks : {inventory.stock != null ?
                inventory.stock
                :
                lowestStockVariant === highestStockVariant ?
                  lowestStockVariant
                  :
                  inventory[0].stock != null ?
                    lowestStockVariant + " - " + highestStockVariant
                    :
                    'N/A'}</Text>
              <Text>Ratings: {product.ratings || 'N/A'}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 55 }}>
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('EditProduct', { item: item, onBackPress: handleServices })
              }
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>EDIT</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleDelete(product)}
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>DELETE</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  const Soldout = () => {
    let stock = 0;
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    latestProducts.map((product, index) => {
      let inventory = product.info.data.inventory;

      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            stock = variant.stock;
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              stock = variant.stock;
              highestStockVariant = variant.stock
            }
          }
        })
      } else {
        stock = inventory.stock;
      }
    })

    return latestProducts.map((product, index) => {
      if (stock === 0) {
        let inventory = product.info.data.inventory;
        // console.log(product.info.data);
        return (
          <View key={product.id} style={{ backgroundColor: '#ffffff' }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 25,
              }}
            >
              <FastImage
                source={
                  product.info.data.image && product.info.data.image.directory ? (
                    { uri: product.info.data.image.full_path }
                  ) : (
                    Assets.specificProduct.icon1
                  )
                }
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <View style={{ marginHorizontal: 20, width: width - 200 }}>
                <Text>{product.product_name}</Text>
                <Text>{`\u20B1 ` + product.price}</Text>
                <Text>Stocks : {inventory.stock != null ?
                  inventory.stock
                  :
                  lowestStockVariant === highestStockVariant ?
                    lowestStockVariant
                    :
                    inventory[0].stock != null ?
                      lowestStockVariant + " - " + highestStockVariant
                      :
                      'N/A'}</Text>
                <Text>Ratings: {product.ratings || 'N/A'}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', height: 55 }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('EditProduct', product)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>EDIT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => handleDelete(product)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    });
  };

  const Stock = () => {
    let stock = 0;
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    latestProducts.map((product, index) => {
      let inventory = product.info.data.inventory;

      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            stock = variant.stock;
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              stock = variant.stock;
              highestStockVariant = variant.stock
            }
          }
        })
      } else {
        stock = inventory.stock;
      }
    })

    return latestProducts.map((product, index) => {
      if (stock != 0) {
        let inventory = product.info.data.inventory;
        // console.log(product.info.data);
        return (
          <View key={product.id} style={{ backgroundColor: '#ffffff' }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'center',
                paddingVertical: 25,
              }}
            >
              <FastImage
                source={
                  product.info.data.image && product.info.data.image.directory ? (
                    { uri: product.info.data.image.full_path }
                  ) : (
                    Assets.specificProduct.icon1
                  )
                }
                style={{
                  width: 100,
                  height: 100,
                  resizeMode: 'contain',
                }}
              />
              <View style={{ marginHorizontal: 20, width: width - 200 }}>
                <Text>{product.product_name}</Text>
                <Text>{`\u20B1 ` + product.price}</Text>
                <Text>Stocks : {inventory.stock != null ?
                  inventory.stock
                  :
                  lowestStockVariant === highestStockVariant ?
                    lowestStockVariant
                    :
                    inventory[0].stock != null ?
                      lowestStockVariant + " - " + highestStockVariant
                      :
                      'N/A'}</Text>
                <Text>Ratings: {product.ratings || 'N/A'}</Text>
              </View>
            </View>
            <View style={{ flexDirection: 'row', height: 55 }}>
              <TouchableOpacity
                onPress={() => navigation.navigate('EditProduct', product)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>EDIT</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => handleDelete(product)}
                style={{
                  borderColor: '#EDEDEB',
                  width: '50%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderTopWidth: 1.5,
                  borderBottomWidth: 1.5,
                  borderLeftWidth: 1.5,
                  borderRightWidth: 0.5,
                }}
              >
                <Text style={{ fontSize: 15 }}>DELETE</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    });
  };

  const Recent = () => {
    let lowestStockVariant = 0;
    let highestStockVariant = 0;

    latestProducts.map((product, index) => {
      let inventory = product.info.data.inventory;


      if (inventory.stock == null) {
        inventory.map((variant, index) => {
          if (lowestStockVariant === 0) {
            lowestStockVariant = variant.stock
          } else {
            if (lowestStockVariant > variant.stock) {
              lowestStockVariant = variant.stock
            }
          }

          if (highestStockVariant === 0) {
            highestStockVariant = variant.stock
          } else {
            if (highestStockVariant < variant.stock) {
              highestStockVariant = variant.stock
            }
          }
        })
      }
    })

    return latestProducts.map((product, index) => {
      let inventory = product.info.data.inventory;
      console.log(product.info.data);
      return (
        <View key={product.id} style={{ backgroundColor: '#ffffff' }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              paddingVertical: 25,
            }}
          >
            <FastImage
              source={
                product.info.data.image && product.info.data.image.directory ? (
                  { uri: product.info.data.image.full_path }
                ) : (
                  Assets.specificProduct.icon1
                )
              }
              style={{
                width: 100,
                height: 100,
                resizeMode: 'contain',
              }}
            />
            <View style={{ marginHorizontal: 20, width: width - 200 }}>
              <Text>{product.product_name}</Text>
              <Text>{`\u20B1 ` + product.price}</Text>
              <Text>Stocks : {inventory.stock != null ?
                inventory.stock
                :
                lowestStockVariant === highestStockVariant ?
                  lowestStockVariant
                  :
                  inventory[0].stock != null ?
                    lowestStockVariant + " - " + highestStockVariant
                    :
                    'N/A'}</Text>
              <Text>Ratings: {product.ratings || 'N/A'}</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row', height: 55 }}>
            <TouchableOpacity
              onPress={() => navigation.navigate('EditProduct', product)}
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>EDIT</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleDelete(product)}
              style={{
                borderColor: '#EDEDEB',
                width: '50%',
                justifyContent: 'center',
                alignItems: 'center',
                borderTopWidth: 1.5,
                borderBottomWidth: 1.5,
                borderLeftWidth: 1.5,
                borderRightWidth: 0.5,
              }}
            >
              <Text style={{ fontSize: 15 }}>DELETE</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    });
  };

  const handleLoading = () => {
    return (
      <Modal
        animationType='slide'
        transparent
        visible={loading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            height: hp('80%'),
          }}
        >
          <View
            style={{
              backgroundColor: '#231f20',
              alignItems: 'center',
              justifyContent: 'center',
              padding: 20,
              width: 170,
              height: 170,
              borderRadius: 30,
            }}
          >
            <MaterialIndicator color={'#ffffff'} size={50} />
            <View
              style={{
                paddingTop: 10,
              }}
            />
            <Animatable.View
              animation='rubberBand'
              easing='ease-out'
              iterationCount='infinite'
              duration={3000}
              useNativeDriver
            >
              <Text
                style={{
                  fontSize: 12,
                  color: '#ffffff',
                  textAlign: 'center',
                }}
              >
                Loading...
              </Text>
            </Animatable.View>
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <Container>
      <Header style={[{ backgroundColor: '#ffffff' }, styles.elevateHeader]}>
        <Left style={styles.left}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <FastImage source={Assets.accountSettings.arrowDarkIcon} style={styles.backIcon} />
          </TouchableOpacity>
          <View style={styles.addVariation}>
            <Text style={styles.headerText}>Products</Text>
          </View>
        </Left>
        <Right>
          <TouchableOpacity onPress={() => alert('Under Development')}>
            <FastImage source={Assets.specificProduct.iconChat} square style={styles.chatIcon} />
          </TouchableOpacity>
        </Right>
      </Header>
      <Content>
        <TabView
          navigationState={{ index, routes }}
          renderScene={renderScene}
          renderTabBar={(props) => (
            <TabBar
              {...props}
              labelStyle={{
                // fontFamily: 'regular',
                fontSize: 11,
                textTransform: 'capitalize',
                color: '#f36e23',
              }}
              indicatorStyle={{
                backgroundColor: '#f36e23',
                borderBottomWidth: 3,
                borderBottomColor: '#f36e23',
              }}
              indicatorContainerStyle={{
                borderBottomWidth: 1,
                borderBottomColor: '#EDEDEB',
              }}
              style={{ backgroundColor: '#ffffff', elevation: 0 }}
              renderLabel={_renderLabel(props)}
            />
          )}
          onIndexChange={(newIndex) => setIndex(newIndex)}
          initialLayout={{ width: Dimensions.get('window').width }}
        />
      </Content>
      {handleLoading()}
    </Container>
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily: 'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  elevateHeader: {
    ...Platform.select({
      ios: {
        justifyContent: 'center',
        marginTop: 0
      },
      android: {
        justifyContent: 'center',
        marginTop: 30
      }
    })
  }
});
