import * as React from 'react';
import { useState, useEffect } from 'react';
import { View, Text, ImageBackground, Image } from 'react-native';

let main = require('../../assets/SplashMain.gif');
let bg = require('../../assets/splashBG.png');
let first = require('../../assets/Logo-Animation.gif');

export default function Splash() {
  const [loading, setloading] = useState(false);

  const init = () => {
    if (loading) {
      setTimeout(() => setloading(false), 2200);
    }
  };

  useEffect(() => {
    init();
  }, []);

  return (
    <View style={{ flex: 1 }}>
      <ImageBackground
        source={bg}
        style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}
      >
        {!loading ? (
          <Image source={first} style={{ width: 150, height: 150 }} />
        ) : (
          <Image source={main} style={{ width: 150, height: 150 }} />
        )}
      </ImageBackground>
    </View>
  );
}
