import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
  Dimensions,
} from 'react-native';
import {
  Header,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Input,
  Item,
  Icon,
} from 'native-base';
import Fire from '../../../FireChat';
import Assets from '../../components/assets.manager.js';
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';
import styles from './styles';

const {width, height} = Dimensions.get('window');

export default function ChatScreen(props) {
  const [loading, setloading] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [conversations, setconversations] = useState([]);

  const init = async () => {
    setloading(true);
    await Fire.shared.getConversations("User", (res) => {
      setloading(false);
      setconversations(res);
    });
  };

  useEffect(() => {
    isLoaded && getNewMessages();
  }, [isLoaded]);

  const getNewMessages = () => {
    Fire.shared.onNewConversationMsg((res) => {
      console.log('Checking new messages...', JSON.stringify(res), 'Convo list', JSON.stringify(conversations));
      let conversationCopy = conversations;
      let convoId = conversationCopy.findIndex(
        (i) => i.data.id === res.data.id,
      );
      console.log('Convo id', convoId);
      conversationCopy[convoId] = res;
      console.warn('New convo', JSON.stringify(conversationCopy, null, '\t'));
      setconversations(conversationCopy);
    });
  };

  useEffect(() => {
    init().then(() => setIsLoaded(true));
  }, []);

  const renderContact = ({item, index}) => {
    let {data, isRead, id, lastUpdate} = item;
    let date = moment(lastUpdate).fromNow();
    return (
      <ListItem
        noIndent
        style={{backgroundColor: isRead !== undefined && !isRead  ? '#d5f0d5' : null, paddingBottom: 10}}
        avatar
        onPress={() => {
          Fire.shared.readAll(id);
          props.navigation.navigate('Conversation', {
            reciever: data,
            conversationID: item.id,
            refresh: init,
          });
        }}>
        <Left noIndent>
          <Thumbnail
            source={{
              uri:
                data.avatar == ''
                  ? 'https://i.pinimg.com/originals/82/d8/6a/82d86ac15e271d9d1fea30436bfc9bfd.jpg'
                  : data.avatar,
            }}
          />
        </Left>
        <Body style={{flexDirection: 'row', alignItems: 'center'}}>
          <View style={{width: width - 200}}>
            <Text style={styles.txtName}>{data.name || 'Name'}</Text>
            <Text
              numberOfLines={1}
              note
              style={{
                fontWeight: isRead !== undefined && !isRead ? 'bold' : 'normal',
              }}>
              {item.lastMessage || 'loading...'}
            </Text>
          </View>
          <Text style={{fontSize: 11, width: 100, textAlign: 'right'}} note>
            {date}
          </Text>
        </Body>
      </ListItem>
    );
  };

  const noMessages = () => {
    return (
      <View style={{flex: 1, height: height - 170, justifyContent: 'center', alignItems: 'center'}}>
        <Text>No messages</Text>
      </View>
    );
  };

  return (
    <View style={styles.con}>
      <Header style={{backgroundColor: '#FFF'}}>
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={styles.arrowCon}>
          <Image
            source={Assets.accountSettings.arrowDarkIcon}
            style={styles.imgBack}
          />
        </TouchableOpacity>
        <Text style={styles.txtMessages}>Messages</Text>
        <TouchableOpacity
          onPress={() => alert('Coming Soon')}
          style={styles.searchCon}></TouchableOpacity>
      </Header>
      <View style={{padding: 10}}>
        <Item rounded>
          <Icon active name="search" />
          <Input style={{fontSize: 14}} placeholder="Search" />
        </Item>
      </View>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={loading} onRefresh={init} />
        }>
        <FlatList
          // inverted
          // data={conversations}
          data={conversations.sort((a, b) => a.lastUpdate < b.lastUpdate)}
          renderItem={renderContact}
          listKey={(index) => `conversation${index}-${Date.now()}`}
          ListEmptyComponent={noMessages}
          extraData={conversations}
        />
      </ScrollView>
    </View>
  );
}
