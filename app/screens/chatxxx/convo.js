import React, {useState, useCallback, useEffect} from 'react';
import {View, Text, TouchableOpacity, Image, BackHandler} from 'react-native';
import {Header, Thumbnail, Icon} from 'native-base';
import Assets from '../../components/assets.manager.js';
import {GiftedChat, Send} from 'react-native-gifted-chat';
import Notifications from '../../components/Notifications';
import Fire from '../../../FireChat';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';

export default function Conversation(props) {
  const [messages, setMessages] = useState([]);
  const [has_thread, setIfHasThread] = useState(
    props.route.params.conversationID ? true : false,
  );
  const [convo_id, setconversationID] = useState(
    props.route.params.conversationID || '',
  );
  const [userId, setUserId] = useState(null);
  const [userName, setUserName] = useState('');
  const [userAvatar, setUserAvatar] = useState('');
  // useEffect(() => {
  //   setMessages([
  //     {
  //       _id: 1,
  //       text: 'Hello developer',
  //       createdAt: new Date(),
  //       user: {
  //         _id: 2,
  //         name: 'React Native',
  //         avatar: 'https://placeimg.com/140/140/any',
  //       },
  //     },
  //   ])
  // }, [])

  useEffect(() => {
    getUserId();
    console.log('UID', Fire.shared.uid);
    console.log('RECIEVER', props.route.params.reciever);
    getConvos();
  }, []);

  const getUserId = async () => {
    let data = await AsyncStorage.getItem('user_data');
    let userData = data ? JSON.parse(data) : null;
    setUserId(userData.data.id);
    setUserName(userData.data.fname + ' ' + userData.data.lname);
    setUserAvatar(userData.data.image);
  };

  const getConvos = async () => {
    await Fire.shared.getConversation(
      props.route.params.reciever,
      (id, data) => {
        setIfHasThread(true);
        setconversationID(id);
      },
    );
    // console.log("Has Thread", has_thread, convo_id)
  };

  const onSend = async (messages) => {
    if (props.route.params) {
      await Notifications.getReceiverToken(
        props.route.params.reciever.id,
        async (receiverToken) => {
          await Notifications.sendNotification(
            userId,
            `You have received a new message from ${userName}.`,
            receiverToken,
            'chat',
          );
          console.warn('Receiver token', receiverToken);
        },
      );
    }
    if (!has_thread) {
      //CREATE CONVO
      await Fire.shared.addConversation(
        messages,
        props.route.params ? props.route.params.reciever : {},
        (id, data) => {
          setIfHasThread(true);
          setconversationID(id);
        },
      );
    } else {
      Fire.shared.send(messages, convo_id);
    }
  } 

  useEffect(() => {
    // console.warn('desc', props.navigation.goBack())
    if (convo_id) {
      console.log('listening');
      Fire.shared.on(convo_id, (message) => {
        setMessages((previousMessages) =>
          GiftedChat.append(previousMessages, message),
        );
      });
    }
    // return Fire.shared.off()
  }, [convo_id]);

  const handleBack = () => {
    props.navigation.goBack();
    try {
      let {refresh} = props.route.params;
      refresh !== undefined && refresh();
    } catch (error) {
      console.warn('Error', error.message);
    }
  };

  useEffect(() => {
    const backAction = () => {
      handleBack();
      return true;
    };
    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  return (
    <View style={styles.con}>
      <Header style={{backgroundColor: '#FFF'}}>
        <TouchableOpacity
          onPress={handleBack}
          style={[styles.arrowCon, {zIndex: 1}]}>
          <Image
            source={Assets.accountSettings.arrowDarkIcon}
            style={styles.imgBack}
          />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Thumbnail small source={{uri: props.route.params.reciever.avatar}} />
          <Text style={{...styles.txtMessages, marginHorizontal: 5}}>
            {props.route.params.reciever.name || 'Messages'}
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => alert('Coming Soon')}
          style={styles.searchCon}>
          {/* <Image source={{uri: props.route.params.reciever.avatar} || Assets.landing.icon_search} style={styles.imgSearch}/> */}
          {/* <Thumbnail small source={{uri: props.route.params.reciever.avatar}} /> */}
        </TouchableOpacity>
      </Header>
      <View style={{flex: 1}}>
        <GiftedChat
          messages={messages}
          onSend={onSend}
          placeholder={'Type a message'}
          user={{
            _id: userId,
            name: userName,
            avatar: userAvatar == '' ? null : userAvatar,
          }}
          renderSend={(props) => (
            <Send
              {...props}
              containerStyle={{
                height: 60,
                width: 60,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon active name="send" />
            </Send>
          )}
          renderChatEmpty={() => (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                transform: [{scaleY: -1}],
              }}>
              <Text>Start a conversation</Text>
            </View>
          )}
        />
      </View>
    </View>
  );
}
