import React from 'react';
import { useState, useEffect } from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import styles from '../styles/Profile/profileStyles';
import AsyncStorage from '@react-native-community/async-storage';
import Unauthorized from '../../components/Unauthenticated';
import ProductScreen from './app_product';

export default function Profile({ navigation, route }) {
  const [auth, setauth] = useState(null)
  const [loaded, setloaded] = useState(false)

  const init = async () => {
    let token = await AsyncStorage.getItem('auth');
    if (token) setauth(token)
    setloaded(true)
  }

  useEffect(() => {
    init()
  }, [])

  console.log("AUTH", auth)

  if (!auth && loaded) {
    return <Unauthorized navigation={navigation} />
  }

  if (auth) {
    return <ProductScreen navigation={navigation} />
  }

  return (
    <View></View>
  )
}