import React from 'react';
import { Text, TouchableOpacity, View, Dimensions, StyleSheet, Image, TextInput } from 'react-native';
import { Header, Left, Container, } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FastImage from 'react-native-fast-image'

const { width, height } = Dimensions.get('window');

export default class VariationsScreen extends React.Component {
  state = {
    isClickedMenu: false,
    modalVisible: false,
  };

  isClickedMenu = () => {
    this.setState({ isClickedMenu: !this.state.isClickedMenu });
  }
  render() {
    let { isClickedMenu } = this.state;
    return (
      <Container style={styles.container}>
        <Header style={styles.header}>
          <Left style={styles.left}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <FastImage source={require('../../../assets/images/headericon/left-arrow-dark.png')} style={styles.imgback} resizeMode={FastImage.resizeMode.contain} />
            </TouchableOpacity>
            <View style={styles.addVariation}>
              <Text style={styles.txtAddVariation}> Add Variations </Text>
            </View>
          </Left>
        </Header>
        <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>
          <View style={styles.Con}>
            <View style={styles.addOptCon}>
              <TextInput
                placeholder={'Enter Variation Name, eg: colour, size etc.'}
                placeholderTextColor={'#f36e23'}
                style={styles.txtInput}
              />
              <TextInput
                placeholder={'Enter Variation Name, eg: colour, size etc.'}
                placeholderTextColor={'#231f20'}
                style={styles.txtInput2}
              />
              <TouchableOpacity style={styles.btnAddOpt} onPress={() => alert('Coming Soon')}>
                <Text style={styles.txtAddOpt}>Add Options</Text>
              </TouchableOpacity>

              {isClickedMenu == false ?
                <TouchableOpacity
                  onPress={() => this.isClickedMenu()}
                  style={styles.popCon}>
                  <Text style={styles.txtAddVar}>Add Variation</Text>
                </TouchableOpacity>
                :
                <View style={styles.conAddVar}>
                  <TouchableOpacity onPress={() => this.isClickedMenu()} style={styles.minusCon}>
                    <FastImage source={require('../../../assets/images/icon_minus.png')} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                  </TouchableOpacity>
                  <TextInput
                    autoFocus={true}
                    placeholder={'Enter Variation Name, eg: colour, size etc.'}
                    placeholderTextColor={'#f36e23'}
                    style={styles.txtInput}
                  />
                  <TextInput
                    placeholder={'Enter Variation Name, eg: colour, size etc.'}
                    placeholderTextColor={'#231f20'}
                    style={styles.txtInput2}
                  />
                  <TouchableOpacity style={styles.btnAddOpt} onPress={() => alert('Coming Soon')}>
                    <Text style={styles.txtAddOpt}>Add Options</Text>
                  </TouchableOpacity>
                </View>
              }
            </View>
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.btnStockCon}>
          <TouchableOpacity
            style={styles.btnSetCon}
            onPress={() => this.props.navigation.navigate('Stock and Price')}
          >
            <Text style={styles.txtStock}>Set Stock and Price</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    width: width,
  },
  header: {
    backgroundColor: '#ffffff',
    justifyContent: 'center'
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  },
  imgback: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  addVariation: {
    marginLeft: 25
  },
  txtAddVariation: {
    fontFamily: 'roboto.medium',
    fontSize: 18
  },
  Con: {
    backgroundColor: '#ecf0f1',
    flex: 1
  },
  addOptCon: {
    backgroundColor: '#FFF',
    marginTop: 15,
    paddingVertical: 20,
  },
  txtInput: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#f36e23',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.15,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center'
  },
  txtInput2: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: width / 1.15,
    height: 40,
    borderWidth: 1.5,
    borderColor: '#bfbfbf',
    alignSelf: 'center',
    marginTop: 10
  },
  btnAddOpt: {
    backgroundColor: '#24ae5f',
    width: width / 1.15,
    height: 31,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around', marginTop: 10
  },
  txtAddOpt: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  popCon: {
    width: width / 1.15,
    height: 31,
    borderRadius: 8,
    alignSelf: 'center',
    justifyContent: 'space-around', marginTop: 20,
    borderWidth: 1.5,
    borderColor: '#f36e23',
    borderStyle: 'dashed'
  },
  txtAddVar: {
    fontFamily: 'regular',
    fontSize: 14,
    color: '#f36e23',
    alignSelf: 'center',
  },
  conAddVar: {
    backgroundColor: '#FFF',
    width,
    paddingHorizontal: 30,
    paddingTop: 10
  },
  minusCon: {
    alignSelf: 'flex-end',
    marginVertical: 10
  },
  imgMinus: {
    tintColor: '#e04f5f',
    resizeMode: 'contain',
    height: 15,
    width: 15
  },
  btnStockCon: {
    height: 50,
    backgroundColor: '#ecf0f1'
  },
  btnSetCon: {
    position: 'absolute',
    bottom: 0,
    backgroundColor: '#24ae5f',
    width: width / 1.15,
    height: 31,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
    margin: 10
  },
  txtStock: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
})
