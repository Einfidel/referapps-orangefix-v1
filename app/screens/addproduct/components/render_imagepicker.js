
import React, { PureComponent } from 'react';

import {
  TouchableOpacity,
} from 'react-native';

import {
  Thumbnail,
} from 'native-base';

class RenderImagePicker extends React.PureComponent {

  render() {
    const {
      onPress,
      onLongPress,
      style,
      key,
      uri,
      imageStyle,
    } = this.props;
    // console.warn(key);
    return (
      <TouchableOpacity
        onPress={onPress}
        onLongPress={onLongPress}
        style={style}
        key={key}
      >
        <Thumbnail
          square
          source={{ uri: uri }}
          style={imageStyle}
          resizeMode='contain'
          defaultSource={require('../../../assets/images/internal/Sell_Product-09.png')}
        />
      </TouchableOpacity>
    );
  }
}
export default RenderImagePicker;
