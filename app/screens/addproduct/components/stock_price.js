import React from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Dimensions,
  StyleSheet,
} from 'react-native';

import {
  Container,
  Header,
  Left,
  Body,
  Right,
} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import FastImage from 'react-native-fast-image';

// import sell from './../../styles/ProductScreenStyles/sellproductstyles';

const { width, height } = Dimensions.get('window');

class StockandPrice extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stock: '',
      price: '',
    }
  }

  _renderVariations = () => {
    return (
      <View>
        <View style={sell.space} />
        <View style={sell.space} />
        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
          <View style={sell.variationRow}>
            <Text style={[sell.variationOption, { fontSize: 11, }]}>Rosa Gold</Text>
          </View>
          <View style={sell.variationRow}>
            <View style={sell.optionInputContainer}>
              <TextInput
                style={sell.StockPriceText}
                placeholder={'Stock'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'numeric'}
              />
            </View>
          </View>
          <View style={sell.variationRow}>
            <View style={sell.optionInputContainer}>
              <TextInput
                style={sell.StockPriceText}
                placeholder={'Price'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'numeric'}
              />
            </View>
          </View>
        </View>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <Left style={{ flex: 0 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <FastImage
                source={require('../../../assets/images/headericon/left-arrow-dark.png')}
                style={sell.backImage}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 0 }}>
            <Text style={sell.headerText}> Stock And Price </Text>
          </Body>
          <Right style={{ flex: 1 }}>
            <TouchableOpacity style={sell.rightView} />
          </Right>
        </Header>
        <KeyboardAwareScrollView style={{ backgroundColor: '#ecf0f1' }}>

          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />

          <View style={sell.stockAndprice1stView}>
            <View style={sell.stockAndpriceViewContainer}>

              <View style={sell.space} />
              <View style={sell.space} />

              <Text style={sell.stockAndPriceText}>Set variation stock and price here.</Text>

              <View style={sell.space} />
              <View style={sell.space} />
              <View style={{ borderBottomWidth: 1, borderBottomColor: '#e1e2e3' }} />
              <View style={sell.space} />

              <View style={{ flexDirection: 'row', justifyContent: 'space-around', paddingTop: 10 }}>
                <View style={sell.variationRow}>
                  <Text style={sell.variationTitle1}>Variation</Text>
                </View>
                <View style={sell.variationRow}>
                  <Text style={sell.variationTitle}>Stock</Text>
                </View>
                <View style={sell.variationRow}>
                  <Text style={sell.variationTitle}>Price</Text>
                </View>
              </View>

              <View>
                <View style={sell.space} />
                <View style={sell.space} />

                {this._renderVariations(1)}

                <View style={sell.space} />
                <View style={sell.space} />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <View style={{ paddingHorizontal: 20, backgroundColor: '#ecf0f1', paddingBottom: 5 }}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => alert('Under Development Stage')}
            style={sell.setStockAndpriceButton}
          >
            <Text style={sell.addOptionText}>Save</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }

}

export default StockandPrice;

const sell = StyleSheet.create({
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  headerText: {
    fontFamily: 'Roboto',
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 25
  },
  stockAndprice1stView: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
  },
  stockAndpriceViewContainer: {
    backgroundColor: '#ffffff',
    height: height / 5.3,
  },
  stockAndPriceText: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingLeft: 25,
  },
  space: {
    paddingTop: 5,
  },
  variationRow: {
    width: (width / 3),
    justifyContent: 'space-around'
  },
  variationTitle1: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
    fontFamily: 'italic'
  },
  variationTitle: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    // alignSelf: 'center',
    textAlign: 'left',
    fontFamily: 'italic'
  },
  variationOption: {
    // fontFamily: 'Roboto',
    // fontSize: 16,
    // color: '#231f20',
    alignSelf: 'center',
  },
  optionInputContainer: {
    width: '80%',
    height: 36,
    borderWidth: .7,
    borderColor: '#e1e2e3',
    justifyContent: 'space-around',
  },
  StockPriceText: {
    fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  variationsOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  setStockAndpriceButton: {
    backgroundColor: '#00a14b',
    width: '100%',
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  addOptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
})