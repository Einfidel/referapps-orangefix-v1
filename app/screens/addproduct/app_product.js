import React from 'react';
import { Text, TouchableOpacity, View, Dimensions, StyleSheet } from 'react-native';
import { Header, Left, Body, Right, Thumbnail, Container, Content } from 'native-base';
const { width, height } = Dimensions.get('window');
export default class AddProductScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header style={styles.header}>
          <Text style={styles.txtHeader}>Market an Item</Text>
        </Header>
        <View style={styles.imgUploadContainer}>
          <Thumbnail
            square
            source={require('../../assets/images/internal/icon-upload.png')}
            style={styles.imgUpload}
          />
          <Text style={styles.txtNote}>To provide marketing tools to companies and individuals and create a perpetual income.</Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Sell Product')}
            // this.props.navigation.navigate('Pinned')}
            style={styles.btnCon}>
            <Text style={styles.txtBtn}>Product</Text>
          </TouchableOpacity>
          <View style={{ padding: 5, }} />
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ServiceScreen')}
            style={styles.btnCon}>
            <Text style={styles.txtBtn}>Service</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#ffffff',
  },
  txtHeader: {
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    fontFamily: 'roboto-bold'
  },
  imgUploadContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    // position:'absolute',
    // backgroundColor:'pink'
  },
  imgUpload: {
    // backgroundColor:'blue',
    width: 130,
    height: 130,
  },
  txtNote: {
    fontFamily: 'Roboto',
    fontSize: 10,
    color: '#7f8c8d',
    textAlign: 'center',
    paddingBottom: 20,
    width: width / 1.4,
    paddingVertical: 25,
  },
  btnCon: {
    backgroundColor: '#f36e23',
    borderWidth: .7,
    alignSelf: 'center',
    borderRadius: 4,
    width: width / 2.3,
    paddingVertical: 3,
    borderColor: '#f36e23',
    shadowColor: '#383a3d',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
  },
  txtBtn: {
    fontSize: 13,
    color: '#ffffff',
    alignSelf: 'center',
    fontFamily: 'Roboto'
  }
})