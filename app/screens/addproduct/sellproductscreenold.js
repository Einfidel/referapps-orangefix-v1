import React from 'react';

import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Item,
  Input,
} from 'native-base';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import FastImage from 'react-native-fast-image';

import ImagePicker from 'react-native-image-picker';

import { connect } from 'react-redux';

import sell from './../styles/ProductScreenStyles/sellproductstyles';

import styles from '../../screens/styles/ProductScreenStyles/serviceScreen'

var width = Dimensions.get('window').width;

class SellProductScreenOld extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible1: false,
      modalVisible2: false,
      referrerModal: false,
      packagingModal: false,
      conditionModal: false,

      packaging_size: [
        {
          width: '',
        },
        {
          length: '',
        },
        {
          height: '',
        },
      ],

      //to send data
      name: '',
      description: '',
      price: '',
      stock: '',
      weight: '',
      to_send_packagingSize: [],
      condition: '',
      warranty: '',
      referrer_commission: '',
      lastCommission: '',
      text: '',
      value: '',
    }
  }


  // Referrer Commission Modal
  setModalVisible1(visible) {
    this.setState({ modalVisible1: visible });
  }

  renderReferrerModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal
          animationType='slide'
          transparent={true}
          visible={this.state.modalVisible1}
          style={styles.modalCon}
        >
          {this._renderReferrModal()}
        </Modal>
      </View>
    )
  }

  handleInputChange = (text) => {
    this.setState({
      referrer_commission: text.replace(/[^0-9]/g, '')
    });
  }

  _renderReferrModal = () => {
    return (
      <TouchableWithoutFeedback onPress={() => { this.setModalVisible1(!this.state.modalVisible1); }}>
        <View style={styles.containerModal}>
          <View style={sell.ReferrerModalConatiner}>
            <Text style={sell.ReferrerModalHeaderText}>Set Referrer Commission(5% - 50%)</Text>
            <Item >
              <Input
                style={sell.SetCommission}
                placeholder={'Set Commission'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                value={this.state.referrer_commission}
                onChangeText={(text) => this.handleInputChange(text)}
                maxLength={2}
              />
              <Text style={sell.percentText}>%</Text>
            </Item>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => this._renderReferrerSaveButton()}
              style={sell.referrerSaveBtn}>
              <Text style={sell.SaveText}>Save</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  _renderReferrerSaveButton = () => {
    if (this.state.referrer_commission < 5 || this.state.referrer_commission > 50) {
      return (
        alert('Invalid Percentage! (5% - 50%)')
      );
    } else {
      this.setState({
        referrerModal: false,
        modalVisible1: false,
        // lastCommission: this.state.referrer_commission,
      })
    }
  }

  //Condition Modal

  onPressButton = (value) => {
    this.setState({ value });
  }

  setConditionModal(visible) {
    this.setState({ conditionModal: visible });
  }

  conditionsModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal
          animationType='slide'
          transparent={true}
          visible={this.state.conditionModal}
          style={styles.modalCon}
        >
          <TouchableWithoutFeedback onPress={() => { this.setConditionModal(!this.state.conditionModal) }}>
            <View style={styles.modalOption}>
              <View style={styles.conditionModalContainer}>
                <Text style={styles.yes} onPress={() => { this.onPressButton('New'); this.setConditionModal(!this.state.conditionModal); }}>New</Text>
                <Text style={styles.no} onPress={() => { this.onPressButton('Used ( Like new )'); this.setConditionModal(!this.state.conditionModal); }}>Used ( Like new )</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    )
  }

  //Packaging Size Modal
  setModalVisible2(visible) {
    this.setState({ modalVisible2: visible });
  }

  renderPackageModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center' }}>
        <Modal
          animationType='slide'
          transparent={true}
          visible={this.state.modalVisible2}
          style={styles.modalCon}
        >
          {this._renderPackagingModal()}
        </Modal>
      </View>
    )
  }

  _renderPackagingModal = () => {
    return (
      <TouchableWithoutFeedback onPress={() => { this.setModalVisible2(!this.state.modalVisible2); }}>
        <View style={styles.containerModal}>
          <View style={sell.PackagingModalConatiner}>
            <Text style={sell.PackagingModalHeaderText}>Packaging Size</Text>
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Width (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 0;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[0].width}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Length (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 1;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[1].length}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.size_group_input}>
              <Input
                style={sell.group_inptut_title}
                placeholder={'Height (in cm)'}
                placeholderTextColor={'#7f8c8d'}
                keyboardType={'number-pad'}
                onChangeText={(x) => {
                  var index = 2;
                  var value = x;
                  this._handlePackagingInput(index, value);
                }}
                value={this.state.packaging_size[2].height}
                maxLength={10}
              />
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <Text style={sell.SizeNote}>Note: Use packaging size, not the product size</Text>

            <View style={sell.space} />
            <View style={sell.space} />

            {this._renderPackagingSaveButton()}

          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  _renderPackagingSaveButton = () => {
    if (this.state.packaging_size[0].width.length > 0 && this.state.packaging_size[1].length.length && this.state.packaging_size[2].height.length) {
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => this.setState({
            packagingModal: false,
            modalVisible2: false,
            to_send_packagingSize: this.state.packaging_size
          }, function () {
            console.warn(this.state.to_send_packagingSize);
          })}
          style={sell.SaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }

    else {
      return (
        <TouchableOpacity
          onPress={() => this.setState({ packagingModal: false, modalVisible2: false })}
          disabled
          style={sell.disableSaveBtn}
        >
          <Text style={sell.SaveText}>Save</Text>
        </TouchableOpacity>
      );
    }

  }

  _handlePackagingInput = (index, value) => {
    var data = [...this.state.packaging_size];
    if (index === 0) {
      data.splice(index, 1, { width: value });
    }
    if (index === 1) {
      data.splice(index, 1, { length: value });
    }
    if (index === 2) {
      data.splice(index, 1, { height: value });
    }
    this.setState({
      packaging_size: data,
    });
    console.log(this.state.packaging_size);
  }

  render() {
    return (
      <Container style={sell.container}>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <Left style={{ flex: 1 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Image source={require('../../assets/images/headericon/left-arrow-dark.png')}
                style={sell.backImage}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 0 }}>
            <Text style={sell.headerText}> Sell Product </Text>
          </Body>
          <Right style={{ flex: 1 }}>
            <View style={sell.rightView}>
            </View>
          </Right>
        </Header>
        <KeyboardAwareScrollView keyboardDismissMode='interactive' style={sell.contentContainer}>
          <View>
            <View style={sell.uploadContainer}>
              <ScrollView horizontal={true} scrollEnabled={true} style={{ width: (width / 2 - 2) }}>
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() => alert('Under Development Stage')}
                >
                  <FastImage
                    source={require('./../../assets/images/Sell_Product-09.png')}
                    large
                    style={sell.newProductImage}
                  />
                </TouchableOpacity>
              </ScrollView>
            </View>
            <View style={sell.whiteName}>
              <View style={{ flexDirection: 'row', height: hp('5.8%') }}>
                <View style={{ flexDirection: 'column', width: wp('90%'), }}>
                  <Input
                    style={{ fontSize: hp('1.7%'), marginLeft: 15, }}
                    onChangeText={(name) => this.setState({ name })}
                    value={this.state.name}
                    placeholder="Product Name"
                    placeholderTextColor="#7f8c8d"
                    maxLength={50}
                  />
                </View>
                <View style={{ flexDirection: 'row', paddingVertical: 10 }}>
                  <Text style={sell.Length50}>{this.state.name.length}/50</Text>
                </View>
              </View>
            </View>

            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(description) => this.setState({ description })}
                    value={this.state.description}
                    placeholder="Description"
                    placeholderTextColor="#7f8c8d"
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.whiteBorder}>
              {this.renderReferrerModal()}
              <TouchableOpacity activeOpacity={0.4}
                onPress={() => this.setModalVisible1(true)}
                style={sell.inputContainer}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={sell.spaceLeft} />
                    <View style={sell.spaceLeft} />
                    <Text style={[sell.hintText, { marginLeft: 38 }]}>Referrer Commission</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>{this.state.referrer_commission == '' ? 'Set Commission %' : this.state.referrer_commission + '%'}</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/images/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.whiteBorder}>
              <TouchableOpacity
                activeOpacity={0.4}
                onPress={() => alert('Under Development Stage')}
              >
                <View style={sell.inputContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ marginLeft: 27 }} />
                    <FastImage
                      source={require('../../assets/images/internal/icon_category.png')}
                      style={sell.sideIcon}
                    />
                    <Text style={sell.inputText}>Category</Text>
                  </View>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                    <Text style={sell.hintText2}>Set Category</Text>
                    <View style={sell.spaceRight} />
                    <FastImage
                      source={require('../../assets/images/icon_sub.png')}
                      style={sell.openIcon}
                      tintColor={'#7f8c8d'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View>

            <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3', }} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Price and Inventory</Text>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 27 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_price.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Price</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.group_input_text}
                    placeholder={'Price'}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'decimal-pad'}
                    onChangeText={(x) => this.setState({ price: x })}
                    value={this.state.price}
                  />
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 27 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_stock.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Stock</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.group_input_text}
                    placeholder={'Stock'}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'number-pad'}
                    onChangeText={(x) => this.setState({ stock: x })}
                    value={this.state.stock}
                  />
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              <TouchableOpacity
                style={sell.inputContainer}
                activeOpacity={0.4}
                onPress={() => this.props.navigation.navigate('VariationsScreen')}
              >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_Variations.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Variations</Text>
                </View>

                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>Set Variations</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/images/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.itemBorder} />

            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Shipping</Text>
            </View>

            <View style={sell.white}>
              <View style={sell.inputContainer}>
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_weight.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Weight in</Text>
                </View>
                <Item style={sell.input_title}>
                  <Input
                    style={sell.weight_group_input_text}
                    placeholder={'Weight in '}
                    placeholderTextColor={'#7f8c8d'}
                    keyboardType={'number-pad'}
                    onChangeText={(x) => this.setState({ weight: x })}
                  />
                  <View style={{ paddingRight: 30 }}>
                    <Text style={sell.Kg}>Kg</Text>
                  </View>
                </Item>
              </View>
            </View>

            <View style={sell.white}>
              {this.renderPackageModal()}
              <TouchableOpacity
                onPress={() => this.setModalVisible2(true)}
                style={sell.inputContainer}
              >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_size.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Packaging Size</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.size_group_input_text}>
                    {this.state.packaging_size[0].width.length > 0 && this.state.packaging_size[0].width + 'cm X ' + this.state.packaging_size[1].length + 'cm X ' + this.state.packaging_size[2].height + 'cm'}
                    {!this.state.packaging_size[0].width.length > 0 && 'Packaging Size'}
                  </Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/images/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.white}>
              <TouchableOpacity
                onPress={() => alert('Under Development Stage')}
                style={sell.inputContainer}
              >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_shipping_fee.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Shipping Fee</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.hintText2}>Set Shipping Fee</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/images/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={sell.spaceLeft}>
              <Text style={sell.group_title}>Others</Text>
            </View>

            <View style={sell.white}>
              {this.conditionsModal()}
              <TouchableOpacity
                style={sell.inputContainer}
                onPress={() => this.setConditionModal(true)}
              >
                <View style={{ flexDirection: 'row' }}>
                  <View style={{ marginLeft: 25 }} />
                  <FastImage
                    source={require('../../assets/images/internal/icon_condition.png')}
                    style={sell.sideIcon}
                  />
                  <Text style={sell.inputText}>Condition</Text>
                </View>
                <View style={{ flexDirection: 'row', paddingHorizontal: 15 }}>
                  <Text style={sell.condition_group_input_text}>{this.state.value}</Text>
                  <View style={sell.spaceRight} />
                  <FastImage
                    source={require('../../assets/images/icon_sub.png')}
                    style={sell.openIcon}
                    tintColor={'#7f8c8d'}
                  />
                </View>
              </TouchableOpacity>
            </View>

            <View style={sell.whiteDescription}>
              <View style={sell.descriptionContainer}>
                <View style={sell.DescriptionItem}>
                  <TextInput
                    style={sell.descriptionText}
                    onChangeText={(warranty) => this.setState({ warranty })}
                    value={this.state.warranty}
                    placeholder="Set Warranty Information"
                    placeholderTextColor={'#7f8c8d'}
                    editable={true}
                    multiline={true}
                  />
                </View>
              </View>
            </View>

            <View style={sell.itemBorder} />
            <View style={sell.space} />
            <View style={sell.space} />

            <View style={{ paddingTop: 10, paddingBottom: 5 }}>
              <TouchableOpacity
                avtiveOpacity={0.4}
                onPress={() => alert('Under Development Stage')}
                style={sell.nextButton}
              >
                <Text style={sell.SellText}>Sell</Text>
              </TouchableOpacity>
            </View>

          </View>
        </KeyboardAwareScrollView>
      </Container>
    );
  }

}

const mapStateToProps = state => {
  return {
    ...state,
  };
};

export default connect(mapStateToProps)(SellProductScreenOld);