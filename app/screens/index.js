// import * as React from 'react';
import React, { useState, useEffect, createContext, useReducer, useMemo } from 'react';
import {
  View,
  Alert,
} from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AsyncStorage from '@react-native-community/async-storage';
import FireChat from '../../FireChat';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { useFocusEffect } from '@react-navigation/native';
import HTML from "react-native-render-html";
import Service from '../components/api/service';

import { LoginButton, AccessToken, LoginManager, Profile, Settings } from 'react-native-fbsdk-next';

import Home from './home/index';
import Pinned from './pin/index';
import AddProduct from './product/index';
import Notifications from './notifications/index';
import MyProfile from './profile/index';
import Auth from './auth/index';

import auth from '@react-native-firebase/auth';
import messaging from '@react-native-firebase/messaging';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const AuthContext = createContext();

export default function MainTabScreen({ navigation, route }) {
  const [state, dispatch] = useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'RESTORE_TOKEN':
          return {
            ...prevState,
            userToken: action.token,
            isLoading: false,
          };
        case 'SIGN_IN':
          return {
            ...prevState,
            isSignout: false,
            userToken: action.token,
          };
        case 'SIGN_OUT':
          return {
            ...prevState,
            isSignout: true,
            userToken: null,
          };
      }
    },
    {
      isLoading: true,
      isSignout: false,
      userToken: null,
    }
  );

  const [userData, setUserData] = useState('');
  const [user, setUser] = useState('');
  const [fbUserIsLoggedIn, setFbUserIsLoggedIn] = useState('');
  const [notifCount, setNotifCount] = useState(0);

  useEffect(() => {
    const init = async () => {
      requestUserPermission()

      // Settings.initializeSDK();
    }
    init();
  }, []);

  useEffect(() => {
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log("A new FCM message arrived!", JSON.stringify(remoteMessage));
      Alert.alert(
        remoteMessage.notification.title,
        remoteMessage.notification.body,
      );
      // Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage.notification.body));
    });

    return unsubscribe;
  }, []);

  useEffect(() => {
    setNotifCount(0);
    console.log("main index useEffect");
    FireChat.shared.init();
    console.log("route.params", route.params);

    if (route.params) {
      dispatch({ type: route.params.type, token: route.params.token });
    }

    // Fetch the token from storage then navigate to our appropriate place
    const bootstrapAsync = async () => {
      let userToken, ud, _userdata;

      try {
        // isLoggedIn();
        ud = await AsyncStorage.getItem('user_data');
        _userdata = ud ? JSON.parse(ud) : null;
        setUserData(_userdata);
        setUser(auth().currentUser);
        // console.log('Check if userData exists on Auth', await AsyncStorage.getItem('user_data'));
        // console.log('Check if user exists on Auth', auth().currentUser);
      } catch (e) {
        // Restoring token failed
      }

      // console.log('USER TOKEN ON INDEX', userToken);
      // console.log('HOME PAGE IS FOCUSED', navigation.isFocused());

      // After restoring token, we may need to validate it in production apps

      // This will switch to the App screen or Auth screen and this loading
      // screen will be unmounted and thrown away.
      dispatch({ type: 'RESTORE_TOKEN', token: userToken });

      let data = {};

      console.log("_userdata 1", _userdata)
      console.log("_userdata.data 1", _userdata.data)
      console.log("_userdata.data.id 1", _userdata.data.id)

      if (_userdata != null) {
        data.user_id = _userdata?.data?.id;
        await Service.getNotificationsList(
          data,
          (res) => {
            // console.log("getNotificationsList 2", res)
            let newNotifCount = 0;
            res.data.map((value, index) => {
              // console.log(index, value.id, "value.is_viewed", value.is_viewed)
              if (value.is_viewed != 'yes') {
                newNotifCount++
              }
            })
            setNotifCount(newNotifCount);
            console.log("newNotifCount", newNotifCount)
          },
          (err) => {
            console.log("Service.getNotificationsList error", err)
          }
        );
      }

      messaging().onNotificationOpenedApp(remoteMessage => {
        console.log(
          'Notification caused app to open from background state:',
          remoteMessage.notification,
        );

        let { description, type, reference, is_viewed, reference_id, id } = remoteMessage.data;

        handleRedirect(type, reference_id, id, is_viewed);
        // navigation.navigate(remoteMessage.data.type);
      });

      // Check whether an initial notification is available
      messaging()
        .getInitialNotification()
        .then(remoteMessage => {
          if (remoteMessage) {
            console.log(
              'Notification caused app to open from quit state:',
              remoteMessage,
            );

            let { description, type, reference, is_viewed, reference_id, id } = remoteMessage.data;

            handleRedirect(type, reference_id, id, is_viewed);
            // navigation.navigate(remoteMessage.data.type);
          }
          // setLoading(false);
        });
    };

    bootstrapAsync();
  }, []);

  // Attempt at logging out a fb authenticated user when his fb has been signed out
  useFocusEffect(
    React.useCallback(() => {
      console.log("useFocusEffect");
      let isActive = true;

      async function reload() {
        let ud = await AsyncStorage.getItem('user_data');
        let user_data = ud ? JSON.parse(ud) : null;
        setUserData(user_data);
        let tmpUser = auth().currentUser;
        setUser(tmpUser);
        let fb_user_id = await AsyncStorage.getItem('fb_user_id');
        console.log("auth().currentUser", user_data);
        console.log("fb_user_id != null", fb_user_id);
        console.log("user_data", tmpUser);
        // console.log("async fb_user_id", fb_user_id);

        console.log("user_data 1", user_data)
        console.log("user_data.data 1", user_data.data)
        console.log("user_data.data.id 1", user_data.data.id)

        // return

        if (user_data != null) {
          data.user_id = user_data?.data?.id;
          await Service.getNotificationsList(
            data,
            (res) => {
              // console.log("getNotificationsList 2", res)
              let newNotifCount = 0;
              res.data.map((value, index) => {
                // console.log(value.id, "value.is_viewed", value.is_viewed)
                if (value.is_viewed != 'yes') {
                  newNotifCount++
                }
              })
              setNotifCount(newNotifCount);
              console.log("newNotifCount", newNotifCount)
            },
            (err) => {
              console.log("Service.getNotificationsList error", err)
            }
          );
        }

        await Profile.getCurrentProfile().then(async (res) => {
          if (res != null) {
            if (fb_user_id != await res?.userID) {
              console.log("logging user out 1");
              await AsyncStorage.removeItem('auth');
              await AsyncStorage.removeItem('user_data');
              await AsyncStorage.removeItem('chat');
              await AsyncStorage.removeItem('fcmtoken');

              // Firebase Signout start                        
              auth()
                .signOut()
                .then((response) => {
                  console.log("THIS IS FIREBASE SIGN OUT RESPONSE", response)
                })
                .catch(error => {
                  alert(error)
                })
              // Firebase Signout end  

              await AsyncStorage.removeItem('fb_user_id');
              LoginManager.logOut();

              Alert.alert(
                'Unauthenticated',
                'You are not logged in! Login now to continue',
                [
                  {
                    text: 'Login',
                    onPress: () => {
                      navigation.navigate("Sign In")
                    },
                  },
                  {
                    text: 'Cancel',
                    style: 'cancel',
                    onPress: () => {
                      navigation.reset({
                        index: 0,
                        routes: [{ name: 'Tab' }],
                      });
                    },
                  },
                ],
                { cancelable: false }
              );
            }
          } else if (auth().currentUser != null && fb_user_id != null) {
            console.log("logging user out 2");
            // await AsyncStorage.removeItem(API_TOKEN)
            await AsyncStorage.removeItem('auth');
            await AsyncStorage.removeItem('user_data');
            await AsyncStorage.removeItem('chat');
            await AsyncStorage.removeItem('fcmtoken');
            // await Notification.removeToken();

            // Firebase Signout start                        
            auth()
              .signOut()
              .then((response) => {
                console.log("THIS IS FIREBASE SIGN OUT RESPONSE", response)
              })
              .catch(error => {
                alert(error)
              })
            // Firebase Signout end  

            await AsyncStorage.removeItem('fb_user_id');
            LoginManager.logOut();

            Alert.alert(
              'Unauthenticated',
              'You are not logged in! Login now to continue',
              [
                {
                  text: 'Login',
                  onPress: () => {
                    navigation.navigate("Sign In")
                  },
                },
                {
                  text: 'Cancel',
                  style: 'cancel',
                  onPress: () => {
                    navigation.reset({
                      index: 0,
                      routes: [{ name: 'Tab' }],
                    });
                  },
                },
              ],
              { cancelable: false }
            );
          }
        })

      };

      reload();

      return () => {
        isActive = false;
      };
    }, [navigation])
  );

  const handleRedirect = async (type, reference_id, notifID, is_viewed) => {
    let ud = await AsyncStorage.getItem('user_data');
    let _userdata = ud ? JSON.parse(ud) : null;
    let userData = _userdata;

    console.log("index handleRedirect userData", userData)
    // setLoading(true)
    let data = {};
    let data2 = {};
    switch (type) {
      case "approved_check_request":
        data.user_id = userData.data.id;

        await Service.getCheckRequestList(
          data,
          async (res2) => {
            // console.log("setLoading(false)")
            if (res2 != null && res2 != '') {
              if (res2?.data?.length > 0) {
                console.log("res2.data.length", res2.data.length)
                for (let index = 0; index < res2.data.length; index++) {
                  // console.log("index", index)
                  if (res2.data[index].id == reference_id) {
                    data2.id = notifID;
                    data2.user_id = userData.data.id;

                    if (is_viewed != "yes") {
                      await Service.viewNotification(
                        data2,
                        (res2) => {
                          console.log("Service.viewNotification: notif is now viewed")
                        },
                        (err) => {
                          console.log("Service.viewNotification error", err)
                        }
                      );
                    }

                    console.log("CheckRequestDetails", res2.data[index])
                    navigation.navigate("CheckRequestDetails", { checkRequest: res2.data[index] })
                    return
                  }
                }
              }
            }

            // setLoading(false);
          },
          (err) => {
            // setLoading(false);
            console.log("Service.getCheckRequestList error", err)
          });


        // setLoading(false)
        break;
      case "declined_check_request":
        // console.log("index handleRedirect userData", userData)
        // console.log("index handleRedirect userData.msg", userData.msg)
        // console.log("index handleRedirect userData.data", userData.data)

        data.user_id = userData.data.id;

        // console.log("index handleRedirect userData.data.id", userData.data.id)
        console.log("index handleRedirect reference_id", reference_id)
        await Service.getCheckRequestList(
          data,
          async (res2) => {
            // console.log("setLoading(false)")
            if (res2 != null && res2 != '') {
              if (res2?.data?.length > 0) {
                // console.log("res2", res2)
                for (let index = 0; index < res2.data.length; index++) {
                  console.log("res2.data[index]", res2.data[index])
                  console.log("res2.data[index].id ---", res2.data[index].id)
                  // console.log("index", index)
                  if (res2.data[index].id == reference_id) {
                    console.log("index handleRedirect res2.data[index].id", res2.data[index].id)
                    data2.id = notifID;
                    data2.user_id = userData.data.id;

                    if (is_viewed != "yes") {
                      await Service.viewNotification(
                        data2,
                        (res2) => {
                          console.log("Service.viewNotification: notif is now viewed")
                        },
                        (err) => {
                          console.log("Service.viewNotification error", err)
                        }
                      );
                    }

                    console.log("CheckRequestDetails", res2.data[index])
                    navigation.navigate("CheckRequestDetails", { checkRequest: res2.data[index] })
                    return
                  }
                }
              }
            }

            // setLoading(false);
          },
          (err) => {
            // setLoading(false);
            console.log("Service.getCheckRequestList error", err)
          });


        // setLoading(false)
        break;
      case "new_order":
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = userData.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            // setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            // setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "new_service":
        // console.log("reference_id", reference_id)
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = userData.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            // setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            // setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "cancel_order":
        // console.log("reference_id", reference_id)
        data.order_id = reference_id;
        await Service.getOrderInfo(
          data,
          async (res) => {
            console.log("getOrderInfo res", res)

            data2.id = notifID;
            data2.user_id = userData.data.id;

            if (is_viewed != "yes") {
              await Service.viewNotification(
                data2,
                (res2) => {
                  console.log("Service.viewNotification: notif is now viewed")
                },
                (err) => {
                  console.log("Service.viewNotification error", err)
                }
              );
            }

            // setLoading(false)
            navigation.navigate("TransactionDetails", {
              id: res.data.transaction_info.order_id,
              shipping: res.data.transaction_info.info.data.shipping_info,
              billing: res.data.transaction_info.info.data.billing_info,
              orders: res.data.transaction_info.order_list,
              user: res.data.transaction_info.info.data.user_info,
              date: res.data.transaction_info.date.data.added_in,
              total: res.data.transaction_info.total_amount,
              description: res.data.transaction_info.description
            })


          },
          (err) => {
            // setLoading(false)
            console.log("Service.getOrderInfo error", err)
          }
        );
        break;
      case "refund_service":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        // setLoading(false)
        break;
      case "return_order":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        // setLoading(false)
        break;
      case "suspend_product":

        if (is_viewed != "yes") {
          await Service.viewNotification(
            data2,
            (res2) => {
              console.log("Service.viewNotification: notif is now viewed")
            },
            (err) => {
              console.log("Service.viewNotification error", err)
            }
          );
        }

        // setLoading(false)
        break;

      default:

        // setLoading(false)
        break;
    }
  }

  const authContext = useMemo(
    () => ({
      signIn: async (data) => {
        // In a production app, we need to send some data (usually username, password) to server and get a token
        // We will also need to handle errors if sign in failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
      signOut: () => dispatch({ type: 'SIGN_OUT' }),
      signUp: async (data) => {
        // In a production app, we need to send user data to server and get a token
        // We will also need to handle errors if sign up failed
        // After getting token, we need to persist the token using `AsyncStorage`
        // In the example, we'll use a dummy token
        dispatch({ type: 'SIGN_IN', token: 'dummy-auth-token' });
      },
    }),
    []
  );

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  return (
    <AuthContext.Provider value={authContext}>
      <Tab.Navigator
        initialRoute='Home'
        backBehavior='initialRoute'
        screenOptions={({ route }) => ({
          headerShown: false,
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = focused ? 'home' : 'home';
              return <SimpleLineIcons name={iconName} size={size - 5} color={color} />;
            } else if (route.name === 'Pinned') {
              // iconName = focused ? 'pin-outline' : 'pin-outline';
              return <SimpleLineIcons name='pin' size={size - 5} color={color} />;
            } else if (route.name === 'Add Product') {
              // iconName = focused ? 'shopping-basket-add' : 'ios-list';
              return <Fontisto name='shopping-basket-add' size={size - 5} color={color} />;
            } else if (route.name === 'Notifications') {
              // iconName = focused ? 'ios-list-box' : 'ios-list';
              return <SimpleLineIcons name='bell' size={size - 5} color={color} />;
            } else if (route.name === 'My Profile') {
              // iconName = focused ? 'ios-list-box' : 'ios-list';
              return <SimpleLineIcons name='user' size={size - 5} color={color} />;
            }
          },
        })}
        tabBarOptions={{
          activeTintColor: '#66b545',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name='Home' component={Home} options={{ headerShown: false }} />

        {/* {state.userToken == null ? ( */}
        {(user == null || userData == null) ? (
          <Tab.Screen name='Pinned' component={Auth} options={{ headerShown: false }} />
        ) : (
          <Tab.Screen name='Pinned' component={Pinned} options={{ headerShown: false }} />
        )}
        {/* {state.userToken == null ? ( */}
        {(user == null || userData == null) ? (
          <Tab.Screen name='Add Product' component={Auth} options={{ headerShown: false }} />
        ) : (
          <Tab.Screen name='Add Product' component={AddProduct} options={{ headerShown: false }} />
        )}
        {/* {state.userToken == null ? ( */}
        {(user == null || userData == null) ? (
          <Tab.Screen name='Notifications' component={Auth} options={{ headerShown: false }} />
        ) :
          <>
            {notifCount > 0 ?
              (
                <Tab.Screen name='Notifications' component={Notifications} options={{ headerShown: false, tabBarBadge: notifCount }} />
              )
              :
              (
                <Tab.Screen name='Notifications' component={Notifications} options={{ headerShown: false, }} />
              )
            }
          </>
        }
        {/* {state.userToken == null ? ( */}
        {(user == null || userData == null) ? (
          <Tab.Screen name='My Profile' component={Auth} options={{ headerShown: false }} />
        ) : (
          <Tab.Screen name='My Profile' component={MyProfile} options={{ headerShown: false }} />
        )}
        {/* <Tab.Screen name="Home" component={Home} options={{headerShown: false}} />
        <Tab.Screen name="Pinned" component={Pinned} options={{headerShown: false}} />
        <Tab.Screen name="Add Product" component={AddProduct} options={{headerShown: false}} />
        <Tab.Screen name="Notifications" component={Notifications} options={{headerShown: false}} />
        <Tab.Screen name="My Profile" component={Auth} options={{headerShown: false}} /> */}
      </Tab.Navigator>
    </AuthContext.Provider>
  );
}

// export default function App() {

//   return (
//     <Stack.Navigator>
//       <Stack.Screen name="Home" options={{headerShown: false}} component={MainTabScreen} />
//     </Stack.Navigator>
//   )
// }
