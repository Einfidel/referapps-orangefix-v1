import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  ScrollView,
  Image,
  Alert
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Content,
  Card,
  CardItem,
  Body,
  Spinner,
} from 'native-base'
import Assets from '../../components/assets.manager';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from '../../components/icons';
import Endpoints from '../../components/api/endpoints';
import FastImage from 'react-native-fast-image'
import { useFocusEffect } from '@react-navigation/native';

const { width, height } = Dimensions.get('window')

function MyAddress({ navigation, route }) {
  const [myAddress, setMyAddress] = useState([])
  const [selectedAddress, setSelectedAddress] = useState([])
  const [user, setUser] = useState('');
  const [loading, setLoading] = useState(true);

  const init = async () => {
    setLoading(true);
    let ud = await AsyncStorage.getItem('user_data');
    let _userdata = ud ? JSON.parse(ud) : null;
    setUser(_userdata)
    await Service.getSpecificAddress(_userdata.data.id,
      (res) => {
        setLoading(false);
        setMyAddress(res.data)
        console.log('data', res.data[0])
      },
      (err) => {
        setLoading(false);
        console.log(err)
      })
  }

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  const handleDeleteAddress = async (index) => {
    Alert.alert(
      "Warning",
      "Are you sure you want to delete this address?",
      [
        {
          text: 'Cancel',
        },
        {
          text: 'Delete',
          onPress: async () => {
            setLoading(true);
            deleteAddress()
            setSelectedAddress(myAddress[index])
          },
        },
      ],
    )
  }

  const deleteAddress = async (verified = 'no', reloads = 0) => {
    let ud = await AsyncStorage.getItem('user_data');
    let user = ud ? JSON.parse(ud) : null;

    let splitEmail = user.data.info.data.email.split('@');

    if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
      // user.data.is_verify_contact_number === "no" ||
      user.data.is_verify === 'no') {
      Alert.alert(
        'Warning!',
        'Your account not yet been verified.\n' +
        'Verify Now!',
        [
          {
            text: 'Cancel'
          },
          {
            text: 'Verify',
            onPress: () => {
              if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                navigation.navigate("AddEmailMobile");
              } else if (user.data.is_verify === 'no') {
                navigation.navigate('EmailVerification')
              }
              //  else if (user.data.is_verify_contact_number === "no") {
              //   navigation.navigate('PhoneVerification')
              // }
            }
          },
        ],
        { cancelable: true }
      );
      return
    }

    let data = {
      uid: user.data.id,
      id: selectedAddress.id,
    }

    await Service.deleteAddress(
      data,
      (res) => {
        if (verified === 'no') {
          if (res.status) {
            console.log("res", res)
            setLoading(false);
            console.log('verification_code', res.verification_code);
            let verifData = {
              verifyCode: res.verification_code,
              medium: res.sent_to,
              expiration: res.verification_expiration,
              user: user,
            }
            navigation.push("VerifyCode", {
              verifData,
              reloads,
              onVerified: (verified, reloads = 0) => {
                deleteAddress(verified, reloads);
              },
            })
          } else {
            setLoading(false);
            alert(res.msg);
          }
        } else {
          if (res.status) {
            setLoading(false);
            // console.log('Data: ', res);
            Alert.alert(
              'Success',
              'Address was deleted Successfully!',
              [
                {
                  text: 'OK',
                  onPress: async () => {
                    navigation.navigate('Addresses')
                  },
                },
              ],
              { cancelable: false },
            );
          } else {
            setLoading(false);
            alert(res.msg);
          }
        }
      },
      (err) => console.log(err),
    )
  }

  return (
    <Container style={{ backgroundColor: '#EDEDEB' }}>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={{
                width: 18,
                height: 18
              }} />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>My Addresses</Text>
      </Header>
      <Content style={{ marginTop: 40 }}>
        {loading ? <Spinner /> : null}
        <TouchableOpacity onPress={() => {
          let splitEmail = user.data.info.data.email.split('@');

          if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
            // user.data.is_verify_contact_number === "no" ||
            user.data.is_verify === 'no') {
            Alert.alert(
              'Warning!',
              'Your account not yet been verified.\n' +
              'Verify Now!',
              [
                {
                  text: 'Cancel'
                },
                {
                  text: 'Verify',
                  onPress: () => {
                    if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                      navigation.navigate("AddEmailMobile");
                    } else if (user.data.is_verify === 'no') {
                      navigation.navigate('EmailVerification')
                    }
                    //  else if (user.data.is_verify_contact_number === "no") {
                    //   navigation.navigate('PhoneVerification')
                    // }
                  }
                },
              ],
              { cancelable: true }
            );
            return
          }
          navigation.navigate('Add Address')
        }}
          style={styles.addAddress}>
          <Text style={styles.addAddressText}> + Add addresses</Text>
        </TouchableOpacity>
        <View style={{ flex: 1 }}>
          <View style={{ padding: 20, }}>
            <Text>My Address Book List</Text>
          </View>
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ScrollView>
              <View>
                {myAddress.map((value, index) => {
                  return (
                    <View style={{ width: wp('90%') }} key={index}>
                      <Card>
                        <CardItem>
                          <Body>
                            <View style={{ flexDirection: 'row', flex: 1 }}>
                              <View>
                                <Text style={{ fontWeight: 'bold' }}>
                                  Full name
                                </Text>
                                <Text>
                                  {value.info.data.contact_person}
                                </Text>
                              </View>
                              <View style={{ justifyContent: 'flex-start', alignItems: 'flex-end', flex: 6 }}>
                                <TouchableOpacity onPress={() => {
                                  let splitEmail = user.data.info.data.email.split('@');

                                  if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
                                    // user.data.is_verify_contact_number === "no" ||
                                    user.data.is_verify === 'no') {
                                    Alert.alert(
                                      'Warning!',
                                      'Your account not yet been verified.\n' +
                                      'Verify Now!',
                                      [
                                        {
                                          text: 'Cancel'
                                        },
                                        {
                                          text: 'Verify',
                                          onPress: () => {
                                            if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                                              navigation.navigate("AddEmailMobile");
                                            } else if (user.data.is_verify === 'no') {
                                              navigation.navigate('EmailVerification')
                                            }
                                            //  else if (user.data.is_verify_contact_number === "no") {
                                            //   navigation.navigate('PhoneVerification')
                                            // }
                                          }
                                        },
                                      ],
                                      { cancelable: true }
                                    );
                                    return
                                  }

                                  navigation.navigate('Edit Address', { value: value })
                                }}>
                                  <Icon.EvilIcons name='pencil' size={21} style={{ color: 'green' }} />
                                </TouchableOpacity>
                              </View>
                              <View style={{ justifyContent: 'flex-start', alignItems: 'flex-end', flex: 1 }}>
                                <TouchableOpacity onPress={() => {
                                  let splitEmail = user.data.info.data.email.split('@');

                                  if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
                                    // user.data.is_verify_contact_number === "no" ||
                                    user.data.is_verify === 'no') {
                                    Alert.alert(
                                      'Warning!',
                                      'Your account not yet been verified.\n' +
                                      'Verify Now!',
                                      [
                                        {
                                          text: 'Cancel'
                                        },
                                        {
                                          text: 'Verify',
                                          onPress: () => {
                                            if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                                              navigation.navigate("AddEmailMobile");
                                            } else if (user.data.is_verify === 'no') {
                                              navigation.navigate('EmailVerification')
                                            }
                                            //  else if (user.data.is_verify_contact_number === "no") {
                                            //   navigation.navigate('PhoneVerification')
                                            // }
                                          }
                                        },
                                      ],
                                      { cancelable: true }
                                    );
                                    return
                                  }

                                  handleDeleteAddress(index)
                                }} style={styles.minusCon}>
                                  <FastImage source={Assets.myProfile.icon_minus} style={styles.imgMinus} resizeMode={FastImage.resizeMode.contain} />
                                </TouchableOpacity>
                              </View>
                            </View>
                            <View>
                              <Text style={{ fontWeight: 'bold' }}>
                                Address
                              </Text>
                              <Text>
                                {value.info.data.street_address}
                              </Text>
                            </View>
                            <View>
                              <Text style={{ fontWeight: 'bold' }}>
                                Postcode
                              </Text>
                              <Text>
                                {value.info.data.province_code} - {value.info.data.city_code} - {value.info.data.barangay_code}
                              </Text>
                            </View>
                            <View>
                              <Text style={{ fontWeight: 'bold' }}>
                                Contact number
                              </Text>
                              <Text>
                                {value.info.data.contact_number}
                              </Text>
                            </View>
                            <View>
                              <Text style={{ fontWeight: 'bold' }}>
                                Shipping / Billing
                              </Text>
                              <Text>
                                {value.info.data.is_shipping == 'yes' ? 'Default Shipping Address' : 'Other'}
                              </Text>
                              <Text>
                                {value.info.data.is_billing == 'yes' ? 'Default Billing Address' : 'Other'}
                              </Text>
                            </View>
                          </Body>
                        </CardItem>
                      </Card>
                    </View>
                  )
                })}
              </View>
            </ScrollView>
          </View>
        </View>
      </Content>
    </Container>
  )
}
export default MyAddress
const styles = StyleSheet.create({
  headerText: {
    // fontFamily:'Roboto', 
    fontSize: 20,
    alignSelf: 'center'
  },
  addAddress: {
    width: width,
    borderWidth: .7,
    borderColor: '#d0dbdb',
    paddingVertical: 20
  },
  addAddressText: {
    // fontFamily:'Roboto', 
    fontSize: 14,
    color: 'gray',
    paddingHorizontal: 40, justifyContent: 'center'
  },
  minusCon: {
    alignSelf: 'flex-end',
    // marginVertical:10
  },
  imgMinus: {
    tintColor: '#e04f5f',
    resizeMode: 'contain',
    height: 15,
    width: 15
  },
})
