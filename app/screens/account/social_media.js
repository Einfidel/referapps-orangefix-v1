import React, { Component } from 'react';
import {
  View,
  Dimensions,
  StyleSheet,
  Modal,
  TouchableWithoutFeedback,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  TimePickerAndroid
} from 'react-native'

import { TextInput } from 'react-native-gesture-handler';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from '../../components/assets.manager';
import { Container, Header, Content, List, ListItem, Text, Left, Right, Icon, Thumbnail, Fab, Button } from 'native-base';
import Endpoints from '../../components/api/endpoints';
import Service from '../../components/api/service';
import AsyncStorage from '@react-native-community/async-storage';
import Toaster from 'react-native-simple-toast';

const { width, height } = Dimensions.get('window')
class SocialMedia extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      imodalVisible: false,
      tmodalVisible: false,
      deleteModal: false,
      target: '',
      facebookInfo: '',
      instagramInfo: '',
      twitterInfo: '',
      fbLinkID: '',
      user: {},
      socialLinks: {},
      linkIDToDelete: null
    }

  }

  closeModal(visible) {
    this.setState({ modalVisible: visible });
    this.setState({ imodalVisible: visible });
    this.setState({ tmodalVisible: visible });
    this.setState({ deleteModal: visible });
  }

  componentWillMount = async () => {
    this.init()
  }

  init = async () => {
    const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = ud ? JSON.parse(ud) : null;
    if (_userdata) {
      let THIS = this;
      console.log("USER DATA", JSON.stringify(_userdata, null, '\t'));
      await Service.userData(
        _userdata.data.id,
        (res) => {
          THIS.setState({
            socialLinks: res.data.info.data.social_links,
            user: _userdata.data
          });
          console.log("social links", res.data.info.data.social_links)
        },
        (err) => console.warn('Error', err.message),
      )
    }
  }

  handleCheckInput = () => {
    if (this.state.facebookInfo == '' || !this.state.facebookInfo) {
      Toaster.showWithGravity("This field is required", Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else {
      this.handleSave();
    }
  }

  handleCheckInputI = () => {
    if (this.state.instagramInfo == '' || !this.state.instagramInfo) {
      Toaster.showWithGravity("This field is required", Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else {
      this.handleSave();
    }
  }

  handleCheckInputT = () => {
    if (this.state.twitterInfo == '' || !this.state.twitterInfo) {
      Toaster.showWithGravity("This field is required", Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else {
      this.handleSave();
    }
  }


  handleSave = async () => {
    let link = ''
    if (this.state.target == 'Facebook') link = `https://www.facebook.com/${this.state.facebookInfo}`
    else if (this.state.target == 'Instagram') link = `https://www.instagram.com/${this.state.instagramInfo}`
    else if (this.state.target == 'Twitter') link = `https://www.twitter.com/${this.state.twitterInfo}`
    const res = await Service.addSocialLink(this.state.target, this.state.user.id, link)
    if (res.status) {
      console.log('Data: ', res);
      Alert.alert(
        'Succesfully!',
        res.msg,
        [
          {
            text: 'OK',
            onPress: async () => {
              this.setState({ modalVisible: false })
              this.setState({ imodalVisible: false })
              this.setState({ tmodalVisible: false })
              this.init()
            },
          },
        ],
        { cancelable: false },
      );
    } else {
      alert(res.msg);
      return false;
    }
  }

  handleDelete = async () => {
    let res = await Service.removeSocialLink(this.state.user.id, this.state.linkIDToDelete)
    console.log("Deletion", res, this.state.linkIDToDelete)
    if (res.status) {
      Alert.alert(
        'Succesfully!',
        res.msg,
        [
          {
            text: 'OK',
            onPress: async () => {
              this.setState({ deleteModal: false })
              this.init()
            },
          },
        ],
        { cancelable: false },
      );
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#ffffff', top: 25 }}>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                style={{
                  width: 18,
                  height: 18,
                }}
              />
            </TouchableOpacity>
          </View>
          <View style={{ alignSelf: 'center' }}>
            <Text style={styles.headerText}>Social Media Accounts</Text>
          </View>
        </Header>

        <List>
          {this.state.socialLinks.length > 0 ? this.state.socialLinks.map((item, i) => {
            return (
              <ListItem>
                <Left>
                  <TouchableOpacity onPress={() => this.setState({ modalVisible: !this.state.modalVisible, target: item.type })}>
                    <Text>{item.type} @{item.link.split(".com/")[1]}</Text>
                  </TouchableOpacity>
                </Left>
                <Right>
                  <TouchableOpacity onPress={() => this.setState({ deleteModal: !this.state.deleteModal, linkIDToDelete: item.id })}>
                    <Icon name="trash" />
                  </TouchableOpacity>
                </Right>
              </ListItem>
            )
          }) : null}
        </List>


        <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: false })}>
          <Modal
            animationType='fade'
            transparent={true}
            visible={this.state.modalVisible}
            onRequestClose={() => { this.setState({ modalVisible: false }) }}
          >
            <TouchableOpacity
              style={styles.mainContainer}
              activeOpacity={1}
              onPressOut={() => { this.closeModal(!this.state.modalVisible) }}>
              <View style={{
                backgroundColor: '#ffffff',
                width: wp('90%'),
                borderRadius: 5,
                paddingVertical: 10
              }}>
                <Text style={{
                  fontWeight: "bold",
                  fontSize: 21,
                  // padding:15,
                  color: "#000",
                  textAlign: "center",
                  paddingVertical: 10

                }}>Facebook Account</Text>
                <View style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "lightgray"
                }}></View>

                <View style={styles.usernameContainer}>
                  <View style={{}}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Facebook Username</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={styles.link}
                    >https://www.facebook.com/ </Text>
                    <TextInput
                      placeholder='username'
                      style={styles.usernameInput}
                      placeholderTextColor={'#7f8c8d'}
                      value={this.state.facebookInfo}
                      onChangeText={(e) => {
                        this.setState({
                          facebookInfo: e,
                        });
                      }}
                    >
                    </TextInput>
                  </View>
                </View>
                <View style={styles.modalFooter}>
                  <View style={{
                    width: "100%",
                    borderBottomColor: 'lightgray',
                    borderBottomWidth: 1
                  }}></View>
                  <View style={{ flexDirection: "row-reverse", margin: 10 }}>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 20,
                      backgroundColor: "#db2828"
                    }}
                      onPress={() => this.setState({ modalVisible: false })} >
                      <Text style={{ color: "#fff" }}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 20,
                      backgroundColor: "#21ba45"
                    }}
                      onPress={() => { this.handleCheckInput(); }}
                    >
                      <Text style={{ color: "#fff" }}>Add</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Modal>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={() => this.setState({ imodalVisible: false })}>
          <Modal
            animationType='fade'
            transparent={true}
            visible={this.state.imodalVisible}
            onRequestClose={() => { this.setState({ imodalVisible: false }) }}
          >
            <TouchableOpacity
              style={styles.mainContainer}
              activeOpacity={1}
              onPressOut={() => { this.closeModal(!this.state.imodalVisible) }}>
              <View style={{
                backgroundColor: '#ffffff',
                width: wp('90%'),
                borderRadius: 5,
                paddingVertical: 10
              }}>
                <Text style={{
                  fontWeight: "bold",
                  fontSize: 21,
                  // padding:15,
                  color: "#000",
                  textAlign: "center",
                  paddingVertical: 10

                }}>Instagram Account</Text>
                <View style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "lightgray"
                }}></View>

                <View style={styles.usernameContainer}>
                  <View style={{}}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Instragram Username</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={styles.link}
                    >https://www.instagram.com/ </Text>
                    <TextInput
                      placeholder='username'
                      style={styles.usernameInput}
                      placeholderTextColor={'#7f8c8d'}
                      value={this.state.instagramInfo}
                      onChangeText={(e) => {
                        this.setState({
                          instagramInfo: e,
                        });
                      }}
                    >
                    </TextInput>
                  </View>
                </View>
                <View style={styles.modalFooter}>
                  <View style={{
                    width: "100%",
                    borderBottomColor: 'lightgray',
                    borderBottomWidth: 1
                  }}></View>
                  <View style={{ flexDirection: "row-reverse", margin: 10 }}>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 20,
                      backgroundColor: "#db2828"
                    }}
                      onPress={() => this.setState({ imodalVisible: false })} >
                      <Text style={{ color: "#fff" }}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 20,
                      backgroundColor: "#21ba45"
                    }}
                      onPress={() => { this.handleCheckInputI(); }}
                    >
                      <Text style={{ color: "#fff" }}>Save</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Modal>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={() => this.setState({ tmodalVisible: false })}>
          <Modal
            animationType='fade'
            transparent={true}
            visible={this.state.tmodalVisible}
            onRequestClose={() => { this.setState({ tmodalVisible: false }) }}
          >
            <TouchableOpacity
              style={styles.mainContainer}
              activeOpacity={1}
              onPressOut={() => { this.closeModal(!this.state.tmodalVisible) }}>
              <View style={{
                backgroundColor: '#ffffff',
                width: wp('90%'),
                borderRadius: 5,
                paddingVertical: 10
              }}>
                <Text style={{
                  fontWeight: "bold",
                  fontSize: 21,
                  // padding:15,
                  color: "#000",
                  textAlign: "center",
                  paddingVertical: 10

                }}>Twitter Account</Text>
                <View style={{
                  width: "100%",
                  height: 1,
                  backgroundColor: "lightgray"
                }}></View>

                <View style={styles.usernameContainer}>
                  <View style={{}}>
                    <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Twitter Username</Text>
                  </View>
                  <View style={{ flexDirection: 'row' }}>
                    <Text
                      style={styles.link}
                    >https://www.twitter.com/ </Text>
                    <TextInput
                      placeholder='username'
                      style={styles.usernameInput}
                      placeholderTextColor={'#7f8c8d'}
                      value={this.state.twitterInfo}
                      onChangeText={(e) => {
                        this.setState({
                          twitterInfo: e,
                        });
                      }}
                    >
                    </TextInput>
                  </View>
                </View>
                <View style={styles.modalFooter}>
                  <View style={{
                    width: "100%",
                    borderBottomColor: 'lightgray',
                    borderBottomWidth: 1
                  }}></View>
                  <View style={{ flexDirection: "row-reverse", margin: 10 }}>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 15,
                      backgroundColor: "#db2828"
                    }}
                      onPress={() => this.setState({ tmodalVisible: false })} >
                      <Text style={{ color: "#fff" }}>Cancel</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 15,
                      backgroundColor: "#21ba45"
                    }}
                      onPress={() => { this.handleCheckInputT(); }}
                    >
                      <Text style={{ color: "#fff" }}>Save</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </Modal>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback onPress={() => this.setState({ deleteModal: false })}>
          <Modal
            animationType='fade'
            transparent={true}
            visible={this.state.deleteModal}
            onRequestClose={() => { this.setState({ deleteModal: false }) }}
          >
            <View style={styles.overlay}>
              <View style={styles.container}>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity
                    style={{
                      borderRadius: 5,
                      marginHorizontal: 10,
                      paddingVertical: 10,
                      paddingHorizontal: 10,

                    }}
                    onPress={() => { this.handleDelete() }}
                  >
                    <Text style={{ color: "#d9534f" }}>Delete Social Link</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ borderBottomWidth: 2, borderBottomColor: '#e9e9e9' }}>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                  <TouchableOpacity style={{
                    borderRadius: 5,
                    marginHorizontal: 10,
                    paddingVertical: 10,
                    paddingHorizontal: 20,
                  }}
                    onPress={() => this.setState({ deleteModal: false })}
                  >
                    <Text style={{ color: "#5cb85c" }}>Cancel</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </TouchableWithoutFeedback>


        <View style={{ flex: 1, top: -25 }}>
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{}}
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}>
            <Icon name="share" />
            <Button style={{ backgroundColor: '#34A34F' }} onPress={() => this.setState({ tmodalVisible: true, target: 'Twitter' })}>
              <Icon name="logo-twitter" />
            </Button>
            <Button style={{ backgroundColor: '#3B5998' }} onPress={() => this.setState({ modalVisible: true, target: 'Facebook' })}>
              <Icon name="logo-facebook" />
            </Button>
            <Button style={{ backgroundColor: '#DD5144' }} onPress={() => this.setState({ imodalVisible: true, target: 'Instagram' })}>
              <Icon name="logo-instagram" />
            </Button>
          </Fab>
        </View>
      </Container>
    )
  }

}

export default (SocialMedia)

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  socialMediaText: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    marginLeft: 10
  },
  socialMediaLinkText: {
    fontFamily: 'Roboto',
    fontSize: 14,
    color: 'gray',
    marginLeft: 10,
    fontStyle: 'italic'
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textContainer: {
    backgroundColor: '#ffffff',
    width: '99%',
    height: 180
  },
  modalViewContainter: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  socialMediaTitle: {
    fontFamily: 'Roboto',
    fontSize: 10,
    marginVertical: 20,
    marginHorizontal: 20
  },
  btnAdd: {
    backgroundColor: '#00a14b',
    width: '20%',
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center'
  },
  btnTextAdd: {
    color: '#ffffff',
    textAlign: 'center',
    paddingVertical: 10
  },
  link: {
    fontStyle: 'italic',
    fontFamily: 'Roboto',
    fontSize: 12,
    color: 'gray',
    textAlignVertical: 'center',
    //  marginHorizontal:20,
    //  marginVertical:10,
  },
  usernameInput: {
    borderBottomColor: 'gray',
    borderBottomWidth: .5,
    fontSize: 12,
    color: 'black',
    textAlignVertical: 'center',
    padding: 0,
    width: wp('30%'),
    // marginLeft:5
  },
  usernameContainer: {
    // flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'baseline',
    alignSelf: 'center',
    marginTop: 20,
    marginBottom: 20
  },
  headerText: {
    fontFamily: 'Roboto',
    fontSize: 20,
    color: '#231f20',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'flex-end',
    padding: 15
  },
  container: {
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 10,
    borderTopRightRadius: 12,
    borderTopLeftRadius: 12,
    borderBottomEndRadius: 12,
    borderBottomStartRadius: 12

  },
})
