import React from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Content
} from 'native-base'
import Assets from '../../components/assets.manager';

const { width, height } = Dimensions.get('window')
class MyAddress extends React.Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#EDEDEB' }}>
        <Header style={{ backgroundColor: '#ffffff' }}>
          <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                style={{
                  width: 18,
                  height: 18
                }} />
            </TouchableOpacity>
          </View>
          <Text style={styles.headerText}>My Devices</Text>
        </Header>
        <Content style={{ marginTop: 40 }}>
          <TouchableOpacity onPress={() => alert("Coming soon!")}
            style={styles.addAddress}>
            <Text style={styles.addAddressText}> + Add Devices</Text>
          </TouchableOpacity>
        </Content>
      </Container>
    )
  }
}
export default MyAddress
const styles = StyleSheet.create({
  headerText: {
    // fontFamily:'Roboto', 
    fontSize: 20,
    alignSelf: 'center'
  },
  addAddress: {
    width: width,
    borderWidth: .7,
    borderColor: '#d0dbdb',
    paddingVertical: 20
  },
  addAddressText: {
    // fontFamily:'Roboto', 
    fontSize: 14,
    color: 'gray',
    paddingHorizontal: 40, justifyContent: 'center'
  }
})
