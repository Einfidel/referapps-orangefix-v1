import React, { useState, useEffect } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native'
import {
  Container,
  Thumbnail,
  Content,
  Header,
  Left,
  Right,
  Body,
  ListItem,
} from 'native-base'
import CheckBox from 'react-native-modest-checkbox'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Assets from '../../../components/assets.manager';
const { width, height } = Dimensions.get('window')

function Language(navigation) {
  const [checkedEnglish, setCheckedEnglish] = useState(true)
  const [checkedChinese, setCheckedChinese] = useState(false)

  const EnglishLanguage = (value) => {
    setCheckedEnglish(value)
  }

  const ChineseLanguage = (value) => {
    setCheckedChinese(value)
  }

  return (
    <Container style={{ backgroundColor: '#F7F5F6' }}>
      <Header style={styles.headerStyle}>
        <Left style={{ flex: 0 }}>
          <TouchableOpacity onPress={() => navigation.navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              square
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 1, justifyContent: 'center' }}>
          <Text style={styles.headerText}>Language</Text>
        </Body>
        <Right style={{ flex: 0 }}>
          <View style={{ width: 20 }} />
        </Right>
      </Header>
      <Content contentContainerStyle={{ backgroundColor: '#F7F5F6' }}>
        <View style={{ height: 30 }}>
        </View>
        {/* <View style={styles.border}/>  */}
        <View style={[styles.textContainer, { justifyContent: 'space-between' }]}>
          <ListItem>
            <CheckBox
              checkedImage={Assets.cartFunction.checkbox}
              uncheckedImage={Assets.cartFunction.uncheck}
              labelBefore={true}
              labelStyle={{ marginRight: wp('45%') }}
              checkboxStyle={{
                width: 18,
                height: 18
              }}
              label='English'
              checked={checkedEnglish}
              onChange={(value) => {
                Alert.alert('Coming Soon!', 'This feature will be available soon.')
                // EnglishLanguage(value.checked)
              }}
            />
          </ListItem>
        </View>
        {/* <View style={styles.border}/> */}
        {/* <View style={[styles.textContainer, { justifyContent: 'space-between' }]}>
          <ListItem>
            <CheckBox
              checkedImage={Assets.cartFunction.checkbox}
              uncheckedImage={Assets.cartFunction.uncheck}
              labelBefore={true}
              labelStyle={{ marginRight: wp('40%') }}
              checkboxStyle={{
                width: 18,
                height: 18
              }}
              label='玩不了啦!'
              checked={checkedChinese}
              onChange={(value) => {
                Alert.alert('Coming Soon!', 'This feature will be available soon.')
                // ChineseLanguage(value.checked)
              }}
            />
          </ListItem>
        </View> */}
        {/* <View style={styles.border}/> */}
      </Content>
    </Container>
  )
}

export default Language;
const styles = {
  headerContainer: {
    backgroundColor: '#FFFFFF',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#FFFFFF'
  },
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  textContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    // paddingTop: 10,
    // paddingBottom: 11,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
}
