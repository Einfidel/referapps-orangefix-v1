import React, { Component } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Alert,
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Content,
  Left,
  Right,
  Body
} from 'native-base';

import Switch from 'react-native-switch-pro'
import Assets from '../../../components/assets.manager';
const { width, height } = Dimensions.get('window')

class PrivacySettings extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      switch_status: false,
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#F7F5F6' }}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={styles.headerText}>Privacy Settings</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{ width: 20 }} />
          </Right>
        </Header>
        <Content contentContainerStyle={{ backgroundColor: '#F7F5F6' }}>
          <View style={styles.space} />
          <View style={styles.space} />
          <View style={styles.border} />
          <View style={styles.textContainer}>
            <View style={{ width: '85%', flex: 0 }}>
              <Text style={styles.titleText}>App access to contacts</Text>
              <Text style={styles.contentText}>This will allow the app to sync your contacts</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Switch
                disabled
                value={this.state.switch_status}
                // onSyncPress={(x) => this.setState({switch_status: x})}
                onSyncPress={() => {
                  Alert.alert('Coming Soon!', 'This feature will be available soon.')
                }}
                circleColorInactive='#ecf0f1'
                circleColorActive='#00a14b'
                backgroundInactive='#7f8c8d'
                style={{
                  alignSelf: 'flex-end',
                  flex: 0,
                  width: 35,
                  height: 20
                }}
              />
            </View>
          </View>
          <View style={styles.border} />
        </Content>
      </Container>
    )
  }
}

export default (PrivacySettings)


const styles = {
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  space: {
    paddingTop: 15
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  textContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 3,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  contentText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingVertical: 10
  },
}
