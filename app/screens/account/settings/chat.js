import React from 'react';

import {
  View,
  Text,
  Settings,
  Dimensions,
  Alert,
} from 'react-native'
import {
  Container,
  Thumbnail,
  Content,
  Left,
  Right,
  Body,
  Header
} from 'native-base';

import { TouchableOpacity } from 'react-native-gesture-handler';
import Switch from 'react-native-switch-pro';
import Assets from '../../../components/assets.manager';
const { width, height } = Dimensions.get('window')


class ChatSettings extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      switch_status: false,
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#F7F5F6' }}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={styles.headerText}>Chat Settings</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{ width: 20 }} />
          </Right>
        </Header>
        {/* <Header
              title='Chat Settings' 
              back={() => this.props.navigation.goBack()}
              style={styles.chatHeader}
          /> */}
        <Content contentContainerStyle={{ backgroundColor: '#F7F5F6' }}>
          <View style={styles.space} />
          <View style={styles.space} />
          <View style={styles.border} />
          <View style={styles.textContainer}>
            <View style={{ width: '85%', flex: 0 }}>
              <Text style={styles.titleText}>Accept Chat From Profile Page</Text>
              <Text style={styles.contentText}>Turn on to allow people to chat you. If you are a seller this is recommended for you</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <Switch
                disabled
                value={this.state.switch_status}
                // onSyncPress={(x) => this.setState({switch_status: x})}
                onSyncPress={() => {
                  Alert.alert('Coming Soon!', 'This feature will be available soon.')
                }}
                circleColorInactive='#ecf0f1'
                circleColorActive='#00a14b'
                backgroundInactive='#7f8c8d'
                // value={true}
                // style={{ transform: [{ scaleX: .9 }, { scaleY: 1 }] }}
                // onValueChange={(value) => {}}
                style={{
                  alignSelf: 'flex-end',
                  flex: 0,
                }}
              />
            </View>
          </View>
          <View style={styles.border} />
        </Content>
      </Container>
    )
  }
}

export default (ChatSettings)

const styles = {
  left: {
    position: 'absolute',
    left: 0,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  headerText: {
    fontSize: 18,
    color: '#231f20',
    // fontFamily:'roboto-medium'
  },
  space: {
    paddingTop: 15
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  textContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 3,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  contentText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingVertical: 10
  },
  chatHeader: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
  },
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
}
