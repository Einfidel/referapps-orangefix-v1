import React, { useState, useEffect } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  BackHandler,
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Spinner,
} from 'native-base'

import Assets from '../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../components/api/service';
import moment from 'moment';

export default function AccountVerification({ navigation, route }) {
  const [user, setUser] = useState('');
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    init()
  }, [])

  const init = async () => {
    setLoading(true);

    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)

    setLoading(false);
  }

  const handleVerifyClick = async (verified = 'no', reloads = 0) => {
    setLoading(true);
    try {
      if (verified === 'no') {
        let data = {
          user_id: user.data.id,
        }

        await Service.resendAccountVerifCode(
          data,
          async (res) => {
            if (res.status) {
              setLoading(false);
              code = res.data.info.data.verification_code;
              console.log('verification_code', res.data.info.data.verification_code);
              let verifData = {
                verifyCode: res.data.info.data.verification_code,
                medium: 'email',
                expiration: new moment().add(15, 'minutes').format('YYYY-MM-DD hh:mm'),
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  handleVerifyClick(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          },
          (err) => {
            setLoading(false);
            console.log(err)
          },
        );
      } else {
        let data2 = {
          user_id: user.data.id,
          verification_code: code,
        }

        await Service.verifyAccountVerifCode(
          data2,
          async (res2) => {
            if (res2.status) {
              setLoading(false);
              console.log('Data: ', res2);
              if (user.data.is_verify_contact_number === 'no') {
                Alert.alert(
                  'Success',
                  'Your Email account has been verified!\n' +
                  'Verify your Contact Number now too!',
                  [
                    {
                      text: 'Verify',
                      onPress: async () => {
                        if (route.params?.from == "Checkout") {
                          route.params.onAdd();
                        }
                        // navigation.goBack();
                        // navigation.goBack();
                        navigation.navigate('PhoneVerification')
                      },
                    },
                    {
                      text: 'Cancel',
                      onPress: async () => {
                        if (route.params?.from == "Checkout") {
                          route.params.onAdd();
                        }
                        // navigation.goBack();
                        // navigation.goBack();
                        navigation.navigate('My Profile')
                      },
                    },
                  ],
                  { cancelable: false },
                );
              } else {
                Alert.alert(
                  'Success',
                  'Your Email account has been verified!\n' +
                  'Congratulations and enjoy your journey!',
                  [
                    {
                      text: 'OK',
                      onPress: async () => {
                        if (route.params?.from == "Checkout") {
                          route.params.onAdd();
                        }
                        // navigation.goBack();
                        // navigation.goBack();
                        navigation.navigate('My Profile')
                      },
                    },
                  ],
                  { cancelable: false },
                );
              }

            } else {
              setLoading(false);
              console.log('Data: ', res2);
              Alert.alert(
                'Alert!',
                'There was a problem verifying your account.\n' +
                'Contact us at support@referapps.com so that we can help you.',
                [
                  {
                    text: 'OK',
                  },
                ],
                { cancelable: false },
              );
            }
          },
          (err) => {
            setLoading(false);
            console.log('Data: ', err);
            alert(res2.msg);
          },
        );
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Container>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>Email Verification</Text>
      </Header>
      {loading ? <Spinner />
        :
        <View style={{ backgroundColor: '#ffffff' }}>
          {user.data.is_verify === 'yes' ?
            // {false ?
            <Text style={styles.sendVText}>
              {
                'Your Email account has already been verified!\n' +
                'Congratulations and enjoy your journey!'
              }
            </Text>
            :
            <>
              <Text style={styles.sendVText}>Verify your Email now to access more features!</Text>
              <TouchableOpacity onPress={() => {
                if (!loading) {
                  handleVerifyClick()
                }
              }}>
                <Text style={styles.sendV}>Send email verification</Text>
              </TouchableOpacity>
            </>
          }
        </View>
      }
    </Container>
  )
}

const styles = StyleSheet.create({
  headerText: {
    // fontFamily:'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center'
  },
  backIcon: {
    width: 18,
    height: 18
  },
  sendV: {
    color: '#f36e23',
    // fontFamily:'Roboto',
    fontSize: 13,
    fontStyle: 'italic',
    alignSelf: 'center'
  },
  sendVText: {
    // fontFamily:'Roboto',
    color: 'gray',
    fontSize: 16,
    padding: 20,
    textAlign: 'center'
  }
})
