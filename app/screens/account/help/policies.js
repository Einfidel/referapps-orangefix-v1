import React from 'react'

import {
    View,
    Text
} from 'react-native'

import { 
  Container,
  Thumbnail,
  Body,
  Left,
  Right,
  Header
} 
from 'native-base'

import styles from '../../../styles/policies/policies'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Assets from '../../../components/assets.manager'

// import Header from '../../../../components/Header/Headers'
class Policies extends React.Component{
  render(){
    return(
      <Container style={{backgroundColor:'F7F5F6'}}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0}}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 1, justifyContent: 'center'}}>
            <Text style={styles.headerText}>ReferApps Policies</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{width: 20}}/>
          </Right>
        </Header>
        <View style={styles.textContainer}>
          <TouchableOpacity 
            onPress ={() => this.props.navigation.navigate('Privacy Policy')}
            style={styles.touchable}>
            <View>
                <Text style={styles.titleText}>Privacy Policy</Text>
            </View>
            <Thumbnail
                source={Assets.uploadProduct.subContentIcon}
                style={styles.iconSub}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.textContainer}>
          <TouchableOpacity 
            onPress ={() => this.props.navigation.navigate('Policy Sellers')}
            style={styles.touchable}>
            <View>
                <Text style={styles.titleText}>Delivery Shipping Policy Sellers</Text>
            </View>
            <Thumbnail
              source={Assets.uploadProduct.subContentIcon}
              style={styles.iconSub}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.textContainer}>
          <TouchableOpacity 
            onPress ={() => this.props.navigation.navigate('Policy Buyers')}
            style={styles.touchable}>
            <View>
              <Text style={styles.titleText}>Delivery Shipping Policy Buyers</Text>
            </View>
            <Thumbnail
              source={Assets.uploadProduct.subContentIcon}
              style={styles.iconSub}
            />
          </TouchableOpacity>
        </View>
      </Container>
    )
  }
}
export default Policies;