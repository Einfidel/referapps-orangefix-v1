import React from 'react';

import {
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  TextInput,
  Image,
  Modal,
  Dimensions,
  SafeAreaView,
} from 'react-native';

import {
  Header,
  Left,
  Body,
  Right,
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Title,
  Thumbnail
} from 'native-base';
import FastImage from 'react-native-fast-image';

import styles from '../../../styles/help_center/help_center';
import Assets from '../../../components/assets.manager'

const { height } = Dimensions.get('window')

class HelpCenterScreen extends React.Component {

  static navigationOptions = {
    Header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      search: '',
      Modal1: false,
      Modal2: false,
      Modal3: false,
      Modal4: false,
      Modal5: false,
      Modal6: false,
      Modal7: false,
      Modal8: false,
      Modal9: false,
      Modal10: false,
      Modal11: false,
      Modal12: false,
      Modal13: false,
      Modal14: false,
      Modal15: false,
      Modal16: false,
      Modal17: false,
      Modal18: false,
    }
  }

  ShowModal1(visible) {
    this.setState({ Modal1: visible });
  }

  ShowModal2(visible) {
    this.setState({ Modal2: visible });
  }

  ShowModal3(visible) {
    this.setState({ Modal3: visible });
  }

  ShowModal4(visible) {
    this.setState({ Modal4: visible });
  }

  ShowModal5(visible) {
    this.setState({ Modal5: visible });
  }

  ShowModal6(visible) {
    this.setState({ Modal6: visible });
  }

  ShowModal7(visible) {
    this.setState({ Modal7: visible });
  }

  ShowModal8(visible) {
    this.setState({ Modal8: visible });
  }

  ShowModal3(visible) {
    this.setState({ Modal3: visible });
  }

  ShowModal9(visible) {
    this.setState({ Modal9: visible });
  }

  ShowModal10(visible) {
    this.setState({ Modal10: visible });
  }

  ShowModal11(visible) {
    this.setState({ Modal11: visible });
  }

  ShowModal12(visible) {
    this.setState({ Modal12: visible });
  }

  ShowModal13(visible) {
    this.setState({ Modal13: visible });
  }

  ShowModal14(visible) {
    this.setState({ Modal14: visible });
  }

  ShowModal15(visible) {
    this.setState({ Modal15: visible });
  }

  ShowModal16(visible) {
    this.setState({ Modal16: visible });
  }

  ShowModal17(visible) {
    this.setState({ Modal17: visible });
  }

  ShowModal18(visible) {
    this.setState({ Modal18: visible });
  }


  render() {
    return (
      <Container style={{ height }}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={styles.headerText}>Help Centre</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{ width: 20 }} />
          </Right>
        </Header>
        <Content style={{ backgroundColor: '#ecf0f1' }}>

          <View style={styles.space20} />

          <View style={styles.searchContainer}>
            <Item style={{ borderColor: '#ffffff', paddingLeft: 5 }} last>
              <FastImage
                source={Assets.accountSettings.searchHelpcenter}
                square
                small
                style={styles.searchIcon}
              />
              <Input
                placeholder='search'
                style={styles.searchText}
                onChangeText={(search) => this.setState({ search })}
              />
            </Item>
          </View>

          <View style={styles.space15} />

          <View style={styles.textContainer}>
            <Text style={styles.titleText}>MOST ASKED QUESTIONS</Text>
          </View>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal1(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>What is ReferApps</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal2(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>What is the Refer Program</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal3(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Can I log in to my account using my Facebook login details</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal4(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>How and when I will get paid</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>


          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal5(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Will I still get commission even if my friend canceled the order of the product I reffered</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal6(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>How do I register/log in to ReferApps</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal7(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Why can't I change my password</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal8(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Does ReferApps have Seller Fees</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal9(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>What is ReferCash</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal10(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Will my ReferCash Expire</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal11(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>How do I change the information in my account</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal12(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>What should I do if I can't log in to my account</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal13(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>How to create an item listing</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal14(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>How will I know when my product sells</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal15(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Can I sell outside my country</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal16(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Can I sell my own artwork on ReferApps</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal17(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Anti-spam policy</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.space} />
          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => { this.ShowModal18(true) }}
            style={[styles.textContainer, { backgroundColor: '#ffffff' }]}
          >
            <Text style={[styles.contentText, { color: '#231f20', width: '90%' }]}>Prohibited Products</Text>
            <FastImage
              source={Assets.uploadProduct.subContentIcon}
              style={[styles.openIcon]}
              tintColor='#7f8c8d'
            />
          </TouchableOpacity>

          <View style={styles.border} />
          <View style={styles.space20} />
          <View style={styles.space20} />

          <View style={styles.textContainer}>
            <Text style={styles.titleText}>For more questions</Text>
          </View>

          <View style={[styles.textContainer, { backgroundColor: '#ffffff' }]}>
            <View style={styles.viewMore}>
              <FastImage
                source={Assets.accountSettings.emailIcon}
                style={styles.emailIcon}
                resizeMode={FastImage.resizeMode.contain}
                tintColor='black'
              />
              <View style={styles.emailView}>
                <Text style={styles.emailUs}>Email Us</Text>
                <Text style={styles.emailText}>support@referapps.com</Text>
              </View>
            </View>
            <View style={{ flex: 1 }} />
          </View>

          <View style={styles.border} />
          <View style={styles.space20} />
          <View style={styles.space20} />

          <View style={[styles.textContainer, { backgroundColor: '#ffffff' }]}>
            <View style={styles.viewMore}>
              <FastImage
                source={Assets.accountSettings.phoneIcon}
                style={styles.phoneIcon}
                resizeMode={FastImage.resizeMode.contain}
                tintColor='black'
              />
              <View style={styles.emailView}>
                <Text style={styles.emailUs}>Call Us</Text>
                <Text style={styles.emailText}>Phone: US : +1 (212) 844-9600, +1 (845) 345-6782 - PH : Globe +63 (967) 279-4324, Smart +63 (928) 952-4353</Text>
              </View>
            </View>
            <View style={{ flex: 1 }} />
          </View>

          <View style={styles.border} />
          <View style={styles.space20} />
          <View style={styles.space5} />
          <View style={styles.space5} />

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal1}
              onRequestClose={() => { this.ShowModal1(!this.state.Modal1) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal1(!this.state.Modal1) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View >
                    <Text style={styles.ModalHeaderTitle}>What is ReferApps</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      ReferApps is the new selling revolution that decentralizes profit and putting people back into the economy's equation.
                      It's a marketplace where everyone can make a profit, whether you are a buyer or a seller.
                      Anyone who refers a friend will receive a commission for every product sold.
                      As for the sellers, you can post your product free of charge, with your own website and logistics.</Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal2}
              onRequestClose={() => { this.ShowModal2(!this.state.Modal2) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal2(!this.state.Modal2) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>What is the Refer Program</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      Refer a product or service to your friends and you will receive a commission fee when the product is sold.
                      Every time your friend buys that product or service, you will automatically receive the given commission percent of the amount of the product or service you referred.
                      The higher the price of the product or service is, the bigger the commission will be.</Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal3}
              onRequestClose={() => { this.ShowModal3(!this.state.Modal3) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal3(!this.state.Modal3) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Can I log in to my account using my Facebook login details</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      We recommend you to log in to your account using your Facebook account as it’s easier for you to refer a product to your friends.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal4}
              onRequestClose={() => { this.ShowModal4(!this.state.Modal4) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal4(!this.state.Modal4) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>How and when will I get paid</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      If you are a Seller, you will get paid once the item has been delivered and the buyer confirmed that they received the product.
                      If you are a Referrer, you will receive your commission once the product has been sold and the sale had taken place.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal5}
              onRequestClose={() => { this.ShowModal5(!this.state.Modal5) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal5(!this.state.Modal5) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Will I still get commission even if my friend canceled the order of the product I referred</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      You will only receive the commission if the order has been fulfilled.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal6}
              onRequestClose={() => { this.ShowModal6(!this.state.Modal6) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal6(!this.state.Modal6) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>How do I register/log in to ReferApps</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      To register, you need to fill out the necessary fields and click "Submit".
                      Be sure that the information you submit is accurate and up to date.
                      You can log in using your Facebook login details or use your authorized email to register.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal7}
              onRequestClose={() => { this.ShowModal7(!this.state.Modal7) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal7(!this.state.Modal7) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Why can’t I change my password</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      The possible reason why you cannot change your password is that you did not confirm your email when we sent you a confirmation email before changing your password.
                      Make sure you check your email for notifications and confirmations.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal8}
              onRequestClose={() => { this.ShowModal8(!this.state.Modal8) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal8(!this.state.Modal8) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Does ReferApps have seller fees</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      ReferApps do not require any fees (for example, listing fees and seller fees).
                      The only fee we require is when you encash your ReferCash on your bank for the transaction fee.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal9}
              onRequestClose={() => { this.ShowModal9(!this.state.Modal9) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal9(!this.state.Modal9) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>What is ReferCash</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>ReferCash is the commission you received from referring a product.
                      You can use ReferCash to purchase another product or encash it in the bank.
                      You can only withdraw your ReferCash when it reached higher than or equal to $20 or equivalent currencies.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal10}
              onRequestClose={() => { this.ShowModal10(!this.state.Modal10) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal10(!this.state.Modal10) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Will my ReferCash expire</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>ReferCash will not expire. It's up to you if you want to encash it or not.</Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal11}
              onRequestClose={() => { this.ShowModal11(!this.state.Modal11) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal11(!this.state.Modal11) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>How do I change the information in my account</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>To change the information in your account, log in to your account and go to “Account”.</Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal12}
              onRequestClose={() => { this.ShowModal12(!this.state.Modal12) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal12(!this.state.Modal12) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>What should I do if I can't log in to my account</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      If you cannot log in to your account, you can change your password by clicking "Forgot Password".
                      We will send a link to your email address that will allow you to create your new password.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal13}
              onRequestClose={() => { this.ShowModal13(!this.state.Modal13) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal13(!this.state.Modal13) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>How to create an item listing</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      1. To create an item listing, go to this icon (icon), then click My Shop. {"\n"} {"\n"}
                      2. Click the "Choose Category" to specify your product. {"\n"} {"\n"}
                      3. Click the Sell New Product, and fill out the necessary fields.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal14}
              onRequestClose={() => { this.ShowModal14(!this.state.Modal14) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal14(!this.state.Modal14) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>How will I know when my product sells</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>You will receive a notification through your registered email if your product is sold.</Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal15}
              onRequestClose={() => { this.ShowModal15(!this.state.Modal15) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal15(!this.state.Modal15) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Can I sell outside my country</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      Every ReferApps account is country-specific, which means you will not be able to sell your product outside of your country. Upon registration, you must specify your country.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"slide"}
              visible={this.state.Modal16}
              onRequestClose={() => { this.ShowModal16(!this.state.Modal16) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPressOut={() => { this.ShowModal16(!this.state.Modal16) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.ModalInsideView}>
                  <View>
                    <Text style={styles.ModalHeaderTitle}>Can I sell my own artwork on ReferApps</Text>
                  </View>
                  <View style={styles.ModalAnswer}>
                    <Text style={styles.TextStyle}>
                      You can sell your original artwork and other artwork that you're authorized to resell on ReferApps.
                    </Text>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"fade"}
              visible={this.state.Modal17}
              onRequestClose={() => { this.ShowModal17(!this.state.Modal17) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { this.ShowModal17(!this.state.Modal17) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.LongModalInsideView}>
                  <View onStartShouldSetResponder={() => true}>
                    <ScrollView>
                      <Text style={styles.LongModalHeaderTitle}>Anti-spam policy</Text>
                      <View style={styles.LongModalAnswer}>
                        <Text style={styles.TextStyle}>
                          1. Introduction {"\n"} {"\n"}
                          1 In the context of electronic messaging, &quot;spam&quot; means [unsolicited, bulk or {"\n"} {"\n"}
                          indiscriminate messages, typically sent for a commercial purpose]. {"\n"} {"\n"}
                          We have a zero-tolerance spam policy. {"\n"} {"\n"}
                          2. Spam filtering {"\n"} {"\n"}
                          Our messaging systems automatically scan all incoming [email and other] {"\n"} {"\n"}
                          messages and filter out messages that appear to be spam. {"\n"} {"\n"}
                          We may also report incoming email as spam. This can result in IP addresses {"\n"} {"\n"}
                          and domain names being blacklisted.{"\n"} {"\n"}
                          3. Problems with spam filtering {"\n"} {"\n"}
                          No message filtering system is 100% accurate, and from time to time {"\n"} {"\n"}
                          legitimate messages will be filtered out by our systems. {"\n"} {"\n"}
                          If you believe that a legitimate message you have sent has been filtered out {"\n"} {"\n"}
                          by our systems, please advise the message recipient by another means. {"\n"} {"\n"}
                          You can reduce the risk of a message being caught by the spam filters by: {"\n"} {"\n"}
                          (a) sending the message in plain text (instead of, or in addition to, HTML); {"\n"} {"\n"}
                          (b) removing any message attachments; {"\n"} {"\n"}
                          (c) avoiding the terminology and text styling typically used by spammers; {"\n"} {"\n"}
                          and/or {"\n"} {"\n"}
                          (d) ensuring that your messages are scanned for malware before dispatch. {"\n"} {"\n"}
                          4. User spam {"\n"} {"\n"}
                          ReferApps provides a facility that enables users to send [email messages / {"\n"} {"\n"}
                          private messages] to others. Users must not use our messaging facility or {"\n"} {"\n"}
                          any of our other services to store, copy, send, relay or distribute spam. {"\n"} {"\n"}
                          Full terms and conditions concerning the use of our messaging facility are set {"\n"} {"\n"}
                          out in [our website terms and conditions of use]. {"\n"} {"\n"}
                          5. Receipt of unwanted messages from us {"\n"} {"\n"}
                          In the unlikely event that you receive any message from us or sent using our {"\n"} {"\n"}
                          systems that may be considered to be spam, please contact us using the {"\n"} {"\n"}
                          details below and the matter will be investigated.{"\n"} {"\n"}
                          6. Variation {"\n"} {"\n"}
                          We may amend this policy at any time by publishing a new version on our {"\n"} {"\n"}
                          website. {"\n"} {"\n"}
                          7. Our details {"\n"} {"\n"}
                          8.1 This website is owned and operated by [name]. {"\n"} {"\n"}
                          8.2 Our principal place of business is at [address]. {"\n"} {"\n"}
                          8.3 You can contact us: {"\n"} {"\n"}
                          (a) (by post, to [the postal address given above)); {"\n"} {"\n"}
                          (b) (using our website contact form); {"\n"} {"\n"}
                          (c) (by telephone, on [the contact number published on our website from {"\n"} {"\n"}
                          time to time)); or {"\n"} {"\n"}
                          (d) (by email, using [the email address published on our website from time {"\n"} {"\n"}
                          to time)). {"\n"} {"\n"}
                        </Text>
                      </View>
                    </ScrollView>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>

          <View style={styles.MainContainer}>
            <Modal
              transparent={true}
              animationType={"fade"}
              visible={this.state.Modal18}
              onRequestClose={() => { this.ShowModal18(!this.state.Modal18) }}
            >
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => { this.ShowModal18(!this.state.Modal18) }}
                style={styles.MainView}
              >
                <TouchableOpacity activeOpacity={1} style={styles.LongModalInsideView}>
                  <View onStartShouldSetResponder={() => true}>
                    <ScrollView>
                      <Text style={styles.LongModalHeaderTitle}>Prohibited Products</Text>
                      <View style={styles.LongModalAnswer}>
                        <Text style={styles.TextStyle}>
                          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>List of Prohibited and Restricted Items {"\n"} {"\n"} {"\n"}</Text>
                          Though you can sell any items on ReferApps, some categories of products are restricted or banned from being listed. Here are the items that are not allowed to be in a seller's listings. {"\n"} {"\n"}
                          NOTE: If you supply products for sale on ReferApps, you must comply with all federal, state, and local laws and ReferApps policies applicable to those products and product listings. {"\n"} {"\n"} {"\n"}
                          Items that are Prohibited and Restricted {"\n"} {"\n"} {"\n"}
                          Adult Items {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Prophylactics and other over-the-counter contraceptives (pre-approval not required){"\n"} {"\n"}
                          {"\t"}● Unrated erotic videos and DVDs {"\n"} {"\n"}
                          {"\t"}● Magazines widely available in non-adult-only bookstores {"\n"} {"\n"}
                          {"\t"}● Sexual health products listed by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}● Sexual aid devices listed by pre-approved sellers {"\n"} {"\n"}
                          ● Adult-only novelty items listed by pre-approved sellers, such as:{"\n"} {"\n"}
                          {"\t"}● Novelty food items {"\n"} {"\n"}
                          {"\t"}● Soaps {"\n"} {"\n"}
                          {"\t"}● Vibrators {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"} {"\n"}
                          ● Products that portray nudity in a gratuitous or graphic manner, such as: {"\n"} {"\n"} {"\n"}
                          {"\t"}● Amateur pornography{"\n"} {"\n"}
                          {"\t"}● Pornography {"\n"} {"\n"}
                          {"\t"}● X-rated movies  {"\n"} {"\n"}
                          {"\t"}● Sexual health products unless listed by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}● Sexual aid devices unless listed by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}● Adult-only novelty items unless listed by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}● Media featuring Traci Lords created prior to May 7, 1986 {"\n"} {"\n"} {"\n"}
                          Alcohol {"\n"} {"\n"} {"\n"}
                          Allowed in Listings {"\n"} {"\n"} {"\n"}
                          {"\t"}● Wine sold by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}● Wine and beer making kits and products that do not contain alcohol {"\n"} {"\n"}
                          {"\t"}● Alcohol-related products and accessories (including corkscrews, decanters, and {"\n"} {"\n"}
                          {"\t"}containers) {"\n"} {"\n"}
                          {"\t"}● Alcohol-related memorabilia and collectibles that do not contain alcohol {"\n"} {"\n"}
                          {"\t"}● Foods with alcoholic ingredients and flavoring (such as liquor-filled chocolates, rum {"\n"} {"\n"}
                          {"\t"}cake, and other items) as long as the item is not included in the list of prohibited food {"\n"} {"\n"}
                          {"\t"}● Hops, seeds for hops, and other raw materials to make beer or wine {"\n"} {"\n"}
                          {"\t"}● Non-alcoholic or de-alcoholized beer and wine {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listings {"\n"} {"\n"}
                          {"\t"}● Alcoholic beverages (except pre-approved wine sellers) {"\n"} {"\n"}
                          {"\t"}● Liquor licenses {"\n"} {"\n"}
                          {"\t"}● Products that contain raw alcohol {"\n"} {"\n"}
                          {"\t"}● Any product marketed for customers over 21 years of age {"\n"} {"\n"} {"\n"}
                          Wine Buyers {"\n"} {"\n"}
                          Before a buyer can purchase wine on ReferApps from pre-approved sellers, they must first meet {"\n"} {"\n"}
                          the following requirements:{"\n"} {"\n"}
                          {"\t"}● Buyer should at least be 21 years old {"\n"} {"\n"}
                          {"\t"}● If the wine will be received on behalf of the buyer, the buyer must make sure that the {"\n"} {"\n"}
                          {"\t"}recipient is at least 21 years old {"\n"} {"\n"}
                          {"\t"}● Make sure that the transaction follows all interstate commerce laws, including all laws {"\n"} {"\n"}
                          {"\t"}around shipping and fulfillment {"\n"} {"\n"}
                          Buyers, by purchasing these items, authorize and represent that they are aware of and are in {"\n"} {"\n"}
                          compliance with their own state laws and regulations {"\n"} {"\n"}
                          State laws {"\n"} {"\n"}
                          Every state in the US has different laws for the sale of wine, especially when dealing with wine {"\n"} {"\n"}
                          sales across state borders. Members should fully research and understand federal and state {"\n"} {"\n"}
                          laws before listing wine. If you want to sell wine, you should also take the time to review state {"\n"} {"\n"}
                          shipping restrictions that are in the Requirements for wine-related transactions on ReferApps {"\n"} {"\n"}
                          section below. The Wine Institute is a great place to find more information on the laws for selling {"\n"} {"\n"}
                          wine. {"\n"} {"\n"} {"\n"}
                          Requirements for wine-related transactions on ReferApps {"\n"} {"\n"}
                          Be sure to meet all the following terms and conditions when buying or selling wine on {"\n"} {"\n"}
                          ReferApps. These terms and conditions may change over time. It is your responsibility to visit {"\n"} {"\n"}
                          these pages to see if and how it may have changed so that you can remain compliant. {"\n"} {"\n"}
                          1. Limitation of liability: Both buyer and seller expressly acknowledge and agree that {"\n"} {"\n"}
                          ReferApps shall not be liable for any special, incidental or consequential damages or lost {"\n"} {"\n"}
                          profits (however arising, including negligence) resulting from or in connection with the {"\n"} {"\n"}
                          sale of property on the ReferApps site. {"\n"} {"\n"}
                          2. Representations and warranties: {"\n"} {"\n"}
                          Each seller represents and warrants that: {"\n"} {"\n"}
                          {"\t"}● The seller of the wine has the right, power, and authority to sell the wine {"\n"} {"\n"}
                          {"\t"}● The owner of the wine owns all necessary and required licenses to lawfully carry out {"\n"} {"\n"}
                          {"\t"}wine-related transactions on our site, and ship the item to its purchaser {"\n"} {"\n"}
                          {"\t"}● The recipient (and buyer if different from the recipient) has proven he or she is 21 years {"\n"} {"\n"}
                          {"\t"}of age or older prior to delivery {"\n"} {"\n"}
                          {"\t"}● The transaction will comply with all interstate commerce laws, including all laws around {"\n"} {"\n"}
                          {"\t"}shipping and fulfillment {"\n"} {"\n"}
                          Each buyer represents and warrants that: {"\n"} {"\n"}
                          {"\t"}● The buyer of the wine is at least 21 years old {"\n"} {"\n"}
                          {"\t"}● The recipient of the wine is at least 21 years old {"\n"} {"\n"}
                          {"\t"}● The transaction will comply with all interstate commerce laws, including all laws around {"\n"} {"\n"}
                          {"\t"}shipping and fulfillment {"\n"} {"\n"}
                          3. Indemnification: The seller is solely responsible for compliance with all laws associated {"\n"} {"\n"}
                          with the sale of wine. The seller indemnifies ReferApps of all responsibility associated {"\n"} {"\n"}
                          with this sale. It is, therefore, the seller's sole discretion of the completion of the {"\n"} {"\n"}
                          transaction based on its ability to assure that the transaction is in compliance with all {"\n"} {"\n"}
                          laws associated with the sale of alcohol. {"\n"} {"\n"}
                          4. Shipping: Certain local, state, and federal laws and regulations govern and limit a {"\n"} {"\n"}
                          seller's ability to sell and ship wine and other alcohol. As a seller, you are solely {"\n"} {"\n"}
                          responsible to consult with legal and/or other experts to determine which laws and {"\n"} {"\n"}
                          regulations may impact your activities and to fully abide by those laws and regulations. {"\n"} {"\n"}
                          Since each state has its own regulations governing the shipment of wine, the seller must {"\n"} {"\n"}
                          clearly note which states it is able to ship to in the item description. {"\n"} {"\n"}
                          5. Age: Wine shipped by seller requires the recipient to be in possession of photo {"\n"} {"\n"}
                          identification confirming they are 21 years of age or older. Buyer, if different from the {"\n"} {"\n"}
                          recipient, must also show proof of age. {"\n"} {"\n"}
                          Shipping Alcohol for Pre-Approved Sellers {"\n"} {"\n"}
                          There are carriers that do not allow the shipment of intoxicating liquors. For example, the US {"\n"} {"\n"}
                          Postal Service doesn't allow intoxicating liquors that contain 0.5% or more alcohol by content. {"\n"} {"\n"}
                          Though there are carriers such as FedEx and UPS may allow these items to be shipped, but {"\n"} {"\n"}
                          you should check their rules and regulations before offering their shipping services in your {"\n"} {"\n"}
                          listing. {"\n"} {"\n"} {"\n"}
                          Animals & Animal Products {"\n"} {"\n"} {"\n"}
                          Allowed in Listings {"\n"} {"\n"}
                          {"\t"}● Live shellfish and crustaceans {"\n"} {"\n"}
                          {"\t"}● Live insects and worms used for agricultural purposes, bait, or pet food {"\n"} {"\n"}
                          {"\t"}● Products made to resemble prohibited animal parts or products, but which are not made {"\n"} {"\n"}
                          {"\t"}from those animals. If the listing includes a clear title and description that the item is not {"\n"} {"\n"}
                          {"\t"}genuine; for example, faux tortoise shell is permitted if the listing states the item is fake {"\n"} {"\n"}
                          {"\t"}or artificial. {"\n"} {"\n"}
                          {"\t"}● Animal parts or products that are not otherwise restricted, such as: {"\n"} {"\n"}
                          {"\t"}○ Shark teeth jewelry {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Animal feces {"\n"} {"\n"}
                          {"\t"}● Most live creatures, such as pets, livestock, or marine mammals {"\n"} {"\n"}
                          {"\t"}● Illegal wildlife products {"\n"} {"\n"}
                          {"\t"}● Fish or wildlife that are taken, possessed, transported, or sold in violation of the Lacey {"\n"} {"\n"}
                          Act {"\n"} {"\n"}
                          {"\t"}● Insects or other creatures designated as "plant pests" {"\n"} {"\n"}
                          {"\t"}● Parts or products, including fur and feathers, from federally endangered or threatened {"\n"} {"\n"}
                          species, such as: {"\n"} {"\n"}
                          {"\t"}○ Cheetah {"\n"} {"\n"}
                          {"\t"}○ Jaguar {"\n"} {"\n"}
                          {"\t"}○ Leopard {"\n"} {"\n"}
                          {"\t"}○ Mountain zebra and Hartmann mountain zebra {"\n"} {"\n"}
                          {"\t"}○ Sable antelope {"\n"} {"\n"}
                          {"\t"}○ Tiger {"\n"} {"\n"}
                          {"\t"}○ Polar bear {"\n"} {"\n"}
                          {"\t"}○ Ocelot {"\n"} {"\n"}
                          {"\t"}○ Wolf {"\n"} {"\n"}
                          {"\t"}○ Indian python {"\n"} {"\n"}
                          {"\t"}○ Crocodile {"\n"} {"\n"}
                          {"\t"}○ Alligator {"\n"} {"\n"}
                          {"\t"}○ Sea turtle {"\n"} {"\n"}
                          {"\t"}○ Colobus monkey {"\n"} {"\n"}
                          {"\t"}○ Vicuna {"\n"} {"\n"}
                          {"\t"}○ Spanish lynx {"\n"} {"\n"}
                          {"\t"}○ Elephant {"\n"} {"\n"}
                          {"\t"}○ Pangolin {"\n"} {"\n"}
                          {"\t"}○ Sea otter {"\n"} {"\n"}
                          {"\t"}● Parts or products from bear or mountain lion; such as: {"\n"} {"\n"}
                          {"\t"}○ Bear Bile {"\n"} {"\n"} {"\n"}
                          {"\t"}● Parts or products from cat or dog {"\n"} {"\n"}
                          {"\t"}● Products made from the skin of seals {"\n"} {"\n"}
                          {"\t"}● Products prohibited for sale under the Migratory Bird Treaty Act of 1918 {"\n"} {"\n"}
                          {"\t"}● Products containing ivory from animals {"\n"} {"\n"}
                          {"\t"}● Vaccines (including human and animal vaccines), such as: {"\n"} {"\n"}
                          {"\t"}○ Rabies {"\n"} {"\n"}
                          {"\t"}○ West Nile Virus {"\n"} {"\n"}
                          {"\t"}○ Distemper {"\n"} {"\n"}
                          {"\t"}○ Puppy CPV {"\n"} {"\n"}
                          {"\t"}○ Lyme Disease {"\n"} {"\n"}
                          {"\t"}● Products that contain parts or ingredients derived from sharks, whales, dolphins, or {"\n"} {"\n"}
                          {"\t"}porpoises (with the exception of teeth, which are permitted), such as: {"\n"} {"\n"}
                          {"\t"}○ Joint pain supplements containing shark cartilage {"\n"} {"\n"}
                          {"\t"}○ Shark fin products {"\n"} {"\n"}
                          {"\t"}○ Shark liver oil {"\n"} {"\n"}
                          {"\t"}○ Whale meat {"\n"} {"\n"}
                          {"\t"}● Products that contain foie gras may not be shipped to addresses within the State of {"\n"} {"\n"}
                          {"\t"}California. {"\n"} {"\n"}
                          {"\t"}● Parts or products made from certain species listed below may not be shipped to {"\n"} {"\n"}
                          {"\t"}addresses within the State of California: {"\n"} {"\n"}
                          {"\t"}○ Cobra {"\n"} {"\n"}
                          {"\t"}○ Python {"\n"} {"\n"}
                          {"\t"}○ Kangaroo {"\n"} {"\n"}
                          {"\t"}○ Feral horse {"\n"} {"\n"}
                          {"\t"}○ Porpoise {"\n"} {"\n"}
                          Art Selling  {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Selling your own original work {"\n"} {"\n"}
                          {"\t"}● Selling exact replicas or reproductions of artwork if the listing includes the word {"\n"} {"\n"}
                          {"\t"}"reproduction" in the title and the description {"\n"} {"\n"}
                          {"\t"}● Genuine artwork for which the seller has and can provide evidence of authenticity {"\n"} {"\n"}
                          {"\t"}● Artwork for which the seller clearly and prominently discloses its condition, alterations, {"\n"} {"\n"}
                          {"\t"}and conservation or repairs in the listing description {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"} {"\n"}
                          {"\t"}● Knowingly selling, exhibiting, trading, producing or advertising any unauthorized copy or {"\n"} {"\n"}
                          {"\t"}reproduction of any work of art {"\n"} {"\n"}
                          {"\t"}● Disclaiming knowledge of, or responsibility for, the authenticity or legality of an item that {"\n"} {"\n"}
                          {"\t"}you describe as genuine {"\n"} {"\n"}
                          {"\t"}● Describing or marketing any item as genuine if you cannot provide reasonable evidence {"\n"} {"\n"}
                          {"\t"}to prove its authenticity {"\n"} {"\n"}
                          {"\t"}● Knowingly participating in any way in the advertisement or sale of any work of art using {"\n"} {"\n"}
                          {"\t"}any deceptive practices, including, but not limited to, false or misleading claims of the {"\n"} {"\n"}
                          {"\t"}item's scarcity, value, provenance, condition or investment potential {"\n"} {"\n"}
                          {"\t"}● Unauthorized copies or reproductions of artwork that violate any copyright or trademark {"\n"} {"\n"}
                          {"\t"}NOTE: You must abide by all laws relating to the sales of works of art and assist in the {"\n"} {"\n"}
                          {"\t"}prosecution of violators of the law in this respect {"\n"} {"\n"} {"\n"}
                          Artifacts, cultural heritage, and grave-related items {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Items that are not authentic {"\n"} {"\n"}
                          {"\t"}● Items without provenance or ownership history of the object {"\n"} {"\n"}
                          {"\t"}● Items that are not approved for import or export {"\n"} {"\n"}
                          {"\t"}● Looted or stolen goods. The International Council of Museums' Red List identify {"\n"} {"\n"}
                          {"\t"}categories of cultural goods most vulnerable to illicit traffic {"\n"} {"\n"}
                          {"\t"}● Authentic artifacts, fossils, and relics that are not clearly stated it was a reproduction {"\n"} {"\n"}
                          {"\t"}● Artifacts, fossils, or relics taken from any of the following places: {"\n"} {"\n"}
                          {"\t"}○ Federal or state public land {"\n"} {"\n"}
                          {"\t"}○ Native American land or battlefield {"\n"} {"\n"}
                          {"\t"}● Listings for government survey markers (like those used by the U.S. Geological Survey) {"\n"} {"\n"}
                          {"\t"}unless have proof of transferred ownership to the seller. This proof can be either a photo {"\n"} {"\n"}
                          {"\t"}or a scanned image of the document {"\n"} {"\n"}
                          {"\t"}● Speleothems, stalactites, and stalagmites took from caves on federal land {"\n"} {"\n"}
                          {"\t"}● Describing items in the following terms because they make it hard for buyers to find {"\n"} {"\n"}
                          {"\t"}authentic versions: {"\n"} {"\n"}
                          {"\t"}○ Alaska Native style {"\n"} {"\n"}
                          {"\t"}○ American Indian style {"\n"} {"\n"}
                          {"\t"}○ Native American style {"\n"} {"\n"}
                          {"\t"}○ Other descriptions that may suggest the item was made by a Native American {"\n"} {"\n"}
                          {"\t"}● Items not made by Native Americans can't be listed in Native American categories {"\n"} {"\n"}
                          {"\t"}● Unknown artisan or tribal affiliation for arts and crafts made in 1935 or later can't be {"\n"} {"\n"}
                          {"\t"}described as Alaska Native, American Indian, or Native American {"\n"} {"\n"}
                          {"\t"}● Items from Native American or Native Hawaiian grave sites, such as: {"\n"} {"\n"} {"\n"}
                          {"\t"}○ A funerary object placed with the dead {"\n"} {"\n"}
                          {"\t"}○ Grave marker {"\n"} {"\n"}
                          {"\t"}○ Human remains {"\n"} {"\n"}
                          {"\t"}● Sacred items used by Native American religious leaders in ceremonial practices, such {"\n"} {"\n"}
                          as: {"\n"} {"\n"}
                          {"\t"}○ Ceremonial Masks {"\n"} {"\n"}
                          {"\t"}○ Prayer sticks {"\n"} {"\n"}
                          {"\t"}● Items listed in the Non-Native American Crafts category described as: {"\n"} {"\n"}
                          {"\t"}○ Alaska Native style {"\n"} {"\n"}
                          {"\t"}○ American Indian style {"\n"} {"\n"}
                          {"\t"}○ Native American reproduction {"\n"} {"\n"}
                          {"\t"}○ Native American style {"\n"} {"\n"}
                          {"\t"}○ Other descriptions that may suggest the item was made by a Native American {"\n"} {"\n"}
                          {"\t"}● Items that were taken from historical grave sites like battlefields, protected lands, or {"\n"} {"\n"}
                          {"\t"}shipwrecks {"\n"} {"\n"}
                          {"\t"}● Used funerary items like headstones, markers, or urns {"\n"} {"\n"}
                          {"\t"}● Items listed in the Non-Native American Crafts category described as: {"\n"} {"\n"}
                          {"\t"}○ Alaska Native style {"\n"} {"\n"}
                          {"\t"}○ American Indian style {"\n"} {"\n"}
                          {"\t"}○ Native American reproduction {"\n"} {"\n"}
                          {"\t"}○ Native American style {"\n"} {"\n"}
                          {"\t"}○ Other descriptions that may suggest the item was made by a Native American {"\n"} {"\n"}
                          {"\t"}● Items that were taken from historical grave sites like battlefields, protected lands, or {"\n"} {"\n"}
                          {"\t"}shipwrecks {"\n"} {"\n"} {"\n"}
                          {"\t"}● Used funerary items like headstones, markers, or urns {"\n"} {"\n"} {"\n"}
                          {"\t"}Laws {"\n"} {"\n"}
                          Any restriction or ban on selling these items is generally based upon various laws that we need {"\n"} {"\n"}
                          to follow, including: {"\n"} {"\n"}
                          {"\t"}● The Indian Arts and Crafts Act {"\n"} {"\n"}
                          {"\t"}● The Native American Graves Protection and Repatriation Act {"\n"} {"\n"}
                          {"\t"}● The Federal Cave Protection Act {"\n"} {"\n"} {"\n"}
                          This policy also reflects regulations that have been set by various government agencies in the {"\n"} {"\n"}
                          U.S, including the U.S. Bureau of Indian Affairs (BIA), the Federal Bureau of Investigation (FBI), {"\n"} {"\n"}
                          the U.S. Department of Agriculture (USDA), and the U.S. Department of the Interior (DOI). {"\n"} {"\n"}
                          For more information on illicit traffic of cultural heritage, please visit the UNESCO Database of {"\n"} {"\n"}
                          National Cultural Heritage Laws. {"\n"} {"\n"}
                          As part of the fight against the traffic of stolen works of art, Interpol encourages not only law {"\n"} {"\n"}
                          enforcement agencies, but also art and antique dealers and owners of works of art to play an {"\n"} {"\n"}
                          active role in the exchange of information about stolen works of art. You can find more {"\n"} {"\n"}
                          information and resources on the Works of Art section of the Interpol website. {"\n"} {"\n"}
                          Make sure your listing follows these guidelines. If it doesn't, it may be removed, and you may be {"\n"} {"\n"}
                          subject to a range of other actions, including restrictions of your buying and selling privileges {"\n"} {"\n"}
                          and suspension of your account. {"\n"} {"\n"} {"\n"}
                          Autographed items {"\n"} {"\n"}
                          Autographs are commonly sold with Certificates of Authenticity (COA) or letters of authenticity {"\n"} {"\n"}
                          (LOAs). COAs and LOAs can offer assurance that an autograph is genuine, but you need to be {"\n"} {"\n"}
                          sure the authenticator is reputable. Before listing an autograph item, be sure to include the{"\n"} {"\n"}
                          following information in your listing: {"\n"} {"\n"}
                          {"\t"}● A clear photo of the actual autographed item {"\n"} {"\n"}
                          {"\t"}● A clear photo of the COA or LOA {"\n"} {"\n"} {"\n"}
                          {"\t"}● Information about the COA or LOA, including the name of the person or company {"\n"} {"\n"}
                          issuing the certificate {"\n"} {"\n"}
                          {"\t"}● Refund policy, in case a reputable dealer informs us or the buyer that the item is "likely {"\n"} {"\n"}
                          not authentic" {"\n"} {"\n"}
                          {"\t"}● Authentic and authorized pre-printed autographs or autographed items should generally {"\n"} {"\n"}
                          be listed in the Autographs-Reprints category. An authorized pre-printed autograph {"\n"} {"\n"}
                          means a company has acquired the rights to accurately reproduce the originally signed {"\n"} {"\n"}
                          item {"\n"} {"\n"} {"\n"}
                          Not Allowed in the Listing {"\n"} {"\n"}
                          {"\t"}● Autographed items with COAs and LOAs, or references to COAs and LOAs from the {"\n"} {"\n"}
                          following people or organizations: {"\n"} {"\n"}
                          {"\t"}● ACE Authentic {"\n"} {"\n"}
                          {"\t"}● Coach's Corner Sports Auctions LLC {"\n"} {"\n"}
                          {"\t"}● Christopher L. Morales {"\n"} {"\n"}
                          {"\t"}● CSC Collectibles {"\n"} {"\n"}
                          {"\t"}● Donald Frangipani {"\n"} {"\n"}
                          {"\t"}● Forensic Document Services {"\n"} {"\n"}
                          {"\t"}● Hollywood Dreams {"\n"} {"\n"}
                          {"\t"}● J. DiMaggio Co. / J. DiMaggio Company {"\n"} {"\n"}
                          {"\t"}● Legends Sports Memorabilia {"\n"} {"\n"}
                          {"\t"}● Nathan's Autographs / N.E. Autographs {"\n"} {"\n"}
                          {"\t"}● Nicholas Burczyk {"\n"} {"\n"}
                          {"\t"}● Pro Sports / Pro Sports Memorabilia {"\n"} {"\n"}
                          {"\t"}● Rare and Signed.com {"\n"} {"\n"}
                          {"\t"}● Robert Prouty {"\n"} {"\n"} {"\n"}
                          {"\t"}● R.R.'s Sports Cards & Collectibles {"\n"} {"\n"}
                          {"\t"}● SCAA / Front Page Art / Angelo Marino {"\n"} {"\n"}
                          {"\t"}● Slamdunk Sportscards & Memorabilia {"\n"} {"\n"}
                          {"\t"}● Sports Alley Memorabilia {"\n"} {"\n"}
                          {"\t"}● Sports Management Group {"\n"} {"\n"}
                          {"\t"}● Stan's Sports / Stans Sports Memorabilia {"\n"} {"\n"}
                          {"\t"}● TTA Authentic (formerly STAT Authentic) {"\n"} {"\n"}
                          {"\t"}● Universal Memorabilia {"\n"} {"\n"}
                          {"\t"}● USA Authentics {"\n"} {"\n"}
                          {"\t"}● Blank COAs and LOAs {"\n"} {"\n"}
                          {"\t"}● COAs and LOAs as stand-alone items {"\n"} {"\n"}
                          {"\t"}● COAs and LOAs from anyone listed on the FBI's Operation Bullpen website {"\n"} {"\n"}
                          {"\t"}● Unauthorized reprints of trading cards that have had authorized reproductions created, {"\n"} {"\n"}
                          such as unauthorized reprints of the: {"\n"} {"\n"}
                          {"\t"}● 1909 Cy Young (T206) {"\n"} {"\n"}
                          {"\t"}● 1909 Eddie Plank error card (T206) {"\n"} {"\n"}
                          {"\t"}● 1909 Honus Wagner (T206) {"\n"} {"\n"}
                          {"\t"}● 1909 Ty Cobb (T206) {"\n"} {"\n"}
                          {"\t"}● 1933 Babe Ruth (Goudey #53, #80, #144, and #181) {"\n"} {"\n"}
                          {"\t"}● 1933 Lou Gehrig (Goudey #92 and #160) {"\n"} {"\n"}
                          {"\t"}● 1951 Mickey Mantle (Bowman #253) {"\n"} {"\n"}
                          {"\t"}● 1952 Mickey Mantle (Topps #311) {"\n"} {"\n"} {"\n"}
                          {"\t"}● 1966-1967 Bobby Orr rookie card (Topps #35) {"\n"} {"\n"}
                          {"\t"}● 1979-1980 Wayne Gretzky rookie card (O-Pee-Chee and Topps #18) {"\n"} {"\n"}
                          {"\t"}● 1986-1987 Michael Jordan rookie card (Fleer #57) {"\n"} {"\n"} {"\n"}
                          Automotive and Powersports {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● New catalytic converters that are certified to conform to the Clean Air Act standards {"\n"} {"\n"}
                          {"\t"}● New tires {"\n"} {"\n"}
                          {"\t"}● Automotive batteries {"\n"} {"\n"}
                          {"\t"}● Portable fuel containers certified by the Environmental Protection Agency (EPA) {"\n"} {"\n"}{"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Motor vehicles that are required to be registered {"\n"} {"\n"}
                          {"\t"}● Photo-blocker sprays or license plate covers designed to block detection by red light {"\n"} {"\n"}
                          cameras, toll both systems, police cameras, or other photodetection sources {"\n"} {"\n"}
                          {"\t"}● Products intended to defeat, bypass, or shut down emission control devices, including {"\n"} {"\n"}
                          oxygen simulators and CAT, DPF or EGR delete kits {"\n"} {"\n"}
                          {"\t"}● Products intended to override malfunction indicator lights {"\n"} {"\n"}
                          {"\t"}● High-intensity discharge (HID) conversion kits {"\n"} {"\n"}
                          {"\t"}● Used Tires {"\n"} {"\n"}
                          {"\t"}● Products intended to affect traffics signals {"\n"} {"\n"}
                          {"\t"}● Products designed to intentionally block, jam, or interfere with licensed or authorized {"\n"} {"\n"}
                          radio communications, such as: {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Cell jammers {"\n"} {"\n"}
                          {"\t"}○ GPS jammers {"\n"} {"\n"}
                          {"\t"}○ Laser jammers {"\n"} {"\n"}
                          {"\t"}○ PCS jammers {"\n"} {"\n"}
                          {"\t"}○ Radar jammers {"\n"} {"\n"}
                          {"\t"}○ Wi-Fi jammers {"\n"} {"\n"}
                          {"\t"}● Products intended to alter odometers {"\n"} {"\n"}
                          {"\t"}● Driver's licenses and other government-issued identification cards {"\n"} {"\n"}
                          {"\t"}● Vehicle Identification Number (VIN) plates {"\n"} {"\n"}
                          {"\t"}● In California: engines, aftermarket, performance, and add-on parts and gas cans that are {"\n"} {"\n"}
                          not compliant with California Air Resources Board requirements {"\n"} {"\n"}
                          {"\t"}● Vehicle airbags and airbag covers {"\n"} {"\n"}
                          {"\t"}● Vehicle airbag inflators and airbag components {"\n"} {"\n"}
                          {"\t"}● Motorcycle helmets that do not meet Federal Motor Vehicle Safety Standard (FMVSS) {"\n"} {"\n"}
                          218 {"\n"} {"\n"}
                          {"\t"}● Engine coolant or antifreeze products that contain more than 10 percent ethylene glycol {"\n"} {"\n"}
                          and do not contain a bittering agent {"\n"} {"\n"}
                          {"\t"}● Vehicle seat belts and seat belt components {"\n"} {"\n"}
                          {"\t"}● Wheel weights containing lead in certain jurisdictions {"\n"} {"\n"}{"\n"}
                          Chance listings {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that comply with EPA formaldehyde {"\n"} {"\n"}
                          emissions standards and labeling requirements. {"\n"} {"\n"}{"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that do not comply with EPA {"\n"} {"\n"}
                          formaldehyde emissions standards. {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that are not individually labeled in {"\n"} {"\n"}
                          accordance with EPA regulation. {"\n"} {"\n"}{"\n"}
                          Composite Wood Products {"\n"} {"\n"}{"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that comply with EPA formaldehyde {"\n"} {"\n"}
                          emissions standards and labeling requirements. {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that do not comply with EPA {"\n"} {"\n"}
                          formaldehyde emissions standards. {"\n"} {"\n"}
                          {"\t"}● Products containing regulated composite wood that are not individually labeled in {"\n"} {"\n"}
                          accordance with EPA regulation. {"\n"} {"\n"} {"\n"}
                          Cosmetics & Skin/Hair Care {"\n"} {"\n"}{"\n"}
                          Before listing cosmetics, the seller must comply with these checklists to ensure the credibility of {"\n"} {"\n"}
                          the product. {"\n"} {"\n"} {"\n"}
                          Packaging {"\n"} {"\n"}
                          {"\t"}● Cosmetics must be sealed in the original manufacturer’s packaging {"\n"} {"\n"}
                          {"\t"}● Cosmetics must be new and unused {"\n"} {"\n"} {"\n"}
                          {"\t"}● Supplements must clearly display identifying codes placed on the packaging by the {"\n"} {"\n"}
                          manufacturer or distributor, such as matrix codes, lot numbers, or serial numbers {"\n"} {"\n"} {"\n"}
                          Labeling {"\n"} {"\n"}
                          {"\t"}● Cosmetics must be labeled in English with the following information: {"\n"} {"\n"}
                          {"\t"}○ The name of the product {"\n"} {"\n"}
                          {"\t"}○ The purpose or use of the product (e.g. cleansing the body, reducing the {"\n"} {"\n"}
                          appearance of wrinkles, moisturizing the skin) {"\n"} {"\n"}
                          {"\t"}○ The amount of content of the cosmetic, in terms of weight, measure, count or a {"\n"} {"\n"}
                          combination (e.g. 30 oz, 5 mL, 10 pills, 5 lbs) {"\n"} {"\n"}
                          {"\t"}○ The ingredient list {"\n"} {"\n"}
                          {"\t"}○ The name and address of the manufacturer, packer, or distributor {"\n"} {"\n"}
                          {"\t"}○ Any necessary label warnings {"\n"} {"\n"}
                          {"\t"}● Cosmetic labels must not state that the products cure, mitigate, treat, or prevent a {"\n"} {"\n"}
                          disease in humans unless that statement is approved by the FDA {"\n"} {"\n"}
                          {"\t"}● Cosmetic labels must not state that the cosmetics are “FDA approved” if they are not {"\n"} {"\n"}
                          FDA approved {"\n"} {"\n"}
                          {"\t"}● Cosmetic labels must not use the FDA logo {"\n"} {"\n"}
                          {"\t"}● Cosmetic labels should not state “tester,” “not for retail sale,” or “not intended for resale” {"\n"} {"\n"}
                          as such items may not be sold on ReferApps {"\n"} {"\n"} {"\n"}
                          Detail Page {"\n"} {"\n"}
                          {"\t"}● Detail pages must include the following information: {"\n"} {"\n"}
                          {"\t"}○ The name of the product {"\n"} {"\n"}
                          {"\t"}○ The purpose or use of the product (e.g. cleansing the body, reducing the {"\n"} {"\n"}
                          appearance of wrinkles, moisturizing the skin) {"\n"} {"\n"} {"\n"}
                          {"\t"}○ The ingredient list, including an image of the ingredient list from the product label {"\n"} {"\n"}
                          {"\t"}○ Any necessary label warnings {"\n"} {"\n"}
                          {"\t"}● Detail pages must not state that the products cure, mitigate, treat, or prevent a disease {"\n"} {"\n"}
                          in humans unless that statement is approved by the FDA {"\n"} {"\n"}
                          {"\t"}● Detail pages must not state that the cosmetics are “FDA approved” if they are not FDA {"\n"} {"\n"}
                          approved {"\n"} {"\n"}
                          {"\t"}● Images associated with detail pages must not include the FDA logo {"\n"} {"\n"} {"\n"}
                          Products and Ingredients {"\n"} {"\n"}
                          {"\t"}● Cosmetics must not be named in an FDA recall or safety alert (for more information, see: {"\n"} {"\n"}
                          Recalls, Market Withdrawals, & Safety Alerts) {"\n"} {"\n"}
                          {"\t"}● Cosmetics must not contain prohibited ingredients (for more information, see: Prohibited {"\n"} {"\n"}
                          & Restricted Ingredients) {"\n"} {"\n"}
                          {"\t"}● Cosmetics must be safe for use and must not be a product that the Food and Drug {"\n"} {"\n"}
                          Administration (FDA) has determined presents an unreasonable risk of injury or illness, {"\n"} {"\n"}
                          such as: {"\n"} {"\n"}
                          {"\t"}○ Products that contain methylene glycol, which, when heated, release {"\n"} {"\n"}
                          formaldehyde into the air (for more information, see: Hair-Smoothing Products {"\n"} {"\n"}
                          That Release Formaldehyde When Heated) {"\n"} {"\n"}
                          {"\t"}○ Eyelash and eyebrow permanent dye (for more information, see: Use Eye {"\n"} {"\n"}
                          Cosmetics Safely) {"\n"} {"\n"}
                          {"\t"}○ Eye makeup containing Kohl, Kajal, Al-Kahal, or Surma (for more information, {"\n"} {"\n"}
                          see Use Eye Cosmetics Safely) {"\n"} {"\n"}
                          {"\t"}○ Skin creams containing mercury (for more information, see: Mercury Poisoning {"\n"} {"\n"}
                          Linked to Skin Products) {"\n"} {"\n"}
                          {"\t"}○ Henna products designed or marketed for body-decorating or any other variation {"\n"} {"\n"}
                          of direct skin application (for more information, see: Cosmetics Safety Q&A: {"\n"} {"\n"}
                          Tattoos and Permanent Makeup) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Cosmetics must not require a prescription or a medical professional's supervision or {"\n"} {"\n"}
                          direction for their use {"\n"} {"\n"}
                          {"\t"}● Cosmetics must not contain controlled substances, such as: {"\n"} {"\n"}
                          {"\t"}○ Anything listed in Schedules I, II, III, IV or V of the Controlled Substances Act (for {"\n"} {"\n"}
                          more information, see: Schedules of Controlled Substances) {"\n"} {"\n"}
                          {"\t"}○ "List I" chemicals or their derivatives as designated by the Drug Enforcement {"\n"} {"\n"}
                          Administration (DEA) (for more information, see: List I and List II Chemicals) {"\n"} {"\n"}
                          {"\t"}● Cosmetics must not contain plastic microbeads (for more information, see: The {"\n"} {"\n"}
                          Microbead-Free Waters Act: FAQs) {"\n"} {"\n"}
                          {"\t"}● In order to be sold into California and New York, antiperspirants, deodorants, and {"\n"} {"\n"}
                          hairsprays must not contain toxic air contaminants (for more information, see California {"\n"} {"\n"}
                          Air Resources Board Consumer Products Enforcement and New York Department of {"\n"} {"\n"}
                          Environmental Conservation) {"\n"} {"\n"}
                          {"\t"}● Cosmetics must comply with ReferApps policies, including: {"\n"} {"\n"}
                          {"\t"}○ Cosmetics that contain ingredients derived from sharks, whales, dolphins, or {"\n"} {"\n"}
                          porpoises are prohibited from sale {"\n"} {"\n"}
                          {"\t"}○ Cosmetics that contain more than 12% hydrogen peroxide are prohibited from {"\n"} {"\n"}
                          sale {"\n"} {"\n"}
                          {"\t"}○ Cosmetics that contain acetone, such as nail polish remover, cannot be sold in {"\n"} {"\n"}
                          volumes more than 16 oz in total {"\n"} {"\n"}
                          {"\t"}○ The products LiLash, LiBrow, and InStyler rotating irons are prohibited from sale {"\n"} {"\n"}
                          {"\t"}○ Claire’s brand cosmetics are prohibited from sale {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Used cosmetics {"\n"} {"\n"}
                          {"\t"}● Used makeup sponges or applicators {"\n"} {"\n"}
                          {"\t"}● Cosmetics, colognes, perfumes, lotions which do not come in the original container {"\n"} {"\n"}
                          {"\t"}● Corrective and cosmetic contact lenses {"\n"} {"\n"} {"\n"}
                          {"\t"}● Latisse {"\n"} {"\n"}
                          {"\t"}● Obagi Nu-Derm Sunfader {"\n"} {"\n"}
                          {"\t"}● Obagi Nu-Derm Clear {"\n"} {"\n"}
                          {"\t"}● Obagi Nu-Derm Blender {"\n"} {"\n"}
                          {"\t"}● Obagi Elastiderm Decolletage Skin Lightening Complex {"\n"} {"\n"}
                          {"\t"}● Obagi C-Therapy Night cream {"\n"} {"\n"}
                          {"\t"}● Obagi RX System Clarifying Serum {"\n"} {"\n"}
                          {"\t"}● Products that contain minoxidil in excess of 5% {"\n"} {"\n"}
                          {"\t"}● Bithionol {"\n"} {"\n"}
                          {"\t"}● Brazilian Blowout Acai Professional Smoothing Solution {"\n"} {"\n"}
                          {"\t"}● Chloroform {"\n"} {"\n"}
                          {"\t"}● Halogenated salicylanilides {"\n"} {"\n"}
                          {"\t"}● Methylene chloride {"\n"} {"\n"}
                          {"\t"}● Vinyl chloride in aerosol products {"\n"} {"\n"}
                          {"\t"}● Zirconium-containing complexes in aerosol products {"\n"} {"\n"}
                          {"\t"}● Synthol, Synthrol, or Swethol posing oil {"\n"} {"\n"} {"\n"}
                          Credit and debit cards {"\n"} {"\n"} {"\n"}
                          Before listing this type of item, you must: {"\n"} {"\n"}
                          {"\t"}● The name and part of the number on the card must be blocked out or blur if including a {"\n"} {"\n"}
                          picture in the listing {"\n"} {"\n"}
                          {"\t"}● Expiration date in the description and picture must be included {"\n"} {"\n"} {"\n"}
                          {"\t"}● Look at the terms and conditions from the issuing card company to make sure the sale {"\n"} {"\n"}
                          of the expired card is allowed. Some credit and debit card companies keep ownership {"\n"} {"\n"}
                          even after the card expires {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Blank plastic cards, with or without magnetic strips {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Currently active bank credit or debit cards {"\n"} {"\n"}
                          {"\t"}● Currently active store credit cards {"\n"} {"\n"}
                          {"\t"}● Pre-paid Visa, MasterCard, American Express, and Discover cards, regardless of the {"\n"} {"\n"}
                          amount on the card {"\n"} {"\n"}
                          {"\t"}● Expired bank or store cards that have been expired less than 10 years {"\n"} {"\n"}
                          {"\t"}● Cards still owned by the company or card issuer, even after the card has expired {"\n"} {"\n"}
                          {"\t"}● Non-embossed bank or store credit cards {"\n"} {"\n"} {"\n"}
                          Currency, Coins, Cash Equivalents, Stamps and Gift Cards {"\n"} {"\n"} {"\n"}
                          Without limiting the foregoing, if you qualify as a dealer of precious metals, precious stones or {"\n"} {"\n"}
                          jewels, you must comply with 31 C.F.R. Part 1027 and other laws and regulations applicable to {"\n"} {"\n"}
                          you and will, upon ReferApps' request, provide copies of your anti-money laundering program to {"\n"} {"\n"}
                          ReferApps. {"\n"} {"\n"}
                          Items considered exonumia, including but not limited to medals, medallions, challenge coins, {"\n"} {"\n"}
                          and commemorative coins not primarily made of a precious metal are exempt from this policy. {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Collectible Coins {"\n"} {"\n"} {"\n"}
                          {"\t"}● Monetized Bullion: Coins or other forms of money manufactured of gold, silver, or other {"\n"} {"\n"}
                          precious metal and used now or subsequently used as a medium of exchange under the {"\n"} {"\n"}
                          laws of a U.S. state, the United States, or any foreign nation. The medium of exchange {"\n"} {"\n"}
                          must have had a legal status equivalent to legal tender that includes relevant information {"\n"} {"\n"}
                          known about the items: {"\n"} {"\n"}
                          {"\t"}○ Numismatic preservation {"\n"} {"\n"}
                          {"\t"}○ Country of circulation of a coin {"\n"} {"\n"}
                          {"\t"}○ Clear image of the item {"\n"} {"\n"}
                          {"\t"}○ Condition {"\n"} {"\n"}
                          {"\t"}○ Grader and Grade {"\n"} {"\n"}
                          {"\t"}○ Date of issue {"\n"} {"\n"}
                          {"\t"}○ Metal composition {"\n"} {"\n"}
                          {"\t"}○ Purity of the metal if the item is monetized bullion {"\n"} {"\n"}
                          {"\t"}○ Restrikes produced or authorized by the issuing government {"\n"} {"\n"}
                          {"\t"}● Collectible coins or monetized bullion that are reproductions, replicas, or copies and {"\n"} {"\n"}
                          satisfy the following conditions as well as comply with the Counterfeit Deterrence Act of {"\n"} {"\n"}
                          1992: {"\n"} {"\n"}
                          {"\t"}○ Marked as such on the item {"\n"} {"\n"}
                          {"\t"}○ Contain a clear image of the mark {"\n"} {"\n"}
                          {"\t"}○ State that the item is a reproduction, replica, or copy in the title and the {"\n"} {"\n"}
                          description {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Items considered legal tender that has been altered outside of numismatic preservation. {"\n"} {"\n"}
                          This includes holograms, overlays, cladding, plating, or colorizing{"\n"} {"\n"}
                          {"\t"}● Paper money, including uncut sheets, bank notes, money orders, bricks, and certificates {"\n"} {"\n"}
                          {"\t"}● Imitation and prop money that is not conspicuously marked "Copy" including: {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Coins {"\n"} {"\n"}
                          {"\t"}○ Token {"\n"} {"\n"}
                          {"\t"}○ Paper money {"\n"} {"\n"}
                          {"\t"}○ Commemorative medals {"\n"} {"\n"}
                          {"\t"}● Rounds made primarily of precious metal {"\n"} {"\n"}
                          {"\t"}● "Unsearched" bags, rolls, tubes, jars, or hoards where the buyers cannot identify clearly {"\n"} {"\n"}
                          the items they will receive {"\n"} {"\n"}
                          {"\t"}● Counterfeit coins and paper money such as bank notes, bonds, money orders, silver {"\n"} {"\n"}
                          certificates, and gold certificates {"\n"} {"\n"}
                          {"\t"}● Stocks and securities {"\n"} {"\n"}
                          {"\t"}● Equipment designed to create counterfeit items {"\n"} {"\n"}
                          {"\t"}● Cash or cash equivalent instruments, including money orders, traveler's checks, stored {"\n"} {"\n"}
                          value products like retail or open loop gift cards or electronic stored-value redemption {"\n"} {"\n"}
                          codes (except for retail gift cards listed by pre-approved sellers) {"\n"} {"\n"}
                          {"\t"}● Gift cards, including: {"\n"} {"\n"}
                          {"\t"}○ Gift cards, except retail gift cards listed by pre-approved sellers {"\n"} {"\n"}
                          {"\t"}○ Retail gift cards that exceed $2,000 in value {"\n"} {"\n"}
                          {"\t"}● Non-monetized bullion (precious metal which has been smelted or refined and has a {"\n"} {"\n"}
                          value dependent primarily upon its precious metal content and not upon its form) {"\n"} {"\n"}
                          {"\t"}● State or federal assistance benefits, such as: {"\n"} {"\n"}
                          {"\t"}○ Special Supplemental Nutrition Program for Women, Infants, and Children (WIC) {"\n"} {"\n"}
                          benefits, including supplemental foods (such as infant formula), cash-value {"\n"} {"\n"}
                          vouchers, and food instruments (such as vouchers, checks, electronic benefits {"\n"} {"\n"}
                          transfer (EBT) cards, and coupons) {"\n"} {"\n"}
                          {"\t"}○ Supplemental Nutrition Assistance Program (SNAP, formerly known as Food {"\n"} {"\n"}
                          Stamp Program) benefits and SNAP EBT cards {"\n"} {"\n"} {"\n"}
                          Dietary Supplements {"\n"} {"\n"} {"\n"}
                          Dietary supplements are vitamins, minerals, herbs, or other substances (like amino acids or {"\n"} {"\n"}
                          fatty acids) eaten to supplement the diet. They often come in forms like tablets, capsules, soft {"\n"} {"\n"}
                          gels, gel caps, powders, and liquids. {"\n"} {"\n"}
                          Supplements must be correctly described and labeled, and they must not be prohibited by {"\n"} {"\n"}
                          ReferApps policies. Please use the checklist below to be sure your product can be sold on {"\n"} {"\n"}
                          ReferApps. {"\n"} {"\n"} {"\n"}
                          Packaging {"\n"} {"\n"}
                          {"\t"}● Drugs must be sealed in the original manufacturer’s packaging {"\n"} {"\n"}
                          {"\t"}● Drugs must be new and unused {"\n"} {"\n"}
                          {"\t"}● Drugs must clearly display identifying codes placed on the packaging by the {"\n"} {"\n"}
                          manufacturer or distributor, such as matrix codes, lot numbers, or serial numbers {"\n"} {"\n"} {"\n"}
                          Labeling {"\n"} {"\n"}
                          {"\t"}● Drugs must be labeled in English with the following information: {"\n"} {"\n"}
                          {"\t"}○ The title of the drug {"\n"} {"\n"}
                          {"\t"}○ The “Drug Facts” panel {"\n"} {"\n"}
                          {"\t"}○ The active ingredient(s) {"\n"} {"\n"}
                          {"\t"}○ The purpose(s) of the drug {"\n"} {"\n"}
                          {"\t"}○ The use(s) of the drug {"\n"} {"\n"}
                          {"\t"}○ Any required warning(s) {"\n"} {"\n"}
                          {"\t"}○ The directions for using the drug {"\n"} {"\n"}
                          {"\t"}● Any other information, as required for the specific product {"\n"} {"\n"}
                          {"\t"}● The inactive ingredients {"\n"} {"\n"} {"\n"}
                          {"\t"}● Drug must use the claim “FDA approved” appropriately {"\n"} {"\n"}
                          {"\t"}● Drug labels must not use the FDA logo {"\n"} {"\n"} {"\n"}
                          Detail page {"\n"} {"\n"}
                          {"\t"}● Detail pages must include the following information: {"\n"} {"\n"}
                          {"\t"}○ The name of the drug {"\n"} {"\n"}
                          {"\t"}○ The active ingredients {"\n"} {"\n"}
                          {"\t"}○ An image of the “Drug Facts” panel (and any “Drug Facts (Continued)” panels, if {"\n"} {"\n"}
                          applicable) from the product label {"\n"} {"\n"} {"\n"}
                          {"\t"}● Detail pages must comply with the approved labeling for the drug {"\n"} {"\n"}
                          {"\t"}● Drug must use the claim “FDA approved” appropriately {"\n"} {"\n"}
                          {"\t"}● Drug labels must not use the FDA logo {"\n"} {"\n"} {"\n"}
                          Drug products and ingredients {"\n"} {"\n"}
                          {"\t"}● Drug products must be approved by the FDA for over-the-counter (OTC) sale {"\n"} {"\n"}
                          {"\t"}● Drug products must not require a prescription or a medical professional's supervision or {"\n"} {"\n"}
                          direction for their use, such as: {"\n"} {"\n"}
                          {"\t"}○ Prescription drugs and their active ingredients {"\n"} {"\n"}
                          {"\t"}○ Antibiotics (including "fish" antibiotics, both over-the-counter and prescription) {"\n"} {"\n"}
                          {"\t"}○ Prescription veterinary products and their active ingredients {"\n"} {"\n"}
                          {"\t"}○ Vaccines (including human and animal vaccines) {"\n"} {"\n"}
                          {"\t"}● Drug products must not be named in an FDA recall or safety alert (for more information, {"\n"} {"\n"}
                          see: Recalls, Market Withdrawals, & Safety Alerts) {"\n"} {"\n"}
                          {"\t"}● Drug products must not have been the subject of a Drug Enforcement Administration {"\n"} {"\n"}
                          (DEA) emergency scheduling (for more information, see: DEA Press Releases {"\n"} {"\n"} {"\n"}
                          {"\t"}● Drug products must not be named by the Federal Trade Commission (FTC) as making {"\n"} {"\n"}
                          untrue marketing claims (for more information, see: Federal Trade Commission Press {"\n"} {"\n"}
                          Releases) {"\n"} {"\n"}
                          {"\t"}● Drug products must not be identified as adulterated (e.g. unsafe or lacking evidence of {"\n"} {"\n"}
                          safety) or misbranded (e.g. having false or misleading information on the label) in an {"\n"} {"\n"}
                          FDA warning letter (for more information, see: FDA Warning Letters) {"\n"} {"\n"}
                          {"\t"}● Drug listings must not be for controlled substances or products containing controlled {"\n"} {"\n"}
                          substances, such as: {"\n"} {"\n"}
                          {"\t"}○ Products containing cannabidiol (CBD), a Schedule I Controlled Substance, {"\n"} {"\n"}
                          including but not limited to: {"\n"} {"\n"}
                          {"\t"}■ Rich Hemp Oil containing cannabidiol (CBD) {"\n"} {"\n"}
                          {"\t"}■ Products that have been identified as containing CBD by LegitScript {"\n"} {"\n"}
                          {"\t"}○ Hemp products containing Resin or tetrahydrocannabinol (THC) {"\n"} {"\n"}
                          {"\t"}○ Hemp (or any cannabis Sativa spp. strain) seeds capable of germination {"\n"} {"\n"}
                          {"\t"}○ Anything listed in Schedules I, II, III, IV or V of the Controlled Substances Act (for {"\n"} {"\n"}
                          more information, see: Schedules of Controlled Substances), such as: {"\n"} {"\n"}
                          {"\t"}■ Coca Leaves, including all variations of leaves, tea, and coca extract {"\n"} {"\n"}
                          {"\t"}■ Hallucinogenic mushrooms {"\n"} {"\n"}
                          {"\t"}■ Poppy pods, poppy straw, and poppy straw concentrate {"\n"} {"\n"}
                          {"\t"}○ "List I" chemicals or their derivatives as designated by the Drug Enforcement {"\n"} {"\n"}
                          Administration (DEA) (for more information, see: List I and List II Chemicals), {"\n"} {"\n"}
                          such as: {"\n"} {"\n"}
                          {"\t"}■ Ephedrine {"\n"} {"\n"}
                          {"\t"}■ Phenylpropanalomine {"\n"} {"\n"}
                          {"\t"}■ Pseudoephedrine {"\n"} {"\n"}
                          {"\t"}■ Ergotamine {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Drugs listings must not be for substances identified by the Drug Enforcement {"\n"} {"\n"}
                          Administration (DEA) as a drug or chemical of concern, such as: {"\n"} {"\n"}
                          {"\t"}■ Dextromethorphan {"\n"} {"\n"}
                          {"\t"}■ Jimson Weed {"\n"} {"\n"}
                          {"\t"}■ Kratom {"\n"} {"\n"}
                          {"\t"}■ Salvia Divinorum {"\n"} {"\n"}
                          {"\t"}○ Drug listings must comply with ReferApps policies: {"\n"} {"\n"}
                          {"\t"}○ Certain loperamide offers are restricted to support limitations on access to high {"\n"} {"\n"}
                          pill count units {"\n"} {"\n"}
                          {"\t"}○ Listings for poppy seeds that make certain claims, including listings for poppy {"\n"} {"\n"}
                          seeds that reference poppy tea, poppy seeds that are “unwashed,” poppy seeds {"\n"} {"\n"}
                          that reference alkaloid content, and poppy seeds that reference opium are {"\n"} {"\n"}
                          prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to have psychoactive or hallucinogenic effects are {"\n"} {"\n"}
                          prohibited, including “designer drugs” such as methoxetamine {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to produce an effect similar to that of an anabolic {"\n"} {"\n"}
                          steroid such as "Legal Steroids" are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to imitate the effects of a controlled substance, {"\n"} {"\n"}
                          prescription drug, or substance identified by the FDA as an unapproved new drug {"\n"} {"\n"}
                          or tainted supplement are prohibited, including synthetic cannabinoids and bath {"\n"} {"\n"}
                          salts {"\n"} {"\n"}
                          {"\t"}○ Listings for homeopathic teething products are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for the active ingredients in prescription-only drugs are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listing for injectable drugs are prohibited {"\n"} {"\n"} {"\n"}
                          Products associated with drugs and controlled substances {"\n"} {"\n"}
                          {"\t"}● Products must not be primarily used for making, preparing, or using a controlled  {"\n"} {"\n"}
                          substance, such as:  {"\n"} {"\n"}  {"\n"}
                          {"\t"}○ Butane Honey Oil "BHO" extractors and kits  {"\n"} {"\n"}
                          {"\t"}○ Bongs and all related accessories  {"\n"} {"\n"}
                          {"\t"}○ Dab kits  {"\n"} {"\n"}
                          {"\t"}○ Pipes made from metal, most woods, acrylic, glass, stone, plastic or ceramic  {"\n"} {"\n"}
                          "Rose in a Glass" pipes and similar products  {"\n"} {"\n"}
                          {"\t"}○ Vaporizers and all related accessories  {"\n"} {"\n"}
                          {"\t"}○ Wired cigarette papers  {"\n"} {"\n"}
                          {"\t"}○ Nitrous Oxide Crackers  {"\n"} {"\n"}
                          {"\t"}○ Drug purity testing kits  {"\n"} {"\n"}
                          {"\t"}○ The term "drug paraphernalia" includes any equipment, product, or material of  {"\n"} {"\n"}
                          any kind which is primarily intended or designed for use in:  {"\n"} {"\n"}
                          {"\t"}■ Manufacturing  {"\n"} {"\n"}
                          {"\t"}■ Compounding  {"\n"} {"\n"}
                          {"\t"}■ Converting  {"\n"} {"\n"}
                          {"\t"}■ Concealing  {"\n"} {"\n"}
                          {"\t"}■ Producing  {"\n"} {"\n"}
                          {"\t"}■ Processing  {"\n"} {"\n"}
                          {"\t"}■ Preparing  {"\n"} {"\n"}
                          {"\t"}■ Injecting  {"\n"} {"\n"}
                          {"\t"}■ Ingesting  {"\n"} {"\n"}
                          {"\t"}■ Inhaling  {"\n"} {"\n"}
                          {"\t"}■ introducing into the human body a controlled substance  {"\n"} {"\n"}
                          {"\t"}● Product listings must comply with ReferApps policies:  {"\n"} {"\n"}
                          {"\t"}○ Products intended to defeat a drug test, such as urine additives and synthetic  {"\n"} {"\n"}
                          urine are prohibited  {"\n"} {"\n"}
                          {"\t"}○ Tablet presses or molds used to press or imprint a pharmaceutical drug name or  {"\n"} {"\n"}
                          identification number onto a tablet or pill are prohibited  {"\n"} {"\n"}
                          {"\t"}○ Listings for products that reference a product’s use with illegal drugs are  {"\n"} {"\n"}
                          prohibited  {"\n"} {"\n"}  {"\n"}
                          Digitally delivered goods  {"\n"} {"\n"}  {"\n"} {"\n"}
                          Before listing digitally delivered goods, the seller must follow these guidelines:  {"\n"} {"\n"}
                          {"\t"}● Your listing clearly states that you legally own the content (sometimes called intellectual  {"\n"} {"\n"}
                          property) or are authorized to sell it by the owner, the owner's authorized agent, or the  {"\n"} {"\n"}
                          law  {"\n"} {"\n"}
                          {"\t"}● Your items can't have any sexually-oriented adult (pornographic) content. For more  {"\n"} {"\n"}
                          details, see our Adult Only category policy  {"\n"} {"\n"}
                          {"\t"}● Your item can't include software that would damage anyone's computer, could be used  {"\n"} {"\n"}
                          for malicious purposes like sending spam emails or spreading a computer virus, or  {"\n"} {"\n"}
                          would violate anyone's privacy (spyware or cookies, for example). For more details, see  {"\n"} {"\n"}
                          our JavaScript policy  {"\n"} {"\n"}
                          {"\t"}● You have to be verified on PayPal to help confirm your identity  {"\n"} {"\n"}
                          {"\t"}● Local pickup is not allowed for digital items  {"\n"} {"\n"}  {"\n"}
                          Allowed in Listing  {"\n"} {"\n"}  {"\n"}
                          The following items can offer digital delivery:  {"\n"} {"\n"}
                          {"\t"}● Tickets  {"\n"} {"\n"}
                          {"\t"}● Domain names  {"\n"} {"\n"}
                          {"\t"}● eTopps trading cards  {"\n"} {"\n"}  {"\n"}
                          {"\t"}● Software product keys  {"\n"} {"\n"}
                          {"\t"}● Online gaming items such as characters, currency, points, in-game items, etc.  {"\n"} {"\n"}
                          Examples of digitally delivered goods that you can list on ReferApps as long as you follow the  {"\n"} {"\n"}
                          requirements above:  {"\n"} {"\n"}
                          {"\t"}● MP3 music you wrote, recorded, and own all the rights to  {"\n"} {"\n"}
                          {"\t"}● Homemade movies you created and own the rights to  {"\n"} {"\n"}
                          {"\t"}● Computer software you created and own the rights to  {"\n"} {"\n"}
                          {"\t"}● Software that you're authorized to sell online by the owner, the owner's authorized  {"\n"} {"\n"}
                          agent, or the law  {"\n"} {"\n"}
                          {"\t"}● Digital art you captured yourself and own the rights to or that you're authorized to sell  {"\n"} {"\n"}
                          online by the owner, the owner's authorized agent, or the law  {"\n"} {"\n"}
                          {"\t"}● Photos you captured yourself and own the rights to  {"\n"} {"\n"}
                          {"\t"}● Recipes you created yourself and own the rights to  {"\n"} {"\n"}  {"\n"}
                          Not Allowed in Listings  {"\n"} {"\n"}
                          Examples of digitally delivered goods that can't be advertised on ReferApps:  {"\n"} {"\n"}
                          {"\t"}● MP3 music copied from a CD you own or recorded at a concert  {"\n"} {"\n"}
                          {"\t"}● eBooks you don't own the copyright to and aren't authorized to resell by the owner, the  {"\n"} {"\n"}
                          owner's authorized agent, or the law  {"\n"} {"\n"}
                          {"\t"}● Songs you bought from iTunes  {"\n"} {"\n"}
                          {"\t"}● Movies you copied from DVDs you own  {"\n"} {"\n"}
                          {"\t"}● Video games you copied from the original CDs  {"\n"} {"\n"}
                          {"\t"}● PDF files of product manuals or user guide that you don't own the copyright of or you  {"\n"} {"\n"}
                          aren't authorized to resell by the owner, the owner's authorized agent, or the law  {"\n"} {"\n"}  {"\n"}
                          Drugs & drug paraphernalia  {"\n"} {"\n"}  {"\n"}
                          A drug is a substance used to diagnose, cure, treat, or prevent diseases in people or animals.  {"\n"} {"\n"}
                          Drugs can also be used to change the structure or function of the body, such as to treat some  {"\n"} {"\n"}
                          kinds of acne. Some drugs require a prescription for sale, such as most antibiotics, while other  {"\n"} {"\n"}
                          drugs may be sold without a prescription because they are approved by the FDA to be sold over  {"\n"} {"\n"}
                          the counter.  {"\n"} {"\n"}
                          “Controlled substances” are drugs that are illegal, such as cocaine or heroin. Products used with  {"\n"} {"\n"}
                          controlled substances may be considered drug paraphernalia. Drug paraphernalia includes  {"\n"} {"\n"}
                          products primarily intended or designed for use in making, preparing, or using a controlled  {"\n"} {"\n"}
                          substance.  {"\n"} {"\n"}
                          ReferApps do not allow selling narcotics, steroids, and all other controlled substances.  {"\n"} {"\n"}
                          You also can't list drug paraphernalia, including items designed or primarily intended for use in  {"\n"} {"\n"}
                          manufacturing, concealing, or using a controlled substance.  {"\n"} {"\n"}  {"\n"}
                          Packaging  {"\n"} {"\n"}
                          {"\t"}● Drugs must be sealed in the original manufacturer’s packaging  {"\n"} {"\n"}
                          {"\t"}● Drugs must be new and unused  {"\n"} {"\n"}
                          {"\t"}● Drugs must clearly display identifying codes placed on the packaging by the  {"\n"} {"\n"}
                          manufacturer or distributor, such as matrix codes, lot numbers, or serial numbers  {"\n"} {"\n"}  {"\n"}
                          Labeling  {"\n"} {"\n"}
                          {"\t"}● Drugs must be labeled in English with the following information:  {"\n"} {"\n"}
                          {"\t"}○ The title of the drug  {"\n"} {"\n"}
                          {"\t"}○ The “Drug Facts” panel  {"\n"} {"\n"}
                          {"\t"}○ The active ingredient(s)  {"\n"} {"\n"}
                          {"\t"}○ The purpose(s) of the drug  {"\n"} {"\n"}  {"\n"}
                          {"\t"}○ The use(s) of the drug  {"\n"} {"\n"}
                          {"\t"}○ Any required warning(s)  {"\n"} {"\n"}
                          {"\t"}○ The directions for using the drug  {"\n"} {"\n"}
                          {"\t"}○ Any other information, as required for the specific product  {"\n"} {"\n"}
                          {"\t"}○ The inactive ingredients  {"\n"} {"\n"}
                          {"\t"}○ Drug must use the claim “FDA approved” appropriately  {"\n"} {"\n"}
                          {"\t"}○ Drug labels must not use the FDA logo  {"\n"} {"\n"}  {"\n"}
                          Detail page  {"\n"} {"\n"}
                          {"\t"}● Detail pages must include the following information:  {"\n"} {"\n"}
                          {"\t"}○ The name of the drug  {"\n"} {"\n"}
                          {"\t"}○ The active ingredients  {"\n"} {"\n"}
                          {"\t"}○ An image of the “Drug Facts” panel (and any “Drug Facts (Continued)” panels, if  {"\n"} {"\n"}
                          applicable) from the product label  {"\n"} {"\n"}  {"\n"}
                          {"\t"}● Detail pages must comply with the approved labeling for the drug  {"\n"} {"\n"}
                          {"\t"}● Drug must use the claim “FDA approved” appropriately  {"\n"} {"\n"}
                          {"\t"}● Drug labels must not use the FDA logo  {"\n"} {"\n"}  {"\n"}
                          Drug products and ingredients  {"\n"} {"\n"}
                          {"\t"}● Drug products must be approved by the FDA for over-the-counter (OTC) sale  {"\n"} {"\n"}
                          {"\t"}● Drug products must not require a prescription or a medical professional's supervision or  {"\n"} {"\n"}
                          direction for their use, such as:  {"\n"} {"\n"}
                          {"\t"}○ Prescription drugs and their active ingredients  {"\n"} {"\n"}
                          {"\t"}○ Antibiotics (including "fish" antibiotics, both over-the-counter and prescription)  {"\n"} {"\n"}  {"\n"}
                          {"\t"}○ Prescription veterinary products and their active ingredients  {"\n"} {"\n"}
                          {"\t"}○ Vaccines (including human and animal vaccines)  {"\n"} {"\n"}
                          {"\t"}● Drug products must not be named in an FDA recall or safety alert (for more information,  {"\n"} {"\n"}
                          see: Recalls, Market Withdrawals, & Safety Alerts)  {"\n"} {"\n"}
                          {"\t"}● Drug products must not have been the subject of a Drug Enforcement Administration  {"\n"} {"\n"}
                          (DEA) emergency scheduling (for more information, see: DEA Press Releases)  {"\n"} {"\n"}
                          {"\t"}● Drug products must not be named by the Federal Trade Commission (FTC) as making  {"\n"} {"\n"}
                          untrue marketing claims (for more information, see: Federal Trade Commission Press  {"\n"} {"\n"}
                          Releases)  {"\n"} {"\n"}
                          {"\t"}● Drug products must not be identified as adulterated (e.g. unsafe or lacking evidence of  {"\n"} {"\n"}
                          safety) or misbranded (e.g. having false or misleading information on the label) in an  {"\n"} {"\n"}
                          FDA warning letter (for more information, see: FDA Warning Letters)  {"\n"} {"\n"}
                          {"\t"}● Drug listings must not be for controlled substances or products containing controlled  {"\n"} {"\n"}
                          substances, such as:  {"\n"} {"\n"}
                          {"\t"}○ Products containing cannabidiol (CBD), a Schedule I Controlled Substance,  {"\n"} {"\n"}
                          including but not limited to:  {"\n"} {"\n"}
                          {"\t"}■ Rich Hemp Oil containing cannabidiol (CBD)  {"\n"} {"\n"}
                          {"\t"}■ Products that have been identified as containing CBD by LegitScript  {"\n"} {"\n"}
                          {"\t"}○ Hemp products containing Resin or tetrahydrocannabinol (THC)  {"\n"} {"\n"}
                          {"\t"}○ Hemp (or any cannabis Sativa spp. strain) seeds capable of germination  {"\n"} {"\n"}
                          {"\t"}○ Anything listed in Schedules I, II, III, IV or V of the Controlled Substances Act (for  {"\n"} {"\n"}
                          more information, see: Schedules of Controlled Substances), such as:  {"\n"} {"\n"}
                          {"\t"}○ Coca Leaves, including all variations of leaves, tea, and coca extract  {"\n"} {"\n"}
                          {"\t"}○ Hallucinogenic mushrooms  {"\n"} {"\n"}
                          {"\t"}○ Poppy pods, poppy straw, and poppy straw concentrate  {"\n"} {"\n"}  {"\n"}
                          {"\t"}● "List I" chemicals or their derivatives as designated by the Drug Enforcement  {"\n"} {"\n"}
                          Administration (DEA) (for more information, see: List I and List II Chemicals),  {"\n"} {"\n"}
                          such as:  {"\n"} {"\n"}
                          {"\t"}○ Ephedrine  {"\n"} {"\n"}
                          {"\t"}○ Phenylpropanolamine  {"\n"} {"\n"}
                          {"\t"}○ Pseudoephedrine  {"\n"} {"\n"}
                          {"\t"}○ Ergotamine  {"\n"} {"\n"}  {"\n"}
                          {"\t"}● Drugs listings must not be for substances identified by the Drug Enforcement {"\n"} {"\n"}
                          Administration (DEA) as a drug or chemical of concern, such as: {"\n"} {"\n"}
                          {"\t"}○ Dextromethorphan {"\n"} {"\n"}
                          {"\t"}○ Jimson Weed {"\n"} {"\n"}
                          {"\t"}○ Kratom {"\n"} {"\n"}
                          {"\t"}○ Salvia Divinorum {"\n"} {"\n"}
                          {"\t"}● Drug listings must comply with ReferApps policies: {"\n"} {"\n"}
                          {"\t"}○ Certain loperamide offers are restricted to support limitations on access to high {"\n"} {"\n"}
                          pill count units {"\n"} {"\n"}
                          {"\t"}○ Listings for poppy seeds that make certain claims, including listings for poppy {"\n"} {"\n"}
                          seeds that reference poppy tea, poppy seeds that are “unwashed,” poppy seeds {"\n"} {"\n"}
                          that reference alkaloid content, and poppy seeds that reference opium are {"\n"} {"\n"}
                          prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to have psychoactive or hallucinogenic effects are {"\n"} {"\n"}
                          prohibited, including “designer drugs” such as methoxetamine {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to produce an effect similar to that of an anabolic {"\n"} {"\n"}
                          steroid such as "Legal Steroids" are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for products claiming to imitate the effects of a controlled substance, {"\n"} {"\n"}
                          prescription drug, or substance identified by the FDA as an unapproved new drug {"\n"} {"\n"}
                          or tainted supplement are prohibited, including synthetic cannabinoids and bath {"\n"} {"\n"}
                          salts {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Listings for homeopathic teething products are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listings for the active ingredients in prescription-only drugs are prohibited {"\n"} {"\n"}
                          {"\t"}○ Listing for injectable drugs are prohibited {"\n"} {"\n"}
                          {"\t"}● Products associated with drugs and controlled substances {"\n"} {"\n"}
                          {"\t"}○ Products must not be primarily used for making, preparing, or using a controlled {"\n"} {"\n"}
                          substance, such as: {"\n"} {"\n"}
                          {"\t"}■ Butane Honey Oil "BHO" extractors and kits {"\n"} {"\n"}
                          {"\t"}■ Bongs and all related accessories {"\n"} {"\n"}
                          {"\t"}■ Dab kits {"\n"} {"\n"}
                          {"\t"}■ Pipes made from metal, most woods, acrylic, glass, stone, plastic or {"\n"} {"\n"}
                          ceramic {"\n"} {"\n"}
                          "Rose in a Glass" pipes and similar products {"\n"} {"\n"}
                          {"\t"}■ Vaporizers and all related accessories {"\n"} {"\n"}
                          {"\t"}■ Wired cigarette papers {"\n"} {"\n"}
                          {"\t"}■ Nitrous Oxide Crackers {"\n"} {"\n"}
                          {"\t"}■ Drug purity testing kits {"\n"} {"\n"}
                          {"\t"}■ The term "drug paraphernalia" includes any equipment, product, or {"\n"} {"\n"}
                          material of any kind which is primarily intended or designed for use in {"\n"} {"\n"}
                          manufacturing, compounding, converting, concealing, producing, {"\n"} {"\n"}
                          processing, preparing, injecting, ingesting, inhaling, or otherwise {"\n"} {"\n"}
                          introducing into the human body a controlled substance. {"\n"} {"\n"} {"\n"}
                          {"\t"}● Product listings must comply with ReferApps policies: {"\n"} {"\n"}
                          {"\t"}○ Products intended to defeat a drug test, such as urine additives and synthetic {"\n"} {"\n"}
                          urine are prohibited {"\n"} {"\n"}
                          {"\t"}○ Tablet presses or molds used to press or imprint a pharmaceutical drug name or {"\n"} {"\n"}
                          identification number onto a tablet or pill are prohibited {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Listings for products that reference a product’s use with illegal drugs are {"\n"} {"\n"}
                          prohibited {"\n"} {"\n"} {"\n"}
                          Electronics/Electrical and electronic equipment {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Radar detectors certified by the Federal Communications Commission (FCC) {"\n"} {"\n"}
                          {"\t"}● Scanning receivers and radios certified by the FCC {"\n"} {"\n"}
                          {"\t"}● Low-power radio transmitters {"\n"} {"\n"}
                          {"\t"}● Extended coverage high-frequency transceivers {"\n"} {"\n"}
                          {"\t"}● Cordless telephones certified by the FCC {"\n"} {"\n"}
                          {"\t"}● Class I lasers (such as CD players), Class IM lasers (such as DVD players), Class II {"\n"} {"\n"}
                          lasers (e.g. barcode scanners - limited to 1 mW), and Non-handheld Class 3R lasers (for {"\n"} {"\n"}
                          example, certain laser light show projectors limited to 5 mW and under and bore sights {"\n"} {"\n"}
                          and gun sight lasers) {"\n"} {"\n"}
                          {"\t"}● Video game controllers that modify rather than replace existing technology in a video {"\n"} {"\n"}
                          game {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products intended to affect traffic signals {"\n"} {"\n"}
                          {"\t"}● Products where the serial number has been removed or altered {"\n"} {"\n"}
                          {"\t"}● Power amplifiers used with CB radios {"\n"} {"\n"}
                          {"\t"}● Products designed to intentionally block, jam or interfere with licensed or authorized {"\n"} {"\n"}
                          radio communications, such as: {"\n"} {"\n"}
                          {"\t"}○ Cell jammers {"\n"} {"\n"}
                          {"\t"}○ GPS jammers {"\n"} {"\n"} {"\n"}
                          {"\t"}○ GPS repeaters {"\n"} {"\n"}
                          {"\t"}○ Laser jammers {"\n"} {"\n"}
                          {"\t"}○ PCS jammers {"\n"} {"\n"}
                          {"\t"}○ Radar jammers {"\n"} {"\n"}
                          {"\t"}○ WiFi jammers {"\n"} {"\n"}
                          {"\t"}○ Radar shifters {"\n"} {"\n"}
                          {"\t"}● Products that descramble, or gain access to cable or satellite television without {"\n"} {"\n"}
                          permission, such as: {"\n"} {"\n"}
                          {"\t"}○ Blocker devices {"\n"} {"\n"}
                          {"\t"}○ Hardware or Software DSS Emulators {"\n"} {"\n"}
                          {"\t"}○ Listings for information or guides on "how to" descramble cable or satellite {"\n"} {"\n"}
                          television without permission {"\n"} {"\n"}
                          {"\t"}○ Programmed Smart Cards {"\n"} {"\n"}
                          {"\t"}○ Smart Card programmers and un-loopers {"\n"} {"\n"}
                          {"\t"}● Products that incorporate Class IIIB lasers (such as laser light show projectors that have {"\n"} {"\n"}
                          accessible emissions above 5mW) and Class IV lasers (such as industrial lasers) {"\n"} {"\n"}
                          {"\t"}● Children's toys that incorporate Class II lasers or higher {"\n"} {"\n"}
                          {"\t"}● Video game controllers (including cards or other devices) that replace existing {"\n"} {"\n"}
                          technology in a video game {"\n"} {"\n"}
                          {"\t"}● DVD duplicators that bypass copyright protections {"\n"} {"\n"}
                          Blu-ray players that have been modified to disable region coding {"\n"} {"\n"}
                          {"\t"}● Any cell phone that was originally locked to a cell phone carrier and has been manually {"\n"} {"\n"}
                          unlocked to work with other carriers (such as jailbroken iPhones, unlock codes for {"\n"} {"\n"}
                          Blackberry) {"\n"} {"\n"}
                          {"\t"}● Cell phone unlocking devices {"\n"} {"\n"}
                          {"\t"}● Cell phones with bad electronic serial numbers (ESNs) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Micro SIM cards modified from standard size SIM cards {"\n"} {"\n"}
                          Any holiday light and/or decorative outfit product that is not compliant with 16 CFR {"\n"} {"\n"}
                          1120.3(c) {"\n"} {"\n"}
                          {"\t"}● Any extension cord product that is not compliant with 16 CFR 1120.3(d) {"\n"} {"\n"}
                          {"\t"}● Certain streaming media players, voice assistant devices, and related accessories. {"\n"} {"\n"}
                          {"\t"}● Any USB-C (or USB Type-C) cable or adapter product that is not compliant with {"\n"} {"\n"}
                          standard specifications issued by “USB Implementers Forum Inc.” {"\n"} {"\n"}
                          {"\t"}● Audiobook players and accessories produced by The National Library Service for the {"\n"} {"\n"}
                          Blind and Physically Handicapped (“NLS”). These products bear an imprint stating {"\n"} {"\n"}
                          “Property of U.S. Government” and the official seal of the Library of Congress. {"\n"} {"\n"}
                          {"\t"}● Cylindrical lithium-ion cell battery types: 14500, 16340, 18650, 20700, 21700, and 26650 {"\n"} {"\n"}
                          {"\t"}● Wireless microphones that operate in the 600 MHz Band (617-652 MHz and 663-698 {"\n"} {"\n"}
                          MHz) or 700 MHz Band (698-806 MHz) {"\n"} {"\n"} {"\n"}
                          Embargoed goods and prohibited countries/Export {"\n"} {"\n"} {"\n"}
                          Under US law, buying or selling certain items made in restricted countries may not be lawful,{"\n"} {"\n"}
                          depending on the nature of the item, when it was manufactured, and when it left that specific {"\n"} {"\n"}
                          country. {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          Certain informational items from sanctioned countries, such as books, movies, artwork, {"\n"} {"\n"}
                          photographs, music, or similar materials. {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● ReferApps don't allow the sale of embargoed items from the following countries, and we {"\n"} {"\n"}
                          don't allow organizations, businesses, or individuals to have or register accounts while {"\n"} {"\n"}
                          based in these countries: {"\n"} {"\n"}
                          {"\t"}○ Cuba {"\n"} {"\n"}
                          {"\t"}○ Democratic People's Republic of Korea (North Korea) {"\n"} {"\n"}
                          {"\t"}○ Iran {"\n"} {"\n"}
                          {"\t"}○ Syria {"\n"} {"\n"}
                          {"\t"}● We also don't allow organizations, businesses or individuals from the countries listed {"\n"} {"\n"}
                          above or on the United States Specially Designated Nationals (SDNs) list to have or {"\n"} {"\n"}
                          register accounts on ReferApps. {"\n"} {"\n"}
                          {"\t"}● We don't permit the sale of items that claim to be pre-embargo because we cannot {"\n"} {"\n"}
                          determine when an item was removed from a specific country {"\n"} {"\n"} {"\n"}
                          Encouraging illegal activity {"\n"} {"\n"}
                          Not Allowed {"\n"} {"\n"}
                          These are some examples of information, items, and activities that aren't allowed on ReferApps: {"\n"} {"\n"}
                          {"\t"}● Examples of information {"\n"} {"\n"}
                          {"\t"}○ Kill switch-activated electronic devices, including smartphones and tablets {"\n"} {"\n"}
                          {"\t"}○ Information on how to make explosive devices, like bombs {"\n"} {"\n"}
                          {"\t"}○ Instructions on how to use or inject steroids (eBook, website, and so forth) {"\n"} {"\n"}
                          {"\t"}○ Lock-picking DVDs or DVDs that provide information on how to pick locks {"\n"} {"\n"}
                          {"\t"}○ A link to a website that provides information on how to hijack an airplane {"\n"} {"\n"}
                          {"\t"}○ An eBook describing how to create methamphetamine {"\n"} {"\n"}
                          {"\t"}○ Other sources of information similar to those listed here {"\n"} {"\n"}
                          {"\t"}● Examples of items {"\n"} {"\n"}
                          {"\t"}○ Pill presses, molds, and dies {"\n"} {"\n"}
                          {"\t"}○ USPS shipping labels, boxes, and tape (these are offered free of charge at the {"\n"} {"\n"}
                          post office) {"\n"} {"\n"}
                          {"\t"}○ Blank retail "rain checks" or blank store coupons where the buyer can fill in the {"\n"} {"\n"}
                          item and amount the coupon is worth {"\n"} {"\n"}
                          {"\t"}○ Products designed to fake the results of a urine or drug test {"\n"} {"\n"}
                          {"\t"}○ Reflective spray paint or number plate covers or diffusers {"\n"} {"\n"}
                          {"\t"}○ Other items similar to those listed here {"\n"} {"\n"}
                          {"\t"}● Examples of activities {"\n"} {"\n"}
                          {"\t"}○ Basing Feedback ratings on another member's decision not to engage in illegal {"\n"} {"\n"}
                          activity may be considered Feedback extortion. {"\n"} {"\n"}
                          {"\t"}○ Winning an item and requesting that the seller falsify the customs or taxation {"\n"} {"\n"}
                          documents related to the item {"\n"} {"\n"}
                          Explosives, Weapons, and Related Items {"\n"} {"\n"}
                          The seller must comply with all applicable federal laws when selling weapons, imitation {"\n"} {"\n"}
                          weapons, and weapon accessories. You must also comply with state and local laws applicable {"\n"} {"\n"}
                          to the jurisdiction into which your products are sold, as well as the jurisdiction from which you {"\n"} {"\n"}
                          ship. For example, many state laws place requirements on size, color, and markings of imitation {"\n"} {"\n"}
                          weapons. In addition, you are responsible for obtaining any required licenses for permitted {"\n"} {"\n"}
                          products and are liable for any penalties resulting from non-compliance. {"\n"} {"\n"}
                          ReferApps prohibits the listing or sale of firearm ammunition and ammunition components for {"\n"} {"\n"}
                          assault weapons, black powder guns, handguns, muzzleloaders, pistols, shotguns, and rifles. {"\n"} {"\n"} {"\n"}
                          Allowed in Listing  {"\n"} {"\n"}
                          Ammunition that does not contain lead or gunpowder such as: {"\n"} {"\n"}
                          {"\t"}● BB, air, pellet, and airsoft ammunition. {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Paintball gun paint pellets {"\n"} {"\n"}{"\n"}
                          {"\t"}● Plastic non-explosive training aids, such as solid plastic ammunition {"\n"} {"\n"} {"\n"}
                          Event ticket {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Reselling tickets that do not violate any laws {"\n"} {"\n"}
                          {"\t"}● Event tickets that take place in regulated locations must follow the guidelines for that {"\n"} {"\n"}
                          location {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Tickets that are not clearly and explicitly stated in the listing {"\n"} {"\n"}
                          {"\t"}● Ticket packages sales that do not qualify our Ticket packages {"\n"} {"\n"}
                          To qualify as a ticket package, you have to sell a ticket with one of these combinations: {"\n"} {"\n"}
                          {"\t"}● At least one item or experience of significant value that relates to the ticketed {"\n"} {"\n"}
                          event that you've coordinated with the event artist, team, promoter, or organizer. {"\n"} {"\n"}
                          Here are some examples: {"\n"} {"\n"}
                          {"\t"}● A backstage pass {"\n"} {"\n"}
                          {"\t"}● A meeting with the performer {"\n"} {"\n"}
                          {"\t"}● An "experience" like being a ball boy at a baseball game {"\n"} {"\n"}
                          or {"\n"} {"\n"}
                          {"\t"}● A complete travel package for the event that includes: {"\n"} {"\n"}
                          {"\t"}● Air transportation to the event location {"\n"} {"\n"}
                          {"\t"}● Commercial lodging for at least one night {"\n"} {"\n"} {"\n"}
                          Note: You must include both the air transportation and the lodging with the event ticket. {"\n"} {"\n"}
                          {"\t"}● Tickets to major charity events and events where all tickets are free to the public. {"\n"} {"\n"} {"\n"}
                          Explosives, Weapons, and Related Items {"\n"} {"\n"} {"\n"}
                          You must comply with all applicable federal laws when selling weapons, imitation weapons, and {"\n"} {"\n"}
                          weapon accessories. You must also comply with state and local laws applicable to the {"\n"} {"\n"}
                          jurisdiction into which your products are sold, as well as the jurisdiction from which you ship. For {"\n"} {"\n"}
                          example, many state laws place requirements on size, color, and markings of imitation {"\n"} {"\n"}
                          weapons. In addition, you are responsible for obtaining any required licenses for permitted {"\n"} {"\n"}
                          products and are liable for any penalties resulting from non-compliance. {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          Ammunition {"\n"} {"\n"}
                          {"\t"}● Ammunition that does not contain lead or gunpowder such as: {"\n"} {"\n"}
                          {"\t"}○ BB, air, pellet, and airsoft ammunition. {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Paintball gun paint pellets {"\n"} {"\n"}
                          {"\t"}○ Plastic non-explosive training aids, such as solid plastic ammunition {"\n"} {"\n"} {"\n"}
                          Explosives {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Flares designed for safety, emergency, or distress signaling purposes {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}○ Plastic toy hand grenades {"\n"} {"\n"} {"\n"}
                          Firearms {"\n"} {"\n"}
                          {"\t"}● Airsoft guns {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Replicas of firearms that were manufactured in or before 1898, if the replica is not {"\n"} {"\n"}
                          capable of firing fixed ammunition {"\n"} {"\n"} {"\n"}
                          {"\t"}● Non-powdered weapons such as BB guns, pellet guns, paintball guns, and air rifles that {"\n"} {"\n"}
                          are clearly marketed to be air or spring driven and identify the type of ammunition that {"\n"} {"\n"}
                          the product discharges {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Toy guns {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"} {"\n"}
                          Firearms Accessories {"\n"} {"\n"}
                          {"\t"}● Butt plates {"\n"} {"\n"}
                          {"\t"}● Fixed stocks, with the exception of thumbhole stocks {"\n"} {"\n"}
                          {"\t"}● Grip extensions that do not add capacity and do not allow for the use of a higher {"\n"} {"\n"}
                          capacity magazine {"\n"} {"\n"}
                          {"\t"}● Grips designed for handguns {"\n"} {"\n"}
                          {"\t"}● Holsters {"\n"} {"\n"}
                          {"\t"}● Laser sights that are properly labeled with laser strength and class {"\n"} {"\n"}
                          {"\t"}● Magazine floor plates/base plates {"\n"} {"\n"}
                          {"\t"}● Magazine loaders, except for those that can accommodate the following calibers: {"\n"} {"\n"}
                          .223/5.56; 7.62x51; 308; 7.62x39; 5.45x39 {"\n"} {"\n"}
                          {"\t"}● Reloading kits and equipment {"\n"} {"\n"}
                          {"\t"}● Scopes, including: {"\n"} {"\n"}
                          {"\t"}○ Night vision or infrared scopes. {"\n"} {"\n"}
                          {"\t"}● Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Shotgun choke tubes {"\n"} {"\n"}
                          {"\t"}● Single Picatinny or Weaver rails {"\n"} {"\n"} {"\n"}
                          {"\t"}● Suppressors or silencers that are permanently affixed to and are integral to the barrel of {"\n"} {"\n"}
                          airsoft, air, and paintball guns. {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Trigger guards {"\n"} {"\n"}
                          {"\t"}● Two point and traditional slings {"\n"} {"\n"}
                          {"\t"}● Vise blocks not intended for use with assault weapons {"\n"} {"\n"} {"\n"}
                          Knives and other bladed products {"\n"} {"\n"}
                          {"\t"}● Knives: typical hunting or kitchen knives, such as: {"\n"} {"\n"}
                          {"\t"}○ Hunting knives {"\n"} {"\n"}
                          {"\t"}○ Kitchen knives {"\n"} {"\n"}
                          {"\t"}○ Replica Swords {"\n"} {"\n"}
                          {"\t"}○ Machetes {"\n"} {"\n"}
                          {"\t"}○ Claw knives or karambit knives {"\n"} {"\n"}
                          {"\t"}○ Throwing spears and knives {"\n"} {"\n"}
                          {"\t"}○ Umbrella knives {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}○ Spring-assisted or assisted-opening knives that do not automatically extend {"\n"} {"\n"} {"\n"}
                          Other Products {"\n"} {"\n"}
                          {"\t"}● Blowguns {"\n"} {"\n"}
                          {"\t"}● Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Crossbows and other archery bows {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"} {"\n"}
                          {"\t"}● Nunchaku with sticks made completely of foam, sponge, rubber, or another bendable {"\n"} {"\n"}
                          substance {"\n"} {"\n"}
                          {"\t"}● Handcuffs {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Koga and kubotan {"\n"} {"\n"}
                          {"\t"}● Night vision accessories {"\n"} {"\n"}
                          {"\t"}● Pepper spray {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions and are not eligible for sale {"\n"} {"\n"}
                          {"\t"}● Products with monkey fist knots that are made entirely of rope {"\n"} {"\n"}
                          {"\t"}● Spearguns for spear-fishing {"\n"} {"\n"}
                          {"\t"}● Self-defense stinger tools {"\n"} {"\n"}
                          {"\t"}● Slingshots, including wrist-brace slingshots. {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Stun guns or electroshock weapons {"\n"} {"\n"}
                          Note: These are subject to geographic sales restrictions. {"\n"} {"\n"}
                          {"\t"}● Tactical Pens {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Firearm ammunition, such as: {"\n"} {"\n"}
                          {"\t"}○ Ammunition for pistols and rifles, including assault weapons {"\n"} {"\n"}
                          {"\t"}○ Slugs {"\n"} {"\n"}
                          {"\t"}○ Shotgun shells {"\n"} {"\n"}
                          {"\t"}○ Ballistic loads {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Hollowpoint {"\n"} {"\n"}
                          {"\t"}○ Round balls {"\n"} {"\n"}
                          {"\t"}● Ammunition components, such as: {"\n"} {"\n"}
                          {"\t"}○ Black powder {"\n"} {"\n"}
                          {"\t"}○ Blanks {"\n"} {"\n"}
                          {"\t"}○ Bullets {"\n"} {"\n"}
                          {"\t"}○ Bullet tips {"\n"} {"\n"}
                          {"\t"}○ Casings {"\n"} {"\n"}
                          {"\t"}○ Gunpowder {"\n"} {"\n"}
                          {"\t"}○ Primers {"\n"} {"\n"}
                          {"\t"}○ Shotgun Shells {"\n"} {"\n"}
                          {"\t"}○ Smokeless powder {"\n"} {"\n"} {"\n"}
                          Explosives {"\n"} {"\n"} {"\n"}
                          {"\t"}● Explosives, such as: {"\n"} {"\n"}
                          {"\t"}○ Fireworks {"\n"} {"\n"}
                          {"\t"}○ Flares, flare launchers, flare guns, flare gun receivers {"\n"} {"\n"}
                          {"\t"}○ Grenades, such as: {"\n"} {"\n"}
                          {"\t"}■ Military practice grenades {"\n"} {"\n"}
                          {"\t"}■ Rifle grenades {"\n"} {"\n"}
                          {"\t"}■ Smoke grenades {"\n"} {"\n"}
                          {"\t"}■ Metal replica grenades {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Grenade launchers {"\n"} {"\n"}
                          {"\t"}● Improvised explosive devices, including training aids, accessories, and certain explosive {"\n"} {"\n"}
                          precursors {"\n"} {"\n"}
                          {"\t"}● Mines {"\n"} {"\n"}
                          {"\t"}● Projectile and concussive products that employ gunpowder or explosives {"\n"} {"\n"} {"\n"}
                          Firearms {"\n"} {"\n"} {"\n"}
                          {"\t"}● Firearms, such as: {"\n"} {"\n"}
                          {"\t"}○ 3D printed guns and 3D gun blueprints {"\n"} {"\n"}
                          {"\t"}○ Antique and collectible guns {"\n"} {"\n"}
                          {"\t"}○ Assault weapons {"\n"} {"\n"}
                          {"\t"}○ Automatic weapons (e.g. machine guns) {"\n"} {"\n"}
                          {"\t"}○ Muzzleloaders and black powder guns {"\n"} {"\n"}
                          {"\t"}○ Rifles, shotguns, and handguns {"\n"} {"\n"}
                          {"\t"}○ Smartphone guns and other guns designed to resemble harmless items {"\n"} {"\n"}
                          {"\t"}○ Sport and hunting guns {"\n"} {"\n"}
                          {"\t"}○ Starter guns {"\n"} {"\n"}
                          {"\t"}○ Wallet guns {"\n"} {"\n"}
                          {"\t"}○ Zip guns {"\n"} {"\n"}
                          {"\t"}● Electronic guns {"\n"} {"\n"}
                          {"\t"}● Replicas of firearms that were manufactured after 1898 and/or are capable of firing fixed {"\n"} {"\n"}
                          ammunition {"\n"} {"\n"} {"\n"}
                          {"\t"}● Non-firing replicas of firearms that were manufactured after 1898 and lack the markings {"\n"} {"\n"}
                          required by 15 U.S.C. § 5001 {"\n"} {"\n"}
                          {"\t"}● Toy, imitation, or "look-alike" firearms that do not have required markings, including cell {"\n"} {"\n"}
                          phone cases and purses that resemble firearms {"\n"} {"\n"}
                          Firearm Accessories {"\n"} {"\n"} {"\n"}
                          {"\t"}● Assault weapon parts or accessories or products marketed as assault weapon parts or {"\n"} {"\n"}
                          accessories, including reference to various assault weapon models {"\n"} {"\n"}
                          {"\t"}● Parts or combination of parts designed or intended to convert a firearm into an assault {"\n"} {"\n"}
                          weapon {"\n"} {"\n"}
                          {"\t"}● Gun parts and accessories, even when designed or marketed for use with paintball, {"\n"} {"\n"}
                          airsoft, or air guns, such as: {"\n"} {"\n"}
                          {"\t"}○ Assault weapon conversion kits {"\n"} {"\n"}
                          {"\t"}○ Assault weapon grips, forend grips, front grips, and fore-grips {"\n"} {"\n"}
                          {"\t"}● Assault weapon and AR-specific tools, such as: {"\n"} {"\n"}
                          {"\t"}○ Vise blocks {"\n"} {"\n"}
                          {"\t"}○ AR-15/M16 armorer’s wrenches or combo wrenches {"\n"} {"\n"}
                          {"\t"}○ Bullet button tools {"\n"} {"\n"}
                          {"\t"}● Auto sears, such as: {"\n"} {"\n"}
                          {"\t"}○ Full auto sears (also known as drop-in auto sear or DIAS) {"\n"} {"\n"}
                          {"\t"}○ AR-15 auto sears {"\n"} {"\n"}
                          {"\t"}○ Auto sears II {"\n"} {"\n"}
                          {"\t"}● Barrels, barrel shrouds, barrel extenders, heat guards, hand-guards, and vent cooling {"\n"} {"\n"}
                          slots {"\n"} {"\n"}
                          {"\t"}● Battery assist levers {"\n"} {"\n"} {"\n"}
                          {"\t"}● Bayonet mounts and any device for attaching a bayonet to a firearm {"\n"} {"\n"}
                          {"\t"}● Camouflaging gun containers designed to fire without removing the gun from its {"\n"} {"\n"}
                          container {"\n"} {"\n"}
                          {"\t"}● Charging handles, latches, and assemblies {"\n"} {"\n"}
                          {"\t"}● Detachable carry handles and assault rifle carry handles {"\n"} {"\n"}
                          {"\t"}● Ejection port, ejection port covers, and dust covers {"\n"} {"\n"}
                          {"\t"}● Frames {"\n"} {"\n"}
                          {"\t"}● Gas blocks, gas tubes, and gas pistons {"\n"} {"\n"}
                          {"\t"}● Certain grips and grip accessories, such as: {"\n"} {"\n"}
                          {"\t"}○ Pistol grips designed for attachment to an assault weapon, rifle, or shotgun {"\n"} {"\n"}
                          {"\t"}○ Folding, vertical, and tactical grips {"\n"} {"\n"}
                          {"\t"}○ Angled foregrips and ambidextrous foregrips {"\n"} {"\n"}
                          {"\t"}○ Grip pods, funnels, and extensions {"\n"} {"\n"}
                          {"\t"}○ Hand stops {"\n"} {"\n"}
                          {"\t"}● Kits that can be used to create gun magazines {"\n"} {"\n"}
                          {"\t"}● Machine gun conversion kits and any part or combination of parts for converting a {"\n"} {"\n"}
                          firearm into an automatic weapon {"\n"} {"\n"}
                          {"\t"}● Magazines and related items, such as: {"\n"} {"\n"}
                          {"\t"}○ Magazine components or extensions {"\n"} {"\n"}
                          {"\t"}○ Magazine cinches, couplers, connectors, dual mag holders, and dual mag {"\n"} {"\n"}
                          clamps {"\n"} {"\n"} {"\n"}
                          {"\t"}● Magazine loaders that can accommodate the following calibers: .223/5.56; 7.62x51; 308; {"\n"} {"\n"}
                          7.62x39; 5.45x39 {"\n"} {"\n"}
                          {"\t"}● Multi-rail systems, including quad rails and rail covers, free-floating and drop-in {"\n"} {"\n"}
                          handguards {"\n"} {"\n"} {"\n"}
                          {"\t"}● Muzzle brakes (also known as muzzle tamers) {"\n"} {"\n"}
                          {"\t"}● Pistol stabilizing braces {"\n"} {"\n"}
                          {"\t"}● Pumpkin punchers {"\n"} {"\n"}
                          {"\t"}● Recoil compensators {"\n"} {"\n"}
                          {"\t"}● Receivers, including lower and upper receivers {"\n"} {"\n"}
                          {"\t"}● Receiver extensions and pins {"\n"} {"\n"}
                          {"\t"}● Receiver wedges or buffers that tighten loose upper and lower receivers {"\n"} {"\n"}
                          {"\t"}● AR-specific jig kits {"\n"} {"\n"}
                          {"\t"}● Revolving cylinder for a shotgun Selector levers, detents, and switches {"\n"} {"\n"}
                          {"\t"}● Single point, 3-point, tactical, and quick detach slings {"\n"} {"\n"}
                          {"\t"}● Sling plates and end plates {"\n"} {"\n"}
                          {"\t"}● Slide fire stock and weapons kits, such as:{"\n"} {"\n"}
                          {"\t"}○ Bump firing devices, stocks, and kits {"\n"} {"\n"}
                          {"\t"}● Solvent trap oil filter adapters, solvent traps, and homemade silencers, and component {"\n"} {"\n"}
                          parts for solvent traps and homemade silencers {"\n"} {"\n"}
                          {"\t"}● Any non-fixed stock, including telescoping, folding or collapsible stocks {"\n"} {"\n"}
                          {"\t"}● Silencers or suppressors, including: {"\n"} {"\n"}
                          {"\t"}○ Flash suppressors {"\n"} {"\n"}
                          {"\t"}○ Sound suppressors {"\n"} {"\n"}
                          {"\t"}○ Suppressors marketed or intended for airsoft, air guns, or paintball guns {"\n"} {"\n"}
                          {"\t"}○ Fake, mock, or faux silencers or suppressors {"\n"} {"\n"}
                          {"\t"}● Takedown pins, pivot pins, cam pins, and roll pins {"\n"} {"\n"}
                          {"\t"}● Threaded barrel adapters {"\n"} {"\n"}
                          {"\t"}● Thumbhole stocks {"\n"} {"\n"}
                          {"\t"}● Trigger activators, such as: {"\n"} {"\n"}
                          {"\t"}○ Multi-burst trigger activators {"\n"} {"\n"}
                          {"\t"}○ Trigger attachments {"\n"} {"\n"}
                          {"\t"}○ Devices that facilitate so-called "bump-firing" {"\n"} {"\n"}
                          {"\t"}● All internal/moving gun parts, even when designed or marketed for use with paintball, {"\n"} {"\n"}
                          airsoft, or air guns, such as: {"\n"} {"\n"}
                          {"\t"}○ Bolt catch releases, assemblies, actuators, and battery assist levers {"\n"} {"\n"}
                          {"\t"}○ Bolt carriers, bolt pins, and buffer pins {"\n"} {"\n"}
                          {"\t"}○ Buffer tubes, extension tubes, recoil buffers, and related parts {"\n"} {"\n"}
                          {"\t"}○ Firing pins, springs, and stops {"\n"} {"\n"}
                          {"\t"}○ Guide rods {"\n"} {"\n"}
                          {"\t"}○ Trigger, trigger assemblies, or fire control groups {"\n"} {"\n"}
                          {"\t"}○ Upgrade parts and replacement parts {"\n"} {"\n"}
                          {"\t"}○ Anti-walk pins {"\n"} {"\n"} {"\n"}
                          Knives and other bladed products {"\n"} {"\n"}
                          {"\t"}● Automatic knives, also known as switchblade knives {"\n"} {"\n"}
                          {"\t"}● Balisong knives, also known as butterfly knives, including some training knives {"\n"} {"\n"}
                          {"\t"}● Concealed swords, including: {"\n"} {"\n"}
                          {"\t"}○ Cane-swords {"\n"} {"\n"}
                          {"\t"}○ Shobi-zue (a staff, crutch, stick, rod, or pole concealing a knife or blade within it) {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Swordsticks {"\n"} {"\n"}
                          {"\t"}● Gravity or gravity-assisted knives {"\n"} {"\n"}
                          {"\t"}● Knives disguised to look like harmless items, such as: {"\n"} {"\n"}
                          {"\t"}○ Air gauge knives {"\n"} {"\n"}
                          {"\t"}○ Belt buckle knives {"\n"} {"\n"}
                          {"\t"}○ Comb knives or hairbrush knives {"\n"} {"\n"}
                          {"\t"}○ Lipstick case knives {"\n"} {"\n"}
                          {"\t"}○ Writing penknives {"\n"} {"\n"}
                          {"\t"}● Knuckle knives, including: {"\n"} {"\n"}
                          {"\t"}○ Fixed or folding blade knives with split knuckle duster handles {"\n"} {"\n"}
                          {"\t"}● Pilum ballistic knives {"\n"} {"\n"}
                          {"\t"}● Push daggers, including: {"\n"} {"\n"}
                          {"\t"}○ Punch daggers {"\n"} {"\n"}
                          {"\t"}○ Punch knives {"\n"} {"\n"}
                          {"\t"}○ Push dirks {"\n"} {"\n"}
                          {"\t"}○ T-handled knives {"\n"} {"\n"}
                          {"\t"}● "Slasher" knives or other devices used in cockfighting {"\n"} {"\n"}
                          {"\t"}● Spring-loaded knives {"\n"} {"\n"}
                          {"\t"}● Stiletto Knives {"\n"} {"\n"}
                          {"\t"}● Throwing stars, including: {"\n"} {"\n"}
                          {"\t"}○ Batman symbols {"\n"} {"\n"}
                          {"\t"}○ Cyclone knives {"\n"} {"\n"}
                          {"\t"}○ Kung Fu stars {"\n"} {"\n"}
                          {"\t"}○ Metal throwing stars {"\n"} {"\n"}
                          {"\t"}○ Ninja stars {"\n"} {"\n"}
                          {"\t"}○ Shirken {"\n"} {"\n"}
                          {"\t"}○ Throwing cards {"\n"} {"\n"} {"\n"}
                          Other Products {"\n"} {"\n"} {"\n"}
                          {"\t"}● Body armor and any product including or designed for the insertion of body armor plates. {"\n"} {"\n"}
                          This includes bulletproof or ballistic helmets and clothing. {"\n"} {"\n"}
                          {"\t"}● Caltrops {"\n"} {"\n"}
                          {"\t"}● Certain martial arts items, such as: {"\n"} {"\n"}
                          {"\t"}○ Brass, plastic, or metal knuckles, including spiked keychains and rings or any {"\n"} {"\n"}
                          product held over one’s knuckles that could be used to strike someone {"\n"} {"\n"}
                          {"\t"}○ Leaded canes, staffs, crutches, and sticks {"\n"} {"\n"}
                          {"\t"}○ Nunchaku (also known as nunchucks or Chuka sticks) {"\n"} {"\n"}
                          {"\t"}○ Sandclubs, sandbags, or slungshots (also known as saps or blackjacks) {"\n"} {"\n"}
                          {"\t"}○ Throwing stars (also known as shirkens or shurikens) {"\n"} {"\n"}
                          {"\t"}○ Tonfas {"\n"} {"\n"}
                          {"\t"}○ Kusari-fundo (also known as manrikis) {"\n"} {"\n"}
                          {"\t"}● Clubs and blunt force striking instruments, including items marketed for striking a person {"\n"} {"\n"}
                          or animal, such as: {"\n"} {"\n"}
                          {"\t"}○ Billy, billy clubs, and police batons {"\n"} {"\n"}
                          {"\t"}○ Escrima sticks {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Rattan canes (when designed or intended as a striking weapon) {"\n"} {"\n"}
                          {"\t"}○ Bo staffs and jo staffs {"\n"} {"\n"}
                          {"\t"}○ Truncheons, nightsticks, and batons {"\n"} {"\n"}
                          {"\t"}○ Sjamboks {"\n"} {"\n"}
                          {"\t"}○ Weighted clubs {"\n"} {"\n"}
                          {"\t"}○ Blackjacks {"\n"} {"\n"}
                          {"\t"}○ Bludgeons {"\n"} {"\n"}
                          {"\t"}○ Sap hats, also known as slap hats {"\n"} {"\n"}
                          {"\t"}● Weapons designed to be concealed {"\n"} {"\n"}
                          {"\t"}● Weapons or other items intended solely for law enforcement or military use {"\n"} {"\n"} {"\n"}
                          Food & Beverage {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Properly prepared and sealed foods and beverages that are appropriately packaged and {"\n"} {"\n"}
                          labeled. Products need to be shipped appropriately to protect the quality and safety of {"\n"} {"\n"}
                          the food, to avoid spoilage, melting, damage, and so on. All food packaging must include {"\n"} {"\n"}
                          the following: {"\n"} {"\n"}
                          {"\t"}○ Product name {"\n"} {"\n"}
                          {"\t"}○ Name and address of the packaging company {"\n"} {"\n"}
                          {"\t"}○ Expiration date {"\n"} {"\n"}
                          {"\t"}○ Net content quantity and weight (the weight of the contents, not including any {"\n"} {"\n"}
                          packaging) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Lot-controlled products with a shelf-life of greater than 90 days. {"\n"} {"\n"}
                          {"\t"}○ Perishable products must be removed from inventory 50 days before the {"\n"} {"\n"}
                          expiration date. {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Unregistered infant formula {"\n"} {"\n"}
                          {"\t"}● Goat's milk infant formula {"\n"} {"\n"}
                          {"\t"}● HIPP formula and milk products {"\n"} {"\n"}
                          {"\t"}● Products containing any meat imported from China {"\n"} {"\n"}
                          {"\t"}● Betel nut products or products containing betel nut flavoring {"\n"} {"\n"}
                          {"\t"}● Kaffir lime leaves imported from Thailand {"\n"} {"\n"}
                          {"\t"}● Listings containing drug claims {"\n"} {"\n"}
                          {"\t"}● Listings that claim the item is intended for use in the diagnosis, cure, mitigation, {"\n"} {"\n"}
                          treatment, or prevention of disease in humans and/or animals {"\n"} {"\n"}
                          {"\t"}● Unpasteurized dairy products {"\n"} {"\n"}
                          {"\t"}● Fruit or vegetable juice that hasn't been heat pasteurized {"\n"} {"\n"}
                          {"\t"}● Wild mushrooms {"\n"} {"\n"}
                          {"\t"}● Ackee fruit (including seeds) {"\n"} {"\n"}
                          {"\t"}● Food items that are subject to recall by the FDA {"\n"} {"\n"}
                          {"\t"}● Expired food items {"\n"} {"\n"}
                          {"\t"}● Meals Ready to Eat (MREs) that have been expired for more than 3 years {"\n"} {"\n"}
                          {"\t"}● Government assistance benefits including, but not limited to: {"\n"} {"\n"}
                          {"\t"}○ Special Supplemental Nutrition Program for Women, Infants, and Children (WIC) {"\n"} {"\n"}
                          {"\t"}○ Supplemental foods such as infant formula and baby food {"\n"} {"\n"}
                          {"\t"}○ Cash-value vouchers {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Food instruments such as vouchers, checks, electronic benefit transfer (EBT) {"\n"} {"\n"}
                          cards, and coupons {"\n"} {"\n"} {"\n"}
                          Shipping and Labeling Requirements {"\n"} {"\n"}
                          Each shipment of produce must have: {"\n"} {"\n"}
                          (1) the name and address of the shipper or owner; {"\n"} {"\n"}
                          (2) the name of the person to whom the shipment is being sent; {"\n"} {"\n"}
                          (3) the name of the country, state, or territory where the contents were grown; {"\n"} {"\n"}
                          (4) a statement of the contents. State and local governments may have additional requirements {"\n"} {"\n"}
                          for both interstate and intrastate shipment and labeling of certain produce. {"\n"} {"\n"} {"\n"}
                          Gambling & Lottery {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Non-functional slot machines created solely for display or as toys{"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Lottery tickets {"\n"} {"\n"}
                          {"\t"}● Coin-operated slot machines {"\n"} {"\n"}
                          {"\t"}● Slot machines that can be converted to use coins or currency {"\n"} {"\n"} {"\n"}
                          Gift cards {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Gift cards for a travel agency {"\n"} {"\n"} {"\n"}
                          {"\t"}● Paper-based gift certificates {"\n"} {"\n"}
                          {"\t"}● Phone cards for minutes {"\n"} {"\n"} {"\n"}
                          Requirements in Posting Gift Cards {"\n"} {"\n"}
                          {"\t"}● The total face value of all gift cards listed by a seller at a given time is $500 or less {"\n"} {"\n"}
                          {"\t"}● You have the card in hand at the time you list it, and you must ship within 5 days of the {"\n"} {"\n"}
                          date your item was purchased {"\n"} {"\n"}
                          Note: At our discretion, we may approve authorized resellers or direct suppliers to {"\n"} {"\n"}
                          list gift cards without these restrictions {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Electronically delivered gift cards {"\n"} {"\n"}
                          {"\t"}● Prepaid credit cards (or money cards) from American Express, Discover, MasterCard, or {"\n"} {"\n"}
                          Visa {"\n"} {"\n"}
                          {"\t"}● Store or merchandise credit in the form of a receipt{"\n"} {"\n"}
                          Note: We allow approved third-party retailers or service providers to sell {"\n"} {"\n"}
                          electronically-delivered gift cards on ReferApps, provided they meet specific {"\n"} {"\n"}
                          additional requirements. {"\n"} {"\n"} {"\n"}
                          Government, transit, and shipping-related items {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Halloween costumes designed to look like mail carriers (a U.S. Postal Service (USPS) {"\n"} {"\n"}
                          outfit, for example) {"\n"} {"\n"}
                          {"\t"}● Promotional merchandise bearing the logo of a shipping company like Federal Express {"\n"} {"\n"}
                          (FedEx), DHL, or United Parcel Service (UPS) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Collectible Transit Authority novelty items like maps, posters, tokens, and other items {"\n"} {"\n"}
                          that are generally available to the public {"\n"} {"\n"}
                          {"\t"}● Expired New York City Metro cards (the expiration date must be clearly stated in the {"\n"} {"\n"}
                          listing) {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Blueprints or schematics of airports, subway systems, or train stations {"\n"} {"\n"}
                          {"\t"}● Classified or restricted government documents (see also our government documents, {"\n"} {"\n"}
                          IDs, and licenses policy) {"\n"} {"\n"}
                          {"\t"}● Design plans of commercial airlines or plane engines {"\n"} {"\n"}
                          {"\t"}● Internal, not-for-public documents, manuals, and other materials from companies that {"\n"} {"\n"}
                          provide public transportation, including airlines, buses, ferries, subways, and trains {"\n"} {"\n"}
                          {"\t"}● Current airline uniforms and related items, including: {"\n"} {"\n"}
                          {"\t"}○ Flight attendant, pilot, or other flight crew uniforms {"\n"} {"\n"}
                          {"\t"}○ Baggage attendant gate agent, and other ground crew uniforms {"\n"} {"\n"}
                          {"\t"}○ Uniform components like badges, wings, and other accessories {"\n"} {"\n"}
                          {"\t"}● Current public transportation uniforms and accessories like hats, pins, scarves, and ties. {"\n"} {"\n"}
                          This covers items from any company that provides public transportation, including {"\n"} {"\n"}
                          buses, ferries, subways, and trains {"\n"} {"\n"}
                          {"\t"}● Government uniforms and accessories, including badges, hats, and patches. This {"\n"} {"\n"}
                          covers items from the Architect of the Capitol (AOC) and the U.S. Senate {"\n"} {"\n"}
                          {"\t"}● New York City Transit (NYCT) uniforms and accessories like badges, hats, patches, and {"\n"} {"\n"}
                          pins, regardless of how old they are {"\n"} {"\n"}
                          {"\t"}● Shipping company uniforms, including those from DHL, Federal Express (FedEx), and {"\n"} {"\n"}
                          United Parcel Service (UPS) {"\n"} {"\n"}
                          {"\t"}● Transportation Security Administration (TSA) uniforms, and accessories like badges {"\n"} {"\n"}
                          (see also our police-related items policy) {"\n"} {"\n"}
                          {"\t"}● U.S. Postal Service (USPS) uniforms, including badges. This applies to old USPS items {"\n"} {"\n"}
                          {"\t"}● Items with the Automotive Service Excellence (ASE) Certified emblems, such as patches {"\n"} {"\n"}
                          and signage {"\n"} {"\n"}
                          {"\t"}● Keys, locks, tools, or anything (regardless of how old the item is) that can control transit {"\n"} {"\n"}
                          vehicles. Examples include railway switch keys or switch lock keys {"\n"} {"\n"}
                          {"\t"}● Shipping company decals, logos, or delivery vehicles like those from Federal Express {"\n"} {"\n"}
                          (FedEx), DHL, or United Parcel Service (UPS) {"\n"} {"\n"}
                          {"\t"}● U.S. Postal Service (USPS) mailbags, mail bins, and delivery vehicles {"\n"} {"\n"}
                          {"\t"}● Valid New York City Metro cards {"\n"} {"\n"}
                          {"\t"}● Library of Congress National Library of Service record players, cassette players, digital {"\n"} {"\n"}
                          talking book machines and accessories labeled with the Library of Congress National {"\n"} {"\n"}
                          Library of Service logo {"\n"} {"\n"} {"\n"}
                          Government documents, IDs and licenses {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Antique (generally more than 100 years old) government documents such as birth {"\n"} {"\n"}
                          certificates, marriage licenses, and ship captain's licenses {"\n"} {"\n"}
                          {"\t"}● Expired U.S. passports that were issued more than 20 years prior to the date of the sale {"\n"} {"\n"}
                          Services for obtaining a license {"\n"} {"\n"}
                          {"\t"}● Collectible vehicle license plates that are at least 3 years old. Sellers need to clearly {"\n"} {"\n"}
                          state the plate's age in their listing description {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● AutoCheck or Carfax reports {"\n"} {"\n"}
                          {"\t"}● Birth certificates or a completed application for a birth certificate {"\n"} {"\n"}
                          {"\t"}● Classified or restricted government documents {"\n"} {"\n"}
                          {"\t"}● Current vehicle license plates and plates that look like current license plates {"\n"} {"\n"}
                          {"\t"}● Documents that are designed to look like official documents but are actually fake {"\n"} {"\n"}
                          {"\t"}● Drivers' licenses or completed applications for a driver's license {"\n"} {"\n"}
                          {"\t"}● Fake certificates or diplomas {"\n"} {"\n"}
                          {"\t"}● Fake IDs such as licenses or passports {"\n"} {"\n"}
                          {"\t"}● Government vehicle license plates {"\n"} {"\n"}
                          {"\t"}● Government-issued medals and certificates for medals, including the Air Force Cross, {"\n"} {"\n"}
                          {"\t"}● Congressional Medal of Honor, Distinguished Service Cross, Navy Cross, Purple Heart, {"\n"} {"\n"}
                          or Silver Star. This also applies to a medal's associated buttons, ribbons, or rosettes {"\n"} {"\n"}
                          {"\t"}● Items that are used to create or modify IDs, government documents, licenses, or plates {"\n"} {"\n"}
                          {"\t"}● Passports or a completed application for a passport {"\n"} {"\n"}
                          {"\t"}● Vehicle Identification Number (VIN) plates {"\n"} {"\n"}
                          {"\t"}● VIN plate rivets {"\n"} {"\n"}
                          {"\t"}● Vehicle titles or vehicle title stocks (although a vehicle title that's being sold with a {"\n"} {"\n"}
                          vehicle is OK) {"\n"} {"\n"} {"\n"}
                          Hazardous and Dangerous Items {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Batteries that do not contain mercury {"\n"} {"\n"}
                          {"\t"}● Rechargeable batteries {"\n"} {"\n"}
                          {"\t"}● Portable fuel containers certified by the United States Environmental Protection Agency {"\n"} {"\n"}
                          (EPA) {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"} {"\n"}
                          {"\t"}● Products containing Bisphenol A (BPA) {"\n"} {"\n"}
                          {"\t"}● Items containing Carbon Tetrachloride, such as: {"\n"} {"\n"}
                          {"\t"}○ Fire extinguishers {"\n"} {"\n"}
                          {"\t"}○ Refrigerants {"\n"} {"\n"}
                          {"\t"}○ Cleaning agents {"\n"} {"\n"}
                          {"\t"}● Any chemical substance or compound that is intended for commercial, industrial, or {"\n"} {"\n"}
                          professional use only and is not available for general consumer purchase {"\n"} {"\n"}
                          {"\t"}● Explosives, such as: {"\n"} {"\n"}
                          {"\t"}○ Black powder {"\n"} {"\n"}
                          {"\t"}○ Caps for toy guns {"\n"} {"\n"}
                          {"\t"}○ Explosive fuses {"\n"} {"\n"}
                          {"\t"}○ Exploding rifle targets {"\n"} {"\n"}
                          {"\t"}○ Fireworks, such as: {"\n"} {"\n"}
                          {"\t"}■ Firecrackers {"\n"} {"\n"}
                          {"\t"}■ Firework kits {"\n"} {"\n"}
                          {"\t"}■ Aerial bombs {"\n"} {"\n"}
                          {"\t"}■ Bottle rockets {"\n"} {"\n"}
                          {"\t"}■ Party poppers {"\n"} {"\n"}
                          {"\t"}■ Roman candles {"\n"} {"\n"}
                          {"\t"}■ Smoke bombs {"\n"} {"\n"}
                          {"\t"}■ Snap caps {"\n"} {"\n"}
                          {"\t"}■ Sparklers {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Flares, such as projectile and road flares {"\n"} {"\n"}
                          {"\t"}○ Flash paper {"\n"} {"\n"}
                          {"\t"}○ Gasoline {"\n"} {"\n"}
                          {"\t"}● Bacteria cultures or other products containing E coli or Escherichia coli {"\n"} {"\n"}
                          {"\t"}● Hydrofluoric acid {"\n"} {"\n"}
                          {"\t"}● Inflatable Neck Floats for children {"\n"} {"\n"}
                          {"\t"}● Information on how to make explosive devices, such as bombs {"\n"} {"\n"}
                          {"\t"}● Kite strings that are intended for kite fighting {"\n"} {"\n"}
                          {"\t"}● Military-style gas masks and their filters {"\n"} {"\n"}
                          {"\t"}● Nitric acid {"\n"} {"\n"}
                          {"\t"}● Products containing red phosphorous {"\n"} {"\n"}
                          {"\t"}● Products containing thermite {"\n"} {"\n"}
                          {"\t"}● Products containing tritium that do not comply with the regulations of the United States {"\n"} {"\n"}
                          Nuclear Regulatory Commission {"\n"} {"\n"}
                          {"\t"}● Products that do not comply with the Safe Drinking Water Act {"\n"} {"\n"}
                          {"\t"}● Used oil, such as cooking oil or motor oil {"\n"} {"\n"}
                          {"\t"}● Water walking balls {"\n"} {"\n"}
                          {"\t"}● Products contaminated by radiation {"\n"} {"\n"}
                          {"\t"}● Liquid mercury and products containing mercury, such as: {"\n"} {"\n"}
                          {"\t"}○ Automotive switches, relays, and diostats {"\n"} {"\n"}
                          {"\t"}○ Batteries, with the exception of alkaline-manganese button cell batteries {"\n"} {"\n"}
                          containing up to 25 mg of mercury {"\n"} {"\n"}
                          {"\t"}○ Manometers, sphygmomanometers, and other medical devices containing {"\n"} {"\n"}
                          mercury {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Mercury-added consumer novelty products such as toys, games, cards, jewelry, {"\n"} {"\n"}
                          apparel, and footwear {"\n"} {"\n"}
                          {"\t"}○ Thermometers {"\n"} {"\n"}
                          {"\t"}○ Thermostats {"\n"} {"\n"}
                          {"\t"}○ Wheel weights {"\n"} {"\n"}
                          {"\t"}● Products containing cyanide {"\n"} {"\n"}
                          {"\t"}● Products that include large quantities of magnetic balls or cubes, such as: {"\n"} {"\n"}
                          {"\t"}○ Buckyballs {"\n"} {"\n"}
                          {"\t"}○ Buckycubes {"\n"} {"\n"}
                          {"\t"}○ Dynocube {"\n"} {"\n"}
                          {"\t"}○ Magnet Balls {"\n"} {"\n"}
                          {"\t"}○ Magnicube {"\n"} {"\n"}
                          {"\t"}○ Neocubes {"\n"} {"\n"}
                          {"\t"}○ Neocubix {"\n"} {"\n"}
                          {"\t"}○ Nanodots {"\n"} {"\n"}
                          {"\t"}● Prohibited ozone-depleting substances (ODS), such as: {"\n"} {"\n"}
                          {"\t"}○ Appliances prohibited by EPA because they contain certain ozone-depleting {"\n"} {"\n"}
                          refrigerants {"\n"} {"\n"}
                          {"\t"}○ Class I and Class II ODS prohibited by EPA, as well as blends of prohibited {"\n"} {"\n"}
                          ODS, and products containing prohibited ODS {"\n"} {"\n"}
                          {"\t"}○ Substitutes for Class I or Class II ODS that are not reviewed and approved by {"\n"} {"\n"}
                          EPA in accordance with the Significant New Alternative Policy Program, as well {"\n"} {"\n"}
                          as substitute refrigerants that are subject to sales restrictions under EPA {"\n"} {"\n"}
                          regulations and are not eligible for any exemption {"\n"} {"\n"}
                          {"\t"}● Vehicle airbags and airbag covers {"\n"} {"\n"}
                          {"\t"}● Toy crossbows that have the capability of shooting small, sharp projectiles (e.g., {"\n"} {"\n"}
                          toothpicks, pins) {"\n"} {"\n"} {"\n"}
                          Human Parts & Burial Artifacts {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Human skulls, bones, and skeletons that are for medical purposes and are permitted for {"\n"} {"\n"}
                          sale in the area that the product is sold {"\n"} {"\n"}
                          {"\t"}● Human hair, such as wigs {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Human body parts, such as: {"\n"} {"\n"}
                          {"\t"}○ Blood {"\n"} {"\n"}
                          {"\t"}○ Eggs {"\n"} {"\n"}
                          {"\t"}○ Organs {"\n"} {"\n"}
                          {"\t"}○ Sperm {"\n"} {"\n"}
                          {"\t"}○ Tissue {"\n"} {"\n"}
                          {"\t"}● Human remains {"\n"} {"\n"}
                          {"\t"}● Human waste and bodily fluids {"\n"} {"\n"}
                          {"\t"}● Historical grave markers or tombstones {"\n"} {"\n"}
                          {"\t"}● Native American burial items {"\n"} {"\n"}
                          {"\t"}● Artifacts removed from Native American land, federal or state land, or a battlefield site {"\n"} {"\n"} {"\n"}
                          Jewelry & Precious Gems {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Gold or silver products that are not stamped in compliance with applicable laws {"\n"} {"\n"}
                          {"\t"}● Products that do not comply with the FTC’s Guides for the Jewelry, Precious Metals, and {"\n"} {"\n"}
                          Pewter Industries {"\n"} {"\n"}
                          {"\t"}● Irradiated gemstones, unless the sale has been authorized by the Nuclear Regulatory {"\n"} {"\n"}
                          Commission {"\n"} {"\n"}
                          {"\t"}● Diamonds that do not comply with the Kimberley Process Certification Scheme {"\n"} {"\n"}
                          {"\t"}● Clarity-enhanced white diamonds {"\n"} {"\n"}
                          {"\t"}● Glass-filled rubies {"\n"} {"\n"} {"\n"}
                          Lighting {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Lighting products that comply with all applicable federal, state, and local laws and are {"\n"} {"\n"}
                          not otherwise prohibited by ReferApps policy {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listings {"\n"} {"\n"}
                          {"\t"}● Incandescent light bulbs that violate applicable energy efficiency standards. {"\n"} {"\n"} {"\n"}
                          Lock Picking & Theft Devices {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Blank keys {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Lockpicking or locksmithing devices, such as: {"\n"} {"\n"}
                          {"\t"}● Autolock bypass keys or jigglers {"\n"} {"\n"}
                          {"\t"}● Concealed handcuff keys {"\n"} {"\n"}
                          {"\t"}● Digital decoders {"\n"} {"\n"}
                          {"\t"}● Lockpicking cards and lock picking guns {"\n"} {"\n"}
                          {"\t"}● Lockpicking sets {"\n"} {"\n"}
                          {"\t"}● Slim-jims {"\n"} {"\n"}
                          {"\t"}● Tension bars {"\n"} {"\n"}
                          {"\t"}● Training locks {"\n"} {"\n"}
                          {"\t"}● Try-out keys {"\n"} {"\n"}
                          {"\t"}● Tubular lock picks {"\n"} {"\n"}
                          {"\t"}● Devices designed to duplicate a key {"\n"} {"\n"}
                          {"\t"}● Shoplifting devices, including Sensormatic detachers {"\n"} {"\n"}
                          {"\t"}● Card skimming devices {"\n"} {"\n"}
                          {"\t"}● Code grabbing devices {"\n"} {"\n"}
                          {"\t"}● Master keys or skeleton keys {"\n"} {"\n"} {"\n"}
                          Medical devices and accessories {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"} {"\n"}
                          {"\t"}● Medical devices that are authorized by the Food and Drug Administration (FDA) for over- {"\n"} {"\n"}
                          the-counter purchase that are not otherwise restricted and are appropriately described {"\n"} {"\n"}
                          and labeled {"\n"} {"\n"}
                          {"\t"}● Eyeglass frames {"\n"} {"\n"}
                          {"\t"}● Tanning devices {"\n"} {"\n"}
                          {"\t"}● Otoscopes {"\n"} {"\n"}
                          {"\t"}● Ionized or ionic bracelets {"\n"} {"\n"}
                          {"\t"}● Personal Sound Amplification Products (PSAPs) {"\n"} {"\n"} {"\n"}
                          Allowed in Listing (Participants of Professional Health Care Program) {"\n"} {"\n"} {"\n"}
                          Note: The following listings are prohibited for any seller not authorized to sell restricted {"\n"} {"\n"}
                          medical products through the Professional Health Care Program. {"\n"} {"\n"}
                          {"\t"}● Products that require a prescription or a medical professional’s supervision or direction {"\n"} {"\n"}
                          for their use {"\n"} {"\n"}
                          {"\t"}● Products that are labeled for professional use only or not for retail sale {"\n"} {"\n"}
                          {"\t"}● Products that have been classified by the FDA as "Class I," "Class II," or "Class III" {"\n"} {"\n"}
                          medical devices that require FDA clearance or approval that has not been cleared or {"\n"} {"\n"}
                          approved by the FDA for over-the-counter use, such as: {"\n"} {"\n"}
                          {"\t"}○ Acupuncture/intradermal needles {"\n"} {"\n"}
                          {"\t"}○ Anesthetic vaporizers {"\n"} {"\n"}
                          {"\t"}○ Asthma inhalers {"\n"} {"\n"}
                          {"\t"}○ Bacteriostatic water {"\n"} {"\n"}
                          {"\t"}○ Cancer tests {"\n"} {"\n"}
                          {"\t"}○ Cardiac monitor {"\n"} {"\n"}
                          {"\t"}○ Cavity varnish {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Chin-up strip {"\n"} {"\n"}
                          {"\t"}○ Circumcision devices {"\n"} {"\n"}
                          {"\t"}○ Contact lens cases (unless approved for over-the-counter use) {"\n"} {"\n"}
                          {"\t"}○ Contact lenses, including both cosmetic and corrective lenses. {"\n"} {"\n"}
                          {"\t"}○ Continuous, Variable and Bilevel Positive Airway Pressure (CPAP, VPAP, {"\n"} {"\n"}
                          BiPAP) devices and certain accessories {"\n"} {"\n"}
                          {"\t"}○ Cranial electrotherapy stimulators {"\n"} {"\n"}
                          {"\t"}○ Defibrillators (unless approved for over-the-counter use) {"\n"} {"\n"}
                          {"\t"}■ See examples of prohibited defibrillators below. {"\n"} {"\n"}
                          {"\t"}○ Prescription dental devices, such as: {"\n"} {"\n"}
                          {"\t"}■ Dental burs {"\n"} {"\n"}
                          {"\t"}■ Dental X-ray units {"\n"} {"\n"}
                          {"\t"}■ Denture relining, repairing, or rebasing resin {"\n"} {"\n"}
                          {"\t"}■ Dental handpiece or dental drill {"\n"} {"\n"}
                          {"\t"}○ Electrical Muscle Stimulation (EMS) units and their accessories {"\n"} {"\n"}
                          {"\t"}○ Electronic Stethoscopes {"\n"} {"\n"}
                          {"\t"}○ Electrosurgical cutting and coagulation devices {"\n"} {"\n"}
                          {"\t"}○ Fetal dopplers {"\n"} {"\n"}
                          {"\t"}○ Home HIV test kits {"\n"} {"\n"}
                          {"\t"}■ With the exception of the Home Access HIV-1 Test System, which has {"\n"} {"\n"}
                          been approved for over-the-counter use by the FDA {"\n"} {"\n"}
                          {"\t"}○ Hypodermic needles and insulin injectors, such as: {"\n"} {"\n"}
                          {"\t"}■ Monject Safety Syringes {"\n"} {"\n"} {"\n"}
                          {"\t"}○ Infant heel warmers {"\n"} {"\n"}
                          {"\t"}○ Implantable devices, such as pacemakers {"\n"} {"\n"}
                          {"\t"}○ Infusion pumps and their accessories (with exception of batteries, which are {"\n"} {"\n"}
                          permitted) {"\n"} {"\n"}
                          {"\t"}○ Infusion set {"\n"} {"\n"}
                          {"\t"}○ Insulin {"\n"} {"\n"}
                          {"\t"}○ Insulin pumps {"\n"} {"\n"}
                          {"\t"}○ Laser combs {"\n"} {"\n"}
                          {"\t"}○ Light therapy devices, such as: {"\n"} {"\n"}
                          {"\t"}■ Pain X {"\n"} {"\n"}
                          {"\t"}○ Mandibular advancement devices {"\n"} {"\n"}
                          {"\t"}○ Mesotherapy products {"\n"} {"\n"}
                          {"\t"}○ Nebulizers and their accessories, such as holding chambers {"\n"} {"\n"}
                          {"\t"}○ Oral cavity abrasive polishing agents {"\n"} {"\n"}
                          {"\t"}○ Oxygen concentrators, compressors, conservers, generators, condensers and {"\n"} {"\n"}
                          their accessories, such as: {"\n"} {"\n"}
                          {"\t"}■ Hyperbaric chambers {"\n"} {"\n"}
                          {"\t"}○ Penis enlargement devices {"\n"} {"\n"}
                          {"\t"}○ Pinhole eyewear that makes unapproved medical claims {"\n"} {"\n"}
                          {"\t"}○ Prescription eyewear {"\n"} {"\n"}
                          {"\t"}○ Prescription toothpaste, such as: {"\n"} {"\n"}
                          {"\t"}■ Colgate PreviDent {"\n"} {"\n"}
                          {"\t"}○ Polar Care Cold Therapy System products manufactured by BREG {"\n"} {"\n"}
                          {"\t"}○ Pulse oximeters (unless marketed for "sports or aviation" use only or "not for {"\n"} {"\n"}
                          medical use") {"\n"} {"\n"}
                          {"\t"}○ Radiesse {"\n"} {"\n"}
                          {"\t"}○ Resin tooth bonding agents {"\n"} {"\n"}
                          {"\t"}○ Ringers solution {"\n"} {"\n"}
                          {"\t"}○ Seizure bite sticks {"\n"} {"\n"}
                          {"\t"}○ Skin glue {"\n"} {"\n"}
                          {"\t"}○ Surgical kits {"\n"} {"\n"}
                          {"\t"}○ Surgical sutures {"\n"} {"\n"}
                          {"\t"}○ Transcutaneous Electrical Nerve Stimulators (TENS) machines and their {"\n"} {"\n"}
                          accessories {"\n"} {"\n"}
                          {"\t"}○ Ultrasound therapy and ultrasound pain relief devices {"\n"} {"\n"}
                          {"\t"}○ Vaginal pessary devices {"\n"} {"\n"}
                          {"\t"}○ Ventilator machines {"\n"} {"\n"}
                          {"\t"}○ Ventros Immunodiagnostic, Integrated, and Chemistry Systems {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products that have been altered to change the product's performance, safety {"\n"} {"\n"}
                          specifications, or indications of use {"\n"} {"\n"}
                          {"\t"}● Products that the Food and Drug Administration (FDA) has determined present an {"\n"} {"\n"}
                          unreasonable risk of injury or illness, such as: {"\n"} {"\n"}
                          {"\t"}○ Ear candle {"\n"} {"\n"}
                          {"\t"}○ Zappers {"\n"} {"\n"}
                          {"\t"}● Products for which a premarket notification is required but has not yet been filed {"\n"} {"\n"}
                          {"\t"}● Products that have passed their expiration or "use by" dates {"\n"} {"\n"} {"\n"}
                          {"\t"}● Products labeled as "tester," "not for retail sale," or "not intended for resale" {"\n"} {"\n"}
                          {"\t"}● Certain medical devices regardless of FDA clearance or approval, such as: {"\n"} {"\n"}
                          {"\t"}○ Hearing aids (with the exception of hearing aid accessories, which are permitted) {"\n"} {"\n"}
                          {"\t"}● Products that are marketed as unapproved or unregistered medical devices, such as: {"\n"} {"\n"}
                          {"\t"}○ Psoriasis lamps {"\n"} {"\n"}
                          {"\t"}● Certain menstrual cups, such as: {"\n"} {"\n"}
                          {"\t"}○ Monzcare R-cup and LadyCup {"\n"} {"\n"}
                          {"\t"}○ Rainbow and iCare menstrual cups {"\n"} {"\n"}
                          {"\t"}● Products that have been the subject of criminal enforcement, injunctions, seizures, or {"\n"} {"\n"}
                          warning letters, such as: {"\n"} {"\n"}
                          {"\t"}○ Lelo LunaBeads and Tor II {"\n"} {"\n"}
                          {"\t"}○ WaxVac Ear Cleaners {"\n"} {"\n"}
                          {"\t"}● Products that incorporate Class IIIB lasers (such as laser light show projectors) and {"\n"} {"\n"}
                          Class IV lasers (for example, industrial lasers) {"\n"} {"\n"}
                          {"\t"}● Listings claiming that a product is intended to be used for the diagnosis, cure, mitigation, {"\n"} {"\n"}
                          treatment, or prevention of disease in humans unless the claim is cleared or approved {"\n"} {"\n"}
                          by the FDA, such as: {"\n"} {"\n"}
                          {"\t"}○ Chelation products, including metal detection test kits {"\n"} {"\n"}
                          {"\t"}■ See examples of chelation products below. {"\n"} {"\n"}
                          {"\t"}○ Miracle Mineral Solutions {"\n"} {"\n"}
                          {"\t"}○ Rife Machines {"\n"} {"\n"}
                          {"\t"}○ Sexually Transmitted Disease (STD) Test Kits {"\n"} {"\n"}
                          {"\t"}○ Testosterone test kits {"\n"} {"\n"}
                          {"\t"}● Products that are adulterated or misbranded {"\n"} {"\n"} {"\n"}
                          {"\t"}● Products that contain mercury, such as thermometers and batteries {"\n"} {"\n"}
                          {"\t"}● Used and refurbished medical devices {"\n"} {"\n"}
                          {"\t"}● Products that are exclusively manufactured or distributed as veterinary devices {"\n"} {"\n"}
                          {"\t"}● Products that claim to be "FDA approved" or products that include the FDA logo in {"\n"} {"\n"}
                          associated images (for more information, see: Is It Really 'FDA Approved'? and FDA {"\n"} {"\n"}
                          Logo Policy) {"\n"} {"\n"} {"\n"}
                          Offensive and Controversial Materials {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Products that promote or glorify hatred, violence, racial, sexual or religious intolerance or {"\n"} {"\n"}
                          promote organizations with such viewsn {"\n"} {"\n"}
                          {"\t"}● Crime scene photographs {"\n"} {"\n"}
                          {"\t"}● Morgue or autopsy photographs {"\n"} {"\n"}
                          {"\t"}● Human body parts {"\n"} {"\n"}
                          {"\t"}● Products retrieved from a disaster or tragedy site {"\n"} {"\n"}
                          {"\t"}● Videos, sound or other recordings taken without the subject's permission {"\n"} {"\n"} {"\n"}
                          Pesticides {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing{"\n"} {"\n"}
                          {"\t"}● Illegal and unregistered pesticides (for example, insecticide chalk, tres pasitos) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Pesticide products that were not intended for sale in the U.S. (these may look like {"\n"} {"\n"}
                          recognized brands, but are not labeled with an EPA registration number) {"\n"} {"\n"}
                          {"\t"}● Pest control devices (for example, ultrasound devices for repelling ants) that have not {"\n"} {"\n"}
                          been manufactured in an EPA registered facility or are not labeled with an EPA facility {"\n"} {"\n"}
                          number {"\n"} {"\n"}
                          {"\t"}● Restricted use pesticides, which are not to be available for purchase or use by the {"\n"} {"\n"}
                          general public {"\n"} {"\n"}
                          {"\t"}● Products not registered with the EPA that make a pesticide claim, unless subject to one {"\n"} {"\n"}
                          of the limited exclusions below {"\n"} {"\n"}
                          {"\t"}● Pesticide products that contain false or misleading claims or are otherwise misbranded {"\n"} {"\n"}
                          (for example, claims regarding the safety of the pesticide or its ingredients, such as {"\n"} {"\n"}
                          “safe,” “nonpoisonous,” “noninjurious,” “harmless”, “nontoxic”, or “all natural”) {"\n"} {"\n"}
                          {"\t"}● Pesticide products that make public health claims (for example, treated wristbands that {"\n"} {"\n"}
                          are marketed as repelling ticks “that carry Lyme disease”) {"\n"} {"\n"}
                          {"\t"}● Pesticide products that are in broken packaging or are being sold in a quantity or amount {"\n"} {"\n"}
                          different from what is listed on the label approved by the EPA. {"\n"} {"\n"} {"\n"}
                          Plants, Plant Products, and Seeds {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, and seeds that are not otherwise restricted {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, and seeds that comply with importation requirements {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, or seeds designated by the U.S. Department of Agriculture {"\n"} {"\n"}
                          (USDA) as “noxious” or similarly classified by applicable state or local government {"\n"} {"\n"}
                          authorities {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, or seeds subject to USDA or applicable state or local government {"\n"} {"\n"}
                          quarantines (such as the USDA’s citrus canker quarantine or the Washington State {"\n"} {"\n"}
                          grape virus quarantine) {"\n"} {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, or seeds that are taken, possessed, transported, or sold in {"\n"} {"\n"}
                          violation of the Lacey Act {"\n"} {"\n"}
                          {"\t"}● Illegal plants, plant products, or seeds {"\n"} {"\n"}
                          {"\t"}● Dangerous or fatal plants, plant products, or seeds (such as the pong pong seed) {"\n"} {"\n"}
                          {"\t"}● Plants, plant products, or seeds that do not comply with importation requirements {"\n"} {"\n"} {"\n"}
                          Requirements in Shipping and Labeling {"\n"} {"\n"}
                          Each shipment of plants, plant products, or seeds must have: {"\n"} {"\n"}
                          1. The name and address of the shipper or owner {"\n"} {"\n"}
                          2. The name of the person to whom the shipment is being sent {"\n"} {"\n"}
                          3. The name of the country, state, or territory where the contents were grown {"\n"} {"\n"}
                          4. A statement of the contents. State and local governments may have additional {"\n"} {"\n"}
                          requirements for both interstate and intrastate shipment and labeling of certain plants, {"\n"} {"\n"}
                          plant products, and seeds. {"\n"} {"\n"} {"\n"}
                          Police-related items {"\n"} {"\n"} {"\n"}
                          Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Fake, novelty, or clearly unofficial badges such as plastic or cartoon badges {"\n"} {"\n"}
                          {"\t"}● School safety patrol badges {"\n"} {"\n"}
                          {"\t"}● Antique emergency lighting or sirens {"\n"} {"\n"}
                          {"\t"}● Aviation beacon lights for aircrafts {"\n"} {"\n"}
                          {"\t"}● Emergency lighting for toy or model cars (no color restriction) {"\n"} {"\n"}
                          {"\t"}● Industrial or security system emergency lighting {"\n"} {"\n"} {"\n"}
                          {"\t"}● Siren speakers {"\n"} {"\n"}
                          {"\t"}● Show car lighting (undercarriage) or Knight Rider-style kits {"\n"} {"\n"}
                          {"\t"}● Novelty hats or shirts even if they say "Police", "Sherriff", or "SWAT" {"\n"} {"\n"}
                          {"\t"}● Police uniform accessories that are not department-issued items (such as cufflinks, {"\n"} {"\n"}
                          nameplates, ranks, tie clips, and so on) {"\n"} {"\n"}
                          {"\t"}● Shoulder patches for police uniforms or jackets {"\n"} {"\n"}
                          {"\t"}● Uniforms and equipment for ambulance services, emergency medical technicians {"\n"} {"\n"}
                          (EMTs), or firefighters {"\n"} {"\n"}
                          {"\t"}● Utility belts or belt accessories {"\n"} {"\n"}
                          {"\t"}● Authorized souvenir items (such as buttons, cuff links, fake badges, hats, money clips, {"\n"} {"\n"}
                          mugs, pens, pins, and t-shirts) {"\n"} {"\n"}
                          {"\t"}● COPS decals or novelty items {"\n"} {"\n"}
                          {"\t"}● Riot shields {"\n"} {"\n"}
                          {"\t"}● Some Fraternal Order of Police (FOP) items are allowed, with the exception of the items {"\n"} {"\n"}
                          listed in the Not allowed section below {"\n"} {"\n"}
                          {"\t"}● Thin Blue Line police items {"\n"} {"\n"} {"\n"}
                          Not Allowed in Listing {"\n"} {"\n"}
                          {"\t"}● Badges from any federal, state or local law enforcement agencies, including historical {"\n"} {"\n"}
                          badges that may resemble a current issue badge {"\n"} {"\n"}
                          {"\t"}● Badges issued by foreign governments, including international police badges {"\n"} {"\n"}
                          {"\t"}● Badges issued by private security companies {"\n"} {"\n"}
                          {"\t"}● Badge patches {"\n"} {"\n"}
                          {"\t"}● Credential cases {"\n"} {"\n"}
                          {"\t"}● Identification cards or credentials for members of law enforcement {"\n"} {"\n"} {"\n"}
                          {"\t"}● Mini TSA badges {"\n"} {"\n"}
                          {"\t"}● Movie prop badges {"\n"} {"\n"}
                          {"\t"}● Quasi-law enforcement badges such as fire department badges {"\n"} {"\n"}
                          {"\t"}● Reproductions of current law enforcement badges, including fake badges that look like {"\n"} {"\n"}
                          current issue police badges – for example, an X-Files FBI badge {"\n"} {"\n"}
                          {"\t"}● Special event badges like inauguration badges {"\n"} {"\n"}
                          {"\t"}● Fully operational emergency vehicles with lights, sirens, and so on {"\n"} {"\n"}
                          {"\t"}● Lighting for emergency vehicles {"\n"} {"\n"}
                          {"\t"}● Listings for emergency lighting systems that have black and white or stock images {"\n"} {"\n"}
                          {"\t"}● Red, green, or blue lenses used for emergency vehicle lights {"\n"} {"\n"}
                          {"\t"}● Red, green, or blue LEDs for emergency vehicles {"\n"} {"\n"}
                          {"\t"}● Siren controls or amplifiers for emergency vehicles {"\n"} {"\n"}
                          {"\t"}● Specific law enforcement decals {"\n"} {"\n"}
                          {"\t"}● Military police uniforms or components of those uniforms {"\n"} {"\n"}
                          {"\t"}● Official, current- or recent-issue police uniforms or components of those uniforms (such {"\n"} {"\n"}
                          as boots, hats, helmets, jackets, and shirts) {"\n"} {"\n"}
                          {"\t"}● Official, current- or recent-issue Canadian military uniforms {"\n"} {"\n"}
                          {"\t"}● Raid jackets {"\n"} {"\n"}
                          {"\t"}● Royal Canadian Mounted Police (RCMP) uniforms or components of those uniforms {"\n"} {"\n"}
                          {"\t"}● US or Canadian Customs and Border Protection uniforms or components of those {"\n"} {"\n"}
                          uniforms, including shoulder patches {"\n"} {"\n"}
                          {"\t"}● Authentic prison uniforms (costume prison uniforms are allowed) {"\n"} {"\n"}
                          {"\t"}● The following Fraternal Order of Police {"\n"} {"\n"}
                        </Text>
                      </View>
                    </ScrollView>
                  </View>
                </TouchableOpacity>
              </TouchableOpacity>
            </Modal>
          </View>
        </Content>
      </Container>
    );
  }
}
export default HelpCenterScreen