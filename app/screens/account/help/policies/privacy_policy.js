import React from 'react'

import {
    View,
    Text,
    StyleSheet
} from 'react-native'

import { 
    Container,
    Thumbnail,
    Left,
    Right,
    Body,
    Header
 } from 'native-base'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Assets from '../../../../components/assets.manager'
// import Header from '../../../../../components/Header/Headers'

class PrivacyPolicy extends React.Component{
  render(){
    return(
      <Container style={{backgroundColor:'#F7F5F6'}}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0}}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 1, justifyContent: 'center'}}>
            <Text style={styles.headerText}>Privacy Policy</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{width: 20}}/>
          </Right>
        </Header>
        <ScrollView style={{backgroundColor: '#F7F5F6'}}> 
          <View style={{paddingTop:10, paddingBottom:30, paddingHorizontal:10}}>
            <Text style={styles.contentText}>ReferApps values our member's personal information. Data protection is a matter of trust and your privacy is important to us. This privacy notice explains our collection, use, disclosure, retention, and protection of your personal information. This privacy notice applies to any ReferApps site, application, service, or tool (collectively "Services") where this privacy notice is referenced, regardless of how you access or use them, including through mobile devices. We will only collect information where it is necessary for us to do so and we will only collect information if it is relevant to our dealings with you. We collect personal information from you when you use our Services.</Text>
          </View>
          <View style={styles.contentContainer}>
            <Text style={styles.contentText}>
              This privacy notice also applies to the provision of ReferApps services through any ReferApps affiliate's website, application, service, or tool where it is referenced and where your listings and their content are published or advertised in accordance with the terms of this privacy notice.
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <Text style={styles.contentText}>
              We may amend this privacy notice at any time by posting the amended version on this site including the effective date of the amended version. We will announce any material changes to this privacy notice through the ReferApps Message Center (?) and/or via email.
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <Text style={styles.contentText}>
                We and our affiliates will be the sole owners of the information collected on this site. You grant us the right to collect, record, hold, store, disclose, transfer and use our member's personal information but will not sell or rent it to anyone.
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <Text style={styles.contentTextTitle}>
                Collection of Personal Information
            </Text>
          </View>
          <View style={styles.contentContainer}>
            <Text style={styles.contentText}>
                When creating a ReferApps account or using our Services, the personal information we will collect may include but is not limited to:
            </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  1. Identifying information (e. g. name, addresses, telephone numbers, e-mail addresses etc.)
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  2. Buying or selling information you provide during a transaction, or other transaction-based content you generate as a result of a transaction you are involved in 
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  3. Other content that is connected to your account or you generate (e. g. items added to your basket, referred items and people you referred, collections created, pinned items, and following other collections and sellers).
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  4. Financial information when doing transactions (e. g. credit card or bank account numbers) in connection with a transaction
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  5. Postage, billing and other information for purchasing, sending or referring an item, as well as, where postal services are provided through one of our programs, information required to clear customs (such as Tax ID or other identification numbers) and relevant postage information (such as tracking numbers and tracking updates)
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  6. Postal services provided by our programs
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  7. Tax ID or other identification numbers 
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  8. Relevant postage information (e. g. tracking numbers and updates)
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  9. Other information you need to update or add through your participation in community discussions, member-to-member communications, chats, conflict in agreement or when you need to communicate with us regarding our Services
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  10. Additional information we require through applicable national laws to collect and process in order to identify or authenticate you or to verify the information we have collected.
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We will only be able to collect your personal information if you voluntarily submit the information to us. The information submitted to us must be correct and accurate and should be up to date. We must be informed of sudden changes or the transaction may be revoked due to the inaccuracy of the information given. We reserve the right to request for documentation to verify the information you provided.
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  ReferApps will collect information about your interaction with our Services, your preferred advertisements and your communication with us. We will receive this information from your devices (e. g. Mobile device, Desktop) you use when accessing our Services. We will also collect computer and connection information (e. g. page views, traffic to and from the sites, IP address, referral URL, ad data, browsing history and weblog information).
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Upon your visitation, usage or interaction with our mobile application or ou site, we may collect certain information by automated or passive means using a variety of technologies, which may be downloaded to your device and may set or modify settings on your device. The information we collect may include, without limitation, your Internet Protocol (IP) address, browser type, type of mobile device and its characteristics, your mobile device's unique device identifier (UDID) or mobile equipment identifier (MEID), the pages you visit on our website or mobile applications and the times of visit.
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Additional information we may collect from other sources about you is your:
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  1. Demographic and other information that is publicly available in an applicable jurisdiction
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  2. Credit check information
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  3. Additional contact information
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  4. Information from credit bureaus
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  5. Personal information you have in your social media account (e. g. content you viewed, liked and advertisements you clicked as well as your friend's list.)
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Note: If you provide personal information about someone else, you must do only if you have their authorization. Please inform them of the process of how we use, collect, disclose and retain their personal information according to our privacy policy. 
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Using technologies such as GPS, Wi-Fi, etc., our mobile applications may collect precise information about the location of your mobile device. We collect, use, disclose and/or process this information for one or more purposes including location-based services that you request, to deliver relevant content to you based on your location, or to allow you to share your location to other users as part of the services under our mobile applications. But then, through your device settings, you can withdraw your permission for us to acquire information on your location.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  When you use ReferApps application, you may grant certain permissions to us for your device. Most mobile devices provide information about these permissions.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Termination Of Account                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  ReferApps reserves the right to terminate accounts, refuse service, remove or edit content, cancel orders or terminate your rights to use ReferApps Services, tools, and applications in its sole discretion.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Use and Disclosure of Personal Information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  User information is vital for the services of ReferApps, therefore if not submitted or had withdrawn your consent to our use of your personal information, you will not be entitled to the services of Refer apps.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Your personal information will be used to connect and respond to you when you contact us. We will only collect necessary information and it will only be disclosed unless you approved for transaction and shipping purposes. The collected information will only be kept for as long as it is relevant for its purpose or as the law required.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  The personal information we collect from you will be used, or shared with third parties (including related companies, third party service providers, and third-party sellers), for some or all of the following purposes:                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To facilitate your use of any of ReferApps Services and Programs and your access to the platform                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To provide payment processing and account management; operate, measure and improve our Services; keep our Services safe, secure and operational; and customize site content that includes items and services that you may like in response to actions that you take                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To contact you concerning your account, to troubleshoot problems with your account, to resolve a dispute, collect fees or money owed or as otherwise necessary to provide your customer service                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  NOTE: We may contact you via email, telephone, SMS/text messages, postal mail, and via mobile push notifications when contacting you for such purposes as outlined above                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To provide other necessary services you requested as described when we collect the information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To verify accounts and activity, combat harmful conduct, maintain the integrity of our Services, and promote safety and security on and off ReferApps Services. We will use the information that we have to investigate suspicious activity or breaches of any of our policies.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To implement our User Agreement, this Privacy Terms and Policy, and our other policies; and to monitor restrictions on offers to buy or sell outside of ReferApps and member-to-member communications for violations of our policies or applicable laws                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To improve our Services by reviewing information (e. g. cases associated with interrupted or crashed pages experienced by users allow us to identify and fix problems and give you a better experience)                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To personalize, measure, and improve our advertising based on your advertising customization preferences                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To contact you about public policy matters, or other current events, related to your ability to use our Services. This could include an invitation to join a petition, letter writing, call or other sorts of public policy related campaigns                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To deliver targeted marketing, service updates, and promotional offers based on your communication preferences                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Measure the performance of our email marketing campaigns (e.g. by analyzing open and click rates)                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * to measure sellers' performance (e.g. by using shipment tracking information that sellers and shipping providers send or provide through ReferApps)                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * to monitor and improve the information security of our site and mobile applications                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  ReferApps may share your personal information with third parties and our affiliates for the above-stated purposes, specifically, completing a transaction with you, managing your account and our relationship with you, marketing and fulfilling any legal or regulatory requirements and requests as deemed necessary by ReferApps. In sharing your personal information with them, we endeavor to ensure that the third parties and our affiliates keep your personal information secure from unauthorized access, collection, use, disclosure, or similar risks and retain your personal information only for as long as they need your personal information to achieve the above-stated purposes.
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We may use technologies considered automated decision making or profiling. We will not make automated-decisions about you that would significantly affect you unless such a decision is necessary as part of a contract we have with you, we have your consent, or we are required by law to use such technology.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  You can control your email communication preferences by contacting us (see Contact Us below).
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  By agreeing to our terms, we may use your personal information to:                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  1. Provide you with marketing via telephone calls, SMS/text                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  2. Provide you with marketing from third parties                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  3. Use your sensitive personal information to facilitate transactions in particular categories.
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  4. Use your precise geo-location to provide location-based services.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  5. Use your social media account that is linked in ReferApps site to access information about you and your friends' list
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  NOTE: You have the right to withdraw your consent at any time.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  ReferApps needs your personal information to:                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  1. Fulfill a contract with you and provide you with our Services              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  2. To comply with our legal obligation              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  3. To protect your vital interest or as may be required for the public good.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  This comprises:             
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  1. Providing payment processing and accounting management in your transactions                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  2. Operating, measuring and improving our Services               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  3. Keeping our services safe, secure and operational for your convenience                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  4. Customizing site content including items and services that you may like to every action that you take
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  5. To contact you in regards to your account, troubleshooting problems with your account or resolve a dispute
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  6. Collecting fees or as otherwise necessary to provide you customer service                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  7. Preventing, mitigating, detecting and investigate security breaches or other potentially prohibited or illegal activities.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  8. Monitoring restrictions on offers to member-to-member communications and buying or selling outside ReferApps' jurisdiction for violations of our policies or applicable laws.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Accessing and Updating Your Personal Information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We respect your right to access, correct, request deletion or request restriction of our usage of your personal information as required by the law. We are also to ensure that the personal information we collect is accurate and up to date. You have the right to:                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Know what personal information we maintain about you                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Have a copy of your personal information in a structured, commonly, used and machine-readable format on request
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Ask us to update your personal information if it is incomplete or incorrect
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Object to our processing of your personal information.              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Ask us to delete or restrict how we use your personal information, but this right is determined by the law and may impact your access to some of our Services             
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We take steps to share the updates to your personal information with third parties and our affiliates with whom we have shared your personal information if your personal information is still necessary for the above-stated purposes.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  You can see, review and change most of your personal information by signing in to your account. Please, update your personal information immediately if it changes or is inaccurate. Keep in mind, once you make a public posting, you may not be able to change or remove it.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We will honor any statutory right you might have to access, modify or erase your personal information. To request access and to find out whether any fees may apply, if permitted by applicable national laws, please contact us following the instructions in the Contact Us section below. Where you have a statutory right to request access or request the modification or erasure of your personal information, we can still withhold that access or decline to modify or erase your personal information in some cases in accordance with applicable national laws but will give you reasons if we do so.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  If you request that we stop processing some or all of your personal information or you withdraw (where applicable) your consent for our use or disclosure of your personal information for purposes set out in this privacy notice, we might not be able to provide you all of the Services and customer support offered to our users and authorized under this privacy notice and our User Agreement.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Upon your request, we will close your account and remove your personal information from view as soon as reasonably possible, based on your account activity and in accordance with applicable national laws.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  How We Might Share Your Personal Information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We may disclose your personal information to the following parties for the following purposes:                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  ReferApps Corporate Family Members               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Provide joint content and services (e. g. registration, transactions, and customer support)                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Help detect, investigate, mitigate and prevent potentially fraudulent and illegal acts, violations of our User Agreement, and data security breaches             
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Provide you personalized advertising               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Improve their products, sites, applications, services, tools, and marketing communications               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Members of our ReferApps Inc. corporate family will use your personal information to send you marketing communications only if you have consented to receive such communications from them or if otherwise permitted by the law               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Service Providers and Financial Institutions Partners                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We employ other companies and individuals to perform functions on our behalf. Examples include delivering packages, sending postal mail and e-mail, and processing credit card payments. They have access to personal information needed to perform their functions, but may not use it for other purposes.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Third party service providers who help us to provide our Services, payment processing services, assist us in providing customized advertising, to assist us with the prevention, detection, mitigation, and investigation of potentially illegal acts, violations of our User Agreement, fraud and/or security breaches, bill collection, affiliate and rewards programs, co-branded credit cards and other business operations              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Third party financial institutions with whom we partner to offer financial products to you, for them to provide joint content and services (such as, registration, transactions, and customer support). These third-party financial institution partners will use your personal information to send you marketing communications only if you have requested their services               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Third party shipping providers (e.g., DHL, UPS, etc.) with whom we share the delivery address, contact information and shipment tracking information for the purposes of facilitating the delivery of items purchased and other delivery related communications              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Third party providers of websites, applications, services, and tools that we cooperate with so that they can publish or advertise your listings and their content on their websites or in their applications, services, and tools. If we transfer personal information along with the content of your listings to third-party providers, this will be solely on the basis of an agreement limiting use by the third party provider of such personal information to processing necessary to fulfil their contract with us and obligating the third party provider to take security measures with regard to such data. Third party providers are not permitted to sell, lease or in any other way transfer the personal information included in your listings to third parties                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Law Enforcement               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  If we receive a lawful court order to release account or other personal information then we will comply with the law. We will also release information when necessary to protect the life, safety, or property of others. This includes exchanging information with other companies and organizations for fraud protection and credit risk reduction.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Other ReferApps Members as Authorized by You or Your Use of Our Services                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * When transacting with another member, the other member may request that we provide him/her with necessary information about you to complete the transaction (e. g. your name, account ID, email address, contact details, shipping and billing address, or other information needed from you to promote the reliability and security of the transaction              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * If a transaction fails, is put on hold, or is later invalidated, we may also provide the other user with details of the unsuccessful transaction              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * The other user receiving your information should only use it for purposes related to the transaction. They should not contact you for marketing purposes, unless you have consented to receive marketing from them.  Before you can refer an item to them, make sure that 1.  they are your friend and 2. They consent and requested the information you are sending. I.E. Where did you buy that nice watch?  Repeat violations can and will suspend your account.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Contacting users with unwanted or threatening messages is a violation of our User Agreement              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Other than as set out above, you will receive notice when information about you might go to third parties, and you will have an opportunity to choose not to share the information.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Paypal Inc. and Its Corporate Family               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To prevent, detect, mitigate, and investigate fraud, security breaches, potentially prohibited or illegal activities               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To provide customer services, including to help service your account or resolve disputes (like billing or transactional disputes)               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To facilitate the processing of payment cards when you pay within our Services with a payment card and we use PayPal to process your payment                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * To facilitate shipping and related services for purchases you made using PayPal                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Change of ownership               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  If we are subject to a merger or acquisition with/by another company, we may share information with them in accordance with our global privacy standards. Should such an event occur, we will require that the new combined entity follow this privacy notice with respect to your personal information. If we intend to handle your personal information for any purposes not covered in this privacy notice, you will receive prior notification of the processing of your personal information for the new purposes.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  How Long We Keep Your Personal Information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Our specific retention times for personal information are documented in our records retention schedules. How long we retain personal information can vary significantly based on the context of the Services we provide and on our legal obligations. The following factors typically influence retention periods:               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * How long is the personal information needed to provide our Services? This includes such things as maintaining and improving the performance of our products, keeping our systems secure, and maintaining appropriate business and financial records. This is the general rule that establishes the baseline for most of our data retention periods.                
              </Text>
          </View>        
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Is the personal information sensitive? If so, a shortened retention time is generally appropriate.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Have you provided consent for a longer retention period? If so, we will retain data in accordance with your consent.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  * Are we subject to a legal, contractual, or similar obligation to retain your personal information? Examples can include mandatory data retention laws in the applicable jurisdiction, government orders to preserve data relevant to an investigation, or personal information retained for the purposes of litigation.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  After it is no longer necessary for us to retain your personal information, we will dispose of it in a secure manner according to our data retention and deletion policies.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Other Important Privacy Information                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  What happens when you share your personal information on our sites or applications?                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Other users have access to the information you share on ReferApps. For example, other users can see your bids, purchases, items for sale, your collections, the sellers and collections you follow, storefronts, Feedback, ratings, and associated comments. Other users can also see any information you chose to share in your profile or your collections.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Your username is displayed throughout ReferApps website and is available to the public. It is connected to all of your activity. Notices sent to other users about suspicious activity and notice violations on our sites refer to User IDs and specific items. So if you associate your name with your User ID, the people to whom you have revealed your name will be able to personally identify your activities in ReferApps site and mobile application.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  To help protect your privacy, we allow only limited access to other users' contact, shipping and financial information as necessary to facilitate your transactions and collect payments. However, when users are involved in a transaction, they have access to each other's name, User ID, email address and other contact and shipping information.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  How you should use the information you receive on ReferApps                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  When you are in a transaction with another user, we enable you to obtain or we may provide you with the personal information of the other user (such as his/her name, account ID, email address, contact details, shipping and billing address) to complete the transaction. We encourage you to inform the other user about your privacy practices and to respect his/her privacy. In all cases, you must give the other user a chance to remove himself/herself from your database and give him/her a chance to review what information you have collected about them.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  You may use the personal information that you have access to only for ReferApps transaction-related purposes, or for other services offered through ReferApps (such as shipping, fraud complaints, and member-to-member communications), and for purposes expressly consented by the user to whom the information relates. Using personal information of other users that you have access to for any other purpose constitutes a violation.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Unwanted or Threatening Email                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We do not tolerate abuse of our Services. You do not have permission to add other users to your mailing list (email or postal), call, or send him/her text messages for commercial purposes, even if this user purchased something from you unless the user has given his/her explicit consent. Sending unwanted or threatening email and text messages is against our User Agreement. To report ReferApps-related spam or spoof emails please forward the email to (spam@referapps.com or spoof@referapps.com).              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Communication tools               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We may scan messages automatically and check for spam, viruses, phishing, and other malicious activity, illegal or prohibited content or violations of our User Agreement, this privacy notice or our other policies.                
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Children's Privacy               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  Our websites are general audience websites and not intended for children. We do not knowingly collect personal information from users deemed to be children under their respective national laws.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Third Party Privacy Practices               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  This privacy notice addresses only our use and handling of personal information we collect from you in connection with providing you our Services. If you disclose your information to a third party or visit a third party website via a link from our Services, their privacy notices and practices will apply to any personal information you provide to them or they collect from you.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We cannot guarantee the privacy or security of your personal information once you provide it to a third party and we encourage you to evaluate the privacy and security policies of your trading partner before entering into a transaction and choosing to share your personal information. This is true even where the third parties to whom you disclose personal information are bidders, buyers or sellers on our site.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Security               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  We protect your information using technical and administrative security measures to reduce the risks of loss, misuse, unauthorized access, disclosure, and alteration. Some of the safeguards we use are firewalls and data encryption, physical access controls to our data centers, and information access authorization controls. If you believe your account has been abused or someone is activating your account without your consent, please contact us following the instructions in the Contact section. We do our best to make your account secure, but the safety of your account when you use our Services lies on you.               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  NOTE: Keep your password protected and updated to secure your account. We recommend you to change your password frequently to ensure that no one knows your password.              
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Changes to the Privacy Policy               
              </Text>
          </View>
          <View style={styles.contentContainer}>
              <Text style={styles.contentText}>
                  ReferApps shall regularly review the sufficiency of this Privacy Policy. We reserve the right to modify and change the Privacy Policy at any time. Any changes to this policy will be published on the Site.               
              </Text>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default(PrivacyPolicy)

const styles = StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
      ...Platform.select({
      ios:{
        height: 60,
        paddingBottom: 15
      },
      android:{

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  contentText:{
    color:'#7a7a7a', 
    fontSize:14.5, 
    // fontFamily:'Lato-Regular'
  },
  contentContainer:{
      paddingHorizontal:10,
      paddingVertical:30
  },
  contentTextTitle:{
    color:'#7a7a7a', 
    fontSize:14.5, 
    // fontFamily:'Lato-Semibold'
  }
})