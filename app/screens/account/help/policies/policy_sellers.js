import React from 'react'

import {
    View,
    Text,
    StyleSheet,
    Platform,
    Dimensions
} from 'react-native'
import { 
    Container, 
    Thumbnail,
    Header,
    Body,
    Left,
    Right
 } from 'native-base'
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler'
import Assets from '../../../../components/assets.manager'

// import Header from '../../../../../components/Header/Headers'

class PolicySellers extends React.Component{
  render() {
    return(
      <Container style={{backgroundColor:'#F7F5F6'}}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0}}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 1, justifyContent: 'center'}}>
            <Text style={styles.headerText}>DELIVERY SHIPPING POLICY SELLERS</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{width: 20}}/>
          </Right>
        </Header>
        <ScrollView style={{backgroundColor:'#F5F6F7'}}>
          <Text style={{  color:'#7a7a7a', fontSize:14.5, 
          // fontFamily:'Lato-Semibold', 
          paddingTop:10, paddingHorizontal:10}}>
              Estimated delivery dates for sellers
          </Text>
          <View style={styles.contentContainer}>
              <Text style={styles.contentTextTitle}>
                  Weekend and holiday 
              </Text>
              <Text style={styles.contentText}>
                  Weekends and holidays aren't included in estimated delivery dates, even though some carriers deliver on weekends.
              </Text>
              <Text style={styles.contentText}>
                  A buyer may contact you if they specifically need a weekend delivery, or want a faster delivery option. If they're willing to pay any difference in cost, it's up to you whether to accept their request.
              </Text>
              <Text style={styles.contentTextTitle}>
                  Shipping rates for sellers
              </Text>
              <Text style={styles.contentTextTitle}>
                  Setting a Competitive Shipping Costs
              </Text>
              <Text style={styles.contentText}>
                  Here are some tips for when you're setting up your shipping charges:
              </Text>
              <View style={{flexDirection:'row'}}>
                  <Text style={styles.bullets}>{'\u2022'}</Text>
                  <Text style={styles.contentText}>
                      Have a look to see what other sellers are charging to ship similar items
                  </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                  <Text style={styles.bullets}>{'\u2022'}</Text>
                  <Text style={styles.contentText}>
                      Consider building the price of shipping into the item's price, so you can offer free shipping
                  </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Use our shipping calculator to estimate how much it costs to send your item through various carriers
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Make sure you're allowing for packaging when checking the item's size and weight
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Decide whether you're going to ship internationally, or just within the US
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Think about whether you want to set a fixed shipping charge for all buyers, or use calculated shipping and charge buyers a cost that will vary based on their location
                </Text>
              </View>
              <Text style={styles.contentTextTitle}>
                  How shipping discounts work
              </Text>
              <Text style={styles.contentText}>
                  You can offer shipping discounts to buyers who buy more than one item from you, but first, you need to opt into offer combined shipping. You can then set up rules that automatically apply to the shipping cost when someone buys multiple items.
              </Text>
              <Text style={styles.contentText}>
                  Depending on whether you offer a flat shipping rate, or are using calculated shipping, these discounts work slightly differently.
              </Text>
              <Text style={styles.contentTextTitle}>
                  Flat rate shipping rules
              </Text>
              <Text style={styles.contentText}>
                  If you're offering a flat rate shipping cost, where all buyers pay the same amount, you can apply two different types of shipping rules:
              </Text>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Add a fixed amount for each additional item purchased. For example, if the shipping cost on the first item is $5 you could choose to charge just $2 in shipping for each additional purchase
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Subtract an amount from the shipping charge for each additional item purchased. In the same example as above, you could choose to charge $1 less than the initial cost (i.e., $4) for each additional purchase
                </Text>
              </View>
              <Text style={styles.contentTextTitle}>
                  Calculated shipping rules
              </Text>
              <Text style={styles.contentText}>
                  If you're offering a calculated shipping cost (where the amount a buyer pays varies based on their location), you can apply rules for combining items into a single package:
              </Text>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Automatically combine the weights of all the items in one package. For example, if a buyer purchases 3 items that weigh 4 lb each, you can set up a rule that charges shipping for a single 12 lb package
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Automatically combine the weights of the items in one package and subtract a weight amount (in ounces). For example, if a buyer purchases 3 items that weigh 4 lb each, you can set up a rule that combines the items in one 12 lb package, and automatically deducts 16 oz from the total weight
                </Text>
              </View>
              <Text style={styles.contentTextTitle}>
                  Shipping Charge Increases
              </Text>
              <Text style={styles.contentText}>
                  Once someone's bought your item, you can't update the shipping charges from what was originally shown on the listing unless the buyer wants to combine shipping for multiple items paid by PayPal, or asks for express delivery and is willing to pay for the service.
              </Text>
              <Text style={styles.contentText}>
                  If a buyer is looking to upgrade their shipping, first, send the buyer a message and ask them not to pay until they receive a new invoice with updated shipping costs.
              </Text>
              <Text style={styles.contentTextTitle}>
                  Packing tips for sellers
              </Text>
              <Text style={styles.contentText}>
                  Here are some tips on packing your items:
              </Text>
              <View style={{flexDirection:'row'}}>
              <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Choose a container that's slightly larger than your item, so you can cushion it with packing material on all sides
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Surround your item with bubble wrap, packing peanuts, foam, or paper – so there's no room for things to move around
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Use clear or brown packaging tape, reinforced packing tape, or paper tape, which should be at least 2 inches wide
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Don't use masking tape, cellophane tape, cord, string, or twine
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Tape the opening of the box and reinforce all seams and edges
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Clearly label the package and include a return address
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    If you're reusing a box, cover any previous labels and cross out any previous addresses with a black marker
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    The maximum shipping costs are determined based on what sellers are charging for shipping (according to recent transaction data) and what buyers say they consider reasonable.
                </Text>
              </View>
              <Text style={styles.contentText}>
                  Having a maximum shipping cost helps buyers find items with reasonable shipping and can help you sell more items.
              </Text>
              <Text style={styles.contentTextTitle}>
                  Packing irregularly shaped items
              </Text>
              <Text style={styles.contentText}>
                  Some things are a little more difficult to ship than others. Here are some ideas for packing irregularly shaped items:
              </Text>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Metals – tape the address label directly to the item, and cover any sharp or protruding edges with cardboard
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Fabric and wallpaper – rolled goods travel best in corrugated boxes. If you ship a fabric roll in a bag, make sure it's tightly wrapped and taped to avoid tearing
                </Text>
              </View>
              <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Tires – apply a wide band of pressure-sensitive tape through the center and completely around the body of the tire. Attach the address label to the tape band where it covers the tread, and cover with clear tape
                </Text>
              </View>
              <Text style={styles.contentTextTitle}>
                  Setting your delivery options
              </Text>
              <Text style={[styles.contentText, {paddingBottom:40}]}>
                  On the listing page, scroll down to the Shipping section. In Shipping Options, enter a fixed cost, the package weight, and dimensions. If you're not happy with the recommended shipping service, select Change shipping service to choose another option. Select the calculated rate, or if you'd like, choose the option to charge a fixed amount and enter the amount. You can also choose to offer free shipping and pay it yourself.
              </Text>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default(PolicySellers)

const styles = StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
      ...Platform.select({
      ios:{
        height: 60,
        paddingBottom: 15
      },
      android:{

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  contentText:{
    color:'#7a7a7a', 
    fontSize:14.5, 
    // fontFamily:'Lato-Regular',
    paddingVertical:20
  },
  contentContainer:{
    paddingHorizontal:10,
  },
  contentTextTitle:{
    color:'#7a7a7a', 
    fontSize:14.5, 
    // fontFamily:'Lato-Semibold',
    paddingVertical:10
  },
  bullets:{
    color :'#7a7a7a',
    fontSize:10,
    // fontFamily:'Lato-Regular',
    paddingVertical:23
  }
})