import React from 'react'

import {
    View,
    Text,
    StyleSheet
} from 'react-native'
import { 
    Container, 
    Thumbnail,
    Header,
    Body,
    Left,
    Right 
} from 'native-base'

import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'
import Assets from '../../../../components/assets.manager'
// import Header from '../../../../../components/Header/Headers'

class PolicyBuyers extends React.Component{
  render() {
    return(
      <Container style={{backgroundColor:'#F5F6F7'}}>
        <Header style={styles.headerStyle}>
          <Left style={{ flex: 0}}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 1, justifyContent: 'center'}}>
            <Text style={styles.headerText}>DELIVERY SHIPPING POLICY BUYERS</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{width: 20}}/>
          </Right>
        </Header>
        <ScrollView style={{backgroundColor:'#F5F6F7'}}>
          <View style={styles.contentContainer}>
            <Text style={styles.contentTextTitle}>
                Shipping rates for buyers
            </Text>
            <Text style={styles.contentText}>
                You can find a seller's shipping rates in the Shipping section of every listing. We always show the shipping cost separately to the item cost to make clear exactly how much you're paying for delivery.
            </Text>
            <Text style={styles.contentTextTitle}>
                Increased shipping rates after a sale
            </Text>
            <Text style={styles.contentText}>
                Once you've bought an item, sellers can't increase shipping rates without your agreement. If the seller makes a change that you don't agree with, and you can't resolve the issue directly with them, contact us and we'll step in to help.
            </Text>
            <Text style={styles.contentTextTitle}>
                Delivery date options for buyers
            </Text>
            <Text style={styles.contentText}>
                You can find an estimated delivery date on any listing that offers to ship. The estimate will usually cover a range of days but can get more precise after you confirm your delivery address during checkout.
            </Text>
            <Text style={styles.contentText}>
                After completing your purchase, you'll see an updated estimate in your purchase history along with any tracking information if it's been uploaded by your seller.
            </Text>
            <Text style={styles.contentText}>
                Estimated delivery dates are based on:
            </Text>
            <Text style={styles.contentTextTitle}>
                Handling time – Sellers set their handling time. This can range from the same working day to up to 30 business days from when they receive payment
            </Text>
            <Text style={styles.contentTextTitle}>
                Shipping or courier services – Delivery times can vary depending on which service is used. Most sellers offer a variety of delivery options, so you can choose the service you prefer when you're checking out
            </Text>
            <Text style={styles.contentTextTitle}>
                Cleared payment – If you're using a payment method such as a bank transfer, where the money isn't available to the seller immediately, the seller may wait until the payment is in their account before they send your item
            </Text>
            <Text style={styles.contentTextTitle}>
                Weekend delivery
            </Text>
            <Text style={styles.contentText}>
                Whether an item can be delivered during the weekend depends on the shipping options provided by the seller. An item's estimated delivery date is based on working days, so weekends and public holidays aren't included in that calculation.
            </Text>
            <Text style={styles.contentText}>
                If the seller doesn't offer weekend delivery, you can contact them to ask if it's possible. If the seller agrees to your request, you may have to pay additional shipping fees.
            </Text>
            <Text style={styles.contentTextTitle}>
                Tracking an Item
            </Text>
            <Text style={styles.contentText}>
                If your seller is using a tracked shipping service, it's easy to follow your item's progress.
            </Text>
            <Text style={styles.contentTextTitle}>
                Here's how:
            </Text>
            <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    Find the item in your Purchase history.
                </Text>
            </View>
            <View style={{flexDirection:'row'}}>
                <Text style={styles.bullets}>{'\u2022'}</Text>
                <Text style={styles.contentText}>
                    The tracking number will be displayed as a link. Open the link to see what stage your item has reached in its journey to you.
                </Text>
            </View>
            <Text style={styles.contentText}>
                If you don't see a tracking number for the item in your Purchase history, check if the item is marked as "shipped". If it is, contact the seller and ask them to upload the number.
            </Text>
            <Text style={styles.contentTextTitle}>
                Changing your shipping address
            </Text>
            <Text style={styles.contentText}>
                When you're buying an item, we'll pre-populate your preferred shipping address in the order summary, which you'll see just before checkout. Make sure this is where you want your item to be sent before completing your purchase.
            </Text>
            <Text style={styles.contentText}>
                If you want to make a change, select a different address in the Ship to field on the checkout page. If the address you want your item delivered to isn't shown, select Add a new address and complete the required details.
            </Text>
            <Text style={styles.contentText}>
                You can also change your preferred shipping address in the Addresses section of My Profile. From here you can also add new addresses or delete old ones.
            </Text>
            <Text style={styles.contentText}>
                If you've already completed your purchase but need to make a change, contact the seller as soon as possible to let them know.
            </Text>
            <Text style={styles.contentTextTitle}>
                About Shipping Restrictions
            </Text>
            <Text style={styles.contentText}>
                Certain restrictions prevent us from shipping certain products to all geographical locations.
            </Text>
            <Text style={[styles.contentText, {paddingBottom:40}]}>
                  You'll be notified at checkout if we're unable to ship specific items to the address you've selected. Please also check the product detail pages for any item-specific shipping restrictions.
              </Text>
          </View>
        </ScrollView>
      </Container>
    )
  }
}

export default(PolicyBuyers)

const styles = StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
      ...Platform.select({
      ios:{
        height: 60,
        paddingBottom: 15
      },
      android:{

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  contentText:{
      color:'#7a7a7a', 
      fontSize:14.5, 
      // fontFamily:'Lato-Regular',
      paddingVertical:20
  },
  contentContainer:{
      paddingHorizontal:10,
  },
  contentTextTitle:{
      color:'#7a7a7a', 
      fontSize:14.5, 
      // fontFamily:'Lato-Semibold',
      paddingVertical:10
  },
  bullets:{
      color :'#7a7a7a',
      fontSize:10,
      // fontFamily:'Lato-Regular',
      paddingVertical:23
  }
})