import React, { useState, useEffect } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  ScrollView,
  Picker
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Left,
  Body,
  Right,
  Spinner,
} from 'native-base'
import { TextInput } from 'react-native-gesture-handler'
import Assets from '../../../components/assets.manager';
import Service from '../../../components/api/service';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '@react-native-community/async-storage';
import Endpoints from '../../../components/api/endpoints';
import appleAuth from '@invertase/react-native-apple-authentication';

export default function AddAddress({ navigation, route }) {
  const [user, setUser] = useState('');
  const [fName, setFName] = useState('')
  const [mName, setMName] = useState('')
  const [lName, setLName] = useState('')
  const [number, setNumber] = useState('')
  const [notes, setNotes] = useState('')
  const [street, setStreet] = useState('')
  const [province, setProvince] = useState([]);
  const [selectedProvince, setSelectedProvince] = useState('');
  const [city, setCity] = useState([]);
  const [selectedCity, setSelectedCity] = useState('');
  const [barangay, setBarangay] = useState([]);
  const [selectedBarangay, setSelectedBarangay] = useState('');
  const [iosMargin, setiosMargin] = useState(0);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    init()
  }, [])

  const init = async () => {
    setLoading(true);
    appleAuth.isSupported ? setiosMargin(10) : setiosMargin(0);

    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)

    let Province = await Service.getAllProvince();
    if (Province) {
      setProvince(Province.data)
    }
    setLoading(false);
  }

  const handleGetCity = async (prov) => {
    let City = await Service.getAllCity(prov);
    if (City) {
      setCity(City.data)
    }
  }

  const handleGetBarangay = async (cit) => {
    let Barangay = await Service.getAllBarangay(cit);
    if (Barangay) {
      setBarangay(Barangay.data)
    }
  }

  const handleSaveAddress = async () => {
    setLoading(true);
    if (fName == '' || !fName) {
      Toast.showWithGravity("First Name field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else if (mName == '' || !mName) {
      Toast.showWithGravity("Middle Name field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else if (lName == '' || !lName) {
      Toast.showWithGravity("Last Name field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else if (number == '' || !number) {
      Toast.showWithGravity("Contact Number field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else if (street == '' || !street) {
      Toast.showWithGravity("House Number, Building and Street Name field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else if (notes == '' || !notes) {
      Toast.showWithGravity("Notes field is required", Toast.SHORT, Toast.BOTTOM);
      return;
    }
    else {
      let splitEmail = user.data.info.data.email.split('@');

      if ((!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') ||
        // user.data.is_verify_contact_number === "no" ||
        user.data.is_verify === 'no') {
        Alert.alert(
          'Warning!',
          'Your account not yet been verified.\n' +
          'Verify Now!',
          [
            {
              text: 'Cancel'
            },
            {
              text: 'Verify',
              onPress: () => {
                if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                  navigation.navigate("AddEmailMobile");
                } else if (user.data.is_verify === 'no') {
                  navigation.navigate('EmailVerification')
                }
                // else if (user.data.is_verify_contact_number === "no") {
                //   navigation.navigate('PhoneVerification')
                // }
              }
            },
          ],
          { cancelable: true }
        );
        return
      }

      submitAddAddress('no');
    }
  }

  const submitAddAddress = async (verified = 'no', reloads = 0) => {
    let data = {
      user_id: user.data.id,
      fname: fName,
      mname: mName,
      lname: lName,
      contact_number: number,
      notes: notes,
      street_address: street,
      province_code: selectedProvince,
      city_code: selectedCity,
      barangay_code: selectedBarangay,
      verified: verified,
    }

    try {
      await Service.addAddress(
        data,
        (res) => {
          if (verified === 'no') {
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.verification_code);
              let verifData = {
                verifyCode: res.verification_code,
                medium: res.sent_to,
                expiration: res.verification_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  submitAddAddress(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          } else {
            if (res.status) {
              setLoading(false);
              console.log('Data: ', res);
              Alert.alert(
                'Success',
                'New Address was added Successfully!',
                [
                  {
                    text: 'OK',
                    onPress: async () => {
                      if (route.params?.from == "Checkout") {
                        route.params.onAdd();
                      }
                      // navigation.goBack();
                      // navigation.goBack();
                      navigation.navigate('Addresses')
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              alert(res.msg);
            }
          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Container style={{ backgroundColor: '#ffffff' }}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}>
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Thumbnail
              source={Assets.accountSettings.iconBack}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'center', alignItems: 'center', marginHorizontal: -43, }}>
          <Text style={{
            // fontFamily: 'Roboto',
            fontSize: 20,
            color: '#231f20',
          }}>Add Address</Text>
        </Body>
        <Right style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity onPress={() => {
            loading ? null : handleSaveAddress();
          }}>
            <Thumbnail
              source={Assets.accountSettings.checkIcon}
              style={styles.backIcon} />
          </TouchableOpacity>
        </Right>
      </Header>

      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        {loading ? <Spinner /> : null}
        <View style={styles.formContainer}>
          <Text style={styles.inputTitle}>First Name</Text>
          <TextInput style={styles.inputContainer}
            placeholder='First name'
            placeholderTextColor='#7f8c8d'
            value={fName}
            onChangeText={(e) => { setFName(e) }}
          />
          <Text style={styles.inputTitle}>Middle Name</Text>
          <TextInput style={styles.inputContainer}
            placeholder='Middle name'
            placeholderTextColor='#7f8c8d'
            value={mName}
            onChangeText={(e) => { setMName(e) }}
          />
          <Text style={styles.inputTitle}>Last Name</Text>
          <TextInput style={styles.inputContainer}
            placeholder='Last name'
            placeholderTextColor='#7f8c8d'
            value={lName}
            onChangeText={(e) => { setLName(e) }}
          />
          <Text style={styles.inputTitle}>Contact Number</Text>
          <TextInput style={styles.inputContainer}
            placeholder='Contact number'
            placeholderTextColor='#7f8c8d'
            maxLength={15}
            keyboardType='numeric'
            value={number}
            onChangeText={(e) => {
              if (+e || e == '' || e == 0) {
                setNumber(e)
              }
            }}
          />
          <Text style={styles.inputTitle}>Notes</Text>
          <TextInput style={styles.inputContainer}
            placeholder='Notes'
            placeholderTextColor='#7f8c8d'
            value={notes}
            onChangeText={(e) => { setNotes(e) }}
          />
          <Text style={styles.inputTitle}>House Number, Building and Street Name</Text>
          <TextInput style={styles.inputContainer}
            placeholder='House number, building and street name'
            placeholderTextColor='#7f8c8d'
            value={street}
            onChangeText={(e) => { setStreet(e) }}
          />
          <Text style={styles.inputTitle}>Province</Text>
          <Picker
            selectedValue={selectedProvince}
            style={[styles.inputPickerContainer, { marginBottom: iosMargin, marginTop: iosMargin }]}
            onValueChange={(itemValue, itemIndex) => {
              setSelectedProvince(itemValue)
              handleGetCity(itemValue)
            }}
          >
            {province.map((item, i) => {
              return (
                <Picker.Item label={item} value={item} key={i} />
              )
            })}
          </Picker>



          <Text style={styles.inputTitle}>City/Municipality</Text>
          <Picker
            selectedValue={selectedCity}
            style={[styles.inputPickerContainer, { marginBottom: iosMargin, marginTop: iosMargin }]}
            onValueChange={(itemValue, itemIndex) => {
              setSelectedCity(itemValue)
              handleGetBarangay(itemValue)
            }}
          >
            {city.map((item, i) => {
              return (
                <Picker.Item label={item} value={item} key={i} />
              )
            })}
          </Picker>

          <Text style={styles.inputTitle}>Barangay</Text>
          <Picker
            selectedValue={selectedBarangay}
            style={[styles.inputPickerContainer, { marginBottom: iosMargin, marginTop: iosMargin }]}
            onValueChange={(itemValue, itemIndex) => setSelectedBarangay(itemValue)}
          >
            {barangay.map((item, i) => {
              return (
                <Picker.Item label={item} value={item} key={i} />
              )
            })}
          </Picker>
          <Text style={{ marginBottom: 30 }}></Text>
        </View>
      </ScrollView>
    </Container>
  )
}

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5,
    height: 40,
    marginBottom: 10,
    // fontSize: 14  
  },
  inputPickerContainer: {
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5,
    // height: 40,
    marginBottom: 10,
    // fontSize: 14  
  },
  inputTitle: {
    // fontFamily:'Roboto',
    fontSize: 12,
  },
  PMBstyles: {
    color: '#7f8c8d',
    marginVertical: 10
  },
  headerStyles: {
    position: 'absolute',
    left: 20,
    alignSelf: 'center'
  },
  backIcon: {
    width: 18,
    height: 18
  },
  headerText: {
    // fontFamily:'Roboto', 
    fontSize: 20,
    alignSelf: 'center'
  },
  formContainer: {
    paddingHorizontal: 20,
    marginTop: 20
  }
})