import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Image,
  Dimensions
} from 'react-native';

import {
  Header,
  Left,
  Body,
  Right,
  Container,
  Thumbnail,
  Form,
  Item,
  Input,
  Label,
  Button,
  Title,
  Content,
} from 'native-base';

import FastImage from 'react-native-fast-image'
import Assets from '../../components/assets.manager';
const { width, height } = Dimensions.get('window')


class BankAccountScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {

    }
  }

  render() {
    return (
      <Container>

        <Header style={styles.header}>
          <Left style={{ flex: 0, paddingHorizontal: 3 }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Thumbnail
                source={Assets.accountSettings.arrowDarkIcon}
                square
                style={styles.backIcon}
              />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={styles.headText}>Bank Accounts / Cards</Text>
          </Body>
          <Right style={{ flex: 0 }}>
            <View style={{ width: 20 }} />
          </Right>
        </Header>

        <Content style={{ backgroundColor: '#f2f2f2' }}>
          <View style={{ paddingTop: 12 }} />

          <View style={styles.textContainer}>
            <Text style={styles.titleText}>Your bank account</Text>
          </View>

          <View style={styles.border} />

          <View style={[styles.textContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
            <Text style={styles.contentText}>BPI Debit Card</Text>
            <TouchableOpacity onPress={() => alert('Coming soon!')}>
              <View style={{
                width: 20,
                height: 15,
              }}>
                <FastImage
                  source={require('../../assets/internal/more-with-three-dots-button.png')}
                  style={styles.threeDots}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.border} />

          <View style={[styles.textContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
            <Text style={styles.contentText}>BDO</Text>
            <TouchableOpacity onPress={() => alert('Coming soon!')}>
              <View style={{
                width: 20,
                height: 15,
              }}>
                <FastImage
                  source={require('../../assets/internal/more-with-three-dots-button.png')}
                  style={styles.threeDots}
                />
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.border} />

          <TouchableOpacity onPress={() => alert('Coming soon!')} style={[styles.textContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
            <View style={{
              flex: 0,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>

              <Text style={styles.addnewAccount}>+ Add New Bank Account</Text>
            </View>
            <View style={{
              flex: 1,
            }} />
          </TouchableOpacity>

          <View style={styles.border} />

        </Content>
      </Container>
    );
  }

}
export default BankAccountScreen;

const styles = {
  header: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold'
  },
  textContainer: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 15,
    paddingBottom: 15,
    width: '100%',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  contentText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  space: {
    paddingTop: 15,
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#e1e2e3',
  },
  threeDots: {
    width: 12,
    height: 12,
    paddingLeft: 20,
    alignSelf: 'center',
  },
  addnewAccount: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
}
