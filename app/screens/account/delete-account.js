import React, { useState, useEffect } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  TextInput,
  BackHandler,
  Dimensions,
  Platform,
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Spinner,
} from 'native-base'

import Assets from '../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../components/api/service';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';

import auth from '@react-native-firebase/auth';
import { LoginManager, Profile, } from 'react-native-fbsdk-next';

const { width, height } = Dimensions.get('window');
const initialLayout = { width: Dimensions.get('window').width };

export default function AccountVerification({ navigation, route }) {
  const [user, setUser] = useState('');
  const [loading, setLoading] = useState(true);
  const [password, setPassword] = useState(true);
  const [emailVerified, setEmailVerified] = useState(true)
  const [phoneVerified, setPhoneVerified] = useState(true)
  const [hasEmailOrContact, setHasEmailOrContact] = useState(true)

  useEffect(() => {
    init()
  }, [])

  const init = async () => {
    setLoading(true)

    let ud = await AsyncStorage.getItem("user_data")
    console.log("this is user_data", ud)
    let _userdata = ud ? JSON.parse(ud) : null
    setUser(_userdata)
    // console.log('_userdata get in profile screen', _userdata)
    if (_userdata) {
      await Service.userData(
        _userdata.data.id,
        async (res) => {
          setUser(res.data)
          _userdata = res;

          await AsyncStorage.setItem('user_data', JSON.stringify(res));
          // console.log('res.data shop directory:', res.data.info.data.shop.directory)
          // console.log('res.data cover_photo directory:', res.data.info.data.cover_photo.full_path)
          // console.log('res.data shop full_path:', res.data.info.data.shop.full_path)
          setCover({ uri: res.data.info.data.cover_photo.full_path })
        },
        (err) => console.log("Service.userData Error in Profile Screen", err)
      )
      await Service.userProducts(_userdata.data.id,
        (res) => setproducts(res.data),
        (err) => console.log("Service.userProducts Error in Profile Screen", err)
      )

      let splitEmail = _userdata.data.info.data.email.split('@');
      if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
        setHasEmailOrContact(false)
        console.log("user has no email", _userdata.data.info.data.email)
      } else {
        console.log("user has email", _userdata.data.info.data.email)
        setHasEmailOrContact(true)

        if (_userdata.data.is_verify === 'no') {
          console.log("user is not email verified", _userdata.data.is_verify)
          setEmailVerified(false)
        } else {
          console.log("user is email verified", _userdata.data.is_verify)
          setEmailVerified(true)
        }
      }

      if (_userdata.data.is_verify_contact_number === 'no') {
        console.log("user is not phone verified", _userdata.data.is_verify_contact_number)
        setPhoneVerified(false)
      } else {
        console.log("user is phone verified", _userdata.data.is_verify_contact_number)
        setPhoneVerified(true)
      }

      setLoading(false)
    } else {
      setLoading(false)
    }
  }

  const verifyPassword = async () => {
    setLoading(true);


    await Service.login(
      { uid: user.info.data.email, pwd: password },
      async (res) => {
        if (res.status) {
          handleVerifyClick()
        } else {
          Alert.alert('Password is incorrect')
          setLoading(false);
        }
      },
      (err) => {
        setLoading(false);
        Alert.alert('API error')
      },
    );


    setLoading(false);
  }

  const handleVerifyClick = async (verified = 'no', reloads = 0) => {
    setLoading(true);
    let data = {
      user_id: user.id,
      verified: verified,
    }

    try {
      await Service.deleteAccount(
        data,
        (res) => {
          if (verified === 'no') {
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.verification_code);
              let verifData = {
                verifyCode: res.verification_code,
                medium: res.sent_to,
                expiration: res.verification_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  handleVerifyClick(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          } else {
            if (res.status) {
              setLoading(false);
              console.log('Data: ', res);
              Alert.alert(
                'Account Deleted',
                'Your Account has been successfully deleted.\n' +
                '\n' +
                'Thank you for using Referapps!',
                [
                  {
                    text: 'Ok',
                    onPress: async () => {
                      console.log("logout start")
                      // await AsyncStorage.removeItem(API_TOKEN)
                      await AsyncStorage.removeItem('auth');
                      await AsyncStorage.removeItem('user_data');
                      await AsyncStorage.removeItem('chat');
                      await AsyncStorage.removeItem('fcmtoken');
                      // await Notification.removeToken();

                      // Firebase Signout start                        
                      auth()
                        .signOut()
                        .then((response) => {
                          console.log("THIS IS FIREBASE SIGN OUT RESPONSE", response)
                        })
                        .catch(error => {
                          alert(error)
                        })
                      // Firebase Signout end  

                      await Profile.getCurrentProfile().then(async (res) => {
                        if (res != null) {
                          // FB signout start
                          await AsyncStorage.removeItem('fb_user_id');
                          LoginManager.logOut();
                          // FB signout end
                        }
                      })

                      console.log("logout end")


                      navigation.reset({
                        index: 0,
                        routes: [{ name: 'Tab' }],
                      });
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              alert(res.msg);
            }
          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Container>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>Delete Account</Text>
      </Header>
      {loading ? <Spinner />
        :
        <>
          {!hasEmailOrContact || (!emailVerified && !phoneVerified) ?
            <>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: "#fff",
                justifyContent: 'center',
                marginTop: 20,
                maxHeight: 40,
              }}>
                <Text style={{ textAlign: 'center', fontSize: 18, color: "gray", }}>
                  Verify your account first!
                </Text>
              </View>

              <View style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: "#fff",
                justifyContent: 'center',
                maxHeight: 50,
              }}>
                <TouchableOpacity onPress={() => {
                  if (!hasEmailOrContact) {
                    navigation.navigate("AddEmailMobile");
                  } else if (!emailVerified) {
                    navigation.navigate('EmailVerification')
                  }
                }} style={{
                  marginVertical: 3,
                  padding: 8,
                  width: width - 10,
                  maxHeight: 50,
                }}>
                  <LinearGradient
                    colors={['#f89522', '#f58223', '#f36e23']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.btnSubmit}>
                    <Text style={styles.btnTextSubmit}>Verify Email</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
              <View style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: "#fff",
                justifyContent: 'center',
                maxHeight: 50,
              }}>
                <TouchableOpacity onPress={() => {
                  if (!hasEmailOrContact) {
                    navigation.navigate("AddEmailMobile");
                  } else if (!phoneVerified) {
                    navigation.navigate('PhoneVerification')
                  }
                }} style={{
                  marginVertical: 3,
                  padding: 8,
                  width: width - 10,
                  maxHeight: 50,
                }}>
                  <LinearGradient
                    colors={['#f89522', '#f58223', '#f36e23']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.btnSubmit}>
                    <Text style={styles.btnTextSubmit}>Verify Mobile</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </>
            :
            <View style={{ backgroundColor: '#ffffff' }}>
              <>
                <Text style={styles.sendVText}>Please enter your password</Text>

                <View style={styles.inputStyles}>
                  <TextInput
                    autoCapitalize="none" autoCorrect={false}
                    style={styles.inputEmail}
                    placeholderTextColor='#7f8c8d'
                    placeholder='Password'
                    value={password}
                    secureTextEntry={true}
                    onChangeText={(val) => {
                      setPassword(val)
                    }}
                  >
                  </TextInput>
                </View>

                <TouchableOpacity onPress={async () => {
                  if (!loading) {
                    verifyPassword();
                  }
                }}
                >
                  <LinearGradient
                    colors={['#f89522', '#f58223', '#f36e23']}
                    start={{ x: 0, y: 0 }}
                    end={{ x: 1, y: 0 }}
                    style={styles.btnSubmit}>
                    <Text style={styles.btnTextSubmit}>Submit</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </>
            </View>
          }
        </>
      }
    </Container>
  )
}

const styles = StyleSheet.create({
  headerText: {
    // fontFamily:'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center'
  },
  backIcon: {
    width: 18,
    height: 18
  },
  sendV: {
    color: '#f36e23',
    // fontFamily:'Roboto',
    fontSize: 13,
    fontStyle: 'italic',
    alignSelf: 'center'
  },
  sendVText: {
    // fontFamily:'Roboto',
    color: 'gray',
    fontSize: 16,
    padding: 20,
    textAlign: 'center'
  },
  inputEmail: {
    fontSize: 15,
    color: '#231f20',
    paddingHorizontal: 20,
    fontFamily: 'Roboto'
  },
  inputStyles: {
    backgroundColor: '#ecf0f1',
    borderRadius: 5,
    marginHorizontal: 30,
    marginBottom: 20,
    height: 40,
  },
  btnSubmit: {
    borderRadius: 5,
    marginHorizontal: 30,
    height: 38,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center'
  },
  btnTextSubmit: {
    fontSize: 16,
    fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',

  },
})
