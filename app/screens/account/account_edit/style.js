import React from 'react';
import { Dimensions } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const { width, height } = Dimensions.get('window');

export const styles = {
  // containerModal: {
  //   top: 0,
  //   right: 0,
  //   bottom: 0,
  //   left: 0,
  //   justifyContent: 'space-around',
  //   alignItems: 'center',
  //   backgroundColor: 'rgba(0, 0, 0, .5)',
  //   position: 'absolute',
  // },
  txtProcessing: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
  },
  loadingMainContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingContainer: {
    backgroundColor: '#231f20',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: 160,
    height: 160,
    borderRadius: 30,
  },
  backIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  submitIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    // marginRight: 10,
  },
  changePassword: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialmedia: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#5394fc',
  },
  headText: {
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    color: '#231f20',
    fontWeight: 'bold',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
  },
  errorText: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: 'red',
    paddingLeft: 20,
    paddingBottom: 2,
  },
  inputText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    flex: 0,
  },
  space: {
    paddingTop: 15,
  },
  border: {
    height: 1,
    backgroundColor: '#ecf0f1',
  },
  headerContainer: {
    height: 190,
    width,
  },
  headerCoverImage: {
    width,
    height: 230,
    justifyContent: 'center',
  },
  titleView: {
    flex: 0,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 15,
    paddingBottom: 0,
  },
  titles: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  textInput: {
    // fontFamily: 'robot-light',
    fontSize: 14,
    color: '#231f20',
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  password: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialAccounts: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#45bcef',
  },
  avatar: {
    justifyContent: 'space-around',
    alignSelf: 'flex-start',
    paddingLeft: 20,
  },
  avatarImage: {
    alignSelf: 'center',
    height: 110,
    width: 110,
    borderRadius: 110 / 2,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    height: 200,
  },
  verifyTextContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    paddingVertical: 20,
  },
  verifyTitle: {
    color: 'black',
    paddingTop: 25,
    paddingHorizontal: '9%',
    // fontFamily:'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingVertical: 10,
  },
  title: {
    color: '#f36e23',
    // paddingVertical:40,
    paddingTop: 40,
    paddingHorizontal: 15,
    // fontFamily:'Roboto',
    fontSize: 10,
  },
  inputPassword: {
    borderBottomColor: '#e1e2e3',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    width: '90%',
    alignSelf: 'center',
    // fontFamily:'Roboto',
  },
  inputVerify: {
    borderColor: '#e1e2e3',
    borderWidth: 1,
    width: '80%',
    alignSelf: 'center',
    // fontFamily:'Roboto',
    // paddingBottom:20
  },
  btnSave: {
    width: '80%',
    // height:'40%',
    backgroundColor: '#ababab',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  btnVerify: {
    width: '80%',
    backgroundColor: '#00a14b',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 3,
  },
  headerMyProfile: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15,
      },
      android: {},
    }),
  },
  btnTextSave: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 15,
    // fontFamily:'Roboto'
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 15
    // marginTop: 22
  },
  modalView: {
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    // padding: 35,
    // alignItems: "center",
    // elevation: 5
    resizeMode: 'contain',
    height: '22%',
    width: '70%'
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  // textStyle: {
  //   color: "white",
  //   fontWeight: "bold",
  //   textAlign: "center"
  // },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
};

export const sell = {
  headerElevate: {
    ...Platform.select({
      ios: {
        justifyContent: 'center',
        marginTop: -20
      },
      android: {
        justifyContent: 'center',
        marginTop: 30,
      }
    })
  },
  container: {
    backgroundColor: '#fff',
  },
  contentContainer: {
    backgroundColor: '#edf0f2',
  },
  backImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  continueImage: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginRight: 10,
  },
  fullHeightContainer: {
    height: height,
  },
  headerText: {
    // fontFamily: 'Roboto',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  uploadContainer: {
    height: height / 4.3,
    width: width,
    flexDirection: 'row',
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#edf0f2',
  },
  imageItem: {
    backgroundColor: '#ffffff',
    paddingTop: 2,
    paddingRight: 2,
    paddingLeft: 2,
    paddingBottom: 2,
    height: 168,
    width: (width / 2 - 8),
    borderWidth: 3,
    borderColor: '#ecf0f1',
  },
  newProductImage: {
    alignSelf: 'center',
    width: wp('43%'),
    height: hp('25%'),
  },
  inputContainer: {
    width: width,
    height: 34,
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    // paddingHorizontal: 15
  },
  inputPickerContainer: {
    width: width,
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    ...Platform.select({
      ios: {
        flexDirection: 'row',
        backgroundColor: '#e6e6e6',
        paddingRight: 10,
        paddingLeft: 5,
        borderRadius: 5,
        marginVertical: 0,
        paddingVertical: 0,
        // height: height / 6,
      },
      android: {
        backgroundColor: '#fff',
        height: 34,
      }
    })
    // paddingHorizontal: 15
  },
  inputContainers: {
    width: width,
    height: 34,
    // flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  descriptionContainer: {
    width: width,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  warrantyContainer: {
    width: width,
    height: 100,
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  inputText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginLeft: 15,
    color: '#231f20',

  },
  descriptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    // color: '#7f8c8d',
    textAlignVertical: 'top',
    justifyContent: 'space-around',
    marginLeft: 15,
    maxHeight: hp('32%'),
    minHeight: hp('11.50%'),
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  hintText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
  },
  hintText2: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
  },
  nextButton: {
    justifyContent: 'space-around',
    alignSelf: 'center',
    width: '35%',
    backgroundColor: '#e67300',
    height: 30,
    borderRadius: 20,
  },
  pickerText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  attribute_title: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: '#7f8c8d',
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
  },
  attribute_required: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: '#ff1a1a',
    paddingRight: 5,
    paddingTop: 10,
    paddingBottom: 10,
    width: '25%',
  },
  item_title: {
    width: '70%',
    paddingLeft: 5,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: 'transparent',
  },

  group_title: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    marginLeft: 10,
    paddingBottom: 5,
    // fontWeight: 'bold',
  },
  group_input: {
    textAlign: 'right',
    paddingLeft: 5,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: 'transparent',
    alignItems: 'flex-end',
  },
  group_input_text: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: 12,
    // color: '#7f8c8d',
    marginRight: 30
  },
  input_title: {
    width: '35%',
    paddingLeft: 5,
    marginRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: 'transparent',
  },

  variationInput: {
    width: '100%',
    height: 40,
    borderWidth: 1,
    borderColor: '#bfbfbf',
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderLeftWidth: 2,
  },
  variationsName: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
    paddingLeft: 10,
    paddingRight: 10,
    width: '100%',
  },
  variationsOption: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  addOptionButton: {
    backgroundColor: '#05ad4f',
    width: '100%',
    height: 30,
    borderRadius: 20,
    justifyContent: 'space-around',
  },
  addOptionText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
    alignSelf: 'center',
  },
  setStockAndpriceButton: {
    backgroundColor: '#00a14b',
    width: '100%',
    height: 30,
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'space-around',
  },
  variationRow: {
    width: (width / 3),
    justifyContent: 'space-around'
  },
  variationTitle: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    // alignSelf: 'center',
    textAlign: 'left',
    // fontFamily: 'italic'
  },
  variationOption: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    alignSelf: 'center',
  },
  optionInputContainer: {
    width: '80%',
    height: 36,
    borderWidth: .7,
    borderColor: '#e1e2e3',
    justifyContent: 'space-around',
  },

  row: {
    flexDirection: 'row',
  },
  space: {
    paddingTop: 5,
  },
  space20: {
    paddingTop: 20,
  },
  spaceLeft: {
    paddingLeft: 10,
  },
  spaceLeft20: {
    paddingRight: 20,
  },
  spaceRight: {
    paddingRight: 5,
  },
  border: {
    height: .7,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  white: {
    backgroundColor: '#ffffff',
    borderTopWidth: 1,
    borderTopColor: '#e1e2e3',
    borderBottomColor: '#e1e2e3',
    paddingTop: 8,
    paddingBottom: 8,
  },
  itemBorder: {
    borderTopWidth: .7,
    borderTopColor: '#b7bdbf',
    width: width,
  },
  whiteBorder: {
    backgroundColor: '#ffffff',
    borderTopWidth: 1,
    borderTopColor: '#e1e2e3',
    borderBottomWidth: .5,
    borderBottomColor: '#e1e2e3',
    paddingTop: 8,
    paddingBottom: 8,
  },
  CategorywhiteBorder: {
    backgroundColor: '#ffffff',
    borderTopWidth: 1,
    borderTopColor: '#e1e2e3',
    borderBottomWidth: 4,
    borderBottomColor: '#e1e2e3',
    paddingTop: 10,
    paddingBottom: 10,
  },
  // statusBar: {
  //   height: STATUSBAR_HEIGHT,
  // },

  // ADDED STYLES
  rightView: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  ProductNameItem: {
    width: '85%',
    // paddingTop: 5,
    // paddingBottom: 5,
  },
  Length50: {
    // fontFamily: 'Roboto',
    fontStyle: 'italic',
    fontSize: hp('1.5%'),
    color: '#7f8c8d',
    // paddingRight: 5,
    paddingTop: 5,
    paddingBottom: 5,
  },
  DescriptionItem: {
    width: '100%',
    // paddingTop: 10,
    // paddingBottom: 10,
    borderColor: 'transparent',
    flex: 0
  },
  right: {
    justifyContent: 'flex-end',
    marginBottom: 1
  },
  openIcon: {
    width: 13,
    height: 13,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    marginTop: 2
  },
  Weight: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    marginLeft: 15,
    // color: '#78c8d',
  },
  Kg: {
    textAlign: 'right',
    // fontFamily: 'italic',
    fontSize: 12,
    color: '#7f8c8d',
  },
  Kg: {
    textAlign: 'right',
    // fontFamily: 'italic',
    fontSize: 12,
    color: '#7f8c8d',
  },
  weight_group_input_text: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: 12,
    // color: '#7f8c8d',
    // marginRight: 30
  },
  size_group_input_text: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
    width: wp('50%')
  },
  sideIcon: {
    resizeMode: 'contain',
    width: 17,
    height: 17,
  },
  WarrantyItem: {
    width: '90%',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    borderColor: 'transparent',
  },
  SellText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto', 
    fontSize: 14,
    color: '#ffffff',
  },
  VariationRightHeader: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  variationsContainer: {
    backgroundColor: '#ffffff',
    paddingLeft: 20,
    paddingRight: 20,
  },
  View1st: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
    paddingBottom: 20,
  },
  PackagingModalConatiner: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    width: '85%',
    borderRadius: 10,
    justifyContent: 'space-around',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
  },
  PhotoModalContainer: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    width: '85%',
    justifyContent: 'space-around',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
  },
  PackagingModalHeaderText: {
    // fontFamily: 'Roboto',
    fontSize: 15,
    color: '#231f20',
  },
  size_group_input: {
    borderWidth: .7,
    borderColor: '#95a5a6',
    height: 30,
    paddingLeft: 10,
    paddingRight: 10,
    justifyContent: 'space-around',
    flexDirection: 'row',
  },
  group_inptut_title: {
    textAlign: 'left',
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 5,
    paddingBottom: 25,
    flex: 1,
    fontWeight: '700',
  },
  SizeNote: {
    // fontFamily: 'Roboto',
    fontSize: 13,
    color: '#231f20',
  },
  SaveBtn: {
    backgroundColor: '#00a14b',
    alignSelf: 'center',
    width: '100%',
    height: 30,
    borderRadius: 20,
    justifyContent: 'center',
  },
  SaveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#ffffff',
  },
  disableSaveBtn: {
    backgroundColor: '#95a5a6',
    alignSelf: 'center',
    width: '100%',
    height: 30,
    borderRadius: 20,
    justifyContent: 'center',
  },
  stockAndprice1stView: {
    backgroundColor: '#ecf0f1',
    justifyContent: 'space-between',
    flex: 3,
  },
  stockAndpriceViewContainer: {
    backgroundColor: '#ffffff',
    // height: height/4.8,
  },
  stockAndPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    paddingLeft: 25,
  },
  variationTitle1: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#7f8c8d',
    alignSelf: 'center',
    // fontFamily: 'italic'
  },
  StockPriceText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    paddingLeft: 10,
    paddingRight: 10,
    width: '92%',
  },
  SetCommission: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: hp('1.50%'),
    color: '#231f20',
    paddingLeft: hp('10%'),
    paddingRight: hp('5%'),
    flex: 0,
    borderBottomColor: '#e1e2e3',
    borderBottomWidth: 1,
    width: wp('40%'),
    marginTop: hp('6%'),
    textAlign: 'right',
    paddingRight: 0,
  },
  ReferrerModalHeaderText: {
    // fontFamily: 'Roboto',
    fontSize: hp('1.50%'),
    color: '#e67300',
    paddingTop: hp('5%'),
  },
  ReferrerModalConatiner: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    width: wp('90%'),
    borderRadius: 5,
    justifyContent: 'space-around',
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 20,
    paddingBottom: 20,
    height: height / 4.1,
    paddingHorizontal: 50
  },
  whiteDescription: {
    backgroundColor: '#ffffff',
    borderTopWidth: 1,
    borderTopColor: '#e1e2e3',
    borderBottomColor: '#e1e2e3',
  },
  percentText: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: hp('1.50%'),
    textAlign: 'center',
    paddingHorizontal: 0,
    paddingLeft: 0,
    marginLeft: 0,
    marginHorizontal: 0,
    marginTop: hp('6%')
  },
  serviceContainer: {
    backgroundColor: '#edf0f2',
    flex: 1,
  },
  condition_group_input_text: {
    textAlign: 'right',
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
    fontWeight: '500'
  },
  whiteName: {
    backgroundColor: '#ffffff',
    borderTopWidth: 1,
    borderTopColor: '#e1e2e3',
    borderBottomColor: '#e1e2e3',
  },
  referrerSaveBtn: {
    backgroundColor: '#00a14b',
    alignSelf: 'center',
    width: wp('70%'),
    height: hp('3.50%'),
    borderRadius: 20,
    justifyContent: 'center',
    marginTop: hp('6%'),
    marginBottom: hp('1.50%')
  },
  //Footer
  footerCon: {
    backgroundColor: 'orange',
    height: 60,
    borderTopColor: "transparent",
    shadowColor: '#47494c',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 4,
    position: 'absolute',
    bottom: 0
  },
  chatCon: {
    justifyContent: 'space-around',
    width: '20%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  barrier: {
    width: .7,
    marginTop: 12,
    marginBottom: 12,
    borderLeftWidth: .7,
    borderColor: '#00a14b'
  },
  btnModalAddCon: {
    justifyContent: 'space-around',
    width: '20%',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
  },
  btnBuynow: {
    width: '40%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#79b956'
  },
  fieldErrors: {
    padding: 10,
    // paddingLeft: 10,
    paddingBottom: 5,
    paddingTop: 13,
  },
  datePicker: {
    paddingHorizontal: 10,
    borderRadius: 5,
    // marginVertical: 5,
    // marginBottom: 10,
    // color: '#7f8c8d',
    width: height / 6,
    ...Platform.select({
      ios: {
        // paddingTop: 12
      },
      android: {
        transform: [
          { scaleX: 0.8 },
          { scaleY: 0.8 }
        ]
      },
    })
  },

}