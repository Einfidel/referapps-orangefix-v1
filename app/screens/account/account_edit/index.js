import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Alert,
  ImageBackground,
  TextInput,
  Modal,
  Dimensions,
  Platform,
  BackHandler
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { Header, Left, Body, Right, Container, Button } from 'native-base';
import auth from '@react-native-firebase/auth';

const { width, height } = Dimensions.get('window');

import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';
import * as Animatable from 'react-native-animatable';
import Moment from 'moment';
import ImagePicker from 'react-native-image-picker';
import Toast, { DURATION } from 'react-native-easy-toast';
import Assets from '../../../components/assets.manager';
import Service from '../../../components/api/service';
// import {sendMessage} from '../../components/Sms';
import stylings from '../../../styles/signup/signup';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';
import { cos } from 'react-native-reanimated';
const profilepic = require('../../../assets/internal/icon_picframe.png');

import WebView from 'react-native-webview'

import { styles, sell } from './style'
import { backgroundColor } from 'styled-system';

class MyProfileScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      lastRefresh: Date(Date.now()).toString(),
      cover: require('../../../assets/internal/default-cover.jpg'),
      fname: '',
      lname: '',
      username: '',
      gender: '',
      birthdate: '',
      contact_number: '',
      title: '',
      description: '',
      email: '',
      image: '',
      password: 'gbHxKK14',
      country_code: '',
      errors: [],
      additional_info_errors: [],

      filePath: {
        data: '',
        uri: '',
      },

      fileData: '',
      fileUri: '',

      reload: null,
      refreshing: false,

      verification_code: '',
      verfication_code_input: '',
      verification_type: '',

      isVerified: 'no',
      verification_session: '',

      imageUpdated: false,
      userLoaded: null,
      update: true,
      user: [],
      dateOpen: false,
      date: new Date(),
      mode: 'date',
      isDateTimePickerVisible: false,
      modalVisible: false,
      vmodalVisible: false,
      webViewModal: false,
      isVisible: false,
      loading: false,
      photoModal: false,
      image: null,
      imageSelected: null,
    };
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = (date) => {
    this.setState({
      birthdate: Moment(date).format('Y-MM-DD'),
    });
    this._hideDateTimePicker();
  };

  componentDidMount = async () => {
    let db = await AsyncStorage.getItem('user_data');
    let user = typeof db == 'string' ? JSON.parse(db) : null

    if (user.data.id == null) return

    let id = user.data.id;

    try {
      await Service.getShopProfile(id).then((res) => {
        console.log("Service.getShopProfile res", res)
        this.setState({ cover: { uri: res.data.info.data.cover_photo.full_path } })
      });
    } catch (error) {
      console.log("Service.getShopProfile Error in View As Screen", error)
    }
  }

  componentWillUnmount = () => {
    this.unsubscribe();
    auth().currentUser !== null &&
      auth().currentUser.phoneNumber !== null &&
      auth()
        .currentUser.delete()
        .then(function () {
          console.log('Firebase user with phone number deleted!');
          auth().signOut();
        })
        .catch(function (error) {
          console.warn('Error', error.message);
        }) &&
      auth().signOut();
    auth().signInAnonymously();

    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  };

  UNSAFE_componentWillMount = async () => {
    this.unsubscribe = auth().onAuthStateChanged(function (user) {
      // user.phoneNumber !== '' && auth().signOut();
      // user.phoneNumber !== null && auth().signOut();
      // user.providerData.phoneNumber !== null && auth().signInAnonymously();
      // user.uid || user && auth().signInAnonymously();
      console.log('User', JSON.stringify(user, null, '\t'));
    });
    let ud = await AsyncStorage.getItem('user_data');
    let userData = ud ? JSON.parse(ud) : null;
    if (userData) {
      let THIS = this;
      console.log("USER DATA", JSON.stringify(userData, null, '\t'));
      await Service.userData(
        userData.data.id,
        (res) => {
          let data = res.data;
          let info = res.data.info.data;
          // console.log("USER DATA", JSON.stringify(res, null, '\t'));
          THIS.setState({
            fname: data.fname,
            lname: data.lname,
            username: data.username,
            gender: info.gender,
            birthdate: info.birthdate,
            title: info.title,
            description: info.description,
            contact_number: info.contact_number,
            email: info.email,
            image: info.avatar.path ? info.avatar.full_path : data.image,
            user: data,
            userLoaded: true,
            isVerified: info.is_verify,
            country_code: userData.data.country_code,
          });
        },
        (err) => console.warn('Error', err.message),
      );
    }
  };

  // signInWithPhoneNumber = async () => {
  //   let { contact_number, country_code } = this.state;
  //   let ccode = country_code === 'PH' ? '+63' : '';
  //   const confirmation = await auth().signInWithPhoneNumber(
  //     ccode + contact_number,
  //   );
  //   console.warn('Confirm', JSON.stringify(confirmation, null, '\t'));
  //   this.setState({ verification_code: confirmation });
  // };

  handleUpdateProfile = async () => {
    await this.setState({
      loading: true,
    });
    await Service.editProfile(
      this.state,
      (res) => {
        if (res.status === true) {
          let { contact_number, email } = this.state;
          // console.log('Response', res, this.state.verification_code);
          this.setState(
            {
              loading: false,
              verification_code: res.verification_code,
              vmodalVisible: true,
              modalVisible: false,
            },
            () => contact_number !== '' && this.handleSendMessage(),
          );
        } else {
          console.log(res, this.state.verification_code);
          this.setState({
            loading: false,
          });
          Alert.alert(
            'Update Failed!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: () => {
                  this.setState({ update: true });
                  this.setState({ errors: res.errors });
                },
              },
            ],
            { cancelable: false },
          );
        }
      },
      (err) => {
        this.setState({
          loading: false,
          modalVisible: false,
        });
        Alert.alert('Error', 'Please try again.');
        console.warn('Error', err.message);
      },
    );
  };

  verifyCode = async () => {

  };

  updateLocalStorage = () => {
    AsyncStorage.getItem('user_data').then((value) => {
      let data = JSON.parse(value).userData.data;
      let newdata = {
        country_code: 'PH',
        "downline_commission": 0,
        "fb_id": null,
        "id": data.id,
        "image": this.state.image != '' ? this.state.image : data.image,
        "is_security_question_set": data.is_security_question_set,
        "is_verify": data.is_verify,
        "fname": this.state.fname != '' ? this.state.fname : data.fname,
        "lname": this.state.lname != '' ? this.state.lname : data.lname,
        "notifications": data.notifications,
        "product_sales": data.product_sales,
        "products": data.products,
        "ratings": data.ratings,
        "refer_id": data.refer_id,
        "reviews": data.reviews,
        "sales_commission": data.sales_commission,
        "services": data.services,
        "shop_name": data.shop_name,
        "total_cash": data.total_cash,
        username:
          this.state.username != '' ? this.state.username : data.username,
      };
      AsyncStorage.setItem('user_data', JSON.stringify({ data: newdata }));
    });
  };

  //USED FOR SEND CODE VIA EMAIL SEARCH
  handleRequestSendCodeFull = async (token) => {
    this.setState({ loading: true })
    const res = await Service.findAccountByEmail(this.state.email)
    let num = res.data.info.data.contact_number
    console.log(num)
    if (res) {
      if (res.status) {
        let otp = await Service.authGoogleSendOtp(token, '+' + res.data.info.data.contact_number)
        if (otp.error) {
          console.log(otp.error.errors)
          alert("Unable to send verification code. Try again later.")
          this.setState({ loading: false })
        } else {
          console.log(otp)
          this.setState({ loading: false, verification_session: otp.sessionInfo, vmodalVisible: true })
        }
      }
    }
  }

  handleRequestSendCode = async (token) => {
    this.setState({ loading: true })
    let otp = await Service.authGoogleSendOtp(token, '+63' + this.state.contact_number)
    if (otp.error) {
      console.log(otp.error.errors)
      alert("Unable to send verification code. Try again later.")
      this.setState({ loading: false })
    } else {
      console.log(otp)
      this.setState({ loading: false, verification_session: otp.sessionInfo, vmodalVisible: true })
    }
  }

  chooseFile = () => {
    // this.setState({ photoModal: true })

    Alert.alert('Coming Soon!', 'This feature will be available soon.')

    // this.handleChangePhoto(this.state.image);
  };

  handleImagePicker = () => {
    if (this.state.image != null && this.state.image != '') {
      // console.log("image", image)
      // console.log("image.uri", image.uri)
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          this.setState({ photoModal: true })
        }}>
          <FastImage
            source={{ uri: this.state.image.uri }}
            style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto();
        }} >
          <FastImage
            source={require('../../../assets/Sell_Product-09.png')}
            large
            style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
          />
        </TouchableOpacity>
      );
    }
  }

  handleAddPhoto = () => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        if (response.fileSize > 5000000) {
          Alert.alert(
            'Alert',
            'Image must be less than 5Mb'
          )
        } else {
          this.setState({ image: response })
        }
      }
    });
  };

  handleChangePhoto = () => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      console.log('showImagePicker response.uri = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        if (response.fileSize > 5000000) {
          Alert.alert(
            'Alert',
            'Image must be less than 5Mb'
          )
        } else {
          this.setState({ image: response })
          // console.log("response image", response)
        }
      }
    });
  };

  handleRemovePhoto = () => {
    this.setState({ image: null })
  };

  renderPhotoModal = () => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={this.state.photoModal}
        // style={{ justifyContent: 'center' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              this.setState({ photoModal: false })
            }}
          // style={{ backgroundColor: 'red' }}
          >
            <View style={styles.containerModal}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    this.handleChangePhoto(this.state.image, 'tinssn');
                    this.setState({ photoModal: false })
                  }}
                >
                  Change Image...
                </Text>

                {/* <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected, 'tinssn');
                    this.setState({ photoModal: !photoModal })
                  }}
                >
                  Remove Image...
                </Text> */}

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    this.setState({ photoModal: false })
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  render() {
    return (
      <Container style={{ backgroundColor: '#ffffff' }}>

        <Header transparent style={styles.headerMyProfile}>
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <FastImage
                source={Assets.accountSettings.arrowDarkIcon}
                square
                small
                style={styles.backIcon}
              />
            </Button>
          </Left>

          <Body style={{ flex: 0, justifyContent: 'center' }}>
            <Text style={styles.headText}>My Profile</Text>
          </Body>

          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => {
                if (this.state.errors && this.state.errors.contact_number) {
                  // this.setState({ vmodalVisible: !this.state.vmodalVisible })
                } else {
                  // this.setState({ vmodalVisible: !this.state.vmodalVisible })
                }
                Alert.alert('Coming Soon!', 'This feature will be available soon.')
              }}>
              <FastImage
                source={require('../../../assets/headericon/continue.png')}
                square
                small
                style={styles.submitIcon}
              />
              {/* {
                modalStatus === true ? <SaveModal /> : null
              } */}
            </Button>
          </Right>
        </Header>

        <TouchableWithoutFeedback
          // containerStyle={{ backgroundColor: "red" }}
          // style={{ backgroundColor: "red" }}
          onPress={() => {
            this.setState({ modalVisible: false })
            console.log("clickity");
          }}>
          <Modal
            style={{ flex: 1, backgroundColor: "red" }}
            animationType='fade'
            transparent={true}
            visible={this.state.modalVisible}
          // onRequestClose={() => {
          //   this.setState({ modalVisible: false });
          // }}
          >
            <View style={styles.mainContainer}>
              <View style={styles.textContainer}>
                <Text style={styles.title}>
                  Enter your password to apply changes
                </Text>
                <View>
                  <TextInput
                    secureTextEntry={true}
                    placeholder="Enter here. . ."
                    style={styles.inputPassword}
                    onChangeText={(e) => {
                      this.setState({
                        password: e,
                      });
                    }}
                  />
                </View>
                <View style={{ paddingVertical: 20 }}>
                  <TouchableOpacity
                    onPress={() => {
                      // this.handleUpdateProfile();
                      this.setState({ webViewModal: true, modalVisible: false })
                    }}
                    style={styles.btnSave}>
                    <Text style={styles.btnTextSave}>Save Changes</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => this.setState({ vmodalVisible: false })}
        >
          <Modal
            style={{ flex: 1 }}
            animationType='fade'
            transparent={true}
            visible={this.state.vmodalVisible}
            onRequestClose={() => {
              this.setState({ vmodalVisible: false });
            }}>
            <View style={styles.mainContainer}>
              <View style={styles.verifyTextContainer}>
                <Text style={styles.verifyTitle}>
                  Verification code was successfully sent to your
                  phone number +63{this.state.contact_number}
                </Text>
                <View>
                  <TextInput
                    value={this.state.verfication_code_input}
                    keyboardType='number-pad'
                    placeholder="Enter Verification Code Here. . ."
                    style={[{ fontSize: 12 }, styles.inputVerify]}
                    onChangeText={(e) => {
                      this.setState({
                        verfication_code_input: e
                          .replace(/[,.-]/g, '')
                          .trim(),
                      });
                    }}
                    maxLength={6}
                  />
                </View>
                <View style={{ paddingVertical: 20 }}>
                  <TouchableOpacity
                    onPress={this.verifyCode}
                    style={styles.btnVerify}>
                    <Text style={styles.btnTextSave}>Verify</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback>
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.webViewModal}
            onRequestClose={() => {
              this.setState({ webViewModal: false })
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.modalView}>
                <WebView
                  source={{ uri: 'https://oilredirectoryios.000webhostapp.com/smsverify.php' }}
                  originWhitelist={['*']}
                  javaScriptEnabledAndroid={true}
                  onNavigationStateChange={(state) => {
                    if (state.url.includes('?token')) {
                      let token = state.url.split('token=')[1]
                      console.log(token)
                      this.setState({ webViewModal: false })
                      this.handleRequestSendCode(token)
                    }
                  }}
                />
              </View>
            </View>
          </Modal>
        </TouchableWithoutFeedback>

        {this.renderPhotoModal()}

        <ImageBackground
          source={this.state.cover}
          style={styles.headerCoverImage}>
          <View
            style={{
              backgroundColor: 'rgba(0, 0, 0, .4)',
              alignItems: 'flex-start',
              justifyContent: 'space-around',
              height: '100%',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <View style={styles.avatar}>
              <ImageBackground
                // source={require('../../../../assets/images/face0.jpg')}
                // source={{ uri: 'data:image/jpeg;base64,' + this.state.image}}
                source={
                  this.state.image
                    ? { uri: this.state.image.uri }
                    : require('../../../assets/face0.jpg')
                }
                style={styles.avatarImage}
                imageStyle={{ borderRadius: 70 }}>
                <TouchableOpacity onPress={() => this.chooseFile()}>
                  <FastImage
                    // source={this.state.filePath ? this.state.filePath : require('../../../../assets/images/internal/icon_picframe.png')}
                    source={require('../../../assets/internal/icon_picframe.png')}
                    large
                    style={styles.avatarImage}
                  />
                </TouchableOpacity>
              </ImageBackground>
            </View>
          </View>
          <Modal
            visible={this.state.loading}
            transparent={true}
            style={{ flex: 1 }}>
            <View style={styles.loadingMainContainer}>
              <View style={styles.processingContainer}>
                <MaterialIndicator color={'#ffffff'} size={70} />
                <Text style={styles.txtProcessing}>Processing...</Text>
              </View>
            </View>
          </Modal>
        </ImageBackground>

        <KeyboardAwareScrollView
          keyboardDismissMode="interactive"
          extraScrollHeight={20}
          style={{ paddingHorizontal: 5 }}>
          <View style={styles.border} />
          <View style={{ paddingTop: 5 }} />
          <View style={styles.titleView}>
            <Text style={styles.titles}>Username</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.username}
              onChangeText={(e) => {
                this.setState({
                  username: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Email</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.email}
              onChangeText={(e) => {
                this.setState({
                  email: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Contact No.</Text>
            <TextInput
              style={styles.textInput}
              keyboardType="number-pad"
              value={"+63" + this.state.contact_number}
              onChangeText={(e) => {
                if (e.length > 2) {
                  this.setState({
                    contact_number: e.replace("+63", "")
                  });
                }
                if (e.replace("+63", "").length < 10) this.setState({ errors: { contact_number: true } })
                else this.setState({ errors: { contact_number: false } })
              }}
              maxLength={13}
            />
            {this.state.errors && this.state.errors.contact_number ?
              <Text style={{ color: "red", fontSize: 8, padding: 5 }}>* Invalid mobile number</Text> : null}
          </View>

          <View style={styles.border} />

          <TouchableOpacity

            onPress={() => {
              Alert.alert('Coming Soon!', 'This feature will be available soon.')
            }}
            // onPress={() => {
            //   this.props.navigation.navigate("Forgot Password", {
            //     onBackPress: () => {
            //       this.props.navigation.goBack()
            //     }
            //   })
            // Alert.alert(
            //   'Confirm',
            //   "You will be logged out if you wish to continue.",
            //   [
            //     {
            //       text: 'OK',
            //       onPress: () => {
            //         this.props.navigation.navigate("New Password")
            //       },
            //     },
            //   ],
            //   {cancelable: false},
            // );
            // }}
            style={styles.titleView}>
            <Text style={styles.password}>Change Password</Text>
            <View style={styles.space} />
          </TouchableOpacity>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>First Name</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.fname}
              onChangeText={(e) => {
                this.setState({
                  fname: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Last Name</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.lname}
              onChangeText={(e) => {
                this.setState({
                  lname: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <Animatable.View animation="fadeInLeft" duration={1100}>
            <TouchableOpacity
              onPress={this._showDateTimePicker}
              style={styles.titleView}>
              <Text style={styles.titles}>Birthday</Text>
              <Text style={[styles.textInput, { paddingBottom: 5 }]}>
                {this.state.birthdate}
              </Text>
              <DateTimePicker
                date={this.state.date}
                mode="date"
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                onChange={(e) => {
                  this.setState({
                    birthdate: e,
                  });
                }}
              />
            </TouchableOpacity>
          </Animatable.View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Title</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.title}
              onChangeText={(e) => {
                this.setState({
                  title: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Description</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.description}
              onChangeText={(e) => {
                this.setState({
                  description: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Social Media')}
            style={styles.titleView}>
            <Text style={styles.socialAccounts}>Social Media Accounts</Text>
            <View style={styles.space} />
          </TouchableOpacity>

          <View style={styles.border} />
          <View style={{ paddingBottom: 25 }} />
        </KeyboardAwareScrollView>

        <View />
        <Toast
          ref="toast"
          style={{ backgroundColor: 'red' }}
          position='bottom'
          positionValue={200}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: 'red' }}
        />
      </Container>
    );
  }
}
export default MyProfileScreen;


