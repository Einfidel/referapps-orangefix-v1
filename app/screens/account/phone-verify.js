import React, { useState, useEffect } from 'react'

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Alert,
  BackHandler,
} from 'react-native'

import {
  Container,
  Header,
  Thumbnail,
  Spinner,
} from 'native-base'

import Assets from '../../components/assets.manager';
import AsyncStorage from '@react-native-community/async-storage';
import Service from '../../components/api/service';

export default function PhoneVerification({ navigation, route }) {
  const [user, setUser] = useState('');
  const [code, setCode] = useState('')
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    init()
  }, [])

  const init = async () => {
    setLoading(true);

    let ud = await AsyncStorage.getItem("user_data")
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(_userdata)

    setLoading(false);
  }

  const handleVerifyClick = async (verified = 'no', reloads = 0) => {
    setLoading(true);
    let data = {
      user_id: user.data.id,
      verified: verified,
    }

    try {
      await Service.verifyPhoneVerifCode(
        data,
        (res) => {
          if (verified === 'no') {
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.verification_code);
              let verifData = {
                verifyCode: res.verification_code,
                medium: res.sent_to,
                expiration: res.verification_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0) => {
                  handleVerifyClick(verified, reloads);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          } else {
            if (res.status) {
              setLoading(false);
              console.log('Data: ', res);
              Alert.alert(
                'Success',
                'Your Contact Number has been verified!\n' +
                'Congratulations and enjoy your journey!',
                [
                  {
                    text: 'OK',
                    onPress: async () => {
                      if (route.params?.from == "Checkout") {
                        route.params.onAdd();
                      }
                      // navigation.goBack();
                      // navigation.goBack();
                      navigation.navigate('My Profile')
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              alert(res.msg);
            }
          }
        },
        (err) => console.log(err),
      );
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Container>
      <Header style={{ backgroundColor: '#ffffff' }}>
        <View style={{ position: 'absolute', left: 20, alignSelf: 'center' }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Thumbnail
              source={Assets.accountSettings.arrowDarkIcon}
              style={styles.backIcon}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.headerText}>Contact Number Verification</Text>
      </Header>
      {loading ? <Spinner />
        :
        <View style={{ backgroundColor: '#ffffff' }}>
          {user.data.is_verify_contact_number === 'yes' ?
            // {false ?
            <Text style={styles.sendVText}>
              {
                "Your Contact Number has already been verified!\n" +
                "Congratulations and enjoy your journey!"
              }
            </Text>
            :
            <>
              <Text style={styles.sendVText}>Verify your Contact Number now to access more features!</Text>
              <TouchableOpacity onPress={() => {
                if (!loading) {
                  handleVerifyClick()
                }
              }}>
                <Text style={styles.sendV}>Send Contact Number Verification</Text>
              </TouchableOpacity>
            </>
          }
        </View>
      }
    </Container>
  )
}

const styles = StyleSheet.create({
  headerText: {
    // fontFamily:'Roboto',
    fontWeight: 'bold',
    fontSize: 20,
    alignSelf: 'center'
  },
  backIcon: {
    width: 18,
    height: 18
  },
  sendV: {
    color: '#f36e23',
    // fontFamily:'Roboto',
    fontSize: 13,
    fontStyle: 'italic',
    alignSelf: 'center'
  },
  sendVText: {
    // fontFamily:'Roboto',
    color: 'gray',
    fontSize: 16,
    padding: 20,
    textAlign: 'center'
  }
})
