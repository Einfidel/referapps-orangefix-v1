import React from 'react';

import {
  Text,
  TouchableOpacity,
  View,
  Alert,
  ImageBackground,
  TextInput,
  Modal,
  Dimensions,
  Platform,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import { Header, Left, Body, Right, Container, Button } from 'native-base';
import auth from '@react-native-firebase/auth';

const { width, height } = Dimensions.get('window');

import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-community/async-storage';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTimePicker from 'react-native-modal-datetime-picker';
import * as Animatable from 'react-native-animatable';
import Moment from 'moment';
import ImagePicker from 'react-native-image-picker';
import Toast, { DURATION } from 'react-native-easy-toast';
import Assets from '../../components/assets.manager';
import Service from '../../components/api/service';
// import {sendMessage} from '../../components/Sms';
import stylings from '../../styles/signup/signup';
import { MaterialIndicator, UIActivityIndicator } from 'react-native-indicators';
import { cos } from 'react-native-reanimated';
import Toaster from 'react-native-simple-toast';
const profilepic = require('../../assets/internal/icon_picframe.png');

class MyProfileScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      lastRefresh: Date(Date.now()).toString(),
      fname: '',
      lname: '',
      username: '',
      gender: '',
      birthdate: '',
      contact_number: '',
      title: '',
      description: '',
      email: '',
      image: '',
      password: 'gbHxKK14',
      country_code: '',
      errors: [],
      additional_info_errors: [],

      filePath: {
        data: '',
        uri: '',
      },

      fileData: '',
      fileUri: '',

      reload: null,
      refreshing: false,

      verification_code: '',
      verfication_code_input: '',
      verification_type: '',

      isVerified: 'no',

      imageUpdated: false,
      userLoaded: null,
      update: true,
      user: [],
      dateOpen: false,
      date: new Date,
      mode: 'date',
      isDateTimePickerVisible: false,
      modalVisible: false,
      vmodalVisible: false,
      isVisible: false,
      loading: false,
    };
    this.refreshScreen = this.refreshScreen.bind(this);
  }

  refreshScreen = async () => {
    this.setState({ lastRefresh: Date(Date.now()).toString() });
    let THIS = this;
    await Service.userData(
      this.state.user.id,
      (res) => {
        let data = res.data;
        let info = res.data.info.data;
        THIS.setState(
          {
            fname: data.fname,
            lname: data.lname,
            username: data.username,
            gender: info.gender,
            birthdate: info.birthdate,
            title: info.title,
            description: info.description,
            contact_number: info.contact_number,
            email: info.email,
            image: info.avatar.path ? info.avatar.full_path : data.image,
            user: data,
            userLoaded: true,
            isVerified: info.is_verify,
          },
          () => {
            console.warn(res);
          },
        );
      },
      (err) => console.log(err),
    );
    // this.fetchUserData(this.state.user.id)
  };

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });
  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _handleDatePicked = (date) => {
    this._hideDateTimePicker();
    this.setState({
      birthdate: Moment(date).format('Y-MM-DD'),
    });
  };



  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      this.setState({
        image: response.uri,
        filePath: response.path,
        imageUpdated: true,
        fileData: response.data,
      });
    });
    console.warn('Image Fetched');
    console.warn(this.state.image);
  };

  componentWillUnmount = () => {
    this.unsubscribe();
    auth().currentUser !== null &&
      auth().currentUser.phoneNumber !== null &&
      auth()
        .currentUser.delete()
        .then(function () {
          console.log('Firebase user with phone number deleted!');
          auth().signOut();
        })
        .catch(function (error) {
          console.warn('Error', error.message);
        }) &&
      auth().signOut();
    auth().signInAnonymously();
  };

  UNSAFE_componentWillMount = async () => {
    this.unsubscribe = auth().onAuthStateChanged(function (user) {
      // user.phoneNumber !== '' && auth().signOut();
      // user.phoneNumber !== null && auth().signOut();
      // user.providerData.phoneNumber !== null && auth().signInAnonymously();
      // user.uid || user && auth().signInAnonymously();
      console.log('User', JSON.stringify(user, null, '\t'));
    });
    let ud = await AsyncStorage.getItem('user_data');
    console.log("USER DATA", ud);
    let userData = ud ? JSON.parse(ud) : null;
    if (userData) {
      let THIS = this;
      console.log("USER DATA", JSON.stringify(userData, null, '\t'));
      await Service.userData(
        userData.data.id,
        (res) => {
          let data = res.data;
          let info = res.data.info.data;
          // console.log("USER DATA", JSON.stringify(res, null, '\t'));
          THIS.setState({
            fname: data.fname,
            lname: data.lname,
            username: data.username,
            gender: info.gender,
            birthdate: info.birthdate,
            title: info.title,
            description: info.description,
            contact_number: info.contact_number,
            email: info.email,
            image: info.avatar.path ? info.avatar.full_path : data.image,
            user: data,
            userLoaded: true,
            isVerified: info.is_verify,
            country_code: userData.data.country_code,
          });
        },
        (err) => console.warn('Error', err.message),
      );
    }
  };

  signInWithPhoneNumber = async () => {
    let { contact_number, country_code } = this.state;
    let ccode = country_code === 'PH' ? '+63' : '';
    const confirmation = await auth().signInWithPhoneNumber(
      ccode + contact_number,
    );
    console.warn('Confirm', JSON.stringify(confirmation, null, '\t'));
    this.setState({ verification_code: confirmation });
  };

  handleSendMessage = () => {
    // let {verification_code, contact_number} = this.state;
    // sendMessage(
    //   (error, res) => {
    //     if (error) {
    //       Alert.alert(
    //         'Error',
    //         'Unable to send verification code to phone number!',
    //       );
    //       console.warn('Error', error);
    //     } else {
    //       console.log(
    //         `Messaging response for messaging phone number: ${this.state.contact_number}` +
    //           ` => code: ${res['status']['code']}` +
    //           `, description: ${res['status']['description']}`,
    //       );
    //     }
    //   },
    //   verification_code,
    //   contact_number,
    // );
  };


  handleUpdateProfile = async () => {
    await this.setState({
      loading: true,
    });
    await Service.editProfile(
      this.state,
      (res) => {
        if (res.status === true) {
          let { contact_number, email } = this.state;
          console.log('Response', res.verification_code, this.state.verification_code);
          this.setState(
            {
              loading: false,
              verification_code: res.verification_code,
              vmodalVisible: true,
              modalVisible: false,
            },
            () => contact_number !== '' && this.handleSendMessage(),
          );
        } else {
          console.log(res, this.state.verification_code);
          this.setState({
            loading: false,
          });
          Alert.alert(
            'Update Failed!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: () => {
                  this.setState({ update: true });
                  this.setState({ errors: res.errors });
                },
              },
            ],
            { cancelable: false },
          );
        }
      },
      (err) => {
        this.setState({
          loading: false,
          modalVisible: false,
        });
        Alert.alert('Error', 'Please try again.');
        console.warn('Error', err.message);
      },
    );
  };

  verifiedUpdate = async () => {
    let THIS = this;
    await Service.verifiedUpdateProfile(
      this.state,
      (res) => {
        if (res.status === true) {
          THIS.setState({ vmodalVisible: false, modalVisible: false });
          Alert.alert(
            'Successfully updated user information!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: async () => {
                  await THIS.updateLocalStorage();
                  await THIS.refreshScreen();
                },
              },
            ],
            { cancelable: false },
          );
        } else {
          Alert.alert(
            'Update Failed!',
            res.msg,
            [
              {
                text: 'OK',
                onPress: () => {
                  THIS.setState({ update: true });
                  THIS.setState({ errors: res.errors });
                },
              },
            ],
            { cancelable: false },
          );
        }
      },
      (err) => console.log(err),
    );
  };

  // confirmCodeFromFirebase = async () => {
  //   try {
  //     let {verfication_code_input, verification_code} = this.state;
  //     if (verification_code.verificationId == null) {
  //       throw Error(
  //         "There's an issue verifying your code. Please try again later.",
  //       );
  //     } else {
  //       await verification_code.confirm(verfication_code_input);
  //       this.verifiedUpdate();
  //     }
  //   } catch (error) {
  //     this.retryVerification(error.message);
  //     console.warn('Error', error.message);
  //   }
  // };

  retryVerification = (message = null) => {
    Alert.alert(
      'Error!',
      message !== null
        ? message
        : 'The verification code you entered does not match',
      [
        {
          text: 'CANCEL',
          style: 'cancel',
        },
        {
          text: 'RESEND',
          onPress: () => {
            this.handleUpdateProfile();
          },
        },
      ],
      { cancelable: false },
    );
  };

  verifyCode = () => {
    let { verification_code, verfication_code_input } = this.state;
    if (verification_code === verfication_code_input) {
      this.verifiedUpdate();
    } else {
      this.retryVerification();
    }
  };

  // _updateAdditionalInfo = async () => {
  //   try {
  //     let formData = new FormData();

  //     formData.append('title', this.state.title);
  //     formData.append('description', this.state.description);
  //     formData.append('birthdate', this.state.birthdate);
  //     formData.append('user_id', this.state.id);
  //     formData.append('api_token', API_TOKEN);

  //     const update = await fetch(API_UPDATE_ADDITIONAL_INFO, {
  //       method: 'POST',
  //       headers: {
  //         'Content-Type': 'multipart/form-data',
  //       },
  //       body: formData,
  //     });

  //     const updated = update.json();

  //     if (updated.status === false) {
  //       console.log(formData);
  //       console.log(updated.errors);
  //       Alert.alert(
  //         'Update Failed!',
  //         updated.msg,
  //         [
  //           {
  //             text: 'OK',
  //             onPress: () => {
  //               this.setState({update: true});
  //               this.setState({additional_info_errors: updated.errors});
  //             },
  //           },
  //         ],
  //         {cancelable: false},
  //       );
  //     }
  //   } catch (e) {
  //     console.warn(e);
  //     console.warn('additional');
  //   }
  // };

  updateLocalStorage = () => {
    AsyncStorage.getItem('user_data').then((value) => {
      let data = JSON.parse(value).userData.data;
      let newdata = {
        country_code: 'PH',
        "downline_commission": 0,
        "fb_id": null,
        "id": data.id,
        "image": this.state.image != '' ? this.state.image : data.image,
        "is_security_question_set": data.is_security_question_set,
        "is_verify": data.is_verify,
        "fname": this.state.fname != '' ? this.state.fname : data.fname,
        "lname": this.state.lname != '' ? this.state.lname : data.lname,
        "notifications": data.notifications,
        "product_sales": data.product_sales,
        "products": data.products,
        "ratings": data.ratings,
        "refer_id": data.refer_id,
        "reviews": data.reviews,
        "sales_commission": data.sales_commission,
        "services": data.services,
        "shop_name": data.shop_name,
        "total_cash": data.total_cash,
        username:
          this.state.username != '' ? this.state.username : data.username,
      };
      AsyncStorage.setItem('user_data', JSON.stringify({ data: newdata }));
    });
  };

  handleCheckInput = () => {
    if (this.state.fname == '' || !this.state.fname) {
      Toaster.showWithGravity('First Name field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else if (this.state.lname == '' || !this.state.lname) {
      Toaster.showWithGravity('Last Name field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else if (this.state.username == '' || !this.state.username) {
      Toaster.showWithGravity('Username field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else if (this.state.email == '' || !this.state.email) {
      Toaster.showWithGravity('Email field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else if (this.state.contact_number == '' || !this.state.contact_number) {
      Toaster.showWithGravity('Contact number field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else if (this.state.birthdate == '' || !this.state.birthdate) {
      Toaster.showWithGravity('Birthday field is required', Toaster.SHORT, Toaster.BOTTOM);
      return;
    }
    else {
      this.setState({ modalVisible: !this.state.modalVisible })
    }
  }

  render() {
    return (
      <Container style={{ backgroundColor: '#ffffff' }}>
        <Header transparent style={styles.headerMyProfile}>
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <FastImage
                source={Assets.accountSettings.arrowDarkIcon}
                square
                small
                style={styles.backIcon}
              />
            </Button>
          </Left>
          <Body style={{ flex: 0, justifyContent: 'center' }}>
            <Text style={styles.headText}>My Profile</Text>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => {
                this.handleCheckInput();
              }}>
              <FastImage
                source={require('../../assets/headericon/continue.png')}
                square
                small
                style={styles.submitIcon}
              />
              {/* {
                modalStatus === true ? <SaveModal /> : null 
              } */}
            </Button>
            <TouchableWithoutFeedback
              onPress={() => this.setState({ modalVisible: false })}>
              <Modal
                style={{ flex: 1 }}
                animationType='fade'
                transparent={true}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  this.setState({ modalVisible: false });
                }}>
                <View style={styles.mainContainer}>
                  <View style={styles.textContainer}>
                    <Text style={styles.title}>
                      Enter your password to apply changes
                    </Text>
                    <View>
                      <TextInput
                        secureTextEntry={true}
                        placeholder="Enter here. . ."
                        style={styles.inputPassword}
                        onChangeText={(e) => {
                          this.setState({
                            password: e,
                          });
                        }}
                      />
                    </View>
                    <View style={{ paddingVertical: 20 }}>
                      <TouchableOpacity
                        onPress={() => {
                          this.handleUpdateProfile();
                        }}
                        style={styles.btnSave}>
                        <Text style={styles.btnTextSave}>Save Changes</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => this.setState({ vmodalVisible: false })}>
              <Modal
                style={{ flex: 1 }}
                animationType='fade'
                transparent={true}
                visible={this.state.vmodalVisible}
                onRequestClose={() => {
                  this.setState({ vmodalVisible: false });
                }}>
                <View style={styles.mainContainer}>
                  <View style={styles.verifyTextContainer}>
                    <Text style={styles.verifyTitle}>
                      Verification code was successfully sent to your email /
                      phone number.
                    </Text>
                    <View>
                      <TextInput
                        value={this.state.verfication_code_input}
                        keyboardType='number-pad'
                        placeholder="Enter Verification Code Here. . ."
                        style={[{ fontSize: 12 }, styles.inputVerify]}
                        onChangeText={(e) => {
                          this.setState({
                            verfication_code_input: e
                              .replace(/[,.-]/g, '')
                              .trim(),
                          });
                        }}
                      />
                    </View>
                    <View style={{ paddingVertical: 20 }}>
                      <TouchableOpacity
                        onPress={this.verifyCode}
                        style={styles.btnVerify}>
                        <Text style={styles.btnTextSave}>Verify</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </Modal>
            </TouchableWithoutFeedback>
          </Right>
        </Header>
        <ImageBackground
          source={require('../../assets/internal/default-cover.jpg')}
          style={styles.headerCoverImage}>
          <View
            style={{
              backgroundColor: 'rgba(0, 0, 0, .4)',
              alignItems: 'flex-start',
              justifyContent: 'space-around',
              height: '100%',
              paddingTop: 5,
              paddingBottom: 5,
            }}>
            <View style={styles.avatar}>
              <ImageBackground
                // source={require('../../../../assets/images/face0.jpg')}
                // source={{ uri: 'data:image/jpeg;base64,' + this.state.image}}
                source={
                  this.state.image
                    ? { uri: this.state.image }
                    : require('../../assets/face0.jpg')
                }
                style={styles.avatarImage}
                imageStyle={{ borderRadius: 70 }}>
                <TouchableOpacity onPress={() => this.chooseFile()}>
                  <FastImage
                    // source={this.state.filePath ? this.state.filePath : require('../../../../assets/images/internal/icon_picframe.png')}
                    source={require('../../assets/internal/icon_picframe.png')}
                    large
                    style={styles.avatarImage}
                  />
                </TouchableOpacity>
              </ImageBackground>
            </View>
          </View>
          <Modal
            visible={this.state.loading}
            transparent={true}
            style={{ flex: 1 }}>
            <View style={styles.loadingMainContainer}>
              <View style={styles.processingContainer}>
                <MaterialIndicator color={'#ffffff'} size={70} />
                <Text style={styles.txtProcessing}>Processing...</Text>
              </View>
            </View>
          </Modal>
        </ImageBackground>
        <KeyboardAwareScrollView
          keyboardDismissMode="interactive"
          extraScrollHeight={20}
          style={{ paddingHorizontal: 5 }}>
          <View style={styles.border} />
          <View style={{ paddingTop: 5 }} />
          <View style={styles.titleView}>
            <Text style={styles.titles}>Username</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.username}
              onChangeText={(e) => {
                this.setState({
                  username: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Email</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.email}
              onChangeText={(e) => {
                this.setState({
                  email: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Contact No.</Text>
            <TextInput
              style={styles.textInput}
              keyboardType="number-pad"
              value={this.state.contact_number}
              onChangeText={(e) => {
                this.setState({
                  contact_number: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => {
              Alert.alert('Coming Soon!', 'This feature will be available soon.')
            }}
            style={styles.titleView}>
            <Text style={styles.password}>Change Password</Text>
            <View style={styles.space} />
          </TouchableOpacity>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>First Name</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.fname}
              onChangeText={(e) => {
                this.setState({
                  fname: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Last Name</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.lname}
              onChangeText={(e) => {
                this.setState({
                  lname: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <Animatable.View animation="fadeInLeft" duration={1100}>
            <TouchableOpacity
              onPress={this._showDateTimePicker}
              style={styles.titleView}>
              <Text style={styles.titles}>Birthday</Text>
              <Text style={[styles.textInput, { paddingBottom: 5 }]}>
                {this.state.birthdate}
              </Text>
              <DateTimePicker
                date={this.state.date}
                mode="date"
                isVisible={this.state.isDateTimePickerVisible}
                onConfirm={this._handleDatePicked}
                onCancel={this._hideDateTimePicker}
                onChange={(e) => {
                  this.setState({
                    birthdate: e,
                  });
                }}
              />
            </TouchableOpacity>
          </Animatable.View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Title</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.title}
              onChangeText={(e) => {
                this.setState({
                  title: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <View style={styles.titleView}>
            <Text style={styles.titles}>Description</Text>
            <TextInput
              style={styles.textInput}
              value={this.state.description}
              onChangeText={(e) => {
                this.setState({
                  description: e,
                });
              }}
            />
          </View>

          <View style={styles.border} />

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Social Media')}
            style={styles.titleView}>
            <Text style={styles.socialAccounts}>Social Media Accounts</Text>
            <View style={styles.space} />
          </TouchableOpacity>

          <View style={styles.border} />
          <View style={{ paddingBottom: 25 }} />
        </KeyboardAwareScrollView>

        <View />
        <Toast
          ref="toast"
          style={{ backgroundColor: 'red' }}
          position='bottom'
          positionValue={200}
          fadeInDuration={750}
          fadeOutDuration={1000}
          opacity={0.8}
          textStyle={{ color: 'red' }}
        />
      </Container>
    );
  }
}
export default MyProfileScreen;

const styles = {
  txtProcessing: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
  },
  loadingMainContainer: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  processingContainer: {
    backgroundColor: '#231f20',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: 160,
    height: 160,
    borderRadius: 30,
  },
  backIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  submitIcon: {
    width: 16,
    height: 16,
    resizeMode: 'contain',
    // marginRight: 10,
  },
  changePassword: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialmedia: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#5394fc',
  },
  headText: {
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    color: '#231f20',
    fontWeight: 'bold',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 11,
    color: '#7f8c8d',
  },
  errorText: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    color: 'red',
    paddingLeft: 20,
    paddingBottom: 2,
  },
  inputText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    flex: 0,
  },
  space: {
    paddingTop: 15,
  },
  border: {
    height: 1,
    backgroundColor: '#ecf0f1',
  },
  headerContainer: {
    height: 190,
    width,
  },
  headerCoverImage: {
    width,
    height: 230,
    justifyContent: 'center',
  },
  titleView: {
    flex: 0,
    marginLeft: 15,
    marginRight: 15,
    paddingTop: 15,
    paddingBottom: 0,
  },
  titles: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  textInput: {
    // fontFamily: 'robot-light',
    fontSize: 14,
    color: '#231f20',
    paddingVertical: 0,
    paddingHorizontal: 0,
  },
  textInputDanger: {
    fontSize: 14,
    color: '#231f20',
    paddingVertical: 0,
    paddingHorizontal: 0,
    borderBottomColor: '##d9534f'
  },
  password: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#f36e23',
  },
  socialAccounts: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#45bcef',
  },
  avatar: {
    justifyContent: 'space-around',
    alignSelf: 'flex-start',
    paddingLeft: 20,
  },
  avatarImage: {
    alignSelf: 'center',
    height: 110,
    width: 110,
    borderRadius: 110 / 2,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  textContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    height: 200,
  },
  verifyTextContainer: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    width: '98%',
    paddingVertical: 20,
  },
  verifyTitle: {
    color: 'black',
    paddingTop: 25,
    paddingHorizontal: '9%',
    // fontFamily:'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingVertical: 10,
  },
  title: {
    color: '#f36e23',
    // paddingVertical:40,
    paddingTop: 40,
    paddingHorizontal: 15,
    // fontFamily:'Roboto',
    fontSize: 10,
  },
  inputPassword: {
    borderBottomColor: '#e1e2e3',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    width: '90%',
    alignSelf: 'center',
    // fontFamily:'Roboto',
  },
  inputVerify: {
    borderColor: '#e1e2e3',
    borderWidth: 1,
    width: '80%',
    alignSelf: 'center',
    // fontFamily:'Roboto',
    // paddingBottom:20
  },
  btnSave: {
    width: '80%',
    // height:'40%',
    backgroundColor: '#ababab',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  btnVerify: {
    width: '80%',
    backgroundColor: '#00a14b',
    borderRadius: 15,
    alignSelf: 'center',
    zIndex: 10,
    justifyContent: 'center',
    paddingVertical: 3,
  },
  headerMyProfile: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15,
      },
      android: {},
    }),
  },
  btnTextSave: {
    color: '#FFFFFF',
    textAlign: 'center',
    fontSize: 15,
    // fontFamily:'Roboto'
  },
};
