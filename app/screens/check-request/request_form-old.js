import React, {
  useState,
  useEffect,
} from 'react';

import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Dimensions,
  Alert,
  Modal,
  FlatList,
  StyleSheet,
  ImageBackground,
  TouchableWithoutFeedback,
} from 'react-native';

import {
  Root,
  Container,
  Header,
  Thumbnail,
  Left,
  CheckBox,
  Body,
  ListItem,
  Right,
  Item,
  Input,
  Toast,
  Spinner,
} from 'native-base';

import Switch from 'react-native-switch-pro'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { MaterialIndicator } from 'react-native-indicators';
import { Picker } from '@react-native-picker/picker';
import * as Animatable from 'react-native-animatable';
import LinearGradient from 'react-native-linear-gradient';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
// import { connect } from 'react-redux';
import { sell } from '../product/styles';
import Service from '../../components/api/service';
import appleAuth from '@invertase/react-native-apple-authentication';
import Assets from '../../components/assets.manager';
import Icon from '../../components/icons';

const { width, height } = Dimensions.get('window');

function CustomEditButton(props) {
  let { onPress } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.goBackHomeButton,
        { width: null, height: null, paddingHorizontal: 5 },
      ]}>
      <Text style={{ color: '#00a14b', fontSize: 12 }}>Change</Text>
    </TouchableOpacity>
  );
}

function Address(props) {
  let { data, type, onPressChange, navigation, noAddress, initAddresses, setAddressModalVisibility } = props;
  let addressType = type === 'Ship' ? 'Add shipping address' : 'Add billing address';
  // console.log("noAddress", noAddress);

  return (

    <View>
      {data === null ? (
        <TouchableOpacity
          onPress={() =>
            noAddress ? navigation.navigate('Add Address', {
              onAdd: () => {
                // console.log("calling initAddresses")
                initAddresses();
                setAddressModalVisibility(true);
              },
              from: "Checkout"
            }) : onPressChange(type)
          }
          style={styles.addAddress}>
          <Text style={[styles.grayText, { paddingHorizontal: 20 }]}>
            + {addressType}
          </Text>
        </TouchableOpacity>
      ) : (
        <View style={{ padding: 10, margin: 10, borderWidth: 2, borderColor: '#bababa' }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={styles.addressGrayText}>{type} to</Text>
            <CustomEditButton onPress={() => onPressChange(type)} />
          </View>
          <Text style={styles.addressNameText}>{data.info.data.contact_person}</Text>
          {/* <Text style={[styles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_person}
          </Text> */}
          <Text style={[styles.grayText, { fontSize: 13 }]}>
            {data.info.data.street_address}
          </Text>
          <Text style={[styles.grayText, { fontSize: 13 }]}>
            {data.info.data.province_code} - {data.info.data.city_code} - {data.info.data.barangay_code}
          </Text>
          <Text style={[styles.grayText, { fontSize: 13 }]}>
            {data.info.data.country_code == "PH" ? "Philippines" : "USA"}
          </Text>
          <Text style={[styles.grayText, { fontSize: 13 }]}>
            {data.info.data.contact_number}
          </Text>
        </View>
      )}
    </View>
  );
}

export default function CheckRequestForm({ navigation, route }) {
  const [submitLoading, setSubmitLoading] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);
  const [modalVisible2, setModalVisible2] = useState(false);
  const [isAddressModalVisible, setAddressModalVisibility] = useState(false);
  const [selectedShippingAddress, setSelectedShippingAddress] = useState(null);

  const [tinssnPhotoModal, setTINSSNPhotoModal] = useState(false);
  const [supportingPhotoModal, setSupportingPhotoModal] = useState(false);

  const [user, setUser] = useState(null);
  const [address, setAddress] = useState('');
  const [tin_ssn, setTin_ssn] = useState('');
  const [amount, setAmount] = useState(0);
  const [modeOfTransaction, setModeOfTransaction] = useState('');
  // const [tinssnImages, setTINSSNImages] = useState([]);
  // const [supportingIDimages, setSupportingIDimages] = useState([]);
  const [tinssnImage, setTINSSNImage] = useState(null);
  const [supportingIDimage, setSupportingIDimage] = useState(null);
  const [imageSelected, setImageSelected] = useState(null);
  const [addresses, setAddresses] = useState([]);
  const [addressToEdit, setAddressToEdit] = useState('');

  const [attributeTmp, setAttributeTmp] = useState("");

  const [submitTapped, setSubmitTapped] = useState(false);

  const [iosMargin, setiosMargin] = useState(0);

  const [currency, setCurrency] = useState(null);

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    init();
  }, [])

  const init = async () => {
    try {
      appleAuth.isSupported ? setiosMargin(10) : setiosMargin(10);

      // Set User
      const _user = await AsyncStorage.getItem('user_data');
      let user = _user ? JSON.parse(_user) : null;
      if (user) setUser(user);

      // Set Currency
      if (user?.data?.country_code === "PH") {
        setCurrency("₱");
      } else if (user?.data?.country_code === "US") {
        setCurrency("$");
      } else {
        setCurrency("");
      }

      initAddresses();

      setLoading(false)
    } catch (error) {
      setLoading(false)
      console.warn(error);
    }
  }

  const initAddresses = async () => {
    let ud = await AsyncStorage.getItem('user_data');
    let _userdata = ud ? JSON.parse(ud) : null;
    await Service.getSpecificAddress(_userdata.data.id,
      (res) => {
        setAddresses(res.data)
        // console.log('address data', res.data)
      },
      (err) => {
        console.log(err)
      }
    );
  }

  const renderAddressModal = () => {
    return (
      <Modal
        style={{ flex: 1 }}
        animationType="fade"
        transparent={true}
        visible={isAddressModalVisible}
        onRequestClose={() => setAddressModalVisibility(false)}>
        <TouchableOpacity
          onPress={() => setAddressModalVisibility(false)}
          style={styles.modalBackgroundContainer}>
          <TouchableWithoutFeedback>
            <View
              style={{
                backgroundColor: 'white',
                borderRadius: 5,
                width: width - 40,
                padding: 10,
                marginBottom: 80,
                marginTop: 80,
              }}>
              <Text style={[styles.grayText, { textAlign: 'center', fontSize: 18 }]}>
                Select Address
              </Text>
              {/* <View style={{ paddingBottom: 8, borderBottomWidth: 0.5, borderTopColor: '#e1e2e3' }} /> */}
              <FlatList
                data={addresses}
                renderItem={renderAddress}
                extraData={addresses}
                keyExtractor={(item, index) => index.toString()}
              />
              <TouchableOpacity
                onPress={() => {
                  // console.log("addresses", addresses);
                  navigation.navigate('Add Address', {
                    onAdd: () => {
                      // console.log("calling initAddresses")
                      initAddresses();
                      setAddressModalVisibility(true);
                    },
                    from: "Checkout"
                  });
                  // navigation.navigate('Add Address',
                  //   {
                  //     route: "checkout"
                  //   }
                  // );
                  setAddressModalVisibility(false);
                }}
                style={{ padding: 10, margin: 10, marginBottom: 0, borderWidth: 2, borderColor: '#dadada' }}>
                <Text style={[styles.grayText, { paddingHorizontal: 20 }]}>
                  + Add Address
                </Text>
              </TouchableOpacity>
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    );
  };

  const renderAddress = ({ item, index }) => {
    let data = item;
    return (
      <TouchableOpacity
        style={{
          padding: 10,
          margin: 10,
          marginBottom: 0,
          borderWidth: 2,
          borderColor: '#dadada',
        }}
        onPress={() => {
          handleAddress(data);
        }}>
        <Body style={{
          alignItems: 'flex-start',
        }}>
          <Text style={styles.addressNameText}>{data.info.data.contact_person}</Text>
          {/* <Text style={[styles.addressGrayText, { fontSize: 13 }]}>
            {data.info.data.contact_person}
          </Text> */}
          <Text style={[styles.addressGrayText, { fontSize: 13 }]}>
            {data.info.data.street_address}
          </Text>
          <Text style={[styles.addressGrayText, { fontSize: 13 }]}>
            {data.info.data.province_code} - {data.info.data.city_code} - {data.info.data.barangay_code}
          </Text>
          <Text style={[styles.addressGrayText, { fontSize: 13 }]}>
            {data.info.data.country_code == "PH" ? "Philippines" : "USA"}
          </Text>
          <Text style={[styles.addressGrayText, { fontSize: 13 }]}>
            {data.info.data.contact_number}
          </Text>

          <TouchableOpacity
            onPress={() => {
              setAddressModalVisibility(false);
              navigation.navigate('Edit Address', {
                onEdit: () => {
                  // console.log("calling initAddresses")
                  initAddresses();
                  setAddressModalVisibility(true);
                },
                from: "Checkout",
                value: data
              })
            }
            }
            style={[
              styles.goBackHomeButton,
              { width: null, height: null, paddingHorizontal: 5 },
            ]}>
            <Text style={{ color: '#00a14b', fontSize: 12 }}>Edit</Text>
          </TouchableOpacity>
        </Body>
      </TouchableOpacity >
    );
  };

  const handleAddress = (item) => {
    setSelectedShippingAddress(item);
    setAddressModalVisibility(false);
    // initAddresses();
  };

  const handleImagePicker = (imageArray, imageType) => {
    if (imageArray != null && imageArray != '') {
      console.log("imageArray", imageArray)
      console.log("imageArray.uri", imageArray.uri)
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          if (imageType === 'tinssn') {
            setTINSSNPhotoModal(true);
          } else if (imageType === 'supporting') {
            setSupportingPhotoModal(true);
          }
          setImageSelected(imageArray);
        }}>
          <FastImage
            source={{ uri: imageArray.uri }}
            style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
            resizeMode={FastImage.resizeMode.contain}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto(imageArray, imageType);
        }} >
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
          />
        </TouchableOpacity>
      );
    }

    // if (imageArray.length > 0) {
    //   return imageArray.map((img, imgIndex) => {
    //     return (
    //       <TouchableOpacity activeOpacity={1} onPress={() => {
    //         if (imageType === 'tinssn') {
    //           setTINSSNPhotoModal(true);
    //         } else if (imageType === 'supporting') {
    //           setSupportingPhotoModal(true);
    //         }
    //         setImageSelected(imgIndex);
    //       }}>
    //         <FastImage
    //           key={imgIndex}
    //           source={{ uri: img.uri }}
    //           style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
    //           resizeMode={FastImage.resizeMode.contain}
    //         />
    //       </TouchableOpacity>
    //     );
    //   });
    // } else {
    //   return (
    //     <TouchableOpacity activeOpacity={1} onPress={() => {
    //       handleAddPhoto(imageArray, imageType);
    //     }} >
    //       <FastImage
    //         source={require('../../assets/Sell_Product-09.png')}
    //         large
    //         style={{ height: 161, width: 161, resizeMode: 'contain', justifyContent: 'center' }}
    //       />
    //     </TouchableOpacity>
    //   );
    // }
  }

  const handleUIAddPhoto = (imageArray, imageType) => {
    if (imageArray.length > 0 && imageArray.length < 11) {
      return (
        <TouchableOpacity activeOpacity={1} onPress={() => {
          handleAddPhoto(imageArray, imageType)
        }}>
          <FastImage
            source={require('../../assets/Sell_Product-09.png')}
            large
            style={sell.newProductImage}
          />
        </TouchableOpacity>
      );
    }
  }

  const handleAddPhoto = (imageArray, imageType) => {
    const options = {
      title: 'Select Image',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        if (response.fileSize > 5000000) {
          Alert.alert(
            'Alert',
            'Image must be less than 5Mb'
          )
        } else {
          if (imageType === 'tinssn') {
            setTINSSNImage(response)
            // setTINSSNImages([...imageArray, response]);
          } else if (imageType === 'supporting') {
            setSupportingIDimage(response)
            // setSupportingIDimages([...imageArray, response]);
          }
        }
      }
    });
  };

  const handleChangePhoto = (index, imageType) => {
    const options = {
      title: 'Select Image',
      // customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    // console.log('ImagePicker = ', ImagePicker);
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        response.data = null;
        response.fullpath = 'content://com.refer.apps.provider/root' + response.path;
        if (response.uri.includes('referappsv2')) {
          //TAKEN THE PICTURE WITH CAMERA
          response.isCaptured = true;
        } else {
          response.isCaptured = false;
        }

        if (response.fileSize > 5000000) {
          Alert.alert(
            'Alert',
            'Image must be less than 5Mb'
          )
        } else {
          if (imageType === 'tinssn') {
            setTINSSNImage(response)
            // let tmpImageArray = [...tinssnImages];
            // tmpImageArray.splice(index, 1, response);
            // setTINSSNImages(tmpImageArray);
          } else if (imageType === 'supporting') {
            setSupportingIDimage(response)
            // let tmpImageArray = [...supportingIDimages];
            // tmpImageArray.splice(index, 1, response);
            // setSupportingIDimages(tmpImageArray);
          }
        }
      }
    });
  };

  const handleRemovePhoto = (index, imageType) => {
    if (imageType === 'tinssn') {
      setTINSSNImage(null)
      // let tmpImageArray = [...tinssnImages];
      // tmpImageArray.splice(index, 1);
      // setTINSSNImages(tmpImageArray);
    } else if (imageType === 'supporting') {
      setSupportingIDimage(null)
      // let tmpImageArray = [...supportingIDimages];
      // tmpImageArray.splice(index, 1);
      // setSupportingIDimages(tmpImageArray);
    }
  };

  const renderTINSSNPhotoModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={tinssnPhotoModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setTINSSNPhotoModal(!tinssnPhotoModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    handleChangePhoto(imageSelected, 'tinssn');
                    setTINSSNPhotoModal(!tinssnPhotoModal);
                  }}
                >
                  Change Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected, 'tinssn');
                    setTINSSNPhotoModal(!tinssnPhotoModal);
                  }}
                >
                  Remove Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    setTINSSNPhotoModal(!tinssnPhotoModal);
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const renderSupportingPhotoModal = (data) => {
    return (
      <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'red' }}>
        <Modal
          // animationType='slide'
          transparent={true}
          visible={supportingPhotoModal}
          style={{ backgroundColor: 'red' }}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              setSupportingPhotoModal(!supportingPhotoModal);
            }}
            style={{ backgroundColor: 'red' }}
          >
            <View style={{ ...styles.containerModal }}>
              <View style={sell.PhotoModalContainer}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>Select Action</Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 20 }}
                  onPress={() => {
                    handleChangePhoto(imageSelected, 'supporting');
                    setSupportingPhotoModal(!supportingPhotoModal);
                  }}
                >
                  Change Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingBottom: 10, paddingTop: 10 }}
                  onPress={() => {
                    handleRemovePhoto(imageSelected, 'supporting');
                    setSupportingPhotoModal(!supportingPhotoModal);
                  }}
                >
                  Remove Image...
                </Text>

                <Text
                  style={{ fontSize: 18, paddingTop: 10, fontWeight: '900', alignSelf: 'flex-end' }}
                  onPress={() => {
                    setSupportingPhotoModal(!supportingPhotoModal);
                  }}
                >
                  Cancel
                </Text>


              </View>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  };

  const handleSubmit = async () => {
    let submitCancelled = false;

    setSubmitTapped(true);

    // If Image Empty
    if (tinssnImages.length <= 0) {
      submitCancelled = true;
    }

    // If Image more than 5mb
    tinssnImages.map((image, imageIndex) => {
      if (image.fileSize > 5000000) {
        submitCancelled = true;
      }
    });

    // If Name Empty
    if (!name.trim()) {
      submitCancelled = true;
    }

    // If Description Empty
    if (!description.trim()) {
      submitCancelled = true;
    }


    // *************************************
    // If any errors found, stop and return
    if (submitCancelled) {
      return;
    }

    createProductData.userDataId = user.data.id;
    createProductData.name = name;
    createProductData.category_id = category_id;
    createProductData.description = description;
    createProductData.tinssnImages = tinssnImages;

    // if no errors then go
    setSubmitLoading(true);

    // console.log("tmp check");

    try {
      await Service.createProduct(
        createProductData,
        (res) => {
          if (res.errors) {
            console.warn('Submit Error', JSON.stringify(res.errors, null, '\t'));
            setSubmitLoading(false);
            Alert.alert('Upload Error', 'Please try again');
          } else {
            setResponseData(res);
            // setLoadingModal(false);

            Alert.alert(
              'Succesfully!',
              res.msg,
              [
                {
                  text: 'OK',
                  onPress: async () => {
                    // await handleShowModal();
                    navigation.goBack()
                  },
                },
              ],
              { cancelable: false }
            );
          }
        },
        (err) => {
          console.warn('Error', err.message)
          setSubmitLoading(false);
        },
      );
    } catch (error) {
      Alert.alert('Upload Error', 'Please try again');
      setSubmitLoading(false);
    }
  };

  const renderImageError = () => {
    if (submitTapped) {
      if (tinssnImages.length <= 0) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Atleast 1 Image is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderNameError = () => {
    if (submitTapped) {
      if (!name.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Name is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  const renderDescriptionError = () => {
    if (submitTapped) {
      if (!description.trim()) {
        return (
          <View style={sell.fieldErrors}>
            <Text
              style={{
                // fontFamily: 'Roboto',
                fontSize: 14,
                color: '#ff1a1a',
              }}
            >
              * Description is required.
            </Text>
          </View>
        );
      }
    }
    return <View />;
  };

  return (
    <Container style={{ backgroundColor: '#FFF' }}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}>
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Thumbnail
              source={Assets.accountSettings.iconBack}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
          <Text style={{
            // fontFamily: 'Roboto',
            fontSize: 20,
            color: '#231f20',
          }}>Create Check Request</Text>
        </Body>
      </Header>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      >
        {loading ? <Spinner /> : null}
        <View style={styles.formContainer}>

          {renderAddressModal()}
          <Text style={styles.inputTitle}>Address</Text>
          <Address
            noAddress={addresses.length === 0}
            navigation={navigation}
            onPressChange={(selectedAddress) => {
              setAddressModalVisibility(true);
              setAddressToEdit(selectedAddress);
            }}
            initAddresses={() => {
              initAddresses();
            }}
            setAddressModalVisibility={(bool) => {
              setAddressModalVisibility(bool);
            }}
            data={selectedShippingAddress}
            type="Ship"
          />

          <Text style={styles.inputTitle}>TIN/SSN</Text>
          <TextInput style={styles.inputContainer}
            placeholder='TIN/SSN'
            placeholderTextColor='#7f8c8d'
            value={tin_ssn}
            onChangeText={(e) => { setTin_ssn(e) }}
          />

          <Text style={styles.inputTitle}>Amount</Text>
          <TextInput style={styles.inputContainer}
            placeholder='Amount'
            placeholderTextColor='#7f8c8d'
            maxLength={15}
            keyboardType='numeric'
            value={amount}
            onChangeText={(e) => {
              if (+e || e == '' || e == 0) {
                setAmount(e)
              }
            }}
          />

          <Text style={styles.inputTitle}>Mode of Transaction</Text>
          <Picker
            selectedValue={modeOfTransaction}
            style={[styles.inputPickerContainer, { marginBottom: iosMargin, marginTop: iosMargin }]}
            onValueChange={(itemValue, itemIndex) => {
              setModeOfTransaction(itemValue)
            }}
          >
            <Picker.Item label={'Cash Pickup'} value={'cs_pickup'} />
            <Picker.Item label={'Cheque Pickup'} value={'cq_pickup'} />
            <Picker.Item label={'Bank Deposit'} value={'bank_deposit'} />
            <Picker.Item label={'Gcash'} value={'gcash'} />
            <Picker.Item label={'Palawan Express Pera Padala'} value={'palawan_express'} />
            <Picker.Item label={'Smart Padala'} value={'smart_padala'} />
          </Picker>
          <View style={sell.space} />
          <View style={sell.space} />

          {renderTINSSNPhotoModal()}

          {renderImageError()}
          <Text style={styles.inputTitle}>TIN/SSN ID Photo/Scan</Text>

          <View style={sell.space} />
          <View style={sell.space} />
          {/* <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} /> */}
          <View style={styles.uploadContainer}>
            {handleImagePicker(tinssnImage, 'tinssn')}
            {/* <ScrollView horizontal={true} scrollEnabled={true}
              style={{ width: width / 2 - 2 }}>
              {handleImagePicker(tinssnImages, 'tinssn')}
              {handleUIAddPhoto(tinssnImages, 'tinssn')}
            </ScrollView> */}
          </View>

          {/* <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} /> */}
          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />

          {renderSupportingPhotoModal()}

          {renderImageError()}
          <Text style={styles.inputTitle}>Supporting ID Photo/Scan</Text>

          <View style={sell.space} />
          <View style={sell.space} />
          {/* <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} /> */}
          <View style={styles.uploadContainer}>
            {handleImagePicker(supportingIDimage, 'supporting')}
            {/* <ScrollView horizontal={true} scrollEnabled={true}
              style={{ width: width / 2 - 2 }}>
              {handleImagePicker(supportingIDimages, 'supporting')}
              {handleUIAddPhoto(supportingIDimages, 'supporting')}
            </ScrollView> */}
          </View>

          {/* <View style={{ borderTopWidth: 3, borderTopColor: '#e1e2e3' }} /> */}
          <View style={sell.space} />
          <View style={sell.space} />
          <View style={sell.space} />


          <TouchableOpacity onPress={async () => {
            if (!loading) {
              setSignupTapped(true)
              handleSubmit()
            }
          }}
          >
            <LinearGradient
              colors={['#f89522', '#f58223', '#f36e23']}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.btnSubmit}>
              <Text style={styles.btnTextSubmit}>Submit</Text>
            </LinearGradient>
          </TouchableOpacity>

          <Text style={{ marginBottom: 30 }}></Text>
        </View>
      </ScrollView>
    </Container>
  );
}

const styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5,
    height: 40,
    marginBottom: 10,
    // fontSize: 14  
  },
  grayText: {
    fontSize: 14,
    color: 'gray',
  },
  addAddress: {
    backgroundColor: '#e6e6e6',
    borderWidth: 0.7,
    borderColor: '#d0dbdb',
    paddingVertical: 20,
    marginBottom: 10,
    marginTop: 5,
  },
  modalBackgroundContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  inputPickerContainer: {
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5,
    // height: 40,
    marginBottom: 10,
    // fontSize: 14  
  },
  inputTitle: {
    // fontFamily:'Roboto',
    fontSize: 16,
    fontWeight: 'bold',
  },
  PMBstyles: {
    color: '#7f8c8d',
    marginVertical: 10
  },
  headerStyles: {
    position: 'absolute',
    left: 20,
    alignSelf: 'center'
  },
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'Roboto', 
    fontSize: 20,
    alignSelf: 'center'
  },
  formContainer: {
    paddingHorizontal: 20,
    marginTop: 20
  },
  addVariation: {
    marginLeft: 25,
  },
  btnSubmit: {
    // borderRadius: 5,
    // marginHorizontal: 30,
    // height: 38,
    // backgroundColor: 'transparent',
    // justifyContent: 'center',
    // alignItems: 'center'

    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10
      },
    }),
    borderRadius: 5,
    width: width - 60,
    marginVertical: 10
  },
  btnTextSubmit: {
    // fontSize: 16,
    // fontFamily: 'Roboto',
    // textAlign: 'center',
    // color: '#ffffff',

    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  uploadContainer: {
    height: height / 4.3,
    flexDirection: 'row',
    padding: 5,
    backgroundColor: '#edf0f2',
  },
  newProductImage: {
    alignSelf: 'center',
    width: wp('43%'),
    height: hp('25%'),
  },
  addressNameText: {
    fontSize: 13,
    color: 'black',
    fontWeight: 'bold',
  },
  addressGrayText: {
    fontSize: 14,
    color: 'gray',
  },
  goBackHomeButton: {
    borderColor: '#00a14b',
    borderWidth: 2,
    height: 35,
    width: 160,
    borderRadius: 4,
    justifyContent: 'center'
  },
})
