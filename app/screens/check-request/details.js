import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  Alert,
  Modal,
  TouchableWithoutFeedback,
} from 'react-native';
import { Container, Header, Thumbnail, Content, Right, Left, Spinner, Card, CardItem, Body, Input } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AsyncStorage from '@react-native-community/async-storage';
import { TabView, TabBar, SceneMap } from 'react-native-tab-view';
import Assets from '../../components/assets.manager';
import Icons from '../../components/icons';
import Service from '../../components/api/service';
import Endpoints from '../../components/api/index';
import { Picker } from '@react-native-picker/picker';
import moment from 'moment';
const { width, height } = Dimensions.get('window');


export default function CheckRequestDetails({ navigation, route }) {

  const [checkRequest, setCheckRequest] = useState({})
  const [orders, setorders] = useState([])
  const [user, setUser] = useState('')
  const [loading, setLoading] = useState(false)
  const [statusColor, setStatusColor] = useState(false)
  const [modeOfTransaction, setModeOfTransaction] = useState('')
  // const [code, setCode] = useState('')

  useEffect(() => {
    try {
      init()
    } catch (err) {
      console.log(err)
    }
  }, [])

  const init = async () => {
    setLoading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      return
    }
    let user = typeof ud == 'string' ? JSON.parse(ud) : null
    setUser(user);

    setCheckRequest(route.params.checkRequest)

    console.log("route.params.checkRequest", route.params.checkRequest)

    switch (route.params.checkRequest.mo_transaction) {
      case 'cs_pickup':
        setModeOfTransaction('Cash Pickup')
        break;
      case 'cq_pickup':
        setModeOfTransaction('Cheque Pickup')
        break;
      case 'bank_deposit':
        setModeOfTransaction('Bank Deposit')
        break;
      case 'gcash':
        setModeOfTransaction('Gcash')
        break;
      case 'palawan_express':
        setModeOfTransaction('Palawan Express Pera Padala')
        break;
      case 'smart_padala':
        setModeOfTransaction('Smart Padala')
        break;

      default:
        setModeOfTransaction('Unknown')
        break;
    }

    switch (route.params.checkRequest?.status) {
      case 'expired':
        setStatusColor('slategray')
        break;
      case 'approved':
        setStatusColor('green')
        break;
      case 'declined':
        setStatusColor('crimson')
        break;
      case 'pending':
        setStatusColor('darkcyan')
        break;
      case 'needs_authentication':
        setStatusColor('orange')
        break;

      default:
        break;
    }

    setLoading(false)
  }

  const renderSubmitBtn = () => {
    if (checkRequest.status === 'needs_authentication') {
      return (
        <View>
          <CardItem style={{ flexDirection: 'row' }}>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <TouchableOpacity
                nativeOpacity={0.4}
                onPress={() => {
                  if (!loading) {
                    setLoading(true);
                    handleCheckRequestVerification()
                  }
                }}
                style={styles.receiveButton}>
                <Text style={styles.receiveText}>
                  Proceed with Verification
                </Text>
              </TouchableOpacity>
            </View>
          </CardItem>
        </View>
      )
    }
  }

  const handleCheckRequestVerification = async (verified = 'no', reloads = 0, verifData) => {
    let code;
    console.log('handleCheckRequestVerification verifData');
    try {
      console.log("handleCheckRequestVerification verifData 1", verifData)
      if (verified === 'no') {
        let data = {
          check_request_id: checkRequest.id,
          user_id: user.data.id,
        }

        await Service.resendCheckRequestCode(
          data,
          async (res) => {
            code = res.data.authentication_code;
            console.log("handleCheckRequestVerification code 2", code)
            if (res.status) {
              setLoading(false);
              console.log('verification_code', res.data.authentication_code);
              let verifData = {
                verifyCode: res.data.authentication_code,
                medium: 'email',
                expiration: res.data.authentication_expiration,
                user: user,
              }
              navigation.push("VerifyCode", {
                verifData,
                reloads,
                onVerified: (verified, reloads = 0, verifData) => {
                  handleCheckRequestVerification(verified, reloads, verifData);
                },
              })
            } else {
              setLoading(false);
              alert(res.msg);
            }
          },
          (err) => {
            setLoading(false);
            console.log(err)
          },
        );
      } else {
        console.log("handleCheckRequestVerification verifData 3", verifData)
        code = verifData.code
        let data2 = {
          check_request_id: checkRequest.id,
          user_id: user.data.id,
          authentication_code: code,
        }

        await Service.verifyCheckRequestCode(
          data2,
          async (res2) => {
            if (res2.msg === "Sorry the authentication code has been expired.") {
              setLoading(false);
              console.log('Data: ', res2);
              Alert.alert(
                'Alert!',
                'Sorry but the Code has expired.\n' +
                'Click the resend code below to try again.',
                [
                  {
                    text: 'OK',
                  },
                ],
                { cancelable: false },
              );
            } else if (res2.status) {
              setLoading(false);
              console.log('Data: ', res2);
              Alert.alert(
                'Success',
                'Your Check Request has been verified!\n' +
                'Please wait for a few days we review your Check Request.',
                [
                  {
                    text: 'OK',
                    onPress: async () => {
                      if (route.params?.from == "Checkout") {
                        route.params.onAdd();
                      }
                      // navigation.goBack();
                      // navigation.goBack();
                      navigation.navigate("CheckRequests")
                    },
                  },
                ],
                { cancelable: false },
              );
            } else {
              setLoading(false);
              console.log('Data: ', res2);
              Alert.alert(
                'Alert!',
                'There was a problem verifying your Check Request.\n' +
                'Contact us at support@referapps.com so that we can help you.',
                [
                  {
                    text: 'OK',
                  },
                ],
                { cancelable: false },
              );
            }
          },
          (err) => {
            setLoading(false);
            console.log('Data: ', err);
            alert(res2.msg);
          },
        );
      }
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <Container style={{ backgroundColor: '#FFF' }}>
      <Header
        transparent
        style={{
          backgroundColor: '#ffffff',
          paddingLeft: 10,
          paddingRight: 10,
          // marginBottom: -5,
        }}>
        <Left style={{ flex: 1, marginHorizontal: 10 }}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
          >
            <Thumbnail
              source={Assets.accountSettings.iconBack}
              square
              style={{
                resizeMode: 'contain',
                width: 20,
                height: 20,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{ flex: 3, justifyContent: 'flex-start', marginHorizontal: -43 }}>
          <Text style={{
            // fontFamily: 'Roboto',
            fontSize: 20,
            color: '#231f20',
          }}>Order Details</Text>
        </Body>
      </Header>
      {loading ? <Spinner /> : null}
      <Content padder style={{ backgroundColor: '#F5F5F5' }}>
        <Card>
          <CardItem>
            <Body>
              {/* <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold' }}>
                Status:
                <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', textTransform: 'capitalize' }}>
                  {" "}{orders[0]?.items[0]?.status}
                </Text>
              </Text> */}
              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Check ID:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d', }}>
                {checkRequest?.check_id}
              </Text>


              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Status:
              </Text>
              <Text style={{ fontSize: 18, color: statusColor, textTransform: 'capitalize', fontWeight: 'bold', }}>
                {checkRequest?.status === 'needs_authentication' ? 'Needs Authentication' : checkRequest?.status}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                TIN/SSN:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {checkRequest?.tin_ssn}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Address:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {checkRequest?.address}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Mode of Transaction:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {modeOfTransaction}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Amount:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {user.country_code === 'PH' && '₱'}
                {user.country_code === 'US' && '$'}
                {checkRequest?.amount}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 5, }}>
                Last Update:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {new moment(checkRequest?.updated_at, 'YYYY-MM-DD hh:mm:ss').format('MMMM Do YYYY, h:mm:ss a')}
              </Text>

              <Text style={{ fontSize: 18, color: '#363f4d', fontWeight: 'bold', marginTop: 8, }}>
                Created at:
              </Text>
              <Text style={{ fontSize: 18, color: '#363f4d' }}>
                {new moment(checkRequest?.created_at, 'YYYY-MM-DD hh:mm:ss').format('MMMM Do YYYY, h:mm:ss a')}
              </Text>
            </Body>
          </CardItem>

          <View style={{ marginHorizontal: 10, borderTopWidth: 3, borderTopColor: '#e1e2e3' }} />

          {renderSubmitBtn()}
        </Card>

      </Content>
    </Container >
  );
}

const styles = StyleSheet.create({
  backIcon: {
    width: 18,
    height: 18,
    resizeMode: 'contain',
    marginLeft: 17,
  },
  headerText: {
    // fontFamily:'roboto.medium',
    fontSize: 20,
    textAlign: 'left',
  },
  left: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  addVariation: {
    marginLeft: 25,
  },
  chatIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    tintColor: '#00a14b',
  },
  receiveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto', 
    fontSize: 18,
    color: '#ffffff',
    // padding: 5,
  },
  receiveButton: {
    justifyContent: 'space-around',
    alignSelf: 'center',
    // width: '50%',
    backgroundColor: '#e67300',
    // height: 30,
    borderRadius: 5,
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginBottom: 5,
    // paddingTop: 10,
    // paddingBottom: 5,
  },
  modalCon: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  containerModal: {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .5)',
    position: 'absolute',
  },
  ReferrerModalContainer: {
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    // width: wp('90%'),
    borderRadius: 5,
    justifyContent: 'space-around',
    paddingVertical: 20,
    paddingHorizontal: 30,
    // paddingLeft: 20,
    // paddingRight: 20,
    // paddingTop: 20,
    // paddingBottom: 20,
    // height: height / 2,
    // paddingHorizontal: 50
  },
  ReferrerModalHeaderText: {
    // fontFamily: 'Roboto',
    fontSize: 18,
    textAlign: "left",
    color: '#e67300',
    paddingBottom: 5,
    // paddingTop: hp('2%'),
  },
  ReferrerModalOptionText: {
    // fontFamily: 'Roboto',
    // fontSize: hp('1.50%'),
    fontSize: 14,
    textAlign: "left",
    color: '#444',
    paddingTop: 5,
  },
  ReferrerModalOptionText2: {
    // fontFamily: 'Roboto',
    // fontSize: hp('1.50%'),
    fontSize: 14,
    textAlign: "left",
    color: '#444',
    paddingTop: 5,
  },
  referrerSaveBtn: {
    backgroundColor: '#00a14b',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: hp('2%'),
    marginBottom: hp('1%'),
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  lbcBtn: {
    backgroundColor: '#F54040',
    alignSelf: 'center',
    // width: wp('70%'),
    // height: hp('3.50%'),
    borderRadius: 15,
    justifyContent: 'center',
    marginTop: hp('2%'),
    marginBottom: hp('1.50%'),
    paddingVertical: 4,
    paddingHorizontal: 15,
  },
  SaveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#ffffff',
  },
  LBCSaveText: {
    alignSelf: 'center',
    // fontFamily: 'Roboto',
    fontSize: 16,
    padding: 3,
    color: '#ffffff',
    fontWeight: "bold",
  },
  group_input_text: {
    maxHeight: hp('8  %'),
    textAlign: 'center',
    fontSize: 16,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ddd',
    marginTop: 10,
    // fontFamily: 'Roboto',
    // color: '#7f8c8d',
    // marginRight: 30,
  },
  inputContainer: {
    // width: width,
    // height: 34,
    // flex: 1,
    paddingTop: 0,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  datePicker: {
    color: '#7f8c8d',
    // width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {
        transform: [
          { scaleX: 0.9 },
          { scaleY: 0.9 }
        ]
      },
    })
  },
  fieldErrors: {
    padding: 10,
    // paddingLeft: 10,
    paddingBottom: 5,
    paddingTop: 13,
  },
});
