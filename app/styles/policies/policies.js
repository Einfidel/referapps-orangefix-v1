import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window')
export default StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  textContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    width,
    backgroundColor: '#F7F5F6',
    borderBottomColor: '#d0dbdb',
    borderBottomWidth: 1,
  },
  touchable: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iconSub: {
    height: 20,
    resizeMode: 'contain',
    alignSelf: 'center',
    tintColor: '#7f8c8d',
    position: 'absolute',
    right: -20
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  contentText: {
    color: '#7a7a7a',
    fontSize: 14,
    // fontFamily:'Roboto'
  }
})