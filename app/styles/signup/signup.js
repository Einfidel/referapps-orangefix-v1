import { StyleSheet, Dimensions, Platform } from 'react-native'

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({

  bgImage: {
    width,
    height,
    justifyContent: 'space-between',
  },
  headerContainer: {
    paddingVertical: 30,
    marginLeft: 30,
    marginRight: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  arrowBackContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 0,
  },
  imgArrow: {
    width: 24,
    height: 24,
    alignSelf: 'center',
  },
  txtSignUp: {
    // fontFamily: 'Roboto',
    fontSize: 28,
    color: '#7f8c8d',
    alignSelf: 'center',
  },
  fieldContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
  txtInputContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    borderRadius: 5,
    marginVertical: 5
  },
  txtInput: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    paddingLeft: 10,
    ...Platform.select({
      ios: {
        paddingVertical: 15,
      },
      android: {
        // paddingVertical: 0,
      },
    }),
  },
  txtInputs: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    paddingLeft: 10,
  },
  birthdateContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingHorizontal: 10,
    paddingVertical: 14,
    borderRadius: 5,
    marginVertical: 5,
    color: '#231f20',
  },
  connectContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 30,
    marginTop: 50
  },
  gradient: {
    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 20,
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 20,
      },
    }),
    borderRadius: 5,
    width: width - 60,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 5
  },
  icons: {
    width: 20,
    height: 20,
  },
  txtConnect: {
    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  noteContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  txtNote: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  modalLoading: {
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, .3)',
  },
  processingContainer: {
    backgroundColor: '#231f20',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    width: 160,
    height: 160,
    borderRadius: 30,
  },
  txtProcessing: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#ffffff',
    textAlign: 'center',
  },
  gradientBtnSignup: {
    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10
      },
    }),
    borderRadius: 5,
    width: width - 60,
    marginVertical: 10
  },
  txtGradientBtnSignup: {
    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  cnumber: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    paddingRight: 10,
    paddingLeft: 5,
    borderRadius: 5,
    marginVertical: 5,
  },
  datePicker: {
    color: '#7f8c8d',
    width: height / 6,
    ...Platform.select({
      ios: {
        paddingTop: 12
      },
      android: {

      },
    })
  },
});