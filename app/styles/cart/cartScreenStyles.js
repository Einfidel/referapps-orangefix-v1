import { StyleSheet, Dimensions, Platform } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const { width } = Dimensions.get('window')

export default StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  cartContent: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: hp('70%'),
    flexDirection: 'column',
  },
  cartContentText: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    marginBottom: 20,
    color: '#231f20',
  },
  goBackHomeButton: {
    borderColor: '#00a14b',
    borderWidth: 2,
    height: 35,
    width: 160,
    borderRadius: 4,
    justifyContent: 'center'
  },
  goBackHomeText: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    color: '#00a14b'
  },
  footer1: {
    backgroundColor: '#ffffff',
    borderTopColor: "transparent",
    shadowColor: '#47494c',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.4
  },
  footerTab: {
    backgroundColor: '#ffffff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
  },
  footer2: {
    backgroundColor: '#ffffff',
    borderTopColor: "transparent",
  },
  footerTab2: {
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  btnCheckout: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: '#f36e23',
    padding: 5,
    borderRadius: 5,
  },
  btnTextCheckout: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#ffffff',
    alignSelf: 'center',
  },
  txtSubTotal: {
    // fontFamily: 'Roboto',
    fontSize: 12,
    color: '#231f20',
  },
  txtPrice: {
    color: '#e85451',
    // fontFamily: 'Roboto',
    fontSize: 12,
  }
})