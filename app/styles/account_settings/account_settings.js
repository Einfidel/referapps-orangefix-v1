import { StyleSheet, Dimensions } from 'react-native';

var width, height = Dimensions.get('window').width;

export default StyleSheet.create({
  headerText: {
    // fontFamily: 'roboto-medium',
    fontSize: 18,
    color: '#231f20',
    alignSelf: 'center',
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  headerStyle: {
    ...Platform.select({
      ios: {
        height: 60,
        paddingBottom: 15
      },
      android: {

      },
    }),
    backgroundColor: '#ffffff',
    paddingLeft: 10,
    paddingRight: 10,
    borderColor: '#ffffff',
  },
  searchContainer: {
    width: '85%',
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    paddingVertical: 0,
    paddingLeft: 0,
  },
  searchIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  searchText: {
    // fontFamily: 'Roboto',
    fontSize: 18,
    color: 'black',
    height: height / 11,
    paddingVertical: 0,
    paddingLeft: 10,
    fontWeight: 'bold',
  },
  textContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 10,
    paddingBottom: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: '#ffffff',
  },
  titleText: {
    // fontFamily: 'Roboto',
    fontSize: 15,
    color: '#231f20',
  },
  contentText: {
    // fontFamily: 'Roboto',
    fontSize: 15,
    color: '#7f8c8d',
  },

  space5: {
    paddingTop: 5,
  },
  space15: {
    paddingTop: 15,
  },
  space20: {
    paddingTop: 20
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#e1e2e3',
  },
  right: {
    alignSelf: 'flex-end',
  },
  openIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  viewMore: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  emailIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  emailView: {
    justifyContent: 'space-between',
    paddingLeft: 13,
  },
  emailUs: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#231f20',
  },
  emailText: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
  phoneIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    alignSelf: 'center',
    transform: [{ rotate: '275deg' }]
  },
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: (Platform.OS == 'ios') ? 20 : 0
  },
  MainView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  ModalInsideView: {
    // justifyContent: 'center',
    // alignItems: 'center', 
    backgroundColor: "#ffffff",
    // height: 300,
    width: '90%',
    paddingBottom: 20,
    // borderRadius:10,
    // borderWidth: 1,
    // borderColor: '#fff'

  },
  ModalHeaderTitle: {
    paddingTop: 20,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  ModalAnswer: {
    backgroundColor: '#e1e2e3',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  TextStyle: {
    fontSize: 17,
    // marginBottom: 20, 
    fontWeight: '100',
    padding: 20,
    // backgroundColor: '#e1e2e3',
    // height: '90%',
  },
  LongModalInsideView: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#ffffff",
    height: '95%',
    width: '90%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  LongModalHeaderTitle: {
    paddingTop: 10,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  LongModalAnswer: {
    backgroundColor: '#f2f1f0',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20
  },
  MediumModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 1.3,
    width: '90%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  MediumModalHeaderTitle: {
    paddingTop: 20,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  MediumModalAnswer: {
    backgroundColor: '#e1e2e3',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  //  ModalInsideView1:{
  //     backgroundColor : "#ffffff", 
  //     // height: height/1.2,
  //     width: '90%',
  //     // borderRadius:10,
  //     // borderWidth: 1,
  //     borderColor: '#fff',
  //     paddingBottom: 20
  //   },
  //   ModalHeaderTitle1: {
  //     paddingTop: 20,
  //     paddingLeft: 10,
  //     textAlign: 'left',
  //     fontWeight: 'bold',
  //     color: '#ff8400',
  //     fontSize: 18,
  //   },
  //   ModalAnswer1: {
  //     backgroundColor: '#e1e2e3',
  //     marginTop: 20,
  //     marginLeft: 10,
  //     marginRight: 10
  //   },
  ShortModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 1.8,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  ShortModalHeaderTitle: {
    paddingTop: 15,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  ShortModalAnswer: {
    backgroundColor: '#e1e2e3',
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10
  },
  SemiMediumModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 1.6,
    width: '90%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  SemiMediumModalHeaderTitle: {
    paddingTop: 10,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  SemiMediumModalAnswer: {
    backgroundColor: '#e1e2e3',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10
  },
  SMModalHeaderTitle: {
    paddingTop: 20,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  SMModalAnswer: {
    backgroundColor: '#e1e2e3',
    marginTop: 20,
    marginLeft: 10,
    marginRight: 10
  },
  ModalInsideView7: {
    backgroundColor: "#ffffff",
    // height: height/1.4,
    width: '90%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff',
    paddingBottom: 20,
  },
  ModalHeaderTitle7: {
    paddingTop: 15,
    paddingLeft: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#ff8400',
    fontSize: 18,
  },
  SmallModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 1.5,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  SSModalInsideView: {

    backgroundColor: "#ffffff",
    height: height / 1.2,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  XSModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 2.4,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  ModalInsideView13: {
    backgroundColor: "#ffffff",
    height: height / 1.3,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
  SmallModalInsideView: {
    backgroundColor: "#ffffff",
    height: height / 2,
    width: '88%',
    // borderRadius:10,
    // borderWidth: 1,
    borderColor: '#fff'
  },
});