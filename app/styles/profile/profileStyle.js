import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({

  //My Profile
  BG: {
    height: height / 3.9,
    width: Dimensions.get('window').width,
    resizeMode: 'contain',
  },
  bgOpacity: {
    backgroundColor: 'rgba(0, 0, 0, .4)',
    alignItems: 'flex-start',
    height: '100%',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnSignIn: {
    paddingHorizontal: 30,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#f36e23',
    alignItems: 'center',
    marginHorizontal: 10
  },
  txtSignIn: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#ffffff',
    textAlign: 'center',
  },
  authorizeContainer: {
    height: height / 2.5,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  imgAuthorize: {
    height: height / 17,
    width: height / 17,
    resizeMode: 'contain'
  },
  txtJoin: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
    textAlign: 'center',
    paddingVertical: 10
  },
});