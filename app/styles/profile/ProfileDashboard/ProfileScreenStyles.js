import { StyleSheet, Dimensions } from 'react-native'

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
  },
  // not Authenticated
  redirectContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  redirectTitleText: {
    color: '#231f20',
    // fontFamily: 'Roboto',
    fontSize: 25,
  },
  redirectButton: {
    width: '80%',
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#00a14b',
  },
  redirectButtonText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 20,
  },

  //Authenticated
  headerContainer: {
    height: 190,
    width: Dimensions.get('window').width,
  },
  headerCoverImage: {
    width: Dimensions.get('window').width,
    height: 225,
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  searchText: {
    // fontFamily: 'Roboto',
    color: '#7f8c8d',
  },
  // pinned products
  itemRow: {
    justifyContent: "space-around",
  },
  pinIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  pinItemText: {
    color: '#231f20',
    fontSize: 15,
  },
  historyIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  forwardIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 10,
    marginLeft: 5,
  },

  // Information
  informationContainer: {
    backgroundColor: '#ffffff',
    height: height / 2
  },

  // contentContainer: {
  //   height: height/2+50
  // },

  // Transaction
  historyText: {
    color: '#231f20',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText1: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText2: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 25,
  },
  transactionHistoryText: {
    color: '#f36e23',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },

  // custom
  space: {
    paddingTop: 5,
  },
  space10: {
    paddingTop: 10,
  },
  space20: {
    paddingTop: 20,
  },
  space65: {
    paddingTop: 65,
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  white: {
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 30,
  },
  userNameText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    alignSelf: 'flex-start',
    paddingBottom: 5,
  },
  userInfoText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: 12,
    alignSelf: 'flex-start',
    paddingBottom: 4,
  },
  ratingText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 12,
    alignSelf: 'center',
    paddingBottom: 4,
  },
  ratingScore: {
    color: '#f36e23',
    // fontFamily: 'Roboto',
    fontSize: 15,
    alignSelf: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
  avatar: {
    justifyContent: 'space-around',
  },
  avatarImage: {
    alignSelf: 'center',
    height: 90,
    width: 90,
    borderRadius: 45,
  },
  searchItem: {
    paddingLeft: 5,
    backgroundColor: '#ffffff',
    width: '100%',
    height: 30,
  },
  cartIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
  profileRow: {
    flexDirection: 'row',
    // justifyContent: 'space-around',
    width: width,
  },
  coverImageContainer: {
    alignItems: 'flex-start',
    height: '100%',
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'space-around',
  },
  backgroundImageContainer: {
    backgroundColor: 'rgba(0, 0, 0, .4)',
    alignItems: 'flex-start',
    height: '100%',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },

  overviewIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
  },
});
