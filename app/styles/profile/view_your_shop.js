import { StyleSheet, Dimensions, Platform } from 'react-native'

import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
  },
  // not Authenticated
  redirectContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  redirectTitleText: {
    color: '#231f20',
    // fontFamily: 'Roboto',
    fontSize: 25,
  },
  redirectButton: {
    width: '80%',
    borderRadius: 20,
    alignSelf: 'center',
    justifyContent: 'center',
    backgroundColor: '#00a14b',
  },
  redirectButtonText: {
    color: '#ffffff',
    // fontFamily: 'Roboto',
    fontSize: 20,
  },

  //Authenticated
  headerContainer: {
    height: 190,
    width: Dimensions.get('window').width,
  },
  headerCoverImage: {
    width: Dimensions.get('window').width,
    // height: 230,
    height: height / 3.5,
    display: 'flex',
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
  // pinned products
  itemRow: {
    justifyContent: "space-around",
  },
  pinIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  pinItemText: {
    color: '#231f20',
    fontSize: 15,
  },
  historyIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  forwardIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 10,
    marginLeft: 5,
  },

  // Information
  informationContainer: {
    backgroundColor: '#ffffff',
    height: height / 2
  },

  // contentContainer: {
  //   height: height/2+50
  // },

  // Transaction
  historyText: {
    color: '#231f20',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText1: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
  },
  historyText2: {
    color: '#7f8c8d',
    // fontFamily: 'Roboto',
    fontSize: 12,
    textAlign: 'center',
    paddingTop: 25,
  },
  transactionHistoryText: {
    color: '#f36e23',
    paddingLeft: 3,
    // fontFamily: 'Roboto',
    fontSize: 12,
  },

  // custom
  space: {
    paddingTop: 5,
  },
  space10: {
    paddingTop: 10,
  },
  space20: {
    paddingTop: 20,
  },
  space65: {
    paddingTop: 65,
  },
  border: {
    height: 1,
    width: width,
    backgroundColor: '#d0dbdb',
  },
  white: {
    backgroundColor: '#ffffff',
    paddingTop: 5,
    paddingBottom: 5,
    flexDirection: 'row',
    height: 30,
  },
  userNameText: {
    color: "white",
    textShadowColor: 'black',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    // fontFamily: 'roboto-bold',
    fontSize: 18,
    // alignSelf:'flex-start',
    paddingBottom: 5,
  },
  userInfoText: {
    color: "white",
    textShadowColor: 'black',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    // fontFamily: 'roboto-bold',
    fontSize: hp('1.50%'),
    alignSelf: 'flex-start',
    paddingBottom: 4,
  },
  ratingScore: {
    color: '#f36e23',
    // fontFamily: 'Roboto',
    fontSize: 15,
    alignSelf: 'flex-end',
  },
  row: {
    flexDirection: 'row',
  },
  column1: {
    justifyContent: 'center',
    flexDirection: 'column',
    marginLeft: 10
  },
  column2: {
    justifyContent: 'center',
    flexDirection: 'column',
  },
  avatar: {
    justifyContent: 'space-around',
  },
  avatarImage: {
    alignSelf: 'center',
    height: 90,
    width: 90,
    borderRadius: 45,
  },
  searchItem: {
    paddingLeft: 5,
    backgroundColor: '#ffffff',
    width: '100%',
    height: 30,
  },
  cartIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      },
      android: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      }
    })
  },
  profileRow: {
    flexDirection: 'row',
    // justifyContent: 'space-around',
    width: width,
  },
  coverImageContainer: {
    alignItems: 'flex-start',
    height: '100%',
    paddingTop: 5,
    paddingBottom: 5,
    justifyContent: 'space-around',
  },
  backgroundImageContainer: {
    backgroundColor: 'rgba(0, 0, 0, .4)',
    alignItems: 'flex-start',
    height: '100%',
    width: '100%',
    paddingTop: 5,
    paddingBottom: 5,
  },

  overviewIcon: {
    width: 10,
    height: 10,
    resizeMode: 'contain',
  },
  //Added Styles for ViewShop
  backIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      },
      android: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      }
    })
  },
  searchContainer: {
    width: width / 1.5,
    backgroundColor: '#ffffff',
    paddingVertical: 0,
    paddingLeft: 0,
    borderRadius: 20
  },
  searchText: {
    // fontFamily: 'Roboto',
    color: '#7f8c8d',
    paddingRight: hp('2%'),
    fontSize: hp('1.70%'),
  },
  ShopRatingText: {
    color: '#ffffff',
    // fontFamily: 'roboto-bold',
    fontSize: hp('1.70%'),
    paddingBottom: hp('1%'),
    width: wp('29.50%'),
    textAlign: 'right',
    fontWeight: '600'
  },
  ratingText: {
    color: '#ffac30',
    // fontFamily: 'roboto-bold',
    fontSize: hp('1.80%'),
    marginLeft: hp('4%')
  },
  MainContainer: {
    // height: 30,
    flexDirection: 'row',
    paddingTop: 10
  },
  headerRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  touchableBackButton: {
    paddingLeft: 15,
    paddingTop: hp('0.80%'),
  },
  ItemContainer: {
    paddingLeft: hp('1.50%'),
    backgroundColor: '#f7f7f7',
    width: wp('55%'),
    height: hp('4%'),
    borderRadius: wp('15%'),
    borderColor: '#ffffff',
    borderBottomWidth: .1,
    ...Platform.select({
      ios: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      },
      android: {
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 1,
      }
    })
  },
  touchableChatIcon: {
    alignSelf: 'flex-start',
    paddingLeft: hp('3%'),
    paddingTop: hp('0.80%'),
  },
  touchableCartIcon: {
    alignSelf: 'flex-start',
    paddingLeft: hp('3%'),
    paddingTop: hp('0.60%'),
  },
  productValues: {
    width: wp('17%'),
    textAlign: 'right',
    fontSize: hp('1.50%'),
    color: "white",
    textShadowColor: 'black',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  Values: {
    width: wp('17.50%'),
    textAlign: 'right',
    color: "white",
    textShadowColor: 'black',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    fontSize: hp('1.50%'),
    marginLeft: 1
  },
  starIcon: {
    resizeMode: 'contain',
    width: wp('10%'),
    height: hp('5%'),
    marginLeft: hp('7%'),
  },
  defaultProfile: {
    borderRadius: wp('25%'),
    height: 110,
    width: 110,
    marginLeft: hp('2%'),
    marginVertical: hp('3%')
  },
  allProductsView: {
    flexDirection: 'row',
    padding: 10,
  },
  titleBorderWidth: {
    alignSelf: 'center',
    height: 20,
    width: '25%',
    borderRightWidth: 2,
    borderColor: '#bfbfbf',
  },
  priceBorderWidth: {
    alignSelf: 'center',
    height: 20,
    width: '25%',
  },
  titles: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#7f8c8d',
    alignSelf: 'center',
  },
  selectedTitle: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#4E4E4E',
    alignSelf: 'center',
  },
  titlesPosition: {
    alignSelf: 'center',
  },
  referredSection: {
    backgroundColor: '#ffffff',
    paddingVertical: 30
  },
  referredText: {
    // fontStyle:'italic', 
    color: '#7f8c8d',
    fontSize: 13,
    textAlign: 'center',
    // fontFamily: 'Roboto'
  },
  overviewSection: {
    backgroundColor: '#ffffff',
    paddingVertical: 10,
    paddingHorizontal: 30
  },
  text1: {
    fontSize: 15,
    // fontFamily: 'Roboto'
  },
  dateText: {
    fontWeight: '700',
    fontSize: 15,
    // fontFamily: 'Roboto'
  },
  locationSection: {
    flexDirection: 'row',
    paddingTop: 15
  },
  locationIcon: {
    width: 19,
    height: 19,
    resizeMode: 'contain',
    marginLeft: 5
  },
  locationText: {
    fontSize: 15,
    marginLeft: 10,
    // fontFamily: 'Roboto'
  },
  emailSection: {
    flexDirection: 'row',
    paddingTop: 27
  },
  emailIcon: {
    width: 17,
    height: 17,
    resizeMode: 'contain',
    marginLeft: 5
  },
  emailText: {
    fontSize: 15,
    marginLeft: 10,
    bottom: 1,
    // fontFamily: 'Roboto'
  },
  facebookIcon: {
    width: 32,
    height: 32,
    resizeMode: 'contain',
    marginLeft: 5,
  },
  viewShopMargin: {
    ...Platform.select({
      android: {
        paddingTop: 0
      },
      ios: {
        paddingTop: 30
      }
    })
  },
});
