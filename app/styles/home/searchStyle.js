import { StyleSheet, Dimensions } from 'react-native'
const { width } = Dimensions.get('window')
export default StyleSheet.create({
    header: {
        backgroundColor: '#FFFFFF',
        marginLeft: 10,
        marginRight: 10,
        borderColor: '#FFFFFF',
    },
    leftArrow: {
        resizeMode: 'contain',
        width: 20,
        height: 20,
    },
    left: {
        flex: 0,
        flexDirection: 'row',
        marginRight: 10,
        marginLeft: 5,
    },
    txtSearch: {
        // fontFamily: 'Roboto',
        fontSize: 14,
        color: '#231F20',
        padding: 10,
        backgroundColor: '#ECF0F1',
        width: '100%',
    },
    right: {
        flex: 0,
        marginRight: 5,
        marginLeft: 10,
    },
    rightIcon: {
        resizeMode: 'contain',
        width: 25,
        height: 25,
    },
    optionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'pink',
    },
    option: {
        width: width / 2 - 10,
        alignItems: 'center',
        justifyContent: 'space-around',
        flexDirection: 'row',
        padding: 10,
    },
    optionText: {
        // fontFamily: 'Roboto',
        fontSize: 14,
        color: '#231F20',
    }
})