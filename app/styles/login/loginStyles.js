import { StyleSheet, Dimensions, Platform } from 'react-native'

const { width, height } = Dimensions.get('window');

export default styles = StyleSheet.create({

  //Login
  imageContainer: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
  loginHeaderContainer: {
    paddingTop: 30,
    paddingBottom: 30,
    marginLeft: 30,
    marginRight: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 0,
  },
  goBack: {
    width: 24,
    height: 24,
    alignSelf: 'center',
  },
  txtLogin: {
    // fontFamily: 'Roboto',
    fontSize: 28,
    color: '#7f8c8d',
    alignSelf: 'center',
  },
  connectContainer: {
    flex: 0,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 30,
    marginRight: 30,
  },
  gradient: {
    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 20
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 20
      },
    }),
    borderRadius: 5,
    width: width - 60,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  imgConnect: {
    width: 20,
    height: 20,
  },
  txtConnect: {
    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  inputContainer: {
    flexDirection: 'row',
    backgroundColor: '#e6e6e6',
    borderRadius: 5,
    paddingHorizontal: 10,
    resizeMode: 'contain',
    height: width / 8.6
    // paddingVertical:5
  },
  imgUser: {
    width: 16,
    height: 16,
    alignSelf: 'center',
    flex: 0,
  },
  txtInput: {
    flex: 1,
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#231f20',
    paddingLeft: 10,
  },
  btnLoginContainer: {
    ...Platform.select({
      ios: {
        paddingVertical: 12,
        paddingHorizontal: 10,
      },
      android: {
        paddingVertical: 10,
        paddingHorizontal: 10,
      },
    }),
    borderRadius: 5,
    width: Dimensions.get('window').width - 60,
  },
  txtBtnLogin: {
    fontSize: 16,
    // fontFamily: 'Roboto',
    textAlign: 'center',
    color: '#ffffff',
    backgroundColor: 'transparent',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtForgot: {
    // fontFamily: 'Roboto',
    fontSize: 14,
    color: '#7f8c8d',
  },
});