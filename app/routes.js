import * as React from 'react';
import { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import AsyncStorage from '@react-native-community/async-storage';

import Splash from './screens/splash/index';
import Auth from './screens/auth/index';
import Login from './screens/auth/login';
import SignUp from './screens/auth/signup';
import AddEmailMobile from './screens/auth/add-email-mobile'

import MainTabScreen from './screens/index';

import Home from './screens/home/index';
import Pinned from './screens/pin/index';
import AddProduct from './screens/product/index';
import Notifications from './screens/notifications/index';
import MyProfile from './screens/profile/index';

import ProductDetails from './screens/product/details';
import CreateProduct from './screens/product/sell';
import ProductVariations from './screens/product/components/variations';
import AddStockPrices from './screens/product/components/stockprices';
import ShippingOptions from './screens/product/components/shippingoptions';
import CreateService from './screens/services/sell';
import ServiceVariations from './screens/services/components/variations';

import PinnedItems from './screens/pin/index';

import Scanner from './screens/scan/index';
import QRGenerator from './screens/scan/generate-qr';
import Vouchers from './screens/profile/orders/vouchers';
import PurchaseHistory from './screens/profile/orders/purchasehistory';

import ProfileOptions from './screens/profile/options';
import EditProfile from './screens/account/account_edit/index';
import EmailVerification from './screens/account/email-verify';
import PhoneVerification from './screens/account/phone-verify';
import Devices from './screens/account/devices';
import Addresses from './screens/account/address';
import BankAccounts from './screens/account/bankaccounts';

import ChatSettings from './screens/account/settings/chat';
import PrivacySettings from './screens/account/settings/privacy';
import LanguageSettings from './screens/account/settings/language';

import HelpCenter from './screens/account/help/center';
import Policies from './screens/account/help/policies';

import DeleteAccount from './screens/account/delete-account';

import ShopOptions from './screens/shop/options';
import EditShop from './screens/shop/edit';

import YourServices from './screens/profile/store/your_services';
import YourProducts from './screens/profile/store/your_products';
import AddAddress from './screens/account/addresses/add_address';
import EditAddress from './screens/account/addresses/edit_address';
import CartScreen from './screens/cart/index';
import PrivacyPolicy from './screens/account/help/policies/privacy_policy';
import PolicySellers from './screens/account/help/policies/policy_sellers';
import PolicyBuyers from './screens/account/help/policies/policy_buyers';
import MyPerformance from './screens/profile/store/my_performance';
import MyShipping from './screens/profile/store/shipping';
import Income from './screens/profile/store/income';
import CheckRequests from './screens/profile/store/check_request';
import History from './screens/profile/store/history';
import SearchResults from './screens/home/search';
import EditProduct from './screens/product/edit';
import EditService from './screens/services/edit';
import ViewShop from './screens/profile/view_shop';
import Checkout from './screens/checkout/index';
import CheckoutService from './screens/checkout/index-service';
import PaypalCheckout from './screens/checkout/paypal_checkout';
import XenditCheckout from './screens/checkout/xendit_checkout';
import SellerShop from './screens/cart/seller_shop'

import CheckRequestForm from './screens/check-request/request_form';
import CheckRequestDetails from './screens/check-request/details';

import ChattScreen from './screens/chat/chat';
import Conversation from './screens/chat/convo';
import ConvoMessages from './screens/chat/messages';

import Transactions from './screens/transactions/index';
import TransactionDetails from './screens/transactions/details';
import SocialMedia from './screens/account/social_media'

//deliveries
import ToShip from './screens/deliveries/to-ship'
import InTransit from './screens/deliveries/in-transit'
import Delivered from './screens/deliveries/delivered'

//RESET PASSWORD
import ForgotPassword from './screens/auth/forgot-password'
import VerifyCode from './screens/auth/verifycode'
import NewPassword from './screens/auth/new-password'
import NewPasswordSuccess from './screens/auth/new-password-success'
import TwoFactorAuth from './screens/auth/two-factor-auth'

import messaging from '@react-native-firebase/messaging';
var PushNotification = require('react-native-push-notification');

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function MainTabScreenx() {
  return (
    <Tab.Navigator
      initialRoute='Home'
      backBehavior='initialRoute'
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home';
            return <SimpleLineIcons name={iconName} size={size - 5} color={color} />;
          } else if (route.name === 'Pinned') {
            // iconName = focused ? 'pin-outline' : 'pin-outline';
            return <SimpleLineIcons name='pin' size={size - 5} color={color} />;
          } else if (route.name === 'Add Product') {
            // iconName = focused ? 'shopping-basket-add' : 'ios-list';
            return <Fontisto name='shopping-basket-add' size={size - 5} color={color} />;
          } else if (route.name === 'Notifications') {
            // iconName = focused ? 'ios-list-box' : 'ios-list';
            return <SimpleLineIcons name='bell' size={size - 5} color={color} />;
          } else if (route.name === 'My Profile') {
            // iconName = focused ? 'ios-list-box' : 'ios-list';
            return <SimpleLineIcons name='user' size={size - 5} color={color} />;
          }
        },
      })}
      tabBarOptions={{
        activeTintColor: '#66b545',
        inactiveTintColor: 'gray',
      }}
    >

      <Tab.Screen name='Home' component={Home} options={{ headerShown: false }} />
      <Tab.Screen name='Pinned' component={Pinned} options={{ headerShown: false, unmountOnBlur: true }}
        listeners={({ navigation }) => ({ blur: () => navigation.setParams({ screen: undefined }) })} />
      <Tab.Screen name='Add Product' component={AddProduct} options={{ headerShown: false }} />
      <Tab.Screen name='Notifications' component={Notifications} options={{ headerShown: false }} />
      <Tab.Screen name='My Profile' component={MyProfile} options={{ headerShown: false }} />

    </Tab.Navigator>
  );
}

function App() {
  const [loading, setloading] = useState(true);

  const init = async () => {
    if (loading) {
      setTimeout(() => setloading(false), 2200);
    }
  };

  useEffect(async () => {
    // const user = await AsyncStorage.getItem("user_data")
    // console.log("THIS IS USER", user)
    init();
  }, []);

  // useEffect(() => {
  //   const unsubscribe = messaging().onMessage(async (remoteMessage) => {
  //     console.log(remoteMessage.notification)
  //     PushNotification.localNotification({
  //       title: remoteMessage.notification.title,
  //       message: remoteMessage.notification.body,
  //       playSound: false,
  //       soundName: "default"
  //     })
  //   })
  //   return unsubscribe
  // }, [])

  if (loading) {
    return <Splash />;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName='Tab'>
        <Stack.Screen name='Tab' options={{ headerShown: false }} component={MainTabScreen} />

        <Stack.Screen name='ProductDetails' options={{ headerShown: false }} component={ProductDetails} />
        <Stack.Screen name='CreateProduct' options={{ headerShown: false }} component={CreateProduct} />
        <Stack.Screen name='CreateService' options={{ headerShown: false }} component={CreateService} />
        <Stack.Screen name='ServiceVariations' options={{ headerShown: false }} component={ServiceVariations} />
        <Stack.Screen name='ProductVariations' options={{ headerShown: false }} component={ProductVariations} />
        <Stack.Screen name='AddStockPrices' options={{ headerShown: false }} component={AddStockPrices} />
        <Stack.Screen name='ShippingOptions' options={{ headerShown: false }} component={ShippingOptions} />

        <Stack.Screen name='PinnedItems' options={{ headerShown: false }} component={PinnedItems} />
        <Stack.Screen name='ProfileOptions' options={{ headerShown: false }} component={ProfileOptions} />

        <Stack.Screen name='Auth' component={Auth} options={{ headerShown: false }} />
        <Stack.Screen name='Sign In' component={Login} options={{ headerShown: false }} />
        <Stack.Screen name='Sign Up' component={SignUp} options={{ headerShown: false }} />
        <Stack.Screen name='AddEmailMobile' component={AddEmailMobile} options={{ headerShown: false }} />

        <Stack.Screen name='EditProfile' component={EditProfile} options={{ headerShown: false }} />
        <Stack.Screen name='Social Media' component={SocialMedia} options={{ headerShown: false }} />
        <Stack.Screen name='EmailVerification' component={EmailVerification} options={{ headerShown: false }} />
        <Stack.Screen name='PhoneVerification' component={PhoneVerification} options={{ headerShown: false }} />
        <Stack.Screen name='Devices' component={Devices} options={{ headerShown: false }} />
        <Stack.Screen name='Addresses' component={Addresses} options={{ headerShown: false }} />
        <Stack.Screen name='BankAccounts' component={BankAccounts} options={{ headerShown: false }} />
        <Stack.Screen name='ChatSettings' component={ChatSettings} options={{ headerShown: false }} />
        <Stack.Screen name='PrivacySettings' component={PrivacySettings} options={{ headerShown: false }} />
        <Stack.Screen name='LanguageSettings' component={LanguageSettings} options={{ headerShown: false }} />
        <Stack.Screen name='HelpCenter' component={HelpCenter} options={{ headerShown: false }} />
        <Stack.Screen name='Policies' component={Policies} options={{ headerShown: false }} />
        <Stack.Screen name='DeleteAccount' component={DeleteAccount} options={{ headerShown: false }} />

        <Stack.Screen name='ShopOptions' component={ShopOptions} options={{ headerShown: false }} />
        <Stack.Screen name='EditShop' component={EditShop} options={{ headerShown: false }} />
        <Stack.Screen name='Your Products' component={YourProducts} options={{ headerShown: false }} />
        <Stack.Screen name='Your Services' component={YourServices} options={{ headerShown: false }} />
        <Stack.Screen name='Add Address' component={AddAddress} options={{ headerShown: false }} />
        <Stack.Screen name='Edit Address' component={EditAddress} options={{ headerShown: false }} />
        <Stack.Screen name='Cart Screen' component={CartScreen} options={{ headerShown: false }} />
        <Stack.Screen name='Checkout' component={Checkout} options={{ headerShown: false }} />
        <Stack.Screen name='CheckoutService' component={CheckoutService} options={{ headerShown: false }} />
        <Stack.Screen name='PaypalCheckout' component={PaypalCheckout} options={{ headerShown: false }} />
        <Stack.Screen name='XenditCheckout' component={XenditCheckout} options={{ headerShown: false }} />
        <Stack.Screen name='Privacy Policy' component={PrivacyPolicy} options={{ headerShown: false }} />
        <Stack.Screen name='Policy Sellers' component={PolicySellers} options={{ headerShown: false }} />
        <Stack.Screen name='Policy Buyers' component={PolicyBuyers} options={{ headerShown: false }} />
        <Stack.Screen name='My Performance' component={MyPerformance} options={{ headerShown: false }} />
        <Stack.Screen name='My Shipping' component={MyShipping} options={{ headerShown: false }} />
        <Stack.Screen name='My Income' component={Income} options={{ headerShown: false }} />
        <Stack.Screen name='CheckRequests' component={CheckRequests} options={{ headerShown: false }} />
        <Stack.Screen name='CheckRequestForm' component={CheckRequestForm} options={{ headerShown: false }} />
        <Stack.Screen name='CheckRequestDetails' component={CheckRequestDetails} options={{ headerShown: false }} />
        <Stack.Screen name='History' component={History} options={{ headerShown: false }} />

        <Stack.Screen name='Scanner' component={Scanner} options={{ headerShown: false }} />
        <Stack.Screen name='QRGenerator' component={QRGenerator} options={{ headerShown: false }} />
        <Stack.Screen name='Vouchers' component={Vouchers} options={{ headerShown: false }} />
        <Stack.Screen name='PurchaseHistory' component={PurchaseHistory} options={{ headerShown: false }} />
        <Stack.Screen name='Search Results' component={SearchResults} options={{ headerShown: false }} />
        <Stack.Screen name='EditProduct' component={EditProduct} options={{ headerShown: false }} />
        <Stack.Screen name='EditService' component={EditService} options={{ headerShown: false }} />
        <Stack.Screen name='ViewShop' component={ViewShop} options={{ headerShown: false }} />
        <Stack.Screen name='Messages' component={ChattScreen} options={{ headerShown: false }} />

        <Stack.Screen name='Conversation' component={Conversation} options={{ headerShown: false }} />
        <Stack.Screen name='ConvoMessages' component={ConvoMessages} options={{ headerShown: false }} />

        <Stack.Screen name='Seller Shop' component={SellerShop} options={{ headerShown: false }} />

        <Stack.Screen name='Transactions' component={Transactions} options={{ headerShown: false }} />
        <Stack.Screen name='TransactionDetails' component={TransactionDetails} options={{ headerShown: false }} />
        <Stack.Screen name='To Ship' component={ToShip} options={{ headerShown: false }} />
        <Stack.Screen name='In Transit' component={InTransit} options={{ headerShown: false }} />
        <Stack.Screen name='Delivered' component={Delivered} options={{ headerShown: false }} />

        <Stack.Screen name='Forgot Password' component={ForgotPassword} options={{ headerShown: false }} />
        <Stack.Screen name='VerifyCode' component={VerifyCode} options={{ headerShown: false }} />
        <Stack.Screen name='New Password' component={NewPassword} options={{ headerShown: false }} />
        <Stack.Screen name='New Password Success' component={NewPasswordSuccess} options={{ headerShown: false }} />
        <Stack.Screen name='Two Factor Auth' component={TwoFactorAuth} options={{ headerShown: false }} />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
