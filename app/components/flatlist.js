import React, { PureComponent } from 'react';

import { FlatList} from 'react-native';
import GridView from 'react-native-super-grid';

class FlatListComponent extends React.PureComponent {

    render() {
      return (
        <FlatList
          {...this.props}
          showsVerticalScrollIndicator={false}
          decelerationRate={0.20}
          pinchGestureEnabled={true}
          columnWrapperStyle={{
            alignItems: 'center',
            justifyContent: 'space-around',
            margin: 5,
          }}
        />
      )
    }
  }
  export default FlatListComponent;
