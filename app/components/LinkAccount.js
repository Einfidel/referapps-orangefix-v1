import React, { useState, useEffect } from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, TextInput, StatusBar, Alert, NativeModules } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginButton, AccessToken, LoginManager, GraphRequest, GraphRequestManager, } from 'react-native-fbsdk-next';
// import { API_URL, API_TOKEN, URL_FB_LOGIN } from '../keys'
import Endpoints from './api/endpoints'
import Service from './api/service';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

GoogleSignin.configure({
  // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  // scopes: [
  //   'email', 'profile', 'https://www.googleapis.com/auth/plus.profile.emails.read', 'https://www.googleapis.com/auth/plus.login'
  // ],
  webClientId: '75162238761-1ap1o48ba5u13gu1abvr19764oo33r0o.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  iosClientId: '75162238761-oeds0c4roo7ordic2m2lt3tn7dqsindl.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});

export const linkAccount = async (navigation, props, callback) => {

  await Service.userData(
    props.id,
    async (res) => {
      const email = res.data.info.data.email;
      if (email) {
        auth().fetchSignInMethodsForEmail(email)
          .then(async (providers) => {
            console.log("providers", providers);

            switch (providers[0].toString()) {
              case "google.com":
                console.log("provider[0]", providers[0])
                Alert.alert(
                  "Warning",
                  "This email already has a Google Account. We will now require you to login via Google.",
                  [
                    {
                      text: "Login via Google",
                      style: "cancel",
                      onPress: async () => {
                        console.log("Login via Google")
                        await GoogleSignin.hasPlayServices();
                        const { idToken } = await GoogleSignin.signIn();
                        const googleCredential = auth.GoogleAuthProvider.credential(idToken);

                        auth().signInWithCredential(googleCredential)
                          .then(async (response) => {
                            console.log("response", response)

                            if (auth().currentUser.email == email) {
                              await auth().currentUser.linkWithCredential(props.pendingCred);
                              // response.user.linkWithCredential(props.pendingCred)

                              await AsyncStorage.setItem('user_data', response);
                              await AsyncStorage.removeItem("app_refer_state");

                              alert("Login Success");

                              // Reset Screen back to Home
                              navigation.reset({
                                index: 0,
                                routes: [{ name: 'Tab' }],
                              });
                            } else {
                              Alert.alert(
                                "Error",
                                "User did not sign-in with the same email. Please try again with the right email",
                                [
                                  {
                                    text: "Dismiss",
                                    style: "cancel",
                                  },
                                ],
                              );
                              console.error("invalid email");

                              auth()
                                .signOut()
                                .then()
                                .catch(error => {
                                  alert(error)
                                })

                              await AsyncStorage.removeItem('auth');
                              await AsyncStorage.removeItem('user_data');
                              await AsyncStorage.removeItem('chat');
                              await AsyncStorage.removeItem('fcmtoken');
                            }
                          })
                          .catch(async (error) => {
                            alert("Error: \n\n" + error);
                            console.error("Error:", error);

                            // If firebase did not successfully log in, sign out laravel
                            await AsyncStorage.removeItem('auth');
                            await AsyncStorage.removeItem('user_data');
                            await AsyncStorage.removeItem('chat');
                            await AsyncStorage.removeItem('fcmtoken');

                          })
                      },
                    },
                  ],
                );
                break;
              case "facebook test":

                break;
              case "email and password test":

                break;
              case "apple test":

                break;

              default:
                console.log("provider", providers[0])
                break;
            }
          })
      }
    },
    (err) => console.log(err),
  );
}