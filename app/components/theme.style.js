const colors = {
  primary: "#fff",
  text: "black",
  green: "#66b545"
}

export default {
  colors: colors
}