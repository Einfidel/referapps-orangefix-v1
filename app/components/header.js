import * as React from 'react';
import { useState, useEffect } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, TextInput, ImageBackground, Platform } from 'react-native';
import { StackActions } from '@react-navigation/native';
import Icons from './icons.js';
import FastImage from 'react-native-fast-image';
import Assets from './assets.manager.js';
import Theme from './theme.style.js';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Badge } from 'native-base';
import Endpoints from './api/endpoints.js';
import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect } from '@react-navigation/native';

const styles = {
  borderedButton: {
    padding: 5,
    borderColor: '#fff',
    borderRadius: 5,
    borderWidth: 2,
    // height: 45,
    width: 130,
    margin: 5,
    justifyContent: 'center',
  },
  textBig: {

  },
  marginTitle: {
    ...Platform.select({
      ios: {
        flex: 3,
        justifyContent: 'center',
        marginTop: 15
      },
      android: {
        flex: 3,
        justifyContent: 'center',
        marginTop: 0
      }
    })
  }
}

export default function CustomHeader(props) {
  const navigation = props.navigation.navigation || props.navigation;
  const btnstyle = { marginLeft: 10 };
  const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
  const [allCartItems, setAllCartItems] = useState('')
  const [loading, setloading] = useState(true)
  const [user, setuser] = useState(true)

  const init = async () => {
    setloading(true)
    let ud = await AsyncStorage.getItem("user_data")
    if (ud == null) {
      setuser(ud)
      return
    }
    let _userdata = typeof ud == 'string' ? JSON.parse(ud) : null

    try {
      let formData = new FormData();
      formData.append("user_id", _userdata.data.id);
      formData.append("api_token", token)
      const res = await fetch(Endpoints.show_added + `?include=date,info`, {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      });
      const cartDetail = await res.json();
      const response = cartDetail
      // console.log('user_data api call', ud)
      // console.log('show_added api call', response.data)
      let data = []

      response.data.map((result, index) => {
        data.push({
          data: result,
        })
      })
      setAllCartItems(data)
      setloading(false)

    }
    catch (e) {
      console.log(e)
    }
  }

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  // useEffect(() => {
  //   init()
  // }, [])

  const openDrawer = () => {
    navigation.openDrawer();
  };
  const BackToHome = () => {
    navigation.navigate(props.backscreen || 'MenuTab');//
  };

  // useEffect(()=>{
  //   console.warn('props', props)
  // }, [])

  const navigate = (screen, onBackPress) => {
    navigation.navigate('MyProfile', {
      tab: screen == 'mycard' ? 'ProfileCardTab' : 'ProfileTransactionsTab',
      onBackPress: onBackPress,
    });
  };

  const ButtonLeft = props.menu ? (
    <TouchableOpacity style={btnstyle} onPress={openDrawer}>
      <Icons.Feather name='menu' size={20} style={{ color: '#fff' }} />
    </TouchableOpacity>
  ) : (
    <TouchableOpacity style={btnstyle} onPress={props.onBackPress ? props.onBackPress : BackToHome}>
      <Icons.AntDesign name={props.back ? 'arrowleft' : 'close'} size={20} style={{ color: Theme.colors.text }} />
    </TouchableOpacity>
  );

  if (props.home_header) {
    return (
      <View>
        <View
          style={{
            flexDirection: 'row',
            height: 55,
            padding: 5,
            elevation: 4,
            backgroundColor: Theme.colors.primary,
            top: Platform.OS == 'ios' && props.top ? props.top : 0,
          }}
        >
          <View style={{ flex: 5, justifyContent: 'center' }}>
            <FastImage source={Assets.landing.header_icon}
              style={{ resizeMode: 'contain', width: 80, height: 45 }}
              resizeMode={FastImage.resizeMode.contain} />
          </View>
          <View style={{ flex: 10, justifyContent: 'center', height: 35, marginTop: 5 }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Search Results');
              }}
              style={{
                flexDirection: 'row',
                backgroundColor: '#ECF0F1',
                justifyContent: 'center',
                alignItems: 'center',
                padding: 2,
                borderRadius: 5,
              }}
            >
              <View style={{ flex: 1 }}>
                <Icons.Feather name='search' size={20} style={{ color: '#fff' }} />
              </View>
              <View style={{ flex: 9 }}>
                <TextInput
                  style={{ backgroundColor: '#ECF0F1', fontSize: 16 }}
                  onFocus={() => {
                    navigation.navigate('Search Results');
                  }}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
            <TouchableOpacity
              style={btnstyle}
              onPress={() => {
                navigation.navigate('Cart Screen');
              }}
            >
              <View>
                <Icons.EvilIcons name='cart' size={30} style={{ color: Theme.colors.green }} />
                <Badge style={{ position: 'absolute', top: -4, right: -4, height: 15 }}>
                  <Text style={{ fontSize: 8, textAlign: 'center', color: '#fff', fontWeight: "bold" }}>{allCartItems.length}</Text>
                </Badge>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View >
    );
  }
  else if (props.title == 'Add New Item' || props.title == 'Notifications' || props.title == 'Pinned Items') {
    return (
      <>
        <Header style={{ backgroundColor: '#fff' }}>
          <Body style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Title style={{ color: '#000' }}>{props.title}</Title>
          </Body>
        </Header>
      </>
    )
  }
  else {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>{props.title}</Title>
          </Body>
          <Right>
            <Button>
              <View>
                {props.rightMenu || null}
              </View>
            </Button>
          </Right>
        </Header>
      </Container>

      // <View>
      //   <View style={{flexDirection: 'row', height: 55, padding: 5, backgroundColor: Theme.colors.primary, top: Platform.OS =='ios' && props.top ? props.top : 0}}>
      //     <View style={{flex:1, justifyContent: 'center'}}>
      //       {props.leftPanel ? ButtonLeft : null}
      //     </View>
      //     <View style={styles.marginTitle}>
      //       <Text style={{textAlign:'center', fontSize: 17, color: Theme.colors.text, justifyContent: 'center'}}>{props.title}</Text>
      //     </View>
      //     <View style={{flex:1, justifyContent: 'center'}}>
      //       {props.rightMenu || null}
      //     </View>
      //     <View style={{ flex: 1, justifyContent: 'center' }}>{props.rightMenu || null}</View>
      //   </View>
      // </View>
    );
  }
}
