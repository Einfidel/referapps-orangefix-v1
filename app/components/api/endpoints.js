// const api_url = "";
const env = '127.0.0.1';
// export const API_URL = 'https://stagingdev.referapps.com'; //DEV

//API TOKENS
const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';



// PROD API URL
const api_url = 'https://www.referapps.com';

// STAGING API URL
// const api_url = 'https://stg-server.referapps.com';

const fb_token =
  'EAAEbQEgvZAsQBAG7gLwbUZAr9yUjIciYIRIoZCFsUCgRg6wtJRqjR7aK3EFLHmHBfDzp7ye2a1vPqyLx47XogY0Uqtk7AZCQKyHZCUcvqxMB6QBmZCZCAtcujs3H5R6nKl9b4PFmcAKZB3jpUzL0lgZCMXfnmnBov5yimTO9m8ELtzPD3ZB6TMFzm7xLWdntxHZBdrZCIZAoqhgPepZCJ1vxoyyM11lAgywU62qRe7zP8dNAiofwZDZD';
const google_token =
  'EAAEbQEgvZAsQBAG7gLwbUZAr9yUjIciYIRIoZCFsUCgRg6wtJRqjR7aK3EFLHmHBfDzp7ye2a1vPqyLx47XogY0Uqtk7AZCQKyHZCUcvqxMB6QBmZCZCAtcujs3H5R6nKl9b4PFmcAKZB3jpUzL0lgZCMXfnmnBov5yimTO9m8ELtzPD3ZB6TMFzm7xLWdntxHZBdrZCIZAoqhgPepZCJ1vxoyyM11lAgywU62qRe7zP8dNAiofwZDZD';

//API ENDPOINTS
const register = '/api/auth/register.json';
const login = '/api/auth/login.json';
const fb_login = '/api/auth/fb-login.json';
const google_login = '/api/auth/google-login.json';
const user_data = '/api/users/show.json';
const user_products = '/api/product/user-products.json';
const all_products = '/api/product/all.json';
const featured_products = '/api/product/featured-products.json';
const slider_ads = '/api/image-slider/all.json';
const delete_account = '/api/users/delete.json';

const update_profile = '/api/auth/profile/update.json';
const update_additional_info = '/api/auth/profile/update-additional-information.json'; // unused
const resend_account_verif_code = '/api/security/resend-verification.json';
const verify_account_verif_code = '/api/security/verify.json';
const verify_phone_verif_code = '/api/security/verify-contact-number.json';
const update_email_contact = '/api/auth/profile/update-email-contact.json';
const pinned = '/api/pin/all.json';

//ADDRESS
const getAllProvince = '/api/account/address-book/ph-province.json';
const getAllCity = '/api/account/address-book/ph-city.json';
const getAllBarangay = '/api/account/address-book/ph-barangay.json';
const addAddress = '/api/account/address-book/add-address.json';
const editAddress = '/api/account/address-book/update-address.json';

//Sell Products
const main_categories = '/api/product/main-category.json';
const sub_categories = '/api/product/main-sub-category.json';
const specific_categories = '/api/product/sub-specific-category.json';
const category_attributes = '/api/product/category-attributes.json';
const logistics = '/api/product/logistics.json';
const partner_logistics = '/api/product/partner-logistics.json';
const create_product = '/api/product/create.json';
const create_service = '/api/service/create.json';

// Fetch user services
const user_services = '/api/product/user-products.json';
const update_service = '/api/service/update.json';
const update_product = '/api/product/update.json';
const delete_service = '/api/product/delete.json';
const product_detail = '/api/product/show.json';
const edit_shop = '/api/account/shop-setting/update.json';
const add_social_link = '/api/auth/profile/add-social-link.json';
const remove_social_link = '/api/auth/profile/remove-social-link.json';
const add_product_image = '/api/product/add-image.json';
const remove_product_image = '/api/product/remove-image.json';

//Add to cart
const add_cart = '/api/cart/add.json';
const show_cart = '/api/cart/all.json';
const delete_cart = '/api/cart/remove.json';
const reset_cart = '/api/cart/reset.json';
const update_cart_single = '/api/cart/single-update.json';
const update_cart_all = '/api/cart/update.json';

//Chat
const create_thread = '/api/account/messages/create.json';
const send_message = '/api/account/messages/send.json';
const thread_list = '/api/account/messages/index.json';
const thread_messages = '/api/account/messages/conversation.json';
const count_messages = '/api/account/messages/counter.json';

//Checkout
const checkout_paypal = '/api/paypal/checkout.json';
const checkout_xendit = '/api/xendit/checkout.json';

//My Address
const specific_myaddress = '/api/account/address-book/index.json';
const delete_address = '/api/account/address-book/delete-address.json';

//Deliveries
const to_ship_deliveries = '/api/account/deliveries/to-ship.json';
const to_ship_processing_deliveries = '/api/account/deliveries/to-ship-processing.json';
const update_to_ship_lbc = '/api/account/deliveries/ship-with-lbc.json';
const update_to_ship_rcd = '/api/account/deliveries/ship-with-rcd.json';
const in_transit_deliveries = '/api/account/deliveries/in-transit.json';
const delivered_deliveries = '/api/account/deliveries/delivered.json';
const update_to_ship = '/api/account/deliveries/update-to-ship.json';
const order_info = '/api/account/deliveries/order-info.json';

//Forgot Password
const auth_search_account = '/api/auth/search-account.json';
const auth_verify = '/api/auth/search-account.json';
const auth_newpassword = '/api/auth/new-password.json';

const auth_two_fac_auth = '/api/security/two-factor-authentication.json';

//Transactions
const transaction_history = "/api/account/transaction/index.json?include=date,info";
const transaction_seller_history = "/api/account/transaction/orders.json?include=date,info";
const transaction_search_order = "/api/account/transaction/search.json?include=date,info";
const transaction_receive = "/api/account/transaction/receive.json?include=date,info";
const transaction_cancel = "/api/account/transaction/cancel-request.json?include=date,info";
const transaction_return = "/api/account/transaction/return-request.json?include=date,info";
const transaction_details = "/api/account/my-purchases/product-orders/transaction-details.json?include=date,info";

//Notifications
const notification_list = "/api/account/notifications/index.json";
const notification_view = "/api/account/notifications/view.json";

// Income
const income_ledger = "/api/account/income/ledger.json";
const check_request = "/api/account/income/check-request.json";
const submit_check_request = "/api/account/income/submit-check-request.json";
const resend_check_request_code = "/api/account/income/resend-authentication-code.json";
const verify_check_request_code = "/api/account/income/authenticate-check-request.json";

// Vouchers
const available_vouchers = "/api/account/my-purchases/service-vouchers/index.json";
const unavailable_vouchers = "/api/account/my-purchases/service-vouchers/unavailable.json";
const use_voucher = "/api/account/my-purchases/service-vouchers/use-service.json";
const recurring_services = "/api/service/recurring.json"

const auth_google_send_otp = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/sendVerificationCode?key=AIzaSyCE5Qpo-X6YmbYgm9Cd3e4h2MTX2yPXMhU';
const auth_validate_otp = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPhoneNumber?key=AIzaSyCE5Qpo-X6YmbYgm9Cd3e4h2MTX2yPXMhU';

export default {
  register: api_url + register,
  login: api_url + login,
  fb_login: api_url + fb_login,
  google_login: api_url + google_login,
  user_data: api_url + user_data,
  user_products: api_url + user_products,
  update_profile: api_url + update_profile,
  update_additional_info: api_url + update_additional_info,
  resend_account_verif_code: api_url + resend_account_verif_code,
  verify_account_verif_code: api_url + verify_account_verif_code,
  verify_phone_verif_code: api_url + verify_phone_verif_code,
  update_email_contact: api_url + update_email_contact,
  pinned: api_url + pinned,
  main_categories: api_url + main_categories,
  sub_categories: api_url + sub_categories,
  specific_categories: api_url + specific_categories,
  category_attributes: api_url + category_attributes,
  logistics: api_url + logistics,
  partner_logistics: api_url + partner_logistics,
  create_product: api_url + create_product,
  create_service: api_url + create_service,
  user_services: api_url + user_services,
  update_service: api_url + update_service,
  delete_service: api_url + delete_service,
  ads: api_url + slider_ads,
  delete_account: api_url + delete_account,
  all_products: api_url + all_products,
  featured_products: api_url + featured_products,
  edit_shop: api_url + edit_shop,
  product_detail: api_url + product_detail,
  token: token,
  update_product: api_url + update_product,
  add_to_cart: api_url + add_cart,
  show_added: api_url + show_cart,
  remove_cart: api_url + delete_cart,
  reset_cart: api_url + reset_cart,
  update_cart_single: api_url + update_cart_single,
  checkout_paypal: api_url + checkout_paypal,
  checkout_xendit: api_url + checkout_xendit,
  update_cart_all: api_url + update_cart_all,
  create_thread: api_url + create_thread,
  send_message: api_url + send_message,
  thread_list: api_url + thread_list,
  thread_messages: api_url + thread_messages,
  count_messages: api_url + count_messages,
  add_social_link: api_url + add_social_link,
  remove_social_link: api_url + remove_social_link,
  add_product_image: api_url + add_product_image,
  remove_product_image: api_url + remove_product_image,
  getAllProvince: api_url + getAllProvince,
  getAllCity: api_url + getAllCity,
  getAllBarangay: api_url + getAllBarangay,
  addAddress: api_url + addAddress,
  editAddress: api_url + editAddress,

  to_ship_deliveries: api_url + to_ship_deliveries,
  to_ship_processing_deliveries: api_url + to_ship_processing_deliveries,
  in_transit_deliveries: api_url + in_transit_deliveries,
  delivered_deliveries: api_url + delivered_deliveries,
  update_to_ship: api_url + update_to_ship,
  update_to_ship_lbc: api_url + update_to_ship_lbc,
  update_to_ship_rcd: api_url + update_to_ship_rcd,
  order_info: api_url + order_info,

  auth_search_account: api_url + auth_search_account,
  auth_verify: api_url + auth_verify,
  auth_newpassword: api_url + auth_newpassword,
  auth_google_send_otp,
  auth_validate_otp,

  auth_two_fac_auth,

  transaction_history: api_url + transaction_history,
  transaction_seller_history: api_url + transaction_seller_history,
  transaction_search_order: api_url + transaction_search_order,
  transaction_receive: api_url + transaction_receive,
  transaction_cancel: api_url + transaction_cancel,
  transaction_return: api_url + transaction_return,
  transaction_details: api_url + transaction_details,

  specific_address: api_url + specific_myaddress,
  delete_specific_address: api_url + delete_address,

  notification_list: api_url + notification_list,
  notification_view: api_url + notification_view,

  income_ledger: api_url + income_ledger,
  check_request: api_url + check_request,
  submit_check_request: api_url + submit_check_request,
  resend_check_request_code: api_url + resend_check_request_code,
  verify_check_request_code: api_url + verify_check_request_code,

  available_vouchers: api_url + available_vouchers,
  unavailable_vouchers: api_url + unavailable_vouchers,
  use_voucher: api_url + use_voucher,
  recurring_services: api_url + recurring_services,
};
