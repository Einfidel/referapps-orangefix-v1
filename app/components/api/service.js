import Endpoints from './endpoints';
import { cond } from 'react-native-reanimated';

const token = 'base64:V5dwuHfNFRImBBwzUV0w0LLBiIpNKhPo060vXl8Czoo=';
const fb_token =
  'EAAEbQEgvZAsQBAG7gLwbUZAr9yUjIciYIRIoZCFsUCgRg6wtJRqjR7aK3EFLHmHBfDzp7ye2a1vPqyLx47XogY0Uqtk7AZCQKyHZCUcvqxMB6QBmZCZCAtcujs3H5R6nKl9b4PFmcAKZB3jpUzL0lgZCMXfnmnBov5yimTO9m8ELtzPD3ZB6TMFzm7xLWdntxHZBdrZCIZAoqhgPepZCJ1vxoyyM11lAgywU62qRe7zP8dNAiofwZDZD';
const google_token =
  'EAAEbQEgvZAsQBAG7gLwbUZAr9yUjIciYIRIoZCFsUCgRg6wtJRqjR7aK3EFLHmHBfDzp7ye2a1vPqyLx47XogY0Uqtk7AZCQKyHZCUcvqxMB6QBmZCZCAtcujs3H5R6nKl9b4PFmcAKZB3jpUzL0lgZCMXfnmnBov5yimTO9m8ELtzPD3ZB6TMFzm7xLWdntxHZBdrZCIZAoqhgPepZCJ1vxoyyM11lAgywU62qRe7zP8dNAiofwZDZD';

export const getSliderAds = async (callback, catcher) => {
  try {
    const response = await fetch(Endpoints.ads + `?api_token=${token}`);
    const responseJson = await response.json();
    // console.log("ADS", responseJson)
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const getProducts = async (page, per, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('page', page || 1);
    formData.append('per_page', per);
    formData.append('include', 'info');
    formData.append('api_token', token);
    const data = await fetch(
      Endpoints.all_products + `?include=info&api_token=${token}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const getAllProvince = async () => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    const data = await fetch(
      Endpoints.getAllProvince + `?include=date,info&api_token=${token}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const response = await data.json();
    return response
  } catch (e) {
    return false
  }
}

export const getAllCity = async (province) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('prov_code', province)
    const data = await fetch(
      Endpoints.getAllCity + `?include=date,info&api_token=${token}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const response = await data.json();
    return response
  } catch (e) {
    return false
  }
}

export const getAllBarangay = async (city) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('city_code', city)
    const data = await fetch(
      Endpoints.getAllBarangay + `?include=date,info&api_token=${token}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const response = await data.json();
    return response
  } catch (e) {
    return false
  }
}

export const getPopularProducts = async (sort, page, callback, catcher) => {
  try {
    let formData = new FormData();
    // formData.append('sort_by', sort || 'popular');
    formData.append('sort_by', sort || 'popularity');
    formData.append('page', page || 1);
    formData.append('per_page', '6');
    formData.append('include', 'info');
    formData.append('api_token', token);
    const data = await fetch(
      Endpoints.all_products + `?include=info&api_token=${token}`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};


export const getFeaturedProducts = async (page, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('page', page || 1);
    formData.append('per_page', '6');
    formData.append('include', 'info');
    formData.append('api_token', token);
    const data = await fetch(Endpoints.featured_products, {
      method: 'POST',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData,
    });
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const getCategories = async (callback, catcher) => {
  try {
    const response = await fetch(
      Endpoints.main_categories + '?api_token=' + token + '&page=1&per_page=0',
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const getAccountCart = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', id);
    formData.append('api_token', token);
    console.log("Endpoints.show_cart formData", formData);
    const data = await fetch(
      Endpoints.show_added + `?include=date,info`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    );
    console.log("Endpoints.show_cart data", data);
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const resetAccountCart = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', id);
    formData.append('api_token', token);
    console.log("Endpoints.show_cart formData", formData);
    const data = await fetch(
      Endpoints.reset_cart + `?include=date,info`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    );
    console.log("Endpoints.show_cart data", data);
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const getUserData = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', id);
    // formData.append('type', 'count');
    // formData.append('include', 'info');
    formData.append('api_token', token);
    const data = await fetch(
      Endpoints.user_data + '?include=info,date',
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    );
    // console.log("Endpoints.user_data", data);
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const getUserServices = async (id, callback, catcher) => {
  try {
    const data = await fetch(
      Endpoints.user_services +
      `?user_id=${id}&include=info,date&page=1&per_page=10&type=service&api_token=${token}`,
      {
        method: 'get',
        headers: { 'Content-Type': 'multipart/form-data' },
      },
    );
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const getAllPinned = async (id, callback, catcher) => {
  console.log(id, 'id ko to ')
  // try{
  //   const data = await fetch(
  //     Endpoints.pinned + `?user_id=${id}&include=info,date&page=1&per_page=2&api_token=${token}`
  //   );
  //   // console.log(data, 'dataaas')
  //   const response = await data.json()
  //   // callback(response)
  //   console.log(response, 'resss')
  // }
  // catch(e){
  //   console.log(e)
  // }
}

export const login = async (req, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('username', req.uid);
    formData.append('password', req.pwd);
    formData.append('device_id', '');
    formData.append('device_model', '');
    if (req.deviceToken)
      formData.append('device_token', req.deviceToken);
    formData.append('api_token', token);
    console.log(Endpoints.login)
    console.log(formData)
    const data = await fetch(
      Endpoints.login + '?include=info,date',
      {
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      });
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const deleteAccount = async (req, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', req.user_id);
    formData.append('verified', req.verified);
    formData.append('api_token', token);

    const data = await fetch(
      Endpoints.delete_account,
      {
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      });

    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const editProfile = async (req, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', req.id || req.user.id);
    formData.append('fname', req.fname);
    formData.append('lname', req.lname);
    formData.append('username', req.username);
    formData.append('birthdate', req.birthdate);
    formData.append('contact_number', req.contact_number);
    formData.append('email', req.email);
    formData.append('password', req.password);
    formData.append('api_token', token);
    const data = await fetch(Endpoints.update_profile, {
      method: 'post',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData,
    });
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const verifiedUpdateProfile = async (req, callback, catcher) => {
  try {
    // let localUri = !req.image.toString().includes('.jpg') ? req.filePath : req.image;
    // let filename = localUri.uri !== undefined ? '' : localUri.split('/').pop();    
    // let match = /\.(\w+)$/.exec(filename);
    // let type = match ? `image/${match[1]}` : `image`;

    let uri = req.file.isCaptured ? req.file.uri : req.file.fullpath;

    let formData = new FormData();
    formData.append('user_id', req.id || req.user.id);
    formData.append('fname', req.fname);
    formData.append('lname', req.lname);
    formData.append('verified', 'yes');
    formData.append('username', req.username);
    formData.append('birthdate', req.birthdate);
    formData.append('contact_number', req.contact_number);
    formData.append('email', req.email);
    formData.append('password', req.password);
    formData.append('api_token', token);

    if (req.imageUpdated) {
      formData.append('file', {
        uri: uri,
        name: filename + 'Image.' + req.file.type.split('/')[1],
        type: req.file.type,
      });
      //   name: filename,
      //   type: 'image/jpeg',
      // });
    }

    const data = await fetch(Endpoints.update_profile, {
      method: 'post',
      headers: { 'Content-Type': 'multipart/form-data' },
      body: formData,
    });
    const response = await data.json();
    callback(response);
  } catch (e) {
    catcher(e);
  }
};

export const updateShop = async (req, callback, catcher) => {
  try {

    var requestData = req

    // console.log('requestData.cover', requestData.cover);
    // if (requestData.cover.data == undefined) {
    //   requestData.cover.data = null;
    // }
    // console.log('requestData.profile', requestData.profile);
    // if (requestData.profile.data == undefined) {
    //   requestData.profile.data = null;
    // }
    let formData = new FormData();
    formData.append('user_id', requestData.id || requestData.user.id);
    formData.append('shop_name', requestData.shop_name);
    formData.append('description', requestData.description);
    formData.append('password', requestData.password);
    formData.append('api_token', token);
    let coverURI = requestData.cover.isCaptured ? requestData.cover.uri : requestData.cover.fullpath;
    if (coverURI) {
      formData.append('cover_file', {
        uri: coverURI,
        name: requestData.cover.fileName,
        type: 'image/jpeg',
      });
    }
    let profileURI = requestData.profile.isCaptured ? requestData.profile.uri : requestData.profile.fullpath;
    if (profileURI) {
      formData.append('file', {
        uri: profileURI,
        name: requestData.profile.fileName,
        type: 'image/jpeg',
      });
    }
    console.log(JSON.stringify(formData));
    // if (requestData.cover?.uri) {
    //   // let localUri = !requestData.cover.uri.toString().includes(".jpg") ? requestData.cover.filePath : requestData.cover.image;
    //   // let filename = localUri.split('/').pop();
    //   // let match = /\.(\w+)$/.exec(filename);
    //   // let type = match ? `image/${match[1]}` : `image`;
    //   formData.append('cover_file', {
    //     uri: requestData.cover.uri,
    //     name: requestData.cover.image.fileName,
    //     type: 'image/jpeg',
    //   });
    // }

    // if (requestData.profile?.uri) {
    //   // let localUri = !requestData.profile.uri.toString().includes(".jpg") ? requestData.profile.filePath : requestData.profile.image;
    //   // let filename = localUri.split('/').pop();
    //   // let match = /\.(\w+)$/.exec(filename);
    //   // let type = match ? `image/${match[1]}` : `image`;
    //   formData.append('file', {
    //     uri: requestData.profile.uri,
    //     name: requestData.profile.image.fileName,
    //     type: 'image/jpeg',
    //   });
    // }

    console.log('updateShop 1', Endpoints.edit_shop);
    const data = await fetch(Endpoints.edit_shop + "?include=date,info", {
      method: 'POST',
      headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
      body: formData,
    });
    const response = await data.json();
    callback(response);
  } catch (e) {
    console.log("updateShop error:", e)
    catcher(e);
  }
};

export const getAccountServices = async (data, callback, catcher) => {
  try {
    const service = await fetch(
      Endpoints.user_products +
      '?user_id=' +
      data.user_id +
      '&include=info,date&page=1&per_page=100&type=service&view_type=' +
      data.viewType +
      '&sort_by=' +
      data.sort_by +
      '&api_token=' +
      token,
    );
    const serviceJson = await service.json();
    let allService = [];
    for (let i = 0; i < serviceJson.data.length; i++) {
      allService.push(serviceJson.data[i]);
    }
    callback(allService);
  } catch (error) {
    catcher(error);
  }
};

export const getAccountProductsAndServices = async (data, callback, catcher) => {
  try {
    // console.log("search data.sort_by", data.sort_by)
    console.log("Endpoint", Endpoints.user_products +
      '?user_id=' +
      data.user_id +
      '&include=info,date&page=1&per_page=100&view_type=' +
      data.viewType +
      '&sort_by=' +
      data.sort_by +
      '&api_token=' +
      token)

    const service = await fetch(
      Endpoints.user_products +
      '?user_id=' +
      data.user_id +
      '&include=info,date&page=1&per_page=100&view_type=' +
      data.viewType +
      '&sort_by=' +
      data.sort_by +
      '&api_token=' +
      token,
    );
    const serviceJson = await service.json();
    let allProducts = [];
    for (let i = 0; i < serviceJson.data.length; i++) {
      allProducts.push(serviceJson.data[i]);
    }
    callback(allProducts);
  } catch (error) {
    catcher(error);
  }
};

export const getAccountProducts = async (data, callback, catcher) => {
  try {
    // console.log("search data.sort_by", data.sort_by)
    // console.log("search data", data)
    // console.log(Endpoints.user_products +
    //   '?user_id=' +
    //   data.user_id +
    //   '&include=info,date&page=1&per_page=100&type=product&view_type=' +
    //   data.viewType +
    //   '&sort_by=' +
    //   data.sort_by +
    //   '&api_token=')

    const service = await fetch(
      Endpoints.user_products +
      '?user_id=' +
      data.user_id +
      '&include=info,date&page=1&per_page=100&type=product&view_type=' +
      data.viewType +
      '&sort_by=' +
      data.sort_by +
      '&api_token=' +
      token,
    );

    const serviceJson = await service.json();
    let allProducts = [];
    for (let i = 0; i < serviceJson.data.length; i++) {
      allProducts.push(serviceJson.data[i]);
    }


    callback(allProducts);
  } catch (error) {
    catcher(error);
  }
};

// export const getAccountProducts = async (id, page, viewType, sortBy, callback, catcher) => {
//   try {
//     const service = await fetch(
//       Endpoints.user_products +
//       '?user_id=' +
//       id +
//       '&include=info,date&page=' +
//       page +
//       '&per_page=10&type=products&view_type=' +
//       viewType +
//       '&sort_by=' +
//       sortBy +
//       '&api_token=' +
//       token,
//     );
//     const serviceJson = await service.json();
//     // let allService = [];
//     // for (let i = 0; i < serviceJson.data.length; i++) {
//     //   allService.push(serviceJson.data[i]);
//     // }
//     callback(serviceJson);
//   } catch (error) {
//     catcher(error);
//   }
// };

export const getProductDetails = async (id, callback, catcher) => {
  try {
    const response = await fetch(
      Endpoints.product_detail +
      `?id=${id}&include=info,date&api_token=${token}`,
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const updateAccountService = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('user_id', data.uid);
    formData.append('product_id', data.pid);
    formData.append('country_code', data.ccode);
    formData.append('product_name', data.service.productName);
    formData.append('description', data.service.description);
    formData.append('category_id', data.cid);
    // formData.append('attribute', JSON.stringify(data.attributes));
    // `[{"attribute":"Model","value":"10","required":"yes","category_id":15},{"attribute":"Storage Capacity","value":"20","required":"yes","category_id":15}]`
    formData.append('is_recurring', data.service.isRecurring);
    formData.append('price', data.service.price);
    // formData.append('referrer_commission', data.service.referrer_commission);
    // data.images.map((image, i) => {
    //   let uri = image.isCaptured ? image.uri : image.fullpath;
    //   formData.append(`file[${i + 1}]`, {
    //     uri: uri,
    //     name: data.name + 'Image.jpg',
    //     type: 'image/jpeg',
    //   });
    // });
    // console.log('Body', formData);
    const response = await fetch(
      Endpoints.update_service + '?include=info,date',
      {
        method: 'POST',
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};

export const deleteAccountService = async (uid, pid, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', uid);
    formData.append('product_id', pid);
    formData.append('api_token', token);
    const removeItem = await fetch(Endpoints.delete_service, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    });
    const response = await removeItem.json();
    callback(response);
  } catch (error) {
    catcher(error);
  }
};

export const deleteProduct = async (uid, pid, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', uid);
    formData.append('product_id', pid);
    formData.append('api_token', token);
    const removeItem = await fetch(Endpoints.delete_service, {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    });
    const response = await removeItem.json();
    callback(response);
  } catch (error) {
    catcher(error);
  }
};

export const createService = async (createServiceData, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('user_id', createServiceData.user.data.id);
    formData.append('country_code', createServiceData.user.data.country_code);
    formData.append('product_name', createServiceData.name);
    formData.append('description', createServiceData.description);
    formData.append('fine_prints', createServiceData.fine_prints);
    formData.append('contact_details', createServiceData.contact_details);
    formData.append('expiration_date', createServiceData.expiration_date);

    if (createServiceData.commissionIsFixed) {
      formData.append('commission_type', 'fix');
      formData.append('referrer_commission', createServiceData.referrerFixedCommission);
    } else {
      formData.append('commission_type', 'percent');
      formData.append('referrer_commission', createServiceData.referrerPercentageCommission);
    }

    formData.append('category_id', createServiceData.category_id);
    formData.append('attribute', JSON.stringify(createServiceData.to_send_attributes));

    if (createServiceData.hasDiscount) {
      formData.append('with_discount', "yes");
      formData.append('price', createServiceData.discountedPrice);
      formData.append('discount_amount', createServiceData.referrerFixedCommission);
      formData.append('discount_percentage', createServiceData.discountPercentageAmmount);
    } else {
      formData.append('with_discount', "no");
      formData.append('price', createServiceData.price);
    }

    formData.append('is_recurring', createServiceData.is_recurring);

    if (createServiceData.variationOptions.length > 0) {
      formData.append('variation', JSON.stringify(createServiceData.variationOptions));
    } else {
      formData.append('variation', null);
    }

    createServiceData.images.map((image, i) => {
      let uri = image.isCaptured ? image.uri : image.fullpath;
      formData.append(`file[${i + 1}]`, {
        uri: uri,
        name: createServiceData.name + 'Image.' + image.type.split('/')[1],
        type: image.type,
      });
      //   name: createServiceData.name + 'Image.jpg',
      //   type: 'image/jpeg',
      // });
      // console.log(uri);
    });

    console.log('formData', formData);

    const result = await fetch(
      Endpoints.create_service + '?include=info,date',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      },
    );
    console.log('RESULT', result);
    const response = await result.json();
    callback(response);
  } catch (error) {
    catcher(error);
  }
};

export const updateService = async (updateServiceData, callback, catcher) => {
  // console.warn('forms', updateServiceData);
  // console.log("updateServiceData.to_send_attributes", updateServiceData.to_send_attributes);
  // return;

  try {
    console.log('updateProduct 1');
    // console.log("updateServiceData", updateServiceData)

    let formData = new FormData();
    formData.append('user_id', updateServiceData.user.data.id);
    formData.append('product_id', updateServiceData.pid);
    formData.append('country_code', updateServiceData.country_code);
    formData.append('product_name', updateServiceData.name);
    formData.append('description', updateServiceData.description);
    formData.append('fine_prints', updateServiceData.fine_prints);
    formData.append('contact_details', updateServiceData.contact_details);
    formData.append('expiration_date', updateServiceData.expiration_date);

    if (updateServiceData.commissionIsFixed) {
      formData.append('commission_type', 'fix');
      formData.append('referrer_commission', updateServiceData.referrerFixedCommission);
    } else {
      formData.append('commission_type', 'percent');
      formData.append('referrer_commission', updateServiceData.referrerPercentageCommission);
    }

    formData.append('category_id', updateServiceData.category_id);
    formData.append('attribute', JSON.stringify(updateServiceData.to_send_attributes));

    if (updateServiceData.hasDiscount) {
      formData.append('with_discount', "yes");
      formData.append('price', updateServiceData.discountedPrice);
      formData.append('discount_amount', updateServiceData.referrerFixedCommission);
      formData.append('discount_percentage', updateServiceData.discountPercentageAmmount);
    } else {
      formData.append('with_discount', "no");
      formData.append('price', updateServiceData.price);
    }

    formData.append('is_recurring', createServiceData.is_recurring);

    if (updateServiceData.hasVariation == "yes") {
      formData.append('variation', JSON.stringify(updateServiceData.variationOptions));
    } else {
      formData.append('variation', null);
    }

    formData.append('api_token', token);

    updateServiceData.images.map((image, i) => {
      if (image.fullpath != null || image.fullpath != '') {
        formData.append(`file[${i + 1}]`, {
          uri: image.fullpath,
          name: updateServiceData.name + 'Image.jpg',
          type: 'image/jpeg',
        });
      }
    });

    // console.log('updateProduct 2');
    // console.warn('Form data', JSON.stringify(formData));
    console.log('updateServiceData', updateServiceData);
    console.warn('Form data', formData);

    // console.log('Endpoints.update_product', Endpoints.update_product + "?include=info,date");

    return;

    const response = await fetch(
      // Endpoints.create_product,
      Endpoints.update_service + "?include=info,date",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        // body: JSON.stringify(formData),
        body: formData,
      },
    );
    // console.log('updateProduct 3');
    // console.log('Update Product Response', response);

    const responseJson = await response.json();
    callback(responseJson);
  } catch (error) {
    console.log('Update  Product Error', error);
    catcher("Update Product Error", error);
  }
};

export const createProduct = async (createProductData, callback, catcher) => {
  try {
    console.log("createProductData", createProductData)

    let formData = new FormData();
    formData.append('user_id', createProductData.userDataId);
    formData.append('product_name', createProductData.name);
    if (createProductData.commissionIsFixed) {
      formData.append('commission_type', 'fix');
      formData.append('referrer_commission', createProductData.referrerFixedCommission);
    } else {
      formData.append('commission_type', 'percent');
      formData.append('referrer_commission', createProductData.referrerPercentageCommission);
    }
    formData.append('attribute', JSON.stringify(createProductData.to_send_attributes));
    formData.append('category_id', createProductData.category_id);
    formData.append('description', createProductData.description);
    formData.append('condition', createProductData.condition);
    formData.append('warranty_details', createProductData.warranty);
    formData.append('country_code', createProductData.country_code);
    formData.append('is_recurring', 'no');
    if (createProductData.variationOptions.length > 0) {
      formData.append('with_variation', createProductData.hasVariation ? "yes" : "no");
      formData.append('variation', JSON.stringify(createProductData.variationOptions));
    } else {
      formData.append('with_variation', createProductData.hasVariation ? "yes" : "no");
      formData.append('variation', null);
    }
    if (createProductData.hasDiscount) {
      formData.append('with_discount', "yes");
      formData.append('price', createProductData.discountedPrice);
      formData.append('discount_amount', createProductData.discountPercentageAmmount);
      formData.append('discount_percentage', createProductData.discountPercentageAmmount);
    } else {
      formData.append('with_discount', "no");
      formData.append('price', createProductData.price);
    }
    formData.append('stock', createProductData.stock);
    formData.append('weight', createProductData.weight);
    formData.append('width', createProductData.to_send_packagingSize[0].width);
    formData.append('length', createProductData.to_send_packagingSize[1].length);
    formData.append('height', createProductData.to_send_packagingSize[2].height);
    if (!createProductData.freeShipping) {
      formData.append(
        'delivery_options',
        JSON.stringify(createProductData.shippingFeeOptions),
      );

      console.log("JSON.stringify(createProductData.shippingFeeOptions)", JSON.stringify(createProductData.shippingFeeOptions))
    } else {
      formData.append(
        'delivery_options',
        "[]"
      );
      console.log("delivery_options []")
    }
    formData.append('is_pickup', createProductData.forPickup === "true" ? "yes" : "no");
    formData.append('is_own_packaging', createProductData.ownPackaging);

    formData.append('api_token', token);

    console.log('formData', formData);
    let res = {
      errors: true
    }
    // callback(res);
    // return

    createProductData.images.map((image, i) => {
      let uri = image.isCaptured ? image.uri : image.fullpath;
      formData.append(`file[${i + 1}]`, {
        uri: uri,
        name: createProductData.name + 'Image.' + image.type.split('/')[1],
        type: image.type,
      });
    });

    console.log('Create Product Form data', formData);
    console.log('Create Product Endpoints', Endpoints.create_product + "?include=info,date");

    const response = await fetch(
      Endpoints.create_product + "?include=info,date",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        body: formData,
      }
    );
    console.log('Create response', response);
    console.log('Create response2', response._bodyBlob._data.__collector);
    console.log('Create response2', response._bodyInit._data.__collector);

    const responseJson = await response.json();
    console.log('Create responseJson', responseJson);
    callback(responseJson);
  } catch (error) {
    console.log('Create  Product Error', error);
    catcher("Create Product Error", error);
  }
};

export const updateProduct = async (updateProductData, callback, catcher) => {
  // console.warn('forms', updateProductData);
  console.log("updateProductData.to_send_attributes", updateProductData.to_send_attributes);
  // return;

  try {
    console.log('updateProduct 1');
    // console.log("updateProductData", updateProductData)

    let formData = new FormData();
    formData.append('product_id', updateProductData.pid);
    formData.append('product_name', updateProductData.name);
    if (updateProductData.commissionIsFixed) {
      formData.append('commission_type', 'fix');
      formData.append('referrer_commission', updateProductData.referrerFixedCommission);
    } else {
      formData.append('commission_type', 'percent');
      formData.append('referrer_commission', updateProductData.referrerPercentageCommission);
    }
    formData.append('attribute', JSON.stringify(updateProductData.to_send_attributes));
    formData.append('category_id', updateProductData.category_id);
    formData.append('description', updateProductData.description);
    formData.append('condition', updateProductData.condition);
    formData.append('warranty_details', updateProductData.warranty);
    formData.append('country_code', updateProductData.country_code);
    formData.append('is_recurring', 'no');
    formData.append('with_variation', updateProductData.hasVariation ? "yes" : "no");
    if (updateProductData.hasVariation) {
      formData.append('variation', JSON.stringify(updateProductData.variationOptions));
    } else {
      formData.append('variation', null);
    }
    if (updateProductData.hasDiscount) {
      formData.append('with_discount', "yes");
      formData.append('price', updateProductData.discountedPrice);
      formData.append('discount_amount', updateProductData.discountPercentageAmmount);
      formData.append('discount_percentage', updateProductData.discountPercentageAmmount);
    } else {
      formData.append('with_discount', "no");
      formData.append('price', updateProductData.price);
    }
    formData.append('stock', updateProductData.stock);
    formData.append('weight', updateProductData.weight);
    formData.append('width', updateProductData.to_send_packagingSize[0].width);
    formData.append('length', updateProductData.to_send_packagingSize[1].length);
    formData.append('height', updateProductData.to_send_packagingSize[2].height)
    if (!updateProductData.freeShipping) {
      formData.append(
        'delivery_options',
        JSON.stringify(updateProductData.shippingFeeOptions),
      );
    } else {
      formData.append(
        'delivery_options',
        "[]"
      );
    }
    formData.append('is_pickup', updateProductData.forPickup === "true" ? "yes" : "no");
    formData.append('is_own_packaging', updateProductData.ownPackaging);

    formData.append('api_token', token);

    updateProductData.images.map((image, i) => {
      if (image.fullpath != null || image.fullpath != '') {
        formData.append(`file[${i + 1}]`, {
          uri: image.fullpath,
          name: updateProductData.name + 'Image.jpg',
          type: 'image/jpeg',
        });
      }
    });

    // formData.append(`file[1]`, '');

    // console.log('updateProduct 2');
    // console.warn('Form data', JSON.stringify(formData));
    console.log('updateProductData', updateProductData);
    console.warn('Form data', formData);

    // console.log('Endpoints.update_product', Endpoints.update_product + "?include=info,date");

    const response = await fetch(
      // Endpoints.create_product,
      Endpoints.update_product + "?include=info,date",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        // body: JSON.stringify(formData),
        body: formData,
      },
    );
    // console.log('updateProduct 3');
    // console.log('Update Product Response', response);

    const responseJson = await response.json();
    callback(responseJson);
  } catch (error) {
    console.log('Update  Product Error', error);
    catcher("Update Product Error", error);
  }
};

export const getMainCategories = async (callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('per_page', 20);
    const response = await fetch(Endpoints.main_categories, {
      method: 'POST',
      body: formData,
    });
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};

export const getSubCategory = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('category_id', id);
    formData.append('api_token', token);
    const sub = await fetch(Endpoints.sub_categories, {
      method: 'POST',
      body: formData,
    });
    const subCategories = await sub.json();
    const specific = await fetch(Endpoints.specific_categories, {
      method: 'POST',
      body: formData,
    });
    const specifiCategories = await specific.json();
    const attribs = await fetch(Endpoints.category_attributes, {
      method: 'POST',
      body: formData,
    });
    const attributes = await attribs.json();
    callback({
      sub: subCategories,
      specific: specifiCategories,
      attributes: attributes,
    });
  } catch (err) {
    catcher(err);
  }
};

export const getSpecificCategory = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('category_id', id);
    const response = await fetch(Endpoints.specific_categories, {
      method: 'POST',
      body: formData,
    });
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};

export const getCategoryAttribute = async (id, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('category_id', id);
    const response = await fetch(Endpoints.category_attributes, {
      method: 'POST',
      body: formData,
    });
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};

export const getShippingOptions = async (callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    const response = await fetch(Endpoints.logistics, {
      method: 'POST',
      body: formData,
    });
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};

export const getPartnerLogistics = async (callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    const response = await fetch(Endpoints.partner_logistics, {
      method: 'POST',
      body: formData,
    });
    const responseJson = await response.json();
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
};


export const addSocialLink = async (type, user_id, link) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('link', link);
    formData.append('user_id', user_id);
    formData.append('type', type);
    const response = await fetch(Endpoints.add_social_link, {
      method: 'POST',
      body: formData
    });
    const responseJson = await response.json();
    return responseJson
  } catch (err) {
    return false
  }
};

export const removeSocialLink = async (user_id, social_id) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('social_id', social_id);
    formData.append('user_id', user_id);
    const response = await fetch(Endpoints.remove_social_link, {
      method: 'POST',
      body: formData
    });
    const responseJson = await response.json();
    return responseJson
  } catch (err) {
    console.log(err)
    return false
  }
};
export const getShopProfile = async (id) => {
  // console.log("getShopProfile is called");
  try {
    let formData = new FormData();
    formData.append('api_token', token)
    formData.append('user_id', id)
    formData.append('include', 'info,user_devices')
    const response = await fetch(Endpoints.user_data, {
      method: 'POST',
      body: formData
    })
    const responseJson = await response.json()
    // console.log("responseJson of getShopProfile", responseJson);
    return responseJson
  } catch (err) {
    console.log(err)
    return false
  }
}

export const getDeliveries = async (data, callback, catcher) => {
  try {
    console.log("getDeliveries start", responseJson)
    let formData = new FormData();
    formData.append('api_token', token)
    formData.append('user_id', data.id)

    console.log("formData", formData)
    let url = ""
    if (data.type == 'ship') url = Endpoints.to_ship_deliveries
    else if (data.type == 'ship+processing') url = Endpoints.to_ship_processing_deliveries
    else if (data.type == 'transit') url = Endpoints.in_transit_deliveries
    else if (data.type == 'delivered') url = Endpoints.delivered_deliveries
    const response = await fetch(
      url + "?include=date,info", {
      // url, {
      method: 'POST',
      body: formData
    })
    const responseJson = await response.json()

    console.log("getDeliveries end", responseJson)
    callback(responseJson);
  } catch (err) {
    catcher(err);
  }
}

export const findAccountByEmail = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token)
    formData.append('email', data.email)

    const response = await fetch(
      Endpoints.auth_search_account + "?include=info,user_devices",
      {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      }
    );

    const responseJson = await response.json();
    console.log('findAccountByEmail 1');

    callback(responseJson)
  } catch (e) {
    catcher(e);
  }
}

export const authVerifyCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token)
    formData.append('email', data.email)
    formData.append('verification_code', code)

    const response = await fetch(
      Endpoints.auth_search_account + "?include=info,user_devices",
      {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      });
    const responseJson = await response.json();
    callback(responseJson)
  } catch (e) {
    catcher(e);
  }
}

export const authNewPassword = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token)
    formData.append('user_id', data.id)
    formData.append('password', data.pwd)
    formData.append('password_confirmation', data.confirmed)

    const response = await fetch(
      Endpoints.auth_newpassword + "?include=info,user_devices",
      {
        method: 'post',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData
      }
    );

    const responseJson = await response.json();
    callback(responseJson)
  } catch (e) {
    catcher(e);
  }
}

export const authGoogleSendOtp = async (token, phone) => {
  try {

    const response = await fetch(Endpoints.auth_google_send_otp, {
      method: 'POST',
      body: JSON.stringify({ phone_number: phone, recaptchaToken: token })
    })
    const responseJson = await response.json()
    return responseJson
  } catch (err) {
    console.log(err)
    return false
  }
}

export const authVerifyOtp = async (session, code) => {
  try {
    console.log("sending...")
    const response = await fetch(Endpoints.auth_validate_otp, {
      method: 'POST',
      body: JSON.stringify({ sessionInfo: session, code: code })
    })
    const responseJson = await response.json()
    return responseJson
  } catch (err) {
    console.log(err)
    return false
  }
}

//my address
export const getSpecificAddress = async (uid, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', uid);
    formData.append('api_token', token);
    const response = await fetch(Endpoints.specific_address + `?include=info`, {
      method: 'POST',
      body: formData,
    })
    const responseJson = await response.json();
    callback(responseJson);
  }
  catch (err) {
    catcher(err);
  }
}

//my address
export const addAddress = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('user_id', data.user_id);
    formData.append('fname', data.fname);
    formData.append('mname', data.mname);
    formData.append('lname', data.lname);
    formData.append('contact_number', data.contact_number);
    formData.append('notes', data.notes);
    formData.append('street_address', data.street_address);
    formData.append('province_code', data.province_code);
    formData.append('city_code', data.city_code);
    formData.append('barangay_code', data.barangay_code);
    formData.append('verified', data.verified);

    const response = await fetch(
      Endpoints.addAddress + `?include=date,info`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const updateAddress = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('api_token', token);
    formData.append('user_id', data.uid);
    formData.append('id', data.pid);
    formData.append('fname', data.fName);
    formData.append('mname', data.mName);
    formData.append('lname', data.lName);
    formData.append('contact_number', data.number);
    formData.append('notes', data.notes);
    formData.append('street_address', data.street);
    formData.append('province_code', data.selectedProvince);
    formData.append('city_code', data.selectedCity);
    formData.append('barangay_code', data.selectedBarangay);
    formData.append('is_shipping', data.is_shipping);
    formData.append('is_billing', data.is_billing);
    formData.append('verified', data.verified);
    // formData.append('is_shipping', "yes");
    // formData.append('is_billing', "yes");
    console.log("formData", formData)
    const res = await fetch(Endpoints.editAddress + `?include=date,info`, {
      method: 'post',
      headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
      body: formData
    });
    const responseJson = await res.json();
    callback(responseJson);
  }
  catch (err) {
    catcher(err);
  }
}

export const deleteAddress = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.uid)
    formData.append('id', data.id)
    formData.append('api_token', token)
    console.log("formData", formData)
    const res = await fetch(Endpoints.delete_specific_address + `?include=date,info`, {
      method: 'post',
      headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
      body: formData
    });
    const responseJson = await res.json();
    callback(responseJson);
  }
  catch (err) {
    catcher(err);
  }
}

export const TwoFactorAuth = async (uid, code) => {
  try {
    let formData = new FormData();
    formData.append('user_id', uid);
    formData.append('api_token', token);
    formData.append('authentication_code', code)
    formData.append('device_id', '')
    formData.append('device', '')
    formData.append('device_model', '')
    formData.append('device_os_version', '')
    formData.append('device_type', '')
    const response = await fetch(Endpoints.auth_two_fac_auth, {
      method: 'POST',
      body: formData,
    })
    const responseJson = await response.json();
    return responseJson.data
  }
  catch (err) {
    return false
  }
}

export const addProductImage = async (data, callback, catcher) => {
  try {
    console.log('addProductImage data.file: ', data.file)
    console.log('addProductImage data.name: ', data.name)
    let uri = data.file.isCaptured ? data.file.uri : data.file.fullpath;
    console.log('addProductImage uri: ', uri)

    let formData = new FormData();
    formData.append('file', {
      uri: uri,
      name: data.name + 'Image.' + data.file.type.split('/')[1],
      type: data.file.type,
    });
    //   name: data.name + 'Image.jpg',
    //   type: 'image/jpeg',
    // });
    formData.append('product_id', data.pid)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.add_product_image + '?include=info,date',
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const checkoutPaypal = async (checkoutData, callback, catcher) => {
  // console.log("checkoutData", checkoutData)
  // console.log("checkoutData.uid", checkoutData?.uid)
  // console.log("checkoutData.checkoutType", checkoutData?.checkoutType)
  // console.log("checkoutData.items[0].pid", checkoutData?.items[0]?.pid)
  // console.log("checkoutData.items[0].amount", checkoutData?.items[0]?.amount)
  // console.log("checkoutData.items[0].variations", checkoutData?.items[0]?.variations)
  // console.log("checkoutData.items[0].variation_1", checkoutData?.items[0]?.variation_1)
  // console.log("checkoutData.items[0].variation_2", checkoutData?.items[0]?.variation_2)

  try {
    let formData = new FormData();
    formData.append("user_id", checkoutData.uid);
    formData.append("item_type", checkoutData.item_type);
    formData.append("type", checkoutData.checkoutType);

    if (checkoutData.checkoutType == "single") {
      if (checkoutData.item_type === 'service') {
        formData.append("product_id", checkoutData.pid);
        if (checkoutData?.variation_id != null) {
          formData.append("variation_id", checkoutData?.variation_id);
        }
      } else {
        formData.append("product_id", checkoutData.pid);
        formData.append("qty", checkoutData.qty);
        formData.append("variations", checkoutData?.variantString);
        formData.append("variation_1", checkoutData?.variant1Index);
        formData.append("variation_2", checkoutData?.variant2Index);
      }
    }

    formData.append("api_token", token);

    console.log("checkoutPaypal formData", formData)
    console.log("Endpoint", Endpoints.checkout_paypal + "?include=date,info")

    const response = await fetch(
      Endpoints.checkout_paypal + "?include=date,info",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json',
        },
        body: formData
      }
    );

    const responseJson = await response.json();
    console.log("RESPONSE", responseJson)
    callback(responseJson);
    // return responseJson;
  } catch (error) {
    catcher(error);
  }
};

export const checkoutXendit = async (checkoutData, callback, catcher) => {
  // console.log("checkoutData", checkoutData)
  // console.log("checkoutData.uid", checkoutData?.uid)
  // console.log("checkoutData.checkoutType", checkoutData?.checkoutType)
  // console.log("checkoutData.items[0].pid", checkoutData?.items[0]?.pid)
  // console.log("checkoutData.items[0].amount", checkoutData?.items[0]?.amount)
  // console.log("checkoutData.items[0].variations", checkoutData?.items[0]?.variations)
  // console.log("checkoutData.items[0].variation_1", checkoutData?.items[0]?.variation_1)
  // console.log("checkoutData.items[0].variation_2", checkoutData?.items[0]?.variation_2)

  try {
    let formData = new FormData();
    formData.append("user_id", checkoutData.uid);
    formData.append("item_type", checkoutData.item_type);
    formData.append("type", checkoutData.checkoutType);

    if (checkoutData.checkoutType == "single") {
      if (checkoutData.item_type === 'service') {
        formData.append("product_id", checkoutData.pid);
        if (checkoutData?.variation_id != null) {
          formData.append("variation_id", checkoutData?.variation_id);
        }
      } else {
        formData.append("product_id", checkoutData.pid);
        formData.append("qty", checkoutData.qty);
        formData.append("variations", checkoutData?.variantString);
        formData.append("variation_1", checkoutData?.variant1Index);
        formData.append("variation_2", checkoutData?.variant2Index);
      }
    }

    formData.append("api_token", token);

    console.log("checkoutPaypal formData", formData)
    console.log("Endpoint", Endpoints.checkout_xendit + "?include=date,info")

    const response = await fetch(
      Endpoints.checkout_xendit + "?include=date,info",
      {
        method: 'POST',
        headers: {
          'Content-Type': 'multipart/form-data',
          'Accept': 'application/json',
        },
        body: formData
      }
    );

    const responseJson = await response.json();
    console.log("RESPONSE", responseJson)
    callback(responseJson);
    // return responseJson;
  } catch (error) {
    catcher(error);
  }
};

export const removeProductImage = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.fid);
    formData.append('product_id', data.pid)
    formData.append('api_token', token);
    console.log('removeProductImage formData 2: ', formData)
    const response = await fetch(
      Endpoints.remove_product_image + '?include=info,date',
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const googleLogin = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('access_token', data.access_token)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.google_login + '?include=info,date',
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    console.log("googleLogin", err)
  }
}

export const transactionReceive = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('order_id', data.order_id)
    formData.append('product_id', data.product_id)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.transaction_receive,
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const transactionCancel = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('order_id', data.order_id)
    formData.append('product_id', data.product_id)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.transaction_cancel,
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const transactionReturn = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('order_id', data.order_id)
    formData.append('product_id', data.product_id)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.transaction_return,
      {
        method: 'POST',
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const transactionDetails = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('order_id', data.order_id)
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.transaction_details,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const transactionSellerHistory = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('api_token', token);
    console.log("formData", formData)
    const response = await fetch(
      Endpoints.transaction_seller_history,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const transactionSearchOrder = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('order_id', data.order_id)
    formData.append('user_type', data.user_type)
    formData.append('api_token', token);
    console.log("formData", formData)
    const response = await fetch(
      Endpoints.transaction_search_order,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson.data)
  }
  catch (err) {
    catcher(err)
  }
}

export const getItemByNameThroughArray = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('page', 1);
    formData.append('per_page', 100);
    formData.append('search', data.name);
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.all_products + `?include=info,date`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const getItemByName = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('page', 1);
    formData.append('per_page', 10);
    formData.append('search', data.name);
    formData.append('specific', 'product_name');
    formData.append('api_token', token);
    const response = await fetch(
      Endpoints.all_products + `?include=info,date`,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const updateToShip = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('order_id', data.order_id)
    formData.append('courier', data.courier)
    formData.append('tracking_number', data.tracking_number)
    formData.append('api_token', token);
    console.log('updateToShip formData', formData)
    // console.log('data.user_id', data.user_id)
    // console.log('data.order_id', data.order_id)
    // console.log('data.courier', data.courier)
    // console.log('data.tracking_number', data.tracking_number)
    const response = await fetch(
      Endpoints.update_to_ship + `?include=info,date`,
      {
        method: 'POST',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const updateToShipLBC = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('order_id', data.order_id)
    formData.append('shipper_street_address', data.street_address)
    formData.append('shipper_barangay', data.barangay)
    formData.append('shipper_municipality', data.municipality)
    formData.append('shipper_province', data.province)
    formData.append('api_token', token);
    console.log('updateToShip formData', formData)
    // console.log('data.user_id', data.user_id)
    const response = await fetch(
      Endpoints.update_to_ship_lbc + `?include=info,date`,
      {
        method: 'POST',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const updateToShipRCD = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id)
    formData.append('order_id', data.order_id)
    formData.append('shipper_street_address', data.street_address)
    formData.append('shipper_barangay', data.barangay)
    formData.append('shipper_municipality', data.municipality)
    formData.append('shipper_province', data.province)
    formData.append('courier', 'RCD Express')
    formData.append('api_token', token);
    console.log('updateToShip formData', formData)
    // console.log('data.user_id', data.user_id)
    const response = await fetch(
      Endpoints.update_to_ship_rcd + `?include=info,date`,
      {
        method: 'POST',
        headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
        body: formData,
      },
    );
    const responseJson = await response.json();
    callback(responseJson);
  } catch (e) {
    catcher(e);
  }
};

export const getItemReferralData = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.item_id)
    formData.append('referrer', data.referrer_username)
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.product_detail,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getNotificationsList = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.notification_list + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const viewNotification = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.id);
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.notification_view + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getOrderInfo = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('order_id', data.order_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.order_info + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getIncomeLedger = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.income_ledger + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getCheckRequestList = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.check_request + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const createCheckRequest = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append('address', data.address);
    formData.append('mo_transaction', data.mo_transaction);
    formData.append('card_name', data?.card_name);
    formData.append('bd_card_name', data?.bd_card_name);
    formData.append('bd_card_number', data?.bd_card_number);
    formData.append('smart_card_name', data?.smart_card_name);
    formData.append('smart_card_number', data?.smart_card_number);
    formData.append('smart_contact_number', data?.smart_contact_number);
    formData.append('palawan_card_name', data?.palawan_card_name);
    formData.append('palawan_contact_number', data?.palawan_contact_number);
    formData.append('tin_ssn', data.tin_ssn);
    formData.append('amount', data.amount);
    formData.append('tin_file', data.tin_file);
    formData.append('supporting_file', data.supporting_file);
    formData.append("api_token", token);
    // console.log("formData", formData);
    // return
    const response = await fetch(
      Endpoints.submit_check_request + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getAllBuyersTransactions = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.transaction_history + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getAvailableVouchers = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.available_vouchers + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getUnavailableVouchers = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.unavailable_vouchers + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const useVoucher = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append('service_id', data.service_id);
    formData.append('verified', data.verified);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.use_voucher,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const getRecurringServices = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.recurring_services,
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const resendAccountVerifCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.resend_account_verif_code + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const verifyAccountVerifCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append('verification_code', data.verification_code);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.verify_account_verif_code + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const verifyPhoneVerifCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id);
    formData.append('verified', data.verified);
    formData.append("api_token", token);
    // console.log("formdata", formData)
    // console.log("Endpoint", Endpoints.verify_phone_verif_code + "?include=info,user_devices")
    const response = await fetch(
      Endpoints.verify_phone_verif_code + "?include=info,user_devices",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const resendCheckRequestCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.check_request_id);
    formData.append('user_id', data.user_id);
    formData.append("api_token", token);
    const response = await fetch(
      Endpoints.resend_check_request_code + "?include=date,info",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const verifyCheckRequestCode = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('id', data.check_request_id.toString());
    formData.append('user_id', data.user_id.toString());
    formData.append('authentication_code', data.authentication_code);
    formData.append("api_token", token);
    console.log("formData: ", formData)
    // console.log("Endpoint: ", Endpoints.verify_check_request_code + "?include=date")
    // return
    const response = await fetch(
      Endpoints.verify_check_request_code + "?include=date",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export const updateEmailOrContact = async (data, callback, catcher) => {
  try {
    let formData = new FormData();
    formData.append('user_id', data.user_id.toString());
    if (data.email != null) {
      formData.append('email', data.email);
    } else {
      formData.append('contact_number', data.contact_number);
    }
    formData.append('verified', data.verified);
    formData.append("api_token", token);

    console.log("formData: ", formData)
    console.log("Endpoint: ", Endpoints.update_email_contact + "?include=info,user_devices")

    const response = await fetch(
      Endpoints.update_email_contact + "?include=info,user_devices",
      {
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' },
        body: formData,
      }
    )
    const responseJson = await response.json();
    callback(responseJson)
  }
  catch (err) {
    catcher(err)
  }
}

export default {
  //HOMEPAGE
  ads: getSliderAds,
  products: getProducts,
  popularProducts: getPopularProducts,
  featuredProducts: getFeaturedProducts,
  categories: getCategories,
  getAccountCart: getAccountCart,
  resetAccountCart: resetAccountCart,

  //PROFILE
  userData: getUserData,
  userProducts: getUserServices,

  //EDIT
  editProfile: editProfile,
  verifiedUpdateProfile: verifiedUpdateProfile,

  //AUTH
  login: login,
  googleLogin: googleLogin,
  deleteAccount: deleteAccount,

  //SHOP
  updateShop: updateShop,

  getAccountServices: getAccountServices,
  getAccountProductsAndServices: getAccountProductsAndServices,
  updateAccountService: updateAccountService,
  deleteAccountService: deleteAccountService,

  getAccountProducts: getAccountProducts,

  deleteProduct: deleteProduct,

  createService: createService,
  createProduct: createProduct,

  getProductDetails: getProductDetails,

  getMainCategories: getMainCategories,
  getSubCategory: getSubCategory,
  getSpecificCategory: getSpecificCategory,
  getCategoryAttribute: getCategoryAttribute,

  getShippingOptions: getShippingOptions,
  getPartnerLogistics: getPartnerLogistics,
  updateProduct: updateProduct,
  updateService: updateService,

  addProductImage: addProductImage,
  removeProductImage: removeProductImage,

  //Pinned items
  getAllPinnedItems: getAllPinned,

  //User Profile
  addSocialLink: addSocialLink,
  removeSocialLink: removeSocialLink,

  //Add Address
  getAllProvince: getAllProvince,
  getAllCity: getAllCity,
  getAllBarangay: getAllBarangay,
  getShopProfile: getShopProfile,

  getDeliveries: getDeliveries,

  findAccountByEmail: findAccountByEmail,
  authVerifyCode: authVerifyCode,
  authNewPassword: authNewPassword,
  authGoogleSendOtp: authGoogleSendOtp,
  authVerifyOtp: authVerifyOtp,
  TwoFactorAuth: TwoFactorAuth,
  //specific address
  getSpecificAddress: getSpecificAddress,
  addAddress: addAddress,
  updateAddress: updateAddress,
  deleteAddress: deleteAddress,

  //Cart  
  checkoutPaypal: checkoutPaypal,
  checkoutXendit: checkoutXendit,

  transactionReceive: transactionReceive,
  transactionCancel: transactionCancel,
  transactionReturn: transactionReturn,
  transactionDetails: transactionDetails,
  transactionSellerHistory: transactionSellerHistory,
  transactionSearchOrder: transactionSearchOrder,
  updateToShip: updateToShip,
  updateToShipLBC: updateToShipLBC,
  updateToShipRCD: updateToShipRCD,
  getOrderInfo: getOrderInfo,
  getIncomeLedger: getIncomeLedger,
  getCheckRequestList: getCheckRequestList,
  createCheckRequest: createCheckRequest,

  getItemByNameThroughArray: getItemByNameThroughArray,
  getItemByName: getItemByName,

  getItemReferralData: getItemReferralData,

  getNotificationsList: getNotificationsList,
  viewNotification: viewNotification,

  getAllBuyersTransactions: getAllBuyersTransactions,

  getAvailableVouchers: getAvailableVouchers,
  getUnavailableVouchers: getUnavailableVouchers,
  getRecurringServices: getRecurringServices,
  resendAccountVerifCode: resendAccountVerifCode,
  verifyAccountVerifCode: verifyAccountVerifCode,
  verifyPhoneVerifCode: verifyPhoneVerifCode,

  resendCheckRequestCode: resendCheckRequestCode,
  verifyCheckRequestCode: verifyCheckRequestCode,
  updateEmailOrContact: updateEmailOrContact,

  useVoucher: useVoucher,
};
