import React, { useState, useEffect } from 'react';
import { firebase } from '@react-native-firebase/auth';
import { appleAuth, AppleButton } from '@invertase/react-native-apple-authentication';
import { Dimensions, View, Platform, Alert } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

// async function onAppleButtonPress() {
//   // performs login request
//   const appleAuthRequestResponse = await appleAuth.performRequest({
//     requestedOperation: appleAuth.Operation.LOGIN,
//     requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
//   });

//   // get current authentication state for user
//   // /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
//   const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);

//   // use credentialState response to ensure the user is authenticated
//   if (credentialState === appleAuth.State.AUTHORIZED) {
//     // user is authenticated
//     console.log(credentialState, 'asda')
//   }
// }

function AppleSignIn(props) {

  const onAppleButtonPress = async (propss) => {
    // 1). start a apple sign-in request
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });

    // 2). if the request was successful, extract the token and nonce
    const { identityToken, nonce } = appleAuthRequestResponse;

    // can be null in some scenarios
    if (identityToken) {
      // 3). create a Firebase `AppleAuthProvider` credential
      const appleCredential = firebase.auth.AppleAuthProvider.credential(identityToken, nonce);

      // 4). use the created `AppleAuthProvider` credential to start a Firebase auth request,
      //     in this example `signInWithCredential` is used, but you could also call `linkWithCredential`
      //     to link the account to an existing user
      const userCredential = await firebase.auth().signInWithCredential(appleCredential);

      // user is now signed in, any Firebase `onAuthStateChanged` listeners you have will trigger
      console.log(`Firebase authenticated via Apple, UID: ${JSON.stringify(userCredential)}`);
      Alert.alert(
        'Success!',
        'Successfully login using Apple',
        [
          {
            text: 'OK',
            onPress: async () => {
              try {
                await AsyncStorage.setItem('auth', `${userCredential.user.uid}`);
                await AsyncStorage.setItem('user_data', `${JSON.stringify({ data: userCredential.user })}`);
                await AsyncStorage.removeItem('app_refer_state');
                props.navigation.navigation.reset({
                  index: 0,
                  routes: [{ name: 'Tab' }],
                });
              } catch (e) {
                console.warn(e);
              }
            },
          },
        ],
        { cancelable: true }
      );
    } else {
      // handle this - retry?
    }
  }


  function IOSorAndroid(props) {
    if (Platform.OS === 'ios') {
      return (
        <View>
          {/* Render your other social provider buttons here */}
          {appleAuth.isSupported && (
            <AppleButton
              cornerRadius={5}
              style={{ width: Dimensions.get('window').width - 62, height: 40 }}
              buttonStyle={AppleButton.Style.WHITE}
              buttonType={AppleButton.Type.SIGN_IN}
              onPress={() => alert("Under Maintenance")}
            // onPress={() => onAppleButtonPress(props)}
            />
          )}
        </View>
      )
    }
    else {
      return (
        <View></View>
      )
    }
  }

  useEffect(() => {
    // onCredentialRevoked returns a function that will remove the event listener. useEffect will call this function when the component unmounts
    return appleAuth.onCredentialRevoked(async () => {
      console.warn('If this function executes, User Credentials have been Revoked');
    });
  }, []); // passing in an empty array as the second argument ensures this is only ran once when component mounts initially.

  return (
    <View>
      {IOSorAndroid(props)}
    </View>
  );
}

export default AppleSignIn;