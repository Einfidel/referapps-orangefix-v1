import React from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, TextInput, StatusBar, Alert } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
// import { GoogleSignin, statusCodes } from '@react-native-community/google-signin';
import { GoogleSignin, statusCodes } from '@react-native-google-signin/google-signin';
import Service from '../components/api/service';
// import { API_URL, API_TOKEN, URL_GOOGLE_LOGIN } from '../keys'
import Endpoints from '../components/api/endpoints'
import { LoginManager, } from 'react-native-fbsdk-next';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

GoogleSignin.configure({
  // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  // scopes: [
  //   'email', 'profile', 'https://www.googleapis.com/auth/plus.profile.emails.read', 'https://www.googleapis.com/auth/plus.login'
  // ],
  webClientId: '75162238761-1ap1o48ba5u13gu1abvr19764oo33r0o.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  iosClientId: '75162238761-oeds0c4roo7ordic2m2lt3tn7dqsindl.apps.googleusercontent.com', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
});

export const loginWithGoogle = async (navigation) => {
  console.log("google start 1 ");
  try {
    // if (Platform.OS === "android") {
    // console.log("Platform is android");
    await GoogleSignin.hasPlayServices();
    // console.log("Device has Google play services");
    const { idToken } = await GoogleSignin.signIn();
    // console.log("google id token", idToken);

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    console.log("idToken", idToken);
    console.log("google credential", googleCredential);

    try {
      await Service.googleLogin(
        { access_token: idToken },
        async (res) => {
          console.error("Service.googleLogin was succesful");
          console.error("res", res);
          if (res.status) {
            console.error("res.status", res.status);
            await AsyncStorage.setItem('user_data', JSON.stringify(res));
            await AsyncStorage.removeItem('app_refer_state');


            // Sign-in the user with the credential
            auth().signInWithCredential(googleCredential)
              .then(async (response) => {
                // console.log("google sign in", response);
                // getFirebaseUserData(response)

                // Reset Screen back to Home
                navigation.reset({
                  index: 0,
                  routes: [{ name: 'Tab' }],
                });
              })
              .catch(async (error) => {
                alert("Error: \n\n" + error);
                console.error("Error:", error);

                // If firebase did not successfully log in, sign out laravel
                await AsyncStorage.removeItem('auth');
                await AsyncStorage.removeItem('user_data');
                await AsyncStorage.removeItem('chat');
                await AsyncStorage.removeItem('fcmtoken');
              })
          } else {
            let errorString = '';

            Object.keys(res.errors).map((key) => {
              res.errors[key].map((error, index) => {
                errorString += "* " + error + '\n';
              })

              alert("googleLogin error \n\n" + errorString);
              console.error("C.1 googleLogin error", errorString);
              console.error("C.2 googleLogin error 2", res);
            });
          }
        }
      )
    } catch (error) {
      alert(error.message)
      console.log("B. google sign in: error", error)
    }
    // } else {
    //   Alert.alert(
    //     'Error',
    //     'Platform is not Android!',
    //   );
    //   console.log("Platform is not Android");
    // }

    return false;
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
      console.log("google sign in: user has cancelled login")
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
      console.log("google sign in: sign in is in progress already")
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
      console.log("google sign in: play services not available or outdated")
    } else {
      alert(error.message)
      console.log("A. google sign in: error", error)
    }

    return false;
  }
};