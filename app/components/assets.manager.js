const landing = {
  home: require('../assets/bottomNav/icon_home.png'),
  homeActive: require('../assets/bottomNav/icon2_home.png'),
  pinned: require('../assets/bottomNav/icon_pinned.png'),
  pinnedActive: require('../assets/bottomNav/icon2_pinned.png'),
  product: require('../assets/bottomNav/icon_addpro.png'),
  productActive: require('../assets/bottomNav/icon2_addpro.png'),
  notif: require('../assets/bottomNav/icon_notif.png'),
  notifActive: require('../assets/bottomNav/icon2_notif.png'),
  profile: require('../assets/bottomNav/icon_profile.png'),
  profileActive: require('../assets/bottomNav/icon2_profile.png'),
  cart: require('../assets/internal/icon_cart.png'),
  icon_search: require('../assets/internal/icon_search.png'),
  header_icon: require('../assets/header.png')
}

const product = {
  upload_icon: require('../assets/internal/icon-upload.png'),
}

const qr = {
  qr_code: require('../assets/internal/uxwing-free-qr-code.png'),
}

const notifications = {
  notification: require('../assets/internal/notifs/notif.png'),
  approved_check_request: require('../assets/internal/notifs/approved_check_request.png'),
  cancel_order: require('../assets/internal/notifs/cancel_order.png'),
  declined_check_request: require('../assets/internal/notifs/declined_check_request.png'),
  new_order: require('../assets/internal/notifs/new_order.png'),
  new_service: require('../assets/internal/notifs/new_service.png'),
  refund_service: require('../assets/internal/notifs/refund_service.png'),
  return_order: require('../assets/internal/notifs/return_order.png'),
  suspend_product: require('../assets/internal/notifs/suspend_product.png'),
}

const myProfile = {
  settings: require('../assets/Icon_settings2.png'),
  chatIcon: require('../assets/ProfileIcons/icon_chat.png'),
  cartIcon: require('../assets/icon_cart_white.png'),
  qrIcon: require('../assets/ProfileIcons/icon_qr.png'),
  defProductImg: require('../assets/placeholder/placeholder.png'),
  certifiedIcon: require('../assets/internal/icon_Verified.png'),
  bg: require('../assets/internal/default-cover.jpg'),
  face: require('../assets/face0.jpg'),
  icon_minus: require('../assets/icon_minus.png'),
}

const orders = {
  serviceVoucher: require('../assets/internal/icon_voucher.png'),
  productOrder: require('../assets/ProfileIcons/icon_history.png'),
}

const stores = {
  myShop: require('../assets/ProfileIcons/icon_shop.png'),
  myPerformance: require('../assets/ProfileIcons/icon_performance.png'),
  myShipping: require('../assets/ProfileIcons/icon_delivery.png'),
  myIncome: require('../assets/ProfileIcons/icon_income.png'),
  shipIcon: require('../assets/internal/icon_toship.png'),
  transitIcon: require('../assets/internal/icon_intransit.png'),
  deliveredIcon: require('../assets/internal/icon_delivered.png'),
  ledgerIcon: require('../assets/internal/icon_ledger.png'),
  earningIcon: require('../assets/internal/icon_overallearning.png'),
  locationIcon: require('../assets/internal/icon_location.png'),
  starIcon: require('../assets/icon_star1.png')
}

const unAuth = {
  unauthBg: require('../assets/internal/default-cover.jpg'),
  unauthIcon: require('../assets/internal/icon_unauthorized.png')
}

const signIn = {
  googleIcon: require('../assets/internal/icon_gglbtn.png'),
  fbIcon: require('../assets/internal/icon_fbbtn.png'),
  person: require('../assets/user.png'),
  lock: require('../assets/pass.png'),
  signinBg: require('../assets/Splash.jpg')
}

const uploadProduct = {
  categoryIcon: require('../assets/internal/icon_category.png'),
  priceIcon: require('../assets/internal/icon_price.png'),
  stockIcon: require('../assets/internal/icon_stock.png'),
  variations: require('../assets/internal/icon_Variations.png'),
  weightIcon: require('../assets/internal/icon_weight.png'),
  packageSizeIcon: require('../assets/internal/icon_size.png'),
  shippingIcon: require('../assets/internal/icon_shipping_fee.png'),
  conditionIcon: require('../assets/internal/icon_condition.png'),
  subContentIcon: require('../assets/icon_sub.png')
}

const accountSettings = {
  camIcon: require('../assets/internal/icon_cam.png'),
  checkIcon: require('../assets/headericon/continue.png'),
  arrowDarkIcon: require('../assets/headericon/left-arrow-dark.png'),
  arrowLightIcon: require('../assets/headericon/left-arrow.png'),
  searchHelpcenter: require('../assets/Icon_search1.png'),
  emailIcon: require('../assets/icon_email.png'),
  phoneIcon: require('../assets/icon_phone.png'),
  iconBack: require('../assets/internal/icon_back.png'),
  iconFilter: require('../assets/internal/icon_filter.png'),
  iconEx: require('../assets/internal/icon_ex.png'),
  socialIcon1: require('../assets/internal/social_media/icon_1.png'),
}

const specificProduct = {
  cartOrange: require('../assets/internal/icon_cart.png'),
  addCart: require('../assets/internal/product/icon_add2cart.png'),
  iconUnpin: require('../assets/internal/icon_Unpin.png'),
  iconPinned: require('../assets/internal/icon_Pinned.png'),
  iconDelivery: require('../assets/ProfileIcons/icon_delivery.png'),
  iconChat: require('../assets/internal/product/icon_chat.png'),
  referIcon: require('../assets/internal/product/icon_refer.png'),
  faceProf: require('../assets/face0.jpg'),
  icon1: require('../assets/icon1.png')
}

const cartFunction = {
  checkbox: require('../assets/internal/checkbox.png'),
  uncheck: require('../assets/internal/uncheck.png'),
  payment_paypal: require('../assets/internal/payment_paypal.png'),
  payment_xendit: require('../assets/internal/xendit.png'),
  payment_card: require('../assets/internal/payment_card.png'),
  payment_cod: require('../assets/internal/payment_cod.png'),
}

export default {
  landing: landing,
  product: product,
  qr: qr,
  notifications: notifications,
  myProfile: myProfile,
  orders: orders,
  stores: stores,
  unAuth: unAuth,
  signIn: signIn,
  uploadProduct: uploadProduct,
  accountSettings: accountSettings,
  specificProduct: specificProduct,
  cartFunction: cartFunction
}