import React, { useState, useEffect } from 'react';
import { View, ImageBackground, TouchableOpacity, Text, Image, TextInput, StatusBar, Alert, NativeModules } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { LoginButton, AccessToken, LoginManager, Profile, GraphRequest, GraphRequestManager, } from 'react-native-fbsdk-next';
// import { API_URL, API_TOKEN, URL_FB_LOGIN } from '../keys'
import Endpoints from './api/endpoints'
import Service from './api/service';
import { linkAccount } from './LinkAccount.js';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

// export default function loginWithFacebook({ navigation, route }) {
export const loginWithFacebook = async (navigation, props, callback) => {
  console.log("logInWithPermissions");

  console.log("facebook 1")
  await Profile.getCurrentProfile()
    .then(async (res) => {
      console.log("Profile.getCurrentProfile() res:", res)
      if (res != null) {
        // FB signout start
        await AsyncStorage.removeItem('fb_user_id');
        LoginManager.logOut();
        // FB signout end
      }
    })
    .catch((err) => {
      console.log("Profile.getCurrentProfile() err:", err)
    })
  console.log("facebook 2")

  LoginManager.logInWithPermissions(["public_profile", "email", "user_friends"])
    .then(function (result) {
      if (result.isCancelled) {
        console.log("result", result);
        console.log("==> Login cancelled");
      } else {
        AccessToken.getCurrentAccessToken()
          .then((data) => {
            let formData = new FormData();
            formData.append('api_token', Endpoints.token);
            formData.append('access_token', data.accessToken);

            fetch(Endpoints.fb_login, {
              method: 'POST',
              headers: { 'Content-type': 'multipart/form-data', 'Accept': 'application/json' },
              body: formData
            })
              .then((response) => response.json())
              .then(async (responseJson) => {

                // if (responseJson.two_factor_authenticated) {
                //   Alert.alert(
                //     'Success!',
                //     responseJson.msg,
                //     [{
                //       text: 'OK', onPress: async () => {
                try {
                  responseJson.data.lname = responseJson.data.fname.split(" ")[responseJson.data.fname.split(" ").length - 1]

                  // Create a Firebase credential with the AccessToken
                  const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
                  // Sign-in the user with the credential

                  // auth().signInWithRedirect(provider);
                  auth().signInWithCredential(facebookCredential)
                    .then(async (response) => {
                      // Firebase collection get
                      // getFirebaseUserData(response)

                      let fbProfile = await Profile.getCurrentProfile();

                      console.log("Profile.getCurrentProfile()", fbProfile)

                      await Service.userData(
                        responseJson.data.id,
                        async (res2) => {

                          await AsyncStorage.setItem('user_data', JSON.stringify(res2));
                          await AsyncStorage.setItem('fb_user_id', fbProfile.userID)
                          await AsyncStorage.removeItem("app_refer_state");

                          console.log("res2.data", res2.data)
                          console.log("res2.data.info.data.email", res2.data.info.data.email)
                          let splitEmail = res2.data.username.split('@');

                          // console.log("!isNaN(parseFloat(splitEmail[0]))", !isNaN(parseFloat(splitEmail[0])))
                          // console.log("splitEmail[1]", splitEmail[1])

                          if (!isNaN(parseFloat(splitEmail[0])) && splitEmail[1] === 'facebook.com') {
                            navigation.navigate("AddEmailMobile");

                            return false;
                          } else {
                            // Reset Screen back to Home
                            navigation.reset({
                              index: 0,
                              routes: [{ name: 'Tab' }],
                            });

                            return false;
                          }
                        },
                        (err) => {
                          alert("B1. Server Login Error: \n\n" + err);
                          console.err("B1. Server Login Error:", error);

                          return false;
                        }
                      )

                    })
                    .catch(async (error) => {
                      // console.error(error.email, error.credential, error.code);
                      if (error.toString().includes("auth/account-exists-with-different-credential")) {
                        let data = {};
                        data.id = responseJson.data.id;
                        data.pendingCred = facebookCredential;
                        data.responseJson = JSON.stringify(responseJson);
                        linkAccount(navigation, data);

                        return false;

                        // alert("A1. Firebase Login Error: \n\n" + error);
                        // alert("Error: \n\n" + "This email has already been used to ");
                        // console.error();
                      } else {
                        alert("A1. Firebase Login Error: \n\n" + error);
                        console.error("A1. Firebase Login Error:", error);

                        // If firebase did not successfully log in, sign out laravel
                        await AsyncStorage.removeItem('auth');
                        await AsyncStorage.removeItem('user_data');
                        await AsyncStorage.removeItem('chat');
                        await AsyncStorage.removeItem('fcmtoken');

                        await Profile.getCurrentProfile().then(async (res) => {
                          if (res != null) {
                            // FB signout start
                            await AsyncStorage.removeItem('fb_user_id');
                            LoginManager.logOut();
                            // FB signout end
                          }
                        })

                        return false;
                      }
                    })
                } catch (e) {
                  console.warn(e);

                  await Profile.getCurrentProfile().then(async (res) => {
                    if (res != null) {
                      // FB signout start
                      await AsyncStorage.removeItem('fb_user_id');
                      LoginManager.logOut();
                      // FB signout end
                    }
                  })

                  return false;
                }

                //       }
                //     }],
                //     { cancelable: false }
                //   );

                // } else {
                //   navigation.navigate('Two Factor Auth', responseJson.data)
                // }

              }).catch(async (error) => {
                console.error(error);


                await Profile.getCurrentProfile().then(async (res) => {
                  if (res != null) {
                    // FB signout start
                    await AsyncStorage.removeItem('fb_user_id');
                    LoginManager.logOut();
                    // FB signout end
                  }
                })

                return false;
              });

          }
          )
      }
    })
    .catch(async (error) => {
      console.error("Error:", error);
      if (error.msg != null) {
        alert("Error: \n\n" + error.msg);
        console.error("Error:", error);

        return false;
      } else {
        alert("Error: \n\n" + error);
        console.error("Error:", error);

        return false;
      }
    })
};