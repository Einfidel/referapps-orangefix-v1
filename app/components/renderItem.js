import React, { PureComponent } from 'react';
import { StyleSheet, Platform, Dimensions } from 'react-native';
import {
  Text,
  TouchableOpacity,
  View,
  Image
} from 'react-native';
import Icons from '../components/icons';

import * as Progress from 'react-native-progress';

import { createImageProgress } from 'react-native-image-progress';

import FastImage from 'react-native-fast-image';

import StarRating from 'react-native-star-rating';

// import { withNavigation } from 'react-navigation';


// const Image = createImageProgress(FastImage);

class RenderItem extends React.PureComponent {
  render() {

    const { onPress, uri, id, product_name, country_code, price, ratings } = this.props;
    console.log("RenderItem uri", uri)

    return (
      <View style={{
        width: '48%',
        height: 230,
        backgroundColor: '#ffffff',
      }}>
        <TouchableOpacity
          onPress={onPress}
          activeOpacity={.7}
          style={{
            paddingTop: 2,
            paddingRight: 2,
            paddingLeft: 2,
            paddingBottom: 2,
            height: 230,
            borderColor: '#ecf0f1',
            justifyContent: 'space-between',
          }}
        >
          <View style={{
            flex: 0,
          }}>
            {/* <Image
            source={{ uri: uri }}
            style={styles.newProductItem}
            indicator={Progress.Circle}
            indicatorProps={{
              color: '#f36e23'
            }}
            imageStyle={styles.newProductItem}
          /> */}
            <FastImage
              source={{ uri: uri }}
              style={styles.newProductItem}
              indicator={Progress.Circle}
              indicatorProps={{
                color: '#f36e23'
              }}
              imageStyle={styles.newProductItem}
            />
          </View>
          <View style={{
            flex: 1,
            padding: 2,
            justifyContent: 'space-between',
          }}>
            <Text
              style={styles.newProductText}
              numberOfLines={2}
              ellipsizeMode='tail'
            >{product_name}</Text>
            <View style={styles.productRating}>
              <View style={{ alignSelf: 'flex-start', flex: 1 }}>
                <Text style={styles.newProductPrice} numberOfLines={1}>{country_code === 'PH' ? '₱' : '$'}{price}</Text>
              </View>
              {/* <View style={{ alignSelf: 'flex-end', flex: 0 }}> */}
              <View style={{ flex: 1, paddingTop: 2, paddingLeft: 16, paddingRight: 16, flexDirection: 'row', }}>
                {ratings > 0 ?
                  // <StarRating
                  //   disabled={true}
                  //   emptyStar={'ios-star-outline'}
                  //   fullStar={'ios-star'}
                  //   halfStar={'ios-star-half'}
                  //   iconSet={'Ionicons'}
                  //   maxStars={5}
                  //   rating={ratings}
                  //   fullStarColor={'#f36e23'}
                  //   halfStarColor={'#f36e23'}
                  //   starSize={10}
                  //   style={{justifyContent: 'flex-end'}}
                  // /> 
                  [1, 2, 3, 4, 5].map((rate, i) => {
                    return (
                      <Icons.AntDesign key={i} name={ratings >= rate ? 'star' : 'staro'} size={10} color='orange' />
                    );
                  })
                  :
                  <Text style={{
                    // fontFamily: 'Roboto',
                    color: '#7f8c8d',
                    fontSize: 10,
                  }} numberOfLines={1}
                    ellipsizeMode='tail'>({ratings})</Text>}
              </View>
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

// export default withNavigation(RenderItem);
export default RenderItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
  },
  container1: {
    backgroundColor: '#ecf0f1',
  },
  categories: {
    backgroundColor: '#ffffff',
  },
  categoriesText: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    paddingLeft: 10,
    color: '#f36e23'
  },
  categoriesRow: {
    flex: 1,
    alignItems: "flex-start",
    justifyContent: "space-around",
  },

  newProduct: {
    flex: 1,
  },

  categoriesContainer: {
    marginTop: 20
  },

  newestItemText: {
    // fontFamily: 'Roboto',
    fontSize: 16,
    color: '#f36e23',
    paddingLeft: 12,
  },

  newProductText: {
    // fontFamily: 'Roboto',
    paddingLeft: 2,
    paddingTop: 5,
    color: '#231f20',
    fontSize: 15,
  },

  newProductPrice: {
    // fontFamily: 'Roboto',
    fontSize: 10,
    paddingLeft: 2,
    color: '#ea4123',
  },

  productRating: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  newProductItem: {
    backgroundColor: '#ffffff',
    paddingTop: 2,
    paddingRight: 2,
    paddingLeft: 2,
    paddingBottom: 2,
    height: 170,
    borderColor: '#ecf0f1',
    justifyContent: 'space-between',
  },

  newProductSeparator: {
    width: 20,
    backgroundColor: '#ecf0f1',
    height: '100%',
  },

  newProductImage: {
    alignSelf: 'center',
    width: Dimensions.get('window').width * .455,
    height: Dimensions.get('window').width * .455,
  },

  popularProducts: {
    backgroundColor: '#ecf0f1',
  },
  popularProductsText: {
    paddingLeft: 10,
    fontSize: 16,
  },
  popularProductsRow: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
    paddingLeft: 4,
    paddingRight: 4,
  },
  popularProductsRowItem: {
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: 'rgba(0,0,0,0.4)',
    fontSize: 14,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    backgroundColor: '#ecf0f1',
    paddingTop: 10,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  headerImage: {
    width: 100,
    height: 80,
    resizeMode: 'contain',
  },
  searchImage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginLeft: 10,
  },
  cartimage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginRight: 10,
  },
  bottomTabImage: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  pinnedIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginRight: 5,
    marginLeft: 10,
  },
  pinItemText: {
    color: '#7f8c8d',
    fontSize: 18,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80
  },
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#9DD6EB',
    padding: 15,
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  space: {
    paddingTop: 5,
  },
  space10: {
    paddingTop: 10,
  },
});
