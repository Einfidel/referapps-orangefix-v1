import firebase from 'firebase';
import moment from 'moment';
import messaging from '@react-native-firebase/messaging';

const tokensRef = firebase.database().ref('tokens');
const FCM_LEGACY_SERVER_KEY = 'AAAAEYAEvyk:APA91bEIgCELUsKaqEk91kaL0TB3J-zj1jYzNvDwtqxcz5NaF9siwBOCGpZVoY-hKQikCkTeVRkmujPUfbH58qx8LHToxbcfQSpcu2AO1CtgShG-KrmeTv7B-_hBocDcV4Ek_k181kN7';

export const getTimeStamp = () => {
  return firebase.database.ServerValue.TIMESTAMP;
};

//function: register token every login
export const registerToken = (token, userId) => {
  //check if user id is existing
  let isUserRegistered = false;
  tokensRef
    .once('value', function (snapshot) {
      snapshot.forEach(function (childSnapshot) {
        var childKey = childSnapshot.key;
        if (childKey.toString() === userId.toString()) isUserRegistered = true;
      });
    })
    .then(() => {
      // console.warn('User registered', isUserRegistered);
      if (!isUserRegistered) {
        // console.warn('User ID not found! Registering new token...');
        tokensRef.child(userId).set({
          userId,
          token,
          timestamp: getTimeStamp(),
        });
      } else {
        //if not, push
        //if existing, update current token
        // console.warn('User ID found, updating current token...');
        tokensRef.child(userId).ref.on('value', (snapshot) => {
          let data = snapshot.val();
          data.token !== token && tokensRef.child(userId).update({token});
        });
      }
    });
};

//function: remove token every logout
export const removeToken = async () => {
  let token = await messaging().getToken();
  console.warn('Token to delete', token);
  await messaging().deleteToken(token);
  await messaging().unregisterDeviceForRemoteMessages();
};

//function: get token of receiver in order to send notification
export const getReceiverToken = (userId, callBack) => {
  tokensRef.child(userId).ref.on('value', (snapshot) => {
    let data = snapshot.val();
    callBack(data.token);
  });
};

//function: send notification
export const sendNotification = async (
  sender,
  message,
  receiverToken,
  type,
) => {
  let date = new moment();
  const SAMPLE = {
    registration_ids: [receiverToken],
    notification: {
      title: 'ReferApps',
      body: message,
      vibrate: 1,
      sound: 'kaching',
      show_in_foreground: true,
      priority: 'high',
      content_available: true,
      date,
    },
    data: {
      title: 'ReferApps',
      body: 'Sample message',
      senderID: sender,
      messageType: type,
      date,
    },
  };
  let response = await fetch('https://fcm.googleapis.com/fcm/send', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: 'key=' + FCM_LEGACY_SERVER_KEY,
    },
    body: JSON.stringify(SAMPLE),
  });
  console.warn('Res', response);
  if (response.status !== 200) {
    return {status: response.ok, status_code: response.status};
  }
  response = await response.json();
  return response;
};

export default {
  registerToken,
  getReceiverToken,
  sendNotification,
  removeToken,
};
