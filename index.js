/**
 * @format
 */

import { AppRegistry, Platform } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import auth from '@react-native-firebase/auth';
import messaging, { AuthorizationStatus } from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';
// import { Settings } from 'react-native-fbsdk-next';
var PushNotification = require("react-native-push-notification");

// Settings.initializeSDK();

if (auth().currentUser) {
  console.log('Index js auth().currentUser is valid');
  if (Platform.OS === 'ios') {
    console.log('Index js platform is ios');
    messaging()
      // .getIsHeadless()
      .setBackgroundMessageHandler(async (remoteMessage) => {
        console.log('Message handled in the background!', remoteMessage);

        // let { data } = remoteMessage.data;
        // let chats = await AsyncStorage.getItem(data.messageType);
        // if (!chats) {
        //   await AsyncStorage.setItem(
        //     data.messageType,
        //     JSON.stringify([remoteMessage]),
        //   );
        // } else {
        //   chats = JSON.parse(chats);
        //   chats.push(remoteMessage);
        //   await AsyncStorage.setItem(data.messageType, JSON.stringify(chats));
        // }

        // PushNotification.localNotification({
        //   title: remoteMessage.data.notification.title,
        //   message: remoteMessage.data.notification.body, 
        //   playSound: true,
        //   soundName: "default"
        // });

        return

      });
  } else {
    console.log('Index js platform is android');
    messaging()
      .setBackgroundMessageHandler(async (remoteMessage) => {
        console.log('Index js Message handled in the background!', remoteMessage);

        // let { data } = remoteMessage;
        // let chats = await AsyncStorage.getItem(data.messageType);

        // if (!chats) {
        //   await AsyncStorage.setItem(
        //     data.messageType,
        //     JSON.stringify([remoteMessage]),
        //   );
        // } else {
        //   chats = JSON.parse(chats);
        //   chats.push(remoteMessage);
        //   await AsyncStorage.setItem(data.messageType, JSON.stringify(chats));
        // }

        return

        // PushNotification.localNotification({
        //   title: remoteMessage.notification.title,
        //   message: remoteMessage.notification.body,
        //   playSound: true,
        //   soundName: "kaching.mp3"
        // });

      });
  }
}

AppRegistry.registerComponent(appName, () => App);
