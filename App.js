import 'react-native-gesture-handler';
import * as React from 'react';
import AppRouter from './app/routes.js';

export default function App() {
  return <AppRouter />;
}
